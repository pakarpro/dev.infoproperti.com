<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-02 15:17:01 KRAT */ ?>

<?php if ($this->_vars['phone_format']): ?>
<?php echo tpl_function_js(array('file' => 'jquery.maskedinput.min.js'), $this);?>
<script><?php echo '
$(function(){
	$(\'#phone\').mask(\'';  echo $this->_vars['phone_format'];  echo '\');
});
'; ?>
</script>
<?php endif; ?>
<form action="" method="post" enctype="multipart/form-data">
	<div class="r">
		<div class="f"><?php echo l('field_fname', 'users', '', 'text', array()); ?>:&nbsp;<span class="ast">*</span></div>
		<div class="v"><input type="text" name="data[fname]" value="<?php echo $this->_run_modifier($this->_vars['data']['fname'], 'escape', 'plugin', 1); ?>
"></div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_sname', 'users', '', 'text', array()); ?>:</div>
		<div class="v"><input type="text" name="data[sname]" value="<?php echo $this->_run_modifier($this->_vars['data']['sname'], 'escape', 'plugin', 1); ?>
"></div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_email', 'users', '', 'text', array()); ?>:&nbsp;<span class="ast">*</span></div>
		<div class="v"><input type="text" name="data[email]" value="<?php echo $this->_run_modifier($this->_vars['data']['email'], 'escape', 'plugin', 1); ?>
"></div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_phone', 'users', '', 'text', array()); ?>:&nbsp;<span class="ast">*</span></div>
		<div class="v"><input type="text" name="data[phone]" value="<?php echo $this->_run_modifier($this->_vars['data']['phone'], 'escape', 'plugin', 1); ?>
" id="phone"></div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_password', 'users', '', 'text', array()); ?>:&nbsp;<span class="ast">*</span></div>
		<div class="v"><input type="password" name="data[password]"></div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_repassword', 'users', '', 'text', array()); ?>:&nbsp;<span class="ast">*</span></div>
		<div class="v"><input type="password" name="data[repassword]"></div>
	</div>
	<div class="r">
		<div class="f"><input type="checkbox" name="license_agreement" value="1" maxlength="<?php echo $this->_vars['data']['show_contact_info']; ?>
" id="license_agreement" /> <label for="license_agreement"><?php echo l('field_license_agreement', 'users', '', 'text', array()); ?></label></div>
		<div class="v"></div>
	</div>	
	<div class="r">
		<div class="f"><?php echo l('field_captcha', 'users', '', 'text', array()); ?>:&nbsp;<span class="ast">*</span></div>
		<div class="v captcha"><?php echo $this->_vars['data']['captcha_image']; ?>
 <input type="text" name="captcha_confirmation" value="" maxlength="<?php echo $this->_vars['data']['captcha_word_length']; ?>
" /></div>
	</div>
	<div class="r">
		<div class="f">&nbsp;</div>
		<div class="v"><input type="submit" value="<?php echo l('btn_register', 'start', '', 'button', array()); ?>" name="btn_register"></div>
	</div>
	</form>
