<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-01 06:14:30 KRAT */ ?>

<?php echo tpl_function_js(array('module' => reviews,'file' => 'reviews-list.js'), $this);?>
<div id="reviews_list"><?php echo $this->_vars['block']; ?>
</div>
<script><?php echo '
$(function(){
	new ReviewsList({
		siteUrl: \'';  echo $this->_vars['site_url'];  echo '\', 
		listAjaxUrl: \''; ?>
reviews/ajax_get_reviews/<?php echo $this->_vars['review_type_gid']; ?>
/<?php echo $this->_vars['review_object_id'];  echo '\',
		order: \'';  echo $this->_vars['order'];  echo '\',
		orderDirection: \'';  echo $this->_vars['order_direction'];  echo '\',
		page: \'';  echo $this->_vars['page'];  echo '\',
		tIds: [\'pages_block_1\', \'pages_block_2\', \'sorter_block\'],
	});
});
'; ?>
</script>
