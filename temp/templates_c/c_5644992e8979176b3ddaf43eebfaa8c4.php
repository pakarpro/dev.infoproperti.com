<?php require_once('/opt/ip/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-09-10 16:36:04 KRAT */ ?>

<div class="content-block load_content">
	<h1><?php echo l('header_reviews_form', 'reviews', '', 'text', array()); ?></h1>
	<div class="inside edit_block">
		<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->module_path. "reviews". $this->module_templates.  $this->get_current_theme_gid('"index"', '"reviews"'). "review_form.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
	</div>
</div>
