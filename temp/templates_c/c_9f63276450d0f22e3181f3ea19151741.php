<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-02 15:56:03 KRAT */ ?>

<?php echo tpl_function_js(array('module' => listings,'file' => 'mortgage-calc.js'), $this);?>
<script>
<?php echo '
    function onChangeRupiah(value)
    {
        var propertiVal = document.getElementById("properti_value").value;
        var dpPercent = document.getElementById("deposit_value_percentage").value;
        var dpRupiah = document.getElementById("deposit_value_rp");
        var principal = document.getElementById("principal");
        
        dpPercent = dpPercent / 100;
    
        if(propertiVal.length >= 1 || propertiVal.length != "")
        {
            dpRupiah.value = propertiVal * dpPercent;
			principal.value = (propertiVal - dpRupiah.value);
        }
        else if(propertiVal == 0 || propertiVal == "")
            {
                dpRupiah.value = 0 * dpPercent;
				principal.value = (propertiVal - dpRupiah.value);
            }
        
    }
    
    function onChangePercent(value)
    {
        var propertiVal = document.getElementById("properti_value").value;
        var dpPercent = document.getElementById("deposit_value_percentage");
        var dpRupiah = document.getElementById("deposit_value_rp").value;
        var principal = document.getElementById("principal");
    
        dpPercent.value = (dpRupiah/propertiVal) * 100;
        principal.value = (propertiVal - dpRupiah);
    }
'; ?>

</script>
<div class="mortgage_calc_block edit_form">
	<div class="inside">
		<h2><?php echo l('mortgage_calc_title', 'listings', '', 'text', array()); ?></h2>
		<form method="POST">
			<div class="r">
				<label>1.Harga properti *</label>
				<input type="text" name="properti_value" size="15" id="properti_value" onKeyUp="onChangeRupiah(this.value)">
			</div>
			<div class="r">
				<label>2.Uang muka *</label>
				<input class="deposit_value_percentage" type="text" name="deposit_value_percentage" size="5" value="30" id="deposit_value_percentage" onKeyUp="onChangeRupiah(this.value)">&nbsp;%<br />
				<input class="deposit_value_rp" type="text" name="deposit_value_rp" size="15" id="deposit_value_rp" onKeyUp="onChangePercent(this.value)">
			</div>			
			<div class="r">
				<label>3.&nbsp;<?php echo l('mortgage_calc_query1', 'listings', '', 'text', array()); ?>&nbsp;*</label>
				<input type="text" name="principal" size="15" id="principal">
			</div>
			<div class="r">
				<label>4.&nbsp;<?php echo l('mortgage_calc_query2', 'listings', '', 'text', array()); ?>&nbsp;*</label>
				<input type="text" name="intRate" size="15" value="10">
			</div>
			<div class="r">
				<label>5.&nbsp;<?php echo l('mortgage_calc_query3', 'listings', '', 'text', array()); ?>&nbsp;*</label>
				<input type="text" name="numYears" size="15" value="10">
			</div>
			<div class="r">
				<input type="button" value="<?php echo l('mortgage_calc_compute', 'listings', '', 'button', array()); ?>" id="calc_btn">
				<input type="button" value="<?php echo l('mortgage_calc_reset', 'listings', '', 'button', array()); ?>" id="empty_btn">
			</div>
			<div class="r">
				<label>6.&nbsp;<?php echo l('mortgage_calc_query4', 'listings', '', 'text', array()); ?>&nbsp;*</label>
				<input type="text" name="moPmt" size="15" dir="ltr">
				<input type="hidden" name="HmoPmt" value=0 size="15">
			</div>
			<p><?php echo l('mortgage_calc_create', 'listings', '', 'text', array()); ?></p>
			<p><input type="button" value="<?php echo l('mortgage_calc_createbutton', 'listings', '', 'button', array()); ?>" id="results_btn"></p>
			<p class="subtext"><?php echo l('mortgage_calc_content2', 'listings', '', 'button', array()); ?></p>
		</form>
	</div>
</div>
<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'currency')); tpl_block_capture(array('assign' => 'currency'), null, $this); ob_start();  $this->_tag_stack[] = array('tpl_block_strip', array()); tpl_block_strip(array(), null, $this); ob_start(); ?>
	<?php echo tpl_function_block(array('name' => 'currency_output','module' => 'start','value' => '%s'), $this);?>
<?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_strip($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack);  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
<script><?php echo '
	$(function(){
		new mortgageCalc({
			siteUrl: \'';  echo $this->_vars['site_url'];  echo '\',
			selectedCurrAbbr: \'';  echo $this->_run_modifier($this->_vars['currency'], 'strip_tags', 'PHP', 1);  echo '\',
		});
	});
'; ?>
</script>