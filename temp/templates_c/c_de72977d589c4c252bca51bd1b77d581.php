<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-02 15:54:48 KRAT */ ?>

<?php echo tpl_function_js(array('module' => start,'file' => 'search.js'), $this);?>
<?php echo tpl_function_js(array('module' => start,'file' => 'selectbox.js'), $this);?>
<?php echo tpl_function_js(array('module' => start,'file' => 'checkbox.js'), $this);?>
<?php echo tpl_function_js(array('module' => start,'file' => 'bookmark.js'), $this);?>
<script>var forms_<?php echo $this->_vars['form_settings']['rand']; ?>
 = <?php echo '{}'; ?>
;</script>
<div class="search-box"><!-- Search home -->
    <?php if ($this->_vars['form_settings']['show_tabs']): ?>
	<div class="tabs tab-size-15" id="search-bookmark-<?php echo $this->_vars['form_settings']['rand']; ?>
">
	    <ul>
		<?php if (is_array($this->_vars['form_settings']['tabs']) and count((array)$this->_vars['form_settings']['tabs'])): foreach ((array)$this->_vars['form_settings']['tabs'] as $this->_vars['key'] => $this->_vars['item']): ?>
		<li<?php if ($this->_vars['form_settings']['object'] == $this->_vars['key']): ?> class="active"<?php endif; ?> id="<?php echo $this->_vars['key']; ?>
-form-tab-<?php echo $this->_vars['form_settings']['rand']; ?>
"><a href="#"><?php echo $this->_vars['item']; ?>
</a></li>
		<script>forms_<?php echo $this->_vars['form_settings']['rand']; ?>
['<?php echo $this->_vars['key']; ?>
']='<?php echo $this->_vars['modules'][$this->_vars['key']]; ?>
';</script>
		<?php endforeach; endif; ?>
	    </ul>
	</div>
    <?php endif; ?>
    <div id="search-form-block-<?php echo $this->_vars['form_settings']['rand']; ?>
"><?php echo $this->_vars['form_block']; ?>
</div>
</div>
<?php if ($this->_vars['form_block']): ?>
    <?php echo '
    <script type="text/javascript">
    $(function(){
	new searchBookmark({\'bmID\': \'search-bookmark-';  echo $this->_vars['form_settings']['rand'];  echo '\'});
	new search({
	    siteUrl: \'';  echo $this->_vars['site_url'];  echo '\', 
	    currentForm: \'';  echo $this->_vars['form_settings']['object'];  echo '\', 
	    currentFormType: \'';  echo $this->_vars['form_settings']['type'];  echo '\',
	    forms: forms_';  echo $this->_vars['form_settings']['rand'];  echo ',
	    rand: \'';  echo $this->_vars['form_settings']['rand'];  echo '\'
	});
    });
    </script>
    '; ?>

<?php endif; ?>
