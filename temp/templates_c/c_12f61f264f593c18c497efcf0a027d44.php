<?php require_once('/opt/ip/system/libraries/template_lite/plugins/function.seolink.php'); $this->register_function("seolink", "tpl_function_seolink");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-02 15:51:01 WIB */ ?>

<?php if ($this->_vars['listings']): ?>
<?php echo tpl_function_js(array('file' => 'dualslider/jquery.dualSlider.0.3.min.js'), $this);?>
<?php echo tpl_function_js(array('file' => 'dualslider/jquery.timers-1.2.js'), $this);?>

<script>var forms_<?php echo $this->_vars['slider_form_settings']['rand']; ?>
 = <?php echo '{}'; ?>
;</script>
<div class="slider_<?php echo $this->_run_modifier($this->_vars['view'], 'escape', 'plugin', 1); ?>
">
	<div class="slider_wrapper">
		<div class="slider_wrapper2">
			<div class="slider">
				<div class="carousel">		
					<div class="backgrounds_wrapper">
						<div class="backgrounds_wrapper2">
							<div class="backgrounds_wrapper3">
								<div class="backgrounds">
														
									<!-- comp slider -->
									<?php /*<div class="item item_100" style="background: url('application/views/default/img/custom-slides/competition-slider.jpg') no-repeat;">
										<a href="http://infoproperti.com/competition/" style="width:100%;height:520px;display:block;" target="_blank"></a>
									</div>*/ ?>
									
									<!-- lebaran slider -->
									<?php /*<div class="item item_100" style="background: url('application/views/default/img/custom-slides/slide-lebaran-6.jpg') no-repeat;"></div>*/ ?>
														
									<!-- 17 agustus slider -->
									<?php /*<div class="item item_100" style="background: url('application/views/default/img/custom-slides/slider-17-agustus-revisi.jpg') no-repeat;"></div>
									<div class="item item_100" style="background: url('application/views/default/img/custom-slides/slider-17-agustus-1.jpg') no-repeat;"></div>*/ ?>
									
									<!-- slider national sport day -->
									<?php /*<div class="item item_100" style="background: url('application/views/default/img/custom-slides/slider-hari-olahraga-nasional.jpg') no-repeat;"></div>*/ ?>
														
									<!-- slider pmi -->
									<?php /*<div class="item item_100" style="background: url('application/views/default/img/custom-slides/slider-pmi.jpg') no-repeat;"></div>*/ ?>
									
									<!-- slider batik -->
									<?php /*<div class="item item_100" style="background: url('application/views/default/img/custom-slides/batik-slider.jpg') no-repeat;"></div>*/ ?>
									
									<!-- slider sumpah pemuda -->
									<?php /*<div class="item item_100" style="background: url('application/views/default/img/custom-slides/slider-sumpah-pemuda.jpg') no-repeat;"></div>*/ ?>
									
									<!-- slider hari guru nasional -->
									<?php /*<div class="item item_100" style="background: url('application/views/default/img/custom-slides/slider-hari-guru.jpg') no-repeat;"></div>*/ ?>

									<?php /* =========== Disabled. for static slider only ===================*/ ?>
									<?php /*disable temporarily <?php if (is_array($this->_vars['listings']) and count((array)$this->_vars['listings'])): foreach ((array)$this->_vars['listings'] as $this->_vars['key'] => $this->_vars['item']): ?> */?>
									<?php /*<div class="item item_<?php echo $this->_run_modifier($this->_vars['key'], 'escape', 'plugin', 1); ?>
" style="background: url('http://infoproperti.com/application/views/default/img/custom-slides/main-img-jakarta.jpg') no-repeat;"></div> */?>
									<?php /* disable temporarily <?php endforeach; endif; ?> */?>
									
									<?php /* =========== for Original slider only  ===================*/ ?>
									<?php if (is_array($this->_vars['sliders']) and count((array)$this->_vars['sliders'])): foreach ((array)$this->_vars['sliders'] as $this->_vars['item']): ?>
										<?php if ($this->_vars['item']['slider_url'] != ""): ?>
											<a href="<?php echo $this->_vars['item']['slider_url']; ?>
" target="_blank">
												<div class="item item_<?php echo $this->_vars['item']['key']; ?>
" style="background: url('<?php echo $this->_vars['item']['media']; ?>
') no-repeat;"></div>
											</a>
										<?php else: ?>
											<div class="item item_<?php echo $this->_vars['item']['key']; ?>
" style="background: url('<?php echo $this->_vars['item']['media']; ?>
') no-repeat;"></div>
										<?php endif; ?>
									<?php endforeach; endif; ?>

									<?php /*<?php if (is_array($this->_vars['listings']) and count((array)$this->_vars['listings'])): foreach ((array)$this->_vars['listings'] as $this->_vars['key'] => $this->_vars['item']): ?>
									<?php if ($this->_vars['item']['slider_image'] != ""): ?>	  #MOD# 
										<div class="item item_<?php echo $this->_run_modifier($this->_vars['key'], 'escape', 'plugin', 1); ?>
" style="background: url('<?php echo $this->_vars['item']['media']['slider']['thumbs'][$this->_vars['view']]; ?>
') no-repeat;"></div>
									<?php endif; ?>  #MOD# 
									<?php endforeach; endif; ?>*/ ?>
						
								</div>
							</div>	
						</div>
					</div>
						
					<div class="gradient_wrapper">
						<div class="gradient_wrapper2">
							<div class="gradient">
								<div class="gradient-l"></div>
								<div class="gradient-r"></div>
							</div>
						</div>
					</div>
					
					<?php if ($this->_vars['slider_form_page_data']['count'] > 1): ?>
					<div class="paging_wrapper">
						<div class="paging_wrapper2">
							<div class="paging">
								<a id="previous_item" class="previous" alt="<?php echo l('nav_prev', 'start', '', 'button', array()); ?>" title="<?php echo l('nav_prev', 'start', '', 'button', array()); ?>"><?php echo l('nav_prev', 'start', '', 'text', array()); ?></a>
								<a id="next_item" class="next" alt="<?php echo l('nav_next', 'start', '', 'button', array()); ?>" title="<?php echo l('nav_next', 'start', '', 'button', array()); ?>"><?php echo l('nav_next', 'start', '', 'text', array()); ?></a>
							</div>
						</div>
					</div>	
					<?php endif; ?>
					
					<div class="panel"> <!-- disable for later use-->
					<?php /*
						<div class="details_wrapper">
							<div class="details">
								<?php if (is_array($this->_vars['listings']) and count((array)$this->_vars['listings'])): foreach ((array)$this->_vars['listings'] as $this->_vars['key'] => $this->_vars['item']): ?>
								<div class="listing detail">
									<a href="<?php echo tpl_function_seolink(array('module' => 'users','method' => 'view','data' => $this->_vars['item']['user']), $this);?>"><img src="<?php echo $this->_vars['item']['user']['media']['user_logo']['thumbs']['small']; ?>
" alt="<?php echo $this->_run_modifier($this->_vars['item']['user']['output_name'], 'escape', 'plugin', 1); ?>
"></a>
									<a href="<?php echo tpl_function_seolink(array('module' => 'users','method' => 'view','data' => $this->_vars['item']['user']), $this);?>"><?php echo $this->_run_modifier($this->_vars['item']['user']['output_name'], 'truncate', 'plugin', 1, 50); ?>
</a>							
									<a href="<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'view','data' => $this->_vars['item']), $this);?>"><?php if ($this->_vars['view'] == '654_395'):  echo $this->_run_modifier($this->_vars['item']['output_name'], 'truncate', 'plugin', 1, 50);  else:  echo $this->_run_modifier($this->_vars['item']['output_name'], 'truncate', 'plugin', 1, 100);  endif; ?></a>
									<span><?php echo $this->_vars['item']['property_type_str']; ?>
 <?php echo $this->_vars['item']['operation_type_str']; ?>
</span>
									<span><?php echo tpl_function_block(array('name' => listing_price_block,'module' => 'listings','data' => $this->_vars['item'],'template' => 'small'), $this);?></span>
								</div>
								<?php endforeach; endif; ?>
							
							</div>
							<div class="details_background"></div>
						</div>
						*/?>
					</div>
					
					
					<div class="slider_search_form">
					<h2 id="inner-search-title-top"><span>Temukan properti <i>ideal</i> anda</span></h2>
						<?php echo $this->_vars['slider_search_form']; ?>

						<div class="background"></div>
					</div>		
				</div>	
				
			</div>
		</div>
	</div>
</div>
<script><?php echo '
	$(function(){
		$(".carousel").dualSlider({
			auto: ';  if ($this->_vars['slider_form_page_data']['slider_auto']): ?>true<?php else: ?>false<?php endif;  echo ',
			autoDelay: ';  if ($this->_vars['slider_form_page_data']['slider_auto']):  echo $this->_vars['slider_form_page_data']['rotation'];  else: ?>false<?php endif;  echo ',
			easingCarousel: "swing",
			easingDetails: "swing",
			durationCarousel: 300,
			durationDetails: 300,
			widthsliderimage: $(".carousel .backgrounds .item").width(),
			';  if ($this->_vars['_LANG']['rtl'] == 'rtl'): ?>rtl: true,<?php endif;  echo '
		});
		$(\'.gradient .gradient-l\').bind(\'click\', function(){
			';  if ($this->_vars['_LANG']['rtl'] === 'rtl'):  echo '
			var next = $(\'#next_item\');
			if(next.css(\'display\') != \'none\') next.trigger(\'click\');
			';  else:  echo '
			var previous = $(\'#previous_item\');
			if(previous.css(\'display\') != \'none\')  previous.trigger(\'click\');
			';  endif;  echo '
		});
		$(\'.gradient .gradient-r\').bind(\'click\', function(){
			';  if ($this->_vars['_LANG']['rtl'] === 'rtl'):  echo '
			var previous = $(\'#previous_item\');
			if(previous.css(\'display\') != \'none\')  previous.trigger(\'click\');
			';  else:  echo '
			var next = $(\'#next_item\');
			if(next.css(\'display\') != \'none\') next.trigger(\'click\');
			';  endif;  echo '
			
		});
		$(\'.slider.backgrounds .item\').bind(\'click\', function(){
			';  if ($this->_vars['_LANG']['rtl'] === 'rtl'):  echo '
			var previous = $(\'#previous_item\');
			if(previous.css(\'display\') != \'none\')  previous.trigger(\'click\');
			';  else:  echo '
			var next = $(\'#next_item\');
			if(next.css(\'display\') != \'none\') next.trigger(\'click\');
			';  endif;  echo '
		});
	});
'; ?>
</script>
<?php endif; ?>
