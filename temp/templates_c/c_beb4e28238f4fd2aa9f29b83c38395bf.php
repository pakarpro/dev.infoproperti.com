<?php require_once('/opt/ip/system/libraries/template_lite/plugins/function.block.php'); $this->register_function("block", "tpl_function_block");  require_once('/opt/ip/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.ld_option.php'); $this->register_function("ld_option", "tpl_function_ld_option");  require_once('/opt/ip/system/libraries/template_lite/plugins/modifier.date_format.php'); $this->register_modifier("date_format", "tpl_modifier_date_format");  require_once('/opt/ip/system/libraries/template_lite/plugins/modifier.truncate.php'); $this->register_modifier("truncate", "tpl_modifier_truncate");  require_once('/opt/ip/system/libraries/template_lite/plugins/modifier.escape.php'); $this->register_modifier("escape", "tpl_modifier_escape");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.seolink.php'); $this->register_function("seolink", "tpl_function_seolink");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.pagination.php'); $this->register_function("pagination", "tpl_function_pagination");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.sorter.php'); $this->register_function("sorter", "tpl_function_sorter");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-12-14 16:30:55 KRAT */ ?>

	<?php if ($this->_vars['requests']): ?>
	<div class="sorter line" id="sorter_block">
		<?php echo tpl_function_sorter(array('links' => $this->_vars['sort_data']['links'],'order' => $this->_vars['sort_data']['order'],'direction' => $this->_vars['sort_data']['direction'],'url' => $this->_vars['sort_data']['url']), $this);?>
		<div class="fright" id="pages_block_1"><?php echo tpl_function_pagination(array('data' => $this->_vars['page_data'],'type' => 'cute'), $this);?></div>
	</div>
	<?php endif; ?>
	
	<div>	
		<?php if (is_array($this->_vars['requests']) and count((array)$this->_vars['requests'])): foreach ((array)$this->_vars['requests'] as $this->_vars['item']): ?>
		<div class="listing-block">
			<div id="item-block-<?php echo $this->_vars['item']['id']; ?>
" class="item listing">
				<div class="image">
					<a href="<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'view','data' => $this->_vars['item']['listing']), $this);?>">
						<img src="<?php echo $this->_vars['item']['listing']['media']['photo']['thumbs']['small']; ?>
" title="<?php echo $this->_run_modifier($this->_vars['item']['output_name'], 'escape', 'plugin', 1); ?>
">
					</a>
				</div>
				<div class="body">
					<h3><a href="<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'view','data' => $this->_vars['item']['listing']), $this);?>" title="<?php echo $this->_run_modifier($this->_vars['item']['listing']['output_name'], 'escape', 'plugin', 1); ?>
"><?php echo $this->_run_modifier($this->_vars['item']['listing']['output_name'], 'truncate', 'plugin', 1, 50); ?>
</a></h3>
					<div class="t-1">		
													<?php switch($this->_vars['item']['listing']['price_period']): case '1':  ?>
								<?php echo $this->_run_modifier($this->_vars['item']['date_start'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']); ?>
 - <?php echo $this->_run_modifier($this->_vars['item']['date_end'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']); ?>

							<?php break; case '2':  ?>
								<?php echo tpl_function_ld_option(array('i' => 'month-names','gid' => 'start','option' => $this->_run_modifier($this->_vars['item']['date_start'], 'date_format', 'plugin', 1, '%m')), $this);?>
								<?php echo $this->_run_modifier($this->_vars['item']['date_start'], 'date_format', 'plugin', 1, '%Y'); ?>

								&mdash; 
								<?php echo tpl_function_ld_option(array('i' => 'month-names','gid' => 'start','option' => $this->_run_modifier($this->_vars['item']['date_end'], 'date_format', 'plugin', 1, '%m')), $this);?>
								<?php echo $this->_run_modifier($this->_vars['item']['date_end'], 'date_format', 'plugin', 1, '%Y'); ?>

						<?php break; endswitch; ?><br>
						<?php echo $this->_vars['item']['guests']; ?>
 <?php echo l('text_booking_guests', 'listings', '', 'text', array()); ?><br>
						<?php echo tpl_function_block(array('name' => 'currency_format_output','module' => 'start','value' => $this->_vars['item']['price'],'cur_gid' => $this->_vars['item']['gid_currency']), $this);?><br>
						<?php if ($this->_vars['item']['comment']): ?><span title="<?php echo $this->_run_modifier($this->_vars['item']['comment'], 'escape', 'plugin', 1); ?>
"><?php echo $this->_run_modifier($this->_vars['item']['comment'], 'truncate', 'plugin', 1, 70); ?>
</span><br><?php endif; ?>
						<?php if ($this->_vars['item']['answer']): ?><span class="order_answer" title="<?php echo $this->_run_modifier($this->_vars['item']['answer'], 'escape', 'plugin', 1); ?>
"><?php echo l('text_order_answer', 'listings', '', 'text', array()); ?> <?php echo $this->_run_modifier($this->_vars['item']['answer'], 'truncate', 'plugin', 1, 70); ?>
</span><br><?php endif; ?>
					</div>
					<div class="t-2">
						<span><?php echo $this->_vars['item']['date_created']; ?>
</span>
					</div>
				</div>
				<div class="clr"></div>
				<?php echo tpl_function_block(array('name' => 'button_contact','module' => 'mailbox','user_id' => $this->_vars['item']['id_owner'],'user_type' => $this->_vars['item']['listing']['user']['user_type'],'template' => 'button'), $this);?>
				<a href="<?php echo $this->_vars['site_url']; ?>
listings/request_delete/<?php echo $this->_vars['item']['id']; ?>
" data-id="<?php echo $this->_vars['item']['id']; ?>
" data-status="<?php echo $this->_vars['item']['status']; ?>
" class="btn-link request_delete" title="<?php echo l('btn_delete', 'start', '', 'button', array()); ?>" onclick="javascript: if(!confirm('<?php echo l('note_request_delete', 'listings', '', 'js', array()); ?>')) return false;"><ins class="with-icon i-delete"></ins></a><span class="btn-text link-r-margin"><?php echo l('btn_delete', 'start', '', 'text', array()); ?></span>
				<div class="clr"></div>
			</div>
		</div>
		
		<?php endforeach; else: ?>
		<div class="item empty"><?php echo l('no_requests', 'listings', '', 'text', array()); ?></div>
		<?php endif; ?>
		
	</div>
	
	<?php if ($this->_vars['requests']): ?><div id="pages_block_2"><?php echo tpl_function_pagination(array('data' => $this->_vars['page_data'],'type' => 'full'), $this);?></div><?php endif; ?>
