<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-02 15:55:12 KRAT */ ?>

<?php if ($this->_vars['header']):  echo $this->_vars['header'];  endif;  if ($this->_vars['phone_format']):  echo tpl_function_js(array('file' => 'jquery.maskedinput.min.js'), $this);?>
<script><?php echo '
$(function(){
	$(\'#phone\').mask(\'';  echo $this->_vars['phone_format'];  echo '\');
});
'; ?>
</script>
<?php endif;  echo tpl_function_js(array('file' => 'jquery-validation/jquery.validate.js'), $this); echo tpl_function_js(array('file' => 'jquery-validation/additional-methods.js'), $this); echo tpl_function_js(array('module' => contact,'file' => 'contact.js'), $this);?>
<div class="contact_form edit_block">
	<form id="contact_form" name="contact_form" action="" method="POST">
		<div class="r">
			<div class="f"><?php echo l('field_contact_sender', 'contact', '', 'text', array()); ?>&nbsp;*</div>
			<div class="v"><input type="text" AUTOCOMPLETE=OFF name="sender" value="<?php echo $this->_run_modifier($this->_vars['sender'], 'escape', 'plugin', 1); ?>
"></div>
		</div>
		<div class="r">
			<div class="f"><?php echo l('field_contact_phone', 'contact', '', 'text', array()); ?>&nbsp;*</div>
			<div class="v"><input type="text" AUTOCOMPLETE=OFF name="phone" value="<?php echo $this->_run_modifier($this->_vars['phone'], 'escape', 'plugin', 1); ?>
" id="phone"></div>
		</div>
		<div class="r">
			<div class="f"><?php echo l('field_contact_email', 'contact', '', 'text', array()); ?>&nbsp;*</div>
			<div class="v"><input type="text" AUTOCOMPLETE=OFF name="email" value="<?php echo $this->_run_modifier($this->_vars['email'], 'escape', 'plugin', 1); ?>
"></div>
		</div>
		<div class="r">
			<div class="f"><?php echo l('field_contact_message', 'contact', '', 'text', array()); ?>&nbsp;(<?php echo l('field_contact_optional', 'contact', '', '', array()); ?>)</div>
			<div class="v"><textarea name="message" rows="5" cols="23"><?php echo $this->_run_modifier($this->_vars['message'], 'escape', 'plugin', 1); ?>
</textarea></div>
		</div>
		<div class="r">
			<div class="f"><?php echo l('field_contact_captcha', 'contact', '', 'text', array()); ?>&nbsp;*</div>
			<div class="v captcha">
				<?php echo $this->_vars['data']['captcha_image']; ?>

				<span><input type="text" AUTOCOMPLETE=OFF size="8" name="code" value="" maxlength="<?php echo $this->_vars['data']['captcha_word_length']; ?>
"></span>				
			</div>
		</div>
		<div class="r">
			<input type="submit" value="<?php echo l('btn_send', 'start', '', 'button', array()); ?>" />
		</div>
		<input type="hidden" name="user_id" value="<?php echo $this->_vars['data']['user_id']; ?>
" /> 
	</form>
</div>
<script><?php echo '
var ct;
$(function(){
	ct = new Contact({
		siteUrl: \'';  echo $this->_vars['site_url'];  echo '\', 
		emptyUser: \'';  echo l('error_empty_user', 'contact', '', 'text', array());  echo '\',
		emptySender: \'';  echo l('error_empty_sender', 'contact', '', 'text', array());  echo '\',
		emptyPhone: \'';  echo l('error_empty_phone', 'contact', '', 'text', array());  echo '\',
		emptyEmail: \'';  echo l('error_empty_email', 'contact', '', 'text', array());  echo '\',
		emptyMessage: \'';  echo l('error_empty_message', 'contact', '', 'text', array());  echo '\',
		emptyCode: \'';  echo l('error_empty_code', 'contact', '', 'text', array());  echo '\',
		invalidEmail: \'';  echo l('error_invalid_email', 'contact', '', 'text', array());  echo '\',
		invalidCode: \'';  echo l('error_invalid_captcha', 'contact', '', 'text', array());  echo '\',
	});		
});
'; ?>
</script>
