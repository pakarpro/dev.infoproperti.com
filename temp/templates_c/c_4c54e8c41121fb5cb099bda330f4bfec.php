<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-02 15:54:48 KRAT */ ?>

<link href="<?php echo $this->_vars['site_root']; ?>
application/modules/reviews/views/default/css/style-<?php echo $this->_vars['_LANG']['rtl']; ?>
.css" rel="stylesheet" type="text/css" />
<?php echo tpl_function_js(array('module' => reviews,'file' => 'reviews.js'), $this); $this->assign('rate_type', $this->_vars['type']['rate_type']); ?>
<!-- div class="rating-info" -->
	<div class="rating <?php if ($this->_vars['template'] == 'mini' || $this->_vars['template'] == 'mini-extended'): ?>small<?php else: ?>large<?php endif; ?>" id="rating-<?php echo $this->_vars['rand']; ?>
">
		<?php if ($this->_vars['template'] == 'extended'): ?><span><?php echo $this->_vars['type']['values'][$this->_vars['rate_type']]['main']['header']; ?>
</span><?php endif; ?>
		<ul class="<?php echo $this->_vars['rate_type']; ?>
">
			<?php $this->assign('count', $this->_run_modifier($this->_vars['type']['values'][$this->_vars['rate_type']]['main']['votes'], 'count', 'PHP', 0)); ?>
			<?php if (is_array($this->_vars['type']['values'][$this->_vars['rate_type']]['main']['votes']) and count((array)$this->_vars['type']['values'][$this->_vars['rate_type']]['main']['votes'])): foreach ((array)$this->_vars['type']['values'][$this->_vars['rate_type']]['main']['votes'] as $this->_vars['vote_id'] => $this->_vars['vote_label']): ?>
			<li data-id="<?php echo $this->_vars['vote_id']; ?>
" data-label="<?php echo $this->_vars['vote_label']; ?>
" <?php if ($this->_vars['read_only'] && $this->_vars['count'] == 2 && $this->_vars['vote_id'] == 1): ?>class="hide"<?php endif; ?>></li>
			<?php endforeach; endif; ?>
		</ul>
		<label class="hide"></label>
		<input type="hidden" name="rating_data[main]" value="<?php echo $this->_vars['rating_data']['main']; ?>
" />
		<div class="clr"></div>
	</div>
	<?php if ($this->_vars['template'] == 'extended' || $this->_vars['template'] == 'mini-extended'): ?>
	<div class="rating_extended <?php if ($this->_vars['template'] == 'mini-extended'): ?>mini<?php endif; ?>" id="extended-rating-<?php echo $this->_vars['rand']; ?>
">
		<?php if (is_array($this->_vars['type']['values'][$this->_vars['rate_type']]) and count((array)$this->_vars['type']['values'][$this->_vars['rate_type']])): foreach ((array)$this->_vars['type']['values'][$this->_vars['rate_type']] as $this->_vars['rate_key'] => $this->_vars['rate_item']): ?>
			<?php if ($this->_vars['rate_key'] != 'main'): ?>
			<span><?php echo $this->_vars['type']['values'][$this->_vars['rate_type']][$this->_vars['rate_key']]['header']; ?>
:</span> 
			<div class="rating small">
				<ul class="<?php echo $this->_vars['rate_type']; ?>
">
					<?php $this->assign('count', $this->_run_modifier($this->_vars['type']['values'][$this->_vars['rate_type']]['main']['votes'], 'count', 'PHP', 0)); ?>
					<?php if (is_array($this->_vars['type']['values'][$this->_vars['rate_type']][$this->_vars['rate_key']]['votes']) and count((array)$this->_vars['type']['values'][$this->_vars['rate_type']][$this->_vars['rate_key']]['votes'])): foreach ((array)$this->_vars['type']['values'][$this->_vars['rate_type']][$this->_vars['rate_key']]['votes'] as $this->_vars['vote_id'] => $this->_vars['vote_label']): ?>
					<li data-id="<?php echo $this->_vars['vote_id']; ?>
" data-label="<?php echo $this->_vars['vote_label']; ?>
" <?php if ($this->_vars['read_only'] && $this->_vars['count'] == 2 && $this->_vars['vote_id'] == 1): ?>class="hide"<?php endif; ?>></li>
					<?php endforeach; endif; ?>
				</ul>
				<label></label>
				<input type="hidden" name="rating_data[<?php echo $this->_vars['rate_key']; ?>
]" value="<?php echo $this->_vars['rating_data'][$this->_vars['rate_key']]; ?>
" />
				<div class="clr"></div>
			</div>
			<br />				
			<?php endif; ?>
		<?php endforeach; endif; ?>
	</div>
	<?php endif; ?>
<!-- /div -->
<script type="text/javascript">
<?php echo '
$(function(){
	new Reviews({
		siteUrl: \'';  echo $this->_vars['site_url'];  echo '\', 
		ratingId: \''; ?>
rating-<?php echo $this->_vars['rand'];  echo '\',
		type: \'';  echo $this->_vars['symbolType'];  echo '\',
		';  if ($this->_vars['read_only']): ?>readOnly: true,<?php endif;  echo '
		';  if ($this->_vars['show_label']): ?>showLabel: true,<?php endif;  echo '
		';  if ($this->_vars['_LANG']['rtl'] == 'rtl'): ?>isRTL: true,<?php endif;  echo '
	});	
});
'; ?>

</script>
