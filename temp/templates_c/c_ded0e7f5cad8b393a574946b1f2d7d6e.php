<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.js.php'); $this->register_function("js", "tpl_function_js");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2014-05-08 11:24:49 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array('load_type' => 'ui'));
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<?php if ($this->_vars['poll_data']['language']): ?>
    <?php $this->assign('cur_lang', $this->_vars['poll_data']['language']); ?>
<?php endif; ?>
<?php echo tpl_function_js(array('file' => 'colorsets/jscolor/jscolor.js'), $this);?>
<?php echo '
    <script type="text/javascript">
		var polls;
        $(function(){
			polls = new adminPollsAnswers({
				siteUrl: \'';  echo $this->_vars['site_url'];  echo '\',
				counter: \'';  echo $this->_vars['answers_count'];  echo '\',
				show_results: \'';  echo $this->_vars['poll_data']['show_results'];  echo '\'
			});
		});
	</script>
'; ?>

<?php if ($this->_vars['responds_count']): ?>
    <div class="filter-form"><?php echo l('error_no_editing', 'polls', '', 'text', array()); ?></div>
<?php endif; ?>
<div class="lef">
	<form method="post" action="" name="save_form" enctype="multipart/form-data">
        <?php $this->assign('language', $this->_vars['poll_data']['language']); ?>
        <div class="edit-form n100">
			<div class="row header"><?php echo l('admin_header_answers_change', 'polls', '', 'text', array()); ?></div>
			<div class="row">
				<div class="h"><?php echo l('field_question', 'polls', '', 'text', array()); ?>: </div>
				<div class="v">
					<?php if ($this->_vars['language'] && $this->_vars['poll_data']['question'][$this->_vars['language']]): ?>
						<?php echo $this->_vars['poll_data']['question'][$this->_vars['language']]; ?>

					<?php elseif ($this->_vars['poll_data']['question'][$this->_vars['cur_lang']]): ?>
						<?php echo $this->_vars['poll_data']['question'][$this->_vars['cur_lang']]; ?>

					<?php else: ?>
						<?php echo $this->_vars['poll_data']['question']['default']; ?>

					<?php endif; ?>
				</div>
			</div>
			<?php if (is_array($this->_vars['poll_data']['answers_colors']) and count((array)$this->_vars['poll_data']['answers_colors'])): foreach ((array)$this->_vars['poll_data']['answers_colors'] as $this->_vars['i'] => $this->_vars['answer']): ?>
				<div id="row_answer_<?php echo $this->_vars['i']; ?>
" class="row row-zebra">
					<div class="h"><?php echo l('field_answer', 'polls', '', 'text', array()); ?>:&nbsp;* </div>
					<div class="v">
						<?php if (! $this->_vars['responds_count']): ?>
							<a id="delete_<?php echo $this->_vars['i']; ?>
" class="delete_answer fright" href="javascript:void(0);">
								<img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-delete.png" width="16" height="16" border="0" alt="<?php echo l('link_delete_answer', 'polls', '', 'text', array()); ?>" title="<?php echo l('link_delete_answer', 'polls', '', 'text', array()); ?>" />
							</a>
						<?php endif; ?>
						<div id="languages_container">
							<?php if (is_array($this->_vars['languages']) and count((array)$this->_vars['languages'])): foreach ((array)$this->_vars['languages'] as $this->_vars['lang_id'] => $this->_vars['item']): ?>
								<div id="answer_<?php echo $this->_vars['i']; ?>
_<?php echo $this->_vars['lang_id']; ?>
" class="question p-top2 <?php if ($this->_vars['poll_data']['language'] > 0 && $this->_vars['poll_data']['language'] != $this->_vars['lang_id']): ?>hide<?php endif; ?>">
									<?php $this->assign('language_item', $this->_vars['i'].'_'.$this->_vars['lang_id']); ?>
									<?php $this->assign('value', $this->_vars['poll_data']['answers_languages'][$this->_vars['language_item']]); ?>
									<?php if (! $this->_vars['value']): ?>
										<?php $this->assign('language_item', $this->_vars['i'].'_default'); ?>
										<?php $this->assign('value', $this->_vars['poll_data']['answers_languages'][$this->_vars['language_item']]); ?>
									<?php endif; ?>
									<input <?php if ($this->_vars['responds_count']): ?>disabled="disabled"<?php endif; ?>
											<?php if ($this->_vars['lang_id'] == $this->_vars['cur_lang']): ?>id="answer_<?php echo $this->_vars['i']; ?>
"<?php else: ?>id="answer_<?php echo $this->_vars['i']; ?>
_input_<?php echo $this->_vars['lang_id']; ?>
"<?php endif; ?>
											class="answer_<?php echo $this->_vars['i']; ?>
_input answer_input <?php if ($this->_vars['lang_id'] == $this->_vars['cur_lang']): ?>default_answer<?php endif; ?>"
											type="text" value="<?php echo $this->_vars['value']; ?>
" name="answer[<?php echo $this->_vars['i']; ?>
_<?php echo $this->_vars['lang_id']; ?>
]" />
									&nbsp;|&nbsp;<?php echo $this->_vars['item']['name']; ?>

								</div>
							<?php endforeach; endif; ?>
						</div>
						<div class="answer_side">
							# <input id="color_answer_<?php echo $this->_vars['i']; ?>
" class="color_input color-pick" type="text" value="<?php echo $this->_vars['poll_data']['answers_colors'][$this->_vars['i']]; ?>
" name="answers_colors[<?php echo $this->_vars['i']; ?>
]">
						</div>
					</div>
				</div>
			<?php endforeach; endif; ?>
			<?php if (! $this->_vars['responds_count']): ?>
				<a href="javascript:void(0);" id="add_answer"><?php echo l('add_answer', 'polls', '', 'text', array()); ?></a>
			<?php endif; ?>
		</div>

		<div class="btn">
			<div class="l">
				<input class="answer_input" type="submit" name="btn_save" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>">
			</div>
		</div>
		<a class="cancel" href="<?php echo $this->_vars['site_url']; ?>
admin/polls/"><?php echo l('btn_cancel', 'start', '', 'text', array()); ?></a>
	</form>
	
	<div id="answer_tpl" class="hide">
		<div id="row_answer_-id-" class="row row-zebra">
			<div class="h"><?php echo l('field_answer', 'polls', '', 'text', array()); ?>: </div>
			<div class="v">
				<?php if (! $this->_vars['responds_count']): ?>
					<a id="delete_-id-" class="delete_answer fright" href="javascript:void(0);">
						<img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-delete.png" width="16" height="16" border="0" alt="<?php echo l('link_delete_answer', 'polls', '', 'text', array()); ?>" title="<?php echo l('link_delete_answer', 'polls', '', 'text', array()); ?>" />
					</a>
				<?php endif; ?>
				<div id="languages_container">
					<?php if (is_array($this->_vars['languages']) and count((array)$this->_vars['languages'])): foreach ((array)$this->_vars['languages'] as $this->_vars['lang_id'] => $this->_vars['item']): ?>
						
						<div id="answer_-id-_<?php echo $this->_vars['lang_id']; ?>
" class="question p-top2 <?php if ($this->_vars['poll_data']['language'] > 0 && $this->_vars['poll_data']['language'] != $this->_vars['lang_id']): ?>hide<?php endif; ?>">
							<?php $this->assign('language_item', '-id-_'.$this->_vars['lang_id']); ?>
							<input <?php if ($this->_vars['responds_count']): ?>disabled="disabled"<?php endif; ?>
									<?php if ($this->_vars['lang_id'] == $this->_vars['cur_lang']): ?>id="answer_-id-"<?php else: ?>id="answer_-id-_input_<?php echo $this->_vars['lang_id']; ?>
"<?php endif; ?>
									class="answer_-id-_input answer_input <?php if ($this->_vars['lang_id'] == $this->_vars['cur_lang']): ?>default_answer<?php endif; ?>"
									type="text" value="<?php echo $this->_vars['poll_data']['answers_languages'][$this->_vars['language_item']]; ?>
" name="answer[-id-_<?php echo $this->_vars['lang_id']; ?>
]" />
							&nbsp;|&nbsp;<?php echo $this->_vars['item']['name']; ?>

						</div>
					<?php endforeach; endif; ?>
				</div>
				<div class="answer_side">
					# <input id="color_answer_-id-" class="color_input color-pick" type="text" value="<?php echo $this->_vars['poll_data']['answers_colors'].id; ?>
" name="answers_colors[-id-]">
				</div>
			</div>
		</div>
	</div>
	
</div>

<div class="ref">
	<div class="preview edit-form n150">
		<div class="poll">
			<div class="row header">
				<?php echo l('admin_header_preview_poll', 'polls', '', 'text', array()); ?></div>
			<div id="question" >
				<div class="row">
					<div class="h">
						<?php if ($this->_vars['language']):  echo $this->_vars['poll_data']['question'][$this->_vars['language']];  else:  echo $this->_vars['poll_data']['question'][$this->_vars['cur_lang']];  endif; ?>
					</div>
				</div>
				<?php if (is_array($this->_vars['poll_data']['answers_colors']) and count((array)$this->_vars['poll_data']['answers_colors'])): foreach ((array)$this->_vars['poll_data']['answers_colors'] as $this->_vars['i'] => $this->_vars['answer']): ?>
					<?php $this->assign('language_item', $this->_vars['i'].'_'.$this->_vars['lang_id']); ?>
					<div class="row" id="preview_answer_<?php echo $this->_vars['i']; ?>
">
						<div class="h">
							<input id="r_<?php echo $this->_vars['i']; ?>
" class="answer_<?php echo $this->_vars['i']; ?>
" type="<?php if ($this->_vars['poll_data']['answer_type'] == 1): ?>checkbox<?php else: ?>radio<?php endif; ?>" value="<?php echo $this->_vars['i']; ?>
" name="answer">
							<label for="r_<?php echo $this->_vars['i']; ?>
"><?php if ($this->_vars['answers_languages'][$this->_vars['language_item']]):  echo $this->_vars['answers_languages'][$this->_vars['language_item']];  endif; ?></label>
						</div>
					</div>
				<?php endforeach; endif; ?>
				
				<div id="preview_tpl" class="hide">
					<div class="row" id="preview_answer_-id-">
						<div class="h">
							<input id="r_-id-" class="answer_-id-" type="<?php if ($this->_vars['poll_data']['answer_type'] == 1): ?>checkbox<?php else: ?>radio<?php endif; ?>" value="-id-" name="answer">
							<label for="r_-id-"><?php if ($this->_vars['answers_languages'][$this->_vars['language_item']]):  echo $this->_vars['answers_languages'][$this->_vars['language_item']];  endif; ?></label>
						</div>
					</div>
				</div>
				
			</div>
		</div>
		<?php if ($this->_vars['poll_data']['show_results']): ?>
			<div class="results">
				<div class="row header"><?php echo l('admin_header_preview_poll_results', 'polls', '', 'text', array()); ?></div>
				<div id="results">
					<p><?php if ($this->_vars['language']):  echo $this->_vars['poll_data']['question'][$this->_vars['language']];  else:  echo $this->_vars['poll_data']['question'][$this->_vars['cur_lang']];  endif; ?></p>
					<div id="results_answers"></div>
				</div>
			</div>
		<?php endif; ?>
	</div>
</div>

<?php echo '
	<script type="text/javascript">
		$(function(){
			polls.properties.counter = \'';  echo $this->_vars['answers_count'];  echo '\';
			polls.bind_events();
		});
	</script>
'; ?>


<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
