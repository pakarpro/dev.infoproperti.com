<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.escape.php'); $this->register_modifier("escape", "tpl_modifier_escape");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2014-06-12 14:11:00 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>


<form method="post" action="<?php echo $this->_vars['data']['action']; ?>
" name="save_form">
<div class="edit-form n150">
	<div class="row header"><?php echo l('admin_header_template_edit', 'upload_gallery', '', 'text', array()); ?></div>
	<div class="row">
		<div class="h"><?php echo l('field_gid', 'upload_gallery', '', 'text', array()); ?>: </div>
		<div class="v"><?php if ($this->_vars['allow_add']): ?><input type="text" value="<?php echo $this->_vars['data']['gid']; ?>
" name="data[gid]"><?php else:  echo $this->_vars['data']['gid'];  endif; ?></div>
	</div>
	<div class="row zebra">
		<div class="h"><?php echo l('field_name', 'upload_gallery', '', 'text', array()); ?>:&nbsp;* </div>
		<div class="v"><input type="text" value="<?php echo $this->_run_modifier($this->_vars['data']['name'], 'escape', 'plugin', 1); ?>
" name="data[name]"></div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_upload_config', 'upload_gallery', '', 'text', array()); ?>:&nbsp;* </div>
		<div class="v">
			<select name="data[gid_upload_config]">
			<?php if (is_array($this->_vars['upload_config']) and count((array)$this->_vars['upload_config'])): foreach ((array)$this->_vars['upload_config'] as $this->_vars['item']): ?><option value="<?php echo $this->_vars['item']['gid']; ?>
" <?php if ($this->_vars['data']['gid_upload_config'] == $this->_vars['item']['gid']): ?>selected<?php endif; ?>><?php echo $this->_vars['item']['name']; ?>
</option><?php endforeach; endif; ?>
			</select>
		</div>
	</div>
	<div class="row zebra">
		<div class="h"><?php echo l('field_max_items', 'upload_gallery', '', 'text', array()); ?>: </div>
		<div class="v"><input type="text" value="<?php echo $this->_vars['data']['max_items_count']; ?>
" name="data[max_items_count]" class="short"> <i><?php echo l('field_max_items_comment', 'upload_gallery', '', 'text', array()); ?></i></div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_use_moderation', 'upload_gallery', '', 'text', array()); ?>: </div>
		<div class="v"><input type="checkbox" <?php if ($this->_vars['data']['use_moderation']): ?>checked<?php endif; ?> name="data[use_moderation]" value="1"></div>
	</div>

	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div></div>
	<a class="cancel" href="<?php echo $this->_vars['site_url']; ?>
admin/upload_gallery/index"><?php echo l('btn_cancel', 'start', '', 'text', array()); ?></a>
</div>
</form>

<div class="clr"></div>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
