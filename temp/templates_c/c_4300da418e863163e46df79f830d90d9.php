<?php require_once('/opt/ip/system/libraries/template_lite/plugins/function.ld_option.php'); $this->register_function("ld_option", "tpl_function_ld_option");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-02 15:54:48 KRAT */ ?>

<div class="user_listings clearfix">
<!-- user listing starts -->
	<h2><?php echo l('header_listings', 'listings', '', 'text', array()); ?></h2>
	<?php if (is_array($this->_vars['listings']) and count((array)$this->_vars['listings'])): foreach ((array)$this->_vars['listings'] as $this->_vars['item']): ?>
	<div class="listing-block <?php if ($this->_vars['item']['is_highlight']): ?>highlight<?php endif; ?>">
		<div id="item-block-<?php echo $this->_vars['item']['id']; ?>
" class="item listing">
			<h3 class="listing-heading"><a href="<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'view','data' => $this->_vars['item']), $this);?>"><?php echo $this->_run_modifier($this->_vars['item']['output_name'], 'truncate', 'plugin', 1, 40); ?>
</a></h3>
			<div class="image">
				<a href="<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'view','data' => $this->_vars['item']), $this);?>">
					<img src="<?php echo $this->_vars['item']['media']['photo']['thumbs']['big']; ?>
" alt="<?php echo $this->_run_modifier($this->_vars['item']['output_name'], 'truncate', 'plugin', 1, 40); ?>
" title="<?php echo $this->_run_modifier($this->_vars['item']['output_name'], 'escape', 'plugin', 1); ?>
">
					<?php if ($this->_vars['item']['photo_count'] || $this->_vars['item']['is_vtour']): ?>
					<div class="photo-info">
						<div class="panel">
							<?php if ($this->_vars['item']['photo_count']): ?><span class="btn-link"><ins class="with-icon-small w i-photo"></ins> <?php echo $this->_vars['item']['photo_count']; ?>
</span><?php endif; ?>
							<?php if ($this->_vars['item']['is_vtour']): ?><span class="btn-link"><ins class="with-icon-small w i-vtour"></ins> 360&deg;</span><?php endif; ?>
						</div>
						<div class="background"></div>
					</div>
					<?php endif; ?>
				</a>
			</div>
			<div class="listing-content">
				<div class="body">
					<?php 
$this->assign('no_info_str', l('no_information', 'start', '', 'text', array()));
 ?>
					<h3 class="price-highlight"><?php echo tpl_function_block(array('name' => listing_price_block,'module' => 'listings','data' => $this->_vars['item']), $this);?></h3>
					<div class="t-1">
                        <?php echo $this->_vars['item']['property_type_str']; ?>
 <?php echo $this->_vars['item']['operation_type_str']; ?>
<br />
						<?php echo l('field_square', 'listings', '', 'text', array()); ?> : <?php echo $this->_run_modifier($this->_vars['item']['square_output'], 'truncate', 'plugin', 1, 30); ?>

                        <ul class="icon-stats">
               			<?php if (is_array($this->_vars['residential']) and count((array)$this->_vars['residential'])): foreach ((array)$this->_vars['residential'] as $this->_vars['residen']): ?>

                            <?php if ($this->_vars['item']['id'] == $this->_vars['residen']['id_listing']): ?>
                            	
                                <?php if ($this->_vars['residen']['fe_bd_rooms_1'] != '0'): ?>	 
                                	<li class="listing-icon bedroom"> : <?php echo $this->_vars['residen']['fe_bd_rooms_1']; ?>
</li>
                                <?php endif; ?>
                                
                                <?php if ($this->_vars['residen']['fe_bth_rooms_1'] != '0'): ?>
                                	<li class="listing-icon bathroom"> : <?php echo $this->_vars['residen']['fe_bth_rooms_1']; ?>
</li>
                                <?php endif; ?>
                                
                                <?php if ($this->_vars['residen']['fe_garages_1'] != '0'): ?>
                                	<li class="listing-icon garages"> : <?php echo $this->_vars['residen']['fe_garages_1']; ?>
</li>
                                <?php endif; ?>
                            <?php endif; ?>
      					<?php endforeach; endif; ?>
                        
						
                        <?php if (is_array($this->_vars['commercial']) and count((array)$this->_vars['commercial'])): foreach ((array)$this->_vars['commercial'] as $this->_vars['commer']): ?>
                            <?php if ($this->_vars['item']['id'] == $this->_vars['commer']['id_listing']): ?>
                                <?php if ($this->_vars['commer']['fe_year_2'] != '0'): ?>	 
                                	<li class="listing-icon year">Year : <?php echo $this->_vars['commer']['fe_year_2']; ?>
</li>
                                <?php endif; ?>
                                
                                <?php if ($this->_vars['commer']['fe_foundation_2'] != '0'): ?>
                                	<li class="listing-icon foundation">Foundation : <?php echo $this->_vars['commer']['fe_foundation_2']; ?>
</li>
                                <?php endif; ?>
                            <?php endif; ?>
      					<?php endforeach; endif; ?>
                        </ul> 
                        
						<?php if ($this->_run_modifier($this->_vars['item']['date_open'], 'strtotime', 'PHP', 1) >= $this->_vars['tstamp']):  echo l('text_date_open_status', 'listings', '', 'text', array());  endif; ?>
						<?php /*** Enable these lines below to show the open house's date and time
						<?php if ($this->_run_modifier($this->_vars['item']['date_open'], 'strtotime', 'PHP', 1) > 0): ?>
							<?php echo $this->_run_modifier($this->_vars['item']['date_open'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']); ?>
 
							<?php if ($this->_vars['item']['date_open_begin']):  echo tpl_function_ld_option(array('i' => 'dayhour-names','gid' => 'start','option' => $this->_vars['item']['date_open_begin']), $this); endif; ?>
							<?php if ($this->_vars['item']['date_open_end']):  if ($this->_vars['item']['date_open_begin']): ?> - <?php endif;  echo tpl_function_ld_option(array('i' => 'dayhour-names','gid' => 'start','option' => $this->_vars['item']['date_open_end']), $this); endif; ?>
						<?php endif; ?>
						* */?>
					</div>
					<div class="t-2">
                    	<?php /* lines below for displaying modified listing by date */ ?>
						<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'date_modified')); tpl_block_capture(array('assign' => 'date_modified'), null, $this); ob_start();  $this->_tag_stack[] = array('tpl_block_strip', array()); tpl_block_strip(array(), null, $this); ob_start(); ?>
						<?php echo $this->_run_modifier($this->_vars['item']['date_modified'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']); ?>
 <?php /* replaced the $listing.date_modified with $item.date_modified */ ?>
						<?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_strip($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack);  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?> 
						<span><?php echo l('field_date_modified', 'listings', '', 'text', array()); ?>: <?php if ($this->_vars['date_modified']):  echo $this->_vars['date_modified'];  else:  echo l('inactive_listing', 'listings', '', 'text', array());  endif; ?></span><br>
						<span><?php echo l('field_views', 'listings', '', 'text', array()); ?>: <?php echo $this->_vars['item']['views']; ?>
</span>
                    </div>
					
				</div>
			</div>
			<div class="clr"></div>
			<?php if ($this->_vars['item']['headline']): ?><p class="headline" title="<?php echo $this->_vars['item']['headline']; ?>
"><?php echo $this->_run_modifier($this->_vars['item']['headline'], 'truncate', 'plugin', 1, 50); ?>
</p><?php endif; ?>
					<div class="a-link clearfix">
						<?php echo tpl_function_block(array('name' => 'save_listing_block','module' => 'listings','listing' => $this->_vars['item'],'template' => 'link','separator' => ''), $this);?>
						<?php echo tpl_function_block(array('name' => 'listings_booking_block','module' => 'listings','listing' => $this->_vars['item'],'template' => 'link','show_link' => 1,'show_price' => 1,'separator' => '','no_save' => 1), $this);?>
						<a href="<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'view','data' => $this->_vars['item']), $this);?>"><?php echo l('link_details', 'listings', '', 'text', array()); ?></a>
						<span class="prioritas">Prioritas</span>
					</div>
		</div>
	</div>
	<?php endforeach; endif; ?>
	
</div>
<?php if ($this->_vars['listings_count']): ?><div class="more-listings"><a href="<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'user','id_user' => $this->_vars['user_id'],'user' => $this->_vars['user_name']), $this);?>"><?php echo l('link_all_user_listings', 'listings', '', 'text', array()); ?> (<?php echo $this->_vars['listings_count']; ?>
)</a></div><?php endif; ?>
