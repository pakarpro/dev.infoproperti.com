<?php require_once('/opt/ip/system/libraries/template_lite/plugins/function.js.php'); $this->register_function("js", "tpl_function_js");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-01-04 19:04:48 KRAT */ ?>

<?php echo tpl_function_js(array('file' => 'admin_lang_inline_editor.js','module' => 'start'), $this);?>
<script type="text/javascript"><?php echo '
var inlineEditor;
$(function(){
	inlineEditor = new langInlineEditor({
		siteUrl: \'';  echo $this->_vars['site_url'];  echo '\',
		';  if ($this->_vars['textarea']): ?>textarea: true,<?php endif;  echo '
	});
});
'; ?>
</script>
