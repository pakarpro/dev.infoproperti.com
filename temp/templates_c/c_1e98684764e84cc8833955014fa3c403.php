<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2014-09-16 11:40:44 KRAT */ ?>

<div class="content-block">
	<h2><?php echo l('show_logo_services', 'users_services', '', 'text', array()); ?></h2>
	<p class="header-comment">
	<?php if ($this->_vars['is_show_logo']['is_show_logo']): ?>
		<?php echo l('your_show_logo_period_till', 'users_services', '', 'text', array()); ?>: <?php echo $this->_run_modifier($this->_vars['is_show_logo']['show_logo_end_date'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']); ?>
<br />
		<?php echo l('add_more_show_logo_period', 'users_services', '', 'text', array()); ?><br />
	<?php else: ?>
		<?php echo l('create_show_logo_period', 'users_services', '', 'text', array()); ?>
	<?php endif; ?>
	</p>
	<div class="r">
		<div class="b"><input type="button" class='btn' value="<?php echo l('btn_apply', 'start', '', 'button', array()); ?>" onclick="document.location.href='<?php echo $this->_vars['site_url']; ?>
users_services/apply_service/<?php echo $this->_vars['user_id']; ?>
/company_show_logo_services'"></div>
	</div>
</div>
