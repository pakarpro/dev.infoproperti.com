<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-02 15:51:01 WIB */ ?>

<?php if ($this->_vars['news']): ?>
<div id="news-n-fb" class="clearfix">
	<div id="news-section">
		<h2 class="head-title"><?php echo l('header_latest_added_news', 'news', '', 'text', array()); ?></h2>
        <?php if ($this->_vars['news_privilege']): ?>
        	<a href="<?php echo $this->_vars['base_url']; ?>
news-add_news_edit">Add news</a>
        <?php endif; ?>        
		<!--<div class="latest_added_news_block">-->
				<div class="latestnews clearfix">
					<?php if (is_array($this->_vars['news_headline']) and count((array)$this->_vars['news_headline'])): foreach ((array)$this->_vars['news_headline'] as $this->_vars['key'] => $this->_vars['item']): ?>
					<div class="news-entry clearfix">
                    <!-- old twin box style is used when no external feeds or news source exists-->
                    
                    <?php if (! $this->_vars['external_news']): ?>
                        <?php if ($this->_vars['item']['img']): ?>
                            <div class="image"><img src="<?php echo $this->_vars['item']['media']['img']['thumbs']['headline']; ?>
" style="margin-right:15px;" align="left" /></div>
                            <?php elseif ($this->_vars['item']['img'] == NULL || $this->_vars['item']['img'] == ''): ?>
                            <div class="image"><img src="<?php echo $this->_vars['base_url']; ?>
uploads/news-logo/default/logo-headline<?php echo $this->_vars['item']['image_randomizer']; ?>
.jpg" style="margin-right:15px;" align="left" /></div>
                        <?php endif; ?>
						<div class="body">
							<b><?php echo $this->_run_modifier($this->_vars['item']['name'], 'truncate', 'plugin', 1, 100); ?>
</b>
							<?php echo $this->_run_modifier($this->_vars['item']['annotation'], 'truncate', 'plugin', 1, 180); ?>
<br />
							<a href="<?php echo tpl_function_seolink(array('module' => 'news','method' => 'view','data' => $this->_vars['item']), $this);?>"><?php echo l('link_view_more', 'news', '', 'text', array()); ?></a>
                        </div>
              		<?php else: ?>	
                    <!-- else for when there is an external news items -->
                    	<div style="width:340px; float:left; margin:4px;">
                        	<?php if ($this->_vars['item']['img']): ?>
                            <div class="image"><img src="<?php echo $this->_vars['item']['media']['img']['thumbs']['headline']; ?>
" style=" margin:0 auto;" align="center" /></div>
                            <?php elseif ($this->_vars['item']['img'] == NULL || $this->_vars['item']['img'] == ''): ?>
                            <div class="image"><img src="<?php echo $this->_vars['base_url']; ?>
uploads/news-logo/default/logo-headline<?php echo $this->_vars['item']['image_randomizer']; ?>
.jpg" style=" margin:0 auto;" align="center" /></div>
                            <?php endif; ?>
                            <div style="margin-top:10px;">
                                <b><?php echo $this->_run_modifier($this->_vars['item']['name'], 'truncate', 'plugin', 1, 100); ?>
</b><br/>
                                <?php echo $this->_run_modifier($this->_vars['item']['annotation'], 'truncate', 'plugin', 1, 180); ?>
<br />
                                <a href="<?php echo tpl_function_seolink(array('module' => 'news','method' => 'view','data' => $this->_vars['item']), $this);?>"><?php echo l('link_view_more', 'news', '', 'text', array()); ?></a>
                            </div>
                        </div>
                        
                    <?php endif; ?>
                    <!-- ORIGINAL TWIN BOX STYLE FOR HEADLINE
                    	<div style="width:340px; float:left; margin:4px;">
                        	<?php if ($this->_vars['item']['img']): ?>
                            <div class="image"><img src="<?php echo $this->_vars['item']['media']['img']['thumbs']['headline']; ?>
" style=" margin:0 auto;" align="center" /></div>
                            <?php elseif ($this->_vars['item']['img'] == NULL || $this->_vars['item']['img'] == ''): ?>
                            <div class="image"><img src="<?php echo $this->_vars['base_url']; ?>
uploads/news-logo/default/logo-headline<?php echo $this->_vars['item']['image_randomizer']; ?>
.jpg" style=" margin:0 auto;" align="center" /></div>
                            <?php endif; ?>
                            <div style="margin-top:10px;">
                                <b><?php echo $this->_run_modifier($this->_vars['item']['name'], 'truncate', 'plugin', 1, 100); ?>
</b><br/>
                                <?php echo $this->_run_modifier($this->_vars['item']['annotation'], 'truncate', 'plugin', 1, 180); ?>
<br />
                                <a href="<?php echo tpl_function_seolink(array('module' => 'news','method' => 'view','data' => $this->_vars['item']), $this);?>"><?php echo l('link_view_more', 'news', '', 'text', array()); ?></a>
                            </div>
                        </div>
                    -->
					<?php endforeach; endif; ?>
                    <!-- external news -->
					<?php if (is_array($this->_vars['external_news']) and count((array)$this->_vars['external_news'])): foreach ((array)$this->_vars['external_news'] as $this->_vars['key'] => $this->_vars['item']): ?>
                    	<div style="width:340px; float:left; margin:4px;">
                        	<?php if ($this->_vars['item']['img']): ?>
                            <div class="image"><img src="<?php echo $this->_vars['item']['media']['img']['thumbs']['headline']; ?>
" style=" margin:0 auto;" align="center" /></div>
                            <?php elseif ($this->_vars['item']['img'] == NULL || $this->_vars['item']['img'] == ''): ?>
                            <div class="image"><img src="<?php echo $this->_vars['base_url']; ?>
uploads/news-logo/default/logo-headline2.jpg" style=" margin:0 auto;" align="center" /></div>
                            <?php endif; ?>
                            <div style="margin-top:10px;">
                                <b><?php echo $this->_run_modifier($this->_vars['item']['name'], 'truncate', 'plugin', 1, 100); ?>
</b><br/>
                                <?php echo $this->_run_modifier($this->_vars['item']['annotation'], 'truncate', 'plugin', 1, 180); ?>
<br />
                                <a href="<?php echo tpl_function_seolink(array('module' => 'news','method' => 'view','data' => $this->_vars['item']), $this);?>"><?php echo l('link_view_more', 'news', '', 'text', array()); ?></a>
                            </div>
                        </div>                    
					<?php endforeach; endif; ?> 
					</div>
                                       
					<?php if (is_array($this->_vars['news']) and count((array)$this->_vars['news'])): foreach ((array)$this->_vars['news'] as $this->_vars['key'] => $this->_vars['item']): ?>
					<div class="news-entry clearfix">
						<?php if ($this->_vars['item']['img']): ?>
                        <div class="image">
                        	<a href="<?php echo tpl_function_seolink(array('module' => 'news','method' => 'view','data' => $this->_vars['item']), $this);?>">
                            <!-- mod -->
                            	<img src="<?php echo $this->_vars['item']['media']['img']['thumbs']['small']; ?>
" align="left" />
                            <!-- end of mod -->
                            </a>
                        </div>
                        <?php else: ?>
                        <div class="image">
                        	<a href="<?php echo tpl_function_seolink(array('module' => 'news','method' => 'view','data' => $this->_vars['item']), $this);?>">
                            <!-- mod -->
                            	<img src="<?php echo $this->_vars['base_url']; ?>
uploads/news-logo/default/logo-small<?php echo $this->_vars['item']['image_randomizer']; ?>
.jpg" align="left" />
                            <!-- end of mod -->
                            </a>
                        </div>                        
                        <?php endif; ?>
                        <!--
                        <?php if ($this->_vars['item']['img']): ?>
						<div class="body">
                        <?php endif; ?>
                        -->
						<div class="body">                        
							<b><?php echo $this->_run_modifier($this->_vars['item']['name'], 'truncate', 'plugin', 1, 100); ?>
</b>
							<?php echo $this->_run_modifier($this->_vars['item']['annotation'], 'truncate', 'plugin', 1, 180); ?>
<br />
							<a href="<?php echo tpl_function_seolink(array('module' => 'news','method' => 'view','data' => $this->_vars['item']), $this);?>"><?php echo l('link_view_more', 'news', '', 'text', array()); ?></a>
						</div>
					</div>
				<?php endforeach; endif; ?>
				</div>
		<!-- </div>-->
	</div>
	<!-- banner mod -->
	<div style="float:left; margin-left:50px; padding-top:76px;">
	<!--<?php echo tpl_function_helper(array('func_name' => show_banner_place,'module' => banners,'func_param' => 'right-news-banner'), $this);?>-->
    <?php echo tpl_function_helper(array('func_name' => show_banner_place,'module' => banners,'func_param' => 'right-tall-news-bann'), $this);?>
    </div>
    <!-- end of banner mod-->
	<!--<p><a href="<?php echo tpl_function_seolink(array('module' => 'news','method' => 'index'), $this);?>"><?php echo l('link_read_more', 'news', '', 'text', array()); ?></a></p>-->
</div>

<?php endif; ?>