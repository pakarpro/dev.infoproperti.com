<?php require_once('/opt/ip/system/libraries/template_lite/plugins/modifier.date_format.php'); $this->register_modifier("date_format", "tpl_modifier_date_format");  require_once('/opt/ip/system/libraries/template_lite/plugins/modifier.truncate.php'); $this->register_modifier("truncate", "tpl_modifier_truncate");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.counter.php'); $this->register_function("counter", "tpl_function_counter");  require_once('/opt/ip/system/libraries/template_lite/plugins/modifier.lower.php'); $this->register_modifier("lower", "tpl_modifier_lower");  require_once('/opt/ip/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.helper.php'); $this->register_function("helper", "tpl_function_helper");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-09-11 10:05:35 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
  echo tpl_function_helper(array('func_name' => get_admin_level1_menu,'helper_name' => menu,'func_param' => 'admin_contact_menu'), $this);?>

<div class="actions">
	<ul>
		<?php if ($this->_vars['filter_data']['contact_need_approve']): ?>
		<li id="send_all"><div class="l"><a href="<?php echo $this->_vars['site_url']; ?>
admin/contact/send/1"><?php echo l('link_send', 'contact', '', 'text', array()); ?></a></div></li>
		<?php endif; ?>
		<li id="delete_all"><div class="l"><a href="<?php echo $this->_vars['site_url']; ?>
admin/contact/delete"><?php echo l('btn_delete', 'start', '', 'text', array()); ?></a></div></li>
	</ul>
	&nbsp;
</div>

<?php if ($this->_vars['filter_data']['contact_make_copies']): ?>
<div class="menu-level3">
	<ul>
		<li class="<?php if ($this->_vars['filter'] == 'all'): ?>active<?php endif;  if (! $this->_vars['filter_data']['all']): ?> hide<?php endif; ?>"><a href="<?php echo $this->_vars['site_url']; ?>
admin/contact/index/all"><?php echo l('stat_header_contact_all', 'contact', '', 'text', array()); ?> (<?php echo $this->_vars['filter_data']['all']; ?>
)</a></li>
		<li class="<?php if ($this->_vars['filter'] == 'unread'): ?>active<?php endif;  if (! $this->_vars['filter_data']['unread']): ?> hide<?php endif; ?>"><a href="<?php echo $this->_vars['site_url']; ?>
admin/contact/index/unread"><?php echo l('stat_header_contact_unread', 'contact', '', 'text', array()); ?> (<?php echo $this->_vars['filter_data']['unread']; ?>
)</a></li>
		<?php if ($this->_vars['filter_data']['contact_need_approve']): ?>
		<li class="<?php if ($this->_vars['filter'] == 'need_approve'): ?>active<?php endif;  if (! $this->_vars['filter_data']['need_approve']): ?> hide<?php endif; ?>"><a href="<?php echo $this->_vars['site_url']; ?>
admin/contact/index/need_approve"><?php echo l('stat_header_contact_need_approve', 'contact', '', 'text', array()); ?> (<?php echo $this->_vars['filter_data']['need_approve']; ?>
)</a></li>
		<?php endif; ?>
	</ul>
	&nbsp;
</div>
<?php endif; ?>
<form id="contact_form" action="" method="post">
<table cellspacing="0" cellpadding="0" class="data" width="100%">
	<tr>
		<th class="first w20"><a href="<?php echo $this->_vars['sort_links']['status']; ?>
"<?php if ($this->_vars['order'] == 'status'): ?> class="<?php echo $this->_run_modifier($this->_vars['order_direction'], 'lower', 'plugin', 1); ?>
"<?php endif; ?>>&nbsp;</a></th>
		<th class="w20"><input type="checkbox" id="grouping_all"></th>
		<th class="w200"><a href="<?php echo $this->_vars['sort_links']['message']; ?>
"<?php if ($this->_vars['order'] == 'message'): ?> class="<?php echo $this->_run_modifier($this->_vars['order_direction'], 'lower', 'plugin', 1); ?>
"<?php endif; ?>><?php echo l('field_contact_message', 'contact', '', 'text', array()); ?></a></th>
		<th class="w150"><a href="<?php echo $this->_vars['sort_links']['sender']; ?>
"<?php if ($this->_vars['order'] == 'sender'): ?> class="<?php echo $this->_run_modifier($this->_vars['order_direction'], 'lower', 'plugin', 1); ?>
"<?php endif; ?>><?php echo l('field_contact_sender', 'contact', '', 'text', array()); ?></a></th>
		<th class="w100"><a href="<?php echo $this->_vars['sort_links']['date_add']; ?>
"<?php if ($this->_vars['order'] == 'date_add'): ?> class="<?php echo $this->_run_modifier($this->_vars['order_direction'], 'lower', 'plugin', 1); ?>
"<?php endif; ?>><?php echo l('field_contact_date_add', 'contact', '', 'text', array()); ?></a></th>
		<th class="w100">&nbsp;</th>
	</tr>
	<?php if (is_array($this->_vars['contacts']) and count((array)$this->_vars['contacts'])): foreach ((array)$this->_vars['contacts'] as $this->_vars['item']): ?>
		<?php echo tpl_function_counter(array('print' => false,'assign' => counter), $this);?>
		<tr<?php if (!($this->_vars['counter'] % 2)): ?> class="zebra"<?php endif; ?>>
			<td class="center">
				<?php if ($this->_vars['item']['status']): ?>
				<a href="<?php echo $this->_vars['site_url']; ?>
admin/contact/read/<?php echo $this->_vars['item']['id']; ?>
/0"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-unmark.png" width="16" height="16" border="0" alt="<?php echo l('link_unread_contact', 'contact', '', 'button', array()); ?>" title="<?php echo l('link_unread_contact', 'contact', '', 'button', array()); ?>"></a>						
				<?php else: ?>
				<a href="<?php echo $this->_vars['site_url']; ?>
admin/contact/read/<?php echo $this->_vars['item']['id']; ?>
/1"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-mark.png" width="16" height="16" border="0" alt="<?php echo l('link_read_contact', 'contact', '', 'button', array()); ?>" title="<?php echo l('link_read_contact', 'contact', '', 'button', array()); ?>"></a>			
				<?php endif; ?>
			</td>	
			<td class="center"><input type="checkbox" name="ids[]" class="grouping" value="<?php echo $this->_vars['item']['id']; ?>
"></td>
			<td class="left"><?php if (! $this->_vars['item']['status']): ?><b><?php endif; ?><a href="<?php echo $this->_vars['site_url']; ?>
admin/contact/show/<?php echo $this->_vars['item']['id']; ?>
"><?php echo $this->_run_modifier($this->_vars['item']['message'], 'truncate', 'plugin', 1, 75); ?>
</a><?php if (! $this->_vars['item']['status']): ?></b><?php endif; ?></td>
			<td class="left"><?php if (! $this->_vars['item']['status']): ?><b><?php endif;  echo $this->_vars['item']['sender'];  if (! $this->_vars['item']['status']): ?></b><?php endif; ?></td>
			<td class="center"><?php if (! $this->_vars['item']['status']): ?><b><?php endif;  echo $this->_run_modifier($this->_vars['item']['date_add'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']);  if (! $this->_vars['item']['status']): ?></b><?php endif; ?></td>
			<td class="icons">				
				<a href="<?php echo $this->_vars['site_url']; ?>
admin/contact/show/<?php echo $this->_vars['item']['id']; ?>
"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-view.png" width="16" height="16" border="0" alt="<?php echo l('link_show_contact', 'contact', '', 'button', array()); ?>" title="<?php echo l('link_show_contact', 'contact', '', 'button', array()); ?>"></a>
				<a href="<?php echo $this->_vars['site_url']; ?>
admin/contact/delete/<?php echo $this->_vars['item']['id']; ?>
" onclick="javascript: if(!confirm('<?php echo l('note_delete_contact', 'contact', '', 'js', array()); ?>')) return false;"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-delete.png" width="16" height="16" border="0" alt="<?php echo l('link_delete_contact', 'contact', '', 'button', array()); ?>" title="<?php echo l('link_delete_contact', 'contact', '', 'button', array()); ?>"></a>				
				<?php if ($this->_vars['filter_data']['contact_need_approve']): ?>
					<?php if ($this->_vars['item']['allow_send'] && ! $this->_vars['item']['send']): ?>
					<a href="<?php echo $this->_vars['site_url']; ?>
admin/contact/send/1/<?php echo $this->_vars['item']['id']; ?>
"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-mail.png" width="16" height="16" border="0" alt="<?php echo l('link_send_contact', 'contact', '', 'button', array()); ?>" title="<?php echo l('link_send_contact', 'contact', '', 'button', array()); ?>"></a>	
					<?php else: ?>
					&nbsp;
					<?php endif; ?>
				<?php endif; ?>
			</td>
		</tr>
	<?php endforeach; else: ?>
		<tr><td colspan="6" class="center"><?php echo l('no_contacts', 'contact', '', 'text', array()); ?></td></tr>
	<?php endif; ?>
</table>
</form>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "pagination.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

<script><?php echo '
var reload_link = "';  echo $this->_vars['site_url']; ?>
admin/contact/index/<?php echo '";
var filter = \'';  echo $this->_vars['filter'];  echo '\';
var order = \'';  echo $this->_vars['order'];  echo '\';
var loading_content;
var order_direction = \'';  echo $this->_vars['order_direction'];  echo '\';
$(function(){
	$(\'#grouping_all\').bind(\'click\', function(){
		var checked = $(this).is(\':checked\');
		if(checked){
			$(\'input.grouping\').attr(\'checked\', \'checked\');
		}else{
			$(\'input.grouping\').removeAttr(\'checked\');
		}
	});
	$(\'#grouping_all\').bind(\'click\', function(){
		var checked = $(this).is(\':checked\');
		if(checked){
			$(\'input[type=checkbox].grouping\').attr(\'checked\', \'checked\');
		}else{
			$(\'input[type=checkbox].grouping\').removeAttr(\'checked\');
		}
	});
	$(\'#send_all,#delete_all\').bind(\'click\', function(){
		if(!$(\'input[type=checkbox].grouping\').is(\':checked\')) return false; 
		if(this.id == \'delete_all\' && !confirm(\'';  echo l('note_delete_all_contacts', 'contact', '', 'js', array());  echo '\')) return false;
		$(\'#contact_form\').attr(\'action\', $(this).find(\'a\').attr(\'href\')).submit();		
		return false;
	});

});
function reload_this_page(value){
	var link = reload_link + filter + \'/\' + value + \'/\' + order + \'/\' + order_direction;
	location.href=link;
}
'; ?>
</script>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
