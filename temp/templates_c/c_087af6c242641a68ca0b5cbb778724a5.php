<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-02 14:30:54 KRAT */ ?>

	<table cellspacing="0" cellpadding="0" class="data" width="100%">
	<tr>
		<th class="first" colspan="2"><?php echo l('stat_header_reviews', 'reviews', '', 'text', array()); ?></th>
	</tr>
	<?php if ($this->_vars['stat_reviews']['index_method']): ?>
	<?php if (is_array($this->_vars['stat_reviews']['types']) and count((array)$this->_vars['stat_reviews']['types'])): foreach ((array)$this->_vars['stat_reviews']['types'] as $this->_vars['item']): ?>
	<?php $this->assign('type_gid', 'type_'.$this->_vars['item']['gid']); ?>
	<?php echo tpl_function_counter(array('print' => false,'assign' => counter), $this);?>
	<tr <?php if (!(! ( $this->_vars['counter'] % 2) ) ): ?>class="zebra"<?php endif; ?>>
		<td class="first"><a href="<?php echo $this->_vars['site_url']; ?>
admin/reviews/index/<?php echo $this->_vars['item']['gid']; ?>
"><?php echo l('stat_header_reviews_'.$this->_vars['item']['gid'], 'reviews', '', 'text', array()); ?></a></td>
		<td class="w50 center"><a href="<?php echo $this->_vars['site_url']; ?>
admin/reviews/index/<?php echo $this->_vars['item']['gid']; ?>
"><?php echo $this->_vars['stat_reviews'][$this->_vars['type_gid']]; ?>
</a></td>
	</tr>
	<?php endforeach; endif; ?>
	<?php endif; ?>
	</table>

