<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.ld_option.php'); $this->register_function("ld_option", "tpl_function_ld_option");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.math.php'); $this->register_function("math", "tpl_function_math");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.date.php'); $this->register_modifier("date", "tpl_modifier_date");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.ld.php'); $this->register_function("ld", "tpl_function_ld");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.date_format.php'); $this->register_modifier("date_format", "tpl_modifier_date_format");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.escape.php'); $this->register_modifier("escape", "tpl_modifier_escape");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-04-17 12:20:04 KRAT */ ?>

<div class="content-block load_content">
	<h1><?php echo l('header_calendar_period_add', 'listings', '', 'text', array()); ?></h1>	
	<div class="inside edit_block">
		<form method="post" action="<?php echo $this->_run_modifier($this->_vars['data']['action'], 'escape', 'plugin', 1); ?>
" name="save_form" id="period_form" enctype="multipart/form-data">
			<?php if ($this->_vars['data']['price_period']): ?>
				<?php $this->assign('price_period', $this->_vars['data']['price_period']); ?>
			<?php else: ?>
				<?php $this->assign('price_period', 1); ?>
			<?php endif; ?>
			<div class="r fleft periodbox">
				<div class="f"><?php echo l('field_booking_date_start', 'listings', '', 'text', array()); ?>:</div>
				<div class="v">
											<?php switch($this->_vars['price_period']): case '1':  ?>
							<input type="text" name="period[date_start]" value="<?php if ($this->_run_modifier($this->_vars['period']['date_start'], 'strtotime', 'PHP', 1) > 0):  echo $this->_run_modifier($this->_run_modifier($this->_run_modifier($this->_vars['period']['date_start'], 'strtotime', 'PHP', 1), 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']), 'escape', 'plugin', 1);  endif; ?>" id="date_start<?php echo $this->_vars['rand']; ?>
" class="middle"> <a href="#" id="date_start_open_btn" title="<?php echo l('link_calendar_open', 'listings', '', 'button', array()); ?>"><ins class="with-icon i-calendar"></ins></a>
							<input type="hidden" name="date_start_alt" value="" id="alt_date_start<?php echo $this->_vars['rand']; ?>
">
							<script><?php echo '
								$(function(){
									$(\'#date_start';  echo $this->_vars['rand'];  echo '\').datepicker({dateFormat: \'';  echo $this->_vars['page_data']['datepicker_date_format'];  echo '\', altFormat: \'';  echo $this->_vars['page_data']['datepicker_alt_format'];  echo '\', altField: \'#alt_date_start';  echo $this->_vars['rand'];  echo '\', showOn: \'both\'});
								});
							'; ?>
</script>
						<?php break; case '2':  ?>
							<?php echo tpl_function_ld(array('i' => 'month-names','gid' => 'start','assign' => 'month_names'), $this);?>
							<select name="date_start_month" class="middle">
								<option value=""><?php echo $this->_vars['month_names']['header']; ?>
</value>
								<?php if (is_array($this->_vars['month_names']['option']) and count((array)$this->_vars['month_names']['option'])): foreach ((array)$this->_vars['month_names']['option'] as $this->_vars['key'] => $this->_vars['item']): ?>
								<option value="<?php echo $this->_vars['key']; ?>
" <?php if ($this->_vars['key'] == $this->_run_modifier($this->_vars['period']['date_start'], 'date_format', 'plugin', 1, '%m')): ?>selected<?php endif; ?>><?php echo $this->_vars['item']; ?>
</option>
								<?php endforeach; endif; ?>
							</select>
							<?php $this->assign('cyear', $this->_run_modifier('now', 'date', 'plugin', 1, 'Y')); ?>
							<select name="date_start_year" class="short">
								<option value=""><?php echo l('text_year', 'listings', '', 'text', array()); ?></option>
								<?php for($for1 = 0; ((0 < 10) ? ($for1 < 10) : ($for1 > 10)); $for1 += ((0 < 10) ? 1 : -1)):  $this->assign('i', $for1); ?>
								<?php echo tpl_function_math(array('equation' => 'x + y','x' => $this->_vars['cyear'],'y' => $this->_vars['i'],'assign' => 'year'), $this);?>
								<option value="<?php echo $this->_vars['year']; ?>
" <?php if ($this->_vars['year'] == $this->_run_modifier($this->_vars['period']['date_start'], 'date_format', 'plugin', 1, '%Y')): ?>selected<?php endif; ?>><?php echo $this->_vars['year']; ?>
</option>
								<?php endfor; ?>
							</select>
					<?php break; endswitch; ?>
				</div>
			</div>
			<div class="r fleft">&nbsp;</div>
			<div class="r fleft periodbox">
				<div class="f"><?php echo l('field_booking_date_end', 'listings', '', 'text', array()); ?>:</div>
				<div class="v">
											<?php switch($this->_vars['price_period']): case '1':  ?>
							<input type="text" name="period[date_end]" value="<?php if ($this->_run_modifier($this->_vars['period']['date_end'], 'strtotime', 'PHP', 1) > 0):  echo $this->_run_modifier($this->_run_modifier($this->_run_modifier($this->_vars['period']['date_end'], 'strtotime', 'PHP', 1), 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']), 'escape', 'plugin', 1);  endif; ?>" id="date_end<?php echo $this->_vars['rand']; ?>
" class="middle"> <a href="#" id="date_end_open_btn" title="<?php echo l('link_calendar_open', 'listings', '', 'button', array()); ?>"><ins class="with-icon i-calendar"></ins></a>
							<input type="hidden" name="date_end_alt" value="" id="alt_date_end<?php echo $this->_vars['rand']; ?>
">
							<script><?php echo '
								$(function(){
									$(\'#date_end';  echo $this->_vars['rand'];  echo '\').datepicker({dateFormat: \'';  echo $this->_vars['page_data']['datepicker_date_format'];  echo '\', altFormat: \'';  echo $this->_vars['page_data']['datepicker_alt_format'];  echo '\', altField: \'#alt_date_end';  echo $this->_vars['rand'];  echo '\', showOn: \'both\'});
								});
							'; ?>
</script>
						<?php break; case '2':  ?>
							<?php echo tpl_function_ld(array('i' => 'month-names','gid' => 'start','assign' => 'month_names'), $this);?>
							<select name="date_end_month" class="middle">
								<option value=""><?php echo $this->_vars['month_names']['header']; ?>
</value>
								<?php if (is_array($this->_vars['month_names']['option']) and count((array)$this->_vars['month_names']['option'])): foreach ((array)$this->_vars['month_names']['option'] as $this->_vars['key'] => $this->_vars['item']): ?>
								<option value="<?php echo $this->_vars['key']; ?>
" <?php if ($this->_vars['key'] == $this->_run_modifier($this->_vars['period']['date_end'], 'date_format', 'plugin', 1, '%m')): ?>selected<?php endif; ?>><?php echo $this->_vars['item']; ?>
</option>
								<?php endforeach; endif; ?>
							</select>
							<?php $this->assign('cyear', $this->_run_modifier('now', 'date', 'plugin', 1, 'Y')); ?>
							<select name="date_end_year" class="short">
								<option value=""><?php echo l('text_year', 'listings', '', 'text', array()); ?></option>
								<?php for($for1 = 0; ((0 < 10) ? ($for1 < 10) : ($for1 > 10)); $for1 += ((0 < 10) ? 1 : -1)):  $this->assign('i', $for1); ?>
								<?php echo tpl_function_math(array('equation' => 'x + y','x' => $this->_vars['cyear'],'y' => $this->_vars['i'],'assign' => 'year'), $this);?>
								<option value="<?php echo $this->_vars['year']; ?>
" <?php if ($this->_vars['year'] == $this->_run_modifier($this->_vars['period']['date_end'], 'date_format', 'plugin', 1, '%Y')): ?>selected<?php endif; ?>><?php echo $this->_vars['year']; ?>
</option>
								<?php endfor; ?>
							</select>
					<?php break; endswitch; ?>
				</div>
			</div>
			
			<div class="r fleft">&nbsp;</div>
			<div class="r fleft">
				<div class="f"><?php echo l('field_booking_guests', 'listings', '', 'text', array()); ?>:</div>
				<div class="v">
					<?php echo tpl_function_ld(array('i' => 'booking_guests','gid' => 'listings','assign' => 'booking_guests'), $this);?>
					<select name="period[guests]" class="middle">
						<option value=""><?php echo $this->_vars['booking_guests']['header']; ?>
</option>
						<?php if (is_array($this->_vars['booking_guests']['option']) and count((array)$this->_vars['booking_guests']['option'])): foreach ((array)$this->_vars['booking_guests']['option'] as $this->_vars['key'] => $this->_vars['item']): ?>
						<option value="<?php echo $this->_vars['key']; ?>
" <?php if ($this->_vars['key'] == $this->_vars['period']['guests']): ?>selected<?php endif; ?>><?php echo $this->_vars['item']; ?>
</option>
						<?php endforeach; endif; ?>
					</select>
				</div>
			</div>
			<?php if ($this->_vars['period']['price']): ?>
				<?php $this->assign('booking_price', $this->_vars['period']['price']); ?>
			<?php elseif ($this->_vars['data']['price_negotiated']): ?>
				<?php $this->assign('booking_price', '0'); ?>
			<?php elseif ($this->_vars['data']['price_reduced'] > 0): ?>
				<?php $this->assign('booking_price', $this->_vars['data']['price_reduced']); ?>
			<?php else: ?>
				<?php $this->assign('booking_price', $this->_vars['data']['price']); ?>
			<?php endif; ?>
			<div class="r fleft clr">
				<div class="f"><?php echo l('field_booking_price', 'listings', '', 'text', array()); ?>:</div>
				<div class="v">
					<input type="text" name="period[price]" value="<?php echo $this->_run_modifier($this->_vars['booking_price'], 'escape', 'plugin', 1); ?>
" class="middle"> <?php echo $this->_vars['current_price_currency']['abbr']; ?>
 <?php echo tpl_function_ld_option(array('i' => 'price_period','gid' => 'listings','option' => $this->_vars['price_period']), $this);?>
				</div>
			</div>
			<div class="r clr">
				<div class="f"><?php echo l('field_booking_comment', 'listings', '', 'text', array()); ?>:</div>
				<div class="v" id="period_comment">
					<textarea name="period[comment]" rows="10" cols="80"><?php echo $this->_run_modifier($this->_vars['period']['comment'], 'escape', 'plugin', 1); ?>
</textarea>
				</div>
			</div>
			<div class="b">
				<input type="submit" name="btn_save_period" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>" id="close_btn">
			</div>
			<input type="hidden" name="save_btn" value="1">
			<input type="hidden" name="period[status]" value="open">
		</form>
	</div>
</div>
