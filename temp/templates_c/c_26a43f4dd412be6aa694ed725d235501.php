<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-05-12 14:13:48 KRAT */ ?>

<ul>
<?php if (is_array($this->_vars['menu']) and count((array)$this->_vars['menu'])): foreach ((array)$this->_vars['menu'] as $this->_vars['key'] => $this->_vars['item']): ?>
<li <?php if ($this->_vars['item']['active']): ?>class="active"<?php endif; ?>>
	<a href="<?php echo $this->_vars['item']['link']; ?>
"><?php echo $this->_vars['item']['value']; ?>
</a>
	<?php if ($this->_vars['item']['sub']): ?>
	<div class="sub_menu_block">
		<span><?php echo $this->_vars['item']['value']; ?>
</span>
		<ul>
			<?php if (is_array($this->_vars['item']['sub']) and count((array)$this->_vars['item']['sub'])): foreach ((array)$this->_vars['item']['sub'] as $this->_vars['key2'] => $this->_vars['item2']): ?>
			<li class="main-menu"><a href="<?php echo $this->_vars['item2']['link']; ?>
"><?php echo $this->_vars['item2']['value']; ?>
 <?php if ($this->_vars['item']['indicator']): ?><span class="num"><?php echo $this->_vars['item']['indicator']; ?>
</span><?php endif; ?></a></li>
			<?php endforeach; endif; ?>
		</ul>
	</div>
	<?php endif; ?>
</li>
<?php endforeach; endif; ?>
<li id="social-links"><a href="#">Social</a>
	<ul id="social-contents">
		<li><a id="fb" class="socmed-icons" href="https://www.facebook.com/inproperti" target="_blank" title="Join Infoproperti on Facebook">Join Infoproperti on Facebook</a></li>
		<li><a id="twitter" class="socmed-icons" href="https://twitter.com/inproperti" target="_blank" title="Join Infoproperti on Twitter"></a></li>
		<li><a id="gplus" class="socmed-icons" href="https://plus.google.com/u/0/111307853610134749510/about" target="_blank" title="Join Infoproperti on Google Plus"></a></li>
		<li><a id="youtube" class="socmed-icons" href="http://www.youtube.com/channel/UCpY67JNdkJ8xxxeacwXJT4Q" target="_blank" title="Join Infoproperti on Youtube"></a></li>
		<!-- <li><a id="pinterest" class="socmed-icons" href="#" target="_blank" title="Join Infoproperti on Pinterest"></a></li>-->
		<li><a id="linkedin" class="socmed-icons" href="https://id.linkedin.com/in/infoproperti" target="_blank" title="Join Infoproperti on linkedIn"></a></li>
		<li><a id="instagram" class="socmed-icons" href="http://instagram.com/inproperti" target="_blank" title="Join Infoproperti on Instagram"></a></li>
	</ul>
</li>
</ul>
