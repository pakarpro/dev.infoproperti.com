<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-02 15:28:17 KRAT */ ?>

<?php if ($this->_run_modifier($this->_vars['current_search_data'], 'count', 'PHP', 1)): ?>
<ul>
	<?php if (is_array($this->_vars['current_search_data']) and count((array)$this->_vars['current_search_data'])): foreach ((array)$this->_vars['current_search_data'] as $this->_vars['key'] => $this->_vars['item']): ?>
	<li title="<?php echo $this->_run_modifier($this->_vars['item']['name'], 'escape', 'plugin', 1); ?>
: <?php echo $this->_run_modifier($this->_vars['item']['label'], 'escape', 'plugin', 1); ?>
" <?php if ($this->_vars['key'] == 'price'): ?>dir="ltr"<?php endif; ?>><?php echo $this->_run_modifier($this->_vars['item']['name'], 'truncate', 'plugin', 1, 10, '...', true); ?>
: <?php echo $this->_run_modifier($this->_vars['item']['label'], 'truncate', 'plugin', 1, 12, '...', true); ?>
 <a href="<?php echo $this->_vars['site_url']; ?>
listings/delete_search_criteria/<?php echo $this->_run_modifier($this->_vars['key'], 'escape', 'plugin', 1); ?>
" class="btn-link small fright"><ins class="with-icon-small i-delete"></ins></a></li>
	<?php endforeach; endif; ?>
</ul>
<?php endif; ?>
