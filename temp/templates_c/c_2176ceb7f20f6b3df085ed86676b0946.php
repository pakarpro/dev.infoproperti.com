<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.counter.php'); $this->register_function("counter", "tpl_function_counter");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.seolink.php'); $this->register_function("seolink", "tpl_function_seolink");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.truncate.php'); $this->register_modifier("truncate", "tpl_modifier_truncate");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.escape.php'); $this->register_modifier("escape", "tpl_modifier_escape");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.js.php'); $this->register_function("js", "tpl_function_js");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-05-06 11:56:41 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array('load_type' => 'ui'));
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

<div class="menu-level3">
	<ul>
		<?php if ($this->_vars['data']['id']): ?>
		<li class="<?php if ($this->_vars['section_gid'] == 'general'): ?>active<?php endif; ?>"><a href="<?php echo $this->_vars['site_url']; ?>
admin/export/edit/<?php echo $this->_vars['data']['id']; ?>
/general"><?php echo l('filter_section_general', 'export', '', 'text', array()); ?></a></li>
		<li class="<?php if ($this->_vars['section_gid'] == 'custom_fields'): ?>active<?php endif; ?>"><a href="<?php echo $this->_vars['site_url']; ?>
admin/export/edit/<?php echo $this->_vars['data']['id']; ?>
/custom_fields"><?php echo l('filter_section_custom_fields', 'export', '', 'text', array()); ?></a></li>
		<li class="<?php if ($this->_vars['section_gid'] == 'search_form'): ?>active<?php endif; ?>"><a href="<?php echo $this->_vars['site_url']; ?>
admin/export/edit/<?php echo $this->_vars['data']['id']; ?>
/search_form"><?php echo l('filter_section_search_form', 'export', '', 'text', array()); ?></a></li>
		<?php else: ?>
		<li class="<?php if ($this->_vars['section_gid'] == 'general'): ?>active<?php endif; ?>"><?php echo l('filter_section_general', 'export', '', 'text', array()); ?></li>
		<?php endif; ?>
	</ul>
	&nbsp;
</div>

<?php if ($this->_vars['section_gid'] == 'general'):  echo tpl_function_js(array('file' => 'jquery-ui.custom.min.js'), $this);?>
<LINK href='<?php echo $this->_vars['site_root'];  echo $this->_vars['js_folder']; ?>
jquery-ui/jquery-ui.custom.css' rel='stylesheet' type='text/css' media='screen'>
<form method="post" action="<?php echo $this->_vars['data']['action']; ?>
" name="save_form" enctype="multipart/form-data">
	<div class="edit-form n150">
		<div class="row header"><?php echo l('admin_header_selections_general_edit', 'export', '', 'text', array()); ?></div>
		<div class="row">
			<div class="h"><?php echo l("field_selection_name", 'export', '', 'text', array()); ?>:&nbsp;* </div>
			<div class="v"><input type="text" name="data[name]" value="<?php echo $this->_run_modifier($this->_vars['data']['name'], 'escape', 'plugin', 1); ?>
" /></div>
		</div>
		<div class="row zebra">
			<div class="h"><?php echo l("field_selection_driver", 'export', '', 'text', array()); ?>:&nbsp;* </div>
			<div class="v">
				<?php if ($this->_vars['data']['id']): ?>
					<?php echo $this->_run_modifier($this->_vars['data']['driver']['output_name'], 'truncate', 'plugin', 1, 100); ?>

				<?php else: ?>
				<select name="data[gid_driver]">
					<?php if (is_array($this->_vars['drivers']) and count((array)$this->_vars['drivers'])): foreach ((array)$this->_vars['drivers'] as $this->_vars['item']): ?>
					<option value="<?php echo $this->_vars['item']['gid']; ?>
"><?php echo $this->_run_modifier($this->_vars['item']['output_name'], 'truncate', 'plugin', 1, 100); ?>
</option>
					<?php endforeach; else: ?>
					<option value="">...</option>
					<?php endif; ?>
				</select>
				<?php endif; ?>
			</div>
		</div>
		<div class="row">
			<div class="h"><?php echo l("field_selection_module", 'export', '', 'text', array()); ?>:&nbsp;* </div>
			<div class="v">
				<?php if ($this->_vars['data']['id']): ?>
					<?php echo $this->_run_modifier($this->_vars['data']['module']['output_name'], 'truncate', 'plugin', 1, 100); ?>

				<?php else: ?>
				<select name="data[id_object]">
					<?php if (is_array($this->_vars['modules']) and count((array)$this->_vars['modules'])): foreach ((array)$this->_vars['modules'] as $this->_vars['item']): ?>
					<option value="<?php echo $this->_vars['item']['id']; ?>
"><?php echo $this->_run_modifier($this->_vars['item']['output_name'], 'truncate', 'plugin', 1, 100); ?>
</option>
					<?php endforeach; else: ?>
					<option value="">-</option>
					<?php endif; ?>
				</select>
				<?php endif; ?>
			</div>
		</div>
		<?php if ($this->_vars['data']['id']): ?>
		<div class="row zebra">
			<div class="h"><?php echo l("field_selection_output_type", 'export', '', 'text', array()); ?>:&nbsp;* </div>
			<div class="v">
				<select name="data[output_type]" id="output_type">
					<?php if (is_array($this->_vars['output_types']) and count((array)$this->_vars['output_types'])): foreach ((array)$this->_vars['output_types'] as $this->_vars['item']): ?>
					<option value="<?php echo $this->_run_modifier($this->_vars['item'], 'escape', 'plugin', 1); ?>
" <?php if ($this->_vars['data']['output_type'] == $this->_vars['item']): ?>selected<?php endif; ?>><?php echo l('output_'.$this->_vars['item'], 'export', '', 'text', array()); ?></option>
					<?php endforeach; endif; ?>
				</select>
			</div>
		</div>
	
		<div id="settings_file" class="settings <?php if ($this->_vars['data']['output_type'] != 'file'): ?>hide<?php endif; ?>">
		
		<div class="row">
			<div class="h"><?php echo l("field_selection_file_format", 'export', '', 'text', array()); ?>: </div>
			<div class="v">
				<input type="text" name="data[file_format]" value="<?php echo $this->_run_modifier($this->_vars['data']['file_format'], 'escape', 'plugin', 1); ?>
">
			</div>
		</div>
		<div class="row zebra">
			<div class="h"><?php echo l("field_selection_append_date", 'export', '', 'text', array()); ?>: </div>
			<div class="v">
				<input type="hidden" name="data[append_date]" value="0">
				<input type="checkbox" name="data[append_date]" value="1" <?php if ($this->_vars['data']['append_date']): ?>checked<?php endif; ?>>
			</div>
		</div>
		<div class="row">
			<div class="h"><?php echo l("field_selection_scheduler", 'export', '', 'text', array()); ?>:&nbsp;* </div>
			<div class="v">
				<p><input type="radio" name="data[scheduler][type]" value="manual" <?php if (! $this->_vars['data']['scheduler']['type'] || $this->_vars['data']['scheduler']['type'] == 'manual'): ?>checked<?php endif; ?>><?php echo l('start_manual', 'export', '', 'text', array()); ?></p>
								<p>
					<input type="radio" name="data[scheduler][type]" value="intime" <?php if ($this->_vars['data']['scheduler']['type'] == 'intime'): ?>checked<?php endif; ?>><?php echo l('start_in_time', 'export', '', 'text', array()); ?>
					<input type='text' value='<?php if ($this->_vars['data']['scheduler']['type'] == 'intime'):  echo $this->_vars['data']['scheduler']['date'];  endif; ?>' name="scheduler[intime][date]" id="datepicker1" maxlength="10" class="short">
					<select name="scheduler[intime][hours]" class="short">
						<?php if (is_array($this->_vars['hours']) and count((array)$this->_vars['hours'])): foreach ((array)$this->_vars['hours'] as $this->_vars['item']): ?>
						<option value="<?php echo $this->_run_modifier($this->_vars['item'], 'escape', 'plugin', 1); ?>
" <?php if ($this->_vars['data']['scheduler']['hours'] == $this->_vars['item'] && $this->_vars['data']['scheduler']['type'] == 'intime'): ?>selected<?php endif; ?>><?php echo $this->_vars['item']; ?>
</option>
						<?php endforeach; endif; ?>
					</select>
					<select name="scheduler[intime][minutes]" class="short">
						<?php if (is_array($this->_vars['minutes']) and count((array)$this->_vars['minutes'])): foreach ((array)$this->_vars['minutes'] as $this->_vars['item']): ?>
						<option value="<?php echo $this->_run_modifier($this->_vars['item'], 'escape', 'plugin', 1); ?>
" <?php if ($this->_vars['data']['scheduler']['minutes'] == $this->_vars['item'] && $this->_vars['data']['scheduler']['type'] == 'intime'): ?>selected<?php endif; ?>><?php echo $this->_vars['item']; ?>
</option>
						<?php endforeach; endif; ?>
					</select>
				</p>
				<p>
					<input type="radio" name="data[scheduler][type]" value="cron" <?php if ($this->_vars['data']['scheduler']['type'] == 'cron'): ?>checked<?php endif; ?>><?php echo l('start_cron', 'export', '', 'text', array()); ?>
					
					<select name="scheduler[cron][period]" class="middle">
						<option value="day" <?php if ($this->_vars['data']['scheduler']['period'] == 'day'): ?>selected<?php endif; ?>><?php echo l('start_type_day', 'export', '', 'text', array()); ?></option>
						<option value="week" <?php if ($this->_vars['data']['scheduler']['period'] == 'week'): ?>selected<?php endif; ?>><?php echo l('start_type_week', 'export', '', 'text', array()); ?></option>
						<option value="month" <?php if ($this->_vars['data']['scheduler']['period'] == 'month'): ?>selected<?php endif; ?>><?php echo l('start_type_month', 'export', '', 'text', array()); ?></option>
					</select>
					<?php echo l('start_type_since', 'export', '', 'text', array()); ?>
					<input type='text' value='<?php if ($this->_vars['data']['scheduler']['type'] == 'cron'):  echo $this->_vars['data']['scheduler']['date'];  endif; ?>' name="scheduler[cron][date]" id="datepicker2" maxlength="10" class="short">
					<select name="scheduler[cron][hours]" class="short">
						<?php if (is_array($this->_vars['hours']) and count((array)$this->_vars['hours'])): foreach ((array)$this->_vars['hours'] as $this->_vars['item']): ?>
						<option value="<?php echo $this->_run_modifier($this->_vars['item'], 'escape', 'plugin', 1); ?>
" <?php if ($this->_vars['data']['scheduler']['hours'] == $this->_vars['item'] && $this->_vars['data']['scheduler']['type'] == 'cron'): ?>selected<?php endif; ?>><?php echo $this->_vars['item']; ?>
</option>
						<?php endforeach; endif; ?>
					</select>
					<select name="scheduler[cron][minutes]" class="short">
						<?php if (is_array($this->_vars['minutes']) and count((array)$this->_vars['minutes'])): foreach ((array)$this->_vars['minutes'] as $this->_vars['item']): ?>
						<option value="<?php echo $this->_run_modifier($this->_vars['item'], 'escape', 'plugin', 1); ?>
" <?php if ($this->_vars['data']['scheduler']['minutes'] == $this->_vars['item'] && $this->_vars['data']['scheduler']['type'] == 'cron'): ?>selected<?php endif; ?>><?php echo $this->_vars['item']; ?>
</option>
						<?php endforeach; endif; ?>
					</select>
				</p>
							</div>
		</div>
		
		<?php if (! $this->_vars['data']['append_date'] && $this->_vars['data']['file_format']): ?>
		<div class="row zebra">
			<div class="h"><?php echo l('link_export_data', 'export', '', 'text', array()); ?></div>
			<div class="v"><?php echo $this->_vars['site_url']; ?>
uploads/export/<?php echo $this->_vars['data']['id']; ?>
/<?php echo $this->_run_modifier($this->_vars['data']['file_format'], 'strftime', 'PHP', 1); ?>
</div>
		</div>
		<?php endif; ?>
		
		</div>
		
		<div id="settings_browser" class="settings <?php if ($this->_vars['data']['output_type'] != 'browser'): ?>hide<?php endif; ?>">
		
		<div class="row">
			<div class="h"><?php echo l("field_selection_published", 'export', '', 'text', array()); ?>: </div>
			<div class="v">
				<input type="hidden" name="data[published]" value="0" />
				<input type="checkbox" name="data[published]" value="1" <?php if ($this->_vars['data']['published']): ?>checked="checked"<?php endif; ?> id="publish_selection" />
				<span id="publish_link" <?php if (! $this->_vars['data']['published']): ?>class="hide"<?php endif; ?>><?php echo tpl_function_seolink(array('module' => 'export','method' => 'index','data' => $this->_vars['data']), $this);?></span>
			</div>
		</div>
		<div class="row zebra">
			<div class="h"><?php echo l("field_selection_editable", 'export', '', 'text', array()); ?>: </div>
			<div class="v">
				<input type="hidden" name="data[editable]" value="0" />
				<input type="checkbox" name="data[editable]" value="1" <?php if ($this->_vars['data']['editable']): ?>checked="checked"<?php endif; ?> <?php if (! $this->_vars['data']['published']): ?>disabled<?php endif; ?> id="advanced_selection" />
				<span id="advanced_link" <?php if (! $this->_vars['data']['editable']): ?>class="hide"<?php endif; ?>><?php echo tpl_function_seolink(array('module' => 'export','method' => 'advanced','data' => $this->_vars['data']), $this);?></span>
				
			</div>
		</div>
		
		</div>
		<?php endif; ?>
	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div></div>
	<a class="cancel" href="<?php echo $this->_vars['site_url']; ?>
admin/export"><?php echo l('btn_cancel', 'start', '', 'text', array()); ?></a>
</form>
<script><?php echo '
	$(function(){
		$(\'#datepicker1, #datepicker2\').datepicker({dateFormat :\'yy-mm-dd\'});
		$(\'#output_type\').bind(\'change\', function(){
			$(\'.settings\').hide();
			$(\'#settings_\'+$(this).val()).show();
		});
		$(\'#publish_selection\').bind(\'change\', function(){
			if(this.checked){
				$(\'#advanced_selection\').removeAttr(\'disabled\');
				$(\'#publish_link\').show();
			}else{
				$(\'#advanced_selection\').attr(\'disabled\', \'disabled\').removeAttr(\'checked\');
				$(\'#advanced_link\').hide();
				$(\'#publish_link\').hide();
			}
		});
		$(\'#advanced_selection\').bind(\'change\', function(){
			if(this.checked){
				$(\'#advanced_link\').show();
			}else{
				$(\'#advanced_link\').hide();
			}
		});
	});
'; ?>
</script>
<?php elseif ($this->_vars['section_gid'] == 'custom_fields' && $this->_vars['data']['id']): ?>

<?php if ($this->_vars['data']['driver']['editable']): ?>
	<?php if ($this->_run_modifier($this->_vars['driver_fields'], 'count', 'PHP', 1)): ?>
	<form method="post" action="<?php echo $this->_vars['site_root']; ?>
admin/export/create_relation/<?php echo $this->_vars['data']['id']; ?>
" name="save_form" enctype="multipart/form-data">
	<div class="filter-form">
		<div class="block-l">
			<?php echo l('form_driver_fields_header', 'export', '', 'text', array()); ?>:<br>
			<select name="name" id="name_select" class="wide"><?php if (is_array($this->_vars['driver_fields']) and count((array)$this->_vars['driver_fields'])): foreach ((array)$this->_vars['driver_fields'] as $this->_vars['key'] => $this->_vars['item']): ?><option value="<?php echo $this->_run_modifier($this->_vars['key'], 'escape', 'plugin', 1); ?>
"><?php echo $this->_vars['item']['name']; ?>
</option><?php endforeach; endif; ?></select>
		</div>
		<div class="block-r">
			<?php echo l('form_module_fields_header', 'export', '', 'text', array()); ?>:<br>
			<select name="link" id="link_select" class="wide"><?php if (is_array($this->_vars['custom_fields']) and count((array)$this->_vars['custom_fields'])): foreach ((array)$this->_vars['custom_fields'] as $this->_vars['key'] => $this->_vars['item']): ?><option value="<?php echo $this->_run_modifier($this->_vars['key'], 'escape', 'plugin', 1); ?>
"><?php echo $this->_vars['item']['label']; ?>
</option><?php endforeach; else: ?><option value="-1">-</option><?php endif; ?></select>
		</div>
	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="<?php echo l('btn_add', 'start', '', 'button', array()); ?>"></div></div>
	</form>

	<script><?php echo '
		$(function(){
			$(\'#name_select\').bind(\'change\', function(){
				var linkSelect = $(\'#link_select\');
				$.post(\'';  echo $this->_vars['site_url'];  echo '/admin/export/ajax_get_module_fields/';  echo $this->_vars['data']['id'];  echo '\', {index: $(this).val()}, function(data){
					linkSelect.empty();
					if($.makeArray(data).length){
						for(i in data){
							linkSelect.append(\'<option value="\'+i+\'">\'+data[i].label+\'</option>\');
						}
					}else{
						linkSelect.append(\'<option value="">-</option>\');
					}
				}, \'json\');			
			});
		});
	'; ?>
</script>		
	<?php endif; ?>
	
			
	<table cellspacing="0" cellpadding="0" class="data" width="100%">
	<tr>
		<th class="first w150"><?php echo l('field_custom_name', 'export', '', 'text', array()); ?></th>
		<th class="w150"><?php echo l('field_custom_type', 'export', '', 'text', array()); ?></th>
		<th class="w300"><?php echo l('field_custom_link', 'export', '', 'text', array()); ?></th>
		<th class="w70">&nbsp;</th>
	</tr>
	<?php if (is_array($this->_vars['data']['relations']) and count((array)$this->_vars['data']['relations'])): foreach ((array)$this->_vars['data']['relations'] as $this->_vars['key'] => $this->_vars['item']): ?>
	<?php echo tpl_function_counter(array('print' => false,'assign' => counter), $this);?>
	<tr<?php if (!($this->_vars['counter'] % 2)): ?> class="zebra"<?php endif; ?>>
		<td><?php echo $this->_run_modifier($this->_vars['item']['name'], 'truncate', 'plugin', 1, 100); ?>
</td>
		<td><?php echo $this->_run_modifier($this->_vars['item']['type'], 'truncate', 'plugin', 1, 30); ?>
</td>
		<td><?php echo $this->_run_modifier($this->_vars['item']['label'], 'truncate', 'plugin', 1, 150); ?>
</td>
		<td class="icons">
			<?php if ($this->_vars['data']['driver']['editable']): ?>
			<a href="<?php echo $this->_vars['site_url']; ?>
admin/export/delete_relation/<?php echo $this->_vars['data']['id']; ?>
/<?php echo $this->_vars['key']; ?>
"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-delete.png" width="16" height="16" border="0" alt="<?php echo l('link_delete_relation', 'export', '', 'button', array()); ?>" title="<?php echo l('link_delete_relation', 'export', '', 'button', array()); ?>"></a>
			<?php endif; ?>
		</td>
	</tr>
	<?php endforeach; else: ?>
	<tr><td colspan="4" class="center"><?php echo l('no_custom_fields', 'export', '', 'text', array()); ?></td></tr>
	<?php endif; ?>
	</table>
	<br><a class="cancel" href="<?php echo $this->_vars['site_url']; ?>
admin/export"><?php echo l('btn_cancel', 'start', '', 'text', array()); ?></a>
			
<?php else: ?>
<form method="post" action="<?php echo $this->_vars['site_root']; ?>
admin/export/save_relations/<?php echo $this->_vars['data']['id']; ?>
" name="save_form" enctype="multipart/form-data">
	<table cellspacing="0" cellpadding="0" class="data" width="100%">
	<tr>
		<th class="first w150"><?php echo l('field_custom_name', 'export', '', 'text', array()); ?></th>
		<th class="w150"><?php echo l('field_custom_type', 'export', '', 'text', array()); ?></th>
		<th class="w300"><?php echo l('field_custom_link', 'export', '', 'text', array()); ?></th>
	</tr>
	<?php if (is_array($this->_vars['driver_fields']) and count((array)$this->_vars['driver_fields'])): foreach ((array)$this->_vars['driver_fields'] as $this->_vars['key'] => $this->_vars['item']): ?>
	<?php echo tpl_function_counter(array('print' => false,'assign' => counter), $this);?>
	<tr<?php if (!($this->_vars['counter'] % 2)): ?> class="zebra"<?php endif; ?>>
		<td><?php echo $this->_run_modifier($this->_vars['item']['name'], 'truncate', 'plugin', 1, 100);  if ($this->_vars['item']['required']): ?>&nbsp;*<?php endif; ?></td>
		<td><?php echo $this->_run_modifier($this->_vars['item']['type'], 'truncate', 'plugin', 1, 30); ?>
</td>
		<td>
			<select name="link[<?php echo $this->_run_modifier($this->_vars['key'], 'escape', 'plugin', 1); ?>
]">
			<option value="-1">...</option>
			<?php if (is_array($this->_vars['item']['custom_fields']) and count((array)$this->_vars['item']['custom_fields'])): foreach ((array)$this->_vars['item']['custom_fields'] as $this->_vars['key2'] => $this->_vars['item2']): ?>
			<option value="<?php echo $this->_run_modifier($this->_vars['key2'], 'escape', 'plugin', 1); ?>
" <?php if ($this->_vars['data']['relations'][$this->_vars['key']] && $this->_vars['data']['relations'][$this->_vars['key']]['link'] == $this->_vars['item2']['name']): ?>selected<?php endif; ?>><?php echo $this->_vars['item2']['label']; ?>
</option>
			<?php endforeach; endif; ?>
			</select>
		</td>				
	</tr>
	<?php endforeach; else: ?>
	<tr><td colspan="3" class="center"><?php echo l('no_custom_fields', 'export', '', 'text', array()); ?></td></tr>
	<?php endif; ?>
	</table>
				
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div></div>
	<a class="cancel" href="<?php echo $this->_vars['site_url']; ?>
admin/export"><?php echo l('btn_cancel', 'start', '', 'text', array()); ?></a>
</form>
<?php endif; ?>
			
<?php elseif ($this->_vars['section_gid'] == 'search_form' && $this->_vars['data']['id']): ?>
<form method="post" action="<?php echo $this->_vars['site_root']; ?>
admin/export/save_form_data/<?php echo $this->_vars['data']['id']; ?>
" name="save_form" enctype="multipart/form-data">
	<div class="edit-form n150" id="export_form_box">
		<div class="row header"><?php echo l('admin_header_selections_form_edit', 'export', '', 'text', array()); ?></div>
		<?php $this->assign('data', $this->_vars['data']['admin_form_data']); ?>
		<?php echo $this->_vars['search_form']; ?>

	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div></div>
	<a class="cancel" href="<?php echo $this->_vars['site_url']; ?>
admin/export"><?php echo l('btn_cancel', 'start', '', 'text', array()); ?></a>
</form>	
<?php endif; ?>

<div class="clr"></div>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
