<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-02 15:54:48 KRAT */ ?>

<a <?php if ($this->_vars['is_listings']): ?>href="<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'user','id_user' => $this->_vars['user_id'],'user' => $this->_vars['user_name']), $this); endif; ?>" class="btn-link link-r-margin" title="<?php echo l('link_more', 'listings', '', 'button', array()); ?>"><ins class="with-icon <?php if (! $this->_vars['is_listings']): ?>g no-hover<?php endif; ?> i-listings"></ins></a>
