<?php require_once('/opt/ip/system/libraries/template_lite/plugins/function.counter.php'); $this->register_function("counter", "tpl_function_counter");  require_once('/opt/ip/system/libraries/template_lite/plugins/modifier.escape.php'); $this->register_modifier("escape", "tpl_modifier_escape");  require_once('/opt/ip/system/libraries/template_lite/plugins/modifier.truncate.php'); $this->register_modifier("truncate", "tpl_modifier_truncate");  require_once('/opt/ip/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-02-25 10:01:56 KRAT */ ?>

<div class="edit_block load_content">
	<h1><?php if ($this->_vars['data']['id']):  echo l('header_edit_photo', 'listings', '', 'text', array());  else:  echo l('header_add_photo', 'listings', '', 'text', array());  endif; ?></h1>
	<div class="inside edit_block">
		<div class="tabs tab-size-15 noPrint">
			<ul id="photo_edit_sections">
				<li id="m_view" sgid="view" class="active"><a href="<?php echo $this->_vars['site_url']; ?>
upload_gallery/view/<?php echo $this->_vars['data']['id']; ?>
"><?php echo l('link_photo_view', 'listings', '', 'text', array()); ?></a></li>
				<li id="m_comment" sgid="comment"><a href="<?php echo $this->_vars['site_url']; ?>
upload_gallery/comment/<?php echo $this->_vars['data']['id']; ?>
"><?php echo l('link_photo_comment', 'listings', '', 'text', array()); ?></a></li>
				<li id="m_rotate" sgid="rotate"><a href="<?php echo $this->_vars['site_url']; ?>
upload_gallery/rotate/<?php echo $this->_vars['data']['id']; ?>
/90"><?php echo l('link_photo_rotate', 'listings', '', 'text', array()); ?></a></li>
				<li id="m_recrop" sgid="recrop"><a href="<?php echo $this->_vars['site_url']; ?>
listings/recrop/<?php echo $this->_vars['data']['id']; ?>
/small"><?php echo l('link_photo_recrop', 'listings', '', 'text', array()); ?></a></li>
			</ul>
		</div>
		<div id="photo_edit_block">
			<div id="content_m_view" class="view-section">
				<div id="photo_edit">
					<div class="source_box">
						<div id="photo_source_view_box" class="photo_source_box">
							<div class="imagebox">
								<img src="<?php echo $this->_vars['data']['media']['file_url']; ?>
?<?php echo $this->_run_modifier('', 'time', 'PHP', 1); ?>
" <?php if (344 * $this->_vars['data']['settings']['width'] > 500 * $this->_vars['data']['settings']['height']): ?>width="<?php echo $this->_run_modifier($this->_vars['data']['settings']['width'], 'min', 'PHP', 1, 500); ?>
"<?php else: ?>height="<?php echo $this->_run_modifier($this->_vars['data']['settings']['height'], 'min', 'PHP', 1, 344); ?>
"<?php endif; ?> id="photo_source_view">
								<div class="statusbar <?php if (! $this->_vars['data']['comment']): ?>hide<?php endif; ?>">
									<div class="panel"><?php echo $this->_run_modifier($this->_vars['data']['comment'], 'truncate', 'plugin', 1, 255); ?>
</div>
									<div class="background"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="b outside" id="actionbar_basic">
					<?php if ($this->_vars['data']['media']['file_name'] && $this->_vars['data']['id']): ?><input type="button" class="btn" value="<?php echo l('btn_delete', 'start', '', 'button', array()); ?>" name="btn_delete" onclick="javascript: gUpload.delete_photo(<?php echo $this->_vars['data']['id']; ?>
);"><?php endif; ?>
				</div>				
				<div class="clr"></div>
			</div>
			<div id="content_m_comment" class="hide view-section">
				<form action="" method="post" enctype="multipart/form-data">
					<div class="r">
						<div class="f"><?php echo l('field_photo_comment', 'listings', '', 'text', array()); ?></div>
						<div class="v"><textarea name="comment" rows="10" cols="80" id="photo_comment"><?php echo $this->_run_modifier($this->_vars['data']['comment'], 'escape', 'plugin', 1); ?>
</textarea></div>
					</div>
					<div class="b outside" id="actionbar_basic">
						<?php if ($this->_vars['data']['media']['file_name'] && $this->_vars['data']['id']): ?><input type="button" class="btn" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>" name="btn_comment" onclick="javascript: gUpload.save_photo_data(<?php echo $this->_vars['data']['id']; ?>
); return false;"><?php endif; ?>
					</div>	
				</form>
				<div class="clr"></div>
			</div>
			<div id="content_m_rotate" class="hide view-section">
				<div id="photo_edit">
					<div class="source_box">
						<div id="photo_source_rotate_box" class="photo_source_box">
							<img src="<?php echo $this->_vars['data']['media']['file_url']; ?>
?<?php echo $this->_run_modifier('', 'time', 'PHP', 1); ?>
" <?php if (4 * $this->_vars['data']['settings']['width'] > 5 * $this->_vars['data']['settings']['height']): ?>width="<?php echo $this->_run_modifier($this->_vars['data']['settings']['width'], 'min', 'PHP', 1, 500); ?>
"<?php else: ?>height="<?php echo $this->_run_modifier($this->_vars['data']['settings']['height'], 'min', 'PHP', 1, 400); ?>
"<?php endif; ?> id="photo_source_rotate">
						</div>
						<div class="statusbar">
							<div class="panel">
								<a href="javascript:void(0);" class="btn-link" id="photo_rotate_left"><ins class="with-icon i-rotate-left w"></ins></a>
								<a href="javascript:void(0);" class="btn-link" id="photo_rotate_right"><ins class="with-icon i-rotate-right w"></ins></a>
							</div>
							<div class="background"></div>
						</div>
					</div>
				</div>
			</div>
			<div id="content_m_recrop" class="hide view-section">
				<form action="" method="post" enctype="multipart/form-data">
					<div id="photo_edit">
						<div class="source_box">
							<ul id="photo_sizes">
								<?php if (is_array($this->_vars['selections']) and count((array)$this->_vars['selections'])): foreach ((array)$this->_vars['selections'] as $this->_vars['key'] => $this->_vars['item']): ?>
								<?php echo tpl_function_counter(array('print' => false,'assign' => counter), $this);?>
								<?php if ($this->_vars['counter'] != 1): ?><li>&rarr;</li><?php endif; ?>
								<li id="photo_size_<?php echo $this->_vars['key']; ?>
" class="<?php if ($this->_vars['counter'] == 1): ?>active<?php endif; ?> photo_size"><span><?php echo l('photo_size_'.$this->_vars['key'], 'listings', '', 'text', array()); ?></span></li>								
								<?php endforeach; endif; ?>
							</ul>
							<div id="photo_source_recrop_box" class="photo_source_box">
								<img src="<?php echo $this->_vars['data']['media']['file_url']; ?>
?<?php echo $this->_run_modifier('', 'time', 'PHP', 1); ?>
" <?php if (344 * $this->_vars['data']['settings']['width'] > 500 * $this->_vars['data']['settings']['height']): ?>width="<?php echo $this->_run_modifier($this->_vars['data']['settings']['width'], 'min', 'PHP', 1, 500); ?>
"<?php else: ?>height="<?php echo $this->_run_modifier($this->_vars['data']['settings']['height'], 'min', 'PHP', 1, 344); ?>
"<?php endif; ?> id="photo_source_recrop">
							</div>
							<div class="statusbar">
								<div class="panel">
									<a href="javascript:void(0);" class="btn-link" id="recrop_btn"><ins class="with-icon i-crop w"></ins></a>
								</div>
								<div class="background"></div>
							</div>
						</div>
					</div>
					<div class="clr"></div>
				</form>
			</div>
		</div>
	</div>
</div>
<?php if ($this->_vars['data']['id']): ?>
<script><?php echo '	
	var pEdit;
	$(function(){
		pEdit = new photoEdit({
			siteUrl: \'';  echo $this->_vars['site_url'];  echo '\',
			imageWidth: ';  echo $this->_vars['data']['settings']['width'];  echo ',
			imageHeight: ';  echo $this->_vars['data']['settings']['height'];  echo ',
			idPhoto: ';  echo $this->_vars['data']['id'];  echo ',
			parentImage: \''; ?>
listing_photo_<?php echo $this->_vars['data']['id'];  echo '\',
			parentThumb: \'small\',
		});
		
		'; ?>

		<?php if (is_array($this->_vars['selections']) and count((array)$this->_vars['selections'])): foreach ((array)$this->_vars['selections'] as $this->_vars['key'] => $this->_vars['item']): ?>
		pEdit.add_selection('<?php echo $this->_vars['key']; ?>
', 0, 0, <?php echo $this->_run_modifier($this->_vars['item']['width'], 'min', 'PHP', 1, $this->_vars['data']['settings']['width']); ?>
, <?php echo $this->_run_modifier($this->_vars['item']['height'], 'min', 'PHP', 1, $this->_vars['data']['settings']['height']); ?>
, <?php echo $this->_vars['item']['width']; ?>
, <?php echo $this->_vars['item']['height']; ?>
);
		<?php endforeach; endif; ?>
		<?php echo '
	});	
'; ?>
</script>	
<?php endif; ?>
