<?php require_once('/opt/ip/system/libraries/template_lite/plugins/function.js.php'); $this->register_function("js", "tpl_function_js");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-12-14 16:30:55 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->module_path. "start". $this->module_templates.  $this->get_current_theme_gid('', '"start"'). "left_panel.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

<div class="rc">
	<div class="content-block">
		<h1><?php echo l('header_my_requests', 'listings', '', 'text', array()); ?> (<span id="requests_count"><?php echo $this->_vars['requests_count_sum']; ?>
</span>)</h1>

		<div class="tabs tab-size-15 noPrint">
			<ul id="requests_sections">
				<li id="m_wait" sgid="wait" class="<?php if ($this->_vars['status'] == 'wait'): ?>active<?php endif; ?>"><a href="<?php echo $this->_vars['site_url']; ?>
listings/requests/wait"><?php echo l('booking_status_wait', 'listings', '', 'text', array()); ?> (<span id="section_wait"><?php echo $this->_vars['requests_count']['wait']; ?>
</span>)</a></li>
				<li id="m_approved" sgid="approve" class="<?php if ($this->_vars['status'] == 'approve'): ?>active<?php endif; ?>"><a href="<?php echo $this->_vars['site_url']; ?>
listings/requests/approve"><?php echo l('booking_status_approved', 'listings', '', 'text', array()); ?> (<span id="section_approve"><?php echo $this->_vars['requests_count']['approve']; ?>
</span>)</a></li>
				<li id="m_declined" sgid="decline" class="<?php if ($this->_vars['status'] == 'decline'): ?>active<?php endif; ?>"><a href="<?php echo $this->_vars['site_url']; ?>
listings/requests/decline"><?php echo l('booking_status_declined', 'listings', '', 'text', array()); ?> (<span id="section_decline"><?php echo $this->_vars['requests_count']['decline']; ?>
</span>)</a></li>
			</ul>
		</div>
		
		<div id="requests_block"><?php echo $this->_vars['block']; ?>
</div>
		
		<?php echo tpl_function_js(array('module' => listings,'file' => 'listings-order.js'), $this);?>
		<?php echo tpl_function_js(array('module' => listings,'file' => 'listings-list.js'), $this);?>
		<script><?php echo '
			var requests;
			$(function(){
				new listingsList({
					siteUrl: \'';  echo $this->_vars['site_url'];  echo '\',
					listAjaxUrl: \''; ?>
listings/ajax_requests<?php echo '\',
					sectionId: \'requests_sections\',
					listBlockId: \'requests_block\',
					operationType: \'';  echo $this->_vars['status'];  echo '\',
					order: \'';  echo $this->_vars['order'];  echo '\',
					orderDirection: \'';  echo $this->_vars['order_direction'];  echo '\',
					page: ';  echo $this->_vars['page_data']['cur_page'];  echo ',
					tIds: [\'pages_block_1\', \'pages_block_2\', \'sorter_block\'],
				});
				requests = new listingsOrder({
					siteUrl: \'';  echo $this->_vars['site_url'];  echo '\',
					blockId: \'requests_block\',
					sectionId: \'requests_sections\',
					waitCnt: ';  echo $this->_vars['requests_count']['wait'];  echo ',
					approveCnt: ';  echo $this->_vars['requests_count']['approve'];  echo ',
					declineCnt: ';  echo $this->_vars['requests_count']['decline'];  echo ',
				});
			});
		'; ?>
</script>
	</div>
</div>
<div class="clr"></div>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
