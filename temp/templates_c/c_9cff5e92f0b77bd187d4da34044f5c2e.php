<?php require_once('/opt/ip/system/libraries/template_lite/plugins/function.ld.php'); $this->register_function("ld", "tpl_function_ld");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-02 14:30:52 KRAT */ ?>

	<?php echo tpl_function_ld(array('i' => 'user_type','gid' => 'users','assign' => 'user_types'), $this);?>
	<table cellspacing="0" cellpadding="0" class="data" width="100%">
	<tr>
		<th class="first"><?php echo l('stat_header_users', 'users', '', 'text', array()); ?></th>
		<?php if (is_array($this->_vars['user_types']['option']) and count((array)$this->_vars['user_types']['option'])): foreach ((array)$this->_vars['user_types']['option'] as $this->_vars['item']): ?>
		<th class="center"><?php echo $this->_vars['item']; ?>
</th>
		<?php endforeach; endif; ?>
	</tr>
	<?php if ($this->_vars['stat_users']['index_method']): ?>
	<tr>
		<td class="first w150"><a href="<?php echo $this->_vars['site_url']; ?>
admin/users/index/"><?php echo l('stat_header_all', 'users', '', 'text', array()); ?></a></td>
		<?php if (is_array($this->_vars['user_types']['option']) and count((array)$this->_vars['user_types']['option'])): foreach ((array)$this->_vars['user_types']['option'] as $this->_vars['key'] => $this->_vars['item']): ?>
		<td class="w150 center"><a href="<?php echo $this->_vars['site_url']; ?>
admin/users/index/<?php echo $this->_vars['key']; ?>
/"><?php echo $this->_vars['stat_users'][$this->_vars['key']]['all']; ?>
</a></td>
		<?php endforeach; endif; ?>
	</tr>
	<tr class="zebra">
		<td class="first w150"><a href="<?php echo $this->_vars['site_url']; ?>
admin/users/index/active"><?php echo l('stat_header_active', 'users', '', 'text', array()); ?></a></td>
		<?php if (is_array($this->_vars['user_types']['option']) and count((array)$this->_vars['user_types']['option'])): foreach ((array)$this->_vars['user_types']['option'] as $this->_vars['key'] => $this->_vars['item']): ?>
		<td class="w150 center"><a href="<?php echo $this->_vars['site_url']; ?>
admin/users/index/<?php echo $this->_vars['key']; ?>
/active"><?php echo $this->_vars['stat_users'][$this->_vars['key']]['active']; ?>
</a></td>
		<?php endforeach; endif; ?>
	</tr>
	<tr>
		<td class="first w150"><a href="<?php echo $this->_vars['site_url']; ?>
admin/users/index/not_active"><?php echo l('stat_header_blocked', 'users', '', 'text', array()); ?></a></td>
		<?php if (is_array($this->_vars['user_types']['option']) and count((array)$this->_vars['user_types']['option'])): foreach ((array)$this->_vars['user_types']['option'] as $this->_vars['key'] => $this->_vars['item']): ?>
		<td class="w150 center"><a href="<?php echo $this->_vars['site_url']; ?>
admin/users/index/<?php echo $this->_vars['key']; ?>
/not_active"><?php echo $this->_vars['stat_users'][$this->_vars['key']]['blocked']; ?>
</a></td>
		<?php endforeach; endif; ?>
	</tr>
	<tr class="zebra">
		<td class="first w150"><a href="<?php echo $this->_vars['site_url']; ?>
admin/users/index/not_confirm"><?php echo l('stat_header_unconfirmed', 'users', '', 'text', array()); ?></a></td>
		<?php if (is_array($this->_vars['user_types']['option']) and count((array)$this->_vars['user_types']['option'])): foreach ((array)$this->_vars['user_types']['option'] as $this->_vars['key'] => $this->_vars['item']): ?>
		<td class="w150 center"><a href="<?php echo $this->_vars['site_url']; ?>
admin/users/index/<?php echo $this->_vars['key']; ?>
/not_confirm"><?php echo $this->_vars['stat_users'][$this->_vars['key']]['unconfirm']; ?>
</a></td>
		<?php endforeach; endif; ?>
	</tr>
	<?php endif; ?>
	</table>
	<br>
