<?php require_once('/opt/ip/system/libraries/template_lite/plugins/modifier.date_format.php'); $this->register_modifier("date_format", "tpl_modifier_date_format");  require_once('/opt/ip/system/libraries/template_lite/plugins/modifier.truncate.php'); $this->register_modifier("truncate", "tpl_modifier_truncate");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.seolink.php'); $this->register_function("seolink", "tpl_function_seolink");  require_once('/opt/ip/system/libraries/template_lite/plugins/block.strip.php'); $this->register_block("strip", "tpl_block_strip");  require_once('/opt/ip/system/libraries/template_lite/plugins/block.capture.php'); $this->register_block("capture", "tpl_block_capture");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.pagination.php'); $this->register_function("pagination", "tpl_function_pagination");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.sorter.php'); $this->register_function("sorter", "tpl_function_sorter");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-01 06:14:30 KRAT */ ?>



<?php if ($this->_vars['reviews']): ?>
<div class="sorter line" id="sorter_block">
	<?php echo tpl_function_sorter(array('links' => $this->_vars['sort_data']['links'],'order' => $this->_vars['sort_data']['order'],'direction' => $this->_vars['sort_data']['direction'],'url' => $this->_vars['sort_data']['url']), $this);?>
	<div class="fright" id="pages_block_1"><?php echo tpl_function_pagination(array('data' => $this->_vars['page_data'],'type' => 'cute'), $this);?></div>
</div>
<?php endif; ?>

<div>
<?php if (is_array($this->_vars['reviews']) and count((array)$this->_vars['reviews'])): foreach ((array)$this->_vars['reviews'] as $this->_vars['item']): ?>
	<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'poster_view_link')); tpl_block_capture(array('assign' => 'poster_view_link'), null, $this); ob_start();  $this->_tag_stack[] = array('tpl_block_strip', array()); tpl_block_strip(array(), null, $this); ob_start(); ?>
		<?php echo tpl_function_seolink(array('module' => 'users','method' => 'view','data' => $this->_vars['item']['poster']), $this);?>
	<?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_strip($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack);  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
	<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'responder_view_link')); tpl_block_capture(array('assign' => 'responder_view_link'), null, $this); ob_start();  $this->_tag_stack[] = array('tpl_block_strip', array()); tpl_block_strip(array(), null, $this); ob_start(); ?>
		<?php echo tpl_function_seolink(array('module' => 'users','method' => 'view','data' => $this->_vars['item']['responder']), $this);?>
	<?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_strip($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack);  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
	<?php if ($this->_vars['item']['id_poster'] == $this->_vars['user_id']):  $this->assign('is_review_owner', '1');  else:  $this->assign('is_review_owner', '0');  endif; ?>
	<?php if ($this->_vars['item']['banned']): ?>
	<div class="banned">
		<a class="fright btn-link" id="item-show-banned-<?php echo $this->_vars['item']['id']; ?>
" title="<?php echo l('link_content_show', 'reviews', '', 'text', array()); ?>"><ins class="with-icon-small g i-expand"></ins></a>
		<a class="fright btn-link hide" id="item-hide-banned-<?php echo $this->_vars['item']['id']; ?>
" title="<?php echo l('link_content_hide', 'reviews', '', 'text', array()); ?>"><ins class="with-icon-small g i-collapse"></ins></a>
		<p><?php echo l('text_content_banned', 'reviews', '', 'text', array()); ?></p>
	</div>
	<?php endif; ?>
	<div id="item-block-<?php echo $this->_vars['item']['id']; ?>
" class="item <?php if ($this->_vars['item']['banned']): ?>hide<?php endif; ?>">
		<div class="review-actions">
			<?php if (! $this->_vars['item']['banned']):  echo tpl_function_block(array('name' => 'mark_as_spam_block','module' => 'spam','object_id' => $this->_vars['item']['id'],'type_gid' => 'reviews_object','template' => 'link','is_owner' => $this->_vars['is_review_owner']), $this); endif; ?>
			<?php if (! $this->_vars['item']['answer'] && $this->_vars['user_id'] == $this->_vars['item']['id_responder']): ?><a href="<?php echo $this->_vars['site_url']; ?>
reviews/reply/<?php echo $this->_vars['type']['gid']; ?>
/<?php echo $this->_vars['object_id']; ?>
" id="reply-btn-<?php echo $this->_vars['item']['id']; ?>
" class="btn-link link-r-margin"><?php echo l('btn_send_reply', 'reviews', '', 'button', array()); ?></a><?php endif; ?>
		</div>
		<h3><a href="<?php echo $this->_vars['poster_view_link']; ?>
"><?php echo $this->_run_modifier($this->_vars['item']['poster']['output_name'], 'truncate', 'plugin', 1, 50); ?>
</a>, <?php echo $this->_run_modifier($this->_vars['item']['date_add'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']); ?>
</h3>
		<div class="image">
			<a href="<?php echo $this->_vars['poster_view_link']; ?>
"><img src="<?php echo $this->_vars['item']['poster']['media']['user_logo']['thumbs']['middle']; ?>
" title="<?php echo $this->_vars['item']['poster']['output_name']; ?>
"></a>
		</div>
		<div class="body">
			<?php 
$this->assign('no_info_str', l('no_information', 'start', '', 'text', array()));
 ?>
			<?php $this->assign('rate_type', $this->_vars['item']['type']['rate_type']); ?>
			<?php $this->assign('rate_score', $this->_vars['item']['rating_data']['main']); ?>
			<div class="t-1">
				<?php echo tpl_function_block(array('name' => get_rate_block,'module' => reviews,'rating_data' => $this->_vars['item']['rating_data'],'type_gid' => $this->_vars['item']['gid_type'],'template' => 'mini-extended','read_only' => "true",'show_label' => "true"), $this);?>
			</div>
			<div class="t-2"><?php echo $this->_vars['item']['message']; ?>
</div>
		</div>
		<div class="clr"></div>
		<div class="<?php if (! $this->_vars['item']['answer']): ?>hide<?php endif; ?> reply" id="reply-block-<?php echo $this->_vars['item']['id']; ?>
">
			<h3><a href="<?php echo $this->_vars['responder_view_link']; ?>
"><?php echo $this->_run_modifier($this->_vars['item']['responder']['output_name'], 'truncate', 'plugin', 1, 50); ?>
</a>, <?php if ($this->_run_modifier($this->_vars['item']['date_answer'], 'strtotime', 'PHP', 1) > 0):  echo $this->_run_modifier($this->_vars['item']['date_answer'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']);  endif; ?></h3>
			<div class="image">
				<a href="<?php echo $this->_vars['responder_view_link']; ?>
"><img src="<?php echo $this->_vars['item']['responder']['media']['user_logo']['thumbs']['middle']; ?>
" title="<?php echo $this->_vars['item']['responder']['output_name']; ?>
"></a>
			</div>
			<div class="body">
				<div class="t-1"></div>
				<div class="t-2" id="reply-text-<?php echo $this->_vars['item']['id']; ?>
"><?php echo $this->_vars['item']['answer']; ?>
</div>
			</div>
		</div>
		<div class="clr"></div>
		<?php if (! $this->_vars['item']['answer'] && $this->_vars['user_id'] == $this->_vars['item']['id_responder']): ?>
		<div class="edit_block">
		<form action="" method="post" id="reply-form-<?php echo $this->_vars['item']['id']; ?>
" class="hide">
			<div class="r">
				<div class="f"><?php echo l('field_reviews_answer', 'reviews', '', 'text', array()); ?>:&nbsp;* </div>
				<div class="v"><textarea name="data[answer]" rows="5" cols="8"></textarea></div>
			</div>
			<div class="r">
				<div class="f">&nbsp;</div>
				<div class="v"><input type="submit" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>" name="btn_save"></div>
			</div>
		</form>
		</div>
		<script><?php echo '
			$(function(){
				var errorObj = new Errors();
				$(\'#reply-form-';  echo $this->_vars['item']['id'];  echo '\').bind(\'submit\', function(){
					var el = $(this);
					var data = $(this).serialize();
					$.ajax({
						url: \'';  echo $this->_vars['site_url'];  echo 'reviews/ajax_reply/';  echo $this->_vars['item']['id'];  echo '\', 
						type: \'POST\',
						cache: false,
						data: data,
						dataType: \'json\',
						success: function(data){
							if(data.success){
								el.hide();
								var block = $(\'#reply-block-';  echo $this->_vars['item']['id'];  echo '\'); 
								var h3 = block.find(\'h3\');
								var html = h3.html() + data.date_answer;
								h3.html(html);
								$(\'#reply-text-';  echo $this->_vars['item']['id'];  echo '\').html(data.answer);
								block.show();
								$(\'#reply-btn-';  echo $this->_vars['item']['id'];  echo '\').hide();
								errorObj.show_error_block(data.success, \'success\');
							}else{
								errorObj.show_error_block(data.error, \'error\');
							}
							return false;
						}
					});
					return false;
				});
				$(\'#reply-btn-';  echo $this->_vars['item']['id'];  echo '\').bind(\'click\', function(){
					$(\'#reply-form-';  echo $this->_vars['item']['id'];  echo '\').show();
					return false;
				});
			});
		'; ?>
</script>
		<?php endif; ?>
	</div>	
	<?php if ($this->_vars['item']['banned']): ?>
	<script><?php echo '
		$(\'#item-show-banned-';  echo $this->_vars['item']['id'];  echo '\').bind(\'click\', function(){
			$(this).hide();
			$(\'#item-hide-banned-';  echo $this->_vars['item']['id'];  echo '\').css(\'display\', \'block\');
			$(this).parent().find(\'p\').hide();
			$(\'#item-block-';  echo $this->_vars['item']['id'];  echo '\').show();
		});
		$(\'#item-hide-banned-';  echo $this->_vars['item']['id'];  echo '\').bind(\'click\', function(){
			$(this).hide();
			$(\'#item-show-banned-';  echo $this->_vars['item']['id'];  echo '\').show();
			$(this).parent().find(\'p\').show();
			$(\'#item-block-';  echo $this->_vars['item']['id'];  echo '\').hide();
		});
	'; ?>
</script>
	<?php endif;  endforeach; else: ?>
	<div class="item empty"><?php echo l('no_reviews', 'reviews', '', 'text', array()); ?></div>
<?php endif; ?>
</div>
<?php if ($this->_vars['reviews']): ?>
<div id="pages_block_2"><?php echo tpl_function_pagination(array('data' => $this->_vars['page_data'],'type' => 'full'), $this);?></div>
<?php endif; ?>
