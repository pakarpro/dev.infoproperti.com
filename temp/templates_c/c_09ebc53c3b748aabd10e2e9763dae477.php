<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.date_format.php'); $this->register_modifier("date_format", "tpl_modifier_date_format");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.escape.php'); $this->register_modifier("escape", "tpl_modifier_escape");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.truncate.php'); $this->register_modifier("truncate", "tpl_modifier_truncate");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.seolink.php'); $this->register_function("seolink", "tpl_function_seolink");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.block.php'); $this->register_function("block", "tpl_function_block");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/block.capture.php'); $this->register_block("capture", "tpl_block_capture");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.lower.php'); $this->register_modifier("lower", "tpl_modifier_lower");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.pagination.php'); $this->register_function("pagination", "tpl_function_pagination");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.sorter.php'); $this->register_function("sorter", "tpl_function_sorter");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-05-12 05:09:04 KRAT */ ?>

	
	<!-- my_block_list -->
	<?php if ($this->_vars['listings']): ?>
	<div class="sorter line" id="sorter_block">
		<?php echo tpl_function_sorter(array('links' => $this->_vars['sort_data']['links'],'order' => $this->_vars['sort_data']['order'],'direction' => $this->_vars['sort_data']['direction'],'url' => $this->_vars['sort_data']['url']), $this);?>
		<div class="fright" id="pages_block_1"><?php echo tpl_function_pagination(array('data' => $this->_vars['page_data'],'type' => 'cute'), $this);?></div>
	</div>
	
	<div id="table-my-list">
		<?php /* DISABLE FOR NOW
		<table class="list-heading">
			<tr id="sorter_block">
				<th class="w100"><?php echo l('field_photo', 'listings', '', 'text', array()); ?></th>		
				<th><a href="<?php echo $this->_vars['sort_links']['name']; ?>
" class="link-sorter"><?php echo l('field_name', 'listings', '', 'text', array());  if ($this->_vars['page_data']['order'] == 'name'): ?><ins class="fright i-sorter with-icon-small <?php echo $this->_run_modifier($this->_vars['page_data']['order_direction'], 'lower', 'plugin', 1); ?>
"></ins><?php endif; ?></a></th>		
				<th><a href="<?php echo $this->_vars['sort_links']['date_modified']; ?>
" class="link-sorter"><?php echo l('field_date_modified', 'listings', '', 'text', array());  if ($this->_vars['page_data']['order'] == 'date_modified'): ?><ins class="fright i-sorter with-icon-small <?php echo $this->_run_modifier($this->_vars['page_data']['order_direction'], 'lower', 'plugin', 1); ?>
"></ins><?php endif; ?></a></th>		
				<th class="w30"><?php echo l('field_views', 'listings', '', 'text', array()); ?></th>
				<th class="w100"><?php echo l('field_search_status', 'listings', '', 'text', array()); ?></th>		
			</tr>
		</table>
		*/ ?>
		
		<?php if (is_array($this->_vars['listings']) and count((array)$this->_vars['listings'])): foreach ((array)$this->_vars['listings'] as $this->_vars['item']): ?>
		<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'property_output')); tpl_block_capture(array('assign' => 'property_output'), null, $this); ob_start();  echo $this->_vars['item']['property_type_str']; ?>
 <?php echo $this->_vars['item']['operation_type_str'];  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
		<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'price_output')); tpl_block_capture(array('assign' => 'price_output'), null, $this); ob_start(); ?>	
			<?php echo tpl_function_block(array('name' => listing_price_block,'module' => 'listings','data' => $this->_vars['item'],'template' => 'small'), $this);?>
		<?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
		<div class="my-list-content clearfix">
			<!-- image / photo -->
			<a class="photo-link" href="<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'view','data' => $this->_vars['item']), $this);?>" title="<?php echo l('btn_preview', 'start', '', 'button', array()); ?>">
				<img src="<?php echo $this->_vars['item']['media']['photo']['thumbs']['small']; ?>
" alt="<?php echo $this->_run_modifier($this->_run_modifier($this->_vars['item']['output_name'], 'truncate', 'plugin', 1, 30), 'escape', 'plugin', 1); ?>
" title="<?php echo $this->_run_modifier($this->_vars['item']['output_name'], 'escape', 'plugin', 1); ?>
">
			</a>
			<div class="prop-detail clearfix">
				<!-- location and details -->
				<p><?php echo $this->_run_modifier($this->_vars['item']['output_name'], 'truncate', 'plugin', 1, 40); ?>
<br/><?php echo $this->_run_modifier($this->_vars['property_output'], 'truncate', 'plugin', 1, 40); ?>
<br /><?php echo $this->_run_modifier($this->_vars['price_output'], 'truncate', 'plugin', 1, 70); ?>
<br /><br />
					<!-- list updated -->
					<span class="list-updated"><?php echo l('field_date_modified', 'listings', '', 'text', array()); ?>:&nbsp;<?php echo $this->_run_modifier($this->_vars['item']['date_modified'], 'date_format', 'plugin', 1); ?>
</span>&nbsp;|&nbsp;
					<!-- number of view -->
					<span class="number-view"><?php echo l('field_views', 'listings', '', 'text', array()); ?>:&nbsp;<?php echo $this->_vars['item']['views']; ?>
</span>
				</p>
				
				<!-- list status -->
				<div class="list-status">
					<?php if ($this->_vars['item']['status']): ?>
						<span class="status active"><ins class="btn-link"></ins><?php echo l('active_listing', 'listings', '', 'text', array()); ?></span><br>
					<?php else: ?>
						<span class="status inactive"><ins class="btn-link"></ins><?php echo l('inactive_listing', 'listings', '', 'text', array()); ?></span><br>
					<?php endif; ?>
					
					<?php if ($this->_vars['item']['moderation_status'] == 'default' && $this->_vars['item']['initial_moderation']): ?><span class="status"><ins class="btn-link ins"></ins><?php echo l('listing_status_default', 'listings', '', 'text', array()); ?></span>
					<?php elseif ($this->_vars['item']['moderation_status'] == 'decline'): ?><span class="status decline"><ins class="btn-link"></ins><?php echo l('listing_status_decline', 'listings', '', 'text', array()); ?></span>
					<?php elseif ($this->_vars['item']['moderation_status'] == 'approved' || $this->_vars['item']['moderation_status'] == 'default' && ! $this->_vars['item']['initial_moderation']): ?><span class="status approved"><ins class="btn-link"></ins><?php echo l('listing_status_approved', 'listings', '', 'text', array()); ?></span>
					<?php elseif ($this->_vars['item']['moderation_status'] == 'wait'): ?><span class="status wait"><ins class="btn-link"></ins><?php echo l('listing_status_wait', 'listings', '', 'text', array()); ?></span>
					<?php endif; ?>
				</div>
			</div>
			<!-- button act -->
			<div class="my-list-act clearfix">
				<a href="<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'view','data' => $this->_vars['item']), $this);?>" class="btn-link act-icon i-eye" title="<?php echo l('btn_preview', 'start', '', 'button', array()); ?>"><ins class="with-icon i-eye"></ins></a>
				<a href="<?php echo $this->_vars['site_url']; ?>
listings/edit/<?php echo $this->_vars['item']['id']; ?>
" class="btn-link act-icon i-edit" title="<?php echo l('btn_edit', 'start', '', 'button', array()); ?>"><ins class="with-icon i-edit"></ins></a>
				<?php if ($this->_vars['item']['operation_type'] == 'rent'): ?><a href="<?php echo $this->_vars['site_url']; ?>
listings/edit/<?php echo $this->_vars['item']['id']; ?>
/calendar/2" class="btn-link act-icon i-calendar" title="<?php echo l('link_calendar_view', 'listings', '', 'button', array()); ?>"><ins class="with-icon i-calendar"></ins></a><?php endif; ?>
				<?php if ($this->_vars['item']['operation_type'] != 'buy' && $this->_vars['item']['operation_type'] != 'lease' && $this->_vars['item']['status']): ?><a href="<?php echo $this->_vars['site_url']; ?>
listings/services/<?php echo $this->_vars['item']['id']; ?>
" class="btn-link act-icon i-dollar" title="<?php echo l('link_services', 'listings', '', 'button', array()); ?>"><ins class="with-icon i-dollar"></ins></a><?php endif; ?>				<a href="<?php echo $this->_vars['site_url']; ?>
listings/delete/<?php echo $this->_vars['item']['id']; ?>
" onclick="javascript: if(!confirm('<?php echo l('note_delete_listing', 'listings', '', 'js', array()); ?>')) return false;" class="btn-link act-icon i-delete" title="<?php echo l('btn_delete', 'start', '', 'button', array()); ?>"><ins class="with-icon i-delete"></ins></a>
			</div>
		</div>
		
		<?php endforeach; endif; ?>
	</div>
	<div id="pages_block_2"><?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "pagination.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?></div>
	<?php else: ?>
	<div class="item empty"><?php echo l('no_listings', 'listings', '', 'text', array()); ?></div>
	<?php endif; ?>
	
	<script><?php echo '
	$(function(){
		$(\'#total_rows\').html(\'';  echo $this->_vars['page_data']['total_rows'];  echo '\');
	});
	'; ?>
</script>
