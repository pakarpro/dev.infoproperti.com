<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-02 14:30:54 KRAT */ ?>

	<table cellspacing="0" cellpadding="0" class="data" width="100%">
	<tr>
		<th class="first" colspan="2"><?php echo l('stat_header_listings', 'listings', '', 'text', array()); ?></th>
	</tr>
	<?php if ($this->_vars['stat_listings']['index_method']): ?>
	<tr>
		<td class="first"><a href="<?php echo $this->_vars['site_url']; ?>
admin/listings/index/"><?php echo l('stat_header_all', 'listings', '', 'text', array()); ?></a></td>
		<td class="w50 center"><a href="<?php echo $this->_vars['site_url']; ?>
admin/listings/index/"><?php echo $this->_vars['stat_listings']['all']; ?>
</a></td>
	</tr>
	<tr class="zebra">
		<td class="first"><a href="<?php echo $this->_vars['site_url']; ?>
admin/listings/active/"><?php echo l('stat_header_active', 'listings', '', 'text', array()); ?></a></td>
		<td class="w50 center"><a href="<?php echo $this->_vars['site_url']; ?>
admin/listings/active/"><?php echo $this->_vars['stat_listings']['active']; ?>
</a></td>
	</tr>
	<tr>
		<td class="first"><a href="<?php echo $this->_vars['site_url']; ?>
admin/listings/not_active/"><?php echo l('stat_header_inactive', 'listings', '', 'text', array()); ?></a></td>
		<td class="w50 center"><a href="<?php echo $this->_vars['site_url']; ?>
admin/listings/not_active/"><?php echo $this->_vars['stat_listings']['inactive']; ?>
</a></td>
	</tr>
	<?php endif; ?>
	<tr class="zebra">
		<td class="first"><a href="<?php echo $this->_vars['site_url']; ?>
admin/listings/moderation/"><?php echo l('stat_header_moderate', 'listings', '', 'text', array()); ?></a></td>
		<td class="w50 center"><a href="<?php echo $this->_vars['site_url']; ?>
admin/listings/moderation/"><?php echo $this->_vars['stat_listings']['moderate']; ?>
</a></td>
	</tr>
	</table>
