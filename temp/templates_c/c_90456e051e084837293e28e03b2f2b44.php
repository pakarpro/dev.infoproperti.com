<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-02 15:54:36 KRAT */ ?>

<div class="tabs tab-size-15">
	<ul id="top_listing_sections">
		<?php if (is_array($this->_vars['display_sections']['top']) and count((array)$this->_vars['display_sections']['top'])): foreach ((array)$this->_vars['display_sections']['top'] as $this->_vars['sgid'] => $this->_vars['item']): ?>
		<?php if ($this->_vars['item']): ?><li id="m_<?php echo $this->_vars['sgid']; ?>
" sgid="<?php echo $this->_vars['sgid']; ?>
" class="<?php if ($this->_vars['sgid'] == $this->_vars['top_section_gid']): ?>active<?php else: ?>noPrint<?php endif; ?>"><a href="<?php echo $this->_vars['site_url']; ?>
listings/view/<?php echo $this->_vars['listing']['id']; ?>
/<?php echo $this->_vars['sgid']; ?>
"><?php echo l('filter_section_'.$this->_vars['sgid'], 'listings', '', 'text', array()); ?></a></li><?php endif; ?>
		<?php endforeach; endif; ?>
	</ul>
</div>
