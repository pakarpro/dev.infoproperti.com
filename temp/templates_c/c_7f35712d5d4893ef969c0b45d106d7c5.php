<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-02 15:51:01 WIB */ ?>

<?php if ($this->_vars['listings']): ?>
	<div id="container-home-featured-prop">
		<div class="slider-container featured-property">
			<h2 class="head-title"><?php echo l('header_'.$this->_vars['type'].'_listings', 'listings', '', 'text', array()); ?></h2>

			<ul class="bxslider clearfix">
				<!-- DISABLE <div class="<?php echo $this->_run_modifier($this->_vars['type'], 'escape', 'plugin', 1); ?>
_listings_block">-->
					<?php if (is_array($this->_vars['listings']) and count((array)$this->_vars['listings'])): foreach ((array)$this->_vars['listings'] as $this->_vars['key'] => $this->_vars['item']): ?>
					<li class="listing <?php echo $this->_run_modifier($this->_vars['photo_size'], 'escape', 'plugin', 1); ?>
"><!-- DISABLE <div class="listing <?php echo $this->_run_modifier($this->_vars['photo_size'], 'escape', 'plugin', 1); ?>
">-->
							<a href="<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'view','data' => $this->_vars['item']), $this);?>">
								<img src="<?php echo $this->_vars['item']['media']['photo']['thumbs'][$this->_vars['photo_size']]; ?>
" alt="<?php echo $this->_run_modifier($this->_vars['item']['output_name'], 'escape', 'plugin', 1); ?>
" title="<?php echo $this->_run_modifier($this->_vars['item']['output_name'], 'escape', 'plugin', 1); ?>
">
								<?php if ($this->_vars['item']['photo_count'] || $this->_vars['item']['is_vtour']): ?>
								<div class="photo-info">
									<div class="panel">
										<?php if ($this->_vars['item']['photo_count']): ?><span class="btn-link"><ins class="with-icon-small w i-photo"></ins> <?php echo $this->_vars['item']['photo_count']; ?>
</span><?php endif; ?>
										<?php if ($this->_vars['item']['is_vtour']): ?><span class="btn-link"><ins class="with-icon-small w i-vtour"></ins> 360&deg;</span><?php endif; ?>
									</div>
									<div class="background"></div>
								</div>
								<?php endif; ?>
							</a>
							<a href="<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'view','data' => $this->_vars['item']), $this);?>"><?php if ($this->_vars['photo_size'] == 'big'):  echo $this->_run_modifier($this->_vars['item']['output_name'], 'truncate', 'plugin', 1, 35);  else:  echo $this->_run_modifier($this->_vars['item']['output_name'], 'truncate', 'plugin', 1, 30);  endif; ?></a>		
							<span><?php echo $this->_vars['item']['property_type_str']; ?>
 <?php echo $this->_vars['item']['operation_type_str']; ?>
</span>
							<span><?php echo tpl_function_block(array('name' => listing_price_block,'module' => 'listings','data' => $this->_vars['item'],'template' => 'small'), $this);?></span>
						<!-- DISABLE </div>-->
					</li>
					<?php endforeach; endif; ?>
				<!-- DISABLE </div> -->
			</ul>
			
			<div id="finance">
				<div id="slidercaption-two"></div>
				<h2 class="finance-tool-btn">Alat Bantu Finansial</h2>
				<div id="fin-tools">
					<ul class="ftools clearfix">
						<li>
							<a href="#">Coming Soon</a>
							<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
						</li>
						<li>
							<a href="#">Coming Soon</a>
							<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
						</li>
						<li>
							<a href="#">Coming Soon</a>
							<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
						</li>
					</ul>
					<button class="close-btn">[x] Close</button>
				</div>
			</div>
		</div>
	</div>

<script><?php echo '
$(document).ready(function () {
   $(\'.bxslider\').bxSlider({
	   auto: false,
	   autoHover:true,
	   touchEnabled: false,
	   autoControls: false,
	   pager: true,
	   minSlides: 4,
	   maxSlides: 4,
	   slideWidth: 220,
	   startSlide: 0
	});
});

$(document).ready(function () {
   $(\'.ftools\').bxSlider({
	   auto: false,
	   touchEnabled: false,
	   autoControls: false,
	   pager: true,
	   controls: false,
	   infiniteLoop: true,
	   captions: false,
	   startSlide: 0
	});
});
'; ?>
</script>
	
	
<?php endif; ?>