<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.truncate.php'); $this->register_modifier("truncate", "tpl_modifier_truncate");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2014-08-28 10:22:26 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

<form method="post" action="<?php echo $this->_vars['data']['action']; ?>
" name="save_form" enctype="multipart/form-data">
	<div class="edit-form n150">
		<div class="row header"><?php echo l('admin_header_data_form_upload', 'import', '', 'text', array()); ?></div>
		<div class="row">
			<div class="h"><?php echo l("field_data_driver", 'import', '', 'text', array()); ?>:&nbsp;* </div>
			<div class="v">
				<select name="data[gid_driver]" id="driver_select">
					<?php if (is_array($this->_vars['drivers']) and count((array)$this->_vars['drivers'])): foreach ((array)$this->_vars['drivers'] as $this->_vars['item']): ?>
					<option value="<?php echo $this->_vars['item']['gid']; ?>
" <?php if ($this->_vars['item']['gid'] == $this->_vars['data']['gid_driver']): ?>selected<?php endif; ?>><?php echo $this->_run_modifier($this->_vars['item']['output_name'], 'truncate', 'plugin', 1, 100); ?>
</option>
					<?php endforeach; else: ?>
					<option value="">-</option>
					<?php endif; ?>
				</select>				
			</div>
		</div>
		<div class="row">
			<div class="h"><?php echo l("field_data_module", 'import', '', 'text', array()); ?>:&nbsp;* </div>
			<div class="v">
				<select name="data[id_object]">
					<?php if (is_array($this->_vars['modules']) and count((array)$this->_vars['modules'])): foreach ((array)$this->_vars['modules'] as $this->_vars['item']): ?>
					<option value="<?php echo $this->_vars['item']['id']; ?>
" <?php if ($this->_vars['item']['id'] == $this->_vars['data']['id_object']): ?>selected<?php endif; ?>><?php echo $this->_run_modifier($this->_vars['item']['output_name'], 'truncate', 'plugin', 1, 100); ?>
</option>
					<?php endforeach; else: ?>
					<option value="">-</option>
					<?php endif; ?>
				</select>
			</div>
		</div>
		<div id="driver_box"><?php echo $this->_vars['settings']; ?>
</div>
		<div class="row">
			<div class="h"><?php echo l('field_data_file', 'import', '', 'text', array()); ?>:&nbsp;* </div>
			<div class="v"><input type="file" name="import_file"></div>
		</div>
	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div></div>
	<a class="cancel" href="<?php echo $this->_vars['site_url']; ?>
admin/import"><?php echo l('btn_cancel', 'start', '', 'text', array()); ?></a>
</form>
<div class="clr"></div>
<script><?php echo '
	$(function(){
		$("div.row:odd").addClass("zebra");
		$(\'#driver_select\').bind(\'change\', function(){
			$.post(
				\'';  echo $this->_vars['site_url']; ?>
admin/import/ajax_upload_form<?php echo '\', 
				{driver_gid: $(this).val()},
				function(data){
					$(\'#driver_box\').html(data);
					$("div.row:odd").addClass("zebra");
				}
			);
		});
		
	});
'; ?>
</script>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
