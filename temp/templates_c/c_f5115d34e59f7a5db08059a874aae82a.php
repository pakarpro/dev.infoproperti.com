<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-02 15:55:12 KRAT */ ?>

<div class="view-user">
<?php 
$this->assign('no_info_str', l('no_information', 'users', '', 'text', array()));
 ?>
<?php 
$this->assign('not_access_str', l('not_access', 'users', '', 'text', array()));
 ?>
<?php 
$this->assign('buy_str', l('please_buy', 'users', '', 'text', array()));
 ?>
<?php if ($this->_vars['user']['user_type'] == 'agent' && $this->_vars['user']['agent_status']): ?>
    <div class="r">
		<div class="f"><?php echo l('company', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><a href="<?php echo tpl_function_seolink(array('module' => 'users','method' => 'view','data' => $this->_vars['user']['company']), $this);?>"><?php echo $this->_vars['user']['company']['output_name']; ?>
</a></div>
    </div>
<?php endif; ?>

<div class="r">
    <div class="f"><?php echo l('field_facebook', 'users', '', 'text', array()); ?>: </div>
    <div class="v">
		<?php if ($this->_vars['user']['no_access_contact']): ?><font class="gray_italic"><?php echo $this->_vars['not_access_str']; ?>
</font>
		<?php elseif (! $this->_vars['user']['facebook']): ?><font class="gray_italic"><?php echo $this->_vars['no_info_str']; ?>
</font>
		<?php elseif (! $this->_vars['user']['is_contact']): ?><font class="gray_italic"><?php echo $this->_vars['buy_str']; ?>
</font>
		<?php else:  echo $this->_vars['user']['facebook']; ?>

		<?php endif; ?>
    </div>
</div>
<div class="r">
    <div class="f"><?php echo l('field_twitter', 'users', '', 'text', array()); ?>: </div>
    <div class="v">
		<?php if ($this->_vars['user']['no_access_contact']): ?><font class="gray_italic"><?php echo $this->_vars['not_access_str']; ?>
</font>
		<?php elseif (! $this->_vars['user']['twitter']): ?><font class="gray_italic"><?php echo $this->_vars['no_info_str']; ?>
</font>
		<?php elseif (! $this->_vars['user']['is_contact']): ?><font class="gray_italic"><?php echo $this->_vars['buy_str']; ?>
</font>
		<?php else:  echo $this->_vars['user']['twitter']; ?>

		<?php endif; ?>
    </div>
</div>

<?php /* disable for now
<div class="r">
    <div class="f"> </div>
    <div class="v">
		<?php if ($this->_vars['user']['no_access_contact']): ?><font class="gray_italic"></font>
		<?php elseif (! $this->_vars['user']['vkontakte']): ?><font class="gray_italic"></font>
		<?php elseif (! $this->_vars['user']['is_contact']): ?><font class="gray_italic"></font>
		<?php else: ?>
		<?php endif; ?>
    </div>
</div>
*/ ?>
	<?php switch($this->_vars['user']['user_type']): case 'private':  ?>
	<div class="r">
		<div class="f"><?php echo l('field_phone', 'users', '', 'text', array()); ?>:</div>
		<div class="v">
		   	<?php if ($this->_vars['user']['no_access_contact']): ?><font class="gray_italic"><?php echo $this->_vars['not_access_str']; ?>
</font>
		    <?php elseif (! $this->_vars['user']['contact_phone']):  echo $this->_vars['user']['phone']; ?>

		    <?php elseif (! $this->_vars['user']['is_contact']): ?><font class="gray_italic"><?php echo $this->_vars['buy_str']; ?>
</font>
		    <?php else:  echo $this->_vars['user']['contact_phone']; ?>

		    <?php endif; ?>
		</div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_email', 'users', '', 'text', array()); ?>:</div>
		<div class="v">
		    <?php if ($this->_vars['user']['no_access_contact']): ?><font class="gray_italic"><?php echo $this->_vars['not_access_str']; ?>
</font>
		    <?php elseif (! $this->_vars['user']['contact_email']): ?><font class="gray_italic"><?php echo $this->_vars['no_info_str']; ?>
</font>
		    <?php elseif (! $this->_vars['user']['is_contact']): ?><font class="gray_italic"><?php echo $this->_vars['buy_str']; ?>
</font>
		    <?php else:  echo $this->_vars['user']['contact_email']; ?>

		    <?php endif; ?>
		</div>
	</div>
    <?php /* movewd to view_block.tpl
    <div class="r">
        <div class="f"><?php echo l('field_description', 'users', '', 'text', array()); ?>: </div>
        <div class="v">
            <?php if ($this->_vars['user']['no_access_contact']): ?><font class="gray_italic"><?php echo $this->_vars['not_access_str']; ?>
</font>
            <?php elseif (! $this->_vars['user']['contact_info']): ?><font class="gray_italic"><?php echo $this->_vars['no_info_str']; ?>
</font>
            <?php elseif (! $this->_vars['user']['is_contact']): ?><font class="gray_italic"><?php echo $this->_vars['buy_str']; ?>
</font>
            <?php elseif ($this->_vars['template'] == 'small'):  echo $this->_run_modifier($this->_vars['user']['contact_info'], 'truncate', 'plugin', 1, 55); ?>

            <?php else:  echo $this->_vars['user']['contact_info']; ?>

            <?php endif; ?>
        </div>
	</div>
	*/ ?>

	<?php break; case 'company':  ?>
	<div class="r">
		<div class="f"><?php echo l('field_region', 'users', '', 'text', array()); ?>:</div>
		<div class="v">
		    <?php if ($this->_vars['user']['no_access_contact']): ?><font class="gray_italic"><?php echo $this->_vars['not_access_str']; ?>
</font>
		    <?php elseif (! $this->_vars['user']['location']): ?><font class="gray_italic"><?php echo $this->_vars['no_info_str']; ?>
</font>
		    <?php elseif (! $this->_vars['user']['is_contact']): ?><font class="gray_italic"><?php echo $this->_vars['buy_str']; ?>
</font>
		    <?php else:  echo $this->_vars['user']['location']; ?>

		    <?php endif; ?>
		</div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_phone', 'users', '', 'text', array()); ?>:</div>
		<div class="v">
		    <?php if ($this->_vars['user']['no_access_contact']): ?><font class="gray_italic"><?php echo $this->_vars['not_access_str']; ?>
</font>
		    <?php elseif (! $this->_vars['user']['contact_phone']):  echo $this->_vars['user']['phone']; ?>

		    <?php elseif (! $this->_vars['user']['is_contact']): ?><font class="gray_italic"><?php echo $this->_vars['buy_str']; ?>
</font>
		    <?php else:  echo $this->_vars['user']['contact_phone']; ?>

		    <?php endif; ?>
		</div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_email', 'users', '', 'text', array()); ?>:</div>
		<div class="v">
		    <?php if ($this->_vars['user']['no_access_contact']): ?><font class="gray_italic"><?php echo $this->_vars['not_access_str']; ?>
</font>
		    <?php elseif (! $this->_vars['user']['contact_email']): ?><font class="gray_italic"><?php echo $this->_vars['no_info_str']; ?>
</font>
		    <?php elseif (! $this->_vars['user']['is_contact']): ?><font class="gray_italic"><?php echo $this->_vars['buy_str']; ?>
</font>
		    <?php else:  echo $this->_vars['user']['contact_email']; ?>

		    <?php endif; ?>
		</div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_address', 'users', '', 'text', array()); ?>:</div>
		<div class="v">
		    <?php if ($this->_vars['user']['no_access_contact']): ?><font class="gray_italic"><?php echo $this->_vars['not_access_str']; ?>
</font>
		    <?php elseif (! $this->_vars['user']['address']): ?><font class="gray_italic"><?php echo $this->_vars['no_info_str']; ?>
</font>
		    <?php elseif (! $this->_vars['user']['is_contact']): ?><font class="gray_italic"><?php echo $this->_vars['buy_str']; ?>
</font>
		    <?php else:  echo $this->_vars['user']['address']; ?>

		    <?php endif; ?>
		</div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_postal_code', 'users', '', 'text', array()); ?>:</div>
		<div class="v">
		    <?php if ($this->_vars['user']['no_access_contact']): ?><font class="gray_italic"><?php echo $this->_vars['not_access_str']; ?>
</font>
		    <?php elseif (! $this->_vars['user']['postal_code']): ?><font class="gray_italic"><?php echo $this->_vars['no_info_str']; ?>
</font>
		    <?php elseif (! $this->_vars['user']['is_contact']): ?><font class="gray_italic"><?php echo $this->_vars['buy_str']; ?>
</font>
		    <?php else:  echo $this->_vars['user']['postal_code']; ?>

		    <?php endif; ?>
		</div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_web_url', 'users', '', 'text', array()); ?>:</div>
		<div class="v">
		    <?php if ($this->_vars['user']['no_access_contact']): ?><font class="gray_italic"><?php echo $this->_vars['not_access_str']; ?>
</font>
		    <?php elseif (! $this->_vars['user']['web_url']): ?><font class="gray_italic"><?php echo $this->_vars['no_info_str']; ?>
</font>
		    <?php elseif (! $this->_vars['user']['is_contact']): ?><font class="gray_italic"><?php echo $this->_vars['buy_str']; ?>
</font>
		    <?php else:  echo $this->_vars['user']['web_url']; ?>

		    <?php endif; ?>
		</div>
	</div>
	<div class="r">
	    <div class="f"><?php echo l('field_working_days', 'users', '', 'text', array()); ?>: </div>
	    <div class="v">
			<?php if ($this->_vars['user']['no_access_contact']): ?><font class="gray_italic"><?php echo $this->_vars['not_access_str']; ?>
</font>
		    <?php elseif (! $this->_vars['user']['working_days_str']): ?><font class="gray_italic"><?php echo $this->_vars['no_info_str']; ?>
</font>
		    <?php elseif (! $this->_vars['user']['is_contact']): ?><font class="gray_italic"><?php echo $this->_vars['buy_str']; ?>
</font>
		    <?php else:  echo $this->_vars['user']['working_days_str']; ?>

		    <?php endif; ?>
		</div>
	</div>
	<div class="r">
	    <div class="f"><?php echo l('field_working_hours', 'users', '', 'text', array()); ?>: </div>
	    <div class="v">
			<?php if ($this->_vars['user']['no_access_contact']): ?><font class="gray_italic"><?php echo $this->_vars['not_access_str']; ?>
</font>
		    <?php elseif (! $this->_vars['user']['working_hours_str']): ?><font class="gray_italic"><?php echo $this->_vars['no_info_str']; ?>
</font>
		    <?php elseif (! $this->_vars['user']['is_contact']): ?><font class="gray_italic"><?php echo $this->_vars['buy_str']; ?>
</font>
		    <?php else:  echo $this->_vars['user']['working_hours_str']; ?>

		    <?php endif; ?>
		</div>
	</div>
	<div class="r">
	    <div class="f"><?php echo l('field_lunch_time', 'users', '', 'text', array()); ?>: </div>
	    <div class="v">
			<?php if ($this->_vars['user']['no_access_contact']): ?><font class="gray_italic"><?php echo $this->_vars['not_access_str']; ?>
</font>
		    <?php elseif (! $this->_vars['user']['lunch_time_str']): ?><font class="gray_italic"><?php echo $this->_vars['no_info_str']; ?>
</font>
		    <?php elseif (! $this->_vars['user']['is_contact']): ?><font class="gray_italic"><?php echo $this->_vars['buy_str']; ?>
</font>
		    <?php else:  echo $this->_vars['user']['lunch_time_str']; ?>

		    <?php endif; ?>
		</div>
	</div>
	
	<?php break; case 'agent':  ?>
	<div class="r">
		<div class="f"><?php echo l('field_phone', 'users', '', 'text', array()); ?>:</div>
		<div class="v">
		    <?php if ($this->_vars['user']['no_access_contact']): ?><font class="gray_italic"><?php echo $this->_vars['not_access_str']; ?>
</font>
		    <?php elseif (! $this->_vars['user']['contact_phone']):  echo $this->_vars['user']['phone']; ?>

		    <?php elseif (! $this->_vars['user']['is_contact']): ?><font class="gray_italic"><?php echo $this->_vars['buy_str']; ?>
</font>
		    <?php else:  echo $this->_vars['user']['contact_phone']; ?>

		    <?php endif; ?>
		</div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_email', 'users', '', 'text', array()); ?>:</div>
		<div class="v">
		    <?php if ($this->_vars['user']['no_access_contact']): ?><font class="gray_italic"><?php echo $this->_vars['not_access_str']; ?>
</font>
		    <?php elseif (! $this->_vars['user']['contact_email']): ?><font class="gray_italic"><?php echo $this->_vars['no_info_str']; ?>
</font>
		    <?php elseif (! $this->_vars['user']['is_contact']): ?><font class="gray_italic"><?php echo $this->_vars['buy_str']; ?>
</font>
		    <?php else:  echo $this->_vars['user']['contact_email']; ?>

		    <?php endif; ?>
		</div>
	</div>
<?php break; endswitch; ?>

<?php if (! $this->_vars['user']['is_contact'] && ! $this->_vars['user']['no_access_contact']): ?>
<div class="buy-box"><input type="button" value="<?php echo l('btn_activate_contact', 'services', '', 'button', array()); ?>" name="contacts_btn"></div>
<?php endif; ?>

</div>
