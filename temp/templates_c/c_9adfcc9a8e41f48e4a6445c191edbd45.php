<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-05-12 13:55:31 KRAT */ ?>

	<?php if ($this->_vars['view_mode'] == 'map'): ?>
		<div class="listing-block">
			<div class="item no-hover no-border listing">
				<h3><a href="<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'view','data' => $this->_vars['listing']), $this);?>" title="<?php echo $this->_run_modifier($this->_vars['listing']['output_name'], 'escape', 'plugin', 1); ?>
"><?php echo $this->_run_modifier($this->_vars['listing']['output_name'], 'truncate', 'plugin', 1, 50); ?>
</a></h3>
				<div class="image">
					<a href="<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'view','data' => $this->_vars['listing']), $this);?>">
						<?php if ($this->_vars['listing']['is_slide_show']): ?>
							<img src="<?php echo $this->_vars['listing']['media']['photo']['thumbs']['middle_anim']; ?>
" title="<?php echo $this->_run_modifier($this->_vars['listing']['output_name'], 'escape', 'plugin', 1); ?>
">
						<?php else: ?>
							<img src="<?php echo $this->_vars['listing']['media']['photo']['thumbs']['middle']; ?>
" title="<?php echo $this->_run_modifier($this->_vars['listing']['output_name'], 'escape', 'plugin', 1); ?>
">
						<?php endif; ?>
						<?php if ($this->_vars['listing']['photo_count'] || $this->_vars['listing']['is_vtour']): ?>
						<div class="photo-info">
							<div class="panel">
								<?php if ($this->_vars['listing']['photo_count']): ?><span class="btn-link"><ins class="with-icon-small w i-photo"></ins> <?php echo $this->_vars['listing']['photo_count']; ?>
</span><?php endif; ?>
								<?php if ($this->_vars['listing']['is_vtour']): ?><span class="btn-link"><ins class="with-icon-small w i-vtour"></ins> 360&deg;</span><?php endif; ?>
							</div>
							<div class="background"></div>
						</div>
						<?php endif; ?>
					</a>
				</div>
					
				<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'status_info')); tpl_block_capture(array('assign' => 'status_info'), null, $this); ob_start();  $this->_tag_stack[] = array('tpl_block_strip', array()); tpl_block_strip(array(), null, $this); ob_start(); ?>
					<?php if ($this->_vars['listing']['is_lift_up'] || $this->_vars['location_filter'] == 'country' && $this->_vars['listing']['is_lift_up_country'] || $this->_vars['location_filter'] == 'region' && ( $this->_vars['listing']['is_lift_up_country'] || $this->_vars['listing']['is_lift_up_region'] ) || $this->_vars['location_filter'] == 'city' && ( $this->_vars['listing']['is_lift_up_country'] || $this->_vars['listing']['is_lift_up_region'] || $this->_vars['listing']['is_lift_up_city'] ) || $this->_vars['sort_data']['order'] != 'default' && ( $this->_vars['listing']['is_lift_up'] || $this->_vars['listing']['is_lift_up_country'] || $this->_vars['listing']['is_lift_up_region'] || $this->_vars['listing']['is_lift_up_city'] )): ?>
						<?php echo l('status_lift_up', 'listings', '', 'text', array()); ?>
					<?php elseif ($this->_vars['listing']['is_featured']): ?>
						<?php echo l('status_featured', 'listings', '', 'text', array()); ?>
					<?php endif; ?>
				<?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_strip($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack);  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
				
				<div class="body">
					<?php 
$this->assign('no_info_str', l('no_information', 'start', '', 'text', array()));
 ?>
					<h3><?php echo tpl_function_block(array('name' => listing_price_block,'module' => 'listings','data' => $this->_vars['listing']), $this);?></h3>
					<div class="t-1">
						<?php echo $this->_vars['listing']['property_type_str']; ?>
 <?php echo $this->_vars['listing']['operation_type_str']; ?>

						<br><?php echo $this->_run_modifier($this->_vars['listing']['square_output'], 'truncate', 'plugin', 1, 30); ?>

						<?php if ($this->_run_modifier($this->_vars['listing']['date_open'], 'strtotime', 'PHP', 1) >= $this->_vars['tstamp']): ?><br><?php echo l('text_date_open_status', 'listings', '', 'text', array());  endif; ?>
						<?php if ($this->_vars['listing']['sold']): ?><br><span class="status_text"><?php echo l('text_sold', 'listings', '', 'text', array()); ?></span><?php endif; ?>
						<?php if ($this->_vars['status_info']): ?><br><span class="status_text"><?php echo $this->_vars['status_info']; ?>
</span><?php endif; ?>
					</div>
					<div class="t-2">		
					</div>
					<?php if ($this->_vars['listing']['user']['status']): ?>
					<div class="t-3">
						<?php echo $this->_run_modifier($this->_vars['listing']['user']['output_name'], 'truncate', 'plugin', 1, 50); ?>

					</div>
					<div class="t-4">
						<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'user_logo')); tpl_block_capture(array('assign' => 'user_logo'), null, $this); ob_start(); ?>
							<?php if ($this->_vars['show_logo'] || $this->_vars['listing']['user']['is_show_logo']): ?>
							<img src="<?php echo $this->_vars['listing']['user']['media']['user_logo']['thumbs']['small']; ?>
" title="<?php echo $this->_run_modifier($this->_vars['listing']['user']['output_name'], 'escape', 'plugin', 1); ?>
">
							<?php else: ?>
							<img src="<?php echo $this->_vars['listing']['user']['media']['default_logo']['thumbs']['small']; ?>
" title="<?php echo $this->_run_modifier($this->_vars['listing']['user']['output_name'], 'escape', 'plugin', 1); ?>
">
							<?php endif; ?>
						<?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
						<?php if ($this->_vars['listing']['user']['status']): ?>
						<a href="<?php echo tpl_function_seolink(array('module' => 'users','method' => 'view','data' => $this->_vars['listing']['user']), $this);?>"><?php echo $this->_vars['user_logo']; ?>
</a>
						<?php else: ?>
							<?php echo $this->_vars['user_logo']; ?>

						<?php endif; ?>
					</div>
					<?php endif; ?>
				</div>
				<div class="clr"></div>
				<?php if ($this->_vars['listing']['headline']): ?><p class="headline" title="<?php echo $this->_vars['listing']['headline']; ?>
"><?php echo $this->_run_modifier($this->_vars['listing']['headline'], 'truncate', 'plugin', 1, 125); ?>
</p><?php endif; ?>
			</div>
		</div>
	<?php else: ?>
		<a href="<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'view','data' => $this->_vars['listing']), $this);?>"><?php echo $this->_run_modifier($this->_vars['listing']['output_name'], 'truncate', 'plugin', 1, 50); ?>
</a>
	<?php endif; ?>
