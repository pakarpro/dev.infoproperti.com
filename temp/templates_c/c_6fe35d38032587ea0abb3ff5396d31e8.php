<?php require_once('/opt/ip/system/libraries/template_lite/plugins/function.js.php'); $this->register_function("js", "tpl_function_js");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.helper.php'); $this->register_function("helper", "tpl_function_helper");  require_once('/opt/ip/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-12-14 16:08:26 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
  $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->module_path. "start". $this->module_templates.  $this->get_current_theme_gid('', '"start"'). "left_panel.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<div class="rc">

	<div class="content-block">

		<h1><?php echo l('text_change_regdata', 'users', '', 'text', array()); ?></h1>
		
		<h2 class="line top bottom"><?php echo l('text_change_email', 'users', '', 'text', array()); ?></h2>
		<div class="view-section">
			<div class="edit_block">
			<form action="" method="post">
				<div class="r">
					<div class="f"><?php echo l('field_current_email', 'users', '', 'text', array()); ?>:</div>
					<div class="v"><b><?php echo $this->_vars['data']['email']; ?>
</b></div>
				</div>
				<div class="r">
					<div class="f"><?php echo l('field_new_email', 'users', '', 'text', array()); ?>:&nbsp;* </div>
					<div class="v"><input type="text" name="email" value="" autocomplite="off"></div>
				</div>
				<div class="r">
					<div class="f">&nbsp;</div>
					<div class="v"><input type="submit" class="btn" value="<?php echo l('btn_send', 'start', '', 'button', array()); ?>" name="btn_email_save"></div>
				</div>
			</form>
			</div>
			<div class="edit_block"><?php echo tpl_function_helper(array('func_name' => show_social_networking_link,'module' => users_connections), $this);?></div>
		</div>
		
		<?php if ($this->_vars['phone_format']): ?>
			<?php echo tpl_function_js(array('file' => 'jquery.maskedinput.min.js'), $this);?>
			<script><?php echo '
				$(function(){
					$(\'#phone\').mask(\'';  echo $this->_vars['phone_format'];  echo '\');
				});
			'; ?>
</script>
		<?php endif; ?>
		<h2 class="line top bottom"><?php echo l('text_change_phone', 'users', '', 'text', array()); ?></h2>
		<div class="view-section">
			<div class="edit_block">
			<form action="" method="post">
				<div class="r">
					<div class="f"><?php echo l('field_current_phone', 'users', '', 'text', array()); ?>:</div>
					<div class="v"><b><?php echo $this->_vars['data']['phone']; ?>
</b></div>
				</div>
				<div class="r">
					<div class="f"><?php echo l('field_new_phone', 'users', '', 'text', array()); ?>:&nbsp;* </div>
					<div class="v"><input type="text" name="phone" value="" autocomplite="off" id="phone"></div>
				</div>
				<div class="r">
					<div class="f">&nbsp;</div>
					<div class="v"><input type="submit" class="btn" value="<?php echo l('btn_send', 'start', '', 'button', array()); ?>" name="btn_phone_save"></div>
				</div>
			</form>
			</div>
			<div class="edit_block"><?php echo tpl_function_helper(array('func_name' => show_social_networking_link,'module' => users_connections), $this);?></div>
		</div>

		<h2 class="line top bottom"><?php echo l('text_change_password', 'users', '', 'text', array()); ?></h2>
		<div class="view-section">
			<div class="edit_block">
			<form action="" method="post">
				<div class="r">
					<div class="f"><?php echo l('field_password_old', 'users', '', 'text', array()); ?>:&nbsp;* </div>
					<div class="v"><input type="password" name="password_old" value=""></div>
				</div>
				<div class="r">
					<div class="f"><?php echo l('field_password', 'users', '', 'text', array()); ?>:&nbsp;* </div>
					<div class="v"><input type="password" name="password" value=""></div>
				</div>
				<div class="r">
					<div class="f"><?php echo l('field_repassword', 'users', '', 'text', array()); ?>:&nbsp;* </div>
					<div class="v"><input type="password" name="repassword" value=""></div>
				</div>
				<div class="r">
					<div class="f">&nbsp;</div>
					<div class="v"><input type="submit" class="btn" value="<?php echo l('btn_send', 'start', '', 'button', array()); ?>" name="btn_passw_save"></div>
				</div>
			</form>
			</div>		
		</div>	
			
	</div>
</div>
<div class="clr"></div>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

