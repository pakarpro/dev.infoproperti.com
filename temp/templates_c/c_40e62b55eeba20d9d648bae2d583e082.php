<?php require_once('/opt/ip/system/libraries/template_lite/plugins/function.seolink.php'); $this->register_function("seolink", "tpl_function_seolink");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-01-05 23:01:14 KRAT */ ?>

<?php if ($this->_vars['action'] == 'personal'): ?>
<h1><?php echo l('table_header_personal', 'users', '', 'text', array()); ?></h1>

<div class="edit_block">
<form action="" method="post" enctype="multipart/form-data">
<div class="r">
		<div class="f"><?php echo l('field_fname', 'users', '', 'text', array()); ?>:&nbsp;<span class="ast">*</span> </div>
		<div class="v"><input type="text" name="data[fname]" value="<?php echo $this->_run_modifier($this->_vars['data']['fname'], 'escape', 'plugin', 1); ?>
"></div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_sname', 'users', '', 'text', array()); ?>:<!--&nbsp;*--> </div>
		<div class="v"><input type="text" name="data[sname]" value="<?php echo $this->_run_modifier($this->_vars['data']['sname'], 'escape', 'plugin', 1); ?>
"></div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_phone', 'users', '', 'text', array()); ?>:&nbsp;<span class="ast">*</span> </div>
		<div class="v"><input type="text" name="data[phone]" value="<?php echo $this->_run_modifier($this->_vars['data']['phone'], 'escape', 'plugin', 1); ?>
" class="phone-field"></div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_icon', 'users', '', 'text', array()); ?>: </div>
		<div class="v">
			<input type="file" name="user_icon">
			<?php if ($this->_vars['data']['user_logo'] || $this->_vars['data']['user_logo_moderation']): ?>
			<br><input type="checkbox" name="user_icon_delete" value="1" id="uichb"><label for="uichb"><?php echo l('field_icon_delete', 'users', '', 'text', array()); ?></label><br>
			<?php if ($this->_vars['data']['user_logo_moderation']): ?><img src="<?php echo $this->_vars['data']['media']['user_logo_moderation']['thumbs']['middle']; ?>
"><?php else: ?><img src="<?php echo $this->_vars['data']['media']['user_logo']['thumbs']['middle']; ?>
"><?php endif; ?>

			<?php endif; ?>
		</div>
	</div>
	<div class="r">
		<div class="f">&nbsp;</div>
		<div class="v">
			<input type="submit" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>" name="btn_register" class="btn" />
			<a href="<?php echo tpl_function_seolink(array('module' => 'users','method' => 'profile'), $this);?>" class="btn-link fleft"><ins class="with-icon i-larr"></ins><?php echo l('back_to_my_profile', 'users', '', 'text', array()); ?></a>
		</div>
	</div>
	</form>
</div>
<?php endif; ?>

<?php if ($this->_vars['action'] == 'contact'): ?>
<h1><?php echo l('table_header_contact', 'users', '', 'text', array()); ?></h1>

<div class="edit_block">
<form action="" method="post" enctype="multipart/form-data">
	<div class="r">
		<div class="f"><?php echo l('field_contact_email', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><input type="text" name="data[contact_email]" value="<?php echo $this->_vars['data']['contact_email']; ?>
"></div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_contact_phone', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><input type="text" name="data[contact_phone]" value="<?php echo $this->_vars['data']['contact_phone']; ?>
" class="phone-field"></div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_contact_info', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><textarea rows="5" cols="80" name="data[contact_info]"><?php echo $this->_run_modifier($this->_vars['data']['contact_info'], 'escape', 'plugin', 1); ?>
</textarea></div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_facebook', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><input type="text" name="data[facebook]" value="<?php echo $this->_run_modifier($this->_vars['data']['facebook'], 'escape', 'plugin', 1); ?>
"></div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_twitter', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><input type="text" name="data[twitter]" value="<?php echo $this->_run_modifier($this->_vars['data']['twitter'], 'escape', 'plugin', 1); ?>
"></div>
	</div>
    <!-- 
	<div class="r">
		<div class="f"><?php echo l('field_vkontakte', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><input type="text" name="data[vkontakte]" value="<?php echo $this->_run_modifier($this->_vars['data']['vkontakte'], 'escape', 'plugin', 1); ?>
"></div>
	</div>
    -->
	<div class="r">
		<div class="f">&nbsp;</div>
		<div class="v">
			<input type="submit" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>" name="btn_register" class="btn" />
			<a href="<?php echo tpl_function_seolink(array('module' => 'users','method' => 'profile'), $this);?>" class="btn-link fleft"><ins class="with-icon i-larr"></ins><?php echo l('back_to_my_profile', 'users', '', 'text', array()); ?></a>
		</div>
	</div>
	</form>
</div>
<?php endif; ?>

<?php if ($this->_vars['action'] == 'subscriptions'): ?>
<h1><?php echo l('table_header_subscriptions', 'users', '', 'text', array()); ?></h1>

<div class="edit_block">
<form action="" method="post" enctype="multipart/form-data">
	<?php echo tpl_function_helper(array('func_name' => get_user_subscriptions_form,'module' => subscriptions,'func_param' => profile), $this);?>
	<div class="r">
		<div class="f">&nbsp;</div>
		<div class="v">
			<input type="submit" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>" name="btn_register" class="btn" />
			<a href="<?php echo tpl_function_seolink(array('module' => 'users','method' => 'profile'), $this);?>" class="btn-link fleft"><ins class="with-icon i-larr"></ins><?php echo l('back_to_my_profile', 'users', '', 'text', array()); ?></a>
		</div>
	</div>
	</form>
</div>
<?php endif; ?>
<script type='text/javascript'><?php echo '
   $(document).ready(function(){
		now = new Date();
		yr =  (new Date(now.getYear() - 80, 0, 1).getFullYear()) + \':\' + (new Date(now.getYear() - 18, 0, 1).getFullYear());
		$( "#datepicker" ).datepicker({
			dateFormat :\'yy-mm-dd\',
			changeYear: true,
			changeMonth: true,
			yearRange: yr
		});
   });
'; ?>
</script>
	
<?php if ($this->_vars['phone_format']): ?>
	<?php echo tpl_function_js(array('file' => 'jquery.maskedinput.min.js'), $this);?>
	<script><?php echo '
	$(function(){
		$(\'.phone-field\').mask(\'';  echo $this->_vars['phone_format'];  echo '\');
	});
	'; ?>
</script>
<?php endif; ?>

