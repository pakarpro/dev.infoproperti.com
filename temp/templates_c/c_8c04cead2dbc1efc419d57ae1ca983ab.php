<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.helper.php'); $this->register_function("helper", "tpl_function_helper");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2014-12-12 14:25:44 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
  echo tpl_function_helper(array('func_name' => get_admin_level1_menu,'helper_name' => menu,'func_param' => 'admin_seo_menu'), $this);?>
<div class="actions">&nbsp;</div>

<div class="filter-form">
<form method="post">
http://<input type="text" name="url" value="<?php echo $this->_vars['url']; ?>
">
<input type="submit" name="btn_save" value="<?php echo l('btn_send', 'start', '', 'button', array()); ?>">
</form>
</div>

<table cellspacing="0" cellpadding="0" class="data" width="100%">
<tr>
	<th class="first" colspan="2"><?php echo l('analytics_h_basic', 'seo', '', 'text', array()); ?></th>
	<th colspan="2"><?php echo l('analytics_h_alexa', 'seo', '', 'text', array()); ?></th>
	<th colspan="2"><?php echo l('analytics_h_backlinks', 'seo', '', 'text', array()); ?></th>
</tr>
<tr class="zebra">
	<td><?php echo l('field_domain_age', 'seo', '', 'text', array()); ?></td>
	<td class="center"><?php if ($this->_vars['domain']['registered']):  echo $this->_vars['domain']['age']['y']; ?>
 <?php echo l('da_years', 'seo', '', 'text', array()); ?> <?php echo $this->_vars['domain']['age']['m']; ?>
 <?php echo l('da_months', 'seo', '', 'text', array()); ?> <?php echo $this->_vars['domain']['age']['d']; ?>
 <?php echo l('da_days', 'seo', '', 'text', array());  else:  echo l('domain_not_registered', 'seo', '', 'text', array());  endif; ?></td>
	<td><?php echo l('field_backlinks', 'seo', '', 'text', array()); ?>:</td>
	<td class="center"><a href="<?php echo $this->_vars['check_links']['alexa_backlinks']; ?>
" target="_blank"><?php echo $this->_vars['domain']['alexa_backlinks']; ?>
</a></td>
	<td><?php echo l('field_google', 'seo', '', 'text', array()); ?></td>
	<td class="center"><a href="<?php echo $this->_vars['check_links']['google_backlinks']; ?>
" target="_blank"><?php echo $this->_vars['domain']['google_backlinks']; ?>
</a></td>
</tr>
<tr>
	<td><?php echo l('field_page_rank', 'seo', '', 'text', array()); ?></td>
	<td class="center"><?php echo $this->_vars['domain']['page_rank']; ?>
</td>
	<td><?php echo l('field_traffic_rank', 'seo', '', 'text', array()); ?>:</td>
	<td class="center"><a href="<?php echo $this->_vars['check_links']['alexa_rank']; ?>
" target="_blank"><?php echo $this->_vars['domain']['alexa_rank']; ?>
</a></td>
	<td><?php echo l('field_yahoo', 'seo', '', 'text', array()); ?></td>
	<td class="center"><a href="<?php echo $this->_vars['check_links']['yahoo_backlinks']; ?>
" target="_blank"><?php echo $this->_vars['domain']['yahoo_backlinks']; ?>
</a></td>
</tr>
<tr>
	<th class="first" colspan="2"><?php echo l('analytics_h_tech', 'seo', '', 'text', array()); ?></th>
	<th colspan="2"><?php echo l('analytics_h_directory', 'seo', '', 'text', array()); ?></th>
	<th colspan="2"><?php echo l('analytics_h_indexed', 'seo', '', 'text', array()); ?></th>
</tr>
<tr class="zebra">
	<td><?php echo l('field_rank', 'seo', '', 'text', array()); ?></td>
	<td class="center"><a href="<?php echo $this->_vars['check_links']['technorati_rank']; ?>
" target="_blank"><?php echo $this->_vars['domain']['technorati_rank']; ?>
</a></td>
	<td><?php echo l('field_dmoz', 'seo', '', 'text', array()); ?>:</td>
	<td class="center"><a href="<?php echo $this->_vars['check_links']['dmoz_listed']; ?>
" target="_blank"><?php if ($this->_vars['domain']['dmoz_listed']):  echo l('field_listed', 'seo', '', 'text', array());  else:  echo l('field_not_listed', 'seo', '', 'text', array());  endif; ?></a></td>
	<td><?php echo l('field_google', 'seo', '', 'text', array()); ?></td>
	<td class="center"><a href="<?php echo $this->_vars['check_links']['google_indexed']; ?>
" target="_blank"><?php echo $this->_vars['domain']['google_indexed']; ?>
</a></td>
</tr>
<tr>
	<td><?php echo l('field_authority', 'seo', '', 'text', array()); ?></td>
	<td class="center"><a href="<?php echo $this->_vars['check_links']['technoraty_authority']; ?>
" target="_blank"><?php echo $this->_vars['domain']['technoraty_authority']; ?>
</a></td>
	<td><?php echo l('field_google', 'seo', '', 'text', array()); ?>:</td>
	<td class="center"><a href="<?php echo $this->_vars['check_links']['google_listed']; ?>
" target="_blank"><?php if ($this->_vars['domain']['google_listed']):  echo l('field_listed', 'seo', '', 'text', array());  else:  echo l('field_not_listed', 'seo', '', 'text', array());  endif; ?></a></td>
	<td><?php echo l('field_yahoo', 'seo', '', 'text', array()); ?></td>
	<td class="center"><a href="<?php echo $this->_vars['check_links']['yahoo_indexed']; ?>
" target="_blank"><?php echo $this->_vars['domain']['yahoo_indexed']; ?>
</a></td>
</tr>
</table>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>