<?php require_once('/opt/ip/system/libraries/template_lite/plugins/block.capture.php'); $this->register_block("capture", "tpl_block_capture");  require_once('/opt/ip/system/libraries/template_lite/plugins/modifier.date_format.php'); $this->register_modifier("date_format", "tpl_modifier_date_format");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.ld.php'); $this->register_function("ld", "tpl_function_ld");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.country_select.php'); $this->register_function("country_select", "tpl_function_country_select");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.block.php'); $this->register_function("block", "tpl_function_block");  require_once('/opt/ip/system/libraries/template_lite/plugins/modifier.escape.php'); $this->register_modifier("escape", "tpl_modifier_escape");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.js.php'); $this->register_function("js", "tpl_function_js");  require_once('/opt/ip/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-02-29 10:06:49 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array('load_type' => 'ui'));
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
	<div class="content-block">
		<div class="edit_block">
			<h1><?php if ($this->_vars['data']['id']):  echo l('header_listing_edit', 'listings', '', 'text', array());  else:  echo l('header_add_listing', 'listings', '', 'text', array());  endif; ?></h1>
			
			<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->module_path. "listings". $this->module_templates.  $this->get_current_theme_gid('', '"listings"'). "my_listings_menu.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

			<?php if ($this->_vars['moderation_alerts']): ?>
			<?php if (is_array($this->_vars['moderation_alerts']) and count((array)$this->_vars['moderation_alerts'])): foreach ((array)$this->_vars['moderation_alerts'] as $this->_vars['type'] => $this->_vars['item']): ?>
			<div class="alert-block <?php echo $this->_vars['type']; ?>
" id="<?php echo $this->_vars['type']; ?>
_message"><div class="close with-icon i-close"></div>
			<?php if ($this->_vars['type'] != 'info'):  echo l("moderation_alert_" . $this->_vars['type'] . "", 'listings', '', 'text', array());  endif; ?>
			<?php if ($this->_vars['item']):  if ($this->_vars['type'] != 'info'): ?><br><b><?php echo l('subheader_moderator_comment', 'listings', '', 'text', array()); ?>: </b><?php endif;  echo $this->_vars['item'];  endif; ?>
			</div>
			<?php endforeach; endif; ?>
			<?php echo tpl_function_js(array('module' => listings,'file' => 'listings-alert.js'), $this);?>
			<script><?php echo '
			$(function(){
				new listingsAlert({
					siteUrl: \'';  echo $this->_vars['site_url'];  echo '\',
					listingId: \'';  echo $this->_vars['data']['id'];  echo '\'
				});
			});
			'; ?>
</script>
			<?php endif; ?>
			<?php echo tpl_function_js(array('module' => listings,'file' => 'listings-edit-steps.js'), $this);?>
			<script><?php echo '
			    
			function change_ot(ot){
			    switch(ot){
					case \'sale\':
						$(\'#price_max\').val(\'\').hide();
						$(\'#square_max\').val(\'\').hide();
						$(\'#price_reduced_box\').show();
						$(\'#price_negotiated_sale\').show();
						$(\'#price_negotiated_buy\').hide();
						$(\'#address_box\').show();
						$(\'#booking_period\').hide(); // #MOD#
						$(\'#price_period\').val(\'\');
						$(\'#price_type\').val(\'\');
					break;
					case \'buy\':
						$(\'#price_max\').show();
						$(\'#square_max\').show();
						$(\'#price_reduced_box\').hide();
						$(\'#price_reduced\').val(\'\');
						$(\'#price_negotiated_sale\').hide();
						$(\'#price_negotiated_buy\').show();
						$(\'#address_box\').hide();
						$(\'#address\').val(\'\');
						$(\'#booking_period\').hide();
						$(\'#price_period\').val(\'\');
						$(\'#price_type\').val(\'\');
					break;
					case \'rent\':
						$(\'#price_max\').val(\'\').hide();
						$(\'#square_max\').val(\'\').hide();
						$(\'#price_reduced_box\').show();
						$(\'#price_negotiated_sale\').show();
						$(\'#price_negotiated_buy\').hide();
						$(\'#address_box\').show();
						$(\'#booking_period\').show();
					break;
					case \'lease\':
						$(\'#price_max\').show();
						$(\'#square_max\').show();
						$(\'#price_reduced_box\').hide();
						$(\'#price_reduced\').val(\'\');
						$(\'#price_negotiated_sale\').hide();
						$(\'#price_negotiated_buy\').show();
						$(\'#address_box\').hide();
						$(\'#address\').val(\'\');
						$(\'#booking_period\').show();
					break;
			    }
			}
			    
			$(function(){
				new listingsEditSteps({
					currentStep: \'';  echo $this->_vars['step'];  echo '\'
				});
				$(\'#price_negotiated\').bind(\'click\', function(){
					if(this.checked){
						$(\'#price\').attr(\'disabled\', \'disabled\');
						$(\'#price_max\').attr(\'disabled\', \'disabled\');
						$(\'#price_reduced\').attr(\'disabled\', \'disabled\');
						$(\'#price_unit\').attr(\'disabled\', \'disabled\');
					}else{
						$(\'#price\').removeAttr(\'disabled\');
						$(\'#price_max\').removeAttr(\'disabled\');
						$(\'#price_reduced\').removeAttr(\'disabled\');
						$(\'#price_unit\').removeAttr(\'disabled\');
					}
				});
			});
			'; ?>
</script>
			<?php if ($this->_vars['section_gid'] == 'overview'): ?>
			<?php if ($this->_vars['data']['id']): ?><div class="rollup-box"><?php endif; ?>
				<form method="post" action="<?php echo $this->_run_modifier($this->_vars['data']['action'], 'escape', 'plugin', 1); ?>
" name="save_form" id="save_form" enctype="multipart/form-data">
				<div class="r">
					<?php if ($this->_vars['data']['id']): ?>
						<div class="v"><?php echo l('field_listing_type', 'listings', '', 'text', array()); ?>: 
							<span class="controller-select"><?php echo $this->_vars['data']['operation_type_str']; ?>
</span>
						</div>
					<?php elseif ($this->_vars['operation_types_count'] == 1): ?>
						<?php if (is_array($this->_vars['operation_types']) and count((array)$this->_vars['operation_types'])): foreach ((array)$this->_vars['operation_types'] as $this->_vars['item']): ?>
						<input type="hidden" name="type" value="<?php echo $this->_run_modifier($this->_vars['item'], 'escape', 'plugin', 1); ?>
" />
						<?php echo $this->_vars['item']; ?>

						<?php endforeach; endif; ?>
						<script><?php echo '
						    $(function(){
							var operation_type = \'';  echo $this->_vars['item'];  echo '\';
							change_ot(operation_type);
						    });
						'; ?>
</script>
					<?php else: ?>
					<div class="f"><?php echo l('field_listing_type', 'listings', '', 'text', array()); ?>:<?php if (! $this->_vars['data']['id']): ?>&nbsp;*<?php endif; ?> </div>
					<div class="v">
						<?php if (is_array($this->_vars['operation_types']) and count((array)$this->_vars['operation_types'])): foreach ((array)$this->_vars['operation_types'] as $this->_vars['item']): ?>
						<input type="radio" name="type" value="<?php echo $this->_run_modifier($this->_vars['item'], 'escape', 'plugin', 1); ?>
" id="for_<?php echo $this->_run_modifier($this->_vars['item'], 'escape', 'plugin', 1); ?>
" <?php if ($this->_vars['data']['operation_type'] == $this->_vars['item']): ?>checked<?php endif; ?> />
						<label for="for_<?php echo $this->_run_modifier($this->_vars['item'], 'escape', 'plugin', 1); ?>
"><?php echo l('operation_type_'.$this->_vars['item'], 'listings', '', 'text', array()); ?></label>
						<?php endforeach; endif; ?>
						<script><?php echo '
							$(function(){
								$(\'input[name=type]\').bind(\'change\', function(){
									change_ot(this.value);
								});
							});
						'; ?>
</script>					
					</div>
					<?php endif; ?>
				</div>
				<div class="r">
					<?php $this->assign('selected_category', $this->_vars['data']['id_category'].'_'.$this->_vars['data']['property_type']); ?>
					<?php if ($this->_vars['data']['id'] && $this->_vars['data']['id_category'] && $this->_vars['data']['property_type']): ?>
					<div class="f"><?php echo l('field_category', 'listings', '', 'text', array()); ?>: <span class="controller-select"><?php echo $this->_vars['data']['category_str']; ?>
 , <?php echo $this->_vars['data']['property_type_str']; ?>
</span></div>
					<div class="v">
					    <?php echo tpl_function_block(array('name' => 'properties_select','module' => 'properties','var_name' => 'data[category]','selected' => $this->_vars['selected_category'],'id_category' => $this->_vars['data']['id_category'],'category_str' => $this->_vars['data']['category_str']), $this);?>
					</div>
					<?php else: ?>
					<div class="f"><?php echo l('field_category', 'listings', '', 'text', array()); ?>:&nbsp;* </div>
					<div class="v">
						<?php echo tpl_function_block(array('name' => 'properties_select','module' => 'properties','var_name' => 'data[category]','selected' => $this->_vars['selected_category'],'cat_select' => false,'js_var_name' => 'category'), $this);?>
					</div>
					<?php endif; ?>
				</div>
				<div class="r">
					<div class="f"><?php echo l('field_location', 'listings', '', 'text', array()); ?>:&nbsp;* </div>
					<div class="v"><?php echo tpl_function_country_select(array('select_type' => 'city','id_country' => $this->_vars['data']['id_country'],'id_region' => $this->_vars['data']['id_region'],'id_city' => $this->_vars['data']['id_city']), $this);?></div>
				</div>
				<div class="r <?php if ($this->_vars['data']['operation_type'] != 'sale' && $this->_vars['data']['operation_type'] != 'rent'): ?>hide<?php endif; ?>" id="address_box">
					<div class="f"><?php echo l('field_address', 'listings', '', 'text', array()); ?>: </div>
					<div class="v"><input type="text" name="data[address]" value="<?php echo $this->_run_modifier($this->_vars['data']['address'], 'escape', 'plugin', 1); ?>
" id="address"></div>
				</div>
				<div class="r">
					<div class="f"><?php echo l('field_postal_code', 'listings', '', 'text', array()); ?>: </div>
					<div class="v"><input type="text" name="data[zip]" value="<?php echo $this->_run_modifier($this->_vars['data']['zip'], 'escape', 'plugin', 1); ?>
" class="middle"/></div>
				</div>
				<div class="r <?php if ($this->_vars['data']['operation_type'] != 'rent' && $this->_vars['data']['operation_type'] != 'lease'): ?>hide<?php endif; ?>" id="booking_period">
					<div class="f"><?php echo l('field_booking_period', 'listings', '', 'text', array()); ?>:&nbsp;* </div>
					<div class="v">
					
					<!-- #MOD# -->
						
						<?php echo tpl_function_ld(array('i' => 'price_period','gid' => 'listings','assign' => 'price_period'), $this);?>
						<?php echo tpl_function_ld(array('i' => 'price_period','gid' => 'listings','assign' => 'price_period'), $this);?>
						<select name="data[price_period]" class="middle" id="price_period">
						<option value=""><?php echo $this->_vars['price_period']['header']; ?>
</option>
						<?php if (is_array($this->_vars['price_period']['option']) and count((array)$this->_vars['price_period']['option'])): foreach ((array)$this->_vars['price_period']['option'] as $this->_vars['key'] => $this->_vars['item']): ?>
						<option value="<?php echo $this->_vars['key']; ?>
" <?php if ($this->_vars['key'] == $this->_vars['data']['price_period']): ?>selected<?php endif; ?>><?php echo $this->_vars['item']; ?>
</option>
						<?php endforeach; endif; ?>
						</select>
						
						<?php echo tpl_function_ld(array('i' => 'price_type','gid' => 'listings','assign' => 'price_type'), $this);?>
						<select name="data[price_type]" class="middle" id="price_type">
						<option value=""><?php echo $this->_vars['price_type']['header']; ?>
</option>
						<?php if (is_array($this->_vars['price_type']['option']) and count((array)$this->_vars['price_type']['option'])): foreach ((array)$this->_vars['price_type']['option'] as $this->_vars['key'] => $this->_vars['item']): ?>
						<option value="<?php echo $this->_vars['key']; ?>
" <?php if ($this->_vars['key'] == $this->_vars['data']['price_type']): ?>selected<?php endif; ?>><?php echo $this->_vars['item']; ?>
</option>
						<?php endforeach; endif; ?>
						</select>
						
					<!-- #MOD# -->	
					</div>
				</div>
				<div class="r">
					<div class="v">
						<input type="hidden" name="data[price_negotiated]" value="0">
						<input type="checkbox" name="data[price_negotiated]" value="1" id="price_negotiated" <?php if ($this->_vars['data']['price_negotiated']): ?>checked<?php endif; ?>> 
						<label for="price_negotiated" id="price_negotiated_buy"<?php if ($this->_vars['data']['operation_type'] != 'buy'): ?>class="hide"<?php endif; ?>><?php echo l('field_price_negotiated_buy', 'listings', '', 'text', array()); ?></label>
						<label for="price_negotiated" id="price_negotiated_sale"<?php if ($this->_vars['data']['operation_type'] == 'buy'): ?>class="hide"<?php endif; ?>><?php echo l('field_price_negotiated_sale', 'listings', '', 'text', array()); ?></label>
					</div>
				</div>
				<div class="r">
					<div class="f"><?php echo l('field_price', 'listings', '', 'text', array()); ?>:&nbsp;* </div>
					<div class="v">
						<input type="text" name="data[price]" value="<?php echo $this->_run_modifier($this->_vars['data']['price'], 'escape', 'plugin', 1); ?>
" id="price" class="middle" <?php if ($this->_vars['data']['price_negotiated']): ?>disabled<?php endif; ?>>
						<input type="text" name="data[price_max]" value="<?php echo $this->_run_modifier($this->_vars['data']['price_max'], 'escape', 'plugin', 1); ?>
" id="price_max" class="<?php if ($this->_vars['data']['operation_type'] != 'buy'): ?>hide<?php endif; ?> middle" <?php if ($this->_vars['data']['price_negotiated']): ?>disabled<?php endif; ?>> 
												<?php if ($this->_vars['data']['id'] && ( $this->_vars['data']['operation_type'] == 'rent' || $this->_vars['data']['operation_type'] == 'lease' )): ?>
							<?php echo $this->_vars['current_price_currency']['abbr']; ?>

						<?php else: ?>
						<select name="data[gid_currency]" class="short" id="price_unit" <?php if ($this->_vars['data']['price_negotiated']): ?>disabled<?php endif; ?>>
							<?php if (is_array($this->_vars['currencies']) and count((array)$this->_vars['currencies'])): foreach ((array)$this->_vars['currencies'] as $this->_vars['item']): ?>
							<option value="<?php echo $this->_run_modifier($this->_vars['item']['gid'], 'escape', 'plugin', 1); ?>
" <?php if ($this->_vars['item']['gid'] == $this->_vars['data']['gid_currency']): ?>selected<?php endif; ?>><?php echo $this->_vars['item']['abbr']; ?>
</option>
							<?php endforeach; endif; ?>
						</select>
						<script><?php echo '
							$(function(){
								$(\'#price_unit\').bind(\'click\', function(){
									var text = $(this).find(\':selected\').html();
									$(\'#price_reduced_unit\').html(text);
								});
							});
						'; ?>
</script>
						<?php endif; ?>
											</div>
				</div>
				<div class="r <?php if ($this->_vars['data']['operation_type'] != 'sale' && $this->_vars['data']['operation_type'] != 'rent'): ?>hide<?php endif; ?>" id="price_reduced_box">
					<div class="f"><?php echo l('field_price_reduced', 'listings', '', 'text', array()); ?>: </div>
					<div class="v">
						<input type="text" name="data[price_reduced]" value="<?php echo $this->_run_modifier($this->_vars['data']['price_reduced'], 'escape', 'plugin', 1); ?>
" id="price_reduced" class="middle" <?php if ($this->_vars['data']['price_negotiated']): ?>disabled<?php endif; ?>> 
						<span id="price_reduced_unit"><?php echo $this->_vars['current_price_currency']['abbr']; ?>
</span>
					</div>
				</div>
				<div class="r">
					<div class="v">
						<input type="hidden" name="data[price_auction]" value="0">
						<input type="checkbox" name="data[price_auction]" value="1" <?php if ($this->_vars['data']['price_auction']): ?>checked<?php endif; ?>> 
						<label for="auction"><?php echo l('field_price_auction', 'listings', '', 'text', array()); ?></label>
					</div>
				</div>
				<div class="r">
					<div class="f"><?php echo l('field_date_open', 'listings', '', 'text', array()); ?>: </div>
					<div class="v periodbox">
						<input type="text" name="data[date_open]" value="<?php if ($this->_run_modifier($this->_vars['data']['date_open'], 'strtotime', 'PHP', 1) > 0):  echo $this->_run_modifier($this->_run_modifier($this->_vars['data']['date_open'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']), 'escape', 'plugin', 1);  endif; ?>" id="date_open" class="middle" /> 
						<input type="hidden" name="date_open_alt" value="" id="alt_date_open">
						<script><?php echo '
							$(function(){
								$(\'#date_open\').datepicker({dateFormat: \'';  echo $this->_vars['page_data']['datepicker_date_format'];  echo '\', altFormat: \'';  echo $this->_vars['page_data']['datepicker_alt_format'];  echo '\', altField: \'#alt_date_open\', showOn: \'both\'});
							});
						'; ?>
</script>
						<?php echo l('text_from', 'listings', '', 'text', array()); ?>:
						<select name="data[date_open_begin]" class="middle"> 
							<option value="" <?php if (! $this->_vars['data']['time_open_begin']): ?>selected="selected"<?php endif; ?>><?php echo $this->_vars['dayhours']['header']; ?>
</option>
							<?php if (is_array($this->_vars['dayhours']['option']) and count((array)$this->_vars['dayhours']['option'])): foreach ((array)$this->_vars['dayhours']['option'] as $this->_vars['key'] => $this->_vars['item']): ?>
							<option value="<?php echo $this->_run_modifier($this->_vars['key'], 'escape', 'plugin', 1); ?>
" <?php if ($this->_vars['data']['date_open_begin'] == $this->_vars['key']): ?>selected<?php endif; ?>><?php echo $this->_vars['item']; ?>
</option>
							<?php endforeach; endif; ?>
						</select>
						<?php echo l('text_till', 'listings', '', 'text', array()); ?>:
						<select name="data[date_open_end]" class="middle"> 
							<option value="" <?php if (! $this->_vars['data']['date_open_end']): ?>selected="selected"<?php endif; ?>><?php echo $this->_vars['dayhours']['header']; ?>
</option>
							<?php if (is_array($this->_vars['dayhours']['option']) and count((array)$this->_vars['dayhours']['option'])): foreach ((array)$this->_vars['dayhours']['option'] as $this->_vars['key'] => $this->_vars['item']): ?>
							<option value="<?php echo $this->_run_modifier($this->_vars['key'], 'escape', 'plugin', 1); ?>
" <?php if ($this->_vars['data']['date_open_end'] == $this->_vars['key']): ?>selected<?php endif; ?>><?php echo $this->_vars['item']; ?>
</option>
							<?php endforeach; endif; ?>
						</select>
					</div>
				</div>
				<div class="r periodbox">
					<div class="f"><?php echo l('field_date_available', 'listings', '', 'text', array()); ?>: </div>
					<div class="v">
						<input type="text" name="data[date_available]" value="<?php if ($this->_run_modifier($this->_vars['data']['date_open'], 'strtotime', 'PHP', 1) > 0):  echo $this->_run_modifier($this->_run_modifier($this->_vars['data']['date_available'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']), 'escape', 'plugin', 1);  endif; ?>" id="date_available" class="middle" /> 
						<input type="hidden" name="date_available_alt" value="" id="alt_date_available">
						<script><?php echo '
							$(function(){
								$(\'#date_available\').datepicker({dateFormat: \'';  echo $this->_vars['page_data']['datepicker_date_format'];  echo '\', altFormat: \'';  echo $this->_vars['page_data']['datepicker_alt_format'];  echo '\', altField: \'#alt_date_available\', showOn: \'both\'});
							});
						'; ?>
</script>
					</div>
				</div>
				<div class="r">
					<div class="f"><?php echo l('field_square', 'listings', '', 'text', array()); ?>:&nbsp;* </div>
					<div class="v">
						<input type="text" name="data[square]" value="<?php echo $this->_run_modifier($this->_vars['data']['square'], 'escape', 'plugin', 1); ?>
" class="middle">
						<input type="text" name="data[square_max]" value="<?php echo $this->_run_modifier($this->_vars['data']['square_max'], 'escape', 'plugin', 1); ?>
" id="square_max" class="<?php if ($this->_vars['data']['operation_type'] != 'buy'): ?>hide<?php endif; ?> middle"> 
						<select name="data[square_unit]" class="short">
							<?php if (is_array($this->_vars['square_units']['option']) and count((array)$this->_vars['square_units']['option'])): foreach ((array)$this->_vars['square_units']['option'] as $this->_vars['key'] => $this->_vars['item']): ?>
							<option value="<?php echo $this->_run_modifier($this->_vars['key'], 'escape', 'plugin', 1); ?>
" <?php if ($this->_vars['key'] == $this->_vars['data']['square_unit']): ?>selected<?php endif; ?>><?php echo $this->_vars['item']; ?>
</option>
							<?php endforeach; else: ?>
							<option value=""><?php echo l('select_default', 'start', '', 'text', array()); ?></option>
							<?php endif; ?>
						</select>
					</div>
				</div>
				<div class="r">
					<div class="f"><?php echo l('field_headline', 'listings', '', 'text', array()); ?>: </div>
					<div class="v">
						<textarea name="data[headline]" rows="10" cols="80"><?php echo $this->_run_modifier($this->_vars['data']['headline'], 'escape', 'plugin', 1); ?>
</textarea>
					</div>
				</div>
				<div class="b">
					<input type="submit" name="btn_save_general" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>">
				</div>
				<input type="hidden" name="save_btn" value="1">
				<input type="hidden" name="data[lat]" value="<?php echo $this->_run_modifier($this->_vars['data']['lat'], 'escape', 'plugin', 1); ?>
" id="lat">
				<input type="hidden" name="data[lon]" value="<?php echo $this->_run_modifier($this->_vars['data']['lon'], 'escape', 'plugin', 1); ?>
" id="lon">
				</form>
			<?php if ($this->_vars['data']['id']): ?></div><?php endif; ?>
			<?php echo tpl_function_block(array('name' => geomap_load_geocoder,'module' => 'geomap'), $this);?>
			<script><?php echo '
				function update_coordinates(country, region, city, address, postal_code){
					if(typeof(geocoder) != \'undefined\'){
						var location = geocoder.getLocationFromAddress(country, region, city, address, postal_code);
						geocoder.geocodeLocation(location, function(latitude, longitude){
							$(\'#lat\').val(latitude);
							$(\'#lon\').val(longitude);
						});	
					}
				}
				$(function(){
					var location_change_wait = 0;
					var country_old = \'';  echo $this->_vars['data']['id_country'];  echo '\';
					var region_old = \'';  echo $this->_vars['data']['id_region'];  echo '\';
					var city_old = \'';  echo $this->_vars['data']['id_city'];  echo '\';
					var address_old = \'';  echo $this->_run_modifier('\'', 'str_replace', 'PHP', 1, '\\\'', $this->_vars['data']['address']);  echo '\';
					var postal_code_old = \'';  echo $this->_run_modifier("'", 'str_replace', 'PHP', 1, "\'", $this->_vars['data']['zip']);  echo '\';
		
					$(\'input[name=id_city]\').bind(\'change\', function(){
						var city = $(this).val();
						if(city == 0) return;
						location_change_wait++;
						check_address_updated();
					});
		
					$(\'input[name=data\\\\[address\\\\]], input[name=data\\\\[zip\\\\]]\').bind(\'keypress\', function(){
						location_change_wait++;
						setTimeout(check_address_updated, 1000);
					});
		
					function check_address_updated(){
						location_change_wait--;
						if(location_change_wait) return;
						var country = $(\'input[name=id_country]\').val();
						var region = $(\'input[name=id_region]\').val();
						var city = $(\'input[name=id_city]\').val();
						var address = $(\'input[name=data\\\\[address\\\\]]\').val();
						var postal_code = $(\'input[name=data\\\\[zip\\\\]]\').val();
						if(country == country_old && region == region_old && 
							city == city_old && address == address_old && postal_code == postal_code_old) return;
						country_old = country;
						region_old = region;
						city_old = city;
						address_old = address;
						postal_code_old = postal_code;
						var country_name = $(\'input[name=id_country]\').attr(\'data-name\');
						var region_name = $(\'input[name=id_region]\').attr(\'data-name\');
						var city_name = $(\'input[name=id_city]\').attr(\'data-name\');
						update_coordinates(country_name, region_name, city_name, address, postal_code);
					}
				});
			'; ?>
</script>
			<?php elseif ($this->_vars['section_gid'] == 'description'): ?>
			<?php if (is_array($this->_vars['sections_data']) and count((array)$this->_vars['sections_data'])): foreach ((array)$this->_vars['sections_data'] as $this->_vars['section_key'] => $this->_vars['section_item']): ?>
			<div class="rollup-box">
				<div class="title">
					<div class="btn-link"><ins class="with-icon<?php if ($this->_vars['step'] != $this->_vars['section_key']): ?> i-expand<?php else: ?> i-collapse<?php endif; ?>"></ins></div>
					<h2><?php echo $this->_vars['section_item']['name']; ?>
</h2>
				</div>
				<div class="cont<?php if ($this->_vars['step'] != $this->_vars['section_key']): ?> hide<?php endif; ?>">
					<form method="post" action="<?php echo $this->_vars['data']['action']; ?>
" enctype="multipart/form-data">
					<?php if (is_array($this->_vars['fields_data']) and count((array)$this->_vars['fields_data'])): foreach ((array)$this->_vars['fields_data'] as $this->_vars['item']): ?>
					<?php if ($this->_vars['item']['section_gid'] == $this->_vars['section_item']['gid']): ?>					
					<div class="r">
						<div class="f">
							<!-- show no max - min char -->
							<?php echo $this->_vars['item']['name']; ?>
: <?php /* Disable for now <?php if ($this->_vars['item']['settings_data_array']['min_char'] > 0):  echo l('text_min_char', 'listings', '', 'text', array()); ?>&nbsp;<b><?php echo $this->_vars['item']['settings_data_array']['min_char']; ?>
</b><?php endif; ?> <?php if ($this->_vars['item']['settings_data_array']['max_char'] > 0):  echo l('text_max_char', 'listings', '', 'text', array()); ?>&nbsp;<b><?php echo $this->_vars['item']['settings_data_array']['max_char']; ?>
</b><?php endif; ?> */ ?>
						</div>
						<div class="v">
						<?php if ($this->_vars['item']['field_type'] == 'select'): ?>
							<?php if ($this->_vars['item']['settings_data_array']['view_type'] == 'select'): ?>
							<select name="<?php echo $this->_vars['item']['field_name']; ?>
">
							<?php if ($this->_vars['item']['settings_data_array']['empty_option']): ?><option value="0"<?php if ($this->_vars['value'] == 0): ?> selected<?php endif; ?>>...</option><?php endif; ?>
							<?php if (is_array($this->_vars['item']['options']['option']) and count((array)$this->_vars['item']['options']['option'])): foreach ((array)$this->_vars['item']['options']['option'] as $this->_vars['value'] => $this->_vars['option']): ?><option value="<?php echo $this->_run_modifier($this->_vars['value'], 'escape', 'plugin', 1); ?>
" <?php if ($this->_vars['value'] == $this->_vars['item']['value']): ?>selected<?php endif; ?>><?php echo $this->_vars['option']; ?>
</option><?php endforeach; endif; ?>
							</select>
							<?php else: ?>
							<?php if ($this->_vars['item']['settings_data_array']['empty_option']): ?><input type="radio" name="<?php echo $this->_vars['item']['field_name']; ?>
" value="0" <?php if ($this->_vars['value'] == 0): ?>checked<?php endif; ?> id="<?php echo $this->_vars['item']['field_name']; ?>
_0">
							<label for="<?php echo $this->_vars['item']['field_name']; ?>
_0">No select</label><br><?php endif; ?>
							<?php if (is_array($this->_vars['item']['options']['option']) and count((array)$this->_vars['item']['options']['option'])): foreach ((array)$this->_vars['item']['options']['option'] as $this->_vars['value'] => $this->_vars['option']): ?>
							<input type="radio" name="<?php echo $this->_vars['item']['field_name']; ?>
" value="<?php echo $this->_run_modifier($this->_vars['value'], 'escape', 'plugin', 1); ?>
" <?php if ($this->_vars['value'] == $this->_vars['item']['value']): ?>checked<?php endif; ?> id="<?php echo $this->_vars['item']['field_name']; ?>
_<?php echo $this->_run_modifier($this->_vars['value'], 'escape', 'plugin', 1); ?>
">
							<label for="<?php echo $this->_vars['item']['field_name']; ?>
_<?php echo $this->_run_modifier($this->_vars['value'], 'escape', 'plugin', 1); ?>
"><?php echo $this->_vars['option']; ?>
</label><br>
							<?php endforeach; endif; ?>
							<?php endif; ?>
						<?php elseif ($this->_vars['item']['field_type'] == 'multiselect'): ?>
							<div class="fleft">
								<?php if (is_array($this->_vars['item']['options']['option']) and count((array)$this->_vars['item']['options']['option'])): foreach ((array)$this->_vars['item']['options']['option'] as $this->_vars['value'] => $this->_vars['option']): ?>
								<input type="checkbox" name="<?php echo $this->_vars['item']['field_name']; ?>
[]" value="<?php echo $this->_run_modifier($this->_vars['value'], 'escape', 'plugin', 1); ?>
" <?php if ($this->_run_modifier($this->_vars['value'], 'in_array', 'PHP', 1, $this->_vars['item']['value'])): ?>checked<?php endif; ?> id="<?php echo $this->_vars['item']['field_name']; ?>
_<?php echo $this->_run_modifier($this->_vars['value'], 'escape', 'plugin', 1); ?>
" class="width4">
								<label for="<?php echo $this->_vars['item']['field_name']; ?>
_<?php echo $this->_run_modifier($this->_vars['value'], 'escape', 'plugin', 1); ?>
" class="width4"><?php echo $this->_vars['option']; ?>
</label>
								<?php endforeach; endif; ?>
								<input type="hidden" name="<?php echo $this->_vars['item']['field_name']; ?>
[]" value="0">
							</div>
							<div class="clr"></div>
							<div class="multiselect_actions">
								<a href="javascript:void(0)" id="<?php echo $this->_vars['item']['field_name']; ?>
_select_all"><?php echo l('select_all', 'start', '', 'text', array()); ?></a>
								<a href="javascript:void(0)" id="<?php echo $this->_vars['item']['field_name']; ?>
_unselect_all"><?php echo l('unselect_all', 'start', '', 'text', array()); ?></a>
							</div>
							<script><?php echo '
								$(function(){
									$(\'#';  echo $this->_vars['item']['field_name']; ?>
_select_all<?php echo '\').bind(\'click\', function(){
										$(\'input[name^=';  echo $this->_vars['item']['field_name'];  echo ']\').attr(\'checked\', \'checked\');
									});
									$(\'#';  echo $this->_vars['item']['field_name']; ?>
_unselect_all<?php echo '\').bind(\'click\', function(){
										$(\'input[name^=';  echo $this->_vars['item']['field_name'];  echo ']\').removeAttr(\'checked\');
									});
								});
							'; ?>
</script>	
						<?php elseif ($this->_vars['item']['field_type'] == 'text'): ?>
							<input type="text" name="<?php echo $this->_vars['item']['field_name']; ?>
" value="<?php echo $this->_run_modifier($this->_vars['item']['value'], 'escape', 'plugin', 1); ?>
" maxlength="<?php echo $this->_run_modifier($this->_vars['item']['settings_data_array']['max_char'], 'escape', 'plugin', 1); ?>
" <?php if ($this->_vars['item']['settings_data_array']['max_char'] < 11): ?>class="short"<?php elseif ($this->_vars['item']['settings_data_array']['max_char'] > 1100): ?>class="long"<?php endif; ?>>
						<?php elseif ($this->_vars['item']['field_type'] == 'textarea'): ?>
							<textarea name="<?php echo $this->_vars['item']['field_name']; ?>
"><?php echo $this->_run_modifier($this->_vars['item']['value'], 'escape', 'plugin', 1); ?>
</textarea>
						<?php elseif ($this->_vars['item']['field_type'] == 'checkbox'): ?>
							<input type="checkbox" name="<?php echo $this->_vars['item']['field_name']; ?>
" value="1" <?php if ($this->_vars['item']['value'] == '1'): ?>checked<?php endif; ?>>
						<?php endif; ?>
						&nbsp;
						<?php if ($this->_vars['item']['comment']): ?><p class="help"><?php echo $this->_vars['item']['comment']; ?>
</p><?php endif; ?>
						</div>
					</div>
					<?php endif; ?>
					<?php endforeach; endif; ?>
					<div class="b">
						<input type="submit" name="btn_save_description" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>">
					</div>
					<input type="hidden" name="field_editor_section_gid" value="<?php echo $this->_vars['section_item']['gid']; ?>
">
					<input type="hidden" name="save_btn" value="1">
					</form>
				</div>
			</div>
			<?php endforeach; endif; ?>
			<?php elseif ($this->_vars['section_gid'] == 'gallery' && ( $this->_vars['data']['operation_type'] == 'sale' || $this->_vars['data']['operation_type'] == 'rent' )): ?>
			<link rel="stylesheet" href="<?php echo $this->_vars['site_root'];  echo $this->_vars['js_folder']; ?>
jquery.imgareaselect/jquery.imgareaselect.css" type="text/css" />
			<?php echo tpl_function_js(array('file' => 'ajaxfileupload.min.js'), $this);?>
			<?php echo tpl_function_js(array('file' => 'gallery-uploads.js'), $this);?>
			<?php echo tpl_function_js(array('file' => 'jquery.rotate.js'), $this);?>
			<?php echo tpl_function_js(array('module' => users_services,'file' => 'available_view.js'), $this);?>
			<?php echo tpl_function_js(array('file' => 'jquery.imgareaselect/jquery.imgareaselect.min.js'), $this);?>
			<?php echo tpl_function_js(array('module' => 'upload_gallery','file' => 'photo-edit.js'), $this);?>
			<div class="rollup-box">
				<div class="title">
					<div class="btn-link"><ins class="with-icon<?php if ($this->_vars['step'] != 1): ?> i-expand<?php else: ?> i-collapse<?php endif; ?>"></ins></div>
					<h2><?php echo l('header_listing_photo', 'listings', '', 'text', array()); ?></h2>
				</div>
				<div class="cont<?php if ($this->_vars['step'] != 1): ?> hide<?php endif; ?>">
					<p class="settings">
						<?php if ($this->_vars['gallery_settings']['max_items_count']):  echo l('max_photo_header', 'listings', '', 'text', array()); ?>: <span><?php echo $this->_vars['gallery_settings']['max_items_count']; ?>
</span><br><?php endif; ?>
						<?php if ($this->_vars['gallery_settings']['upload_settings']['max_size_str']):  echo l('max_photo_size_header', 'listings', '', 'text', array()); ?>: <span><?php echo $this->_vars['gallery_settings']['upload_settings']['max_size_str']; ?>
</span><br><?php endif; ?>
						<?php if ($this->_vars['gallery_settings']['upload_settings']['min_width'] && $this->_vars['gallery_settings']['upload_settings']['min_height']):  echo l('min_photo_width_and_height_header', 'listings', '', 'text', array()); ?>: <span><?php echo $this->_vars['gallery_settings']['upload_settings']['min_width']; ?>
x<?php echo $this->_vars['gallery_settings']['upload_settings']['min_height']; ?>
 <?php echo l('photo_width_and_height_unit', 'listings', '', 'text', array()); ?></span><br><?php endif; ?>
						<?php if ($this->_vars['gallery_settings']['upload_settings']['file_formats']):  echo l('text_accepted_file_types', 'listings', '', 'text', array()); ?>: <span><?php echo $this->_run_modifier(', ', 'implode', 'PHP', 1, $this->_vars['gallery_settings']['upload_settings']['file_formats']); ?>
</span><br><?php endif; ?>
					</p>
					
					<p><?php echo l('photo_upload_header', 'listings', '', 'text', array()); ?></p>
					
					<script><?php echo '
						var gUpload;
						var intervalID;
						var rotateAngle = -2;
						$(function(){
							gUpload = new galleryUploads({
								siteUrl: \'';  echo $this->_vars['site_url'];  echo '\',
								objectId: \'';  echo $this->_vars['data']['id'];  echo '\',
								maxPhotos: \'';  echo $this->_vars['data']['max_photos'];  echo '\',
								maxPhotoError: \'';  echo l('error_max_photos_reached', 'listings', '', 'js', array());  echo '\',
								uploadPhotoUrl: \'listings/photo_upload/';  echo $this->_vars['data']['id'];  echo '/\',
								savePhotoDataUrl: \'listings/ajax_save_photo_data/';  echo $this->_vars['data']['id'];  echo '/\',
								saveSortingUrl: \'listings/ajax_save_photo_sorting/';  echo $this->_vars['data']['id'];  echo '\',
								deletePhotoUrl: \'listings/ajax_delete_photo/';  echo $this->_vars['data']['id'];  echo '/\',
								formPhotoUrl: \'listings/ajax_photo_form/';  echo $this->_vars['data']['id'];  echo '/\',
								reloadBlockUrl: \'listings/ajax_reload_photo_block/';  echo $this->_vars['data']['id'];  echo '/\',
								reloadCallback: function(id){
									var photo_edit_block = $(\'#photo_edit_block\');
									if(!photo_edit_block.length) return;
									var photo_comment = photo_edit_block.find(\'#photo_comment\').val();
									photo_edit_block.find(\'#photo_source_view_box .statusbar .panel\').html(photo_comment);
									if(photo_comment){
										photo_edit_block.find(\'#photo_source_view_box .statusbar\').show();
									}else{
										photo_edit_block.find(\'#photo_source_view_box .statusbar\').hide();
									}
								},
								deleteCallback: function(id){
									if($(\'#photo_carousel .photo-info-area\').length == 0) $(\'#reorder_btn\').hide();
								},
								listItemID: \'photo_carousel\',
								closeAfterSave: false,
							});		
							$(\'#photo_carousel\').sortable(\'disable\');
							$(\'#reorder_btn\').bind(\'click\', function(){
								$(\'#photo_carousel\').addClass(\'rotate\');
								$(\'#photo_carousel\').find(\'.photo-info-area .photo-area\').each(function(index, item){
									var childs = $(item).children();
									$(childs[0]).rotateLeft(rotateAngle);
								});
								$(this).hide().next().show();
								$(\'#photo_carousel\').sortable(\'enable\');
							}).next().bind(\'click\', function(){
								$(\'#photo_carousel\').find(\'.photo-info-area .photo-area\').each(function(index, item){
									$(item).parent().find(\'.canvas\').html(\'\').hide();
									$(item).show();
									$(item).next().show();
								});
								$(this).hide().prev().show();
								$(\'#photo_carousel\').removeClass(\'rotate\');
								$(\'#photo_carousel\').sortable(\'disable\');
							});
						});
					'; ?>
</script>
				
					<form method="post" action="<?php echo $this->_run_modifier($this->_vars['data']['action'], 'escape', 'plugin', 1); ?>
" name="save_photo_form" id="save_photo_form" enctype="multipart/form-data">
					<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => "uploader_callback")); tpl_block_capture(array('assign' => "uploader_callback"), null, $this); ob_start();  echo '
						function(name, data){
							if(typeof(data.id) != \'undefined\' && data.id > 0){
								gUpload.save_photo_data(data.id);
								$(\'#reorder_btn\').show();
							}
						}
					';  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
					<?php echo tpl_function_block(array('name' => 'uploader_block','module' => 'uploads','field_name' => 'photo_file','form_id' => 'save_photo_form','url' => 'listings/photo_upload/'.$this->_vars['data']['id'],'callback' => $this->_vars['uploader_callback']), $this);?>
					
					<div class="clr"></div>
					
					<ol class="blocks" id="photo_carousel">
						<?php if (is_array($this->_vars['data']['photos']) and count((array)$this->_vars['data']['photos'])): foreach ((array)$this->_vars['data']['photos'] as $this->_vars['photo']): ?>
						<li id="pItem<?php echo $this->_vars['photo']['id']; ?>
">
							<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->module_path. "listings". $this->module_templates.  $this->get_current_theme_gid('', '"listings"'). "photo_view_block.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
						</li>
						<?php endforeach; endif; ?>
					</ol>
					<div class="clr"></div>			
					<div class="b">
						<a href="javascript:void(0)" class="btn-link <?php if ($this->_vars['data']['photo_count'] == 0): ?>hide<?php endif; ?>" id="reorder_btn"><ins class="with-icon i-reorder"></ins><?php echo l('btn_photo_reorder', 'listings', '', 'button', array()); ?></a>
						<a class="hide btn-link" href="javascript:void(0)"><ins class="with-icon i-larr"></ins><?php echo l('btn_cancel', 'start', '', 'text', array()); ?></a>
					</div>					
					<input type="hidden" name="save_btn" value="1">
					</form>
					<div class="clr"></div>
				</div>
			</div>
			<div class="rollup-box">			
				<div class="title">
					<div class="btn-link"><ins class="with-icon<?php if ($this->_vars['step'] != 2): ?> i-expand<?php else: ?> i-collapse<?php endif; ?>"></ins></div>
					<h2><?php echo l('header_listing_virtual_tour', 'listings', '', 'text', array()); ?></h2>
				</div>
				<div class="cont<?php if ($this->_vars['step'] != 2): ?> hide<?php endif; ?>">
					<p class="settings">
						<?php if ($this->_vars['vtour_settings']['max_items_count']):  echo l('max_panorama_header', 'listings', '', 'text', array()); ?>: <span><?php echo $this->_vars['vtour_settings']['max_items_count']; ?>
</span><br><?php endif; ?>
						<?php if ($this->_vars['vtour_settings']['upload_settings']['max_size_str']):  echo l('max_panorama_size_header', 'listings', '', 'text', array()); ?>: <span><?php echo $this->_vars['vtour_settings']['upload_settings']['max_size_str']; ?>
</span><br><?php endif; ?>
						<?php if ($this->_vars['vtour_settings']['upload_settings']['min_width'] && $this->_vars['vtour_settings']['upload_settings']['min_height']):  echo l('min_panorama_width_and_height_header', 'listings', '', 'text', array()); ?>: <span><?php echo $this->_vars['vtour_settings']['upload_settings']['min_width']; ?>
x<?php echo $this->_vars['vtour_settings']['upload_settings']['min_height']; ?>
 <?php echo l('photo_width_and_height_unit', 'listings', '', 'text', array()); ?></span><br><?php endif; ?>
						<?php if ($this->_vars['vtour_settings']['upload_settings']['file_formats']):  echo l('text_accepted_file_types', 'listings', '', 'text', array()); ?>: <span><?php echo $this->_run_modifier(', ', 'implode', 'PHP', 1, $this->_vars['vtour_settings']['upload_settings']['file_formats']); ?>
</span><br><?php endif; ?>
					</p>
					
					<p><?php echo l('panorama_upload_header', 'listings', '', 'text', array()); ?></p>
					
					<script><?php echo '
						var vtourUpload;
						var rotateAngle = -2;
						$(function(){
							vtourUpload = new galleryUploads({
								siteUrl: \'';  echo $this->_vars['site_url'];  echo '\',
								objectId: \'';  echo $this->_vars['data']['id'];  echo '\',
								fileElementId: \'panorama_file\',
								commentElementId: \'panorama_comment\',
								maxPhotos: \'';  echo $this->_vars['data']['max_panorama'];  echo '\',
								maxPhotoError: \'';  echo l('error_max_panorama_reached', 'listings', '', 'js', array());  echo '\',
								uploadPhotoUrl: \'listings/panorama_upload/';  echo $this->_vars['data']['id'];  echo '/\',
								savePhotoDataUrl: \'listings/ajax_save_panorama_data/';  echo $this->_vars['data']['id'];  echo '/\',
								saveSortingUrl: \'listings/ajax_save_panorama_sorting/';  echo $this->_vars['data']['id'];  echo '\',
								deletePhotoUrl: \'listings/ajax_delete_panorama/';  echo $this->_vars['data']['id'];  echo '/\',
								formPhotoUrl: \'listings/ajax_panorama_form/';  echo $this->_vars['data']['id'];  echo '/\',
								reloadBlockUrl: \'listings/ajax_reload_panorama_block/';  echo $this->_vars['data']['id'];  echo '/\',
								reloadCallback: function(id){
									$.get(\'';  echo $this->_vars['site_url'];  echo 'listings/ajax_reload_panorama/';  echo $this->_vars['data']['id'];  echo '/\'+id, {}, function(data){
										$(\'#panorama_block\').html(data);
									});
									var photo_edit_block = $(\'#photo_edit_block\');
									if(!photo_edit_block.length) return;
									var photo_comment = photo_edit_block.find(\'#panorama_comment\').val();
									photo_edit_block.find(\'#photo_source_view_box .statusbar .panel\').html(photo_comment);
									if(photo_comment){
										photo_edit_block.find(\'#photo_source_view_box .statusbar\').show();
									}else{
										photo_edit_block.find(\'#photo_source_view_box .statusbar\').hide();
									}
								},
								deleteCallback: function(id){
									if($(\'#panorama_carousel .photo-info-area\').length == 0) $(\'#reorder_vtour_btn\').hide();
									$(\'#panorama_block\').html(\'\');
								},
								listItemID: \'panorama_carousel\',
								closeAfterSave: false,
							});	
							$(\'#panorama_carousel\').sortable(\'disable\');
							$(\'#reorder_vtour_btn\').bind(\'click\', function(){
								$(\'#panorama_carousel\').addClass(\'rotate\');
								$(\'#panorama_carousel\').find(\'.photo-info-area .photo-area\').each(function(index, item){
									var childs = $(item).children();
									$(childs[0]).rotateLeft(rotateAngle);
								});
								$(this).hide().next().show();
								$(\'#panorama_carousel\').sortable(\'enable\');
							}).next().bind(\'click\', function(){
								$(\'#panorama_carousel\').find(\'.photo-info-area .photo-area\').each(function(index, item){
									$(item).parent().find(\'.canvas\').html(\'\').hide();
									$(item).show();
									$(item).next().show();
								});
								$(\'#panorama_carousel\').removeClass(\'rotate\');
								$(this).hide().prev().show();
								$(\'#panorama_carousel\').sortable(\'disable\');
							});
						});
					'; ?>
</script>
			
					<form method="post" action="<?php echo $this->_run_modifier($this->_vars['data']['action'], 'escape', 'plugin', 1); ?>
" name="save_panorama_form" id="save_panorama_form" enctype="multipart/form-data">
					<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => "uploader_callback")); tpl_block_capture(array('assign' => "uploader_callback"), null, $this); ob_start();  echo '
						function(name, data){
							if(typeof(data.id) != \'undefined\' && data.id > 0){
								vtourUpload.save_photo_data(data.id);
								$.get(\'';  echo $this->_vars['site_url'];  echo 'listings/ajax_reload_panorama/';  echo $this->_vars['data']['id'];  echo '/\'+data.id, {}, function(data){
									$(\'#reorder_vtour_btn\').show();
									$(\'#panorama_block\').html(data);
								});
							}
						}
					';  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
					<?php echo tpl_function_block(array('name' => 'uploader_block','module' => 'uploads','field_name' => 'panorama_file','form_id' => 'save_panorama_form','url' => 'listings/panorama_upload/'.$this->_vars['data']['id'],'callback' => $this->_vars['uploader_callback']), $this);?>
					
					<div class="clr"></div>
					
					<div id="panorama_block" class="edit"><?php if ($this->_vars['data']['virtual_tour_count']):  echo tpl_function_block(array('name' => virtual_tour_block,'module' => listings,'data' => $this->_vars['data']['virtual_tour']['0']), $this); endif; ?></div>
					
					<ol class="blocks" id="panorama_carousel">
						<?php if (is_array($this->_vars['data']['virtual_tour']) and count((array)$this->_vars['data']['virtual_tour'])): foreach ((array)$this->_vars['data']['virtual_tour'] as $this->_vars['panorama']): ?>
						<li id="pItem<?php echo $this->_vars['panorama']['id']; ?>
">
							<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->module_path. "listings". $this->module_templates.  $this->get_current_theme_gid('', '"listings"'). "panorama_view_block.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
						</li>
						<?php endforeach; endif; ?>
					</ol>
					<div class="clr"></div>			
					<div class="b">
						<a href="javascript:void(0)" class="btn-link <?php if ($this->_vars['data']['is_vtour'] == 0): ?>hide<?php endif; ?>" id="reorder_vtour_btn"><ins class="with-icon i-reorder"></ins><?php echo l('btn_panorama_reorder', 'listings', '', 'button', array()); ?></a>
						<a class="hide btn-link" href="javascript:void(0)"><ins class="with-icon i-larr"></ins><?php echo l('btn_cancel', 'start', '', 'text', array()); ?></a>
					</div>
					<input type="hidden" name="save_btn" value="1">
					</form>
					<div class="clr"></div>
				</div>
			</div>
						<div class="rollup-box">			
				<div class="title">
					<div class="btn-link"><ins class="with-icon<?php if ($this->_vars['step'] != 3): ?> i-expand<?php else: ?> i-collapse<?php endif; ?>"></ins></div>
					<h2><?php echo l('header_listing_file', 'listings', '', 'text', array()); ?></h2>
				</div>
				<div class="cont<?php if ($this->_vars['step'] != 3): ?> hide<?php endif; ?>">
					<p class="settings">
						<?php echo l('max_file_header', 'listings', '', 'text', array()); ?>: <span>1</span><br>
						<?php if ($this->_vars['file_settings']['max_size_str']):  echo l('max_file_size_header', 'listings', '', 'text', array()); ?>: <span><?php echo $this->_vars['file_settings']['max_size_str']; ?>
</span><br><?php endif; ?>
						<?php if ($this->_vars['file_settings']['file_formats']):  echo l('text_accepted_file_types', 'listings', '', 'text', array()); ?>: <span><?php echo $this->_run_modifier(', ', 'implode', 'PHP', 1, $this->_vars['file_settings']['file_formats']); ?>
</span><br><?php endif; ?>
					</p>
					<form method="post" action="<?php echo $this->_run_modifier($this->_vars['data']['action'], 'escape', 'plugin', 1); ?>
" name="save_file_form" id="save_file_form" enctype="multipart/form-data">
					<div class="r">
						<div class="f"><?php echo l('field_listing_file', 'listings', '', 'text', array()); ?>:&nbsp;* </div>
						<div class="v">
							<input type="file" name="listing_file">
							<?php if ($this->_vars['data']['is_file']): ?>
							<br><br><a href="<?php echo $this->_vars['data']['listing_file_content']['file_url']; ?>
" target="_blank"><?php echo l('field_file_download', 'listings', '', 'text', array()); ?></a>
							<br><br><input type="checkbox" name="listing_file_delete" value="1" id="uichb"><label for="uichb"><?php echo l('field_file_delete', 'listings', '', 'text', array()); ?></label><br><br>
							<?php endif; ?>
						</div>
					</div>
					<div class="r">
						<div class="f"><?php echo l('field_listing_file_name', 'listings', '', 'text', array()); ?>:&nbsp;* </div>
						<div class="v"><input type="text" name="data[listing_file_name]" value="<?php echo $this->_run_modifier($this->_vars['data']['listing_file_name'], 'escape', 'plugin', 1); ?>
"></div>
					</div>
					<div class="r">
						<div class="f"><?php echo l('field_listing_file_comment', 'listings', '', 'text', array()); ?>: </div>
						<div class="v"><textarea name="data[listing_file_comment]" rows="10" cols="80"><?php echo $this->_run_modifier($this->_vars['data']['listing_file_comment'], 'escape', 'plugin', 1); ?>
</textarea></div>
					</div>
					<div class="b"><input type="submit" name="btn_save_file" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div>
					<input type="hidden" name="save_btn" value="1">
					</form>
				</div>
			</div>
									<div class="rollup-box">			
				<div class="title">
					<div class="btn-link"><ins class="with-icon<?php if ($this->_vars['step'] != 4): ?> i-expand<?php else: ?> i-collapse<?php endif; ?>"></ins></div>
					<h2><?php echo l('header_listing_video', 'listings', '', 'text', array()); ?></h2>
				</div>
				<div class="cont<?php if ($this->_vars['step'] != 4): ?> hide<?php endif; ?>">
					<p class="settings">
						<?php echo l('max_video_header', 'listings', '', 'text', array()); ?>: <span>1</span><br>
						<?php if ($this->_vars['video_settings']['max_size_str']):  echo l('max_video_size_header', 'listings', '', 'text', array()); ?>: <span><?php echo $this->_vars['video_settings']['max_size_str']; ?>
</span><br><?php endif; ?>
						<?php if ($this->_vars['video_settings']['file_formats']):  echo l('text_accepted_file_types', 'listings', '', 'text', array()); ?>: <span><?php echo $this->_run_modifier(', ', 'implode', 'PHP', 1, $this->_vars['video_settings']['file_formats']); ?>
</span><br><?php endif; ?>
					</p>
					<form method="post" action="<?php echo $this->_run_modifier($this->_vars['data']['action'], 'escape', 'plugin', 1); ?>
" name="save_video_form" id="save_video_form" enctype="multipart/form-data">
					<div class="r">
						<div class="f"><?php echo l('field_video', 'listings', '', 'text', array()); ?>:&nbsp;* </div>
						<div class="v">
							<input type="file" name="listing_video">
							<?php if ($this->_vars['data']['listing_video']): ?>
								<br><br><?php echo l('field_video_status', 'listings', '', 'text', array()); ?>:
								<?php if ($this->_vars['data']['listing_video_data']['status'] == 'end' && $this->_vars['data']['listing_video_data']['errors']): ?>
								<font color="red"><?php if (is_array($this->_vars['data']['listing_video_data']['errors']) and count((array)$this->_vars['data']['listing_video_data']['errors'])): foreach ((array)$this->_vars['data']['listing_video_data']['errors'] as $this->_vars['item']):  echo $this->_vars['item']; ?>
<br><?php endforeach; endif; ?></font>
								<?php elseif ($this->_vars['data']['listing_video_data']['status'] == 'end'): ?>
								<font color="green"><?php echo l('field_video_status_end', 'listings', '', 'text', array()); ?></font><br>
								<?php elseif ($this->_vars['data']['listing_video_data']['status'] == 'images'): ?>
								<font color="yellow"><?php echo l('field_video_status_images', 'listings', '', 'text', array()); ?></font><br>
								<?php elseif ($this->_vars['data']['listing_video_data']['status'] == 'waiting'): ?> 
								<font color="yellow"><?php echo l('field_video_status_waiting', 'listings', '', 'text', array()); ?></font><br>
								<?php elseif ($this->_vars['data']['listing_video_data']['status'] == 'start'): ?> 
								<font color="yellow"><?php echo l('field_video_status_start', 'listings', '', 'text', array()); ?></font><br>
								<?php endif; ?>
								<?php if ($this->_vars['data']['listing_video_content']['embed']): ?><br><?php echo $this->_vars['data']['listing_video_content']['embed'];  endif; ?>
								<br><input type="checkbox" name="listing_video_delete" value="1" id="uvchb"><label for="uvchb"><?php echo l('field_video_delete', 'listings', '', 'text', array()); ?></label>
							<?php endif; ?>
						</div>
					</div>					
					<div class="b">
						<input type="submit" name="btn_save_video" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>">
					</div>
					<input type="hidden" name="save_btn" value="1">
					</form>
				</div>
			</div>
						<?php elseif ($this->_vars['section_gid'] == 'map'): ?>
						<script><?php echo '
				if(typeof(get_listing_type_data) == \'undefined\'){
					function get_listing_type_data(type){
						$(\'#map_type\').val(type);
					}
				}
				if(typeof(get_listing_zoom_data) == \'undefined\'){
					function get_listing_zoom_data(zoom){
						$(\'#map_zoom\').val(zoom);
					}
				}
				if(typeof(get_listing_drag_data) == \'undefined\'){
					function get_listing_drag_data(point_gid, lat, lon){
						$(\'#lat\').val(lat);
						$(\'#lon\').val(lon);
					}
				}
			'; ?>
</script>
			<div class="rollup-box">
				<form method="post" action="<?php echo $this->_run_modifier($this->_vars['data']['action'], 'escape', 'plugin', 1); ?>
" name="save_form" id="save_form" enctype="multipart/form-data">
					<div class="r">
						<div class="f"><?php echo l('field_geomap', 'listings', '', 'text', array()); ?>:</div>
						<div class="v"><?php echo tpl_function_block(array('name' => show_default_map,'module' => geomap,'object_id' => $this->_vars['data']['id'],'gid' => 'listing_view','markers' => $this->_vars['markers'],'settings' => $this->_vars['map_settings'],'width' => '630','height' => '400'), $this);?></div>
					</div>	
					<div class="b">
						<input type="submit" name="btn_save" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>">
					</div>
					<input type="hidden" name="map[view_type]" value="<?php echo $this->_vars['listing_map_settings']['view_type']; ?>
" id="map_type">
					<input type="hidden" name="map[zoom]" value="<?php echo $this->_vars['listing_map_settings']['zoom']; ?>
" id="map_zoom">
					<input type="hidden" name="data[lat]" value="<?php echo $this->_run_modifier($this->_vars['data']['lat'], 'escape', 'plugin', 1); ?>
" id="lat">
					<input type="hidden" name="data[lon]" value="<?php echo $this->_run_modifier($this->_vars['data']['lon'], 'escape', 'plugin', 1); ?>
" id="lon">
					<input type="hidden" name="save_btn" value="1">
				</form>
			</div>
						<?php elseif ($this->_vars['section_gid'] == 'calendar' && $this->_vars['data']['operation_type'] == 'rent'): ?>
			<div class="rollup-box">
				<div class="title">
					<div class="btn-link"><ins class="with-icon<?php if ($this->_vars['step'] != 1): ?> i-expand<?php else: ?> i-collapse<?php endif; ?>"></ins></div>
					<h2><?php echo l('header_calendar_settings', 'listings', '', 'text', array()); ?></h2>
				</div>
				<div class="cont <?php if ($this->_vars['step'] != 1): ?>hide<?php endif; ?>">
					<form method="post" action="<?php echo $this->_run_modifier($this->_vars['data']['action'], 'escape', 'plugin', 1); ?>
" name="save_form" id="save_form" enctype="multipart/form-data">				
						<div class="r">
							<div class="f"><?php echo l('field_calendar_enabled', 'listings', '', 'text', array()); ?>:</div>
							<div class="v">
								<input type="hidden" name="data[use_calendar]" value="0">
								<input type="checkbox" name="data[use_calendar]" <?php if ($this->_vars['data']['use_calendar']): ?>checked<?php endif; ?>>
							</div>
						</div>
						<div class="r">
							<div class="f"><?php echo l('field_calendar_period_min', 'listings', '', 'text', array()); ?>:</div>
							<div class="v">
								<input type="text" name="data[calendar_period_min]" value="<?php echo $this->_run_modifier($this->_vars['data']['calendar_period_min'], 'escape', 'plugin', 1); ?>
" class="middle">
							</div>
						</div>
						<div class="r">
							<div class="f"><?php echo l('field_calendar_period_max', 'listings', '', 'text', array()); ?>:</div>
							<div class="v">
								<input type="text" name="data[calendar_period_max]" value="<?php echo $this->_run_modifier($this->_vars['data']['calendar_period_max'], 'escape', 'plugin', 1); ?>
" class="middle">
							</div>
						</div>
						<div class="b">
							<input type="submit" name="btn_save" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>">
						</div>
						<input type="hidden" name="save_btn" value="1">
					</form>
				</div>
			</div>
			<div class="rollup-box">
				<div class="title">
					<div class="btn-link"><ins class="with-icon<?php if ($this->_vars['step'] != 2): ?> i-expand<?php else: ?> i-collapse<?php endif; ?>"></ins></div>
					<h2><?php echo l('header_calendar_period_management', 'listings', '', 'text', array()); ?></h2>
				</div>
				<div class="cont <?php if ($this->_vars['step'] != 2): ?>hide<?php endif; ?>">
					<div class="view-section"><?php echo tpl_function_block(array('name' => 'listings_calendar_block','module' => 'listings','listing' => $this->_vars['data'],'template' => 'edit','count' => 3), $this);?></div>
				</div>
			</div>
			<?php endif; ?>

			<div class="b outside">
				<input type="button" name="btn_previous" value="<?php echo l('nav_prev', 'start', '', 'button', array()); ?>" class="btn hide" id="edit_listings_prev" />
				<input type="button" name="btn_next" value="<?php echo l('nav_next', 'start', '', 'button', array()); ?>" class="btn hide" id="edit_listings_next" />
				<a href="<?php echo $this->_vars['site_url']; ?>
listings/my" class="btn-link"><ins class="with-icon i-larr"></ins><?php echo l('link_back_to_my_listings', 'listings', '', 'text', array()); ?></a>
			</div>
			<div class="clr"></div>
		</div>
	</div>
<div class="clr"></div>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
