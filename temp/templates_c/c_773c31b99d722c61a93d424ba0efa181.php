<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-02 15:55:27 KRAT */ ?>

<?php echo l('sort_by', $this->_vars['sort_module'], '', 'text', array()); ?>: 
<select id="sorter-select-<?php echo $this->_vars['sort_rand']; ?>
">
<?php if (is_array($this->_vars['sort_links']) and count((array)$this->_vars['sort_links'])): foreach ((array)$this->_vars['sort_links'] as $this->_vars['key'] => $this->_vars['item']): ?>
<option value="<?php echo $this->_vars['key']; ?>
"<?php if ($this->_vars['key'] == $this->_vars['sort_order']): ?> selected<?php endif; ?>><?php echo $this->_vars['item']; ?>
</option>
<?php endforeach; endif; ?>
</select>
<input type="button" id="sorter-dir-<?php echo $this->_vars['sort_rand']; ?>
" name="sorter_btn" value="" class="sorter-btn i-sorter with-icon-small<?php if ($this->_vars['sort_direction'] == 'ASC'): ?> up<?php else: ?> down<?php endif; ?>">
