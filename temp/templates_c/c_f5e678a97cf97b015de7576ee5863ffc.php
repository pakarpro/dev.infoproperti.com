<?php require_once('/opt/ip/system/libraries/template_lite/plugins/modifier.escape.php'); $this->register_modifier("escape", "tpl_modifier_escape");  require_once('/opt/ip/system/libraries/template_lite/plugins/modifier.truncate.php'); $this->register_modifier("truncate", "tpl_modifier_truncate");  require_once('/opt/ip/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-02-23 10:51:42 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array('load_type' => 'ui'));
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<div class="actions">
	<ul>
		<li><div class="l"><a href="<?php echo $this->_vars['site_url']; ?>
admin/listings/add_kpr_button">Add KPR button</a></div></li>               
	</ul>
    <font style="color:white; font-size:16px; ">Recommended dimension for KPR button is 167 pixels wide and 20 pixels tall</font>
</div>
<form id="listings_form" action="" method="post">
<table cellspacing="0" cellpadding="0" class="data" width="100%">
<tr>
	<th><?php echo l('field_photo', 'listings', '', 'text', array()); ?></th>
	<th class="w200">Bank</th>
	<th class="w150">Status</th>
	<th class="w150">&nbsp;</th>
</tr>
<?php if (is_array($this->_vars['kpr_button']) and count((array)$this->_vars['kpr_button'])): foreach ((array)$this->_vars['kpr_button'] as $this->_vars['item']): ?>
<tr<?php if (!($this->_vars['counter'] % 2)): ?> class="zebra"<?php endif; ?>>
	<td><img src="<?php echo $this->_vars['base_url']; ?>
uploads/kpr_button/<?php echo $this->_vars['item']['image']; ?>
" alt="<?php echo $this->_run_modifier($this->_run_modifier($this->_vars['item']['output_name'], 'truncate', 'plugin', 1, 20), 'escape', 'plugin', 1); ?>
" style="width:167px; height:20px;"></td>
	<td><?php echo $this->_run_modifier($this->_vars['item']['name'], 'truncate', 'plugin', 1, 23); ?>
</td>
	<td>
    	<?php if ($this->_vars['item']['status'] == '1'): ?>
        	Active
        <?php else: ?>
        	Disabled
        <?php endif; ?>
    </td>
	<td class="icons">
		<?php if ($this->_vars['item']['status']): ?>
		<a href="<?php echo $this->_vars['site_url']; ?>
admin/listings/activate_kpr_button/<?php echo $this->_vars['item']['id']; ?>
/0"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-full.png" width="16" height="16" border="0" alt="<?php echo l('link_deactivate_listing', 'listings', '', 'button', array()); ?>" title="<?php echo l('link_deactivate_listing', 'listings', '', 'button', array()); ?>"></a>
		<?php else: ?>
		<a href="<?php echo $this->_vars['site_url']; ?>
admin/listings/activate_kpr_button/<?php echo $this->_vars['item']['id']; ?>
/1"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-empty.png" width="16" height="16" border="0" alt="<?php echo l('link_activate_listing', 'listings', '', 'button', array()); ?>" title="<?php echo l('link_activate_listing', 'listings', '', 'button', array()); ?>"></a>
		<?php endif; ?>
		<a href="<?php echo $this->_vars['site_url']; ?>
admin/listings/edit_kpr_button/<?php echo $this->_vars['item']['id']; ?>
"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-edit.png" width="16" height="16" border="0" alt="<?php echo l('link_edit_listing', 'listings', '', 'button', array()); ?>" title="<?php echo l('link_edit_listing', 'listings', '', 'button', array()); ?>"></a>
		<a href="<?php echo $this->_vars['site_url']; ?>
admin/listings/delete_kpr_button/<?php echo $this->_vars['item']['id']; ?>
" onclick="javascript: if(!confirm('<?php echo l('note_delete_listing', 'listings', '', 'js', array()); ?>')) return false;"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-delete.png" width="16" height="16" border="0" alt="<?php echo l('link_delete_listing', 'listings', '', 'button', array()); ?>" title="<?php echo l('link_delete_listing', 'listings', '', 'button', array()); ?>"></a>
	</td>
</tr>
<?php endforeach; else: ?>
<?php $this->assign('colspan', 6); ?>
<?php $this->assign('colspan', $this->_vars['colspan']); ?><tr><td colspan="<?php echo $this->_vars['colspan']; ?>
" class="center"><?php echo l('no_listings', 'listings', '', 'text', array()); ?></td></tr>
<?php endif; ?>
</table>
</form>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
