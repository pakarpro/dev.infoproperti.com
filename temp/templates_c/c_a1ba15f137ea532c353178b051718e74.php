<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-02 14:30:12 KRAT */ ?>

					<?php switch($this->_vars['listing']['price_period']): case '1':  ?>
				<p class="calendar-year"><?php echo $this->_vars['years']; ?>
</p>
				<div class="calendar-cont">
					<?php if (is_array($this->_vars['months']) and count((array)$this->_vars['months'])): foreach ((array)$this->_vars['months'] as $this->_vars['month']): ?>
					<div class="calendar">
						<p class="calendar-month"><?php echo $this->_vars['month']['name']; ?>
</p>
						<table cellpadding="0" cellspacing="0">
							<thead>
								<tr>							
									<?php if (is_array($this->_vars['weeks']) and count((array)$this->_vars['weeks'])): foreach ((array)$this->_vars['weeks'] as $this->_vars['key'] => $this->_vars['item']): ?>
									<th><?php echo $this->_vars['item']; ?>
</th>
									<?php endforeach; endif; ?>
								</tr>
							</thead>
							<tbody>
								<?php if (is_array($this->_vars['month']['weeks']) and count((array)$this->_vars['month']['weeks'])): foreach ((array)$this->_vars['month']['weeks'] as $this->_vars['key'] => $this->_vars['week']): ?>
								<tr>							
									<?php if (is_array($this->_vars['week']) and count((array)$this->_vars['week'])): foreach ((array)$this->_vars['week'] as $this->_vars['item']): ?>
									<td class="<?php if ($this->_vars['item']['start']): ?>period-start<?php endif; ?> <?php if ($this->_vars['item']['end']): ?>period-end<?php endif; ?>">
										<div class="<?php echo $this->_vars['item']['status']; ?>
" title="<?php echo tpl_function_ld_option(array('i' => 'booking_status','gid' => 'listings','option' => $this->_vars['item']['status'],'type' => 'button'), $this);?>"><?php echo $this->_vars['item']['day']; ?>
</div>
										<?php if ($this->_vars['item']['status'] == 'book'): ?>
										<?php if ($this->_vars['item']['comment']): ?>
										<span class="period-comment" title="<?php echo $this->_run_modifier($this->_vars['item']['comment'], 'escape', 'plugin', 1); ?>
">i</span>
										<?php else: ?>
										<span>&nbsp;</span>
										<?php endif; ?>
										<?php else: ?>
										<?php echo $this->_vars['item']['price']; ?>

										<?php endif; ?>
									</td>
									<?php endforeach; endif; ?>
								</tr>
								<?php endforeach; endif; ?>
							</tbody>
						</table>
					</div>
					<?php endforeach; endif; ?>
					<div class="clr"></div>
				</div>
			<?php break; case '2':  ?>
				<div class="calendar-cont">
					<?php if (is_array($this->_vars['years']) and count((array)$this->_vars['years'])): foreach ((array)$this->_vars['years'] as $this->_vars['key'] => $this->_vars['year']): ?>
					<div class="calendar">
						<p class="calendar-months-year"><?php echo $this->_vars['key']; ?>
</p>
						<table cellpadding="0" cellspacing="0">
							<tr>
								<?php if (is_array($this->_vars['year']['0']) and count((array)$this->_vars['year']['0'])): foreach ((array)$this->_vars['year']['0'] as $this->_vars['item']): ?>
								<td>
									<div class="<?php echo $this->_vars['item']['status']; ?>
"><?php echo $this->_vars['item']['month']; ?>
</div>
									<?php if ($this->_vars['item']['status'] == 'book'): ?>
									<?php if ($this->_vars['item']['comment']): ?>
									<span class="period-comment" title="<?php echo $this->_run_modifier($this->_vars['item']['comment'], 'escape', 'plugin', 1); ?>
">i</span>
									<?php else: ?>
									<span>&nbsp;</span>
									<?php endif; ?>
									<?php else: ?>
									<?php echo $this->_vars['item']['price']; ?>

									<?php endif; ?>
								</td>
								<?php endforeach; endif; ?>
							</tr>	
							<tr>
								<?php if (is_array($this->_vars['year']['1']) and count((array)$this->_vars['year']['1'])): foreach ((array)$this->_vars['year']['1'] as $this->_vars['item']): ?>
								<td><div class="<?php echo $this->_vars['item']['status']; ?>
"><?php echo $this->_vars['item']['month']; ?>
</div>
									<?php if ($this->_vars['item']['status'] == 'book'): ?>
									<?php if ($this->_vars['item']['comment']): ?>
									<span class="period-comment" title="<?php echo $this->_run_modifier($this->_vars['item']['comment'], 'escape', 'plugin', 1); ?>
">i</span>
									<?php else: ?>
									<span>&nbsp;</span>
									<?php endif; ?>
									<?php else: ?>
									<?php echo $this->_vars['item']['price']; ?>

									<?php endif; ?>
								</td>
								<?php endforeach; endif; ?>
							</tr>
							<tr>
								<?php if (is_array($this->_vars['year']['2']) and count((array)$this->_vars['year']['2'])): foreach ((array)$this->_vars['year']['2'] as $this->_vars['item']): ?>
								<td>
									<div class="<?php echo $this->_vars['item']['status']; ?>
"><?php echo $this->_vars['item']['month']; ?>
</div>
									<?php if ($this->_vars['item']['status'] == 'book'): ?>
									<?php if ($this->_vars['item']['comment']): ?>
									<span class="period-comment" title="<?php echo $this->_run_modifier($this->_vars['item']['comment'], 'escape', 'plugin', 1); ?>
">i</span>
									<?php else: ?>
									<span>&nbsp;</span>
									<?php endif; ?>
									<?php else: ?>
									<?php echo $this->_vars['item']['price']; ?>

									<?php endif; ?>
									</td>
								<?php endforeach; endif; ?>
							</tr>
						</table>
					</div>
					<?php endforeach; endif; ?>
				</div>
		<?php break; endswitch; ?>
