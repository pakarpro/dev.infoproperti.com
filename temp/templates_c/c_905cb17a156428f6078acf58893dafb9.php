<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.block.php'); $this->register_function("block", "tpl_function_block");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.escape.php'); $this->register_modifier("escape", "tpl_modifier_escape");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2014-09-30 15:01:17 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array('load_type' => 'ui'));
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

<form method="post" action="<?php echo $this->_run_modifier($this->_vars['data']['action'], 'escape', 'plugin', 1); ?>
" name="save_form" id="save_form" enctype="multipart/form-data">
<div class="edit-form n150">
	<div class="row header"><?php if ($this->_vars['data']['id']):  echo l('admin_header_wish_list_edit', 'listings', '', 'text', array());  else:  echo l('admin_header_wish_list_add', 'listings', '', 'text', array());  endif; ?></div>
	<div class="row">
		<div class="h"><?php echo l('field_wish_list_name', 'listings', '', 'text', array()); ?>:&nbsp;* </div>
		<div class="v" id="wish_list_name">
			<?php if (is_array($this->_vars['langs']) and count((array)$this->_vars['langs'])): foreach ((array)$this->_vars['langs'] as $this->_vars['lang_id'] => $this->_vars['lang_item']): ?>
			<?php $this->assign('name', 'name_'.$this->_vars['lang_id']); ?>
			<input type="<?php if ($this->_vars['lang_id'] == $this->_vars['current_lang_id']): ?>text<?php else: ?>hidden<?php endif; ?>" name="data[name_<?php echo $this->_vars['lang_id']; ?>
]" value="<?php echo $this->_run_modifier($this->_vars['data'][$this->_vars['name']], 'escape', 'plugin', 1); ?>
" lang-editor="value" lang-editor-type="data-name" lang-editor-lid="<?php echo $this->_vars['lang_id']; ?>
" />
			<?php endforeach; endif; ?>
			<a href="#" lang-editor="button" lang-editor-type="data-name"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-translate.png" width="16" height="16"></a>
		</div>
	</div>
</div>
<div class="btn"><div class="l"><input type="submit" name="btn_save" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div></div>
<a class="cancel" href="<?php echo $this->_vars['site_url']; ?>
admin/listings/wish_lists"><?php echo l('btn_cancel', 'start', '', 'text', array()); ?></a>
</form>
<div class="clr"></div>
<?php echo tpl_function_block(array('name' => lang_inline_editor,'module' => start), $this);?>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
