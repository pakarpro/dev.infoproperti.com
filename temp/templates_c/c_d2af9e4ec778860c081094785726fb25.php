<?php require_once('/opt/ip/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-02-23 10:49:32 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array('load_type' => 'ui'));
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<div class="actions">
	<ul>
		<li><div class="l"><a href="<?php echo $this->_vars['site_url']; ?>
admin/listings/add_kpr_button">Add KPR button</a></div></li>
		<li><div class="l"><a href="<?php echo $this->_vars['site_url']; ?>
admin/listings/kpr_button">Back</a></div></li> 
	</ul>
	&nbsp;
</div>
<?php echo $this->_vars['button']->id; ?>

<form method="post" action="<?php if ($this->_vars['button']):  echo $this->_vars['site_url']; ?>
admin/listings/update_kpr_button/<?php echo $this->_vars['button']['id'];  else:  echo $this->_vars['site_url']; ?>
admin/listings/save_kpr_button<?php endif; ?>" name="save_form" enctype="multipart/form-data">
	<div class="edit-form n150">
		<div class="row header"><?php if ($this->_vars['button']['id']): ?>Edit KPR button<?php else: ?>Add KPR button<?php endif; ?></div>
		<div class="row">
			<div class="h">Bank</div>
			<div class="v">
            	<select name="bank_id">
                		<option value="empty">-- SELECT BANK --</option>
                	<?php if (is_array($this->_vars['bank']) and count((array)$this->_vars['bank'])): foreach ((array)$this->_vars['bank'] as $this->_vars['item']): ?>
                    	<option value="<?php echo $this->_vars['item']->bankID; ?>
" <?php if ($this->_vars['bank_id'] == $this->_vars['item']->bankID): ?>selected = 'selected'<?php elseif ($this->_vars['button']['bank_id'] == $this->_vars['item']->bankID): ?> selected = 'selected'<?php endif; ?> ><?php echo $this->_vars['item']->name; ?>
</option>
                    <?php endforeach; endif; ?>
                </select>
            </div>
		</div>
		<?php if ($this->_vars['data']['user_id'] == 0): ?>
		<div class="row">
			<div class="h">Active</div>
			<div class="v"><input type="checkbox" value="1" name="status" <?php if ($this->_vars['status']): ?>checked<?php elseif ($this->_vars['button']['status'] == '1'): ?>checked<?php endif; ?>></div>
		</div>
		<?php endif; ?>
		<div class="row">
			<div class="h">Image</div>
			<div class="v">
            	<input type="file" name="userfile" >
                <?php if ($this->_vars['button']): ?>
                <input type="hidden" name="old_image" value="<?php echo $this->_vars['button']['image']; ?>
" />
				<input type="hidden" name="button_id" value="<?php echo $this->_vars['button']['id']; ?>
" />                
                <?php endif; ?>
			</div>
		</div>


	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div></div>
	<a class="cancel" href="<?php echo $this->_vars['site_url']; ?>
admin/listings/kpr_button"><?php echo l('btn_cancel', 'start', '', 'text', array()); ?></a>
</form>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
