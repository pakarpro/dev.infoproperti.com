<?php require_once('/opt/ip/system/libraries/template_lite/plugins/function.js.php'); $this->register_function("js", "tpl_function_js");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.pagination.php'); $this->register_function("pagination", "tpl_function_pagination");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.counter.php'); $this->register_function("counter", "tpl_function_counter");  require_once('/opt/ip/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-12-02 11:37:59 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->module_path. "start". $this->module_templates.  $this->get_current_theme_gid('', '"start"'). "left_panel.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

<div class="rc">
	<div class="content-block">

		<h1><?php echo l('header_my_banners', 'banners', '', 'text', array()); ?></h1>
		<div class="content-value">
			<a class="btn-link fright" href="<?php echo $this->_vars['site_url']; ?>
banners/edit"><ins class="with-icon i-list-add"></ins><span><?php echo l('link_add_banner', 'banners', '', 'text', array()); ?></span></a>
			<br><br>
			<table class="list">
			<tr id="sorter_block">
				<th class="w30"><?php echo l('field_number', 'banners', '', 'text', array()); ?></th>
				<th class="w30">&nbsp;</th>
				<th><?php echo l('field_name', 'banners', '', 'text', array()); ?></th>
				<th><?php echo l('field_approve', 'banners', '', 'text', array()); ?></th>
				<th class="w150">&nbsp;</th>
			</tr>
			<?php if (is_array($this->_vars['banners']) and count((array)$this->_vars['banners'])): foreach ((array)$this->_vars['banners'] as $this->_vars['banner']): ?>
			<?php echo tpl_function_counter(array('print' => false,'assign' => counter), $this);?>
			<tr>
				<td class="centered"><?php echo $this->_vars['counter']; ?>
</td>
				<td class="view-banner">
					<a href='#' onclick="return false;" id="view_<?php echo $this->_vars['banner']['id']; ?>
" class="btn-link" title="<?php echo l('link_view_banner', 'banners', '', 'text', array()); ?>"><ins class="with-icon i-eye"></ins></a>
					<div id="view_<?php echo $this->_vars['banner']['id']; ?>
_content" style="display: none">
						<?php if ($this->_vars['banner']['banner_type'] == 1): ?><img src="<?php echo $this->_vars['banner']['media']['banner_image']['file_url']; ?>
" width="<?php echo $this->_vars['banner']['banner_place_obj']['width']; ?>
" height="<?php echo $this->_vars['banner']['banner_place_obj']['height']; ?>
" /><?php else:  echo $this->_vars['banner']['html'];  endif; ?>
					</div>
				</td>
				<td>
				<b><?php echo $this->_vars['banner']['name']; ?>

				<?php if ($this->_vars['banner']['banner_place_obj']): ?>
				(<?php echo $this->_vars['banner']['banner_place_obj']['name']; ?>
 <?php echo $this->_vars['banner']['banner_place_obj']['width']; ?>
X<?php echo $this->_vars['banner']['banner_place_obj']['height']; ?>
)
				<?php endif; ?></b><br>
				<?php $this->assign('limit', ''); ?>
				<?php if ($this->_vars['banner']['number_of_views']): ?>
				<?php $this->assign('limit', 1); ?>
				<?php echo l('shows', 'banners', '', 'text', array()); ?> - <?php echo $this->_vars['banner']['number_of_views']; ?>

				<br/>
				<?php endif; ?>
				<?php if ($this->_vars['banner']['number_of_clicks']): ?>
				<?php $this->assign('limit', 1); ?>
				<?php echo l('clicks', 'banners', '', 'text', array()); ?> - <?php echo $this->_vars['banner']['number_of_clicks']; ?>

				<br/>
				<?php endif; ?>
				<?php if ($this->_vars['banner']['expiration_date'] && $this->_vars['banner']['expiration_date'] != '0000-00-00 00:00:00'): ?>
				<?php $this->assign('limit', 1); ?>
				<?php echo l('till', 'banners', '', 'text', array()); ?> - <?php echo $this->_vars['banner']['expiration_date']; ?>

				<?php endif; ?>
				<?php if (! $this->_vars['limit']):  echo l('never_stop', 'banners', '', 'text', array());  endif; ?>

				</td>
				<td>
				<?php if ($this->_vars['banner']['approve'] == '1'): ?><span class="status"><ins></ins><?php echo l('approved', 'banners', '', 'text', array()); ?></span>
				<?php elseif ($this->_vars['banner']['approve'] == '-1'): ?><span class="status decline"><ins></ins><?php echo l('declined', 'banners', '', 'text', array()); ?></span>
				<?php else: ?><span class="status wait"><ins></ins><?php echo l('not_approved', 'banners', '', 'text', array()); ?></span><?php endif; ?>
				</td>
				<td class="r">
					<a href="<?php echo $this->_vars['site_url']; ?>
banners/delete/<?php echo $this->_vars['banner']['id']; ?>
" onclick="javascript: if(!confirm('<?php echo l('note_delete_banner', 'banners', '', 'js', array()); ?>')) return false;" class="btn-link fright"><ins class="with-icon i-delete"></ins></a>
				<?php if ($this->_vars['banner']['approve'] == '1'): ?>
					<a href="<?php echo $this->_vars['site_url']; ?>
banners/statistic/<?php echo $this->_vars['banner']['id']; ?>
" class="btn-link fright" title="<?php echo l('link_banner_stat', 'banners', '', 'button', array()); ?>"><ins class="with-icon i-stat"></ins></a>
					<?php if (! $this->_vars['banner']['status']): ?><a href="<?php echo $this->_vars['site_url']; ?>
banners/activate/<?php echo $this->_vars['banner']['id']; ?>
" class="btn-link fright" title="<?php echo l('link_banner_activate', 'banners', '', 'text', array()); ?>"><ins class="with-icon i-start"></ins></a><?php endif; ?>
				<?php endif; ?>
				</td>
			</tr>
			<?php endforeach; else: ?>
			<tr>
				<td class="empty" colspan=5><?php echo l('no_banners', 'banners', '', 'text', array()); ?></td>
			</tr>
			<?php endif; ?>
			</table>

			<?php echo tpl_function_pagination(array('data' => $this->_vars['page_data'],'type' => 'full'), $this);?>
			<br>

		</div>
	</div>
</div>
<?php echo tpl_function_js(array('file' => 'easyTooltip.min.js'), $this);?>
<script type='text/javascript'>
<?php echo '
$(function(){
	$("td.view-banner > a").each(function(){
		var id = $(this).attr(\'id\')+\'_content\';
		$(this).easyTooltip({useElement: id});
	});
});
'; ?>

</script>

<div class="clr"></div>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
