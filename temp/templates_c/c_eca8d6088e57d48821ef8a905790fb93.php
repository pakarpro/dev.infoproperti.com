<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-02 15:55:32 KRAT */ ?>

<?php echo tpl_function_js(array('module' => spam,'file' => 'spam.js'), $this); if ($this->_vars['template'] == 'link'):  if (! $this->_vars['is_send']): ?><a href="<?php echo $this->_vars['site_url']; ?>
spam/mark_as_spam" data-id="<?php echo $this->_vars['object_id']; ?>
" data-type="<?php echo $this->_vars['type']['gid']; ?>
" id="mark-as-span-<?php echo $this->_vars['rand']; ?>
" class="btn-link link-r-margin"><?php echo l('btn_mark_as_spam', 'spam', '', 'button', array()); ?></a><?php endif;  else: ?>
<a <?php if (! $this->_vars['is_send']): ?>href="<?php echo $this->_vars['site_url']; ?>
spam/mark_as_spam"<?php endif; ?> data-id="<?php echo $this->_vars['object_id']; ?>
" data-type="<?php echo $this->_vars['type']['gid']; ?>
" id="mark-as-span-<?php echo $this->_vars['rand']; ?>
" class="btn-link link-r-margin" title="<?php echo l('btn_mark_as_spam', 'spam', '', 'button', array()); ?>"><ins class="with-icon <?php if ($this->_vars['is_send']): ?>g<?php endif; ?> i-spam"></ins></a>
<?php endif;  if (! $this->_vars['is_send']): ?>
<script><?php echo '
$(function(){
	';  if ($this->_vars['is_guest']):  echo '
	$(\'#mark-as-span-';  echo $this->_vars['rand'];  echo '\').bind(\'click\', function(){
		$(\'html, body\').animate({
			scrollTop: $("#ajax_login_link").offset().top
		}, 2000);
		$("#ajax_login_link").click();
		return false;
	});
	';  else:  echo '
	new Spam({
		siteUrl: \'';  echo $this->_vars['site_url'];  echo '\', 
		use_form: ';  if ($this->_vars['type']['form_type'] != 'checkbox'): ?>true<?php else: ?>false<?php endif;  echo ',
		';  if ($this->_vars['is_spam_owner']): ?>isOwner: true,<?php endif;  echo '
		mark_as_spam_btn: \''; ?>
mark-as-span-<?php echo $this->_vars['rand'];  echo '\',
	});		
	';  endif;  echo '
});
'; ?>
</script>
<?php endif; ?>
