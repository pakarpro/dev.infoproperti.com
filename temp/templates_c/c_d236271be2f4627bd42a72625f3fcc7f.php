<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.escape.php'); $this->register_modifier("escape", "tpl_modifier_escape");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.helper.php'); $this->register_function("helper", "tpl_function_helper");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2014-06-12 14:30:25 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
  echo tpl_function_helper(array('func_name' => get_admin_level1_menu,'helper_name' => menu,'func_param' => 'admin_contact_menu'), $this);?>
<div class="actions">&nbsp;</div>

<form method="post" action="<?php echo $this->_run_modifier($this->_vars['data']['action'], 'escape', 'plugin', 1); ?>
" name="save_form">
	<div class="edit-form n150">
		<div class="row header"><?php echo l('admin_header_contact_settings', 'contact', '', 'text', array()); ?></div>
		<div class="row">
			<div class="h"><label for="contact_settings_make_copies"><?php echo l('field_contact_make_copies', 'contact', '', 'text', array()); ?></label>: </div>
			<div class="v">
				<input type="hidden" name="contact_make_copies" value="0"> 
				<input type="checkbox" name="contact_make_copies" value="1" id="contact_settings_make_copies" <?php if ($this->_vars['data']['contact_make_copies']): ?>checked="checked"<?php endif; ?>> 
			</div>
		</div>
		<div class="row zebra">
			<div class="h"><label for="contact_settings_need_approve"><?php echo l('field_contact_need_approve', 'contact', '', 'text', array()); ?></label>: </div>
			<div class="v">
				<input type="hidden" name="contact_need_approve" value="0"> 
				<input type="checkbox" name="contact_need_approve" value="1" id="contact_settings_need_approve" <?php if ($this->_vars['data']['contact_need_approve']): ?>checked="checked"<?php endif; ?> <?php if (! $this->_vars['data']['contact_make_copies']): ?>disabled<?php endif; ?>> 
			</div>
		</div>		
		<div class="row">
			<div class="h"><label for="contact_send_mail"><?php echo l('field_contact_send_mail', 'contact', '', 'text', array()); ?></label>: </div>
			<div class="v">
				<input type="hidden" name="contact_send_mail" value="0"> 
				<input type="checkbox" name="contact_send_mail" value="1" id="contact_send_mail" <?php if ($this->_vars['data']['contact_send_mail']): ?>checked="checked"<?php endif; ?>> 
				&nbsp;&nbsp;
				<?php echo l('field_contact_admin_email', 'contact', '', 'text', array()); ?>
				<input type="text" name="contact_admin_email" value="<?php echo $this->_run_modifier($this->_vars['data']['contact_admin_email'], 'escape', 'plugin', 1); ?>
" id="contact_admin_email" <?php if (! $this->_vars['data']['contact_send_mail']): ?>disabled<?php endif; ?>> 
			</div>
		</div>
	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div></div>
</form>
<div class="clr"></div>
<script><?php echo '
$(function(){
	$(\'#contact_settings_make_copies\').bind(\'change\', function(){
		if(this.checked){
			$(\'#contact_settings_need_approve\').removeAttr(\'disabled\');
		}else{
			$(\'#contact_settings_need_approve\').removeAttr(\'checked\').attr(\'disabled\', \'disabled\');
			$(\'#contact_send_mail\').removeAttr(\'checked\').attr(\'disabled\', \'disabled\');
			$(\'#contact_admin_email\').attr(\'disabled\', \'disabled\');
		}
	});
	$(\'#contact_settings_need_approve\').bind(\'change\', function(){
		if(this.checked){
			$(\'#contact_send_mail\').removeAttr(\'disabled\');
		}else{
			$(\'#contact_send_mail\').removeAttr(\'checked\').attr(\'disabled\', \'disabled\');
			$(\'#contact_admin_email\').attr(\'disabled\', \'disabled\');
		}
	});
	$(\'#contact_send_mail\').bind(\'change\', function(){
		if(this.checked){
			$(\'#contact_admin_email\').removeAttr(\'disabled\');
		}else{
			$(\'#contact_admin_email\').attr(\'disabled\', \'disabled\');
		}
	});
});
'; ?>
</script>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
