<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-02 15:54:48 KRAT */ ?>

		<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => "review_callback")); tpl_block_capture(array('assign' => "review_callback"), null, $this); ob_start();  echo '
			function(data){
				window.location.href = \'';  echo tpl_function_seolink(array('module' => 'users','method' => 'view','data' => $this->_vars['user'],'section' => 'reviews'), $this);?>#m_reviews<?php echo '\';
			}
		';  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
		<!-- content for user profile-->
		<h1><?php echo tpl_function_seotag(array('tag' => 'header_text'), $this);?></h1>
		<div class="actions noPrint">
			<div class="act-icon contact"><?php echo tpl_function_block(array('name' => 'button_contact','module' => 'mailbox','user_id' => $this->_vars['user']['id'],'user_type' => $this->_vars['user_type']), $this);?></div>
			<div class="act-icon send-review"><?php echo tpl_function_block(array('name' => 'send_review_block','module' => 'reviews','object_id' => $this->_vars['user']['id'],'type_gid' => 'users_object','responder_id' => $this->_vars['user']['id'],'success' => $this->_vars['review_callback'],'is_owner' => $this->_vars['is_user_owner']), $this);?></div>
			<div class="act-icon view-all-listing"><?php echo tpl_function_block(array('name' => 'user_listings_button','module' => 'listings','user' => $this->_vars['user']), $this);?></div>
			<div class="act-icon pdf-view"><a href="<?php echo tpl_function_seolink(array('method' => 'view','module' => 'users','data' => $this->_vars['user'],'pdf' => 'yes'), $this);?>" id="pdf_btn" class="btn-link link-r-margin" title="<?php echo l('link_pdf', 'listings', '', 'button', array()); ?>"><ins class="with-icon i-pdf"></ins></a></div>
			<div class="act-icon print-view"><a href="javascript:void(0);" id="print_btn" class="btn-link link-r-margin" title="<?php echo l('link_print', 'listings', '', 'button', array()); ?>" onclick="javascript: window.print(); return false;"><ins class="with-icon i-print"></ins></a></div>
			<div class="act-icon report-spam"><?php echo tpl_function_block(array('name' => 'mark_as_spam_block','module' => 'spam','object_id' => $this->_vars['user']['id'],'type_gid' => 'users_object','is_owner' => $this->_vars['is_user_owner']), $this);?></div>
		</div>
		<div class="clr"></div>
		<div class="view_user">
			<div class="image">
				<a href="<?php echo tpl_function_seolink(array('module' => 'users','method' => 'view','data' => $this->_vars['user']), $this);?>"><img src="<?php echo $this->_vars['user']['media']['user_logo']['thumbs']['big']; ?>
" title="<?php echo $this->_run_modifier($this->_vars['user']['output_name'], 'escape', 'plugin', 1); ?>
"></a>
			</div>
			<div class="body">
				<?php 
$this->assign('no_info_str', l('no_information', 'start', '', 'text', array()));
 ?>
				<div class="t-1 user-info-left">
					<!-- user info left -->
										<?php echo tpl_function_block(array('name' => get_rate_block,'module' => reviews,'rating_data_main' => $this->_vars['user']['review_sorter'],'type_gid' => 'users_object','template' => 'normal','read_only' => 'true'), $this);?><br>
					<span><?php echo l('field_rate', 'users', '', 'text', array()); ?>:</span> <?php echo $this->_vars['user']['review_sorter']; ?>
<br>
					<span><?php echo l('field_reviews_count', 'users', '', 'text', array()); ?>:</span> <?php echo $this->_vars['user']['review_count']; ?>
<br><br>
										<div id="content_m_contacts" class="view-section<?php if ($this->_vars['section_gid'] != 'contacts'): ?> hide<?php endif; ?> print_block"><?php echo $this->_vars['user_content']['contacts']; ?>
</div>
					<span class="status_text"><?php if ($this->_vars['user']['is_featured']):  echo l('status_featured', 'users', '', 'text', array());  endif; ?></span>
					<?php if ($this->_vars['user']['user_type'] == "company" && $this->_vars['user']['agent_count']): ?><span><?php echo l('field_agent_count', 'users', '', 'text', array()); ?>:</span> <?php echo $this->_vars['user']['agent_count']; ?>
<br><?php endif; ?>
					
					
				</div>
				<div class="t-2 user-info-right">
					<!-- user info right -->
					<span><?php echo l('field_register', 'users', '', 'text', array()); ?>:</span><br> <?php echo $this->_run_modifier($this->_vars['user']['date_created'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']); ?>
<br><br>
					<span><?php echo l('field_views', 'users', '', 'text', array()); ?>:</span> <?php echo $this->_vars['user']['views']; ?>
<br>
					<?php if ($this->_vars['user']['listings_for_sale_count']): ?><span><?php echo l('field_listings_for_sale', 'users', '', 'text', array()); ?>:</span> <?php echo $this->_vars['user']['listings_for_sale_count']; ?>
<br><?php endif; ?>
					<?php if ($this->_vars['user']['listings_for_buy_count']): ?><span><?php echo l('field_listings_for_buy', 'users', '', 'text', array()); ?>:</span> <?php echo $this->_vars['user']['listings_for_buy_count']; ?>
<br><?php endif; ?>
					<?php if ($this->_vars['user']['listings_for_rent_count']): ?><span><?php echo l('field_listings_for_rent', 'users', '', 'text', array()); ?>:</span> <?php echo $this->_vars['user']['listings_for_rent_count']; ?>
<br><?php endif; ?>
					<?php if ($this->_vars['user']['listings_for_lease_count']): ?><span><?php echo l('field_listings_for_lease', 'users', '', 'text', array()); ?>:</span> <?php echo $this->_vars['user']['listings_for_lease_count']; ?>
<br><?php endif; ?>
                    <!-- #modification# -->
				</div>
				
				<div class="r user-info-about-us">
					<div class="f" ><?php echo l('field_description', 'users', '', 'text', array()); ?>: </div>
					<div class="v">
						<?php if ($this->_vars['user']['no_access_contact']): ?><font class="gray_italic"><?php echo $this->_vars['not_access_str']; ?>
</font>
						<?php elseif (! $this->_vars['user']['contact_info']): ?><font class="gray_italic"><?php echo $this->_vars['no_info_str']; ?>
</font>
						<?php elseif (! $this->_vars['user']['is_contact']): ?><font class="gray_italic"><?php echo $this->_vars['buy_str']; ?>
</font>
						<?php elseif ($this->_vars['template'] == 'small'):  echo $this->_run_modifier($this->_vars['user']['contact_info'], 'truncate', 'plugin', 1, 55); ?>

						<?php else:  echo $this->_vars['user']['contact_info']; ?>

						<?php endif; ?>
					</div>
				</div>
				
			</div>
			<div class="clr"></div>
		</div>
		<div class="edit_block">
			<?php /* disable for now <?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->module_path. "users". $this->module_templates.  $this->get_current_theme_gid('"default"', '"users"'). "view_menu.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?> */ ?>
			<div id="user_block">
				<?php if ($this->_vars['user']['user_type_str'] == "agent"): ?><div id="content_m_company" class="view-section<?php if ($this->_vars['section_gid'] != 'company'): ?> hide<?php endif; ?> noPrint"><?php echo $this->_vars['user_content']['company']; ?>
</div><?php endif; ?>
				<div id="content_m_reviews" class="view-section<?php if ($this->_vars['section_gid'] != 'reviews'): ?> hide<?php endif; ?> noPrint"><?php echo $this->_vars['user_content']['reviews']; ?>
</div>
				<?php if ($this->_vars['user_type_str'] != "private"): ?><div id="content_m_map" class="view-section<?php if ($this->_vars['section_gid'] != 'map'): ?> hide<?php endif; ?> noPrint"><?php echo $this->_vars['user_content']['map']; ?>
</div><?php endif; ?>
				<!-- #modification# <div id="content_m_contacts" class="view-section<?php if ($this->_vars['section_gid'] != 'contacts'): ?> hide<?php endif; ?> print_block"><?php echo $this->_vars['user_content']['contacts']; ?>
</div> -->
				<div id="content_m_listings" class="view-section<?php if ($this->_vars['section_gid'] != 'listings'): ?> hide<?php endif; ?> noPrint"><?php echo $this->_vars['user_content']['listings']; ?>
</div>
			</div>
		</div>