<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.date_format.php'); $this->register_modifier("date_format", "tpl_modifier_date_format");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.truncate.php'); $this->register_modifier("truncate", "tpl_modifier_truncate");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.counter.php'); $this->register_function("counter", "tpl_function_counter");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.lower.php'); $this->register_modifier("lower", "tpl_modifier_lower");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.helper.php'); $this->register_function("helper", "tpl_function_helper");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-05-06 11:56:30 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
  echo tpl_function_helper(array('func_name' => get_admin_level1_menu,'helper_name' => menu,'func_param' => 'admin_import_menu'), $this);?>
<div class="actions">
	<ul>
		<li id="add"><div class="l"><a href="<?php echo $this->_vars['site_url']; ?>
admin/import/upload/"><?php echo l('btn_add', 'start', '', 'text', array()); ?></a></div></li>
	</ul>
	&nbsp;
</div>

<form id="import_form" action="" method="post">
<table cellspacing="0" cellpadding="0" class="data" width="100%">
	<tr>
		<th class="first w100"><?php echo l('field_data_driver', 'import', '', 'text', array()); ?></th>
		<th class="w100"><?php echo l('field_data_module', 'import', '', 'text', array()); ?></th>
		<th class="w50"><?php echo l('field_data_total', 'import', '', 'text', array()); ?></th>
		<th class="w50"><a href="<?php echo $this->_vars['sort_links']['date_created']; ?>
"<?php if ($this->_vars['order'] == 'date_created'): ?> class="<?php echo $this->_run_modifier($this->_vars['order_direction'], 'lower', 'plugin', 1); ?>
"<?php endif; ?>><?php echo l('field_data_date_created', 'import', '', 'text', array()); ?></a></th>
		<th class="w70">&nbsp;</th>
	</tr>
	<?php if (is_array($this->_vars['data']) and count((array)$this->_vars['data'])): foreach ((array)$this->_vars['data'] as $this->_vars['item']): ?>
		<?php echo tpl_function_counter(array('print' => false,'assign' => counter), $this);?>
		<tr<?php if (!($this->_vars['counter'] % 2)): ?> class="zebra"<?php endif; ?>>				
			<td><?php echo $this->_run_modifier($this->_vars['item']['driver']['output_name'], 'truncate', 'plugin', 1, 50); ?>
</td>
			<td><?php echo $this->_run_modifier($this->_vars['item']['module']['output_name'], 'truncate', 'plugin', 1, 50); ?>
</td>
			<td class="center"><?php echo $this->_vars['item']['total']; ?>
</td>
			<td class="center"><?php echo $this->_run_modifier($this->_vars['item']['date_created'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']); ?>
</td>
			<td class="icons">
				<a href="<?php echo $this->_vars['site_url']; ?>
admin/import/edit/<?php echo $this->_vars['item']['id']; ?>
"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-edit.png" width="16" height="16" border="0" alt="<?php echo l('link_edit_data', 'import', '', 'button', array()); ?>" title="<?php echo l('link_edit_data', 'import', '', 'button', array()); ?>"></a>
				<a href="<?php echo $this->_vars['site_url']; ?>
admin/import/delete/<?php echo $this->_vars['item']['id']; ?>
" onclick="javascript: if(!confirm('<?php echo l('note_delete_data', 'import', '', 'js', array()); ?>')) return false;"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-delete.png" width="16" height="16" border="0" alt="<?php echo l('link_delete_data', 'import', '', 'button', array()); ?>" title="<?php echo l('link_delete_data', 'import', '', 'button', array()); ?>"></a>
			</td>
		</tr>
	<?php endforeach; else: ?>
		<tr><td colspan="5" class="first center"><?php echo l('no_data', 'import', '', 'text', array()); ?></td></tr>
	<?php endif; ?>
</table>
</form>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "pagination.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

<script><?php echo '
	var reload_link = "';  echo $this->_vars['site_url']; ?>
admin/import/index<?php echo '";
	var filter = \'';  echo $this->_vars['filter'];  echo '\';
	var order = \'';  echo $this->_vars['order'];  echo '\';
	var loading_content;
	var order_direction = \'';  echo $this->_vars['order_direction'];  echo '\';
	function reload_this_page(value){
		var link = reload_link + filter + \'/\' + value + \'/\' + order + \'/\' + order_direction;
		location.href=link;
	}
'; ?>
</script>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
