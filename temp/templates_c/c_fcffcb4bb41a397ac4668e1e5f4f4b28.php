<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-02-22 01:40:44 KRAT */ ?>

<?php echo tpl_function_js(array('file' => 'jquery.reel.js'), $this);?>
<div id="panorama_box_<?php echo $this->_vars['virtual_tour_rand']; ?>
" class="panorama_box">
	<img src="<?php echo $this->_vars['virtual_tour_data']['media']['thumbs']['620_400']; ?>
" hspace="3" id="panorama_<?php echo $this->_vars['virtual_tour_rand']; ?>
" width="<?php echo $this->_vars['virtual_tour_width']; ?>
" height="<?php echo $this->_vars['virtual_tour_height']; ?>
">
	<div class="panorama-comment <?php if (! $this->_vars['virtual_tour_data']['comment']): ?>hide<?php endif; ?>">
		<div class="comment-panel" title="<?php echo $this->_vars['virtual_tour_data']['comment']; ?>
"><?php echo $this->_run_modifier($this->_vars['virtual_tour_data']['comment'], 'truncate', 'plugin', 1, 255); ?>
</div>
		<div class="background"></div>
	</div>
</div>		
<script><?php echo '
	$(function(){
		var panorama = $(\'.panorama\');
		if(panorama.length){
			panorama.bind(\'click\', function(){
				var vtour_box = $(\'#panorama_box_';  echo $this->_vars['virtual_tour_rand'];  echo '\');
				var vtour = vtour_box.find(\'#panorama_';  echo $this->_vars['virtual_tour_rand'];  echo '\');
				vtour.unreel();
				var item = $(this);
				var w = 400*item.attr(\'data-height\')/item.attr(\'data-height\');
				var comment = item.attr(\'data-comment\') || \'\';
				vtour.reel({
					brake: 0,
					timeout: 0,
					cursor: \'pointer\',
					path: item.attr(\'data-url\'), 
					image: item.attr(\'data-file\'), 
					suffix: \'\',
					indicator: 10,
					stitched: w,
					speed: -0.02,
					steppable: false,
					preloader: 0,
					frames: w,
				});
				if(comment){
					vtour_box.find(\'.panorama-comment\').removeClass(\'hide\');
				}else{
					vtour_box.find(\'.panorama-comment\').addClass(\'hide\');
				}
				vtour_box.find(\'.comment-panel\').html(comment);
				return false;
			});
							
			var item = $(panorama.get(0));
			var w = 400*item.attr(\'data-height\')/item.attr(\'data-height\');
			$(\'#panorama_';  echo $this->_vars['virtual_tour_rand'];  echo '\').reel({
				brake: 0,
				timeout: 0,
				cursor: \'pointer\',
				path: item.attr(\'data-url\'), 
				image: item.attr(\'data-file\'), 
				suffix: \'\', 
				indicator: 10,
				stitched: w,
				speed: -0.02,
				steppable: false,
				preloader: 0,
				frames: w,
			});
		}
	});
'; ?>
</script>
