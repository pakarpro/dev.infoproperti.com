<?php require_once('/opt/ip/system/libraries/template_lite/plugins/function.block.php'); $this->register_function("block", "tpl_function_block");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.seolink.php'); $this->register_function("seolink", "tpl_function_seolink");  require_once('/opt/ip/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  require_once('/opt/ip/system/libraries/template_lite/plugins/modifier.date_format.php'); $this->register_modifier("date_format", "tpl_modifier_date_format");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.seotag.php'); $this->register_function("seotag", "tpl_function_seotag");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-02 15:43:43 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<!-- fb share feature -->
<div id="fb-root"></div>
<script language="javascript">
<?php echo '
  window.fbAsyncInit = function() {
    FB.init({
      appId      : \'868222196624062\',
      xfbml      : true,
      version    : \'v2.5\'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, \'script\', \'facebook-jssdk\'));

   //twitter js
	!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?\'http\':\'https\';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+\'://platform.twitter.com/widgets.js\';fjs.parentNode.insertBefore(js,fjs);}}(document, \'script\', \'twitter-wjs\');

	//google+ JS
  (function() {
    var po = document.createElement(\'script\'); po.type = \'text/javascript\'; po.async = true;
    po.src = \'https://apis.google.com/js/platform.js\';
    var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(po, s);
  })();
'; ?>

   
   
</script>
<!-- end of fb share mod -->
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->module_path. "start". $this->module_templates.  $this->get_current_theme_gid('', '"start"'). "news_panel.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<!-- news detail view -->
<div class="rc">
	<div class="content-block">
    	<!-- mod for tags -->
        <?php if ($this->_vars['existing_tag']): ?>
        Tag:&nbsp;        
            <?php if (is_array($this->_vars['existing_tag']) and count((array)$this->_vars['existing_tag'])): foreach ((array)$this->_vars['existing_tag'] as $this->_vars['item']): ?>
                <div style="min-height:10px; display:inline-block; position:relative; margin-right:5px; background-color:white; border-bottom:1px solid black;">
                    <a href="<?php echo $this->_vars['site_url']; ?>
news/tags/<?php echo $this->_vars['item']['tag_name']; ?>
">
                        <div style="position:relative; float:left;">
                            <?php echo $this->_vars['item']['tag_name']; ?>
                    
                        </div>
                    </a>                                        
                </div>
            <?php endforeach; endif; ?>
        <?php endif; ?>    
        <!-- end of mod -->    
		<h1>
        	<?php echo tpl_function_seotag(array('tag' => 'header_text'), $this);?>
            <br/>
            <!-- Twitter tweet button -->
            <a href="https://twitter.com/share" class="twitter-share-button" data-text="<?php echo tpl_function_seotag(array('tag' => 'header_text'), $this);?>" data-via="inproperti" data-hashtags="infoproperti, rumah"<?php if ($this->_vars['googl_url']): ?>data-url="<?php echo $this->_vars['googl_url']; ?>
"<?php endif; ?>>Tweet</a>                
            <!-- FB share button -->
            <div class="fb-share-button" data-href="<?php echo $this->_vars['fb_share_url']; ?>
" data-layout="button_count" style="bottom:9px;"></div> 
            <!-- Place this tag where you want the +1 button to render. -->
            <div class="g-plusone" data-href="<?php echo $this->_vars['fb_share_url']; ?>
" data-size="medium"></div>                    
        </h1>
		<div class="news-view">
			<span class="date"><?php echo $this->_run_modifier($this->_vars['data']['date_add'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']); ?>
</span>
            <!-- #MOD# only show big image if it's news original code is if $data.img  -->
			
            <?php if (getimagesize ( $this->_vars['data']['media']['img']['thumbs']['big'] ) && $this->_vars['data']['news_type'] == 'news'): ?>
			<img src="<?php echo $this->_vars['data']['media']['img']['thumbs']['big']; ?>
" align="left" />
			<div class="clr"></div>
			<?php endif; ?>
			<?php if (! $this->_vars['data']['content']):  echo $this->_vars['data']['annotation'];  else:  echo $this->_vars['data']['content'];  endif; ?>
			<?php if ($this->_vars['data']['video_content']['embed']): ?>
				<p><?php echo $this->_vars['data']['video_content']['embed']; ?>
</p>
			<?php endif; ?>

			<?php if ($this->_vars['data']['feed_link']):  echo l('feed_source', 'news', '', 'text', array()); ?>: <a href="<?php echo $this->_vars['data']['feed_link']; ?>
"><?php echo $this->_vars['data']['feed']['title']; ?>
</a><?php endif; ?>
			<div class="clr"></div>
			<br>
			<div class="a-link">
				<a href="<?php echo tpl_function_seolink(array('module' => 'news','method' => 'index'), $this);?>"><?php echo l('link_back_to_news', 'news', '', 'text', array()); ?></a>
			</div>
		</div>
        <div class="fb-comments" data-href="<?php echo $this->_vars['current_url']; ?>
" data-width="783" data-numposts="10"></div>        
	</div>

	<?php echo tpl_function_block(array('name' => show_social_networks_like,'module' => social_networking), $this);?>
	<?php echo tpl_function_block(array('name' => show_social_networks_share,'module' => social_networking), $this);?>
	<?php echo tpl_function_block(array('name' => show_social_networks_comments,'module' => social_networking), $this);?>
</div>
<div class="clr"></div>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
