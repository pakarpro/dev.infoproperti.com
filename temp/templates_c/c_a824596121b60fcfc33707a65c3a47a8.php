<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.truncate.php'); $this->register_modifier("truncate", "tpl_modifier_truncate");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2014-12-16 12:24:29 KRAT */ ?>

	<div class="photo-info-area">
		<div class="photo-area">
		<img src="<?php echo $this->_vars['photo']['media']['thumbs']['200_200']; ?>
" hspace="3" onclick="javascript: gUpload.full_view('<?php echo $this->_vars['photo']['media']['thumbs']['620_400']; ?>
');" /><br>
		<b><?php echo l('photo_status', 'listings', '', 'text', array()); ?>: <?php if ($this->_vars['photo']['status']): ?><font class="stat-active"><?php echo l('photo_active', 'listings', '', 'text', array()); ?></font><?php else: ?><font class="stat-moder"><?php echo l('photo_moderate', 'listings', '', 'text', array()); ?></font><?php endif; ?></b><br>
		<?php if ($this->_vars['photo']['comment']):  echo $this->_run_modifier($this->_vars['photo']['comment'], 'truncate', 'plugin', 1, 50, '...', true); ?>
<br><?php endif; ?>
		</div>
		<a href="#" onclick="javascript: gUpload.open_edit_form(<?php echo $this->_vars['photo']['id']; ?>
); return false;"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-edit.png" width="16" height="16" border="0" alt="<?php echo l('btn_edit', 'start', '', 'button', array()); ?>" title="<?php echo l('btn_edit', 'start', '', 'button', array()); ?>"></a>
		<a href="#" onclick="javascript: gUpload.delete_photo(<?php echo $this->_vars['photo']['id']; ?>
); return false;"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-delete.png" width="16" height="16" border="0" alt="<?php echo l('btn_delete', 'start', '', 'button', array()); ?>" title="<?php echo l('btn_delete', 'start', '', 'button', array()); ?>"></a>
		<div class="clr"></div>
	</div>
