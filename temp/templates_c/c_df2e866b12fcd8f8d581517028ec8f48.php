<?php require_once('/opt/ip/system/libraries/template_lite/plugins/function.block.php'); $this->register_function("block", "tpl_function_block");  require_once('/opt/ip/system/libraries/template_lite/plugins/modifier.escape.php'); $this->register_modifier("escape", "tpl_modifier_escape");  require_once('/opt/ip/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-01-04 16:45:49 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
  $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->module_path. "start". $this->module_templates.  $this->get_current_theme_gid('', '"start"'). "left_panel.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
  echo $this->_vars['data']['js']; ?>

<div class="rc">
	<div class="content-block">
		<h1 class="inl"><?php echo l('header_payment_form', 'payments', '', 'text', array()); ?></h1>

		<div class="content-value">
			

			<form method="post" action="">
			<input type="hidden" name="payment_type_gid" value="<?php echo $this->_vars['data']['payment_type_gid']; ?>
">
			<input type="hidden" name="amount" value="<?php echo $this->_vars['data']['amount']; ?>
">
			<input type="hidden" name="currency_gid" value="<?php echo $this->_vars['data']['currency_gid']; ?>
">
			<input type="hidden" name="system_gid" value="<?php echo $this->_vars['data']['system_gid']; ?>
">
			<input type="hidden" name="payment_data[name]" value="<?php echo $this->_run_modifier($this->_vars['data']['payment_data']['name'], 'escape', 'plugin', 1); ?>
">

			<div class="edit_block">
				<div class="payment_table">
					<table>
						<tr>
							<td><?php echo l('field_payment_name', 'payments', '', 'text', array()); ?>:</td>
							<td class="value"><?php echo $this->_vars['data']['payment_data']['name']; ?>
 (<?php echo $this->_vars['data']['payment_type_gid']; ?>
)</td>
						</tr>
						<tr>
							<td><?php echo l('field_amount', 'payments', '', 'text', array()); ?>:</td>
							<td class="value"><?php echo tpl_function_block(array('name' => currency_format_output,'module' => start,'value' => $this->_vars['data']['amount'],'cur_gid' => $this->_vars['data']['currency_gid']), $this);?></td>
						</tr>
						<tr>
							<td><?php echo l('field_billing_type', 'payments', '', 'text', array()); ?>:</td>
							<td class="value"><?php echo $this->_vars['data']['system']['name']; ?>
</td>
						</tr>
						<?php if ($this->_vars['data']['system']['info_data']): ?>
						<tr>
							<td><?php echo l('field_info_data', 'payments', '', 'text', array()); ?>:</td>
							<td class="value"><?php echo $this->_vars['data']['system']['info_data']; ?>
</td>
						</tr>
						<?php endif; ?>
						<?php if (is_array($this->_vars['data']['map']) and count((array)$this->_vars['data']['map'])): foreach ((array)$this->_vars['data']['map'] as $this->_vars['key'] => $this->_vars['item']): ?>
						<tr>
							<td><?php echo $this->_vars['item']['name']; ?>
:</td>
							<td class="value">
								<?php if ($this->_vars['item']['type'] == 'text'): ?>
								<input type="text" name="map[<?php echo $this->_vars['key']; ?>
]" value="<?php echo $this->_vars['data']['payment_data'][$this->_vars['key']]; ?>
" <?php if ($this->_vars['item']['size'] == 'small'): ?>class="short"<?php elseif ($this->_vars['item']['size'] == 'big'): ?>class="long"<?php endif; ?>>
								<?php elseif ($this->_vars['item']['type'] == 'textarea'): ?>
								<textarea name="map[<?php echo $this->_vars['key']; ?>
]" rows="10" cols="40"><?php echo $this->_vars['data']['payment_data'][$this->_vars['key']]; ?>
</textarea>
								<?php endif; ?>
							</td>
						</tr>
						<?php endforeach; endif; ?>

					</table>
				</div>
				<div class="b outside">
					<input type="submit" class='btn' value="<?php echo l('btn_send', 'start', '', 'button', array()); ?>" name="btn_save" id="btn_pay">
					<a class="btn-link" href="<?php echo $this->_vars['site_url']; ?>
payments/statistic">
						<ins class="with-icon i-larr"></ins>
						<?php echo l('btn_cancel', 'start', '', 'text', array()); ?>
					</a>
				</div>
			</div>
			</form>
		</div>
	</div>
</div>
<div class="clr"></div>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
