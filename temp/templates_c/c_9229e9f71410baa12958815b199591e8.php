<?php require_once('/opt/ip/system/libraries/template_lite/plugins/function.math.php'); $this->register_function("math", "tpl_function_math");  require_once('/opt/ip/system/libraries/template_lite/plugins/modifier.date.php'); $this->register_modifier("date", "tpl_modifier_date");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-02 14:45:57 KRAT */ ?>

	<div id="booking_<?php if ($this->_vars['full_form']): ?>full_<?php endif; ?>block" class="<?php if (! $this->_vars['full_form'] && $this->_vars['listing']['price_period'] == 2): ?>edit_block<?php endif; ?> noPrint">
		<h2><?php echo l('header_order_add', 'listings', '', 'text', array()); ?></h2>
		<form method="post" action="" name="save_form" id="period_form<?php echo $this->_vars['rand']; ?>
" enctype="multipart/form-data">
			<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'booking_price')); tpl_block_capture(array('assign' => 'booking_price'), null, $this); ob_start(); ?>
				<?php if ($this->_vars['current_price'] > 0): ?>
					<?php echo tpl_function_block(array('name' => 'currency_format_output','module' => 'start','value' => $this->_vars['current_price'],'cur_gid' => $this->_vars['listing']['gid_currency']), $this);?>
				<?php else: ?>
					<?php echo l('text_negotiated_price_rent', 'listings', '', 'text', array()); ?>
				<?php endif; ?>
			<?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
			<div class="r <?php if (! $this->_vars['full_form'] && $this->_vars['listing']['price_period'] == 1): ?>fleft<?php endif; ?>">
				<div class="f"><?php echo l('field_booking_date_start', 'listings', '', 'text', array()); ?>:</div>
				<div class="v periodbox">
											<?php switch($this->_vars['listing']['price_period']): case '1':  ?>
							<input type="text" name="period[date_start]" value="<?php echo $this->_run_modifier($this->_run_modifier($this->_vars['current_date_start'], 'date_format', 'plugin', 1, $this->_vars['date_format']), 'escape', 'plugin', 1); ?>
" id="date_start<?php echo $this->_vars['rand']; ?>
" class="middle">
							<input type="hidden" name="date_start_alt" value="" id="alt_date_start<?php echo $this->_vars['rand']; ?>
">
							<script><?php echo '
								$(function(){
									$(\'#date_start';  echo $this->_vars['rand'];  echo '\').datepicker({dateFormat: \'';  echo $this->_vars['datepicker_date_format'];  echo '\', altFormat: \'';  echo $this->_vars['datepicker_alt_format'];  echo '\', altField: \'#alt_date_start';  echo $this->_vars['rand'];  echo '\', showOn: \'both\'});
								});
							'; ?>
</script>
						<?php break; case '2':  ?>
							<?php echo tpl_function_ld(array('i' => 'month-names','gid' => 'start','assign' => 'month_names'), $this);?>
							<select name="date_start_month" class="middle">
								<option value=""><?php echo $this->_vars['month_names']['header']; ?>
</option>
								<?php if (is_array($this->_vars['month_names']['option']) and count((array)$this->_vars['month_names']['option'])): foreach ((array)$this->_vars['month_names']['option'] as $this->_vars['key'] => $this->_vars['item']): ?>
								<option value="<?php echo $this->_vars['key']; ?>
" <?php if ($this->_vars['key'] == $this->_vars['current_month_start']): ?>selected<?php endif; ?>><?php echo $this->_vars['item']; ?>
</option>
								<?php endforeach; endif; ?>
							</select>
							<?php $this->assign('cyear', $this->_run_modifier('now', 'date', 'plugin', 1, 'Y')); ?>
							<select name="date_start_year" class="short">
								<option value=""><?php echo l('text_year', 'listings', '', 'text', array()); ?></option>
								<?php for($for1 = 0; ((0 < 10) ? ($for1 < 10) : ($for1 > 10)); $for1 += ((0 < 10) ? 1 : -1)):  $this->assign('i', $for1); ?>
								<?php echo tpl_function_math(array('equation' => 'x + y','x' => $this->_vars['cyear'],'y' => $this->_vars['i'],'assign' => 'year'), $this);?>
								<option value="<?php echo $this->_vars['year']; ?>
" <?php if ($this->_vars['year'] == $this->_vars['current_year_start']): ?>selected<?php endif; ?>><?php echo $this->_vars['year']; ?>
</option>
								<?php endfor; ?>
							</select>
					<?php break; endswitch; ?>
				</div>
			</div>
			<?php if (! $this->_vars['full_form'] && $this->_vars['listing']['price_period'] == 1): ?><div class="r fleft">&nbsp;</div><?php endif; ?>
			<div class="r <?php if (! $this->_vars['full_form'] && $this->_vars['listing']['price_period'] == 1): ?>fleft<?php endif; ?>">
				<div class="f"><?php echo l('field_booking_date_end', 'listings', '', 'text', array()); ?>:</div>
				<div class="v periodbox">
											<?php switch($this->_vars['listing']['price_period']): case '1':  ?>
							<input type="text" name="period[date_end]" value="<?php echo $this->_run_modifier($this->_run_modifier($this->_vars['current_date_end'], 'date_format', 'plugin', 1, $this->_vars['date_format']), 'escape', 'plugin', 1); ?>
" id="date_end<?php echo $this->_vars['rand']; ?>
" class="middle">
							<input type="hidden" name="date_end_alt" value="" id="alt_date_end<?php echo $this->_vars['rand']; ?>
">
							<script><?php echo '
								$(function(){
									$(\'#date_end';  echo $this->_vars['rand'];  echo '\').datepicker({dateFormat: \'';  echo $this->_vars['datepicker_date_format'];  echo '\', altFormat: \'';  echo $this->_vars['datepicker_alt_format'];  echo '\', altField: \'#alt_date_end';  echo $this->_vars['rand'];  echo '\', showOn: \'both\'});
								});
							'; ?>
</script>
						<?php break; case '2':  ?>
							<?php echo tpl_function_ld(array('i' => 'month-names','gid' => 'start','assign' => 'month_names'), $this);?>
							<select name="date_end_month" class="middle">
								<option value=""><?php echo $this->_vars['month_names']['header']; ?>
</option>
								<?php if (is_array($this->_vars['month_names']['option']) and count((array)$this->_vars['month_names']['option'])): foreach ((array)$this->_vars['month_names']['option'] as $this->_vars['key'] => $this->_vars['item']): ?>
								<option value="<?php echo $this->_vars['key']; ?>
" <?php if ($this->_vars['key'] == $this->_vars['current_month_end']): ?>selected<?php endif; ?>><?php echo $this->_vars['item']; ?>
</option>
								<?php endforeach; endif; ?>
							</select>
							<?php $this->assign('cyear', $this->_run_modifier('now', 'date', 'plugin', 1, 'Y')); ?>
							<select name="date_end_year" class="short">
								<option value=""><?php echo l('text_year', 'listings', '', 'text', array()); ?></option>
								<?php for($for1 = 0; ((0 < 10) ? ($for1 < 10) : ($for1 > 10)); $for1 += ((0 < 10) ? 1 : -1)):  $this->assign('i', $for1); ?>
								<?php echo tpl_function_math(array('equation' => 'x + y','x' => $this->_vars['cyear'],'y' => $this->_vars['i'],'assign' => 'year'), $this);?>
								<option value="<?php echo $this->_vars['year']; ?>
" <?php if ($this->_vars['year'] == $this->_vars['current_year_end']): ?>selected<?php endif; ?>><?php echo $this->_vars['year']; ?>
</option>
								<?php endfor; ?>
							</select>
					<?php break; endswitch; ?>
				</div>
			</div>
			<?php if (! $this->_vars['full_form'] && $this->_vars['listing']['price_period'] == 1): ?><div class="r fleft">&nbsp;</div><?php endif; ?>
			<div class="r <?php if (! $this->_vars['full_form'] && $this->_vars['listing']['price_period'] == 1): ?>fleft<?php endif; ?>">
				<div class="f"><?php echo l('field_booking_guests', 'listings', '', 'text', array()); ?>:</div>
				<div class="v">
					<?php echo tpl_function_ld(array('i' => 'booking_guests','gid' => 'listings','assign' => 'booking_guests'), $this);?>
					<select name="period[guests]" <?php if ($this->_vars['full_form']): ?>class="middle"<?php endif; ?>>
						<?php if (is_array($this->_vars['booking_guests']['option']) and count((array)$this->_vars['booking_guests']['option'])): foreach ((array)$this->_vars['booking_guests']['option'] as $this->_vars['key'] => $this->_vars['item']): ?>
						<option value="<?php echo $this->_vars['key']; ?>
" <?php if ($this->_vars['key'] == $this->_vars['data']['calendar_guests_min']): ?>selected<?php endif; ?>><?php echo $this->_vars['item']; ?>
</option>
						<?php endforeach; endif; ?>
					</select>
				</div>
			</div>
			<?php if ($this->_vars['full_form']): ?>
			<div class="r">
				<div class="f"><?php echo l('field_booking_comment', 'listings', '', 'text', array()); ?>:</div>
				<div class="v"><textarea name="period[comment]" rows="10" cols="80"></textarea></div>
			</div>
			<div class="r">
				<div class="f"><?php echo l('field_booking_price', 'listings', '', 'text', array()); ?></div>
				<div class="v booking_price booking_price_value"><?php echo $this->_vars['booking_price']; ?>
</div>
			</div>
			<?php else: ?>
			<div class="clr r booking_price"><?php echo l('text_price_total', 'listings', '', 'text', array()); ?>: <span class="booking_price_value"><?php echo $this->_vars['booking_price']; ?>
</span></div>
			<?php endif; ?>
			<div class="b clr">
				<input type="submit" name="btn_booking" value="<?php echo l('btn_order', 'listings', '', 'button', array()); ?>">
			</div>
			<div class="clr"></div>
		</form>
	</div>
