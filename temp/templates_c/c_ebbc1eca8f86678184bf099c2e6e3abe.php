<?php require_once('/opt/ip/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  require_once('/opt/ip/system/libraries/template_lite/plugins/modifier.date_format.php'); $this->register_modifier("date_format", "tpl_modifier_date_format");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.seolink.php'); $this->register_function("seolink", "tpl_function_seolink");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.seotag.php'); $this->register_function("seotag", "tpl_function_seotag");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-02 15:04:26 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->module_path. "start". $this->module_templates.  $this->get_current_theme_gid('', '"start"'). "news_panel.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<div class="rc">
	<div class="content-block">
	<h1>
		<?php echo tpl_function_seotag(array('tag' => 'header_text'), $this);?>
		<div class="fright"><a class="btn-link" href="<?php echo $this->_vars['site_url']; ?>
news/rss">
		<ins class="with-icon i-rss"></ins>
		</a></div>
	</h1>
    
<!-- MOD FOR TAG SEARCH -->
<div align="right" style="margin-bottom:5px;">
    <form action="<?php echo $this->_vars['site_url']; ?>
news/index" method="post" enctype="multipart/form-data">
    <table>
        <tr>
            <td>
            	<input type="text" name="search" value="<?php echo $this->_vars['search']; ?>
" size="50" placeholder="Gunakan # (hashtag) untuk mencari dengan tag" />
                <!--
				<select name="category"	style="
                    margin: 0px;
                    border: 1px solid #111;
                    width: 150px;
                    font-size: 16px;
                    border: 1px solid #ccc;
                    height: 27px;
                    -webkit-appearance: none;
                    -moz-appearance: none;
                    appearance: none;
            	">
                	<option value="title">Judul</option>
                    <option value="tag">Kata Kunci</option>
                </select>
                -->                
            </td>
        	<td><input type="submit" value="search" name="btn_save"/></td>            
        </tr>
	<?php if ($this->_vars['search'] != ''): ?>
        <tr>
        	<td><input type="submit" name="cancel_search" value="Cancel search"/></td>
            <td align="right"></td>
        </tr>
    <?php endif; ?>                    
        
    </table>
    </form>
</div>
<!-- END OF MOD -->
    
    
	<?php if (is_array($this->_vars['news']) and count((array)$this->_vars['news'])): foreach ((array)$this->_vars['news'] as $this->_vars['item']): ?>
		<div class="news">
            <!-- <?php if (is_array($this->_vars['jono']) and count((array)$this->_vars['jono'])): foreach ((array)$this->_vars['jono'] as $this->_vars['residen']): ?>
                Id Listing : <?php echo $this->_vars['residen']['id_listing']; ?>
<br/>
                Bd Room : <?php echo $this->_vars['residen']['fe_bd_rooms_1']; ?>
<br/>
                Bth Room : <?php echo $this->_vars['residen']['fe_bth_rooms_1']; ?>
<br/>
                Garages : <?php echo $this->_vars['residen']['fe_garages_1']; ?>
<br/>
                Total(s) Floors : <?php echo $this->_vars['residen']['fe_total_floors_1']; ?>
 <br/>
            <?php endforeach; endif; ?> -->
			<?php if ($this->_vars['item']['img']): ?>
			<div class="image">
				<a href="<?php echo tpl_function_seolink(array('module' => 'news','method' => 'view','data' => $this->_vars['item']), $this);?>"><img src="<?php echo $this->_vars['item']['media']['img']['thumbs']['small']; ?>
" align="left" /></a>
			</div>
			<div class="body">
			<?php endif; ?>
			<span class="date"><?php echo $this->_run_modifier($this->_vars['item']['date_add'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']); ?>
</span>
			<h3><a href="<?php echo tpl_function_seolink(array('module' => 'news','method' => 'view','data' => $this->_vars['item']), $this);?>"><?php echo $this->_vars['item']['name']; ?>
</a></h3>
			
			<span class="annotation"><?php echo $this->_vars['item']['annotation']; ?>
</span><br>
			<div class="links">
				<?php if ($this->_vars['item']['feed']):  echo l('feed_source', 'news', '', 'text', array()); ?>: <a href="<?php echo $this->_vars['item']['feed']['site_link']; ?>
"><?php echo $this->_vars['item']['feed']['title']; ?>
</a><br><?php endif; ?>
			</div>
			<?php if ($this->_vars['item']['img']): ?>
			</div>
			<?php endif; ?>
			<div class="clr"></div>
		</div>
	<?php endforeach; else: ?>
	<div class="empty"><?php echo l("no_news_yet_header", 'news', '', 'text', array()); ?></div>
	<?php endif; ?>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "pagination.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>    
		<div class="clr"></div>
	</div>
</div>
<div class="clr"></div>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
