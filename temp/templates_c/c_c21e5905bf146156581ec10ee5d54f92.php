<?php require_once('/opt/ip/system/libraries/template_lite/plugins/function.counter.php'); $this->register_function("counter", "tpl_function_counter");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-05-15 16:27:08 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<form method="post" action="<?php echo $this->_vars['langs']['action']; ?>
" name="save_form">
	<table class="data" width="100%" cellspacing="0" cellpadding="0">
	<tbody>
		<tr>
			<th class="first">Name</th>
			<th class="w150">Status</th>
			<th class="w50">Install</th>
			<th class="w50">Uninstall</th>
			<th class="w50">Export</th>
			<th class="w50">Update</th>
		</tr>
		<?php if (is_array($this->_vars['langs']) and count((array)$this->_vars['langs'])): foreach ((array)$this->_vars['langs'] as $this->_vars['key'] => $this->_vars['lang']): ?>
		<?php echo tpl_function_counter(array('print' => false,'assign' => counter), $this);?>
		<tr<?php if (!($this->_vars['counter'] % 2)): ?> class="zebra"<?php endif; ?>>
			<td class="first">
				<?php if ($this->_vars['lang']['is_default']): ?><b><?php echo $this->_vars['lang']['name']; ?>
 (default)</b><?php else:  echo $this->_vars['lang']['name'];  endif; ?>
			</td>
			<td class="center">
				<?php if (! $this->_vars['lang']['id']): ?>Not installed
				<?php elseif ($this->_vars['lang']['id'] && ! $this->_vars['lang']['update']): ?>Created manually
				<?php else: ?>Installed<?php endif; ?></td>
			<td class="center">
				<?php if (! $this->_vars['lang']['id']): ?>
					<a href="<?php echo $this->_vars['site_root']; ?>
admin/install/langs/install/<?php echo $this->_vars['key']; ?>
"><img width="16" height="16" border="0" alt="Install lang" src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
/icon-add.png"></a>
				<?php endif; ?>
			</td>
			<td class="center">
				<?php if ($this->_vars['lang']['id'] && $this->_vars['lang']['update'] && ! $this->_vars['lang']['is_default'] && $this->_vars['langs_count'] > 1): ?>
					<a href="<?php echo $this->_vars['site_root']; ?>
admin/install/langs/delete/<?php echo $this->_vars['lang']['id']; ?>
"><img width="16" height="16" border="0" alt="Uninstall lang" src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
/icon-clear.png"></a>
				<?php endif; ?>
			</td>
			<td class="center">
				<?php if ($this->_vars['lang']['id']): ?>
					<a href="<?php echo $this->_vars['site_root']; ?>
admin/install/langs/export/<?php echo $this->_vars['lang']['id']; ?>
"><img width="16" height="16" border="0" alt="Export lang" src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
/icon-export.png"></a>
				<?php endif; ?>
			</td>
			<td class="center">
				<?php if ($this->_vars['lang']['id'] && $this->_vars['lang']['update']): ?>
					<a href="<?php echo $this->_vars['site_root']; ?>
admin/install/langs/update/<?php echo $this->_vars['lang']['id']; ?>
"><img width="16" height="16" border="0" alt="Update lang" src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
/icon-refresh.png"></a>
				<?php endif; ?>
			</td>
		</tr>
		<?php endforeach; endif; ?>
	</tbody>
	</table>
</form>
<div class="clr"></div>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
