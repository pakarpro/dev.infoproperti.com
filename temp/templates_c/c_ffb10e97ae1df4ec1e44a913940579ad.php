<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-01 16:09:32 KRAT */ ?>

<script><?php $this->_tag_stack[] = array('tpl_block_strip', array()); tpl_block_strip(array(), null, $this); ob_start();  echo '
var options = {};
$(function(){
	'; ?>

	if(typeof(<?php echo $this->_vars['map_id']; ?>
) == 'undefined') return;
	<?php echo $this->_vars['map_id']; ?>
.clear();
	<?php if (is_array($this->_vars['markers']) and count((array)$this->_vars['markers'])): foreach ((array)$this->_vars['markers'] as $this->_vars['item']): ?>
		options = <?php echo '{'; ?>

			<?php if ($this->_vars['item']['gid']): ?>gid: '<?php echo $this->_vars['item']['gid']; ?>
',<?php endif; ?>
			<?php if ($this->_vars['item']['dragging']): ?>draggable: true, 
				<?php if ($this->_vars['view_settings']['drag_listener']): ?>drag_listener: <?php echo $this->_vars['view_settings']['drag_listener']; ?>
,<?php endif; ?>
			<?php endif; ?>
			<?php if ($this->_vars['item']['info']): ?>info: '<?php $this->_tag_stack[] = array('tpl_block_strip', array()); tpl_block_strip(array(), null, $this); ob_start();  echo $this->_run_modifier('\'', 'str_replace', 'PHP', 1, '\\\'', $this->_vars['item']['info']);  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_strip($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>',<?php endif; ?>
		<?php echo '}'; ?>
;
		<?php echo $this->_vars['map_id']; ?>
.addMarker(<?php echo $this->_vars['item']['lat']; ?>
, <?php echo $this->_vars['item']['lon']; ?>
, options);
	<?php endforeach; endif; ?>
	<?php echo '
});
';  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_strip($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?></script>
