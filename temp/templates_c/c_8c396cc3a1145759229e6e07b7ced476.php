<?php require_once('/opt/ip/system/libraries/template_lite/plugins/function.js.php'); $this->register_function("js", "tpl_function_js");  require_once('/opt/ip/system/libraries/template_lite/plugins/modifier.escape.php'); $this->register_modifier("escape", "tpl_modifier_escape");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-02-26 13:22:09 KRAT */ ?>

<form action="<?php echo $this->_vars['data']['action']; ?>
" method="post" enctype="multipart/form-data" name="save_form">
<div class="edit-form n150">
<div class="row header"><?php if ($this->_vars['data']['id']):  echo l('admin_header_users_change', 'users', '', 'text', array());  else:  echo l('admin_header_users_add', 'users', '', 'text', array());  endif; ?></div>
	<div class="row">
		<div class="h"><?php echo l('field_user_type', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><b><?php echo $this->_vars['data']['user_type_str']; ?>
</b></div>
	</div>
	
	<div class="row">
		<div class="h"><?php echo l('field_email', 'users', '', 'text', array()); ?>:&nbsp;* </div>
		<div class="v"><input type="text" name="data[email]" value="<?php echo $this->_run_modifier($this->_vars['data']['email'], 'escape', 'plugin', 1); ?>
"></div>
	</div>
	<?php if ($this->_vars['data']['id']): ?>
	<div class="row">
		<div class="h"><?php echo l('field_change_password', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><input type="checkbox" value="1" name="update_password" id="pass_change_field"></div>
	</div>
	<?php endif; ?>
	<div class="row">
		<div class="h"><?php echo l('field_password', 'users', '', 'text', array()); ?>:<?php if (! $this->_vars['data']['id']): ?>&nbsp;*<?php endif; ?> </div>
		<div class="v"><input type="password" name="data[password]" id="pass_field" <?php if ($this->_vars['data']['id']): ?>disabled<?php endif; ?>></div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_repassword', 'users', '', 'text', array()); ?>:<?php if (! $this->_vars['data']['id']): ?>&nbsp;*<?php endif; ?> </div>
		<div class="v"><input type="password" name="data[repassword]" id="repass_field" <?php if ($this->_vars['data']['id']): ?>disabled<?php endif; ?>></div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_fname', 'users', '', 'text', array()); ?>:&nbsp;* </div>
		<div class="v"><input type="text" name="data[fname]" value="<?php echo $this->_run_modifier($this->_vars['data']['fname'], 'escape', 'plugin', 1); ?>
"></div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_sname', 'users', '', 'text', array()); ?>:&nbsp;* </div>
		<div class="v"><input type="text" name="data[sname]" value="<?php echo $this->_run_modifier($this->_vars['data']['sname'], 'escape', 'plugin', 1); ?>
"></div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_phone', 'users', '', 'text', array()); ?>:&nbsp;* </div>
		<div class="v"><input type="text" name="data[phone]" value="<?php echo $this->_run_modifier($this->_vars['data']['phone'], 'escape', 'plugin', 1); ?>
" class="phone-field"></div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_icon', 'users', '', 'text', array()); ?>: </div>
		<div class="v">
			<input type="file" name="user_icon">
			<?php if ($this->_vars['data']['user_logo'] || $this->_vars['data']['user_logo_moderation']): ?>
			<br><input type="checkbox" name="user_icon_delete" value="1" id="uichb"><label for="uichb"><?php echo l('field_icon_delete', 'users', '', 'text', array()); ?></label><br>
			<?php if ($this->_vars['data']['user_logo_moderation']): ?><img src="<?php echo $this->_vars['data']['media']['user_logo_moderation']['thumbs']['middle']; ?>
"><?php else: ?><img src="<?php echo $this->_vars['data']['media']['user_logo']['thumbs']['middle']; ?>
"><?php endif; ?>
			<?php endif; ?>
		</div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_contact_email', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><input type="text" name="data[contact_email]" value="<?php echo $this->_run_modifier($this->_vars['data']['contact_email'], 'escape', 'plugin', 1); ?>
"></div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_contact_phone', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><input type="text" name="data[contact_phone]" value="<?php echo $this->_run_modifier($this->_vars['data']['contact_phone'], 'escape', 'plugin', 1); ?>
" class="phone-field"></div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_contact_info', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><textarea rows="5" cols="80" name="data[contact_info]"><?php echo $this->_run_modifier($this->_vars['data']['contact_info'], 'escape', 'plugin', 1); ?>
</textarea></div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_twitter', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><input type="text" name="data[twitter]" value="<?php echo $this->_run_modifier($this->_vars['data']['twitter'], 'escape', 'plugin', 1); ?>
"></div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_facebook', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><input type="text" name="data[facebook]" value="<?php echo $this->_run_modifier($this->_vars['data']['facebook'], 'escape', 'plugin', 1); ?>
"></div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_vkontakte', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><input type="text" name="data[vkontakte]" value="<?php echo $this->_run_modifier($this->_vars['data']['vkontakte'], 'escape', 'plugin', 1); ?>
"></div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_confirm', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><input type="checkbox" name="data[confirm]" value="1" <?php if ($this->_vars['data']['confirm']): ?>checked<?php endif; ?>></div>
	</div>
</div>
<div class="btn"><div class="l"><input type="submit" name="btn_save" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div></div>
<a class="cancel" href="<?php echo $this->_vars['back_url']; ?>
"><?php echo l('btn_cancel', 'start', '', 'text', array()); ?></a>
</form>
<div class="clr"></div>
<?php if ($this->_vars['phone_format']): ?>
<?php echo tpl_function_js(array('file' => 'jquery.maskedinput.min.js'), $this);?>
<script><?php echo '
$(function(){
	$(\'.phone-field\').mask(\'';  echo $this->_vars['phone_format'];  echo '\');
});
'; ?>
</script>
<?php endif; ?>

<?php echo tpl_function_js(array('file' => 'jquery-ui.custom.min.js'), $this);?>
<link href='<?php echo $this->_vars['site_root'];  echo $this->_vars['js_folder']; ?>
jquery-ui/jquery-ui.custom.css' rel='stylesheet' type='text/css' media='screen' />
<script type='text/javascript'><?php echo '
    $(function(){
		now = new Date();
		yr =  (new Date(now.getYear() - 80, 0, 1).getFullYear()) + \':\' + (new Date(now.getYear() - 18, 0, 1).getFullYear());
           $( "#datepicker" ).datepicker({
			dateFormat :\'yy-mm-dd\',
			changeYear: true,
			changeMonth: true,
			yearRange: yr
		});
    });
	$(function(){
		$("div.row:odd").addClass("zebra");
		$("#pass_change_field").click(function(){
			if(this.checked){
				$("#pass_field").removeAttr("disabled");
				$("#repass_field").removeAttr("disabled");
			}else{
				$("#pass_field").attr(\'disabled\', \'disabled\'); $("#repass_field").attr(\'disabled\', \'disabled\');
			}
		});
	});
'; ?>
</script>
