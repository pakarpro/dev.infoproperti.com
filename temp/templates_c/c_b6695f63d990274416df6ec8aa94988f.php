<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-02-29 10:06:49 KRAT */ ?>

	<?php if ($this->_vars['data']['id']): ?>
	<div class="tabs tab-size-15" id="edit_listings_menu">
		<ul>
			<li class="<?php if ($this->_vars['section_gid'] == 'overview'): ?>active<?php endif; ?>"><a href="<?php echo $this->_vars['site_url']; ?>
listings/edit/<?php echo $this->_vars['data']['id']; ?>
/overview"><?php echo l('filter_section_overview', 'listings', '', 'text', array()); ?></a></li>
			<li class="<?php if ($this->_vars['section_gid'] == 'description'): ?>active<?php endif; ?>"><a href="<?php echo $this->_vars['site_url']; ?>
listings/edit/<?php echo $this->_vars['data']['id']; ?>
/description"><?php echo l('filter_section_description', 'listings', '', 'text', array()); ?></a></li>
			<?php if ($this->_vars['data']['operation_type'] == 'sale' || $this->_vars['data']['operation_type'] == 'rent'): ?><li class="<?php if ($this->_vars['section_gid'] == 'gallery'): ?>active<?php endif; ?>"><a href="<?php echo $this->_vars['site_url']; ?>
listings/edit/<?php echo $this->_vars['data']['id']; ?>
/gallery"><?php echo l('filter_section_gallery', 'listings', '', 'text', array()); ?></a></li><?php endif; ?>
			<?php if ($this->_vars['data']['operation_type'] == 'rent'): ?><li class="<?php if ($this->_vars['section_gid'] == 'calendar'): ?>active<?php endif; ?>"><a href="<?php echo $this->_vars['site_url']; ?>
listings/edit/<?php echo $this->_vars['data']['id']; ?>
/calendar"><?php echo l('filter_section_calendar', 'listings', '', 'text', array()); ?></a></li><?php endif; ?>
			<li class="<?php if ($this->_vars['section_gid'] == 'map'): ?>active<?php endif; ?>"><a href="<?php echo $this->_vars['site_url']; ?>
listings/edit/<?php echo $this->_vars['data']['id']; ?>
/map"><?php echo l('filter_section_map', 'listings', '', 'text', array()); ?></a></li>			<li class="<?php if ($this->_vars['section_gid'] == 'activity'): ?>active<?php endif; ?>"><a href="<?php echo $this->_vars['site_url']; ?>
listings/activity/<?php echo $this->_vars['data']['id']; ?>
"><?php echo l('filter_section_activity', 'listings', '', 'text', array()); ?></a></li>
			<?php if ($this->_vars['data']['operation_type'] != 'buy' && $this->_vars['data']['operation_type'] != 'lease' && $this->_vars['data']['status']): ?><li class="<?php if ($this->_vars['section_gid'] == 'services'): ?>active<?php endif; ?>"><a href="<?php echo $this->_vars['site_url']; ?>
listings/services/<?php echo $this->_vars['data']['id']; ?>
"><?php echo l('filter_section_services', 'listings', '', 'text', array()); ?></a></li><?php endif; ?>		</ul>
		&nbsp;
	</div>	
	<?php endif; ?>
