<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.block.php'); $this->register_function("block", "tpl_function_block");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.country_select.php'); $this->register_function("country_select", "tpl_function_country_select");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.escape.php'); $this->register_modifier("escape", "tpl_modifier_escape");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2014-09-09 17:55:49 KRAT */ ?>

	<div class="row">
		<div class="h"><?php echo l('field_listing_type', 'listings', '', 'text', array()); ?></div>
		<div class="v">
			<select name="filters[type]" id="operation_type">
				<option value="">...</option>
				<?php if (is_array($this->_vars['operation_types']) and count((array)$this->_vars['operation_types'])): foreach ((array)$this->_vars['operation_types'] as $this->_vars['item']): ?>
				<option value="<?php echo $this->_run_modifier($this->_vars['item'], 'escape', 'plugin', 1); ?>
" <?php if ($this->_vars['item'] == $this->_vars['data']['type']): ?>selected<?php endif; ?>><?php echo l('operation_search_'.$this->_vars['item'], 'listings', '', 'text', array()); ?></option>
				<?php endforeach; endif; ?>
			</select>
		</div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_location', 'listings', '', 'text', array()); ?>:</div>
		<div class="v"><?php echo tpl_function_country_select(array('select_type' => 'city','id_country' => $this->_vars['data']['id_country'],'id_region' => $this->_vars['data']['id_region'],'id_city' => $this->_vars['data']['id_city'],'var_country' => 'filters[id_country]','var_region' => 'filters[id_region]','var_city' => 'filters[id_city]'), $this);?></div>	
	</div>	
	<div  class="row">
		<div class="h"><?php echo l('field_category', 'listings', '', 'text', array()); ?></div>
		<div class="v">
		<?php if ($this->_vars['data']['property_type'] > 0):  $this->assign('selected_category', $this->_vars['data']['id_category'].'_'.$this->_vars['data']['property_type']);  else:  $this->assign('selected_category', $this->_vars['data']['id_category']);  endif; ?>
		<?php echo tpl_function_block(array('name' => 'properties_select','module' => 'properties','var_name' => 'filters[id_category]','js_var_name' => 'id_category','selected' => $this->_vars['selected_category'],'cat_select' => true), $this);?>
		</div>
	</div>
	<div id="export_search_form">
		<?php echo $this->_vars['extend_search_form']; ?>

	</div>
	<div class="row">
		<div class="h"><?php echo l('field_keyword', 'listings', '', 'text', array()); ?>:</div>
		<div class="v"><input type="text" name="filters[keyword]" value="<?php echo $this->_run_modifier($this->_vars['data']['keyword'], 'escape', 'plugin', 1); ?>
"></div>
	</div>
	<div class="row">
		<div class="h"><label for="status"><?php echo l('field_status', 'listings', '', 'text', array()); ?></label>:</div>
		<div class="v"><input type="checkbox" name="filters[status]" value="1" id="status" <?php if ($this->_vars['data']['status']): ?>checked=""<?php endif; ?>></div>
	</div>
	<input type="hidden" name="form" value="admin_export_form">
		
	<script><?php echo '
		$(function(){
			function get_extend_form(type, id_category){
				if(type && id_category != 0){
					$.ajax({
						url: \'';  echo $this->_vars['site_url']; ?>
admin/listings/ajax_get_export_extend_form/<?php echo $this->_vars['data']['id'];  echo '\',
						data: {type: type, id_category: id_category},
						cache: false,
						type: \'post\',
						success: function(data){
							$(\'#export_search_form\').html(data);
							$(\'div.row\').removeClass(\'zebra\');
							$(\'div.row:odd\').addClass(\'zebra\');
						}
					});
				}else{
					$(\'#export_search_form\').html(\'\');
				}
			}
			$(\'#operation_type\').bind(\'change\', function(){
				var type = $(this).val();
				var id_category = $(\'#id_category\').val();
				get_extend_form(type, id_category)
			});
			$(\'#id_category\').bind(\'change\', function(){
				var id_category = $(this).val();
				var type = $(\'#operation_type\').val();
				get_extend_form(type, id_category)
			});
			$(\'div.row:odd\').addClass(\'zebra\');
		});
	'; ?>
</script>
	
