<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.pagination.php'); $this->register_function("pagination", "tpl_function_pagination");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-03-08 03:31:23 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->module_path. "start". $this->module_templates.  $this->get_current_theme_gid('', '"start"'). "left_panel.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

<div class="rc">
	<div class="content-block">
		<h1><?php echo l('header_saved_searches', 'listings', '', 'text', array()); ?></h1>		
		<table class="searches_list list">
		<tr>
			<th><?php echo l('field_searches', 'listings', '', 'text', array()); ?></th>
			<th class="w100">&nbsp;</th>
		</tr>
		<?php if (is_array($this->_vars['searches']) and count((array)$this->_vars['searches'])): foreach ((array)$this->_vars['searches'] as $this->_vars['item']): ?>
		<tr class="item">
			<td>
				<?php echo l('field_search', 'listings', '', 'text', array()); ?> <b><?php echo $this->_vars['item']['date_search']; ?>
</b><br>
				<?php echo $this->_vars['item']['name']; ?>

			</td>
			<td class="center">
				<a href="<?php echo $this->_vars['site_url']; ?>
listings/load_saved_search/<?php echo $this->_vars['item']['id']; ?>
" class="btn-link fright"><ins class="with-icon i-load"></ins></a>
				<a href="<?php echo $this->_vars['site_url']; ?>
listings/delete_saved_search/<?php echo $this->_vars['item']['id']; ?>
" class="btn-link fright"><ins class="with-icon i-delete"></ins></a>
			</td>
		</tr>
		<?php endforeach; else: ?>
		<tr><td colspan="2"><div class="item empty"><?php echo l('no_saved_searches', 'listings', '', 'text', array()); ?></div></td></tr>
		<?php endif; ?>
		</table>
		<?php if ($this->_vars['searches']): ?><div id="pages_block_2"><?php echo tpl_function_pagination(array('data' => $this->_vars['page_data'],'type' => 'full'), $this);?></div><?php endif; ?>
	</div>
</div>
<div class="clr"></div>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

