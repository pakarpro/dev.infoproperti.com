<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2014-09-02 17:47:16 KRAT */ ?>

	<?php if (is_array($this->_vars['messages']) and count((array)$this->_vars['messages'])): foreach ((array)$this->_vars['messages'] as $this->_vars['item']): ?>
		<li id="message_<?php echo $this->_vars['item']['id']; ?>
" class="<?php echo $this->_vars['item']['message_type']; ?>
">
			<a href="#" onclick="javascript: if(confirm('<?php echo l('note_delete_message', 'mailbox', '', 'js', array()); ?>')) <?php echo '{'; ?>
 mb.delete_message(<?php echo $this->_vars['item']['id']; ?>
); <?php echo '}'; ?>
 return false;" class="btn-link fright"><ins class="with-icon i-close"></ins></a>
			<div class="top <?php if ($this->_vars['item']['message_type'] == 'inbox'): ?>friend<?php else: ?>owner<?php endif; ?>">
				<font class="date"><?php echo $this->_run_modifier($this->_vars['item']['date_add'], 'date_format', 'plugin', 1, $this->_vars['date_format']); ?>
</font>
				<font class="user"><?php echo $this->_vars['item']['user_data']['output_name']; ?>
:</font>
			</div>
			<div class="message"><?php echo $this->_vars['item']['message']; ?>
</div>
		</li>
	<?php endforeach; endif; ?>