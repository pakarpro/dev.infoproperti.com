<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-05-12 14:08:32 KRAT */ ?>

<?php if ($this->_vars['page_type'] == 'cute'): ?>
<div class="pages">
	<div class="inside">		
		<ins class="prev<?php if ($this->_vars['page_data']['prev_page'] == $this->_vars['page_data']['cur_page']): ?> gray<?php endif; ?>"><a href="<?php if ($this->_run_modifier($this->_vars['page_data']['base_url'], 'strpos', 'PHP', 1, '[page]')):  echo $this->_run_modifier($this->_vars['page_data']['base_url'], 'replace', 'plugin', 1, '[page]', $this->_vars['page_data']['prev_page']);  else:  echo $this->_vars['page_data']['base_url'];  echo $this->_vars['page_data']['prev_page'];  endif; ?>" data-page="<?php echo $this->_vars['page_data']['prev_page']; ?>
">&nbsp;</a></ins>
		<ins class="current"><?php echo $this->_vars['page_data']['cur_page']; ?>
 <?php echo l('text_of', 'start', '', 'text', array()); ?> <?php echo $this->_vars['page_data']['total_pages']; ?>
</ins>
		<ins class="next<?php if ($this->_vars['page_data']['next_page'] == $this->_vars['page_data']['cur_page']): ?> gray<?php endif; ?>"><a href="<?php if ($this->_run_modifier($this->_vars['page_data']['base_url'], 'strpos', 'PHP', 1, '[page]')):  echo $this->_run_modifier($this->_vars['page_data']['base_url'], 'replace', 'plugin', 1, '[page]', $this->_vars['page_data']['next_page']);  else:  echo $this->_vars['page_data']['base_url'];  echo $this->_vars['page_data']['next_page'];  endif; ?>" data-page="<?php echo $this->_vars['page_data']['next_page']; ?>
">&nbsp;</a></ins>
	</div>
</div>
<?php elseif ($this->_vars['page_type'] == 'full'):  if ($this->_vars['page_data']['nav']): ?>
<div class="line pages">
	<div class="inside"><?php echo $this->_vars['page_data']['nav']; ?>
</div>
</div>
<?php endif;  endif; ?>
