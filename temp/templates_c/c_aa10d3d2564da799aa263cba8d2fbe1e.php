<?php require_once('/opt/ip/system/libraries/template_lite/plugins/function.module_tpl.php'); $this->register_function("module_tpl", "tpl_function_module_tpl");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-02-26 13:22:38 KRAT */ ?>

<h2><?php echo l('header_my_statistics', 'start', '', 'text', array()); ?></h2>
<div class="container statistic">
	<?php echo tpl_function_block(array('name' => listings_count,'module' => listings), $this);?>
	<?php echo tpl_function_block(array('name' => contacts_count,'module' => mailbox), $this);?>
	<?php echo tpl_function_block(array('name' => new_messages,'module' => mailbox,'template' => homepage), $this);?>
	<?php echo tpl_function_block(array('name' => user_account_block,'module' => users_payments), $this);?>
	<?php echo tpl_function_block(array('name' => listings_saved_block,'module' => listings), $this);?>
	<?php echo tpl_function_block(array('name' => listings_searches_block,'module' => listings), $this);?>
</div>

<h2><?php echo l('header_activity', 'start', '', 'text', array()); ?></h2>
<div class="container statistic">
	<div>
		<table cellspacing="0" cellpadding="0" class="data" width="100%">
		<tr>
			<th class="first"><?php echo l('stat_header_name', 'start', '', 'text', array()); ?></th>
			<th class="w100"><?php echo l('stat_header_month', 'start', '', 'text', array()); ?></th>
			<th class="w100"><?php echo l('stat_header_week', 'start', '', 'text', array()); ?></th>
		</tr>
		<?php echo tpl_function_block(array('name' => users_visitors_block,'module' => users), $this);?>
		<?php echo tpl_function_block(array('name' => listings_visitors_block,'module' => listings), $this);?>
		<?php echo tpl_function_block(array('name' => reviews_visitors_block,'module' => reviews,'type' => 'users_object'), $this);?>
		<?php echo tpl_function_block(array('name' => reviews_visitors_block,'module' => reviews,'type' => 'listings_object'), $this);?>
		</table>
	</div>
	
	<div>
		<table cellspacing="0" cellpadding="0" class="data" width="100%">
		<tr>
			<th class="first"><?php echo l('stat_header_name', 'start', '', 'text', array()); ?></th>
			<th class="w100"><?php echo l('stat_header_month', 'start', '', 'text', array()); ?></th>
			<th class="w100"><?php echo l('stat_header_week', 'start', '', 'text', array()); ?></th>
		</tr>
		<?php echo tpl_function_block(array('name' => users_visits_block,'module' => users), $this);?>
		<?php echo tpl_function_block(array('name' => listings_visits_block,'module' => listings), $this);?>
		<?php echo tpl_function_block(array('name' => reviews_visits_block,'module' => reviews,'type' => 'users_object'), $this);?>
		<?php echo tpl_function_block(array('name' => reviews_visits_block,'module' => reviews,'type' => 'listings_object'), $this);?>
		</table>
	</div>
</div>

<h2><?php echo l('header_actions', 'start', '', 'text', array()); ?></h2>
<div class="container statistic">
	<?php echo tpl_function_module_tpl(array('module' => users,'tpl' => link_profile), $this);?>
	<?php echo tpl_function_module_tpl(array('module' => users,'module_1' => users_payments,'tpl' => link_add_funds), $this);?>
	<?php echo tpl_function_module_tpl(array('module' => users,'tpl' => link_change_password), $this);?>
	<?php echo tpl_function_module_tpl(array('module' => contact_us,'tpl' => link_contact_admin), $this);?>
</div>
