<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.date_format.php'); $this->register_modifier("date_format", "tpl_modifier_date_format");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.block.php'); $this->register_function("block", "tpl_function_block");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.truncate.php'); $this->register_modifier("truncate", "tpl_modifier_truncate");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.counter.php'); $this->register_function("counter", "tpl_function_counter");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.lower.php'); $this->register_modifier("lower", "tpl_modifier_lower");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.helper.php'); $this->register_function("helper", "tpl_function_helper");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-05-08 09:58:29 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
  echo tpl_function_helper(array('func_name' => get_admin_level1_menu,'helper_name' => menu,'func_param' => 'admin_users_types_menu'), $this);?>

<div class="actions">
	<ul>
		<li><div class="l"><a href="<?php echo $this->_vars['site_url']; ?>
admin/users/add/<?php echo $this->_vars['user_type']; ?>
"><?php echo l('link_add_user', 'users', '', 'text', array()); ?></a></div></li>
		<?php echo tpl_function_helper(array('func_name' => button_add_funds,'module' => users_payments), $this);?>
	</ul>
	&nbsp;
</div>

<div class="menu-level3">
	<ul>
		<?php if (is_array($this->_vars['filters']) and count((array)$this->_vars['filters'])): foreach ((array)$this->_vars['filters'] as $this->_vars['item']): ?>
		<?php $this->assign('l', 'filter_'.$this->_vars['item'].'_users'); ?>
		<li class="<?php if ($this->_vars['filter'] == $this->_vars['item']): ?>active<?php endif;  if (! $this->_vars['filter_data'][$this->_vars['item']]): ?> hide<?php endif; ?>"><a href="<?php echo $this->_vars['site_url']; ?>
admin/users/index/<?php echo $this->_vars['user_type']; ?>
/<?php echo $this->_vars['item']; ?>
"><?php echo l($this->_vars['l'], 'users', '', 'text', array()); ?> (<?php echo $this->_vars['filter_data'][$this->_vars['item']]; ?>
)</a></li>
		<?php endforeach; endif; ?>
	</ul>
	&nbsp;
</div>
<table cellspacing="0" cellpadding="0" class="data" width="100%">
<tr>
	<th class="first"><input type="checkbox" id="grouping_all"></th>
	<th class="w150"><a href="<?php echo $this->_vars['sort_links']['name']; ?>
"<?php if ($this->_vars['order'] == 'name'): ?> class="<?php echo $this->_run_modifier($this->_vars['order_direction'], 'lower', 'plugin', 1); ?>
"<?php endif; ?>><?php echo l('field_name', 'users', '', 'text', array()); ?></a></th>
	<th class="w150"><a href="<?php echo $this->_vars['sort_links']['email']; ?>
"<?php if ($this->_vars['order'] == 'email'): ?> class="<?php echo $this->_run_modifier($this->_vars['order_direction'], 'lower', 'plugin', 1); ?>
"<?php endif; ?>><?php echo l('field_email', 'users', '', 'text', array()); ?></a></th>
	<th><a href="<?php echo $this->_vars['sort_links']['account']; ?>
"<?php if ($this->_vars['order'] == 'account'): ?> class="<?php echo $this->_run_modifier($this->_vars['order_direction'], 'lower', 'plugin', 1); ?>
"<?php endif; ?>><?php echo l('field_account', 'users', '', 'text', array()); ?></a></th>
		<th class="w30"><a href="<?php echo $this->_vars['sort_links']['reviews']; ?>
"<?php if ($this->_vars['order'] == 'reviews'): ?> class="<?php echo $this->_run_modifier($this->_vars['order_direction'], 'lower', 'plugin', 1); ?>
"<?php endif; ?>><?php echo l('field_reviews_count', 'users', '', 'text', array()); ?></a></th>
		<th class="w100"><a href="<?php echo $this->_vars['sort_links']['date_created']; ?>
"<?php if ($this->_vars['order'] == 'date_created'): ?> class="<?php echo $this->_run_modifier($this->_vars['order_direction'], 'lower', 'plugin', 1); ?>
"<?php endif; ?>><?php echo l('field_date_created', 'users', '', 'text', array()); ?></a></th>
	<th class="w150">&nbsp;</th>
</tr>
<?php if (is_array($this->_vars['users']) and count((array)$this->_vars['users'])): foreach ((array)$this->_vars['users'] as $this->_vars['item']):  echo tpl_function_counter(array('print' => false,'assign' => counter), $this);?>
<tr<?php if (!($this->_vars['counter'] % 2)): ?> class="zebra"<?php endif; ?>>
	<td class="first w20 center"><input type="checkbox" class="grouping" value="<?php echo $this->_vars['item']['id']; ?>
"></td>
	<td><?php echo $this->_run_modifier($this->_vars['item']['output_name'], 'truncate', 'plugin', 1, 100); ?>
</td>
	<td><?php echo $this->_run_modifier($this->_vars['item']['email'], 'truncate', 'plugin', 1, 50); ?>
</td>
	<td class="center"><?php echo tpl_function_block(array('name' => currency_format_output,'module' => start,'value' => $this->_vars['item']['account']), $this);?></td>
		<td class="center"><a href="<?php echo $this->_vars['site_url']; ?>
admin/reviews/index/users_object/<?php echo $this->_vars['item']['id']; ?>
"><?php echo $this->_vars['item']['review_count']; ?>
</a></td>
		<td class="center"><?php echo $this->_run_modifier($this->_vars['item']['date_created'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']); ?>
</td>
	<td class="icons">
		<?php if ($this->_vars['item']['status']): ?>
		<a href="<?php echo $this->_vars['site_url']; ?>
admin/users/activate/<?php echo $this->_vars['item']['id']; ?>
/0"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-full.png" width="16" height="16" border="0" alt="<?php echo l('link_deactivate_user', 'users', '', 'button', array()); ?>" title="<?php echo l('link_deactivate_user', 'users', '', 'button', array()); ?>"></a>
		<?php else: ?>
		<a href="<?php echo $this->_vars['site_url']; ?>
admin/users/activate/<?php echo $this->_vars['item']['id']; ?>
/1"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-empty.png" width="16" height="16" border="0" alt="<?php echo l('link_activate_user', 'users', '', 'button', array()); ?>" title="<?php echo l('link_activate_user', 'users', '', 'button', array()); ?>"></a>
		<?php endif; ?>
		<a href="<?php echo $this->_vars['site_url']; ?>
admin/users/edit/<?php echo $this->_vars['item']['id']; ?>
"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-edit.png" width="16" height="16" border="0" alt="<?php echo l('link_edit_user', 'users', '', 'button', array()); ?>" title="<?php echo l('link_edit_user', 'users', '', 'button', array()); ?>"></a>
		<a href="<?php echo $this->_vars['site_url']; ?>
admin/users/delete/<?php echo $this->_vars['item']['id']; ?>
" onclick="javascript: if(!confirm('<?php echo l('note_delete_user', 'users', '', 'js', array()); ?>')) return false;"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-delete.png" width="16" height="16" border="0" alt="<?php echo l('link_delete_user', 'users', '', 'button', array()); ?>" title="<?php echo l('link_delete_user', 'users', '', 'button', array()); ?>"></a>
	</td>
</tr>
<?php endforeach; else:  $this->assign('colspan', 7);  $this->assign('colspan', $this->_vars['colspan']); ?><tr><td colspan="<?php echo $this->_vars['colspan']; ?>
" class="center"><?php echo l('no_users', 'users', '', 'text', array()); ?></td></tr>
<?php endif; ?>
</table>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "pagination.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

<script type="text/javascript"><?php echo '
var reload_link = "';  echo $this->_vars['site_url']; ?>
admin/users/index/<?php echo '";
var filter = \'';  echo $this->_vars['filter'];  echo '\';
var order = \'';  echo $this->_vars['order'];  echo '\';
var loading_content;
var order_direction = \'';  echo $this->_vars['order_direction'];  echo '\';
$(function(){
	$(\'#grouping_all\').bind(\'click\', function(){
		var checked = $(this).is(\':checked\');
		if(checked){
			$(\'input[type=checkbox].grouping\').attr(\'checked\', \'checked\');
		}else{
			$(\'input[type=checkbox].grouping\').removeAttr(\'checked\');
		}
	});
});

function reload_this_page(value){
	var link = reload_link + value + \'/\' + filter + \'/\' + order + \'/\' + order_direction;
	location.href=link;
}
'; ?>
</script>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
