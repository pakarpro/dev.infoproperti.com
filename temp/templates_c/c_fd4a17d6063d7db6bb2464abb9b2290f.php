<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.block.php'); $this->register_function("block", "tpl_function_block");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2014-05-07 09:44:38 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>


<form method="post" action="<?php echo $this->_vars['data']['action']; ?>
" name="save_form">
	<div class="edit-form n150">
		<div class="row header"><?php echo l('admin_header_template_edit', 'notifications', '', 'text', array()); ?></div>
		<div class="row zebra">
			<div class="h"><?php echo l('field_template_gid', 'notifications', '', 'text', array()); ?>:&nbsp;* </div>
			<div class="v"><?php if ($this->_vars['allow_edit']): ?><input type="text" value="<?php echo $this->_vars['data']['gid']; ?>
" name="gid"><?php else:  echo $this->_vars['data']['gid'];  endif; ?></div>
		</div>

		<div class="row">
			<div class="h"><?php echo l('field_notification_name', 'notifications', '', 'text', array()); ?>:&nbsp;* </div>
			<div class="v" id="field_notification_name">
				<?php $this->assign('name_i', $this->_vars['data']['name_i']); ?>
				<?php if (is_array($this->_vars['languages']) and count((array)$this->_vars['languages'])): foreach ((array)$this->_vars['languages'] as $this->_vars['lang_id'] => $this->_vars['lang_item']): ?>										
				<input type="<?php if ($this->_vars['lang_id'] == $this->_vars['cur_lang']): ?>text<?php else: ?>hidden<?php endif; ?>" name="langs[<?php echo $this->_vars['lang_id']; ?>
]" value="<?php if ($this->_vars['validate_lang']):  echo $this->_vars['validate_lang'][$this->_vars['lang_id']];  else:  echo l($this->_vars['data']['name_i'], 'notifications', $this->_vars['lang_id'], 'text', array());  endif; ?>" lang-editor="value" lang-editor-type="langs" lang-editor-lid="<?php echo $this->_vars['lang_id']; ?>
">
				<?php endforeach; endif; ?>
				<a href="#" lang-editor="button" lang-editor-type="langs"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-translate.png" width="16" height="16"></a>
			</div>
		</div>
		<div class="row zebra">
			<div class="h"><?php echo l('field_send_type', 'notifications', '', 'text', array()); ?>:&nbsp;* </div>
			<div class="v"><?php if ($this->_vars['allow_edit']): ?>
				<select name="send_type">
					<option value="que" <?php if ($this->_vars['data']['send_type'] == 'que'): ?>selected<?php endif; ?>><?php echo l('field_send_type_que', 'notifications', '', 'text', array()); ?></option>
					<option value="simple" <?php if ($this->_vars['data']['send_type'] == 'simple'): ?>selected<?php endif; ?>><?php echo l('field_send_type_simple', 'notifications', '', 'text', array()); ?></option>
				</select>
				<?php elseif ($this->_vars['data']['send_type'] == 'que'):  echo l('field_send_type_que', 'notifications', '', 'text', array()); ?>
					<?php elseif ($this->_vars['data']['send_type'] == 'simple'):  echo l('field_send_type_simple', 'notifications', '', 'text', array()); ?>
						<?php endif; ?>
						</div>
					</div>
					<div class="row">
						<div class="h"><?php echo l('field_default_template', 'notifications', '', 'text', array()); ?>:&nbsp;* </div>
						<div class="v">
							<select name="id_template_default">
						<?php if (is_array($this->_vars['templates']) and count((array)$this->_vars['templates'])): foreach ((array)$this->_vars['templates'] as $this->_vars['item']): ?><option value="<?php echo $this->_vars['item']['id']; ?>
" <?php if ($this->_vars['data']['id_template_default'] == $this->_vars['item']['id']): ?>selected<?php endif; ?>><?php echo $this->_vars['item']['name']; ?>
</option><?php endforeach; endif; ?>
					</select>
				</div>
			</div>

			<div class="btn"><div class="l"><input type="submit" name="btn_save" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div></div>
			<a class="cancel" href="<?php echo $this->_vars['site_url']; ?>
admin/notifications/index"><?php echo l('btn_cancel', 'start', '', 'text', array()); ?></a>
		</div>
	</form>
<?php echo tpl_function_block(array('name' => lang_inline_editor,'module' => start), $this);?>	
	<script>
		<?php echo '
function showLangs(divId){
	$(\'#\'+divId).slideToggle();
}

function openTab(id, object){
	$(\'#edit_divs > div.tab\').hide();
	$(\'#\'+id).show();
	$(\'#edit_tabs > li\').removeClass(\'active\');
	$(object).addClass(\'active\');
}

		'; ?>

	</script>
	<div class="clr"></div>
	<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
