<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2014-05-08 11:26:52 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array('load_type' => 'ui'));
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

<?php if ($this->_vars['max_results']): ?>
	<div class="filter-form"><?php echo l('error_no_editing', 'polls', '', 'text', array()); ?></div>
<?php endif; ?>
<form method="post" action="" name="save_form" enctype="multipart/form-data">
	<div class="edit-form n150">
		<div class="row header"><?php if ($this->_vars['poll_data']['id']):  echo l('admin_header_polls_change', 'polls', '', 'text', array());  else:  echo l('admin_header_polls_add', 'polls', '', 'text', array());  endif; ?></div>
		<div class="row">
			<div class="h"><?php echo l('field_language', 'polls', '', 'text', array()); ?>:&nbsp;* </div>
			<div class="v">
				<select name="language" id="poll_language">
					<option value="0" <?php if ($this->_vars['poll_data']['language'] == 0): ?>selected="selected"<?php endif; ?>><?php echo l('all_languages', 'polls', '', 'text', array()); ?></option>
					<?php if ($this->_vars['languages_count'] > 1): ?>
						<?php if (is_array($this->_vars['languages']) and count((array)$this->_vars['languages'])): foreach ((array)$this->_vars['languages'] as $this->_vars['lang_id'] => $this->_vars['item']): ?>
							<option value="<?php echo $this->_vars['lang_id']; ?>
" <?php if ($this->_vars['poll_data']['language'] == $this->_vars['lang_id']): ?>selected="selected"<?php endif; ?>><?php echo $this->_vars['item']['name']; ?>
</option>
						<?php endforeach; endif; ?>
					<?php endif; ?>
				</select>
			</div>
		</div>
		<div class="row row-zebra">
			<div class="h"><?php echo l('field_question', 'polls', '', 'text', array()); ?>:&nbsp;* </div>
			<div class="v">
				<?php if ($this->_vars['languages_count'] > 0): ?>
					<div id="languages_container">
						<?php if (is_array($this->_vars['languages']) and count((array)$this->_vars['languages'])): foreach ((array)$this->_vars['languages'] as $this->_vars['lang_id'] => $this->_vars['item']): ?>
							<div id="question_<?php echo $this->_vars['lang_id']; ?>
" class="question p-top2 <?php if ($this->_vars['poll_data']['language'] > 0 && $this->_vars['poll_data']['language'] != $this->_vars['lang_id']): ?>hide<?php endif; ?>">
                                <input type="text" <?php if ($this->_vars['max_results']): ?>disabled="disabled"<?php endif; ?> class="question_input" id="question_input_<?php echo $this->_vars['lang_id']; ?>
" name="question[<?php echo $this->_vars['lang_id']; ?>
]"
									   value="<?php if ($this->_vars['poll_data']['question'][$this->_vars['lang_id']]):  echo $this->_vars['poll_data']['question'][$this->_vars['lang_id']];  else:  echo $this->_vars['poll_data']['question']['default'];  endif; ?>">
								&nbsp;|&nbsp;<?php echo $this->_vars['item']['name']; ?>

							</div>
						<?php endforeach; endif; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
		<div class="row">
			<div class="h"><?php echo l('field_poll_type', 'polls', '', 'text', array()); ?>:&nbsp;* </div>
			<div class="v">
				<select name="poll_type">
					<?php if (is_array($this->_vars['poll_types']) and count((array)$this->_vars['poll_types'])): foreach ((array)$this->_vars['poll_types'] as $this->_vars['key'] => $this->_vars['item']): ?>
					<option value="<?php echo $this->_vars['key']; ?>
" <?php if ($this->_vars['poll_data']['poll_type'] == $this->_vars['key']): ?>selected<?php endif; ?>><?php echo $this->_vars['item']; ?>
</option>
					<?php endforeach; endif; ?>
				</select>
			</div>
		</div>
		<div class="row row-zebra">
			<div class="h"><?php echo l('field_answer_type', 'polls', '', 'text', array()); ?>:&nbsp;* </div>
			<div class="v">
				<select name="answer_type" <?php if ($this->_vars['max_results']): ?>disabled="disabled"<?php endif; ?>>
					<option value="0" <?php if ($this->_vars['poll_data']['answer_type'] == 0): ?>selected="selected"<?php endif; ?>><?php echo l('answer_type_0', 'polls', '', 'text', array()); ?></option>
					<option value="1" <?php if ($this->_vars['poll_data']['answer_type'] == 1): ?>selected="selected"<?php endif; ?>><?php echo l('answer_type_1', 'polls', '', 'text', array()); ?></option>
				</select>
			</div>
		</div>
		<div class="row">
			<div class="h"><?php echo l('field_sorter', 'polls', '', 'text', array()); ?>:&nbsp;* </div>
			<div class="v">
				<select name="sorter">
					<option value="0" <?php if ($this->_vars['poll_data']['sorter'] == 0): ?>selected="selected"<?php endif; ?>><?php echo l('sorter_0', 'polls', '', 'text', array()); ?></option>
					<option value="1" <?php if ($this->_vars['poll_data']['sorter'] == 1): ?>selected="selected"<?php endif; ?>><?php echo l('sorter_1', 'polls', '', 'text', array()); ?></option>
					<option value="2" <?php if ($this->_vars['poll_data']['sorter'] == 2): ?>selected="selected"<?php endif; ?>><?php echo l('sorter_2', 'polls', '', 'text', array()); ?></option>
				</select>
			</div>
		</div>
		<div class="row row-zebra">
			<div class="h"><?php echo l('field_show_results', 'polls', '', 'text', array()); ?>: </div>
			<div class="v">
				<input type="checkbox" name="show_results" <?php if ($this->_vars['poll_data']['show_results']): ?>checked<?php endif; ?> />
			</div>
		</div>
		<div class="row">
			<div class="h"><?php echo l('use_comments', 'polls', '', 'text', array()); ?>: </div>
			<div class="v">
				<input type="checkbox" name="use_comments" <?php if ($this->_vars['poll_data']['use_comments']): ?>checked<?php endif; ?> />
			</div>
		</div>
		<div class="row row-zebra">
			<div class="h"><?php echo l('field_date_start', 'polls', '', 'text', array()); ?>: </div>
			<div class="v">
				<input type="text" value="<?php echo $this->_vars['poll_data']['date_start']; ?>
" name="date_start" class="datepicker" id="date_start">
			</div>
		</div>
		<div class="row">
			<div class="h"><?php echo l('field_date_end', 'polls', '', 'text', array()); ?>: </div>
			<div class="v">
				<input id="use_expiration" type="checkbox" name="use_expiration" value="1" <?php if ($this->_vars['poll_data']['use_expiration']): ?>checked<?php endif; ?>>
				<input <?php if (! $this->_vars['poll_data']['use_expiration']): ?>disabled<?php endif; ?> type="text" value="<?php echo $this->_vars['poll_data']['date_end']; ?>
" name="date_end" class="datepicker" id="date_end">
			</div>
		</div>

	</div>

	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div></div>
	<a class="cancel" href="<?php echo $this->_vars['site_url']; ?>
admin/polls"><?php echo l('btn_cancel', 'start', '', 'text', array()); ?></a>
</form>

<?php echo '
	<script type="text/javascript">
		var polls;
		$(function(){
			polls = new adminPolls({
				siteUrl: \'';  echo $this->_vars['site_url'];  echo '\'
			});
			polls.bind_events();
		});
	</script>
'; ?>


<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
