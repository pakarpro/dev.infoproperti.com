<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.truncate.php'); $this->register_modifier("truncate", "tpl_modifier_truncate");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.counter.php'); $this->register_function("counter", "tpl_function_counter");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.helper.php'); $this->register_function("helper", "tpl_function_helper");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-05-06 11:56:46 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
  echo tpl_function_helper(array('func_name' => get_admin_level1_menu,'helper_name' => menu,'func_param' => 'admin_export_menu'), $this);?>
<div class="actions">
	<ul>
		<li id="add"><div class="l"><a href="<?php echo $this->_vars['site_url']; ?>
admin/export/edit/"><?php echo l('btn_add', 'start', '', 'text', array()); ?></a></div></li>
	</ul>
	&nbsp;
</div>

<form id="export_form" action="" method="post">
<table cellspacing="0" cellpadding="0" class="data" width="100%">
	<tr>
		<th class="w100"><?php echo l('field_selection_name', 'export', '', 'text', array()); ?></th>
		<th class="w100"><?php echo l('field_selection_driver', 'export', '', 'text', array()); ?></th>
		<th class="w100"><?php echo l('field_selection_module', 'export', '', 'text', array()); ?></th>
		<th class="w50"><?php echo l('field_selection_status', 'export', '', 'text', array()); ?></th>
		
		<th class="w70">&nbsp;</th>
	</tr>
	<?php if (is_array($this->_vars['selections']) and count((array)$this->_vars['selections'])): foreach ((array)$this->_vars['selections'] as $this->_vars['item']): ?>
		<?php echo tpl_function_counter(array('print' => false,'assign' => counter), $this);?>
		<tr<?php if (!($this->_vars['counter'] % 2)): ?> class="zebra"<?php endif; ?>>				
			<td><?php echo $this->_run_modifier($this->_vars['item']['name'], 'truncate', 'plugin', 1, 100); ?>
</td>
			<td><?php echo $this->_run_modifier($this->_vars['item']['driver']['output_name'], 'truncate', 'plugin', 1, 50); ?>
</td>
			<td><?php echo $this->_run_modifier($this->_vars['item']['module']['output_name'], 'truncate', 'plugin', 1, 50); ?>
</td>
			<td class="center">
				<?php if ($this->_vars['item']['status']): ?>
				<a href="<?php echo $this->_vars['site_url']; ?>
admin/export/activate/<?php echo $this->_vars['item']['id']; ?>
/0"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-full.png" width="16" height="16" border="0" alt="<?php echo l('link_deactivate_selection', 'export', '', 'button', array()); ?>" title="<?php echo l('link_deactivate_selection', 'export', '', 'button', array()); ?>">
				<?php else: ?>
				<a href="<?php echo $this->_vars['site_url']; ?>
admin/export/activate/<?php echo $this->_vars['item']['id']; ?>
/1"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-empty.png" width="16" height="16" border="0" alt="<?php echo l('link_activate_selection', 'export', '', 'button', array()); ?>" title="<?php echo l('link_activate_selection', 'export', '', 'button', array()); ?>"></a>
				<?php endif; ?>
			</td>
			
			<td class="icons">
				<a href="<?php echo $this->_vars['site_url']; ?>
admin/export/edit/<?php echo $this->_vars['item']['id']; ?>
"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-edit.png" width="16" height="16" border="0" alt="<?php echo l('link_edit_selection', 'export', '', 'button', array()); ?>" title="<?php echo l('link_edit_selection', 'export', '', 'button', array()); ?>"></a>
				<a href="<?php echo $this->_vars['site_url']; ?>
admin/export/delete/<?php echo $this->_vars['item']['id']; ?>
" onclick="javascript: if(!confirm('<?php echo l('note_delete_selection', 'export', '', 'js', array()); ?>')) return false;"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-delete.png" width="16" height="16" border="0" alt="<?php echo l('link_delete_selection', 'export', '', 'button', array()); ?>" title="<?php echo l('link_delete_selection', 'export', '', 'button', array()); ?>"></a>
				<?php if ($this->_vars['item']['status']): ?>
				<a href="<?php echo $this->_vars['site_url']; ?>
admin/export/generate/<?php echo $this->_vars['item']['id']; ?>
"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-play.png" width="16" height="16" border="0" alt="<?php echo l('link_generate_selection', 'export', '', 'button', array()); ?>" title="<?php echo l('link_generate_selection', 'export', '', 'button', array()); ?>"></a>
				<?php endif; ?>
			</td>
		</tr>
	<?php endforeach; else: ?>
		<tr><td colspan="6" class="center"><?php echo l('no_selections', 'export', '', 'text', array()); ?></td></tr>
	<?php endif; ?>
</table>
</form>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "pagination.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

<script><?php echo '
	var reload_link = "';  echo $this->_vars['site_url']; ?>
admin/export/index<?php echo '";
	var filter = \'';  echo $this->_vars['filter'];  echo '\';
	var order = \'';  echo $this->_vars['order'];  echo '\';
	var loading_content;
	var order_direction = \'';  echo $this->_vars['order_direction'];  echo '\';
	$(function(){
		$(\'#grouping_all\').bind(\'click\', function(){
			var checked = $(this).is(\':checked\');
			if(checked){
				$(\'input.grouping\').attr(\'checked\', \'checked\');
			}else{
				$(\'input.grouping\').removeAttr(\'checked\');
			}
		});
		
		$(\'#grouping_all\').bind(\'click\', function(){
			var checked = $(this).is(\':checked\');
			if(checked){
				$(\'input[type=checkbox].grouping\').attr(\'checked\', \'checked\');
			}else{
				$(\'input[type=checkbox].grouping\').removeAttr(\'checked\');
			}
		});
	
		$(\'#delete_all\').bind(\'click\', function(){
			if(!$(\'input[type=checkbox].grouping\').is(\':checked\')) return false; 
			if(this.id == \'delete_all\' && !confirm(\'';  echo l('note_selections_delete_all', 'export', '', 'js', array());  echo '\')) return false;
			$(\'#export_form\').attr(\'action\', $(this).find(\'a\').attr(\'href\')).submit();		
			return false;
		});
	});
	function reload_this_page(value){
		var link = reload_link + filter + \'/\' + value + \'/\' + order + \'/\' + order_direction;
		location.href=link;
	}
'; ?>
</script>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
