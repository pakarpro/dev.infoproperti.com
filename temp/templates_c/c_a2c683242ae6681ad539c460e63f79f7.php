<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.escape.php'); $this->register_modifier("escape", "tpl_modifier_escape");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2014-09-09 17:51:45 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<form method="post" action="<?php echo $this->_vars['data']['action']; ?>
" name="save_form">
	<div class="edit-form n150">
		<div class="row header"><?php if ($this->_vars['add_flag']):  echo l('admin_header_driver_field_create', 'export', '', 'text', array());  else:  echo l('admin_header_driver_field_edit', 'export', '', 'text', array());  endif; ?></div>
		<div class="row">
			<div class="h"><?php echo l('field_setting_name', 'export', '', 'text', array()); ?>:&nbsp;* </div>
			<div class="v"><input type="text" name="data[name]" value="<?php echo $this->_run_modifier($this->_vars['data']['name'], 'escape', 'plugin', 1); ?>
"></div>
		</div>
		<div class="row">
			<div class="h"><?php echo l('field_setting_type', 'export', '', 'text', array()); ?>:&nbsp;* </div>
			<div class="v">
				<select name="data[type]">
					<?php if (is_array($this->_vars['types']) and count((array)$this->_vars['types'])): foreach ((array)$this->_vars['types'] as $this->_vars['item']): ?>
					<option value="<?php echo $this->_run_modifier($this->_vars['item'], 'escape', 'plugin', 1); ?>
" <?php if ($this->_vars['item'] == $this->_vars['data']['type']): ?>selected<?php endif; ?>><?php echo l('field_type_'.$this->_vars['item'], 'export', '', 'text', array()); ?></option>
					<?php endforeach; endif; ?>
				</select>
			</div>
		</div>
		
	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div></div>
	<a class="cancel" href="<?php echo $this->_vars['site_url']; ?>
admin/export/driver_fields/<?php echo $this->_vars['driver_gid']; ?>
"><?php echo l('btn_cancel', 'start', '', 'text', array()); ?></a>
</form>
<div class="clr"></div>

<script><?php echo '
$(function(){
	$("div.row:odd").addClass("zebra");
});
'; ?>
</script>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
