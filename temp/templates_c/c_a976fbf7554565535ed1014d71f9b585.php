<?php require_once('/opt/ip/system/libraries/template_lite/plugins/function.pagination.php'); $this->register_function("pagination", "tpl_function_pagination");  require_once('/opt/ip/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  require_once('/opt/ip/system/libraries/template_lite/plugins/modifier.truncate.php'); $this->register_modifier("truncate", "tpl_modifier_truncate");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.seotag.php'); $this->register_function("seotag", "tpl_function_seotag");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-02-28 06:38:47 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

<h1><?php echo tpl_function_seotag(array('tag' => 'header_text'), $this);?></h1>

<?php if (is_array($this->_vars['wish_lists']) and count((array)$this->_vars['wish_lists'])): foreach ((array)$this->_vars['wish_lists'] as $this->_vars['item']): ?>
	<div class="listing-block">
		<div id="item-block-<?php echo $this->_vars['item']['id']; ?>
" class="item wish_list">
			<h3><a href="<?php echo $this->_vars['site_url']; ?>
listings/wish_list/<?php echo $this->_vars['item']['id']; ?>
"><?php echo $this->_run_modifier($this->_vars['item']['output_name'], 'truncate', 'plugin', 1, 50); ?>
</a></h3>
		</div>
	</div>
<?php endforeach; else: ?>
	<div class="item empty"><?php echo l('no_wish_lists', 'listings', '', 'text', array()); ?></div>
<?php endif; ?>
<?php if ($this->_vars['listings']): ?><div id="pages_block_2"><?php echo tpl_function_pagination(array('data' => $this->_vars['page_data'],'type' => 'full'), $this);?></div><?php endif; ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
