<?php require_once('/opt/ip/system/libraries/template_lite/plugins/function.js.php'); $this->register_function("js", "tpl_function_js");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-12-15 13:37:52 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
  $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->module_path. "start". $this->module_templates.  $this->get_current_theme_gid('', '"start"'). "left_panel.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<div class="rc">
	<div class="content-block">
		<?php echo tpl_function_js(array('module' => mailbox,'file' => 'mailbox.js'), $this);?>
		<script type='text/javascript'><?php echo '

			var mb;
			$(function(){
				mb = new Mailbox({
					siteUrl: \'';  echo $this->_vars['site_url'];  echo '\',
					folderId: \'';  echo $this->_vars['data']['active_folder'];  echo '\',
					viewType: \'chats\',
					pageData:{
						orderKey: \'';  echo $this->_vars['data']['order_key'];  echo '\',
						orderDirection: \'';  echo $this->_vars['data']['order_direction'];  echo '\'
					}
				});
			});

		'; ?>
</script>


		<h1><?php echo l('my_communication', 'mailbox', '', 'text', array()); ?></h1>

		<div class="content-value mailbox">
			<div id="folders" class="fright actions">
				<?php echo $this->_vars['folders_block']; ?>

			</div>
			<div id="chat_window"><?php echo $this->_vars['chats_block']; ?>
</div>
			<div class="clr"></div>
		</div>
	</div>
</div>
<div class="clr"></div>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>