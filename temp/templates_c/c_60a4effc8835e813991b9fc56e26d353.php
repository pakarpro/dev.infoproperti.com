<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-04-21 11:39:24 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<form method="post" action="<?php echo $this->_vars['data']['action']; ?>
" name="save_form" enctype="multipart/form-data">
	<div class="edit-form n150">
		<div class="row header"><?php if ($this->_vars['data']['id']):  echo l('admin_header_news_change', 'news', '', 'text', array());  else:  echo l('admin_header_news_add', 'news', '', 'text', array());  endif; ?></div>
		<div class="row">
			<div class="h"><?php echo l('field_name', 'news', '', 'text', array()); ?>:&nbsp;* </div>
			<div class="v"><input type="text" value="<?php echo $this->_vars['data']['name']; ?>
" name="name" class="long"></div>
		</div>
		<div class="row">
			<div class="h"><?php echo l('field_gid', 'news', '', 'text', array()); ?>:&nbsp;* </div>
			<div class="v"><input type="text" value="<?php echo $this->_vars['data']['gid']; ?>
" name="gid" class="long"></div>
		</div>
		<div class="row">
			<div class="h"><?php echo l('field_news_lang', 'news', '', 'text', array()); ?>: </div>
			<div class="v"><select name="id_lang"><?php if (is_array($this->_vars['languages']) and count((array)$this->_vars['languages'])): foreach ((array)$this->_vars['languages'] as $this->_vars['item']): ?><option value="<?php echo $this->_vars['item']['id']; ?>
"<?php if ($this->_vars['item']['id'] == $this->_vars['data']['id_lang']): ?> selected<?php endif; ?>><?php echo $this->_vars['item']['name']; ?>
</option><?php endforeach; endif; ?></select></div>
		</div>
		<div class="row">
			<div class="h"><?php echo l('field_icon', 'news', '', 'text', array()); ?>: </div>
			<div class="v">
				<input type="file" name="news_icon">
				<?php if ($this->_vars['data']['img']): ?>
				<br><img src="<?php echo $this->_vars['data']['media']['img']['thumbs']['small']; ?>
"  hspace="2" vspace="2" />
				<br><input type="checkbox" name="news_icon_delete" value="1" id="uichb"><label for="uichb"><?php echo l('field_icon_delete', 'news', '', 'text', array()); ?></label>
				<?php endif; ?>
			</div>
		</div>
		<div class="row">
			<div class="h"><?php echo l('field_video', 'news', '', 'text', array()); ?>: </div>
			<div class="v">
				<input type="file" name="news_video">
				<?php if ($this->_vars['data']['video']): ?>
					<br><?php echo l('field_video_status', 'news', '', 'text', array()); ?>: 
					<?php if ($this->_vars['data']['video_data']['status'] == 'end' && $this->_vars['data']['video_data']['errors']): ?>
						<font color="red"><?php if (is_array($this->_vars['data']['video_data']['errors']) and count((array)$this->_vars['data']['video_data']['errors'])): foreach ((array)$this->_vars['data']['video_data']['errors'] as $this->_vars['item']):  echo $this->_vars['item']; ?>
<br><?php endforeach; endif; ?></font>
					<?php elseif ($this->_vars['data']['video_data']['status'] == 'end'): ?>	<font color="green"><?php echo l('field_video_status_end', 'news', '', 'text', array()); ?></font><br>
					<?php elseif ($this->_vars['data']['video_data']['status'] == 'images'): ?>	<font color="yellow"><?php echo l('field_video_status_images', 'news', '', 'text', array()); ?></font><br>
					<?php elseif ($this->_vars['data']['video_data']['status'] == 'waiting'): ?> <font color="yellow"><?php echo l('field_video_status_waiting', 'news', '', 'text', array()); ?></font><br>
					<?php elseif ($this->_vars['data']['video_data']['status'] == 'start'): ?> <font color="yellow"><?php echo l('field_video_status_start', 'news', '', 'text', array()); ?></font><br>
					<?php endif; ?>
					<?php if ($this->_vars['data']['video_content']['thumbs']['small']): ?>
					<br><img src="<?php echo $this->_vars['data']['video_content']['thumbs']['small']; ?>
"  hspace="2" vspace="2" />
					<?php endif; ?>
					<br><input type="checkbox" name="news_video_delete" value="1" id="uvchb"><label for="uvchb"><?php echo l('field_video_delete', 'news', '', 'text', array()); ?></label>
				<?php endif; ?>
			</div>
		</div>
		<div class="row">
			<div class="h"><?php echo l('field_annotation', 'news', '', 'text', array()); ?>: </div>
			<div class="v"><textarea name="annotation"><?php echo $this->_vars['data']['annotation']; ?>
</textarea><br><i><?php echo l('field_annotation_text', 'news', '', 'text', array()); ?></i></div>
		</div>
		<div class="row">
			<div class="h"><?php echo l('field_content', 'news', '', 'text', array()); ?>: </div>
			<div class="v"><?php echo $this->_vars['data']['content_fck']; ?>
&nbsp;</div>
		</div>
	
	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div></div>
	<a class="cancel" href="<?php echo $this->_vars['site_url']; ?>
admin/news"><?php echo l('btn_cancel', 'start', '', 'text', array()); ?></a>
</form>
<div class="clr"></div>
<script><?php echo '
$(function(){
	$("div.row:odd").addClass("zebra");
});
'; ?>
</script>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
