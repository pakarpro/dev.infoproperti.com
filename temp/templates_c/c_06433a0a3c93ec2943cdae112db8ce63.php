<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.escape.php'); $this->register_modifier("escape", "tpl_modifier_escape");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.helper.php'); $this->register_function("helper", "tpl_function_helper");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-03-09 12:42:27 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
  echo tpl_function_helper(array('func_name' => get_admin_level1_menu,'helper_name' => menu,'func_param' => 'admin_moderation_menu'), $this);?>
<div class="actions">&nbsp;</div>

<form method="post" action="" name="moder_sattings_save">
	<div class="edit-form n150">
		<?php if (is_array($this->_vars['moder_types']) and count((array)$this->_vars['moder_types'])): foreach ((array)$this->_vars['moder_types'] as $this->_vars['key'] => $this->_vars['item']): ?>
		<div class="row">
			<div class="h"><?php echo l('mtype_'.$this->_vars['item']['name'], 'moderation', '', 'text', array()); ?>:</div>
			<div class="v">
				<input type="hidden" name="type_id[]" value="<?php echo $this->_vars['item']['id']; ?>
">
				<?php if ($this->_vars['item']['mtype'] >= 0): ?>
				<input type="radio" name="mtype[<?php echo $this->_vars['item']['id']; ?>
]" value="2" id="mtype_<?php echo $this->_vars['item']['id']; ?>
_2"<?php if ($this->_vars['item']['mtype'] == '2'): ?>checked<?php endif; ?>><label for="mtype_<?php echo $this->_vars['item']['id']; ?>
_2"><?php echo l('mtype_2', 'moderation', '', 'text', array()); ?></label><br>
				<input type="radio" name="mtype[<?php echo $this->_vars['item']['id']; ?>
]" value="1" id="mtype_<?php echo $this->_vars['item']['id']; ?>
_1"<?php if ($this->_vars['item']['mtype'] == '1'): ?>checked<?php endif; ?>><label for="mtype_<?php echo $this->_vars['item']['id']; ?>
_1"><?php echo l('mtype_1', 'moderation', '', 'text', array()); ?></label><br>
				<input type="radio" name="mtype[<?php echo $this->_vars['item']['id']; ?>
]" value="0" id="mtype_<?php echo $this->_vars['item']['id']; ?>
_0"<?php if ($this->_vars['item']['mtype'] == '0'): ?>checked<?php endif; ?>><label for="mtype_<?php echo $this->_vars['item']['id']; ?>
_0"><?php echo l('mtype_0', 'moderation', '', 'text', array()); ?></label><br>
				<?php else: ?>
				<input type="hidden" value="mtype[<?php echo $this->_vars['item']['id']; ?>
]" value="<?php echo $this->_vars['item']['mtype']; ?>
">
				<?php endif; ?>
				<?php if ($this->_vars['item']['check_badwords'] != '2'): ?><input type="checkbox" name="check_badwords[<?php echo $this->_vars['item']['id']; ?>
]" value="1" <?php if ($this->_vars['item']['check_badwords'] == '1'): ?>checked<?php endif; ?> id="chbw_<?php echo $this->_vars['item']['id']; ?>
"><label for="chbw_<?php echo $this->_vars['item']['id']; ?>
"><?php echo l('check_badwords', 'moderation', '', 'text', array()); ?></label><?php endif; ?>
			</div>
		</div>
		<?php endforeach; else: ?>
		<div class="row content"><?php echo l('no_types', 'moderation', '', 'text', array()); ?></div>
		<?php endif; ?>
		<div class="row">
			<div class="h"><label for="moderation_send_mail"><?php echo l('field_moderation_send_mail', 'moderation', '', 'text', array()); ?></label>: </div>
			<div class="v">
				<input type="hidden" name="moderation_send_mail" value="0"> 
				<input type="checkbox" name="moderation_send_mail" value="1" id="moderation_send_mail" <?php if ($this->_vars['data']['moderation_send_mail']): ?>checked="checked"<?php endif; ?>> 
				&nbsp;&nbsp;
				<?php echo l('field_admin_moderation_emails', 'moderation', '', 'text', array()); ?>
				<input type="text" name="admin_moderation_emails" value="<?php echo $this->_run_modifier($this->_vars['data']['admin_moderation_emails'], 'escape', 'plugin', 1); ?>
" id="admin_moderation_emails" <?php if (! $this->_vars['data']['moderation_send_mail']): ?>disabled<?php endif; ?>> 
			</div>
		</div>
	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div></div>
</form>
<script><?php echo '
$(function(){
	$("div.row:not(.hide):even").addClass("zebra");
	$(\'#moderation_send_mail\').bind(\'change\', function(){
		if(this.checked){
			$(\'#admin_moderation_emails\').removeAttr(\'disabled\');
		}else{
			$(\'#admin_moderation_emails\').attr(\'disabled\', \'disabled\');
		}
	});
});
'; ?>
</script>


<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
