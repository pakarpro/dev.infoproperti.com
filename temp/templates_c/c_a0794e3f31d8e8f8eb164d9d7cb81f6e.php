<?php require_once('/opt/ip/system/libraries/template_lite/plugins/modifier.date_format.php'); $this->register_modifier("date_format", "tpl_modifier_date_format");  require_once('/opt/ip/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-02-21 13:51:09 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
  $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->module_path. "start". $this->module_templates.  $this->get_current_theme_gid('', '"start"'). "left_panel.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<div class="rc">
	<div class="content-block">
		<h1><?php echo l('header_services', 'users_services', '', 'text', array()); ?></h1>
		<p class="header-comment"><?php echo l('text_services', 'users_services', '', 'text', array()); ?></p>
	</div>
	<div class="content-block">
		<h2 class="line top bottom"><?php echo l('header_contact_service', 'users_services', '', 'text', array()); ?></h2>
		<p class="header-comment"><?php echo l('text_contact_service', 'users_services', '', 'text', array()); ?></p>
		<?php if ($this->_vars['current_services']['contact']): ?>
			<table class="list">
			<tr>
				<th><?php echo l('field_date_add', 'users_services', '', 'text', array()); ?></th>
				<th><?php echo l('field_service_name', 'users_services', '', 'text', array()); ?></th>
				<th><?php echo l('field_service_contacts_left', 'users_services', '', 'text', array()); ?></th>
				<th><?php echo l('field_service_exp_date', 'users_services', '', 'text', array()); ?></th>
			</tr>
			<?php if (is_array($this->_vars['current_services']['contact']) and count((array)$this->_vars['current_services']['contact'])): foreach ((array)$this->_vars['current_services']['contact'] as $this->_vars['item']): ?>
				<?php $this->assign('service_name', $this->_vars['item']['service_name']); ?>
				<tr>
					<td><?php echo $this->_run_modifier($this->_vars['item']['date_created'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']); ?>
</td>
					<td>
						<?php echo l("service_string_name_" . $this->_vars['service_name'] . "", 'users_services', '', 'text', array()); ?>
						(<?php if ($this->_vars['item']['service_data']['contact_count'] > 0):  echo $this->_vars['item']['service_data']['contact_count'];  else:  echo l('unlim', 'users_services', '', 'text', array());  endif; ?> <?php echo l('contact_count', 'users_services', '', 'text', array()); ?> /
						<?php if ($this->_vars['item']['service_data']['service_period'] > 0):  echo $this->_vars['item']['service_data']['service_period']; ?>
 <?php echo l('service_expiration_period', 'users_services', '', 'text', array());  else:  echo l('unlim', 'users_services', '', 'text', array());  endif; ?>)
					</td>
					<td><?php if ($this->_vars['item']['contact_count'] > 0):  echo $this->_vars['item']['contact_count'];  else:  echo l('unlim', 'users_services', '', 'text', array());  endif; ?></td>
					<td><?php if ($this->_vars['item']['service_data']['service_period'] > 0):  echo $this->_run_modifier($this->_vars['item']['contact_service_end_date'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']);  else:  echo l('unlim', 'users_services', '', 'text', array());  endif; ?></td>
				</tr>
			<?php endforeach; endif; ?>
			</table><br>
		<?php endif; ?>
		<a class="btn-link" href="<?php echo $this->_vars['site_url']; ?>
users_services/index/contact">
			<ins class="with-icon i-rarr"></ins>
			<?php echo l('btn_pay', 'users_services', '', 'text', array()); ?>
		</a>
		<br>
	</div>
	<div class="content-block">
		<h2 class="line top bottom"><?php echo l('header_post_service_'.$this->_vars['usertype'], 'users_services', '', 'text', array()); ?></h2>
		<p class="header-comment"><?php echo l('text_post_service_'.$this->_vars['usertype'], 'users_services', '', 'text', array()); ?></p>
		<?php if ($this->_vars['current_services']['post']): ?>
			<table class="list">
			<tr>
				<th><?php echo l('field_date_add', 'users_services', '', 'text', array()); ?></th>
				<th><?php echo l('field_service_name', 'users_services', '', 'text', array()); ?></th>
				<th><?php echo l('field_service_post_left', 'users_services', '', 'text', array()); ?></th>
			</tr>
			<?php if (is_array($this->_vars['current_services']['post']) and count((array)$this->_vars['current_services']['post'])): foreach ((array)$this->_vars['current_services']['post'] as $this->_vars['item']): ?>
				<?php $this->assign('service_name', $this->_vars['item']['service_name']); ?>
				<tr>
					<td><?php echo $this->_run_modifier($this->_vars['item']['date_created'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']); ?>
</td>
					<td>
						<b><?php echo l("service_string_name_" . $this->_vars['service_name'] . "", 'users_services', '', 'text', array()); ?></b>
						<?php if ($this->_vars['item']['service_data']['post_count'] > 0):  echo $this->_vars['item']['service_data']['post_count'];  else:  echo l('unlim', 'users_services', '', 'text', array());  endif; ?> <?php echo l('post_count', 'users_services', '', 'text', array()); ?> /
						<?php if ($this->_vars['item']['service_data']['period'] > 0):  echo $this->_vars['item']['service_data']['period'];  else:  echo l('unlim', 'users_services', '', 'text', array());  endif; ?> <?php echo l('service_expiration_period', 'users_services', '', 'text', array()); ?>
					</td>
					<td>
						<?php if ($this->_vars['item']['post_count'] > 0):  echo $this->_vars['item']['post_count'];  else:  echo l('unlim', 'users_services', '', 'text', array());  endif; ?>
					</td>
				</tr>
			<?php endforeach; endif; ?>
			</table><br>
		<?php endif; ?>
		<a class="btn-link" href="<?php echo $this->_vars['site_url']; ?>
users_services/index/post">
			<ins class="with-icon i-rarr"></ins>
			<?php echo l('btn_pay', 'users_services', '', 'text', array()); ?>
		</a>
		<br>
	</div>
	<div class="content-block">
		<h2 class="line top bottom"><?php echo l('header_combined_service', 'users_services', '', 'text', array()); ?></h2>
		<p class="header-comment"><?php echo l('text_combined_service', 'users_services', '', 'text', array()); ?></p>
		<?php if ($this->_vars['current_services']['combined']): ?>
			<table class="list">
			<tr>
				<th><?php echo l('field_date_add', 'users_services', '', 'text', array()); ?></th>
				<th><?php echo l('field_service_name', 'users_services', '', 'text', array()); ?></th>
				<th><?php echo l('field_service_contacts_left', 'users_services', '', 'text', array()); ?></th>
				<th><?php echo l('field_service_post_left', 'users_services', '', 'text', array()); ?></th>
			</tr>
			<?php if (is_array($this->_vars['current_services']['combined']) and count((array)$this->_vars['current_services']['combined'])): foreach ((array)$this->_vars['current_services']['combined'] as $this->_vars['item']): ?>
				<?php $this->assign('service_name', $this->_vars['item']['service_name']); ?>
				<tr>
					<td><?php echo $this->_run_modifier($this->_vars['item']['date_created'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']); ?>
</td>
					<td>
						<b><?php echo l("service_string_name_" . $this->_vars['service_name'] . "", 'users_services', '', 'text', array()); ?></b><br>
						<b><?php echo l('contact_count', 'users_services', '', 'text', array()); ?>:</b> <?php if ($this->_vars['item']['service_data']['contact_count'] > 0):  echo $this->_vars['item']['service_data']['contact_count'];  else:  echo l('unlim', 'users_services', '', 'text', array());  endif; ?>/<?php if ($this->_vars['item']['service_data']['service_period'] > 0):  echo $this->_vars['item']['service_data']['service_period'];  else:  echo l('unlim', 'users_services', '', 'text', array());  endif; ?> <?php echo l('service_expiration_period', 'users_services', '', 'text', array()); ?><br>
						<b><?php echo l('post_count', 'users_services', '', 'text', array()); ?>:</b> <?php if ($this->_vars['item']['service_data']['post_count'] > 0):  echo $this->_vars['item']['service_data']['post_count'];  else:  echo l('unlim', 'users_services', '', 'text', array());  endif; ?> /
						<?php if ($this->_vars['item']['service_data']['period'] > 0):  echo $this->_vars['item']['service_data']['period'];  else:  echo l('unlim', 'users_services', '', 'text', array());  endif; ?> <?php echo l('service_expiration_period', 'users_services', '', 'text', array()); ?>
					</td>
					<td>
					<?php if ($this->_vars['item']['contact_status']): ?>
						<?php if ($this->_vars['item']['contact_count'] > 0):  echo $this->_vars['item']['contact_count'];  else:  echo l('unlim', 'users_services', '', 'text', array());  endif; ?> /
						<?php if ($this->_vars['item']['service_data']['service_period'] > 0):  echo $this->_run_modifier($this->_vars['item']['contact_service_end_date'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']);  else:  echo l('unlim', 'users_services', '', 'text', array());  endif; ?>
					<?php else: ?>
						<?php echo l('service_expiried', 'users_services', '', 'text', array()); ?>
					<?php endif; ?>
					</td>
					<td>
					<?php if ($this->_vars['item']['post_status']): ?>
						<?php if ($this->_vars['item']['post_count'] > 0):  echo $this->_vars['item']['post_count'];  else:  echo l('unlim', 'users_services', '', 'text', array());  endif; ?>
					<?php else: ?>
						<?php echo l('service_expiried', 'users_services', '', 'text', array()); ?>
					<?php endif; ?>
					</td>
				</tr>
			<?php endforeach; endif; ?>
			</table><br>
		<?php endif; ?>
		<a class="btn-link" href="<?php echo $this->_vars['site_url']; ?>
users_services/index/combined">
			<ins class="with-icon i-rarr"></ins>
			<?php echo l('btn_pay', 'users_services', '', 'text', array()); ?>
		</a>
		<br>
	</div>
</div>
<div class="clr"></div>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
