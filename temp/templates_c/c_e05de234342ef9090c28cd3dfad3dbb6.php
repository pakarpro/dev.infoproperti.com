<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.escape.php'); $this->register_modifier("escape", "tpl_modifier_escape");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2014-09-09 17:51:06 KRAT */ ?>

	<div  class="row">
		<div class="h"><?php echo l('field_csv_delimiter', 'export', '', 'text', array()); ?></div>
		<div class="v"><input type="text" name="data[delimiter]" value="<?php echo $this->_run_modifier($this->_vars['data']['delimiter'], 'escape', 'plugin', 1); ?>
" /></div>
	</div>
	<div  class="row">
		<div class="h"><?php echo l('field_csv_enclosure', 'export', '', 'text', array()); ?></div>
		<div class="v"><input type="text" name="data[enclosure]" value="<?php echo $this->_run_modifier($this->_vars['data']['enclosure'], 'escape', 'plugin', 1); ?>
" /></div>
	</div>
