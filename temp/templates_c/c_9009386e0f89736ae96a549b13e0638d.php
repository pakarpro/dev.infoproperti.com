<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.seolink.php'); $this->register_function("seolink", "tpl_function_seolink");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-05-12 05:07:46 KRAT */ ?>

	<div class="profile-info">
		<a href="<?php echo tpl_function_seolink(array('module' => 'users','method' => 'view','data' => $this->_vars['user']), $this);?>"><img src="<?php echo $this->_vars['user']['media']['user_logo']['thumbs']['small']; ?>
" alt="<?php echo $this->_run_modifier($this->_run_modifier($this->_vars['user']['output_name'], 'truncate', 'plugin', 1, 30), 'escape', 'plugin', 1); ?>
" /></a>
		<div class="sponsor-info">
			<h3 class="s-title"><?php echo $this->_run_modifier($this->_vars['user']['output_name'], 'truncate', 'plugin', 1, 100); ?>
</h3>
			<?php if ($this->_vars['user']['phone']):  echo $this->_vars['user']['phone'];  endif; ?>
			<p><?php echo l('text_profile_complete', 'users', '', 'text', array()); ?>:<p>
			<p class="filling-profile">
				<span class="filling-box"><spam class="filling-bar" style="width:<?php echo $this->_vars['complete']; ?>
%"></span></span>
				<span class="percent"><?php echo $this->_vars['complete']; ?>
%</span>
			</p>
			<a href="<?php echo $this->_vars['site_url']; ?>
users/profile" class="btn-link link-r-margin"><ins class="with-icon i-edit"></ins><?php echo l('link_edit_profile', 'users', '', 'text', array()); ?></a>
		</div>
		<div class="clr"></div>
	</div>
