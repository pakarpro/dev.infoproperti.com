<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-02 15:56:07 KRAT */ ?>

<?php echo tpl_function_js(array('module' => 'reviews','file' => 'reviews-form.js'), $this); if ($this->_vars['template'] == 'form'):  if (! $this->_vars['is_send']): ?>
<h1><?php echo l('header_reviews_form', 'reviews', '', 'text', array()); ?></h1>
<div class="edit_block">
	<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->module_path. "reviews". $this->module_templates.  $this->get_current_theme_gid('"index"', '"reviews"'). "review_form.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
</div>
<?php endif;  else: ?>
<a <?php if (! $this->_vars['is_send']): ?>href="<?php echo $this->_vars['site_url']; ?>
reviews/send_review"<?php endif; ?> data-id="<?php echo $this->_vars['object_id']; ?>
" data-type="<?php echo $this->_vars['type']['gid']; ?>
" data-responder="<?php echo $this->_vars['responder_id']; ?>
" id="review_btn_<?php echo $this->_vars['rand']; ?>
" class="btn-link link-r-margin" title="<?php if ($this->_vars['is_send']):  echo l('text_send_review', 'reviews', '', 'button', array());  else:  echo l('link_send_review', 'reviews', '', 'button', array());  endif; ?>"><ins class="with-icon <?php if ($this->_vars['is_send']): ?>g no-hover<?php endif; ?> i-reviews"></ins></a>
<?php endif;  if (! $this->_vars['is_send'] || $this->_vars['template'] == 'form'): ?>
<script><?php echo '
$(function(){
	';  if ($this->_vars['is_guest']):  echo '
	$(\'#review_btn_';  echo $this->_vars['rand'];  echo '\').bind(\'click\', function(){
		$(\'html, body\').animate({
			scrollTop: $("#ajax_login_link").offset().top
		}, 2000);
		$("#ajax_login_link").click();
		return false;
	});
	';  else:  echo '
	new ReviewsForm({
		siteUrl: \'';  echo $this->_vars['site_url'];  echo '\', 
		';  if ($this->_vars['template'] == 'form'): ?>isPopup: false,<?php endif;  echo '
		';  if ($this->_vars['success']): ?>success: <?php $this->_tag_stack[] = array('tpl_block_strip', array()); tpl_block_strip(array(), null, $this); ob_start();  echo $this->_vars['success'];  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_strip($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>,<?php endif;  echo '
		rand: ';  echo $this->_vars['rand'];  echo ',
	});	
	';  endif;  echo '
});
'; ?>
</script>
<?php endif; ?>

