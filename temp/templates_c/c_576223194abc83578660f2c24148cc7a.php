<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.block.php'); $this->register_function("block", "tpl_function_block");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.helper.php'); $this->register_function("helper", "tpl_function_helper");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.seotag.php'); $this->register_function("seotag", "tpl_function_seotag");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-05-08 10:02:02 KRAT */ ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html DIR="<?php echo $this->_vars['_LANG']['rtl']; ?>
">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="expires" content="0">
	<meta http-equiv="pragma" content="no-cache">
	<meta name="revisit-after" content="3 days">
	<meta name="robot" content="All">
	<?php echo tpl_function_seotag(array('tag' => 'title|description|keyword'), $this);?>

	<?php echo tpl_function_helper(array('func_name' => css,'helper_name' => theme,'func_param' => $this->_vars['load_type']), $this);?>
	<script type="text/javascript">
		var site_url = '<?php echo $this->_vars['site_url']; ?>
';
		var site_rtl_settings = '<?php echo $this->_vars['_LANG']['rtl']; ?>
';
		var site_error_position = 'left';
	</script>

	<link rel="shortcut icon" href="<?php echo $this->_vars['site_root']; ?>
favicon.ico">
	<?php echo tpl_function_helper(array('func_name' => js,'helper_name' => theme,'func_param' => $this->_vars['load_type']), $this);?>
</head>

<body>

<?php echo tpl_function_helper(array('func_name' => demo_panel,'helper_name' => start,'func_param' => 'admin'), $this);?>
	<div id="error_block"><?php if (is_array($this->_vars['_PREDEFINED']['error']) and count((array)$this->_vars['_PREDEFINED']['error'])): foreach ((array)$this->_vars['_PREDEFINED']['error'] as $this->_vars['item']):  echo $this->_vars['item']; ?>
<br><?php endforeach; endif; ?></div>
	<div id="info_block"><?php if (is_array($this->_vars['_PREDEFINED']['info']) and count((array)$this->_vars['_PREDEFINED']['info'])): foreach ((array)$this->_vars['_PREDEFINED']['info'] as $this->_vars['item']):  echo $this->_vars['item']; ?>
<br><?php endforeach; endif; ?></div>
	<div id="success_block"><?php if (is_array($this->_vars['_PREDEFINED']['success']) and count((array)$this->_vars['_PREDEFINED']['success'])): foreach ((array)$this->_vars['_PREDEFINED']['success'] as $this->_vars['item']):  echo $this->_vars['item']; ?>
<br><?php endforeach; endif; ?></div>

	<div class="bg">
		<div class="main">
			<!-- Left column -->
			<div class="lc">
				<!-- Logo -->
				<div class="logo">
					<div class="b">
						<a href="<?php echo $this->_vars['site_url']; ?>
admin/"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['logo_settings']['path']; ?>
" border="0" alt="logo" width="<?php echo $this->_vars['logo_settings']['width']; ?>
" height="<?php echo $this->_vars['logo_settings']['height']; ?>
"></a>
					</div>
				</div>

				<!-- Menu -->
				<?php if ($this->_vars['initial_setup']): ?>
					<?php echo tpl_function_helper(array('func_name' => get_initial_setup_menu,'helper_name' => install,'func_param' => $this->_vars['step']), $this);?>
				<?php elseif ($this->_vars['modules_setup']): ?>
					<?php echo tpl_function_helper(array('func_name' => get_modules_setup_menu,'helper_name' => install,'func_param' => $this->_vars['step']), $this);?>
				<?php elseif ($this->_vars['product_setup']): ?>
					<?php echo tpl_function_helper(array('func_name' => get_product_setup_menu,'helper_name' => install,'func_param' => $this->_vars['step']), $this);?>
				<?php else: ?>
					<?php echo tpl_function_helper(array('func_name' => get_admin_main_menu,'helper_name' => menu), $this);?>
				<?php endif; ?>

			</div>

			<!-- Right column -->
			<div class="rc">
			<?php if ($this->_vars['auth_type'] == 'admin'): ?>
				<div class="w-login-str">
					<div class="version"><?php echo tpl_function_helper(array('func_name' => product_version,'helper_name' => start), $this);?></div>
				<?php if (! $this->_vars['initial_setup'] && ! $this->_vars['modules_setup'] && ! $this->_vars['product_setup']): ?>
					<?php echo tpl_function_block(array('name' => lang_select,'module' => languages), $this);?>
				<?php endif; ?> |
				<a href="https://pilotgroup.zendesk.com/home" target="_blank"><?php echo l('link_get_help', start, '', 'text', array()); ?></a>
				
				<script><?php echo '
					function redirectDocumentation(value) {
						if (value) {
							document.location.href = value;
						}
						return false;
					}
				'; ?>
</script>

				<?php if ($this->_vars['modules_setup']): ?> | <a href="<?php echo $this->_vars['site_url']; ?>
admin/install/logoff" class="logoff">LogOff</a>
				<?php elseif ($this->_vars['initial_setup'] || $this->_vars['product_setup']): ?>
				<?php elseif ($this->_vars['auth_type'] == 'admin'): ?> | <a href="<?php echo $this->_vars['site_url']; ?>
admin/ausers/logoff" class="logoff">LogOff</a>
				<?php endif; ?>
				</div>
				<?php endif; ?>
				<div class="w-area">
					<div class="b">
<?php if ($this->_vars['_PREDEFINED']['back_link']): ?>
							<div class="quest-block">
								 <a href="<?php echo $this->_vars['_PREDEFINED']['back_link']; ?>
" class="back"><?php echo l('btn_back', 'start', '', 'text', array()); ?></a>&nbsp;

							</div>
<?php endif; ?>
							<h1><?php echo $this->_vars['_PREDEFINED']['header']; ?>
</h1>
