<?php require_once('/opt/ip/system/libraries/template_lite/plugins/function.block.php'); $this->register_function("block", "tpl_function_block");  require_once('/opt/ip/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-02 15:54:48 KRAT */ ?>

<?php 
$this->assign('no_info_str', l('no_information', 'users', '', 'text', array()));
 ?>
<?php 
$this->assign('buy_str', l('please_buy', 'users', '', 'text', array()));
 ?>
<?php 
$this->assign('not_access_str', l('not_access', 'users', '', 'text', array()));
 ?>

<?php if ($this->_vars['section_gid'] == 'overview'): ?>
	
<?php endif; ?>
<?php if ($this->_vars['section_gid'] == 'company'): ?>
	
<?php endif; ?>
<?php if ($this->_vars['section_gid'] == 'reviews'): ?>
	<?php echo tpl_function_block(array('name' => get_reviews_block,'module' => reviews,'object_id' => $this->_vars['user']['id'],'type_gid' => 'users_object'), $this);?>
<?php endif; ?>
<?php if ($this->_vars['section_gid'] == 'contacts'): ?>
	<?php echo tpl_function_block(array('name' => view_user_block,'module' => users,'user_id' => $this->_vars['user']['id'],'user' => $this->_vars['user']), $this);?>
<?php endif; ?>
<?php if ($this->_vars['section_gid'] == 'map'): ?>
	<?php if (! $this->_vars['ajax']): ?>
    <?php echo '
	<script>
	if(typeof(get_user_geocode_data) != \'function\'){
	    function get_user_geocode_data(point_gid, lat, lon){
		$.ajax({
		    url: \'';  echo $this->_vars['site_url'];  echo 'users/ajax_set_geocode_coordinates/\'+point_gid,
		    data: \'lat=\'+lat+\'&lon=\'+lon,
		    type: \'post\'
		});
	    }
	}
	</script>
	'; ?>

	<?php echo tpl_function_block(array('name' => show_default_map,'module' => geomap,'gid' => 'user_profile','id_user' => $this->_vars['user_id'],'object_id' => $this->_vars['user']['id'],'markers' => $this->_vars['markers'],'settings' => $this->_vars['map_settings'],'width' => '630','height' => '500','only_load_scripts' => 1), $this);?>	
	<?php endif; ?>
   
	<?php if ($this->_vars['user']['no_access_contact']): ?>
		<div class="r gray_italic"><?php echo $this->_vars['not_access_str']; ?>
</div>
	<?php elseif ($this->_vars['user']['is_contact']): ?>
		<?php echo tpl_function_block(array('name' => show_default_map,'module' => geomap,'gid' => 'user_profile','id_user' => $this->_vars['user_id'],'object_id' => $this->_vars['user']['id'],'markers' => $this->_vars['markers'],'settings' => $this->_vars['map_settings'],'width' => '630','height' => '500','only_load_content' => 1), $this);?>
	<?php else: ?>
		<div class="r gray_italic"><?php echo $this->_vars['buy_str']; ?>
</div>
	<?php endif; ?>
	<?php if (! $this->_vars['user']['is_contact'] && ! $this->_vars['user']['no_access_contact']): ?>
	<div class="buy-box"><input type="button" value="<?php echo l('btn_activate_contact', 'services', '', 'text', array()); ?>" name="map_btn" /></div>
	<?php endif; ?>
<?php endif; ?>
<?php if ($this->_vars['section_gid'] == 'map_info'): ?>
    <div id="map_container_info">
	<?php if ($this->_vars['user']['no_access_contact']): ?>
		<div class="r gray_italic"><?php echo $this->_vars['not_access_str']; ?>
</div>
	<?php elseif ($this->_vars['user']['is_contact']): ?>
		<?php echo tpl_function_block(array('name' => show_default_map,'module' => geomap,'gid' => 'user_info','id_user' => $this->_vars['user_id'],'object_id' => $this->_vars['user']['id'],'markers' => $this->_vars['markers'],'settings' => $this->_vars['map_settings'],'width' => '300','height' => '300','only_load_content' => 1), $this);?>
	<?php else: ?>
	   <div class="r gray_italic"><?php echo $this->_vars['buy_str']; ?>
</div>
	<?php endif; ?>
	<?php if (! $this->_vars['user']['is_contact'] && ! $this->_vars['user']['no_access_contact']): ?>
	<div class="buy-box"><input type="button" value="<?php echo l('btn_activate_contact', 'services', '', 'text', array()); ?>" name="map_info_btn" /></div>
	<?php endif; ?>
    </div>
<?php endif; ?>
<?php if ($this->_vars['section_gid'] == 'listings'): ?>
<?php endif; ?>
