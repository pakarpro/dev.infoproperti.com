<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.block.php'); $this->register_function("block", "tpl_function_block");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.escape.php'); $this->register_modifier("escape", "tpl_modifier_escape");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2014-05-23 15:10:50 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array('load_type' => 'ui'));
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<form method="post" action="<?php echo $this->_vars['data']['action']; ?>
" name="save_form" enctype="multipart/form-data">
	<div class="edit-form n150">
		<div class="row header"><?php if ($this->_vars['data']['id']):  echo l('admin_header_field_change', 'field_editor', '', 'text', array());  else:  echo l('admin_header_field_add', 'field_editor', '', 'text', array());  endif; ?></div>
		<div class="row">
			<div class="h"><?php echo l('field_gid', 'field_editor', '', 'text', array()); ?>: </div>
			<div class="v"><?php echo $this->_vars['data']['gid']; ?>
</div>
		</div>
		<div class="row">
			<div class="h"><?php echo l('field_section_data', 'field_editor', '', 'text', array()); ?>: </div>
			<div class="v"><?php echo $this->_vars['section_data']['name']; ?>
</div>
		</div>
		<div class="row">
			<div class="h"><?php echo l('field_field_type', 'field_editor', '', 'text', array()); ?>:&nbsp;* </div>
			<div class="v">
			<?php if ($this->_vars['data']['id']): ?>
			<?php if (is_array($this->_vars['field_type_lang']['option']) and count((array)$this->_vars['field_type_lang']['option'])): foreach ((array)$this->_vars['field_type_lang']['option'] as $this->_vars['key'] => $this->_vars['item']):  if ($this->_vars['key'] == $this->_vars['data']['field_type']):  echo $this->_vars['item'];  endif;  endforeach; endif; ?>
			<?php else: ?>
				<select name="field_type">
				<?php if (is_array($this->_vars['field_type_lang']['option']) and count((array)$this->_vars['field_type_lang']['option'])): foreach ((array)$this->_vars['field_type_lang']['option'] as $this->_vars['key'] => $this->_vars['item']): ?><option value="<?php echo $this->_vars['key']; ?>
"<?php if ($this->_vars['key'] == $this->_vars['data']['field_type']): ?> selected<?php endif; ?>><?php echo $this->_vars['item']; ?>
</option><?php endforeach; endif; ?>
				</select>
			<?php endif; ?>
			</div>
		</div>
		<div class="row">
			<div class="h"><?php echo l('field_name', 'field_editor', '', 'text', array()); ?>:&nbsp;* </div>
			<div class="v">
				<input type="text" value="<?php if ($this->_vars['validate_lang']):  echo $this->_vars['validate_lang'][$this->_vars['cur_lang']];  else:  echo $this->_vars['data']['name'];  endif; ?>" name="langs[<?php echo $this->_vars['cur_lang']; ?>
]">
				<?php if ($this->_vars['languages_count'] > 1): ?>
				&nbsp;&nbsp;<a href="#" onclick="showLangs('name_langs'); return false;"><?php echo l('others_languages', 'field_editor', '', 'text', array()); ?></a><br>
				<div id="name_langs" class="hide p-top2">
					<?php if (is_array($this->_vars['languages']) and count((array)$this->_vars['languages'])): foreach ((array)$this->_vars['languages'] as $this->_vars['lang_id'] => $this->_vars['item']):  if ($this->_vars['lang_id'] != $this->_vars['cur_lang']): ?>
					<input type="text" value="<?php if ($this->_vars['validate_lang']):  echo $this->_vars['validate_lang'][$this->_vars['lang_id']];  else:  echo $this->_vars['data']['name'];  endif; ?>" name="langs[<?php echo $this->_vars['lang_id']; ?>
]">&nbsp;|&nbsp;<?php echo $this->_vars['item']['name']; ?>
<br>
					<?php endif;  endforeach; endif; ?>
				</div>
				<?php endif; ?>
			</div>
		</div>
		<?php if ($this->_vars['type_settings']['fulltext_use']): ?>
		<div class="row">
			<div class="h"><?php echo l('field_fts', 'field_editor', '', 'text', array()); ?>: </div>
			<div class="v"><input type="checkbox" value="1" name="fts" <?php if ($this->_vars['data']['fts']): ?>checked<?php endif; ?>></div>
		</div>
		<?php endif; ?>
		<div id="type_block">
		<?php echo $this->_vars['type_block_content']; ?>

		</div>
		<div class="row">
			<div class="h"><?php echo l('field_comment', 'field_editor', '', 'text', array()); ?>: </div>
			<div class="v">
				<?php if (is_array($this->_vars['languages']) and count((array)$this->_vars['languages'])): foreach ((array)$this->_vars['languages'] as $this->_vars['lang_id'] => $this->_vars['lang_item']): ?>
				<?php $this->assign('name', 'comment_'.$this->_vars['lang_id']); ?>
				<?php if ($this->_vars['lang_id'] == $this->_vars['cur_lang']): ?>
				<textarea name="comment[<?php echo $this->_vars['name']; ?>
]" rows="5" cols="80" lang-editor="value" lang-editor-type="data-name" lang-editor-lid="<?php echo $this->_vars['lang_id']; ?>
"><?php echo $this->_run_modifier($this->_vars['data'][$this->_vars['name']], 'escape', 'plugin', 1); ?>
</textarea>
				<?php else: ?>
				<input type="hidden" name="comment[<?php echo $this->_vars['name']; ?>
]" value="<?php echo $this->_run_modifier($this->_vars['data'][$this->_vars['name']], 'escape', 'plugin', 1); ?>
" lang-editor="value" lang-editor-type="data-name" lang-editor-lid="<?php echo $this->_vars['lang_id']; ?>
" />
				<?php endif; ?>
				<?php endforeach; endif; ?>
				<a href="#" lang-editor="button" lang-editor-type="data-name"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-translate.png" width="16" height="16"></a>
			</div>
		</div>
	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div></div>
	<a class="cancel" href="<?php echo $this->_vars['site_url']; ?>
admin/field_editor/fields/<?php echo $this->_vars['type']; ?>
/<?php echo $this->_vars['section']; ?>
"><?php echo l('btn_cancel', 'start', '', 'text', array()); ?></a>
</form>
<div class="clr"></div>
<?php echo tpl_function_block(array('name' => lang_inline_editor,'module' => start,'textarea' => 1), $this);?>
<script><?php echo '
$(function(){
	$("div.row:odd").addClass("zebra");
});
function showLangs(divId){
	$(\'#\'+divId).slideToggle();
}

'; ?>
</script>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
