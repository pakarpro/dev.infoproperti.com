<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2014-10-15 15:18:16 KRAT */ ?>

<?php if (is_array($this->_vars['pages']) and count((array)$this->_vars['pages'])): foreach ((array)$this->_vars['pages'] as $this->_vars['key'] => $this->_vars['item']): ?>
<li id="module_pages_<?php echo $this->_vars['module_id']; ?>
_<?php echo $this->_vars['key']; ?>
" <?php if ($this->_vars['item']['group_id']): ?>class="inactive"<?php else: ?>class="sortable"<?php endif; ?>>
	<b class="name"><?php echo $this->_vars['item']['name']; ?>
</b><br><i><?php echo $this->_vars['site_url']; ?>
<span class="link"><?php echo $this->_vars['item']['link']; ?>
</span></i>
	<?php if ($this->_vars['item']['group_id']): ?><br><font class="used_page"><?php echo l('page_used_in_group', 'banners', '', 'text', array()); ?>: <?php echo $this->_vars['item']['group_name']; ?>
</font><?php endif; ?>
</li>
<?php endforeach; endif; ?>