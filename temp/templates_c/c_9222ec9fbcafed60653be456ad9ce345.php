<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.math.php'); $this->register_function("math", "tpl_function_math");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2014-04-16 14:44:20 KRAT */ ?>

<?php if ($this->_vars['poll_data']['show_results']): ?>
	<div class="results_<?php echo $this->_vars['poll_data']['id']; ?>
">
		<?php if (is_array($this->_vars['poll_data']['results']) and count((array)$this->_vars['poll_data']['results'])): foreach ((array)$this->_vars['poll_data']['results'] as $this->_vars['key'] => $this->_vars['item']): ?>
			<?php $this->assign('item', $this->_vars['poll_data']['answers_colors'][$this->_vars['key']]); ?>
			<?php $this->assign('respond', $this->_vars['poll_data']['results'][$this->_vars['key']]); ?>
			<?php if (! $this->_vars['respond']):  $this->assign('respond', 0);  endif; ?>
			<?php if ($this->_vars['max_results']): ?>
				<div class="poll_result" id="result_answer_<?php echo $this->_vars['item']; ?>
">
					<?php if ($this->_vars['language']):  $this->assign('language_item', $this->_vars['key'].'_'.$this->_vars['language']);  else:  $this->assign('language_item', $this->_vars['key'].'_'.$this->_vars['cur_lang']);  endif; ?>
					<?php echo $this->_vars['poll_data']['answers_languages'][$this->_vars['language_item']]; ?>

					<br />
					<div class="poll_progress" style="background-color: #<?php echo $this->_vars['item']; ?>
; width: <?php echo tpl_function_math(array('equation' => "floor((x / y) * z * 0.7)+1",'x' => $this->_vars['respond'],'y' => $this->_vars['max_results'],'z' => 100), $this);?>%;"></div>
					<div class="percent">
						<?php echo tpl_function_math(array('equation' => "floor((x / y) * z)",'x' => $this->_vars['respond'],'y' => $this->_vars['max_results'],'z' => 100), $this);?>%
					</div>
				</div>
				<br />
			<?php else: ?>
				<div class="poll_result" id="result_answer_<?php echo $this->_vars['item']; ?>
">
					<?php if ($this->_vars['language']):  $this->assign('language_item', $this->_vars['key'].'_'.$this->_vars['language']);  else:  $this->assign('language_item', $this->_vars['key'].'_'.$this->_vars['cur_lang']);  endif; ?>
					<?php echo $this->_vars['poll_data']['answers_languages'][$this->_vars['language_item']]; ?>

					<br />
					<div class="poll_progress" style="float: left; background-color: #<?php echo $this->_vars['item']; ?>
; width: 1%;"></div>
					<div style="float: left; margin-left: 10px;">0%</div>
				</div>
				<br />
			<?php endif; ?>
		<?php endforeach; endif; ?>
	</div>
<?php else: ?>
	<p><?php echo l('dont_show_results_message', 'polls', '', 'text', array()); ?></p>
<?php endif; ?>
<?php if (! $this->_vars['one_poll_place'] && 1 < $this->_vars['polls_count']): ?>
	<div class="poll_action">
		<a class="poll_link next_poll" href="javascript:void(0);"><?php echo l('next_poll', 'polls', '', 'text', array()); ?></a>
	</div>
<?php endif; ?>