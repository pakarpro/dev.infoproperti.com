<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-02-24 13:50:04 KRAT */ ?>

<div id="listing_select_<?php echo $this->_vars['select_data']['rand']; ?>
" class="listing-select">
	<?php if ($this->_vars['select_data']['max_select'] == 1):  echo l('text_filter', 'listings', '', 'text', array()); ?>:<?php endif; ?>
	<a href="#" id="listing_link_<?php echo $this->_vars['select_data']['rand']; ?>
"><?php echo l('link_manage_listings', 'listings', '', 'text', array()); ?></a><?php if ($this->_vars['select_data']['max_select'] > 1): ?> <i>(<?php echo l('max_listing_select', 'listings', '', 'text', array()); ?>: <?php echo $this->_vars['select_data']['max_select']; ?>
)</i><?php endif; ?><br>
	<?php if ($this->_vars['select_data']['max_select'] > 1): ?>
	<span id="listing_text_<?php echo $this->_vars['select_data']['rand']; ?>
">
	<?php if (is_array($this->_vars['select_data']['selected']) and count((array)$this->_vars['select_data']['selected'])): foreach ((array)$this->_vars['select_data']['selected'] as $this->_vars['item']): ?>
	<?php echo $this->_run_modifier($this->_vars['item']['output_name'], 'truncate', 'plugin', 1, 100); ?>
 <?php if ($this->_vars['select_data']['max_select'] != 1): ?><br><?php endif; ?><input type="hidden" name="<?php echo $this->_vars['select_data']['var_name'];  if ($this->_vars['select_data']['max_select'] != 1): ?>[]<?php endif; ?>" value="<?php echo $this->_vars['item']['id']; ?>
">
	<?php endforeach; endif; ?>
	</span>
	<?php else: ?>
	<div id="listing_text_<?php echo $this->_vars['select_data']['rand']; ?>
">
	<?php if (is_array($this->_vars['select_data']['selected']) and count((array)$this->_vars['select_data']['selected'])): foreach ((array)$this->_vars['select_data']['selected'] as $this->_vars['item']): ?>
	<div class="item">
		<a href="<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'view','data' => $this->_vars['item']), $this);?>" title="<?php echo l('btn_preview', 'start', '', 'button', array()); ?>"><img src="<?php echo $this->_vars['item']['media']['photo']['thumbs']['small']; ?>
" alt="<?php echo $this->_run_modifier($this->_run_modifier($this->_vars['item']['output_name'], 'truncate', 'plugin', 1, 30), 'escape', 'plugin', 1); ?>
" title="<?php echo $this->_run_modifier($this->_vars['item']['output_name'], 'escape', 'plugin', 1); ?>
"></a>
		<span><?php echo $this->_run_modifier($this->_vars['item']['output_name'], 'truncate', 'plugin', 1, 100); ?>
</span>
		<span><?php echo $this->_vars['item']['property_type_str']; ?>
 <?php echo $this->_vars['item']['operation_type_str']; ?>
</span>
		<span><?php echo tpl_function_block(array('name' => listing_price_block,'module' => 'listings','data' => $this->_vars['item'],'template' => 'small'), $this);?></span>
		<div class="clr"></div>
		<input type="hidden" name="<?php echo $this->_vars['select_data']['var_name'];  if ($this->_vars['select_data']['max_select'] != 1): ?>[]<?php endif; ?>" value="<?php echo $this->_vars['item']['id']; ?>
">
	</div>
	<?php endforeach; endif; ?>
	</div>
	<?php endif; ?>
	<div class="clr"></div>
</div>

<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'append_callback')); tpl_block_capture(array('assign' => 'append_callback'), null, $this); ob_start();  echo '
	function(data){
	';  if ($this->_vars['select_data']['max_select'] > 1):  echo '
	return data.output_name +
	\' <br><input type="hidden" name="';  echo $this->_vars['select_data']['var_name'];  echo '[]" value="\'+data.id+\'">\';	
	';  else:  echo '
	return \'\' +
	\'<div class="item">\' +
	\'	<a href="\'+data.seo_url+\'" title="';  echo l('btn_preview', 'start', '', 'js', array());  echo '"><img src="\'+data.media.photo.thumbs.small+\'" alt="\'+data.output_name+\'" title="\'+data.output_name+\'"></a>\' +
	\'	<span>\'+data.output_name+\'</span>\' +
	\'	<span>\'+data.property_type_str+\' \'+data.operation_type_str+\'</span>\' +
	\'	<span>\'+data.price_str+\'</span>\' +
	\'	<div class="clr"></div>\' +
	\'	<input type="hidden" name="';  echo $this->_vars['select_data']['var_name'];  echo '" value="\'+data.id+\'">\' +
	\'</div>\';
	';  endif;  echo '
	}
';  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack);  echo tpl_function_js(array('module' => listings,'file' => 'listings-select.js'), $this);?>
<script type='text/javascript'><?php echo '
$(function(){
	new listingsSelect({
		siteUrl: \'';  echo $this->_vars['site_url'];  echo '\',	
		selected_items: [';  echo $this->_vars['select_data']['selected_str'];  echo '],
		max: \'';  echo $this->_vars['select_data']['max_select'];  echo '\',
		var_name: \'';  echo $this->_vars['select_data']['var_name'];  echo '\',
		template: \'';  echo $this->_vars['select_data']['template'];  echo '\',
		params: {';  if (is_array($this->_vars['select_data']['params']) and count((array)$this->_vars['select_data']['params'])): foreach ((array)$this->_vars['select_data']['params'] as $this->_vars['key'] => $this->_vars['item']):  echo $this->_vars['key']; ?>
:'<?php echo $this->_vars['item']; ?>
',<?php endforeach; endif;  echo '},
		';  if ($this->_vars['select_data']['callback']): ?>change_callback: <?php echo $this->_vars['select_data']['callback']; ?>
,<?php endif;  echo '
		append_callback: ';  echo $this->_vars['append_callback'];  echo ',
		rand: \'';  echo $this->_vars['select_data']['rand'];  echo '\',	
	});
});
'; ?>
</script>
