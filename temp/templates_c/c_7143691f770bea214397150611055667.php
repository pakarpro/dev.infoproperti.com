<?php require_once('/opt/ip/system/libraries/template_lite/plugins/modifier.truncate.php'); $this->register_modifier("truncate", "tpl_modifier_truncate");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-12-15 13:37:52 KRAT */ ?>

	<select name="folder">
	<?php if (is_array($this->_vars['folders']) and count((array)$this->_vars['folders'])): foreach ((array)$this->_vars['folders'] as $this->_vars['key'] => $this->_vars['item']): ?>
	<option gid="<?php echo $this->_vars['item']['folder_type']; ?>
" value="<?php echo $this->_vars['item']['id']; ?>
" <?php if ($this->_vars['item']['id'] == $this->_vars['id_active_folder']): ?>selected="selected"<?php endif; ?>><?php echo $this->_run_modifier($this->_vars['item']['name'], 'truncate', 'plugin', 1, 12, '...', true); ?>
 (<?php echo $this->_vars['item']['chats']; ?>
)</option>
	<?php endforeach; endif; ?>
	</select>
	
	<a href="<?php echo $this->_vars['site_url']; ?>
mailbox/folders" class="btn-link fright"><ins class="with-icon i-folder-add"></ins></a>