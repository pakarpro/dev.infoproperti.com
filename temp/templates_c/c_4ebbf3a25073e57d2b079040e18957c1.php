<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-02 15:51:01 WIB */ ?>

<?php if ($this->_vars['users']): ?>
<div class="container-home-featured-companies">
	<div class="slider-container featured-companies">
		<h2 class="head-title">
			<?php if (! $this->_vars['header_featured_users'] || $this->_vars['header_featured_users'] == ''): ?>
			<?php echo l('header_featured_users', 'users', '', 'text', array()); ?>
			<?php else: ?>
			<?php echo $this->_vars['header_featured_users']; ?>

			<?php endif; ?>
		</h2>
		<!-- <div class="<?php echo $this->_vars['type']; ?>
_users_block"> -->
		<ul class="feat-comp clearfix">
			<?php if (is_array($this->_vars['users']) and count((array)$this->_vars['users'])): foreach ((array)$this->_vars['users'] as $this->_vars['key'] => $this->_vars['item']): ?>
			<li>
				<!--<div class="user <?php echo $this->_vars['photo_size']; ?>
">-->
					<?php if ($this->_vars['item']['media']['user_logo']['thumbs'][$this->_vars['photo_size']]): ?>
					<a class="feat-thumb" href="<?php echo tpl_function_seolink(array('module' => 'users','method' => 'view','data' => $this->_vars['item']), $this);?>"><img src="<?php echo $this->_vars['item']['media']['user_logo']['thumbs'][$this->_vars['photo_size']]; ?>
" alt="<?php echo $this->_run_modifier($this->_run_modifier($this->_vars['item']['output_name'], 'truncate', 'plugin', 1, 30), 'escape', 'plugin', 1); ?>
" title="<?php echo $this->_run_modifier($this->_vars['item']['output_name'], 'escape', 'plugin', 1); ?>
"></a>
					<?php endif; ?>
					<a href="<?php echo tpl_function_seolink(array('module' => 'users','method' => 'view','data' => $this->_vars['item']), $this);?>"><?php echo $this->_run_modifier($this->_vars['item']['output_name'], 'truncate', 'plugin', 1, 30); ?>
</a>
					<span class="business-type"><?php echo l($this->_vars['item']['user_type'], 'users', '', 'text', array()); ?></span>
				<!--</div>-->
			</li>
			<?php endforeach; endif; ?>
		</ul>
		<!-- </div> -->
	</div>
</div>

<?php endif; ?>
