<?php require_once('/opt/ip/system/libraries/template_lite/plugins/function.seotag.php'); $this->register_function("seotag", "tpl_function_seotag");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.seolink.php'); $this->register_function("seolink", "tpl_function_seolink");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.helper.php'); $this->register_function("helper", "tpl_function_helper");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-02 15:54:36 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array('load_type' => 'ui'));
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<?php echo tpl_function_helper(array('func_name' => 'listings_pagination_block','module' => 'listings','func_param' => $this->_vars['listing']), $this);?>	
<?php echo tpl_function_block(array('name' => show_social_networks_like,'module' => social_networking,'func_param' => true), $this);?>

<div class="rc_wrapper">
<div class="panel">
<div class="inside">

<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => "review_callback")); tpl_block_capture(array('assign' => "review_callback"), null, $this); ob_start();  echo '
	function(data){
		window.location.href = \'';  echo tpl_function_seolink(array('module' => 'listings','method' => 'view','data' => $this->_vars['listing'],'section' => 'reviews'), $this);?>#m_reviews<?php echo '\';
	}
';  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>

<div class="lc-1" id="view_listing">
	<h1><?php echo tpl_function_seotag(array('tag' => 'header_text'), $this);?></h1>
		<div class="actions noPrint clearfix">
			<div class="act-icon add-to-fav"><?php echo tpl_function_block(array('name' => 'save_listing_block','module' => 'listings','listing' => $this->_vars['listing'],'template' => 'icon'), $this);?></div>
			<div class="act-icon pdf-view"><a href="<?php echo tpl_function_seolink(array('method' => 'view','module' => 'listings','data' => $this->_vars['listing'],'pdf' => 'yes'), $this);?>" id="pdf_btn" class="btn-link link-r-margin" title="<?php echo l('link_pdf', 'listings', '', 'button', array()); ?>"><ins class="with-icon i-pdf"></ins></a></div>
			<div class="act-icon print-view"><a href="javascript:void(0);" id="print_btn" class="btn-link link-r-margin" title="<?php echo l('link_print', 'listings', '', 'button', array()); ?>" onclick="javascript: window.print(); return false;"><ins class="with-icon i-print"></ins></a></div>
			<div class="act-icon share-listing"><a href="<?php echo $this->_vars['site_url']; ?>
listings/share/<?php echo $this->_vars['listing']['id']; ?>
" id="share_btn" data-id="<?php echo $this->_vars['listing']['id']; ?>
" class="btn-link link-r-margin" title="<?php echo l('link_share', 'listings', '', 'button', array()); ?>"><ins class="with-icon i-share"></ins></a></div>
			<div class="act-icon send-review"><?php echo tpl_function_block(array('name' => 'send_review_block','module' => 'reviews','object_id' => $this->_vars['listing']['id'],'type_gid' => 'listings_object','responder_id' => $this->_vars['listing']['id_user'],'success' => $this->_vars['review_callback'],'is_owner' => $this->_vars['is_listing_owner']), $this);?></div>
			<div class="act-icon report-spam"><?php echo tpl_function_block(array('name' => 'mark_as_spam_block','module' => 'spam','object_id' => $this->_vars['listing']['id'],'type_gid' => 'listings_object','is_owner' => $this->_vars['is_listing_owner']), $this);?></div>
		
		</div>
	<div class="clr"></div>
	
	<?php if ($this->_run_modifier($this->_vars['section_gid'], 'array_key_exists', 'PHP', 1, $this->_vars['display_sections']['top'])): ?>
		<?php $this->assign('top_section_gid', $this->_vars['section_gid']); ?>
	<?php else: ?>
		<?php $this->assign('top_section_gid', 'gallery'); ?>
	<?php endif; ?>

	<?php if ($this->_run_modifier($this->_vars['section_gid'], 'array_key_exists', 'PHP', 1, $this->_vars['display_sections']['bottom'])): ?>
		<?php $this->assign('bottom_section_gid', $this->_vars['section_gid']); ?>
	<?php else: ?>
		<?php $this->assign('bottom_section_gid', 'overview'); ?>
	<?php endif; ?>
		
	<?php echo tpl_function_js(array('module' => listings,'file' => 'listings-menu.js'), $this);?>
	<?php echo tpl_function_js(array('file' => 'jquery.waitforimages.min.js'), $this);?>
	<?php echo tpl_function_js(array('file' => 'listings-email-alert.js','module' => 'listings'), $this);?>
	<?php echo tpl_function_js(array('file' => 'listings-share.js','module' => 'listings'), $this);?>
	<?php echo tpl_function_js(array('file' => 'listings-print.js','module' => 'listings'), $this);?>
	<script><?php echo '
		var tMenu;
		var bMenu;
		$(function(){
			tMenu = new listingsMenu({
				siteUrl: \'';  echo $this->_vars['site_url'];  echo '\',
				idListing: \'';  echo $this->_vars['listing']['id'];  echo '\',
				listBlockId: \'top_listing_block\',
				sectionId: \'top_listing_sections\',
				currentSection: \''; ?>
m_<?php echo $this->_vars['top_section_gid'];  echo '\',
			});
			bMenu = new listingsMenu({
				siteUrl: \'';  echo $this->_vars['site_url'];  echo '\',
				idListing: \'';  echo $this->_vars['listing']['id'];  echo '\',
				currentSection: \''; ?>
m_<?php echo $this->_vars['bottom_section_gid'];  echo '\',
			});
			new listingsEmailAlert({
				siteUrl: \'';  echo $this->_vars['site_url'];  echo '\',
				idListing: \'';  echo $this->_vars['listing']['id'];  echo '\',
			});
			new listingsShare({
				siteUrl: \'';  echo $this->_vars['site_url'];  echo '\',
				listingId: \'';  echo $this->_vars['listing']['id'];  echo '\',
			});
			
			new listingsPrint({
				siteUrl: \'';  echo $this->_vars['site_url'];  echo '\',
				idListing: \'';  echo $this->_vars['listing']['id'];  echo '\',
			});
		});
	'; ?>
</script>
	
	<div class="edit_block noPrint">
		<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->module_path. "listings". $this->module_templates.  $this->get_current_theme_gid('"default"', '"listings"'). "view_listing_menu_top.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
		<div id="top_listing_block">
			<div id="content_m_gallery" class="view-section <?php if ($this->_vars['top_section_gid'] != 'gallery'): ?>hide<?php endif; ?> noPrint"><?php echo $this->_vars['listing_content']['gallery']; ?>
</div>
			<div id="content_m_map" class="view-section<?php if ($this->_vars['top_section_gid'] != 'map'): ?> hide<?php endif; ?> noPrint"><?php echo $this->_vars['listing_content']['map']; ?>
</div>
			<div id="content_m_panorama" class="view-section<?php if ($this->_vars['top_section_gid'] != 'panorama'): ?> hide<?php endif; ?> noPrint"><?php echo $this->_vars['listing_content']['panorama']; ?>
</div>
			<div id="content_m_virtual_tour" class="view-section<?php if ($this->_vars['top_section_gid'] != 'virtual_tour'): ?> hide<?php endif; ?> noPrint"><?php echo $this->_vars['listing_content']['virtual_tour']; ?>
</div>
			<div id="content_m_video" class="view-section<?php if ($this->_vars['top_section_gid'] != 'video'): ?> hide<?php endif; ?> noPrint"><?php echo $this->_vars['listing_content']['video']; ?>
</div>
		</div>
	</div>
	<div class="xview">
		<?php /* <h2><?php echo tpl_function_block(array('name' => listing_price_block,'module' => 'listings','data' => $this->_vars['listing']), $this);?></h2>
		<div class="t-1">
		<?php echo $this->_vars['listing']['property_type_str']; ?>
 <?php echo $this->_vars['listing']['operation_type_str']; ?>
<br>
		<?php if ($this->_vars['listing']['sold']): ?><span class="status_text"><?php echo l('text_sold', 'listings', '', 'text', array()); ?></span><br><?php endif; ?>
		<?php if ($this->_vars['listing']['is_lift_up'] || $this->_vars['listing']['is_lift_up_country'] || $this->_vars['listing']['is_lift_up_region'] || $this->_vars['listing']['is_lift_up_city']): ?>
			<span class="status_text"><?php echo l('status_lift_up', 'listings', '', 'text', array()); ?></span><br>
		<?php elseif ($this->_vars['listing']['is_featured']): ?>
			<span class="status_text"><?php echo l('status_featured', 'listings', '', 'text', array()); ?></span><br>
		<?php endif; ?>
		</div> */ ?>
		
		<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'date_modified')); tpl_block_capture(array('assign' => 'date_modified'), null, $this); ob_start();  $this->_tag_stack[] = array('tpl_block_strip', array()); tpl_block_strip(array(), null, $this); ob_start(); ?>
			<?php echo $this->_run_modifier($this->_vars['listing']['date_modified'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']); ?>

		<?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_strip($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack);  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?> 
		
		<div class="listing-update-date">
			<?php /* <h2><?php echo l('field_id', 'listings', '', 'text', array()); ?> <?php echo $this->_vars['listing']['id']; ?>
</h2> */ ?>
			<div class="noPrint">	
				<?php /* Disable the original code -
					<span><?php if ($this->_run_modifier($this->_vars['listing']['date_modified'], 'strtotime', 'PHP', 1) - $this->_run_modifier($this->_vars['listing']['date_created'], 'strtotime', 'PHP', 1) > 120):  echo l('field_date_modified', 'listings', '', 'text', array());  else:  echo l('field_date_created', 'listings', '', 'text', array());  endif; ?>:</span> <?php if ($this->_vars['date_modified']):  echo $this->_vars['date_modified'];  else:  echo l('inactive_listing', 'listings', '', 'text', array());  endif; ?><br> 
				*/?>
				
				<?php /* The code below to display the Modified date for the listing */ ?>
				<?php /*<span><?php echo l('field_date_modified', 'listings', '', 'text', array()); ?>: <?php if ($this->_vars['date_modified']):  echo $this->_vars['date_modified'];  else:  echo l('inactive_listing', 'listings', '', 'text', array());  endif; ?></span><br> */ ?>
				<?php /* <span><?php echo l('field_views', 'listings', '', 'text', array()); ?>:</span> <?php echo $this->_vars['listing']['views']; ?>
<br> */ ?>
			</div>
		</div>
	</div>
	<div class="edit_block">
		<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->module_path. "listings". $this->module_templates.  $this->get_current_theme_gid('"default"', '"listings"'). "view_listing_menu_bottom.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
		<div id="listing_block">
			<div id="content_m_overview" class="view-section<?php if ($this->_vars['bottom_section_gid'] != 'overview'): ?> hide<?php endif; ?> print_block"><?php echo $this->_vars['listing_content']['overview']; ?>
</div>
			<div id="content_m_reviews" class="view-section<?php if ($this->_vars['bottom_section_gid'] != 'reviews'): ?> hide<?php endif; ?> noPrint"><?php echo $this->_vars['listing_content']['reviews']; ?>
</div>			
			<div id="content_m_file" class="view-section<?php if ($this->_vars['bottom_section_gid'] != 'file'): ?> hide<?php endif; ?> noPrint"><?php echo $this->_vars['listing_content']['file']; ?>
</div>
			<div id="content_m_calendar" class="view-section<?php if ($this->_vars['bottom_section_gid'] != 'calendar'): ?> hide<?php endif; ?>"><?php echo $this->_vars['listing_content']['calendar']; ?>
</div>
			<div id="content_m_kpr" class="view-section<?php if ($this->_vars['bottom_section_gid'] != 'kpr'): ?> hide<?php endif; ?>"><?php echo $this->_vars['listing_content']['kpr']; ?>
</div>
		</div>
		<div class="clr"></div>
		<div id="content_m_print" class="hide print_block"><?php echo $this->_vars['listing_content']['print']; ?>
</div>
	</div>
	<?php echo tpl_function_helper(array('func_name' => 'similar_listings_block','module' => 'listings','func_param' => $this->_vars['listing']), $this);?>	
</div>
<div class="rc-2 right-panel">
	<?php echo tpl_function_block(array('name' => 'listings_booking_block','module' => 'listings','listing' => $this->_vars['listing'],'no_save' => 1), $this);?>
	<?php $this->assign('show_all_listings', 1); ?>
	<?php echo tpl_function_block(array('name' => "user_info",'module' => "users",'user' => $this->_vars['listing']['user'],'all_listing' => true), $this);?>
	<?php echo tpl_function_helper(array('func_name' => 'show_banner_place','module' => 'banners','func_param' => 'right-banner'), $this);?>
	<?php echo tpl_function_helper(array('func_name' => 'show_mortgage_calc','module' => 'listings'), $this);?>
</div>

<div class="clr"></div>

</div>
</div>
</div>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
