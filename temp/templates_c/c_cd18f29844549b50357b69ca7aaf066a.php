<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.truncate.php'); $this->register_modifier("truncate", "tpl_modifier_truncate");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.escape.php'); $this->register_modifier("escape", "tpl_modifier_escape");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.seolink.php'); $this->register_function("seolink", "tpl_function_seolink");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.block.php'); $this->register_function("block", "tpl_function_block");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/block.strip.php'); $this->register_block("strip", "tpl_block_strip");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/block.capture.php'); $this->register_block("capture", "tpl_block_capture");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-04-13 13:23:34 KRAT */ ?>

<div class="content-block load_content">
	<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'facebook_share')); tpl_block_capture(array('assign' => 'facebook_share'), null, $this); ob_start();  $this->_tag_stack[] = array('tpl_block_strip', array()); tpl_block_strip(array(), null, $this); ob_start(); ?>
		<?php echo tpl_function_block(array('name' => show_social_networks_share,'module' => social_networking), $this);?>
	<?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_strip($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack);  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
	<h1><?php echo l('header_share', 'listings', '', 'text', array()); ?></h1>
	<div class="inside edit_block">
		<div class="tabs tab-size-15 noPrint">
			<ul id="share_sections">
				<li id="m_send_email" sgid="send_email" class="active"><a href="#"><?php echo l('link_send_mail_friend', 'listings', '', 'text', array()); ?></a></li>
				<?php if ($this->_vars['facebook_share']): ?><li id="m_send_facebook" sgid="send_facebook"><a href="#"><?php echo l('link_share_facebook', 'listings', '', 'text', array()); ?></a></li><?php endif; ?>
				<li id="m_send_twitter" sgid="send_twitter"><a href="#"><?php echo l('link_share_twitter', 'listings', '', 'text', array()); ?></a></li>
			</ul>
		</div>
		<div class="clr"></div>
		<div id="share_block" class="view edit-form">
			<div id="content_m_send_email" class="view-section">
				
				<form action="" method="post" id="share_form">
					<div class="popup">
					<div class="r">
						<div class="h"><?php echo l('send_mail_email', 'listings', '', 'text', array()); ?>:&nbsp;*</div>
						<div class="v"><input type="text" id="email" name="email"></div>
					</div>
					<div class="r">
						<div class="h"><?php echo l('send_mail_user', 'listings', '', 'text', array()); ?>:&nbsp;*</div>
						<div class="v"><input type="text" id="user" name="user"></div>
					</div>
					<div class="r">
						<div class="h"><?php echo l('send_mail_text', 'listings', '', 'text', array()); ?>:&nbsp;*</div>
						<div class="v"><textarea id="message" name="message" rows="3"></textarea></div>
					</div>
					</div>
					<div class="r">
						<input type="submit" value="<?php echo l('btn_send', 'start', '', 'button', array()); ?>">
					</div>
				</form>
	
			</div>
			<?php if ($this->_vars['facebook_share']): ?>
			<div id="content_m_send_facebook" class="hide view-section">	
				<div class="popup">
				<div class="listing">
					<h3><a href="<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'view','data' => $this->_vars['data']), $this);?>" title="<?php echo $this->_run_modifier($this->_vars['data']['output_name'], 'escape', 'plugin', 1); ?>
"><?php echo $this->_run_modifier($this->_vars['data']['output_name'], 'truncate', 'plugin', 1, 50); ?>
</a></h3>
					<div class="image">
						<a href="<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'view','data' => $this->_vars['data']), $this);?>">
							<img src="<?php echo $this->_vars['data']['media']['photo']['thumbs']['middle']; ?>
" title="<?php echo $this->_run_modifier($this->_vars['data']['output_name'], 'escape', 'plugin', 1); ?>
">
							<?php if ($this->_vars['data']['photo_count'] || $this->_vars['data']['is_vtour']): ?>
							<div class="photo-info">
								<div class="panel">
									<?php if ($this->_vars['data']['photo_count']): ?><span class="btn-link"><ins class="with-icon-small w i-photo"></ins> <?php echo $this->_vars['data']['photo_count']; ?>
</span><?php endif; ?>
									<?php if ($this->_vars['data']['is_vtour']): ?><span class="btn-link"><ins class="with-icon-small w i-vtour"></ins> 360&deg;</span><?php endif; ?>
								</div>
								<div class="background"></div>
							</div>
							<?php endif; ?>
						</a>
					</div>
					
					<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'status_info')); tpl_block_capture(array('assign' => 'status_info'), null, $this); ob_start();  $this->_tag_stack[] = array('tpl_block_strip', array()); tpl_block_strip(array(), null, $this); ob_start(); ?>
					<?php if ($this->_vars['data']['is_lift_up'] || $this->_vars['data']['is_lift_up_country'] || $this->_vars['data']['is_lift_up_region'] || $this->_vars['data']['is_lift_up_city']): ?>
						<?php echo l('status_lift_up', 'listings', '', 'text', array()); ?>
					<?php elseif ($this->_vars['data']['is_featured']): ?>
						<?php echo l('status_featured', 'listings', '', 'text', array()); ?>
					<?php endif; ?>
					<?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_strip($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack);  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
			
					<div class="body">
						<h3><?php echo tpl_function_block(array('name' => listing_price_block,'module' => 'listings','data' => $this->_vars['data']), $this);?></h3>
						<div class="t-1">
							<?php echo $this->_vars['data']['property_type_str']; ?>
 <?php echo $this->_vars['data']['operation_type_str']; ?>

							<br><?php echo $this->_run_modifier($this->_vars['data']['square_output'], 'truncate', 'plugin', 1, 30); ?>

							<?php if ($this->_run_modifier($this->_vars['data']['date_open'], 'strtotime', 'PHP', 1) >= $this->_vars['tstamp']): ?><br><?php echo l('text_date_open_status', 'listings', '', 'text', array());  endif; ?>
							<?php if ($this->_vars['data']['sold']): ?><br><span class="status_text"><?php echo l('text_sold', 'listings', '', 'text', array()); ?></span><?php endif; ?>
							<?php if ($this->_vars['status_info']): ?><br><span class="status_text"><?php echo $this->_vars['status_info']; ?>
</span><?php endif; ?>
						</div>
						<div class="t-2">
						</div>
						<?php if ($this->_vars['data']['user']['status']): ?>
						<div class="t-3">
							<?php echo $this->_run_modifier($this->_vars['data']['user']['output_name'], 'truncate', 'plugin', 1, 50); ?>

						</div>
						<div class="t-4">
							<a href="<?php echo tpl_function_seolink(array('module' => 'users','method' => 'view','data' => $this->_vars['data']['user']), $this);?>"><img src="<?php echo $this->_vars['data']['user']['media']['user_logo']['thumbs']['small']; ?>
" title="<?php echo $this->_run_modifier($this->_vars['data']['user']['output_name'], 'truncate', 'plugin', 1); ?>
"></a>
						</div>
						<?php endif; ?>
					</div>
					<div class="clr"></div>
				</div>
				<?php if ($this->_vars['data']['headline']): ?><p class="headline" title="<?php echo $this->_vars['data']['headline']; ?>
"><?php echo $this->_run_modifier($this->_vars['data']['headline'], 'truncate', 'plugin', 1, 50); ?>
</p><?php endif; ?>
				</div>
				<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'description')); tpl_block_capture(array('assign' => 'description'), null, $this); ob_start();  $this->_tag_stack[] = array('tpl_block_strip', array()); tpl_block_strip(array(), null, $this); ob_start(); ?>
					<?php echo tpl_function_block(array('name' => 'currency_format_output','module' => 'start','value' => $this->_vars['data']['price'],'cur_gid' => $this->_vars['data']['gid_currency']), $this);?>
				<?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_strip($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack);  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
				<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'facebook_ui')); tpl_block_capture(array('assign' => 'facebook_ui'), null, $this); ob_start();  $this->_tag_stack[] = array('tpl_block_strip', array()); tpl_block_strip(array(), null, $this); ob_start();  echo '
					FB.ui({
						method: \'feed\',
						redirect_uri: \'';  echo tpl_function_seolink(array('module' => 'listings','method' => 'view','data' => $this->_vars['data']), $this); echo '\',
						link: \'https://developers.facebook.com/docs/reference/dialogs/\',
						picture: \'';  echo $this->_vars['data']['media']['photo']['thumbs']['small'];  echo '\',
						name: \'';  echo $this->_run_modifier($this->_vars['data']['output_name'], 'escape', 'plugin', 1, 'quotes');  echo '\',
						caption: \'';  echo $this->_run_modifier($this->_vars['data']['property_type_str'], 'escape', 'plugin', 1, 'quotes'); ?>
 <?php echo $this->_run_modifier($this->_vars['data']['operation_type_str'], 'escape', 'plugin', 1, 'quotes');  echo '\',
						description: \'';  echo $this->_run_modifier($this->_run_modifier($this->_vars['description'], 'strip_tags', 'PHP', 1), 'escape', 'plugin', 1, 'quotes');  echo '\'
					}, function(response){})
				';  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_strip($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack);  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>	
				<input type="button" onclick="javascript:<?php echo $this->_vars['facebook_ui']; ?>
;" value="<?php echo l('btn_share', 'listings', '', 'button', array()); ?>">
			</div>
			<?php endif; ?>
			<div id="content_m_send_twitter" class="hide view-section">
				<div class="popup">
				<div class="r"><?php echo tpl_function_seolink(array('module' => 'listings','method' => 'view','data' => $this->_vars['data']), $this);?></div>
				</div>
				<div class="r"><input type="button" onclick="javascript:window.open('http://twitter.com/home?status=<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'view','data' => $this->_vars['data']), $this);?>');" value="<?php echo l('btn_share', 'listings', '', 'button', array()); ?>" class="btn_small"></div>
			</div>
		</div>
	</div>
</div>
<script><?php echo '
	var pMenu;
	$(function(){
		pMenu = new listingsMenu({
			siteUrl: \'';  echo $this->_vars['site_url'];  echo '\',
			idListing: \'';  echo $this->_vars['data']['id'];  echo '\',
			listBlockId: \'share_block\',
			sectionId: \'share_sections\',
			currentSection: \'m_send_email\',
		});
	});
'; ?>
</script>	
