<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-02 15:54:48 KRAT */ ?>

<div id="pages_block_1">
	<div class="pages">
		<div class="inside">		
			<ins class="prev<?php if ($this->_vars['prev_user']['id'] == $this->_vars['user']['id']): ?> gray<?php endif; ?>"><a href="<?php echo tpl_function_seolink(array('module' => 'users','method' => 'view','data' => $this->_vars['prev_user']), $this);?>">&nbsp;</a></ins>
			<ins class="current"><?php echo $this->_vars['current_page']; ?>
 <?php echo l('text_of', 'start', '', 'text', array()); ?> <?php echo $this->_vars['total_pages']; ?>
</ins>
			<ins class="next<?php if ($this->_vars['next_user']['id'] == $this->_vars['user']['id']): ?> gray<?php endif; ?>"><a href="<?php echo tpl_function_seolink(array('module' => 'users','method' => 'view','data' => $this->_vars['next_user']), $this);?>">&nbsp;</a></ins>
		</div>
	</div>
	<div class="clr"></div>
</div>
