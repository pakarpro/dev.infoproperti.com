<?php require_once('/opt/ip/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-02-29 11:13:26 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<form method="post" action="<?php echo $this->_vars['data']['action']; ?>
" name="save_form" enctype="multipart/form-data">
	<div class="edit-form n150">
		<div class="row header"><?php if ($this->_vars['data']['id']):  echo l('admin_header_cronjob_change', 'cronjob', '', 'text', array());  else:  echo l('admin_header_cronjob_add', 'cronjob', '', 'text', array());  endif; ?></div>
		<div class="row">
			<div class="h"><?php echo l('field_name', 'cronjob', '', 'text', array()); ?>:&nbsp;* </div>
			<div class="v"><input type="text" value="<?php echo $this->_vars['data']['name']; ?>
" name="name"></div>
		</div>
		<div class="row zebra">
			<div class="h"><?php echo l('field_cron_module', 'cronjob', '', 'text', array()); ?>:&nbsp;* </div>
			<div class="v"><input type="text" value="<?php echo $this->_vars['data']['module']; ?>
" name="module"></div>
		</div>
		<div class="row">
			<div class="h"><?php echo l('field_cron_model', 'cronjob', '', 'text', array()); ?>:&nbsp;* </div>
			<div class="v"><input type="text" value="<?php echo $this->_vars['data']['model']; ?>
" name="model"></div>
		</div>
		<div class="row zebra">
			<div class="h"><?php echo l('field_cron_method', 'cronjob', '', 'text', array()); ?>:&nbsp;* </div>
			<div class="v"><input type="text" value="<?php echo $this->_vars['data']['method']; ?>
" name="method"></div>
		</div>

		<div  class="row zebra"><i><?php echo l('crontab_comment', 'cronjob', '', 'text', array()); ?></i></div>

		<div class="row">
			<div class="h"><?php echo l('field_cron_tab_min', 'cronjob', '', 'text', array()); ?>: </div>
			<div class="v"><input type="text" value="<?php echo $this->_vars['data']['ct_min']; ?>
" name="ct_min" class="short"> <i><?php echo l('field_cron_tab_min_text', 'cronjob', '', 'text', array()); ?></i></div>
		</div>
		<div class="row zebra">
			<div class="h"><?php echo l('field_cron_tab_hour', 'cronjob', '', 'text', array()); ?>: </div>
			<div class="v"><input type="text" value="<?php echo $this->_vars['data']['ct_hour']; ?>
" name="ct_hour" class="short"> <i><?php echo l('field_cron_tab_hour_text', 'cronjob', '', 'text', array()); ?></i></div>
		</div>
		<div class="row">
			<div class="h"><?php echo l('field_cron_tab_day', 'cronjob', '', 'text', array()); ?>: </div>
			<div class="v"><input type="text" value="<?php echo $this->_vars['data']['ct_day']; ?>
" name="ct_day" class="short"> <i><?php echo l('field_cron_tab_day_text', 'cronjob', '', 'text', array()); ?></i></div>
		</div>
		<div class="row zebra">
			<div class="h"><?php echo l('field_cron_tab_month', 'cronjob', '', 'text', array()); ?>: </div>
			<div class="v"><input type="text" value="<?php echo $this->_vars['data']['ct_month']; ?>
" name="ct_month" class="short"> <i><?php echo l('field_cron_tab_month_text', 'cronjob', '', 'text', array()); ?></i></div>
		</div>
		<div class="row">
			<div class="h"><?php echo l('field_cron_tab_wday', 'cronjob', '', 'text', array()); ?>: </div>
			<div class="v"><input type="text" value="<?php echo $this->_vars['data']['ct_wday']; ?>
" name="ct_wday" class="short"> <i><?php echo l('field_cron_tab_wday_text', 'cronjob', '', 'text', array()); ?></i></div>
		</div>

	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div></div>
	<a class="cancel" href="<?php echo $this->_vars['site_url']; ?>
admin/cronjob"><?php echo l('btn_cancel', 'start', '', 'text', array()); ?></a>
</form>
<div class="clr"></div>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
