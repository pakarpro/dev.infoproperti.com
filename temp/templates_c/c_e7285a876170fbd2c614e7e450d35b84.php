<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2014-06-24 07:37:07 KRAT */ ?>

<?php if ($this->_vars['wish_lists']): ?><h2><?php echo l('header_wish_list', 'listings', '', 'text', array()); ?></h2><div class="wish_lists_block">	<?php if (is_array($this->_vars['wish_lists']) and count((array)$this->_vars['wish_lists'])): foreach ((array)$this->_vars['wish_lists'] as $this->_vars['key'] => $this->_vars['item']): ?>	<div class="wish_list<?php if (!($this->_vars['key'] % 8)): ?> first<?php endif; ?>">		<div class="image">			<a href="<?php echo $this->_vars['site_url']; ?>
listings/wish_list/<?php echo $this->_vars['item']['id']; ?>
">				<img src="<?php echo $this->_vars['item']['media']['photo']['thumbs']['big']; ?>
" alt="<?php echo $this->_run_modifier($this->_run_modifier($this->_vars['item']['output_name'], 'truncate', 'plugin', 1, 30), 'escape', 'plugin', 1); ?>
" title="<?php echo $this->_run_modifier($this->_vars['item']['output_name'], 'escape', 'plugin', 1); ?>
" />				<div class="listings_count"><span class="panel"><?php echo $this->_vars['item']['listings_count']; ?>
 <?php echo l('text_listings', 'listings', '', 'text', array()); ?></span><div class="background"></div></div>			</a>		</div>		<p class="location"><a href="<?php echo $this->_vars['site_url']; ?>
listings/wish_list/<?php echo $this->_vars['item']['id']; ?>
"><?php echo $this->_run_modifier($this->_vars['item']['output_name'], 'truncate', 'plugin', 1, 30); ?>
</a></p>	</div>	<?php endforeach; endif; ?></div><?php endif; ?>