<?php require_once('/opt/ip/system/libraries/template_lite/plugins/function.block.php'); $this->register_function("block", "tpl_function_block");  require_once('/opt/ip/system/libraries/template_lite/plugins/modifier.escape.php'); $this->register_modifier("escape", "tpl_modifier_escape");  require_once('/opt/ip/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-01-04 19:04:48 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array('load_type' => 'ui'));
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

<div class="actions">
	<ul>
		<li><div class="l"><a href="#" onclick="javascript: mlSorter.update_sorting(); return false"><?php echo l('link_save_block_sorting', 'dynamic_blocks', '', 'text', array()); ?></a></div></li>
	</ul>
	&nbsp;
</div>

<div class="menu-level3">
	<ul>
		<li class="active"><a href="<?php echo $this->_vars['site_url']; ?>
admin/dynamic_blocks/area_blocks/<?php echo $this->_vars['area']['id']; ?>
"><?php echo l('filter_area_blocks', 'dynamic_blocks', '', 'text', array()); ?></a></li>
		<li><a href="<?php echo $this->_vars['site_url']; ?>
admin/dynamic_blocks/area_layout/<?php echo $this->_vars['area']['id']; ?>
"><?php echo l('filter_area_layout', 'dynamic_blocks', '', 'text', array()); ?></a></li>
	</ul>
	&nbsp;
</div>

<div class="filter-form">
	<form action="" method="post">
	<h3><?php echo l('add_area_block_header', 'dynamic_blocks', '', 'text', array()); ?>:</h3>
	<select name="id_block">
	<option value="0">...</option>
	<?php if (is_array($this->_vars['blocks']) and count((array)$this->_vars['blocks'])): foreach ((array)$this->_vars['blocks'] as $this->_vars['block_id'] => $this->_vars['item']): ?>
	<option value="<?php echo $this->_vars['block_id']; ?>
"><?php echo l($this->_vars['item']['name_i'], $this->_vars['item']['lang_gid'], '', 'text', array()); ?></option>
	<?php endforeach; endif; ?>
	</select>
	<input type="submit" name="add_block" value="<?php echo l('link_add_block', 'dynamic_blocks', '', 'button', array()); ?>">
	</form>
</div>

<div id="area_blocks">
<ul name="parent_0" class="sort connected" id="clsr0ul">
<?php if (is_array($this->_vars['area_blocks']) and count((array)$this->_vars['area_blocks'])): foreach ((array)$this->_vars['area_blocks'] as $this->_vars['item']): ?>
<li id="item_<?php echo $this->_vars['item']['id']; ?>
">
	<div class="icons">
		<a href='#' onclick="if (confirm('<?php echo l('note_delete_area_block', 'dynamic_blocks', '', 'js', array()); ?>')) mlSorter.deleteItem(<?php echo $this->_vars['item']['id']; ?>
);return false;"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-delete.png" width="16" height="16" alt="<?php echo l('btn_delete', 'start', '', 'text', array()); ?>" title="<?php echo l('btn_delete', 'start', '', 'text', array()); ?>"></a>
	</div>

	<form id="form_item_<?php echo $this->_vars['item']['id']; ?>
">
	<h3><?php echo l($this->_vars['item']['block_data']['name_i'], $this->_vars['item']['block_data']['lang_gid'], '', 'text', array()); ?></h3>
	<div class="edit-form n150">
	<?php if (is_array($this->_vars['item']['block_data']['params_data']) and count((array)$this->_vars['item']['block_data']['params_data'])): foreach ((array)$this->_vars['item']['block_data']['params_data'] as $this->_vars['param']): ?>
		<?php $this->assign('param_gid', $this->_vars['param']['gid']); ?>
		<div class="row" id="row_<?php echo $this->_vars['item']['id']; ?>
_<?php echo $this->_vars['param']['gid']; ?>
">
			<div class="h"><?php echo l($this->_vars['param']['i'], $this->_vars['item']['block_data']['lang_gid'], '', 'text', array()); ?>: </div>
			<div class="v">
				<?php if ($this->_vars['param']['type'] == 'textarea'): ?>
				
				<textarea name="params[<?php echo $this->_vars['param_gid']; ?>
]" rows="5" cols="80"><?php echo $this->_run_modifier($this->_vars['item']['params'][$this->_vars['param_gid']], 'escape', 'plugin', 1); ?>
</textarea>
				
				<?php elseif ($this->_vars['param']['type'] == 'text'): ?>
				
				<?php if (is_array($this->_vars['langs']) and count((array)$this->_vars['langs'])): foreach ((array)$this->_vars['langs'] as $this->_vars['lang_id'] => $this->_vars['lang_item']): ?>
					<?php $this->assign('value_id', $this->_vars['param_gid'].'_'.$this->_vars['lang_id']); ?>
					<input type="<?php if ($this->_vars['lang_id'] == $this->_vars['current_lang_id']): ?>text<?php else: ?>hidden<?php endif; ?>" name="params[<?php echo $this->_vars['param_gid']; ?>
_<?php echo $this->_vars['lang_id']; ?>
]" value="<?php echo $this->_run_modifier($this->_run_modifier($this->_vars['item']['params'][$this->_vars['value_id']], 'strval', 'PHP', 1), 'escape', 'plugin', 1); ?>
" lang-editor="value" lang-editor-type="params-<?php echo $this->_vars['param_gid']; ?>
" lang-editor-lid="<?php echo $this->_vars['lang_id']; ?>
">
				<?php endforeach; endif; ?>
				<a href="#" lang-editor="button" lang-editor-type="params-<?php echo $this->_vars['param_gid']; ?>
"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-translate.png" width="16" height="16"></a>

				<?php elseif ($this->_vars['param']['type'] == 'int'): ?>
				
				<input type="text" value="<?php echo $this->_run_modifier($this->_run_modifier($this->_vars['item']['params'][$this->_vars['param_gid']], 'intval', 'PHP', 1), 'escape', 'plugin', 1); ?>
" name="params[<?php echo $this->_vars['param_gid']; ?>
]">
				
				<?php else: ?>

				<input type="text" value="<?php echo $this->_run_modifier($this->_vars['item']['params'][$this->_vars['param_gid']], 'escape', 'plugin', 1); ?>
" name="params[<?php echo $this->_vars['param_gid']; ?>
]">

				<?php endif; ?>
			</div>
		</div>
	<?php endforeach; endif; ?>
		<div class="row">
			<div class="h"><?php echo l('field_view', 'dynamic_blocks', '', 'text', array()); ?>: </div>
			<div class="v">
				<select name="view_str">
				<?php if (is_array($this->_vars['item']['block_data']['views_data']) and count((array)$this->_vars['item']['block_data']['views_data'])): foreach ((array)$this->_vars['item']['block_data']['views_data'] as $this->_vars['view']): ?>
				<?php $this->assign('view_gid', $this->_vars['view']['gid']); ?>
				<option value="<?php echo $this->_vars['view_gid']; ?>
" <?php if ($this->_vars['view_gid'] == $this->_vars['item']['view_str']): ?>selected<?php endif; ?>><?php echo l($this->_vars['view']['i'], $this->_vars['item']['block_data']['lang_gid'], '', 'text', array()); ?></option>
				<?php endforeach; endif; ?>
				</select>
			</div>
		</div>
		<div class="row">
			<div class="h"><?php echo l('field_cache_time', 'dynamic_blocks', '', 'text', array()); ?>: </div>
			<div class="v"><input type="text" value="<?php echo $this->_vars['item']['cache_time']; ?>
" name="cache_time" class="short"> <i><?php echo l('field_cache_time_text', 'dynamic_blocks', '', 'text', array()); ?></i></div>
		</div>
		<div class="row">
			<div class="h">&nbsp;</div>
			<div class="v"><input type="button" name="save_data" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>" onclick="javascript: saveBlock('<?php echo $this->_vars['item']['id']; ?>
');"></div>
		</div>
		<div class="clr"></div>
	</div>
	</form>
</li>
<?php endforeach; endif; ?>
</ul>
</div>
<?php echo tpl_function_block(array('name' => lang_inline_editor,'module' => start), $this);?>
<script >
	var save_url = '<?php echo $this->_vars['site_url']; ?>
admin/dynamic_blocks/ajax_save_area_block/';
<?php echo '
	var mlSorter;
	$(function(){
		mlSorter = new multilevelSorter({
			siteUrl: \'';  echo $this->_vars['site_url'];  echo '\', 
			urlSaveSort: \'admin/dynamic_blocks/save_area_block_sorter/';  echo $this->_vars['area']['id'];  echo '\',
			urlDeleteItem: \'admin/dynamic_blocks/ajax_delete_area_block/\',
		});
	});

	function saveBlock(id){
		$.ajax({
			url: save_url + id, 
			type: \'POST\',
			data: $(\'#form_item_\'+id).serialize(), 
			cache: false,
			success: function(data){
				error_object.show_error_block(\'';  echo l("success_update_area_block", "dynamic_blocks", '', "js", array());  echo '\', \'success\');
			}
		});
	}
'; ?>
</script><?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
