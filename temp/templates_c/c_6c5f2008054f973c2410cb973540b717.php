<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2014-06-12 14:06:42 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<form method="post" action="<?php echo $this->_vars['data']['action']; ?>
" name="save_form" enctype="multipart/form-data">
	<div class="edit-form n150">
		<div class="row header"><?php if ($this->_vars['data']['id']):  echo l('admin_header_place_change', 'banners', '', 'text', array());  else:  echo l('admin_header_place_add', 'banners', '', 'text', array());  endif; ?></div>
		<div class="row">
			<div class="h"><?php echo l('field_place_name', 'banners', '', 'text', array()); ?>:&nbsp;* </div>
			<div class="v"><input type="text" value="<?php echo $this->_vars['data']['name']; ?>
" name="name"></div>
		</div>
		<div class="row">
			<div class="h"><?php echo l('field_place_keyword', 'banners', '', 'text', array()); ?>:&nbsp;* </div>
			<div class="v"><input type="text" value="<?php echo $this->_vars['data']['keyword']; ?>
" name="keyword"></div>
		</div>
		<div class="row">
			<div class="h"><?php echo l('field_place_sizes', 'banners', '', 'text', array()); ?>:&nbsp;* </div>
			<div class="v"><input type="text" value="<?php echo $this->_vars['data']['width']; ?>
" name="width" class="short"> X <input type="text" value="<?php echo $this->_vars['data']['height']; ?>
" name="height" class="short"></div>
		</div>
		<div class="row">
			<div class="h"><?php echo l('field_place_rotate_time', 'banners', '', 'text', array()); ?>: <br></div>
			<div class="v"><input type="text" value="<?php echo $this->_vars['data']['rotate_time']; ?>
" name="rotate_time" class="short"> sec. <i>(0 - <?php echo l('no_rotation', 'banners', '', 'text', array()); ?>)</i></div>
		</div>
		<div class="row">
			<div class="h"><?php echo l('field_place_in_rotation', 'banners', '', 'text', array()); ?>: </div>
			<div class="v"><input type="text" value="<?php echo $this->_vars['data']['places_in_rotation']; ?>
" name="places_in_rotation" class="short"></div>
		</div>
		<div class="row">
			<div class="h"><?php echo l('field_access', 'banners', '', 'text', array()); ?>: </div>
			<div class="v">
				<select name="access">
				<option value="0">...</option>
				<?php if (is_array($this->_vars['place_access_lang']['option']) and count((array)$this->_vars['place_access_lang']['option'])): foreach ((array)$this->_vars['place_access_lang']['option'] as $this->_vars['key'] => $this->_vars['item']): ?><option value="<?php echo $this->_vars['key']; ?>
"<?php if ($this->_vars['key'] == $this->_vars['data']['access']): ?> selected<?php endif; ?>><?php echo $this->_vars['item']; ?>
</option><?php endforeach; endif; ?>
				</select>
			</div>
		</div>
		<div class="row">
			<div class="h"><?php echo l('field_groups', 'banners', '', 'text', array()); ?>: </div>
			<div class="v">
				<?php if (is_array($this->_vars['groups']) and count((array)$this->_vars['groups'])): foreach ((array)$this->_vars['groups'] as $this->_vars['key'] => $this->_vars['item']): ?>
					<div><input type="checkbox" name="place_groups[]" value="<?php echo $this->_vars['item']['id']; ?>
" id="pg_<?php echo $this->_vars['item']['id']; ?>
" <?php if ($this->_vars['item']['selected'] == '1'): ?>checked<?php endif; ?>> <label for="pg_<?php echo $this->_vars['item']['id']; ?>
"><?php echo $this->_vars['item']['name']; ?>
</label></div>
				<?php endforeach; endif; ?>
				&nbsp;
			</div>
		</div>
	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div></div>
	<a class="cancel" href="<?php echo $this->_vars['site_url']; ?>
admin/banners/places_list"><?php echo l('btn_cancel', 'start', '', 'text', array()); ?></a>
</form>
<script type="text/javascript"><?php echo '
	$(function(){
		$("div.row:odd").addClass("zebra");
	});
'; ?>
</script>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
