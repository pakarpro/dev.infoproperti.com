<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-02-29 15:34:14 KRAT */ ?>

<ul>
	<?php if (is_array($this->_vars['save_search_data']) and count((array)$this->_vars['save_search_data'])): foreach ((array)$this->_vars['save_search_data'] as $this->_vars['item']): ?>
	<li>
		<a href="<?php echo $this->_vars['site_url']; ?>
listings/load_saved_search/<?php echo $this->_vars['item']['id']; ?>
" title="<?php echo $this->_run_modifier($this->_vars['item']['name'], 'escape', 'plugin', 1); ?>
"><?php echo $this->_run_modifier($this->_vars['item']['name'], 'truncate', 'plugin', 1, 16, '...', true); ?>
</a>&nbsp;
		<a href="<?php echo $this->_vars['site_url']; ?>
listings/delete_saved_search/<?php echo $this->_vars['item']['id']; ?>
" class="btn-link small fright"><ins class="with-icon-small i-delete"></ins></a>
	</li>
	<?php endforeach; endif; ?>	
</ul>
<div class="clr"></div>
