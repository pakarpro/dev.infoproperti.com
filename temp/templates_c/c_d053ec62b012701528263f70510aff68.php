<?php require_once('/opt/ip/system/libraries/template_lite/plugins/function.ld_option.php'); $this->register_function("ld_option", "tpl_function_ld_option");  require_once('/opt/ip/system/libraries/template_lite/plugins/modifier.date_format.php'); $this->register_modifier("date_format", "tpl_modifier_date_format");  require_once('/opt/ip/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  require_once('/opt/ip/system/libraries/template_lite/plugins/block.strip.php'); $this->register_block("strip", "tpl_block_strip");  require_once('/opt/ip/system/libraries/template_lite/plugins/block.capture.php'); $this->register_block("capture", "tpl_block_capture");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.block.php'); $this->register_function("block", "tpl_function_block");  require_once('/opt/ip/system/libraries/template_lite/plugins/modifier.truncate.php'); $this->register_modifier("truncate", "tpl_modifier_truncate");  require_once('/opt/ip/system/libraries/template_lite/plugins/modifier.escape.php'); $this->register_modifier("escape", "tpl_modifier_escape");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.seolink.php'); $this->register_function("seolink", "tpl_function_seolink");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.pagination.php'); $this->register_function("pagination", "tpl_function_pagination");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.sorter.php'); $this->register_function("sorter", "tpl_function_sorter");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-02 15:56:06 KRAT */ ?>

<?php if ($this->_vars['listings']): ?>
	<!-- inner pg result in Search page-->
	<div class="sorter line" id="sorter_block">
		<?php echo tpl_function_sorter(array('links' => $this->_vars['sort_data']['links'],'order' => $this->_vars['sort_data']['order'],'direction' => $this->_vars['sort_data']['direction'],'url' => $this->_vars['sort_data']['url']), $this);?>
		<div class="fright" id="pages_block_1"><?php echo tpl_function_pagination(array('data' => $this->_vars['page_data'],'type' => 'cute'), $this);?></div>
	</div>
	
	<!-- MOD inner pg result for comparison -->
	<div id="comp-container" class="hidden clearfix">
		<button class="close-container" title="Tutup">x</button>
		<h3 class="msg-compare">There is 0 product selected</h3>
		<a class="compare-btn" href="#" target="_blank">Bandingkan</a>
		<!--<a class="compare-btn btn-close" href="#">Close</a>-->
	</div>
    <!-- End MOD : Inner pg result for comparison -->
<?php endif; ?>

<div>
    <?php if ($this->_vars['msg_word_1']): ?>        
        <span class="search-msg"><?php echo $this->_vars['msg_word_1']; ?>
</span>
    <?php endif; ?>
        
    <?php if (is_array($this->_vars['listings']) and count((array)$this->_vars['listings'])): foreach ((array)$this->_vars['listings'] as $this->_vars['item']): ?>
		<div class="listing-block <?php if ($this->_vars['item']['is_highlight']): ?>highlight<?php endif; ?>">
			<div id="item-block-<?php echo $this->_vars['item']['id']; ?>
" class="item listing <?php if ($this->_vars['item']['is_highlight']): ?>highlight<?php endif; ?>">
				<h3 class="listing-heading search-result-entry-heading">
                	<a href="<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'view','data' => $this->_vars['item']), $this);?>" title="<?php echo $this->_run_modifier($this->_vars['item']['output_name'], 'escape', 'plugin', 1); ?>
"><?php echo $this->_run_modifier($this->_vars['item']['output_name'], 'truncate', 'plugin', 1, 50); ?>
</a>
                <div style="position:relative; float:right; margin-top:-5px; display:block;">
                <?php if ($this->_vars['item']['review_sorter'] != 0): ?>                
                                    <?php echo tpl_function_block(array('name' => get_rate_block,'module' => reviews,'rating_data_main' => $this->_vars['item']['review_sorter'],'type_gid' => 'listings_object','template' => 'normal','read_only' => 'true'), $this);?>
				                <?php endif; ?>
                </div>                    
                </h3>
				<div class="image">
					<a href="<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'view','data' => $this->_vars['item']), $this);?>">
						<?php if ($this->_vars['animate_available'] && $this->_vars['item']['is_slide_show']): ?>
							<img src="<?php echo $this->_vars['item']['media']['photo']['thumbs']['middle_anim']; ?>
" title="<?php echo $this->_run_modifier($this->_vars['item']['output_name'], 'escape', 'plugin', 1); ?>
">
						<?php else: ?>
							<img src="<?php echo $this->_vars['item']['media']['photo']['thumbs']['middle']; ?>
" title="<?php echo $this->_run_modifier($this->_vars['item']['output_name'], 'escape', 'plugin', 1); ?>
">
						<?php endif; ?>
                        
						<?php if ($this->_vars['item']['photo_count'] || $this->_vars['item']['is_vtour']): ?>
							<div class="photo-info">
								<div class="panel">
									<?php if ($this->_vars['item']['photo_count']): ?><span class="btn-link"><ins class="with-icon-small w i-photo"></ins> <?php echo $this->_vars['item']['photo_count']; ?>
</span><?php endif; ?>
									<?php if ($this->_vars['item']['is_vtour']): ?><span class="btn-link"><ins class="with-icon-small w i-vtour"></ins> 360&deg;</span><?php endif; ?>
								</div>
								<div class="background"></div>
							</div>
						<?php endif; ?>
					</a>
				</div>
		
				<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'status_info')); tpl_block_capture(array('assign' => 'status_info'), null, $this); ob_start();  $this->_tag_stack[] = array('tpl_block_strip', array()); tpl_block_strip(array(), null, $this); ob_start(); ?>
					<?php if ($this->_vars['item']['is_lift_up'] || $this->_vars['location_filter'] == 'country' && $this->_vars['item']['is_lift_up_country'] || $this->_vars['location_filter'] == 'region' && ( $this->_vars['item']['is_lift_up_country'] || $this->_vars['item']['is_lift_up_region'] ) || $this->_vars['location_filter'] == 'city' && ( $this->_vars['item']['is_lift_up_country'] || $this->_vars['item']['is_lift_up_region'] || $this->_vars['item']['is_lift_up_city'] ) || $this->_vars['sort_data']['order'] != 'default' && ( $this->_vars['item']['is_lift_up'] || $this->_vars['item']['is_lift_up_country'] || $this->_vars['item']['is_lift_up_region'] || $this->_vars['item']['is_lift_up_city'] )): ?>
						<?php echo l('status_lift_up', 'listings', '', 'text', array()); ?>
					<?php elseif ($this->_vars['item']['is_featured']): ?>
						<?php echo l('status_featured', 'listings', '', 'text', array()); ?>
					<?php endif; ?>
				<?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_strip($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack);  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
				
				<div class="body clearfix">
					<div class="prop-info">
						<h3 class="price-highlight"><?php echo tpl_function_block(array('name' => listing_price_block,'module' => 'listings','data' => $this->_vars['item']), $this);?></h3>
						<div class="t-1">
							<?php echo $this->_vars['item']['property_type_str']; ?>
 <?php echo $this->_vars['item']['operation_type_str']; ?>

                            <br>
							<?php echo l('field_square', 'listings', '', 'text', array()); ?> : <?php echo $this->_run_modifier($this->_vars['item']['square_output'], 'truncate', 'plugin', 1); ?>

							<ul class="icon-stats">
							<?php if (is_array($this->_vars['residential']) and count((array)$this->_vars['residential'])): foreach ((array)$this->_vars['residential'] as $this->_vars['residen']): ?>
								<?php if ($this->_vars['item']['id'] == $this->_vars['residen']['id_listing']): ?>
									<?php if ($this->_vars['residen']['fe_bd_rooms_1'] != '0'): ?>	 
										<li class="listing-icon bedroom"> : <?php echo $this->_vars['residen']['fe_bd_rooms_1']; ?>
</li>
									<?php endif; ?>
									
									<?php if ($this->_vars['residen']['fe_bth_rooms_1'] != '0'): ?>
										<li class="listing-icon bathroom"> : <?php echo $this->_vars['residen']['fe_bth_rooms_1']; ?>
</li>
									<?php endif; ?>
									
									<?php if ($this->_vars['residen']['fe_garages_1'] != '0'): ?>
										<li class="listing-icon garages"> : <?php echo $this->_vars['residen']['fe_garages_1']; ?>
</li>
									<?php endif; ?>
								<?php endif; ?>
							<?php endforeach; endif; ?>
							
							<?php if (is_array($this->_vars['commercial']) and count((array)$this->_vars['commercial'])): foreach ((array)$this->_vars['commercial'] as $this->_vars['commer']): ?>
								<?php if ($this->_vars['item']['id'] == $this->_vars['commer']['id_listing']): ?>
									<?php if ($this->_vars['commer']['fe_year_2'] != '0'): ?>	 
										<li class="listing-icon year">Year : <?php echo $this->_vars['commer']['fe_year_2']; ?>
</li>
									<?php endif; ?>
									
									<?php if ($this->_vars['commer']['fe_foundation_2'] != '0'): ?>
										<li class="listing-icon foundation">Foundation : <?php echo $this->_vars['commer']['fe_foundation_2']; ?>
</li>
									<?php endif; ?>
								<?php endif; ?>
							<?php endforeach; endif; ?>
							</ul>
														
							<?php if ($this->_run_modifier($this->_vars['item']['date_open'], 'strtotime', 'PHP', 1) >= $this->_vars['tstamp']):  echo l('text_date_open_status', 'listings', '', 'text', array());  endif; ?>
                            <!-- Enable these lines below to show the open house's date and time
							<?php if ($this->_run_modifier($this->_vars['item']['date_open'], 'strtotime', 'PHP', 1) > 0): ?>
                            	<?php echo $this->_run_modifier($this->_vars['item']['date_open'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']); ?>
 
                                <?php if ($this->_vars['item']['date_open_begin']):  echo tpl_function_ld_option(array('i' => 'dayhour-names','gid' => 'start','option' => $this->_vars['item']['date_open_begin']), $this); endif; ?>
                                <?php if ($this->_vars['item']['date_open_end']):  if ($this->_vars['item']['date_open_begin']): ?> - <?php endif;  echo tpl_function_ld_option(array('i' => 'dayhour-names','gid' => 'start','option' => $this->_vars['item']['date_open_end']), $this); endif; ?>
                            <?php endif; ?>
							-->
							<?php if ($this->_vars['item']['sold']): ?><br><span class="status_text"><?php echo l('text_sold', 'listings', '', 'text', array()); ?></span><?php endif; ?>
							<span class="status_text"><?php echo $this->_vars['status_info']; ?>
</span>
						</div>
						<div class="t-2">
			
							<?php /* lines below for displaying modified listing by date */ ?>
							<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'date_modified')); tpl_block_capture(array('assign' => 'date_modified'), null, $this); ob_start();  $this->_tag_stack[] = array('tpl_block_strip', array()); tpl_block_strip(array(), null, $this); ob_start(); ?>
							<?php echo $this->_run_modifier($this->_vars['item']['date_modified'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']); ?>
 <?php /* replaced the $listing.date_modified with $item.date_modified */ ?>
							<?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_strip($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack);  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?> 
							
							<span><?php echo l('field_date_modified', 'listings', '', 'text', array()); ?>: <?php if ($this->_vars['date_modified']):  echo $this->_vars['date_modified'];  else:  echo l('inactive_listing', 'listings', '', 'text', array());  endif; ?></span><br />
							<span><?php echo l('field_views', 'listings', '', 'text', array()); ?>: <?php echo $this->_vars['item']['views']; ?>
</span>
                            
						</div>
					</div>
					<?php if ($this->_vars['show_user_info']): ?>
					<div class="user-info">
						<div class="t-3"></div>
						<div class="t-4">
							<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'user_logo')); tpl_block_capture(array('assign' => 'user_logo'), null, $this); ob_start(); ?>
								<?php /* #MOD# <?php if ($this->_vars['show_logo'] || $this->_vars['item']['user']['is_show_logo']): ?> */ ?>
								<img src="<?php echo $this->_vars['item']['user']['media']['user_logo']['thumbs']['small']; ?>
" title="<?php echo $this->_run_modifier($this->_vars['item']['user']['output_name'], 'escape', 'plugin', 1); ?>
">
								<?php /* #MOD# <?php else: ?>
								<img src="<?php echo $this->_vars['item']['user']['media']['default_logo']['thumbs']['small']; ?>
" title="<?php echo $this->_run_modifier($this->_vars['item']['user']['output_name'], 'escape', 'plugin', 1); ?>
">
								<?php endif; ?> */ ?>
								<?php echo $this->_run_modifier($this->_vars['item']['user']['output_name'], 'truncate', 'plugin', 1, 20); ?>

							<?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
							<?php if ($this->_vars['item']['user']['status']): ?>
							<a href="<?php echo tpl_function_seolink(array('module' => 'users','method' => 'view','data' => $this->_vars['item']['user']), $this);?>"><?php echo $this->_vars['user_logo']; ?>
</a>
							<?php else: ?>
							<?php echo $this->_vars['user_logo']; ?>

							<?php endif; ?>
						</div>
					</div>	
					<?php endif; ?>
				</div>
				<div class="clr"></div>
				<?php if ($this->_vars['item']['headline']): ?><p class="headline" title="<?php echo $this->_vars['item']['headline']; ?>
"><?php echo $this->_run_modifier($this->_vars['item']['headline'], 'truncate', 'plugin', 1, 100); ?>
</p>
					<?php else: ?><p class="headline">&nbsp;</p>
				<?php endif; ?>
				<div class="a-link clearfix">
					<?php echo tpl_function_block(array('name' => 'save_listing_block','module' => 'listings','listing' => $this->_vars['item'],'template' => 'link','separator' => ''), $this);?>
					<?php echo tpl_function_block(array('name' => 'listings_booking_block','module' => 'listings','listing' => $this->_vars['item'],'template' => 'link','show_link' => 1,'show_price' => 1,'separator' => '','no_save' => 1), $this);?>
					<a href="<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'view','data' => $this->_vars['item']), $this);?>"><?php echo l('link_details', 'listings', '', 'text', array()); ?></a>

					<?php /* compare / comparison feature */ ?>
					<div class="compare-feat">
						<span class="ListId"><?php echo $this->_run_modifier($this->_vars['item']['output_name'], 'escape', 'plugin', 1); ?>
</span>
						<a id="<?php echo $this->_vars['item']['id']; ?>
" href="#" class="compare">Bandingkan</a>
						<span class="prodId" hidden=""><?php echo $this->_vars['item']['id']; ?>
</span>
						<img class="ListImg hidden" src="<?php echo $this->_vars['item']['media']['photo']['thumbs']['middle']; ?>
" title="<?php echo $this->_run_modifier($this->_vars['item']['output_name'], 'escape', 'plugin', 1); ?>
">
					</div>

					<span class="prioritas">Prioritas</span>
				</div>
				<!--<div class="a-link clearfix">-->
                <?php if ($this->_vars['kpr_button'] && $this->_vars['item']['property_type_str'] != 'Lain-Lain'): ?>
                <div style="margin-top:5px; margin-bottom:-20px;">
                <!--
                <div style="margin-top:5px;">                
					<a href="kpr-bca-<?php echo $this->_vars['item']['property']; ?>
-id-<?php echo $this->_vars['item']['id']; ?>
" target="#"><?php echo l('kpr_bca', 'listings', '', 'text', array()); ?></a>	
					<a href="kpr-mandiri-<?php echo $this->_vars['item']['property']; ?>
-id-<?php echo $this->_vars['item']['id']; ?>
" target="#"><?php echo l('kpr_mandiri', 'listings', '', 'text', array()); ?></a>
                </div>                    
                -->
                <?php if (is_array($this->_vars['kpr_button']) and count((array)$this->_vars['kpr_button'])): foreach ((array)$this->_vars['kpr_button'] as $this->_vars['button']): ?>
                	<a href="kpr-<?php echo $this->_vars['button']['name']; ?>
-<?php echo $this->_vars['item']['property']; ?>
-id-<?php echo $this->_vars['item']['id']; ?>
" target="#"><img src="<?php echo $this->_vars['base_url']; ?>
uploads/kpr_button/<?php echo $this->_vars['button']['image']; ?>
" style=" width:167px; height:19px; padding:0px; border:none;" /></a>	
                <?php endforeach; endif; ?>
                </div>
                <?php endif; ?>
                
			</div>
	</div>
	<?php endforeach; else: ?>
		<div class="item empty"><?php echo l('no_listings', 'listings', '', 'text', array()); ?></div>
	<?php endif; ?>
	</div>
	<?php if ($this->_vars['listings']): ?><div id="pages_block_2"><?php echo tpl_function_pagination(array('data' => $this->_vars['page_data'],'type' => 'full'), $this);?></div><?php endif; ?>

	<?php if ($this->_vars['update_map']): ?>
	<!-- display google map  -->
	<?php echo tpl_function_block(array('name' => update_default_map,'module' => geomap,'markers' => $this->_vars['markers'],'map_id' => 'listings_map_container'), $this);?>
	<?php endif; ?>
	
	<?php if ($this->_vars['update_operation_type']): ?>
	<script><?php echo '
	$(function(){
		var operation_type = $(\'#operation_type\');
		if(operation_type.val() != \'';  echo $this->_vars['update_operation_type'];  echo '\')
			$(\'#operation_type\').val(\'';  echo $this->_vars['update_operation_type'];  echo '\').trigger(\'change\');
	});
	'; ?>
</script>
	<?php endif; ?>

	<script><?php echo '
	$(function(){
		';  if ($this->_vars['update_search_block']): ?>$('#search_filter_block').html('<?php $this->_tag_stack[] = array('tpl_block_strip', array()); tpl_block_strip(array(), null, $this); ob_start();  echo $this->_vars['search_filters_block'];  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_strip($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>');<?php endif;  echo '
		$(\'#total_rows\').html(\'';  echo $this->_vars['page_data']['total_rows'];  echo '\');
	});
	'; ?>
</script>

<script><?php echo '
// JS Compare
$(function(){
	function getSelectedIds() {
   return $(\'.box .list-id\').map(function(){ 
		return $(this).text(); }).toArray();
   }

	function updateLinkAndCounter() {
    var ids = getSelectedIds().map(function(x,i) { return [x].join(\'\'); });
    $(\'#comp-container > a\').attr(\'href\', \'';  echo $this->_vars['site_url'];  echo 'compare-listings-id-\' + ids.join(\'-\'));
    $("h3.msg-compare").text(ids.length == 1 ? \'Anda telah memilih 1 properti:\' : \'Anda telah memilih \' + ids.length + \' properti:\');
	if (ids.length == 0)
	{
		$("#comp-container").slideToggle("fast");
	}
	}

	// trigger
	$(".compare").click(function(e) {
	var prodName= $(this).prev(\'.ListId\').text();
    var id= $(this).next(\'.prodId\').text();
	var img= $(this).next(".prodId").next(".ListImg").attr("src");
		
    var selected = getSelectedIds();
	if(selected.length == 0)
	{
		$("#comp-container").slideToggle("fast"); //open slide the container
		e.preventDefault();
	}
    if(selected.length == 4) return false; // already 4 items added
    if(selected.indexOf(id) != -1) return false; // item already added
	
    $(\'<div/>\', { \'class\': \'box\' })
       .append($(\'<img/>\', { class: \'list-img\', src:img }))
	   .append($(\'<span/>\', { class: \'list-name\', text:prodName }))
       .append($(\'<a/>\', { href: \'#\', text: \'x\', \'class\':\'close\' }))
	   .append($(\'<span/>\', { class: \'list-id hidden\', text:id }))
       .appendTo(\'#comp-container\');

	e.preventDefault();
    updateLinkAndCounter();
    $("#comp-container").removeClass("hidden");
	});
	
	// remove from list
	$(".close").live("click", function(e) {
		$(this).parent().remove();
		e.preventDefault() //disable jumping to top
		updateLinkAndCounter();
	});
	
//	$(".btn-close").live("click", function(e) {
//		$("#comp-container").slideToggle("fast"); //close the container
//		e.preventDefault() //disable jumping to top
//		var minimize=1;
//	}); 

	$(".close-container").live("click", function(e) {
		$("#comp-container").slideToggle("fast"); //close the container
		$(".box").remove();
		$(".compare-btn").attr("href", "#");
//		e.preventDefault() //disable jumping to top
	}); 

}); 
'; ?>
</script>