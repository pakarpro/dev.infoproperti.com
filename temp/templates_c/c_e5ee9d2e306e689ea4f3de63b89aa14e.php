<?php require_once('/opt/ip/system/libraries/template_lite/plugins/function.block.php'); $this->register_function("block", "tpl_function_block");  require_once('/opt/ip/system/libraries/template_lite/plugins/modifier.escape.php'); $this->register_modifier("escape", "tpl_modifier_escape");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.js.php'); $this->register_function("js", "tpl_function_js");  require_once('/opt/ip/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-02-25 10:04:37 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

	<div class="content-block">		
		<div class="edit_block">
			<h1><?php echo l('header_listing_edit', 'listings', '', 'text', array()); ?></h1>

			<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->module_path. "listings". $this->module_templates.  $this->get_current_theme_gid('', '"listings"'). "my_listings_menu.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

			<?php echo tpl_function_js(array('module' => listings,'file' => 'listings-edit-steps.js'), $this);?>
			<script><?php echo '
				$(function(){
					new listingsEditSteps();
				});
			'; ?>
</script>
			<div class="rollup-box">
			
			<form method="post" action="<?php echo $this->_run_modifier($this->_vars['data']['action'], 'escape', 'plugin', 1); ?>
" name="save_form" id="save_form" enctype="multipart/form-data">
			<table class="list" id="services_list">
			<tr>
				<th class="w200"><?php echo l('service_name', 'listings', '', 'text', array()); ?></th>		
				<th class="w50"><?php echo l('service_price', 'listings', '', 'text', array()); ?>, <?php echo tpl_function_block(array('name' => currency_output,'module' => start,'cur_gid' => $this->_vars['base_currency']['gid']), $this);?></th>
				<th class="w150"><?php if ($this->_vars['one_param']):  echo $this->_vars['param_name'];  else:  echo l('service_params', 'listings', '', 'text', array());  endif; ?></th>
				<th class="w100">&nbsp;</th>		
			</tr>
			<?php if ($this->_vars['page_data']['use_services'] && $this->_vars['page_data']['listings_services']): ?>
			<?php if (is_array($this->_vars['page_data']['listings_services']) and count((array)$this->_vars['page_data']['listings_services'])): foreach ((array)$this->_vars['page_data']['listings_services'] as $this->_vars['item']): ?>
			<tr>
				<td><?php echo $this->_vars['item']['name']; ?>
</td>
				<td><?php echo tpl_function_block(array('name' => 'currency_format_output','module' => 'start','value' => $this->_vars['item']['price'],'cur_gid' => $this->_vars['base_currency']['gid'],'disable_abbr' => 1), $this);?></td>
				<td>
					<?php if ($this->_vars['one_param']): ?>
						<?php echo $this->_vars['item']['data_admin'][$this->_vars['param_gid']]; ?>
<br>
					<?php else: ?>
						<?php if (is_array($this->_vars['item']['data_admin']) and count((array)$this->_vars['item']['data_admin'])): foreach ((array)$this->_vars['item']['data_admin'] as $this->_vars['key2'] => $this->_vars['item2']): ?>
						<?php echo $this->_vars['item']['template']['data_admin_array'][$this->_vars['key2']]['name']; ?>
: <?php echo $this->_vars['item']['data_admin'][$this->_vars['key2']]; ?>
<br>
						<?php endforeach; endif; ?>
					<?php endif; ?>
				</td>
				<td class="center">
					<a class="btn-link" href="<?php echo $this->_vars['site_url']; ?>
listings/apply_service/<?php echo $this->_vars['data']['id']; ?>
/<?php echo $this->_vars['item']['gid']; ?>
" target="blank">
						<ins class="with-icon i-rarr"></ins>
						<?php echo l('btn_pay', 'listings', '', 'text', array()); ?>
					</a>	
				</tr>
			</tr>
			<?php endforeach; endif; ?>
			<?php else: ?>
			<tr><td colspan="4" class="center"><?php echo l('no_services', 'listings', '', 'text', array()); ?></td></tr>
			<?php endif; ?>
			</table>
			</div>
			
			<div class="b outside">
				<input type="button" name="btn_previous" value="<?php echo l('nav_prev', 'start', '', 'button', array()); ?>" class="btn hide" id="edit_listings_prev" />
				<input type="button" name="btn_next" value="<?php echo l('nav_next', 'start', '', 'button', array()); ?>" class="btn hide" id="edit_listings_next" />
				<a href="<?php echo $this->_vars['site_url']; ?>
listings/my" class="btn-link"><ins class="with-icon i-larr"></ins><?php echo l('link_back_to_my_listings', 'listings', '', 'text', array()); ?></a>
			</div>
			</form>	
		</div>	
	</div>

<div class="clr"></div>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
