<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.ld_option.php'); $this->register_function("ld_option", "tpl_function_ld_option");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-05-12 11:45:00 KRAT */ ?>

<?php echo tpl_function_js(array('module' => users,'file' => 'users-search.js'), $this);?>
<div class="users_form edit_block">
	<h2><?php echo l('header_search_users', 'users', '', 'text', array()); ?></h2>
	<form id="search_users_form" name="search_user_form" action="" method="POST">
	<?php echo tpl_function_ld(array('i' => 'professionals','gid' => 'users','assign' => 'professinals'), $this);?>
	<?php if ($this->_vars['user_types']): ?>
	<div class="r">
		<div class="f"><?php echo l('field_user_type', 'users', '', 'text', array()); ?>:</div>
		<div class="v">
			<select name="user_type" id="user_type">
				<?php if (is_array($this->_vars['user_types']) and count((array)$this->_vars['user_types'])): foreach ((array)$this->_vars['user_types'] as $this->_vars['item']): ?>
			    <option value="<?php echo $this->_run_modifier($this->_vars['item'], 'escape', 'plugin', 1); ?>
" <?php if ($this->_vars['item'] == $this->_vars['user_type']): ?>selected<?php endif; ?>><?php echo tpl_function_ld_option(array('i' => 'professionals','gid' => 'users','option' => $this->_vars['item']), $this);?></option>
				<?php endforeach; endif; ?>
			</select>
			<script><?php echo '
				$(function(){
					$(\'#user_type\').bind(\'change\', function(){
						if($(this).val() == \'company\'){
							$(\'#regionbox\').show();
							$(\'#postalcodebox\').show();
						}else{
							$(\'#regionbox\').hide();
							$(\'#postalcodebox\').hide();
						}
					});
				});
			'; ?>
</script>
		</div>
	</div>	
	<?php endif; ?>
	<div class="r <?php if ($this->_vars['user_type'] != 'company'): ?>hide<?php endif; ?>" id="regionbox">
		<div class="f"><?php echo l('field_region', 'users', '', 'text', array()); ?>:</div>
		<div class="v"><?php echo tpl_function_country_input(array('select_type' => 'city','id_country' => $this->_vars['data']['country'],'id_region' => $this->_vars['data']['region'],'id_city' => $this->_vars['data']['city']), $this);?></div>
	</div>
	<div class="r <?php if ($this->_vars['user_type'] != 'company'): ?>hide<?php endif; ?>" id="postalcodebox">
		<div class="f"><?php echo l('field_postal_code', 'users', '', 'text', array()); ?>:</div>
		<div class="v"><input type="text" name="zip" value="<?php echo $this->_run_modifier($this->_vars['data']['zip'], 'escape', 'plugin', 1); ?>
" /></div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_keyword', 'users', '', 'text', array()); ?>:</div>
		<div class="v"><input type="text" name="keyword" value="<?php echo $this->_run_modifier($this->_vars['data']['keyword'], 'escape', 'plugin', 1); ?>
" /></div>
	</div>
	<div class="r">
		<input type="submit" value="<?php echo l('btn_search', 'start', '', 'button', array()); ?>" />
	</div>
	</form>
</div>
<script type='text/javascript'><?php echo '
	var uSearch;
	$(function(){
		uSearch = new userSearch({
			siteUrl: \'';  echo $this->_vars['site_url'];  echo '\',
		});
	});
'; ?>
</script>
