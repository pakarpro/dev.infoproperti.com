<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-05-06 10:17:36 KRAT */ ?>

<h2 class="line top bottom linked">
	<?php echo l('reg_subscriptions', 'subscriptions', '', 'text', array()); ?>
	<a class="btn-link fright" href="<?php echo $this->_vars['site_url']; ?>
users/profile/subscriptions/"><ins class="with-icon i-edit"></ins></a>
</h2>
<div class="view-section">
	<div class="view-section">
	<?php if (is_array($this->_vars['subscriptions_list']) and count((array)$this->_vars['subscriptions_list'])): foreach ((array)$this->_vars['subscriptions_list'] as $this->_vars['item']): ?>
		<div class="r">
			<div class="f"><?php echo $this->_vars['item']['name']; ?>
:</div>
			<div class="v"><?php if ($this->_vars['item']['subscribed']):  echo l('user_subscribed', 'subscriptions', '', 'text', array());  else:  echo l('user_not_subscribed', 'subscriptions', '', 'text', array());  endif; ?></div>
		</div>
	<?php endforeach; endif; ?>
	</div>
</div>