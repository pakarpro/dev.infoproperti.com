<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-02 15:56:08 KRAT */ ?>

	<?php switch($this->_vars['template']): case 'icon':  ?>
		<a href="<?php echo $this->_vars['site_url']; ?>
listings/save/<?php echo $this->_vars['listing']['id']; ?>
" id="btn-save-<?php echo $this->_vars['listing']['id'];  echo $this->_vars['rand']; ?>
" class="btn-link link-r-margin" title="<?php if ($this->_vars['listing']['is_saved']):  echo l('text_listing_saved', 'listings', '', 'button', array());  else:  echo l('link_add_to_comparison', 'listings', '', 'button', array());  endif; ?>"><ins class="with-icon <?php if ($this->_vars['listing']['is_saved']): ?>g<?php endif; ?> i-favorite"></ins></a>
	<?php break; case 'link':  ?>
		<!-- add to fav 1. -->
		<a href="<?php echo $this->_vars['site_url']; ?>
listings/save/<?php echo $this->_vars['listing']['id']; ?>
" id="link-save-<?php echo $this->_vars['listing']['id'];  echo $this->_vars['rand']; ?>
" class="w-icon-fav <?php if ($this->_vars['listing']['is_saved']): ?>hide<?php endif; ?>"><?php echo l('link_add_to_comparison', 'listings', '', 'text', array()); ?></a><span id="text-save-<?php echo $this->_vars['listing']['id'];  echo $this->_vars['rand']; ?>
" <?php if (! $this->_vars['listing']['is_saved']): ?>class="hide"<?php endif; ?>><?php echo l('text_listing_saved', 'listings', '', 'text', array()); ?></span>
	<?php break; default:  ?>
		<!-- add to fav 2. -->
		<a href="<?php echo $this->_vars['site_url']; ?>
listings/save/<?php echo $this->_vars['listing']['id']; ?>
" id="link-save-<?php echo $this->_vars['listing']['id'];  echo $this->_vars['rand']; ?>
" class="w-icon-fav <?php if ($this->_vars['listing']['is_saved']): ?>hide<?php endif; ?>"><?php echo l('link_add_to_comparison', 'listings', '', 'text', array()); ?></a><span id="text-save-<?php echo $this->_vars['listing']['id'];  echo $this->_vars['rand']; ?>
" <?php if (! $this->_vars['listing']['is_saved']): ?>class="hide"<?php endif; ?>><?php echo l('text_listing_saved', 'listings', '', 'text', array()); ?></span>
<?php break; endswitch;  echo $this->_vars['separator']; ?>

<?php echo tpl_function_js(array('file' => 'listings-saved.js','module' => 'listings'), $this);?>
<script><?php echo '
	$(function(){
		new listingsSaved({
			siteUrl: \'';  echo $this->_vars['site_url'];  echo '\',
			saveBtn: \''; ?>
btn-save-<?php echo $this->_vars['listing']['id'];  echo $this->_vars['rand'];  echo '\',
			saveLink: \''; ?>
link-save-<?php echo $this->_vars['listing']['id'];  echo $this->_vars['rand'];  echo '\',
			saveText: \''; ?>
text-save-<?php echo $this->_vars['listing']['id'];  echo $this->_vars['rand'];  echo '\',
			listingId: ';  echo $this->_vars['listing']['id'];  echo ',
			isSaved: ';  if ($this->_vars['listing']['is_saved']): ?>1<?php else: ?>0<?php endif;  echo ',
			savedTitle: \'';  echo l('text_listing_saved', 'listings', '', 'button', array());  echo '\',
			';  if ($this->_vars['is_guest']): ?>displayLogin: true,<?php endif;  echo '
		});
	});
'; ?>
</script>
