<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-02-17 10:43:37 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<form method="post" action="<?php echo $this->_vars['data']['action']; ?>
" name="save_form" enctype="multipart/form-data">
	<div class="edit-form n150">
		<div class="row header"><?php if ($this->_vars['data']['id']):  echo l('admin_header_service_change', 'services', '', 'text', array());  else:  echo l('admin_header_service_add', 'services', '', 'text', array());  endif; ?></div>
		<div class="row">
			<div class="h"><?php echo l('field_gid', 'services', '', 'text', array()); ?>:&nbsp;* </div>
			<div class="v"><input type="text" value="<?php echo $this->_vars['data']['gid']; ?>
" name="gid"></div>
		</div>
		<div class="row">
			<div class="h"><?php echo l('field_name', 'services', '', 'text', array()); ?>:&nbsp;* </div>
			<div class="v">
				<?php if (is_array($this->_vars['languages']) and count((array)$this->_vars['languages'])): foreach ((array)$this->_vars['languages'] as $this->_vars['lang_id'] => $this->_vars['lang_item']): ?>
				<input type="<?php if ($this->_vars['lang_id'] == $this->_vars['cur_lang']): ?>text<?php else: ?>hidden<?php endif; ?>" name="langs[<?php echo $this->_vars['lang_id']; ?>
]" value="<?php if ($this->_vars['validate_lang']):  echo $this->_vars['validate_lang'][$this->_vars['lang_id']];  else:  echo $this->_vars['data']['name'];  endif; ?>" lang-editor="value" lang-editor-type="langs" lang-editor-lid="<?php echo $this->_vars['lang_id']; ?>
">
				<?php endforeach; endif; ?>
				<a href="#" lang-editor="button" lang-editor-type="langs"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-translate.png" width="16" height="16"></a>
				<?php echo tpl_function_block(array('name' => lang_inline_editor,'module' => start), $this);?>
			</div>
		</div>
		<div class="row">
			<div class="h"><?php echo l('field_pay_type', 'services', '', 'text', array()); ?>:&nbsp;* </div>
			<div class="v"><select name="pay_type">
				<?php if (is_array($this->_vars['pay_type_lang']['option']) and count((array)$this->_vars['pay_type_lang']['option'])): foreach ((array)$this->_vars['pay_type_lang']['option'] as $this->_vars['key'] => $this->_vars['item']): ?><option value="<?php echo $this->_vars['key']; ?>
"<?php if ($this->_vars['key'] == $this->_vars['data']['pay_type']): ?> selected<?php endif; ?>><?php echo $this->_vars['item']; ?>
</option><?php endforeach; endif; ?>
			</select></div>
		</div>
		<div class="row">
			<div class="h"><?php echo l('field_status', 'services', '', 'text', array()); ?>: </div>
			<div class="v"><input type="checkbox" value="1" <?php if ($this->_vars['data']['status']): ?>checked<?php endif; ?> name="status"></div>
		</div>

		<div class="row">
			<div class="h"><?php echo l('field_template', 'services', '', 'text', array()); ?>: </div>
			<div class="v">
			<?php if ($this->_vars['template']): ?>
				<?php echo $this->_vars['template']['name']; ?>
<input type="hidden" name="template_gid" value="<?php echo $this->_vars['data']['template_gid']; ?>
">
			<?php else: ?>
				<select name="template_gid" onchange="javascript: load_param_block(this.value);">
					<?php if (is_array($this->_vars['templates']) and count((array)$this->_vars['templates'])): foreach ((array)$this->_vars['templates'] as $this->_vars['item']): ?><option value="<?php echo $this->_vars['item']['gid']; ?>
"><?php echo $this->_vars['item']['name']; ?>
</option><?php endforeach; endif; ?>
				</select>
			<?php endif; ?>
			</div>
		</div>

		<div id="admin_params">
		<?php echo $this->_vars['template_block']; ?>

		</div>

	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div></div>
	<a class="cancel" href="<?php echo $this->_vars['site_url']; ?>
admin/services"><?php echo l('btn_cancel', 'start', '', 'text', array()); ?></a>
</form>
<div class="clr"></div>
<script><?php echo '
$(function(){
	$("div.row:odd").addClass("zebra");
});
function showLangs(divId){
	$(\'#\'+divId).slideToggle();
}

function load_param_block(id){
	$(\'#admin_params\').load(\'';  echo $this->_vars['site_url']; ?>
admin/services/ajax_get_template_admin_param_block/<?php echo '\'+id);
	$("div.row:odd").addClass("zebra");
}

'; ?>
</script>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
