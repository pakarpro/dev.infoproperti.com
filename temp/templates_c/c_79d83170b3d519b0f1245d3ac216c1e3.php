<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.block.php'); $this->register_function("block", "tpl_function_block");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.truncate.php'); $this->register_modifier("truncate", "tpl_modifier_truncate");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.js.php'); $this->register_function("js", "tpl_function_js");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2014-09-30 14:58:18 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array('load_type' => 'ui'));
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<?php echo tpl_function_js(array('module' => listings,'file' => 'wish-list.js'), $this);?>
<div class="actions">
	<ul>
		<li><div class="l"><a href="<?php echo $this->_vars['site_url']; ?>
admin/listings/delete_from_wish_list/<?php echo $this->_vars['wish_list']['id']; ?>
" id="delete_from_wish_list"><?php echo l('link_delete_from_wish_list', 'listings', '', 'text', array()); ?></a></div></li>
	</ul>
	&nbsp;
</div>

<div id="wish_list_content">
<?php if ($this->_run_modifier($this->_vars['listings'], 'count', 'PHP', 1)): ?>
<ol class="blocks" id="sortList">
	<?php if (is_array($this->_vars['listings']) and count((array)$this->_vars['listings'])): foreach ((array)$this->_vars['listings'] as $this->_vars['item']): ?>
	<li id="pItem<?php echo $this->_vars['item']['listing']['id']; ?>
">
	<div class="item listing">
		<div class="fright"><a href="<?php echo $this->_vars['site_url']; ?>
admin/listings/delete_from_wish_list/<?php echo $this->_vars['wish_list']['id']; ?>
/<?php echo $this->_vars['item']['listing']['id']; ?>
" onclick="javascript: if(!confirm('<?php echo l('note_delete_from_wish_list', 'listings', '', 'js', array()); ?>')) return false; wishList.delete_from_wish_list(<?php echo $this->_vars['item']['listing']['id']; ?>
); return false;"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-delete.png" width="16" height="16" border="0" alt="<?php echo l('link_delete_from_wish_list', 'listings', '', 'button', array()); ?>" title="<?php echo l('link_delete_from_wish_list', 'listings', '', 'button', array()); ?>"></a></div>
		<h3><a href="<?php echo $this->_vars['site_url']; ?>
admin/listings/edit/<?php echo $this->_vars['item']['listing']['id']; ?>
"><?php echo $this->_run_modifier($this->_vars['item']['listing']['output_name'], 'truncate', 'plugin', 1, 45); ?>
</a></h3>
		<div class="image">
			<a href="<?php echo $this->_vars['site_url']; ?>
admin/listings/edit/<?php echo $this->_vars['item']['listing']['id']; ?>
"><img src="<?php echo $this->_vars['item']['listing']['media']['photo']['thumbs']['small']; ?>
" title="<?php echo $this->_vars['item']['listing']['output_name']; ?>
"></a>
		</div>
		<div class="body">
			<?php 
$this->assign('no_info_str', l('no_information', 'start', '', 'text', array()));
 ?>
			<h3><?php echo tpl_function_block(array('name' => listing_price_block,'module' => 'listings','data' => $this->_vars['item']['listing']), $this);?></h3>
			<div class="t-1">
				<?php echo $this->_vars['item']['listing']['property_type_str']; ?>
 <?php echo $this->_vars['item']['listing']['operation_type_str']; ?>

				<br><?php echo $this->_run_modifier($this->_vars['item']['listing']['square_output'], 'truncate', 'plugin', 1, 20); ?>

				<?php if ($this->_run_modifier($this->_vars['item']['listing']['date_open'], 'strtotime', 'PHP', 1) >= $this->_vars['tstamp']): ?><br><?php echo l('text_date_open_status', 'listings', '', 'text', array());  endif; ?>
			</div>
			<div class="t-2">
				
			</div>
			<div class="t-3">
				<?php echo $this->_run_modifier($this->_vars['item']['listing']['user']['output_name'], 'truncate', 'plugin', 1, 50); ?>

			</div>
		</div>
		<div class="clr"></div>
	</div>
	</li>
	<?php endforeach; endif; ?>
</ol>
<?php endif; ?>
<p <?php if ($this->_run_modifier($this->_vars['listings'], 'count', 'PHP', 1) > 0): ?>class="hide"<?php endif; ?> id="empty_content"><?php echo l('no_listings', 'listings', '', 'text', array()); ?></p>
<div class="clr"></div>
</div>

<script><?php echo '
	var wishList;
	$(function(){
		wishList = new wishList({
			siteUrl: \'';  echo $this->_vars['site_url'];  echo '\',
			saveSortingUrl: \'admin/listings/ajax_save_wish_list_sorting/';  echo $this->_vars['wish_list']['id'];  echo '\',
			deleteFromWishListUrl: \'admin/listings/ajax_delete_from_wish_list/';  echo $this->_vars['wish_list']['id'];  echo '/\',
			reloadBlockUrl: \'admin/listings/ajax_reload_wish_list_content/';  echo $this->_vars['wish_list']['id'];  echo '/\'
		});
	});
'; ?>
</script>
	
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
