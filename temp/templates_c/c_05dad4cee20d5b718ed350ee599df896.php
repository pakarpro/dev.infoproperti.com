<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2014-10-01 10:43:57 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

<div class="menu-level3">
	<ul id="edit_tabs">
		<li class="active" onclick="javascript: openTab('template_settings', this); return false;"><a href="#" onclick="return false;"><?php echo l('template_settings', 'notifications', '', 'text', array()); ?></a></li>
		<?php if (is_array($this->_vars['langs']) and count((array)$this->_vars['langs'])): foreach ((array)$this->_vars['langs'] as $this->_vars['key'] => $this->_vars['item']): ?>
			<li onclick="javascript: openTab('lang<?php echo $this->_vars['item']['id']; ?>
', this); return false;"><a href="#" onclick="return false;"><?php echo $this->_vars['item']['name']; ?>
</a></li>
		<?php endforeach; endif; ?>
	</ul>
	&nbsp;
</div>
<form method="post" action="<?php echo $this->_vars['data']['action']; ?>
" name="save_form">
	<div class="edit-form n150" id="edit_divs">
		<div id="template_settings"class="tab">
			<div class="row header"><?php echo l('admin_header_template_edit', 'notifications', '', 'text', array()); ?></div>
			<div class="row zebra">
				<div class="h"><?php echo l('field_template_gid', 'notifications', '', 'text', array()); ?>:&nbsp;* </div>
				<div class="v"><?php if ($this->_vars['allow_edit']): ?><input type="text" value="<?php echo $this->_vars['data']['gid']; ?>
" name="gid"><?php else:  echo $this->_vars['data']['gid'];  endif; ?></div>
			</div>
			<div class="row">
				<div class="h"><?php echo l('field_template_name', 'notifications', '', 'text', array()); ?>:&nbsp;* </div>
				<div class="v"><?php if ($this->_vars['allow_edit']): ?><input type="text" value="<?php echo $this->_vars['data']['name']; ?>
" name="name"><?php else:  echo $this->_vars['data']['name'];  endif; ?></div>
			</div>
			<div class="row zebra">
				<div class="h"><?php echo l('field_content_type', 'notifications', '', 'text', array()); ?>:&nbsp;* </div>
				<div class="v">
					<?php if ($this->_vars['allow_edit']): ?>
						<select name="content_type">
							<option value="text" <?php if ($this->_vars['data']['content_type'] == 'text'): ?>selected<?php endif; ?>><?php echo l('field_content_type_text', 'notifications', '', 'text', array()); ?></option>
							<option value="html" <?php if ($this->_vars['data']['content_type'] == 'html'): ?>selected<?php endif; ?>><?php echo l('field_content_type_html', 'notifications', '', 'text', array()); ?></option>
						</select>
				<?php else:  echo $this->_vars['data']['content_type'];  endif; ?>
			</div>
		</div>
		<div class="row">
			<div class="h"><?php echo l('field_template_vars', 'notifications', '', 'text', array()); ?>: </div>
			<div class="v">
				<?php if ($this->_vars['allow_edit'] && $this->_vars['allow_var_edit']): ?>
					<input type="text" value="<?php echo $this->_vars['data']['vars_str']; ?>
" name="vars" class="long">
			<?php else:  echo $this->_vars['data']['vars_str'];  endif; ?>
			<br><i><?php echo l('field_template_vars_text', 'notifications', '', 'text', array()); ?></i></div>
	</div>
</div>

<?php if (is_array($this->_vars['langs']) and count((array)$this->_vars['langs'])): foreach ((array)$this->_vars['langs'] as $this->_vars['key'] => $this->_vars['item']): ?>
	<?php $this->assign('content', $this->_vars['data_content'][$this->_vars['key']]); ?>
	<div id="lang<?php echo $this->_vars['item']['id']; ?>
" class="tab hide">
		<div class="row header"><?php echo l('admin_header_template_content', 'notifications', '', 'text', array()); ?>: <?php echo $this->_vars['item']['name']; ?>
</div>
		<div class="row zebra">
			<div class="h"><?php echo l('field_available_global_variables', 'notifications', '', 'text', array()); ?>: </div>
			<div class="v"><?php if (is_array($this->_vars['global_vars']) and count((array)$this->_vars['global_vars'])): foreach ((array)$this->_vars['global_vars'] as $this->_vars['var']): ?>[<?php echo $this->_vars['var']; ?>
] <?php endforeach; else: ?><i><?php echo l('empty_variables', 'notifications', '', 'text', array()); ?></i><?php endif; ?></div>
		</div>
		<div class="row">
			<div class="h"><?php echo l('field_available_variables', 'notifications', '', 'text', array()); ?>: </div>
			<div class="v"><?php if (is_array($this->_vars['data']['vars']) and count((array)$this->_vars['data']['vars'])): foreach ((array)$this->_vars['data']['vars'] as $this->_vars['var']): ?>[<?php echo $this->_vars['var']; ?>
] <?php endforeach; else: ?><i><?php echo l('empty_variables', 'notifications', '', 'text', array()); ?></i><?php endif; ?></div>
		</div>
		<div class="row zebra">
			<div class="h"><?php echo l('field_subject', 'notifications', '', 'text', array()); ?>: </div>
			<div class="v"><input type="text" value="<?php echo $this->_vars['content']['subject']; ?>
" name="subject[<?php echo $this->_vars['item']['id']; ?>
]" class="long"></div>
		</div>
		<div class="row">
			<div class="h"><?php echo l('field_content', 'notifications', '', 'text', array()); ?>: </div>
			<div class="v"><?php if ($this->_vars['data']['content_type'] == 'html'):  echo $this->_vars['content']['content_fck'];  else: ?><textarea name="content[<?php echo $this->_vars['item']['id']; ?>
]" class="mail-content"><?php echo $this->_vars['content']['content']; ?>
</textarea><?php endif; ?></div>
		</div>
	</div>
<?php endforeach; endif; ?>

<div class="btn"><div class="l"><input type="submit" name="btn_save" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div></div>
<a class="cancel" href="<?php echo $this->_vars['site_url']; ?>
admin/notifications/templates"><?php echo l('btn_cancel', 'start', '', 'text', array()); ?></a>
</div>
</form>
<script>
	<?php echo '
function openTab(id, object){
	$(\'#edit_divs > div.tab\').hide();
	$(\'#\'+id).show();
	$(\'#edit_tabs > li\').removeClass(\'active\');
	$(object).addClass(\'active\');
}

	'; ?>

</script>
<div class="clr"></div>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
