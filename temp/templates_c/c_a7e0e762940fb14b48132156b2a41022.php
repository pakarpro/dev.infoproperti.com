<?php require_once('/opt/ip/system/libraries/template_lite/plugins/function.helper.php'); $this->register_function("helper", "tpl_function_helper");  require_once('/opt/ip/system/libraries/template_lite/plugins/modifier.escape.php'); $this->register_modifier("escape", "tpl_modifier_escape");  require_once('/opt/ip/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-01 17:25:18 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

<div class="content-block">
	<div class="inside logform">
		<h1><?php echo l('header_login', 'users', '', 'text', array()); ?></h1>
		<form action="<?php echo $this->_vars['site_url']; ?>
users/login" method="post" >
			<div class="r">
				<div class="f"><?php echo l('field_email', 'users', '', 'text', array()); ?>:&nbsp;*</div>
				<div class="v"><input type="text" name="email" <?php if ($this->_vars['DEMO_MODE']): ?>value="<?php echo $this->_run_modifier($this->_vars['demo_user_type_login_settings']['login'], 'escape', 'plugin', 1); ?>
"<?php endif; ?>></div>
			</div>
			<div class="r">
				<div class="f"><?php echo l('field_password', 'users', '', 'text', array()); ?>:&nbsp;*</div>
				<div class="v">
					<input type="password" name="password" <?php if ($this->_vars['DEMO_MODE']): ?>value="<?php echo $this->_run_modifier($this->_vars['demo_user_type_login_settings']['password'], 'escape', 'plugin', 1); ?>
"<?php endif; ?>>
					<span class="v-link"><a href="<?php echo $this->_vars['site_url']; ?>
users/restore"><?php echo l('link_restore', 'users', '', 'text', array()); ?></a></span>
				</div>
			</div>
			
			
			<div class="r">
				<input type="submit" value="<?php echo l('btn_login', 'start', '', 'button', array()); ?>" name="logbtn">
			</div>
		</form>
		<?php echo tpl_function_helper(array('func_name' => show_social_networking_login,'module' => users_connections), $this);?>
	</div>
	<div class="clr"></div>
</div>

	<br><br><br>

<div class="clr"></div>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
