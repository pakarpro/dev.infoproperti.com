<?php require_once('/opt/ip/system/libraries/template_lite/plugins/function.helper.php'); $this->register_function("helper", "tpl_function_helper");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.js.php'); $this->register_function("js", "tpl_function_js");  require_once('/opt/ip/system/libraries/template_lite/plugins/modifier.replace.php'); $this->register_modifier("replace", "tpl_modifier_replace");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-02-27 11:00:40 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array('load_type' => 'ui'));
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

<div class="rc">
	<div class="content-block">
		
		<?php if ($this->_vars['use_rss']): ?><span id="rss_link" class="btn-link fright" title="<?php echo l('link_search_results_rss', 'listings', '', 'button', array()); ?>"><ins class="with-icon i-rss"></ins></span><?php endif; ?>
		
		<h1><?php echo l('header_listings_result', 'listings', '', 'text', array()); ?> - <span id="total_rows"><?php echo $this->_vars['page_data']['total_rows']; ?>
</span> <?php echo l('header_listings_found', 'listings', '', 'text', array()); ?></h1>
		
		<div class="tabs tab-size-15 noPrint">
			<ul id="search_listings_sections">
				<?php if (is_array($this->_vars['operation_types']) and count((array)$this->_vars['operation_types'])): foreach ((array)$this->_vars['operation_types'] as $this->_vars['tgid']): ?>
				<li id="m_<?php echo $this->_vars['tgid']; ?>
" sgid="<?php echo $this->_vars['tgid']; ?>
" class="<?php if ($this->_vars['current_operation_type'] == $this->_vars['tgid']): ?>active<?php endif; ?>"><a href="<?php echo $this->_run_modifier($this->_vars['menu_action_link'], 'replace', 'plugin', 1, '[operation_type]', $this->_vars['tgid']); ?>
/default/DESC/1"><?php echo l('operation_search_'.$this->_vars['tgid'], 'listings', '', 'text', array()); ?></a></li>
				<?php endforeach; endif; ?>
			</ul>
			<a href="<?php echo $this->_vars['site_url'].'listings/set_view_mode/list'; ?>
" class="btn-link fright" title="<?php echo l('link_view_list', 'listings', '', 'button', array()); ?>"><ins class="with-icon i-list"></ins></a>
		</div>
		
		<div class="sorter line" id="sorter_block">
			<?php echo tpl_function_sorter(array('links' => $this->_vars['sort_data']['links'],'order' => $this->_vars['sort_data']['order'],'direction' => $this->_vars['sort_data']['direction'],'url' => $this->_vars['sort_data']['url']), $this);?>
			<?php if ($this->_vars['listings']): ?><div class="fright" id="pages_block_1"><?php echo tpl_function_pagination(array('data' => $this->_vars['page_data'],'type' => 'cute'), $this);?></div><?php endif; ?>
		</div>
		
		<?php echo tpl_function_block(array('name' => show_default_map,'module' => geomap,'id_user' => $this->_vars['data']['id_user'],'gid' => 'listing_search','settings' => $this->_vars['map_settings'],'markers' => $this->_vars['markers'],'width' => '754','height' => '400','map_id' => 'listings_map_full_container'), $this);?>
		
		<div id="listings_map"><?php echo $this->_vars['block']; ?>
</div>

		<div id="pages_block_2"><?php if ($this->_vars['listings']):  echo tpl_function_pagination(array('data' => $this->_vars['page_data'],'type' => 'full'), $this); endif; ?></div>
		
		<?php echo tpl_function_js(array('module' => listings,'file' => 'listings-map.js'), $this);?>
		<script><?php echo '
			$(function(){
				new listingsMap({
					siteUrl: \'';  echo $this->_vars['site_url'];  echo '\',
					mapAjaxUrl: \''; ?>
listings/ajax_index<?php echo '\',
					sectionId: \'search_listings_sections\',
					';  if ($this->_vars['use_rss']): ?>useRss: true,<?php endif;  echo '
					operationType: \'';  echo $this->_vars['current_operation_type'];  echo '\',
					order: \'';  echo $this->_vars['order'];  echo '\',
					orderDirection: \'';  echo $this->_vars['order_direction'];  echo '\',
					page: ';  echo $this->_vars['page'];  echo ',
					tIds: [\'pages_block_1\', \'pages_block_2\', \'sorter_block\'],
				});
			});
		'; ?>
</script>
	</div>
</div>

<div class="lc">
	<div class="inside account_menu">
		<?php echo tpl_function_block(array('name' => listings_search_block,'module' => listings), $this);?>
		<?php if ($this->_vars['use_poll_in_search']):  echo tpl_function_block(array('name' => show_poll_place_block,'module' => polls,'one_poll_place' => 0), $this); endif; ?>
		<?php echo tpl_function_helper(array('func_name' => show_banner_place,'module' => banners,'func_param' => 'big-left-banner'), $this);?>
		<?php echo tpl_function_helper(array('func_name' => show_banner_place,'module' => banners,'func_param' => 'left-banner'), $this);?>
	</div>
</div>

<div class="clr"></div>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
