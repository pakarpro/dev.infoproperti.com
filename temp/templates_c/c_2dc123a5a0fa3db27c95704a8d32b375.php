<?php require_once('/opt/ip/system/libraries/template_lite/plugins/function.pagination.php'); $this->register_function("pagination", "tpl_function_pagination");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.sorter.php'); $this->register_function("sorter", "tpl_function_sorter");  require_once('/opt/ip/system/libraries/template_lite/plugins/block.strip.php'); $this->register_block("strip", "tpl_block_strip");  require_once('/opt/ip/system/libraries/template_lite/plugins/block.capture.php'); $this->register_block("capture", "tpl_block_capture");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.block.php'); $this->register_function("block", "tpl_function_block");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-02-27 11:00:40 KRAT */ ?>

<?php echo tpl_function_block(array('name' => update_default_map,'module' => geomap,'markers' => $this->_vars['markers'],'map_id' => 'listings_map_full_container'), $this);?>

<?php if ($this->_vars['update_operation_type']): ?>
<script><?php echo '
	$(function(){
		var operation_type = $(\'#operation_type\');
		if(operation_type.val() != \'';  echo $this->_vars['update_operation_type'];  echo '\')
			$(\'#operation_type\').val(\'';  echo $this->_vars['update_operation_type'];  echo '\').trigger(\'change\');
	});
'; ?>
</script>
<?php endif; ?>

<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'sorter_block')); tpl_block_capture(array('assign' => 'sorter_block'), null, $this); ob_start();  $this->_tag_stack[] = array('tpl_block_strip', array()); tpl_block_strip(array(), null, $this); ob_start(); ?>
	<?php echo tpl_function_sorter(array('links' => $this->_vars['sort_data']['links'],'order' => $this->_vars['sort_data']['order'],'direction' => $this->_vars['sort_data']['direction'],'url' => $this->_vars['sort_data']['url']), $this);?>
	<?php if ($this->_vars['page_data']['total_rows']): ?><div class="fright" id="pages_block_1"><?php echo tpl_function_pagination(array('data' => $this->_vars['page_data'],'type' => 'cute'), $this);?></div><?php endif;  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_strip($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack);  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack);  $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'paginator_bottom_block')); tpl_block_capture(array('assign' => 'paginator_bottom_block'), null, $this); ob_start();  if ($this->_vars['page_data']['total_rows']):  echo tpl_function_pagination(array('data' => $this->_vars['page_data'],'type' => 'full'), $this); endif;  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>

<script><?php echo '
	$(function(){
		';  if ($this->_vars['update_search_block']): ?>$('#search_filter_block').html('<?php $this->_tag_stack[] = array('tpl_block_strip', array()); tpl_block_strip(array(), null, $this); ob_start();  echo $this->_vars['search_filters_block'];  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_strip($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>');<?php endif;  echo '
		$(\'#total_rows\').html(\'';  echo $this->_vars['page_data']['total_rows'];  echo '\');
		$(\'#sorter_block\').html(decodeURIComponent(\'';  echo $this->_run_modifier($this->_vars['sorter_block'], 'rawurlencode', 'PHP', 1);  echo '\'));
		$(\'#pages_block_2\').html(\'';  $this->_tag_stack[] = array('tpl_block_strip', array()); tpl_block_strip(array(), null, $this); ob_start();  echo $this->_run_modifier($this->_vars['paginator_bottom_block'], 'addslashes', 'PHP', 1);  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_strip($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack);  echo '\');		
	});
'; ?>
</script>
