<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.js.php'); $this->register_function("js", "tpl_function_js");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2014-09-02 17:47:16 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
  $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->module_path. "start". $this->module_templates.  $this->get_current_theme_gid('', '"start"'). "left_panel.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<div class="rc">
	<div class="content-block">
		<?php echo tpl_function_js(array('module' => mailbox,'file' => 'mailbox.js'), $this);?>
		<script type='text/javascript'><?php echo '

			var mb;
			$(function(){
				mb = new Mailbox({
					siteUrl: \'';  echo $this->_vars['site_url'];  echo '\',
					folderId: \'';  echo $this->_vars['data']['active_folder'];  echo '\',
					chatId: \'';  echo $this->_vars['data']['active_chat'];  echo '\',
					viewType: \'chat\',
					initialNextBtn: \'';  echo $this->_vars['data']['next_btn'];  echo '\'
				});
			});

		'; ?>
</script>


		<h1><?php echo l('my_communication', 'mailbox', '', 'text', array()); ?></h1>

		<div class="content-value mailbox">
			<div id="chat_window">
				<p class="header-comment"><?php echo l('header_chat_with', 'mailbox', '', 'text', array()); ?> <?php echo $this->_vars['chat']['user_data']['output_name']; ?>
:</p>
				<div>
					<textarea id="message_area"></textarea><br>
					<input type="button" name="delete_btn" value="<?php echo l('btn_send_message', 'mailbox', '', 'button', array()); ?>" class='btn' id="msg_submit">
				</div>
				<div class="clr"></div>

				<div class="msgs">
					<ul id="message_list">
					<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->module_path. "mailbox". $this->module_templates.  $this->get_current_theme_gid('', '"mailbox"'). "block_messages_item.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
					</ul>
					<a class="btn-link<?php if (! $this->_vars['data']['next_btn']): ?> hide<?php endif; ?>" href="#" id="next-page">
					<ins class="with-icon i-rarr"></ins>
					<?php echo l('btn_next_messages', 'mailbox', '', 'button', array()); ?>
					</a>

				</div>

			</div>
			<div class="clr"></div>
		</div>
	</div>
</div>
<div class="clr"></div>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>