<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.js.php'); $this->register_function("js", "tpl_function_js");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.helper.php'); $this->register_function("helper", "tpl_function_helper");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2014-05-07 16:20:47 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

<?php echo tpl_function_helper(array('func_name' => get_admin_level1_menu,'helper_name' => menu,'func_param' => 'admin_banners_menu'), $this);?>
<div class="actions">&nbsp;</div>

<div class='menu-level3'>
	<ul>
		<li<?php if ($this->_vars['stat_type'] == 'day'): ?> class="active"<?php endif; ?>><a href="<?php echo $this->_vars['site_url']; ?>
admin/banners/statistic/<?php echo $this->_vars['banner_data']['id']; ?>
/day"><?php echo l('stat_day', 'banners', '', 'text', array()); ?></a></li>
		<li<?php if ($this->_vars['stat_type'] == 'week'): ?> class="active"<?php endif; ?>><a href="<?php echo $this->_vars['site_url']; ?>
admin/banners/statistic/<?php echo $this->_vars['banner_data']['id']; ?>
/week"><?php echo l('stat_week', 'banners', '', 'text', array()); ?></a></li>
		<li<?php if ($this->_vars['stat_type'] == 'month'): ?> class="active"<?php endif; ?>><a href="<?php echo $this->_vars['site_url']; ?>
admin/banners/statistic/<?php echo $this->_vars['banner_data']['id']; ?>
/month"><?php echo l('stat_month', 'banners', '', 'text', array()); ?></a></li>
		<li<?php if ($this->_vars['stat_type'] == 'year'): ?> class="active"<?php endif; ?>><a href="<?php echo $this->_vars['site_url']; ?>
admin/banners/statistic/<?php echo $this->_vars['banner_data']['id']; ?>
/year"><?php echo l('stat_year', 'banners', '', 'text', array()); ?></a></li>
	</ul>
	&nbsp;
</div>

<link rel="stylesheet" type="text/css" href="<?php echo $this->_vars['site_root']; ?>
application/modules/banners/js/jqplot/jquery.jqplot.min.css" />
<!--[if lt IE 9]<?php echo tpl_function_js(array('module' => banners,'file' => 'jqplot/excanvas.min.js'), $this);?><![endif]-->
<?php echo tpl_function_js(array('module' => banners,'file' => 'jqplot/jquery.jqplot.min.js'), $this);?>
<?php echo tpl_function_js(array('module' => banners,'file' => 'jqplot/plugins/jqplot.canvasTextRenderer.min.js'), $this);?>
<?php echo tpl_function_js(array('module' => banners,'file' => 'jqplot/plugins/jqplot.canvasAxisLabelRenderer.min.js'), $this);?>
<?php echo tpl_function_js(array('module' => banners,'file' => 'jqplot/plugins/jqplot.highlighter.min.js'), $this);?>
<?php echo tpl_function_js(array('module' => banners,'file' => 'jqplot/plugins/jqplot.cursor.min.js'), $this);?>
<?php echo tpl_function_js(array('module' => banners,'file' => 'jqplot/plugins/jqplot.dateAxisRenderer.min.js'), $this);?>
<?php echo tpl_function_js(array('module' => banners,'file' => 'jqplot/plugins/jqplot.canvasAxisTickRenderer.min.js'), $this);?>
<?php echo tpl_function_js(array('module' => banners,'file' => 'jqplot/plugins/jqplot.categoryAxisRenderer.min.js'), $this);?>

<?php if ($this->_vars['stat_type'] == 'day'): ?>
<div class="filter-form">
	<div class="row">
		<div class="h"><?php echo l('stat_day', 'banners', '', 'text', array()); ?>:</div>
		<div class="v"><a href="<?php echo $this->_vars['navigation']['prev']; ?>
"><?php echo l("nav_prev", 'start', '', 'text', array()); ?></a> <b><?php echo $this->_vars['navigation']['current']; ?>
</b> <a href="<?php echo $this->_vars['navigation']['next']; ?>
"><?php echo l("nav_next", 'start', '', 'text', array()); ?></a></div>
	</div>
	<div class="row" id="view_links">
		<div class="h"><?php echo l('stat_view_type', 'banners', '', 'text', array()); ?>:</div>
		<div class="v"><a href="#" class="active"><?php echo l('stat_by_hours', 'banners', '', 'text', array()); ?></a></div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('stat_overall_views', 'banners', '', 'text', array()); ?>:</div>
		<div class="v"><?php echo $this->_vars['statistic']['all']['view']; ?>
</div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('stat_overall_clicks', 'banners', '', 'text', array()); ?>:</div>
		<div class="v"><?php echo $this->_vars['statistic']['all']['click']; ?>
</div>
	</div>
</div>


<div id="view_blocks">
	<div id="by_hours_div">
		<div id="jplot_div"></div>
		<table cellspacing="0" cellpadding="0" class="data" width="100%">
		<tr>
			<th class="first"><?php echo l('stat_hour', 'banners', '', 'text', array()); ?></th>
			<th class="w100"><?php echo l('stat_views', 'banners', '', 'text', array()); ?></th>
			<th class="w100"><?php echo l('stat_clicks', 'banners', '', 'text', array()); ?></th>
		</tr>
		<?php if (is_array($this->_vars['statistic']['hour']) and count((array)$this->_vars['statistic']['hour'])): foreach ((array)$this->_vars['statistic']['hour'] as $this->_vars['hour'] => $this->_vars['item']): ?>
		<tr class="stat">
			<td class="first hour"><?php echo $this->_vars['hour']; ?>
</td>
			<td class="center views"><?php echo $this->_vars['item']['view']; ?>
</td>
			<td class="center clicks"><?php echo $this->_vars['item']['click']; ?>
</td>
		</tr>
		<?php endforeach; endif; ?>
		</table>
	</div>
	<script type='text/javascript'><?php echo '
	$(document).ready(function(){
		var viewPoints = [];
		var clicksPoints = [];
		var ticks = [];
		$("#by_hours_div tr.stat").each(function(){
			var hour = parseInt($(this).find(\'td.hour\').text());
			var views = parseInt($(this).find(\'td.views\').text());
			var clicks = parseInt($(this).find(\'td.clicks\').text());
			viewPoints.push([hour, views]);
			clicksPoints.push([hour, clicks]);
			ticks.push(hour);
		});

		var plot2 = $.jqplot (\'jplot_div\', [viewPoints, clicksPoints], {
			axesDefaults: { labelRenderer: $.jqplot.CanvasAxisLabelRenderer },
			seriesDefaults:{ renderer:$.jqplot.BarRenderer, rendererOptions: {fillToZero: true} },
			legend: { show: true, placement: \'insideGrid\' },
			series:[
				{ lineWidth:5, markerOptions: { style:\'dimaond\' }, label: "';  echo l('stat_views', 'banners', '', 'js', array());  echo '" },
				{ lineWidth:2, markerOptions: { style:"filledSquare", size:10 }, label: "';  echo l('stat_clicks', 'banners', '', 'js', array());  echo '" }
			],
			axes: { xaxis: { label: "';  echo l('stat_hour', 'banners', '', 'js', array());  echo '", tickOptions: {formatString: \'%d h\'}, ticks: ticks } },
			highlighter: { show: true, sizeAdjust: 7.5 },
			cursor: { show: false }
		});
	});
	'; ?>
</script>
</div>

<?php endif; ?>

<?php if ($this->_vars['stat_type'] == 'week'): ?>
<div class="filter-form">
	<div class="row">
		<div class="h"><?php echo l('stat_week', 'banners', '', 'text', array()); ?>:</div>
		<div class="v"><a href="<?php echo $this->_vars['navigation']['prev']; ?>
"><?php echo l("nav_prev", 'start', '', 'text', array()); ?></a> <b><?php echo $this->_vars['navigation']['current']; ?>
</b> <a href="<?php echo $this->_vars['navigation']['next']; ?>
"><?php echo l("nav_next", 'start', '', 'text', array()); ?></a></div>
	</div>
	<div class="row" id="view_links">
		<div class="h"><?php echo l('stat_view_type', 'banners', '', 'text', array()); ?>:</div>
		<div class="v"><a href="#" class="active"><?php echo l('stat_by_days', 'banners', '', 'text', array()); ?></a></div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('stat_overall_views', 'banners', '', 'text', array()); ?>:</div>
		<div class="v"><?php echo $this->_vars['statistic']['all']['view']; ?>
</div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('stat_overall_clicks', 'banners', '', 'text', array()); ?>:</div>
		<div class="v"><?php echo $this->_vars['statistic']['all']['click']; ?>
</div>
	</div>
</div>

<div id="view_blocks">
	<div id="by_days_div">
		<div id="jplot_div"></div>
		<table cellspacing="0" cellpadding="0" class="data" width="100%">
		<tr>
			<th class="first"><?php echo l('stat_week', 'banners', '', 'text', array()); ?></th>
			<th class="w100"><?php echo l('stat_views', 'banners', '', 'text', array()); ?></th>
			<th class="w100"><?php echo l('stat_clicks', 'banners', '', 'text', array()); ?></th>
		</tr>
		<?php if (is_array($this->_vars['statistic']['day']) and count((array)$this->_vars['statistic']['day'])): foreach ((array)$this->_vars['statistic']['day'] as $this->_vars['day'] => $this->_vars['item']): ?>
		<tr class="stat">
			<td class="first date"><?php echo $this->_vars['item']['date']; ?>
</td>
			<td class="center views"><?php echo $this->_vars['item']['view']; ?>
</td>
			<td class="center clicks"><?php echo $this->_vars['item']['click']; ?>
</td>
		</tr>
		<?php endforeach; endif; ?>
		</table>
	</div>
	<script type='text/javascript'><?php echo '
	$(document).ready(function(){
		var viewPoints = [];
		var clicksPoints = [];
		$("#by_days_div tr.stat").each(function(){
			var date = $(this).find(\'td.date\').text();
			var views = parseInt($(this).find(\'td.views\').text());
			var clicks = parseInt($(this).find(\'td.clicks\').text());
			viewPoints.push([date, views]);
			clicksPoints.push([date, clicks]);
		});

		var plot2 = $.jqplot (\'jplot_div\', [viewPoints, clicksPoints], {
			axesDefaults: { labelRenderer: $.jqplot.CanvasAxisTickRenderer },
			seriesDefaults:{ renderer:$.jqplot.BarRenderer, rendererOptions: {fillToZero: true} },
			legend: { show: true, placement: \'insideGrid\' },
			series:[
				{ lineWidth:5, markerOptions: { style:\'dimaond\' }, label: "';  echo l('stat_views', 'banners', '', 'js', array());  echo '" },
				{ lineWidth:2, markerOptions: { style:"filledSquare", size:10 }, label: "';  echo l('stat_clicks', 'banners', '', 'js', array());  echo '" }
			],
			axes: { xaxis: { label: "';  echo l('stat_day', 'banners', '', 'js', array());  echo '", renderer: $.jqplot.CategoryAxisRenderer} },
			highlighter: { show: true, sizeAdjust: 7.5 },
			cursor: { show: false }
		});
	});
	'; ?>
</script>
</div>
<?php endif; ?>

<?php if ($this->_vars['stat_type'] == 'month'): ?>
<div class="filter-form">
	<div class="row">
		<div class="h"><?php echo l('stat_month', 'banners', '', 'text', array()); ?>:</div>
		<div class="v"><a href="<?php echo $this->_vars['navigation']['prev']; ?>
"><?php echo l("nav_prev", 'start', '', 'text', array()); ?></a> <b><?php echo $this->_vars['navigation']['current']; ?>
</b> <a href="<?php echo $this->_vars['navigation']['next']; ?>
"><?php echo l("nav_next", 'start', '', 'text', array()); ?></a></div>
	</div>
	<div class="row" id="view_links">
		<div class="h"><?php echo l('stat_view_type', 'banners', '', 'text', array()); ?>:</div>
		<div class="v">
			<a href="#" class="active" id="by_weeks" onclick="switchView('by_weeks'); return false;"><?php echo l('stat_by_weeks', 'banners', '', 'text', array()); ?></a>
			<a href="#" id="by_days" onclick="switchView('by_days'); return false;"><?php echo l('stat_by_days', 'banners', '', 'text', array()); ?></a>
		</div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('stat_overall_views', 'banners', '', 'text', array()); ?>:</div>
		<div class="v"><?php echo $this->_vars['statistic']['all']['view']; ?>
</div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('stat_overall_clicks', 'banners', '', 'text', array()); ?>:</div>
		<div class="v"><?php echo $this->_vars['statistic']['all']['click']; ?>
</div>
	</div>
</div>

<div id="view_blocks">
	<div id="by_weeks_div" >
		<table cellspacing="0" cellpadding="0" class="data" width="100%">
		<tr>
			<th class="first"><?php echo l('stat_week', 'banners', '', 'text', array()); ?></th>
			<th class="w100"><?php echo l('stat_views', 'banners', '', 'text', array()); ?></th>
			<th class="w100"><?php echo l('stat_clicks', 'banners', '', 'text', array()); ?></th>
		</tr>
		<?php if (is_array($this->_vars['statistic']['week']) and count((array)$this->_vars['statistic']['week'])): foreach ((array)$this->_vars['statistic']['week'] as $this->_vars['day'] => $this->_vars['item']): ?>
		<tr class="stat">
			<td class="first week"><?php echo $this->_vars['item']['start_day']; ?>
 - <?php echo $this->_vars['item']['end_day']; ?>
</td>
			<td class="center views"><?php echo $this->_vars['item']['view']; ?>
</td>
			<td class="center clicks"><?php echo $this->_vars['item']['click']; ?>
</td>
		</tr>
		<?php endforeach; endif; ?>
		</table>
	</div>
	<div id="by_days_div" style="display: none;">
		<table cellspacing="0" cellpadding="0" class="data" width="100%">
		<tr>
			<th class="first"><?php echo l('stat_week', 'banners', '', 'text', array()); ?></th>
			<th class="w100"><?php echo l('stat_views', 'banners', '', 'text', array()); ?></th>
			<th class="w100"><?php echo l('stat_clicks', 'banners', '', 'text', array()); ?></th>
		</tr>
		<?php if (is_array($this->_vars['statistic']['day']) and count((array)$this->_vars['statistic']['day'])): foreach ((array)$this->_vars['statistic']['day'] as $this->_vars['day'] => $this->_vars['item']): ?>
		<tr class="stat">
			<td class="first date"><?php echo $this->_vars['item']['date']; ?>
</td>
			<td class="center views"><?php echo $this->_vars['item']['view']; ?>
</td>
			<td class="center clicks"><?php echo $this->_vars['item']['click']; ?>
</td>
		</tr>
		<?php endforeach; endif; ?>
		</table>
	</div>
</div>
<?php endif; ?>

<?php if ($this->_vars['stat_type'] == 'year'): ?>
<div class="filter-form">
	<div class="row">
		<div class="h"><?php echo l('stat_year', 'banners', '', 'text', array()); ?>:</div>
		<div class="v"><a href="<?php echo $this->_vars['navigation']['prev']; ?>
"><?php echo l("nav_prev", 'start', '', 'text', array()); ?></a> <b><?php echo $this->_vars['navigation']['current']; ?>
</b> <a href="<?php echo $this->_vars['navigation']['next']; ?>
"><?php echo l("nav_next", 'start', '', 'text', array()); ?></a></div>
	</div>
	<div class="row" id="view_links">
		<div class="h"><?php echo l('stat_view_type', 'banners', '', 'text', array()); ?>:</div>
		<div class="v">
			<a href="#" class="active" id="by_month" onclick="switchView('by_month'); return false;"><?php echo l('stat_by_month', 'banners', '', 'text', array()); ?></a>
			<a href="#" id="by_weeks" onclick="switchView('by_weeks'); return false;"><?php echo l('stat_by_weeks', 'banners', '', 'text', array()); ?></a>
		</div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('stat_overall_views', 'banners', '', 'text', array()); ?>:</div>
		<div class="v"><?php echo $this->_vars['statistic']['all']['view']; ?>
</div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('stat_overall_clicks', 'banners', '', 'text', array()); ?>:</div>
		<div class="v"><?php echo $this->_vars['statistic']['all']['click']; ?>
</div>
	</div>
</div>

<div id="view_blocks">
	<div id="by_month_div">
		<table cellspacing="0" cellpadding="0" class="data" width="100%">
		<tr>
			<th class="first"><?php echo l('stat_month', 'banners', '', 'text', array()); ?></th>
			<th class="w100"><?php echo l('stat_views', 'banners', '', 'text', array()); ?></th>
			<th class="w100"><?php echo l('stat_clicks', 'banners', '', 'text', array()); ?></th>
		</tr>
		<?php if (is_array($this->_vars['statistic']['month']) and count((array)$this->_vars['statistic']['month'])): foreach ((array)$this->_vars['statistic']['month'] as $this->_vars['day'] => $this->_vars['item']): ?>
		<tr>
			<td class="first"><?php echo $this->_vars['item']['month']; ?>
</td>
			<td class="center"><?php echo $this->_vars['item']['view']; ?>
</td>
			<td class="center"><?php echo $this->_vars['item']['click']; ?>
</td>
		</tr>
		<?php endforeach; endif; ?>
		</table>
	</div>
	<div id="by_weeks_div" style="display: none;" >
		<table cellspacing="0" cellpadding="0" class="data" width="100%">
		<tr>
			<th class="first"><?php echo l('stat_week', 'banners', '', 'text', array()); ?></th>
			<th class="w100"><?php echo l('stat_views', 'banners', '', 'text', array()); ?></th>
			<th class="w100"><?php echo l('stat_clicks', 'banners', '', 'text', array()); ?></th>
		</tr>
		<?php if (is_array($this->_vars['statistic']['week']) and count((array)$this->_vars['statistic']['week'])): foreach ((array)$this->_vars['statistic']['week'] as $this->_vars['day'] => $this->_vars['item']): ?>
		<tr>
			<td class="first"><?php echo $this->_vars['item']['start_day']; ?>
 - <?php echo $this->_vars['item']['end_day']; ?>
</td>
			<td class="center"><?php echo $this->_vars['item']['view']; ?>
</td>
			<td class="center"><?php echo $this->_vars['item']['click']; ?>
</td>
		</tr>
		<?php endforeach; endif; ?>
		</table>
	</div>
</div>
<?php endif; ?>

<script>
<?php echo '
function switchView(id){
	$("#view_links a").removeClass(\'active\');
	$("#view_blocks > div").hide();

	$("#"+id).addClass(\'active\');
	$("#"+id+"_div").show();
}
'; ?>

</script>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>