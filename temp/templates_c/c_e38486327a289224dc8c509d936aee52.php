<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2014-07-24 15:42:13 KRAT */ ?>

<table cellpadding="0" cellspacing="0">
	<tr>
		<td rowspan="2">
			<a href="<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'view','data' => $this->_vars['listing']), $this);?>"><img src="<?php echo $this->_vars['listing']['media']['photo']['thumbs']['middle']; ?>
" title="<?php echo $this->_run_modifier($this->_vars['listing']['output_name'], 'escape', 'plugin', 1); ?>
"></a>
		</td>
		<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'status_info')); tpl_block_capture(array('assign' => 'status_info'), null, $this); ob_start();  $this->_tag_stack[] = array('tpl_block_strip', array()); tpl_block_strip(array(), null, $this); ob_start(); ?>
			<?php if ($this->_vars['listing']['is_lift_up'] || $this->_vars['location_filter'] == 'country' && $this->_vars['listing']['is_lift_up_country'] || $this->_vars['location_filter'] == 'region' && ( $this->_vars['listing']['is_lift_up_country'] || $this->_vars['listing']['is_lift_up_region'] ) || $this->_vars['location_filter'] == 'city' && ( $this->_vars['listing']['is_lift_up_country'] || $this->_vars['listing']['is_lift_up_region'] || $this->_vars['listing']['is_lift_up_city'] ) || $this->_vars['sort_data']['order'] != 'default' && ( $this->_vars['listing']['is_lift_up'] || $this->_vars['listing']['is_lift_up_country'] || $this->_vars['listing']['is_lift_up_region'] || $this->_vars['listing']['is_lift_up_city'] )): ?>
				<?php echo l('status_lift_up', 'listings', '', 'text', array()); ?>
			<?php elseif ($this->_vars['listing']['is_featured']): ?>
				<?php echo l('status_featured', 'listings', '', 'text', array()); ?>
			<?php endif; ?>
		<?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_strip($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack);  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
		<td colspan="2">
			<h3><?php echo tpl_function_block(array('name' => listing_price_block,'module' => 'listings','data' => $this->_vars['listing']), $this);?></h3>
			<?php echo $this->_vars['listing']['property_type_str']; ?>
 <?php echo $this->_vars['listing']['operation_type_str']; ?>

			<br><?php echo $this->_run_modifier($this->_vars['listing']['square_output'], 'truncate', 'plugin', 1, 50); ?>

			<?php if ($this->_run_modifier($this->_vars['listing']['date_open'], 'strtotime', 'PHP', 1) >= $this->_vars['tstamp']): ?><br><?php echo l('text_date_open_status', 'listings', '', 'text', array());  endif; ?>
			<?php if ($this->_vars['listing']['sold']): ?><br><span class="status_text"><?php echo l('text_sold', 'listings', '', 'text', array()); ?></span><?php endif; ?>
			<?php if ($this->_vars['status_info']): ?><br><span class="status_text"><?php echo $this->_vars['status_info']; ?>
</span><?php endif; ?>
		</td>
	</tr>
	<?php if ($this->_vars['listing']['user']['status']): ?>
	<tr>
		<td><?php echo $this->_run_modifier($this->_vars['listing']['user']['output_name'], 'truncate', 'plugin', 1, 50); ?>
</td>
		<td>
			<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'user_logo')); tpl_block_capture(array('assign' => 'user_logo'), null, $this); ob_start(); ?>
				<img src="<?php echo $this->_vars['listing']['user']['media']['user_logo']['thumbs']['small']; ?>
" title="<?php echo $this->_run_modifier($this->_vars['listing']['user']['output_name'], 'escape', 'plugin', 1); ?>
">
			<?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
			<?php if ($this->_vars['listing']['user']['status']): ?>
			<a href="<?php echo tpl_function_seolink(array('module' => 'users','method' => 'view','data' => $this->_vars['listing']['user']), $this);?>"><?php echo $this->_vars['user_logo']; ?>
</a>
			<?php else: ?>
			<?php echo $this->_vars['user_logo']; ?>

			<?php endif; ?>
		</td>
	</tr>
	<?php endif; ?>
	<?php if ($this->_vars['listing']['headline']): ?>
	<tr>
		<td colspan="3"><?php echo $this->_run_modifier($this->_vars['listing']['headline'], 'truncate', 'plugin', 1, 100); ?>
</td>
	</tr>	
	<?php endif; ?>
</table>
