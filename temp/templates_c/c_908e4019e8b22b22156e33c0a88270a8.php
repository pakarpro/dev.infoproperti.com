<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.block.php'); $this->register_function("block", "tpl_function_block");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-03-09 12:54:41 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
  $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->module_path. "start". $this->module_templates.  $this->get_current_theme_gid('', '"start"'). "left_panel.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<div class="rc">
	<div class="content-block">
		<h1><?php echo l('header_services', 'users_services', '', 'text', array()); ?></h1>
		<p class="header-comment"><?php echo l('text_services', 'users_services', '', 'text', array()); ?></p>
	</div>
	<?php if ($this->_vars['contact_services']): ?>
		<div class="content-block">
			<h2 class="line top bottom"><?php echo l('header_contact_service', 'users_services', '', 'text', array()); ?></h2>
			<p class="header-comment"><?php echo l('text_contact_service', 'users_services', '', 'text', array()); ?></p>
			<table class="list">
				<tr>
					<th><?php echo l('field_service_name', 'users_services', '', 'text', array()); ?></th>
					<th><?php echo l('field_service_contacts', 'users_services', '', 'text', array()); ?></th>
					<th><?php echo l('field_service_exp_period', 'users_services', '', 'text', array()); ?></th>
					<th><?php echo l('field_amount', 'users_services', '', 'text', array()); ?></th>
					<th class="w50"></th>
				</tr>
				<?php if (is_array($this->_vars['contact_services']) and count((array)$this->_vars['contact_services'])): foreach ((array)$this->_vars['contact_services'] as $this->_vars['item']): ?>
					<tr>
						<td><?php echo $this->_vars['item']['name']; ?>
</td>
						<td><?php if ($this->_vars['item']['data_admin']['contact_count'] > 0):  echo $this->_vars['item']['data_admin']['contact_count'];  else:  echo l('unlim', 'users_services', '', 'text', array());  endif; ?></td>
						<td><?php if ($this->_vars['item']['data_admin']['service_period'] > 0):  echo $this->_vars['item']['data_admin']['service_period']; ?>
 <?php echo l('service_expiration_period', 'users_services', '', 'text', array());  else:  echo l('unlim', 'users_services', '', 'text', array());  endif; ?></td>
						<td><?php echo tpl_function_block(array('name' => currency_format_output,'module' => start,'value' => $this->_vars['item']['price'],'cur_gid' => $this->_vars['base_currency']['gid']), $this);?></td>
						<td class="center">
							<a href="<?php echo $this->_vars['site_url']; ?>
services/form/<?php echo $this->_vars['item']['gid']; ?>
" class="btn-link fright"><ins class="with-icon i-dollar"></ins></a>
						</td>
					</tr>
				<?php endforeach; endif; ?>
			</table>
		</div>
	<?php endif; ?>
	<?php if ($this->_vars['post_services']): ?>
		<div class="content-block">
			<h2 class="line top bottom"><?php echo l('header_post_service_'.$this->_vars['usertype'], 'users_services', '', 'text', array()); ?></h2>
			<p class="header-comment"><?php echo l('text_post_service_'.$this->_vars['usertype'], 'users_services', '', 'text', array()); ?></p>
			<table class="list">
				<tr>
					<th><?php echo l('field_service_name', 'users_services', '', 'text', array()); ?></td>
					<th><?php echo l('field_service_post_'.$this->_vars['usertype'], 'users_services', '', 'text', array()); ?></th>
					<th><?php echo l('field_service_act_period', 'users_services', '', 'text', array()); ?></th>
					<th><?php echo l('field_amount', 'users_services', '', 'text', array()); ?></th>
					<th class="w50"></th>
				</tr>
				<?php if (is_array($this->_vars['post_services']) and count((array)$this->_vars['post_services'])): foreach ((array)$this->_vars['post_services'] as $this->_vars['item']): ?>
					<tr>
						<td><?php echo $this->_vars['item']['name']; ?>
</td>
						<td><?php if ($this->_vars['item']['data_admin']['post_count'] > 0):  echo $this->_vars['item']['data_admin']['post_count'];  else:  echo l('unlim', 'users_services', '', 'text', array());  endif; ?></td>
						<td><?php if ($this->_vars['item']['data_admin']['period'] > 0):  echo $this->_vars['item']['data_admin']['period']; ?>
 <?php echo l('service_expiration_period', 'users_services', '', 'text', array());  else:  echo l('unlim', 'users_services', '', 'text', array());  endif; ?></td>
						<td><?php echo tpl_function_block(array('name' => currency_format_output,'module' => start,'value' => $this->_vars['item']['price'],'cur_gid' => $this->_vars['base_currency']['gid']), $this);?></td>
						<td class="center">
							<a href="<?php echo $this->_vars['site_url']; ?>
services/form/<?php echo $this->_vars['item']['gid']; ?>
" class="btn-link fright"><ins class="with-icon i-dollar"></ins></a>
						</td>
					</tr>
				<?php endforeach; endif; ?>
			</table>
		</div>
	<?php endif; ?>
	<?php if ($this->_vars['combined_services']): ?>
		<div class="content-block">
			<h2 class="line top bottom"><?php echo l('header_combined_service', 'users_services', '', 'text', array()); ?></h2>
			<p class="header-comment"><?php echo l('text_combined_service', 'users_services', '', 'text', array()); ?></p>
			<table class="list">
				<tr>
					<th><?php echo l('field_service_name', 'users_services', '', 'text', array()); ?></th>
					<th><?php echo l('field_service_contacts', 'users_services', '', 'text', array()); ?></th>
					<th><?php echo l('field_service_post_'.$this->_vars['usertype'], 'users_services', '', 'text', array()); ?></th>
					<th><?php echo l('field_amount', 'users_services', '', 'text', array()); ?></th>
					<th class="w50"></th>
				</tr>
				<?php if (is_array($this->_vars['combined_services']) and count((array)$this->_vars['combined_services'])): foreach ((array)$this->_vars['combined_services'] as $this->_vars['item']): ?>
					<tr>
						<td><?php echo $this->_vars['item']['name']; ?>
</td>
						<td>
							<?php if ($this->_vars['item']['data_admin']['contact_count'] > 0):  echo $this->_vars['item']['data_admin']['contact_count'];  else:  echo l('unlim', 'users_services', '', 'text', array());  endif; ?> <?php echo l('contact_count', 'users_services', '', 'text', array()); ?> /
							<?php if ($this->_vars['item']['data_admin']['service_period'] > 0):  echo $this->_vars['item']['data_admin']['service_period'];  else:  echo l('unlim', 'users_services', '', 'text', array());  endif; ?>  <?php echo l('service_expiration_period', 'users_services', '', 'text', array()); ?>
						</td>
						<td>
							<?php if ($this->_vars['item']['data_admin']['post_count'] > 0):  echo $this->_vars['item']['data_admin']['post_count'];  else:  echo l('unlim', 'users_services', '', 'text', array());  endif; ?> <?php echo l('post_count', 'users_services', '', 'text', array()); ?> /
							<?php if ($this->_vars['item']['data_admin']['period'] > 0):  echo $this->_vars['item']['data_admin']['period'];  else:  echo l('unlim', 'users_services', '', 'text', array());  endif; ?> <?php echo l('service_expiration_period', 'users_services', '', 'text', array()); ?>
						</td>
						<td><?php echo tpl_function_block(array('name' => currency_format_output,'module' => start,'value' => $this->_vars['item']['price'],'cur_gid' => $this->_vars['base_currency']['gid']), $this);?></td>
						<td class="center">
							<a href="<?php echo $this->_vars['site_url']; ?>
services/form/<?php echo $this->_vars['item']['gid']; ?>
" class="btn-link fright"><ins class="with-icon i-dollar"></ins></a>
						</td>
					</tr>
				<?php endforeach; endif; ?>
			</table>
		</div>
	<?php endif; ?>
</div>
<div class="clr"></div>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
