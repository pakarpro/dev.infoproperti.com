<?php require_once('/opt/ip/system/libraries/template_lite/plugins/function.counter.php'); $this->register_function("counter", "tpl_function_counter");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-02 14:30:54 KRAT */ ?>

	<table cellspacing="0" cellpadding="0" class="data" width="100%">
	<tr>
		<th class="first" colspan="2"><?php echo l('stat_header_moderation', 'moderation', '', 'text', array()); ?></th>
	</tr>
	<?php if ($this->_vars['stat_moderation']['index_method']): ?>
	<?php if (is_array($this->_vars['stat_moderation']['types']) and count((array)$this->_vars['stat_moderation']['types'])): foreach ((array)$this->_vars['stat_moderation']['types'] as $this->_vars['item']): ?>
	<?php $this->assign('type_gid', 'type_'.$this->_vars['item']['name']); ?>
	<?php echo tpl_function_counter(array('print' => false,'assign' => counter), $this);?>
	<tr <?php if (!($this->_vars['counter'] % 2)): ?>class="zebra"<?php endif; ?>>
		<td class="first"><a href="<?php echo $this->_vars['site_url']; ?>
admin/moderation/index/"><?php echo l('mtype_'.$this->_vars['item']['name'], 'moderation', '', 'text', array()); ?></a></td>
		<td class="w50 center"><a href="<?php echo $this->_vars['site_url']; ?>
admin/moderation/index/"><?php echo $this->_vars['stat_moderation'][$this->_vars['type_gid']]; ?>
</a></td>
	</tr>
	<?php endforeach; endif; ?>
	<?php endif; ?>
	</table>

