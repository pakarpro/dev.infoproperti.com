<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.escape.php'); $this->register_modifier("escape", "tpl_modifier_escape");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2014-08-28 10:22:35 KRAT */ ?>

		<?php if (is_array($this->_vars['settings']) and count((array)$this->_vars['settings'])): foreach ((array)$this->_vars['settings'] as $this->_vars['key'] => $this->_vars['item']): ?>
		<div class="row">
			<div class="h"><?php echo l('field_'.$this->_vars['driver_gid'].'_'.$this->_vars['item']['name'], 'import', '', 'text', array()); ?>: </div>
			<div class="v">
				<?php if ($this->_vars['item']['type'] == 'checkbox'): ?>
				<input type="checkbox" name="data[settings][<?php echo $this->_vars['item']['name']; ?>
]]" value="1" <?php if ($this->_vars['item']['default']): ?>checked<?php endif; ?>>
				<?php else: ?>
				<input type="text" name="data[settings][<?php echo $this->_vars['item']['name']; ?>
]]" value="<?php echo $this->_run_modifier($this->_vars['item']['default'], 'escape', 'plugin', 1); ?>
">
				<?php endif; ?>
			</div>
		</div>
		<?php endforeach; endif; ?>
