<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.date_format.php'); $this->register_modifier("date_format", "tpl_modifier_date_format");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2014-09-16 16:20:49 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<div class="actions">
	<ul>
		<li><div class="l"><a href="<?php echo $this->_vars['site_url']; ?>
admin/users/deactivated_alerts_delete/<?php echo $this->_vars['data']['id']; ?>
" onclick="javascript: if(!confirm('<?php echo l('note_deactivated_alerts_delete', 'users', '', 'js', array()); ?>')) return false;"><?php echo l('btn_delete', 'start', '', 'text', array()); ?></a></div></li>
	</ul>
	&nbsp;
</div>
<div class="edit-form n150">
	<div class="row header"><?php echo l('admin_header_deactivated_alerts_show', 'users', '', 'text', array()); ?></div>
	<div class="row">
		<div class="h"><?php echo l('field_deactivated_alert_name', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><?php echo $this->_vars['data']['name']; ?>
</div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_deactivated_alert_email', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><?php echo $this->_vars['data']['email']; ?>
</div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_deactivated_alert_phone', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><?php echo $this->_vars['data']['phone']; ?>
</div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_deactivated_alert_reason', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><?php echo $this->_vars['data']['reason']; ?>
</div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_deactivated_message', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><?php echo $this->_vars['data']['message']; ?>
</div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_deactivated_alert_date_add', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><?php echo $this->_run_modifier($this->_vars['data']['date_add'], 'date_format', 'plugin', 1, $this->_vars['date_format']); ?>
</div>
	</div>
</div>
<a class="cancel" href="<?php echo $this->_vars['site_url']; ?>
admin/users/deactivated_alerts/<?php echo $this->_vars['filter']; ?>
"><?php echo l('btn_cancel', 'start', '', 'text', array()); ?></a>
<div class="clr"></div>
<script><?php echo '
	$(function(){
		$("div.row:odd").addClass("zebra");
	});
'; ?>
</script>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
