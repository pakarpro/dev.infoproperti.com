<?php require_once('/opt/ip/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  require_once('/opt/ip/system/libraries/template_lite/plugins/block.strip.php'); $this->register_block("strip", "tpl_block_strip");  require_once('/opt/ip/system/libraries/template_lite/plugins/block.capture.php'); $this->register_block("capture", "tpl_block_capture");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.block.php'); $this->register_function("block", "tpl_function_block");  require_once('/opt/ip/system/libraries/template_lite/plugins/modifier.truncate.php'); $this->register_modifier("truncate", "tpl_modifier_truncate");  require_once('/opt/ip/system/libraries/template_lite/plugins/modifier.escape.php'); $this->register_modifier("escape", "tpl_modifier_escape");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.seolink.php'); $this->register_function("seolink", "tpl_function_seolink");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-02-26 11:13:18 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array('load_type' => 'ui'));
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<?php if ($this->_vars['listing']['price']): ?>
	<input type="hidden" value ="<?php echo $this->_vars['listing']['price']; ?>
" id="listing_price" />
<?php endif; ?>

<?php if ($this->_vars['bank']): ?>
	<input type="hidden" value ="<?php echo $this->_vars['bank']; ?>
" id="bank" />
<?php endif; ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>
<?php echo '
$(document).ready(function(){
function numberWithCommas(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\\B(?=(\\d{3})+(?!\\d))/g, ",");
    return parts.join(".");
}
	//serves as a timer delay functions
	var delay = (function(){
	  var timer = 0;
	  return function(callback, ms){
		clearTimeout (timer);
		timer = setTimeout(callback, ms);
	  };
	})();
	function loader(){
		$(\'#tabletest\').append(
		"<thead>"+
		"<tr>"+
			"<th align=\'center\' colspan=\'2\'><img src=\'';  echo $this->_vars['base_url'];  echo '/application/modules/listings/views/default/css/loader.gif\' style=\'border:none;\'/></th>"+
		"</tr>"+
		"</thead>"
		);
		
	}
	function kpr_table(){
		var propertiID=$(\'#properti\').val();
		var promosiID=$(\'#promosi\').val();
		var bank=$(\'#bank\').val();
		$(\'#tabletest\').empty();
		loader();
		$.post("listings-kpr_get_bank",{ propertiID: propertiID, promosiID: promosiID, bank: bank}, function(json) {
			$(\'#tabletest\').empty();
			var data = JSON.parse(json);
			i=0;
			$(\'#tabletest\').append(
			"<thead>"+
			"<tr>"+
				"<th style=\'background-color:#3a5a98; color:white;\'>Masa Promosi</td>"+
				"<th style=\'background-color:#3a5a98; color:white;\'>Masa pinjaman</td>"+
				"<th style=\'background-color:#3a5a98; color:white;\'>Bunga</td>"+				
				"<th style=\'background-color:#3a5a98; color:white;\'>Angsuran per Bulan</td>"+
			"</tr>"+
			"</thead>"
			);
			if(data)
			{
			$.each(data, function(){
				var pinjaman_get = $(\'#sisapinjaman\').val();
				var pinjaman_comma = parseFloat(pinjaman_get.replace(/,/g, \'\'))
				var pinjaman_pokok = pinjaman_comma;
				var lama_pinjaman = $(\'#masa_pinjaman\').val();
				/*
				var cicilan_pokok = pinjaman_pokok/(lama_pinjaman*12);
				var cicilan_pokok_rounded = (cicilan_pokok).toFixed(2);
				var bunga_bulan = (pinjaman_pokok*data[i].bunga/12/100).toFixed(2);
				var cicilan_bulan = (parseFloat(bunga_bulan)+parseFloat(cicilan_pokok_rounded)).toFixed(2);
				*/
				var tenor = lama_pinjaman*12;
				var bunga_bulan = data[i].bunga/12/100;
				var step1 = 1+bunga_bulan;
				var step2 = Math.pow(step1, -tenor);
				var step3 = 1-step2;
				var step4 = bunga_bulan/step3;
				var step5 = pinjaman_pokok*step4;
				var cicilan_bulan = parseFloat(step5).toFixed(2);
				
				var image_check =  data[i].logo;
				if(i%2==0){
					if(image_check!=\'\')
					{
			//$base_url only works if it\'s placed outside of the literal tags
						$(\'#tabletest\').append(
						"<tr>"+
							"<td style=\'background-color:#ebedfa; text-align:center;\'><font style=\'font-size:24px; font-style:italic;\'>"+data[i].periode+"</font> Tahun</td>"+
							"<td style=\'background-color:#ebedfa; text-align:center;\'><font style=\'font-size:24px; font-style:italic;\'>"+lama_pinjaman+"</font> Tahun</td>"+
							"<td id=\'bungabank"+i+"\' style=\'background-color:#ebedfa; text-align:center;\'><font style=\'font-size:24px; color:red; font-style:italic;\'>"+data[i].bunga+"</font> %</td>"+							
							"<td style=\'background-color:#ebedfa; text-align:center;\'><font style=\'font-size:24px; color:#1C51FF; font-style:italic;\'>Rp. "+numberWithCommas(cicilan_bulan)+"</font></td>"+
						"</tr>"
						);
					}
					else
					{
						$(\'#tabletest\').append(
						"<tr>"+
							"<td style=\'background-color:#ebedfa; text-align:center;\'><font style=\'font-size:24px; font-style:italic;\'>"+data[i].periode+"</font> Tahun</td>"+
							"<td style=\'background-color:#ebedfa; text-align:center;\'><font style=\'font-size:24px; font-style:italic;\'>"+lama_pinjaman+"</font> Tahun</td>"+
							"<td id=\'bungabank"+i+"\' style=\'background-color:#ebedfa; text-align:center;\'><font style=\'font-size:24px; color:red; font-style:italic;\'>"+data[i].bunga+"</font> %</td>"+							
							"<td style=\'background-color:#ebedfa; text-align:center;\'><font style=\'font-size:24px; color:#1C51FF; font-style:italic;\'>Rp. "+numberWithCommas(cicilan_bulan)+"</font></td>"+
						"</tr>"
						);
					}
				i++;
				}
				else
				{
					if(image_check!=\'\')
					{
						$(\'#tabletest\').append(
						"<tr>"+
							"<td style=\'background-color:white; text-align:center;\'><font style=\'font-size:24px; font-style:italic;\'>"+data[i].periode+"</font> Tahun</td>"+
							"<td style=\'background-color:white; text-align:center;\'><font style=\'font-size:24px; font-style:italic;\'>"+lama_pinjaman+"</font> Tahun</td>"+
							"<td id=\'bungabank"+i+"\' style=\'background-color:white; text-align:center;\'><font style=\'font-size:24px; color:red; font-style:italic;\'>"+data[i].bunga+"</font> %</td>"+							
							"<td style=\'background-color:white; text-align:center;\'><font style=\'font-size:24px; color:#1C51FF; font-style:italic;\'>Rp. "+numberWithCommas(cicilan_bulan)+"</font></td>"+
						"</tr>"
						);
					}
					else
					{
						$(\'#tabletest\').append(
						"<tr>"+
							"<td style=\'background-color:white; text-align:center;\'><font style=\'font-size:24px; font-style:italic;\'>"+data[i].periode+"</font> Tahun</td>"+
							"<td style=\'background-color:white; text-align:center;\'><font style=\'font-size:24px; font-style:italic;\'>"+lama_pinjaman+"</font> Tahun</td>"+
							"<td id=\'bungabank"+i+"\' style=\'background-color:white; text-align:center;\'><font style=\'font-size:24px; color:red; font-style:italic;\'>"+data[i].bunga+"</font> %</td>"+							
							"<td style=\'background-color:white; text-align:center;\'><font style=\'font-size:24px; color:#1C51FF; font-style:italic;\'>Rp. "+numberWithCommas(cicilan_bulan)+"</font></td>"+
						"</tr>"
						);
					}
				i++;
				}
			});
			}
			else
			{
					$(\'#tabletest\').append(
					"<tr>"+
						"<td align=\'center\' style=\'background-color:#ebedfa;\' colspan=\'4\'><font style=\'font-size:18px; color:#1C51FF; font-style:italic;\'>For special rate, please contact our representative.</font></td>"+
					"</tr>"
					);
			}			
		});
	
			
	}
	// RUN BY DEFAULT
	//kpr_table();
	// START OPTIONAL FUNCTIONS
	$(\'#properti\').change(function(){
		kpr_table();
	});
	$(\'#promosi\').change(function(){
		kpr_table();
	});
	
	$(\'#masa_pinjaman\').change(function(){
		kpr_table();		
	});
	
	//added mod support to be able to directly output KPR calculation based on listings from Infoproperti
	if($(\'#listing_price\').val()!=\'\')
	{
		$(\'#pinjaman\').val($(\'#listing_price\').val());
		$(\'#check_pinjaman\').hide();
		$(\'#pinjaman_check\').empty();
		var check_dp = $(\'#dp\').val();
		var check_pinjaman = $(\'#pinjaman\').val();
		//$(\'#pinjaman_check\').val(print_pinjaman);
			if(($.isNumeric(check_dp)==true) && ($.isNumeric(check_pinjaman)==true)){
				//append commas to a span below the input box
				var pinjaman_comma = numberWithCommas(check_pinjaman);
				var print_pinjaman = \'Rp. \'+pinjaman_comma;
				$(\'#pinjaman_check\').append(print_pinjaman);
				
				
				var pinjaman = $(\'#pinjaman\').val();
				var dp = $(\'#dp\').val();
				var sisapinjaman = pinjaman-(pinjaman*dp/100);
				var test_sisapinjaman = numberWithCommas(sisapinjaman)
				$(\'#sisapinjaman\').val(test_sisapinjaman);	
					
				$(\'#tabletest\').empty();
				delay(function(){
					kpr_table();
				}, 2000);
				$(\'#pinjaman\').val(pinjaman_comma);
			}
			else if($.isNumeric(check_pinjaman)==false&&check_pinjaman!=\'\'){
				$(\'#check_pinjaman\').show();
			}
	}

	$(\'#pinjaman\').keyup(function(){
	$(\'#check_pinjaman\').hide();
	$(\'#pinjaman_check\').empty();
	var check_dp = $(\'#dp\').val();
	var check_pinjaman = $(\'#pinjaman\').val();
	//$(\'#pinjaman_check\').val(print_pinjaman);
		if(($.isNumeric(check_dp)==true) && ($.isNumeric(check_pinjaman)==true)){
			//append commas to a span below the input box
			var pinjaman_comma = numberWithCommas(check_pinjaman);
			var print_pinjaman = \'Rp. \'+pinjaman_comma;
			$(\'#pinjaman_check\').append(print_pinjaman);
			
			
			var pinjaman = $(\'#pinjaman\').val();
			var dp = $(\'#dp\').val();
			var sisapinjaman = pinjaman-(pinjaman*dp/100);
			var test_sisapinjaman = numberWithCommas(sisapinjaman)
			$(\'#sisapinjaman\').val(test_sisapinjaman);	
				
			$(\'#tabletest\').empty();
			delay(function(){
				kpr_table();
			}, 2000);
		}
		else if($.isNumeric(check_pinjaman)==false&&check_pinjaman!=\'\'){
		 	$(\'#check_pinjaman\').show();
		}
	});
	
	$(\'#pinjaman\').blur(function(){
		var pinjaman_check = $(\'#pinjaman\').val();
		var pinjaman_comma = numberWithCommas(pinjaman_check);
		$(\'#pinjaman\').val(pinjaman_comma);
	});
	
	$(\'#pinjaman\').focus(function(){
		var pinjaman_check = $(\'#pinjaman\').val();
		var pinjaman_int = parseFloat(pinjaman_check.replace(/,/g, \'\'))
		if(pinjaman_check==\'\')
		{
			$(\'#pinjaman\').val(\'\');
		}
		else
		{
			$(\'#pinjaman\').val(pinjaman_int);
		}
	});
	
	$(\'#dp\').keyup(function(){
	$(\'#check_dp\').hide();	
	var check_dp = $(\'#dp\').val();
	var check_pinjaman = $(\'#pinjaman\').val();
	var pinjaman = parseFloat(check_pinjaman.replace(/,/g, \'\'))
		if(($.isNumeric(check_dp)==true) && ($.isNumeric(pinjaman)==true)){
			
			//var pinjaman = $(\'#pinjaman\').val();
			var dp = $(\'#dp\').val();
			var sisapinjaman = pinjaman-(pinjaman*dp/100);
			var sisapinjaman_comma = numberWithCommas(sisapinjaman);
			$(\'#sisapinjaman\').val(sisapinjaman_comma);	
				
			$(\'#tabletest\').empty();
			delay(function(){
				kpr_table();
			}, 1000);
		}
		else if($.isNumeric(check_dp)==false){
		 	$(\'#check_dp\').show();
		}
		
	});
});
'; ?>

</script>
<div class="kpr_wrapper">

<form>
<div class="kpr_header">Kalkulasi Cicilan KPR</div>
<div style="position:relative;width:250px; float:left;"></div>
<div class="kpr_input" style="position:relative; float:left;">

	<table style="width:980px;"  cellpadding="5" cellspacing="0" >
    	<tr>
        	<td colspan="2">
            	KPR Bank <?php echo $this->_vars['bank_detail']['name']; ?>

            </td>
            <td rowspan="6" valign="top">
		<div class="listing-block <?php if ($this->_vars['listing']['is_highlight']): ?>highlight<?php endif; ?>" >
			<div id="item-block-<?php echo $this->_vars['listing']['id']; ?>
" class="item listing <?php if ($this->_vars['listing']['is_highlight']): ?>highlight<?php endif; ?>">
				<h3 class="listing-heading search-result-entry-heading">
                <a href="<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'view','data' => $this->_vars['listing']), $this);?>" title="<?php echo $this->_run_modifier($this->_vars['listing']['output_name'], 'escape', 'plugin', 1); ?>
"><?php echo $this->_run_modifier($this->_vars['listing']['output_name'], 'truncate', 'plugin', 1, 50); ?>
</a>
                <div style="position:relative; float:right; margin-top:-5px; display:block;">
                <?php if ($this->_vars['listing']['review_sorter'] != 0): ?>                
                                    <?php echo tpl_function_block(array('name' => get_rate_block,'module' => reviews,'rating_data_main' => $this->_vars['listing']['review_sorter'],'type_gid' => 'listings_object','template' => 'normal','read_only' => 'true'), $this);?>
				                <?php endif; ?>
                </div>
				</h3>
                <!-- #MOD FOR REVIEW STARS ON LISTINGS LIST -->
                <!-- #END OF MOD# -->
				<div class="image">
					<a href="<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'view','data' => $this->_vars['listing']), $this);?>">
						<?php if ($this->_vars['animate_available'] && $this->_vars['listing']['is_slide_show']): ?>
						<img src="<?php echo $this->_vars['listing']['media']['photo']['thumbs']['middle_anim']; ?>
" title="<?php echo $this->_run_modifier($this->_vars['listing']['output_name'], 'escape', 'plugin', 1); ?>
">
						<?php else: ?>
						<img src="<?php echo $this->_vars['listing']['media']['photo']['thumbs']['middle']; ?>
" title="<?php echo $this->_run_modifier($this->_vars['listing']['output_name'], 'escape', 'plugin', 1); ?>
">
						<?php endif; ?>
						<?php if ($this->_vars['listing']['photo_count'] || $this->_vars['listing']['is_vtour']): ?>
						<div class="photo-info">
							<div class="panel">
								<?php if ($this->_vars['listing']['photo_count']): ?><span class="btn-link"><ins class="with-icon-small w i-photo"></ins> <?php echo $this->_vars['listing']['photo_count']; ?>
</span><?php endif; ?>
								<?php if ($this->_vars['listing']['is_vtour']): ?><span class="btn-link"><ins class="with-icon-small w i-vtour"></ins> 360&deg;</span><?php endif; ?>
							</div>
							<div class="background"></div>
						</div>
						<?php endif; ?>
					</a>
				</div>
		
				<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'status_info')); tpl_block_capture(array('assign' => 'status_info'), null, $this); ob_start();  $this->_tag_stack[] = array('tpl_block_strip', array()); tpl_block_strip(array(), null, $this); ob_start(); ?>
					<?php if ($this->_vars['listing']['is_lift_up'] || $this->_vars['location_filter'] == 'country' && $this->_vars['listing']['is_lift_up_country'] || $this->_vars['location_filter'] == 'region' && ( $this->_vars['listing']['is_lift_up_country'] || $this->_vars['listing']['is_lift_up_region'] ) || $this->_vars['location_filter'] == 'city' && ( $this->_vars['listing']['is_lift_up_country'] || $this->_vars['listing']['is_lift_up_region'] || $this->_vars['listing']['is_lift_up_city'] ) || $this->_vars['sort_data']['order'] != 'default' && ( $this->_vars['listing']['is_lift_up'] || $this->_vars['listing']['is_lift_up_country'] || $this->_vars['listing']['is_lift_up_region'] || $this->_vars['listing']['is_lift_up_city'] )): ?>
						<?php echo l('status_lift_up', 'listings', '', 'text', array()); ?>
					<?php elseif ($this->_vars['listing']['is_featured']): ?>
						<?php echo l('status_featured', 'listings', '', 'text', array()); ?>
					<?php endif; ?>
				<?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_strip($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack);  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
				
				<div class="body clearfix">
					<div class="prop-info">
						<h3 class="price-highlight"><?php echo tpl_function_block(array('name' => listing_price_block,'module' => 'listings','data' => $this->_vars['listing']), $this);?></h3>
						<div class="t-1">
							<?php echo $this->_vars['listing']['property_type_str']; ?>
 <?php echo $this->_vars['listing']['operation_type_str']; ?>
<br />
							<?php echo l('field_square', 'listings', '', 'text', array()); ?> : <?php echo $this->_run_modifier($this->_vars['listing']['square_output'], 'truncate', 'plugin', 1); ?>

							<ul class="icon-stats">
							<?php if (is_array($this->_vars['residential']) and count((array)$this->_vars['residential'])): foreach ((array)$this->_vars['residential'] as $this->_vars['residen']): ?>

								<?php if ($this->_vars['listing']['id'] == $this->_vars['residen']['id_listing']): ?>
									
									<?php if ($this->_vars['residen']['fe_bd_rooms_1'] != '0'): ?>	 
										<li class="listing-icon bedroom"> : <?php echo $this->_vars['residen']['fe_bd_rooms_1']; ?>
</li>
									<?php endif; ?>
									
									<?php if ($this->_vars['residen']['fe_bth_rooms_1'] != '0'): ?>
										<li class="listing-icon bathroom"> : <?php echo $this->_vars['residen']['fe_bth_rooms_1']; ?>
</li>
									<?php endif; ?>
									
									<?php if ($this->_vars['residen']['fe_garages_1'] != '0'): ?>
										<li class="listing-icon garages"> : <?php echo $this->_vars['residen']['fe_garages_1']; ?>
</li>
									<?php endif; ?>
								<?php endif; ?>
							<?php endforeach; endif; ?>
							
							<?php if (is_array($this->_vars['commercial']) and count((array)$this->_vars['commercial'])): foreach ((array)$this->_vars['commercial'] as $this->_vars['commer']): ?>
								<?php if ($this->_vars['listing']['id'] == $this->_vars['commer']['id_listing']): ?>
									<?php if ($this->_vars['commer']['fe_year_2'] != '0'): ?>	 
										<li class="listing-icon year">Year : <?php echo $this->_vars['commer']['fe_year_2']; ?>
</li>
									<?php endif; ?>
									
									<?php if ($this->_vars['commer']['fe_foundation_2'] != '0'): ?>
										<li class="listing-icon foundation">Foundation : <?php echo $this->_vars['commer']['fe_foundation_2']; ?>
</li>
									<?php endif; ?>
								<?php endif; ?>
							<?php endforeach; endif; ?>
							</ul>
				<?php if ($this->_vars['listing']['headline']): ?><p class="headline" title="<?php echo $this->_vars['listing']['headline']; ?>
"><?php echo $this->_run_modifier($this->_vars['listing']['headline'], 'truncate', 'plugin', 1, 100); ?>
</p>
					<?php else: ?><p class="headline">&nbsp;</p>
				<?php endif; ?>
                            
						</div>
					</div>
				</div>
				<div class="clr"></div>
			</div>
	</div>
            </td>
        </tr>
    	<tr>
        	<td colspan="2">
            	<img src="<?php echo $this->_vars['base_url']; ?>
ext/kpr_cms/images/logo/<?php echo $this->_vars['bank_detail']['logo']; ?>
" style="height:100px; border:0px;" />
            </td>
        </tr>
    	<tr>
        	<td>
            Harga Properti <font color="red"><span id="check_pinjaman" style="display:none;">Must be Numeric!</span></font><br/>
            <input name="pinjaman" id="pinjaman" type="text" style="width:100%;" value="0" fcsa-number/>
            </td>
            <td>
            Down Payment (Uang Muka) <font color="red"><span id="check_dp" style="display:none;">Must be Numeric!</span></font><br/>
            <input name="dp" id="dp" type="text" value="30" size="2" />%
            </td>
        </tr>
        <tr>
        	<td><font color="#ae8d6a"><span id="pinjaman_check"></span></font></td>
            <td></td>
        </tr>
        <tr>
        	<td>
            Pinjaman<br/>
            <input name="sisapinjaman" id="sisapinjaman" value="0"  type="text" disabled="disabled" style="width:100%; background-color:#E6E6E6; cursor:not-allowed;"/>
            </td>
            <td></td>
        </tr>
        <tr>
        	<td colspan="2">
            <table cellspacing="10">
                <tr>
                    <td>Tipe Properti</td>
                    <td>Lama Pinjaman</td>
            	<?php if (! $this->_vars['bank']): ?>
                    <td>Masa Promosi</td>
                <?php endif; ?>
                </tr>
                <tr>
                    <td>
                    <select name="properti" id="properti">
                    <?php if (is_array($this->_vars['properti']) and count((array)$this->_vars['properti'])): foreach ((array)$this->_vars['properti'] as $this->_vars['item']): ?>
                    <option value="<?php echo $this->_vars['item']->propertiID; ?>
" <?php if ($this->_vars['item']->propertiID == $this->_vars['propertiID']): ?>selected="selected"<?php endif; ?>><?php echo $this->_vars['item']->namaproperti; ?>
</option>
                    <?php endforeach; endif; ?>
                    </select>
                    </td>
                    <td>
                    <select name="masa_pinjaman" id="masa_pinjaman" >
                    <?php if (is_array($this->_vars['pinjaman']) and count((array)$this->_vars['pinjaman'])): foreach ((array)$this->_vars['pinjaman'] as $this->_vars['item']): ?>
                    <option value="<?php echo $this->_vars['item']->lamapinjaman; ?>
"><?php echo $this->_vars['item']->lamapinjaman; ?>
 Tahun</option>
                    <?php endforeach; endif; ?>
                    </select>
                    </td>
                <!-- show this option on normal KPR page, hide this when it's the special kpr page -->
            	<?php if (! $this->_vars['bank']): ?>
                    <td>
                    <select name="promosi" id="promosi">
                    <?php if (is_array($this->_vars['promosi']) and count((array)$this->_vars['promosi'])): foreach ((array)$this->_vars['promosi'] as $this->_vars['item']): ?>
                    <option value="<?php echo $this->_vars['item']->promosiID; ?>
"><?php echo $this->_vars['item']->periode; ?>
 Tahun</option>
                    <?php endforeach; endif; ?>
                    </select>
                    </td>
               	<?php endif; ?>
                </tr>
            
            </table>
            </td>
            
        </tr>
    </table>
</div>
	<!-- top part form -->
    <br/><br/>
</form>      
    <table id="tabletest" align="center" style="width:100%; margin-bottom:25px; margin-top:25px; border:1px black solid;"  cellpadding="10" cellspacing="0">
    
    </table>
    

</div>


<?php if ($this->_vars['bank']): ?>
	<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array('load_type' => 'ui'));
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<?php endif; ?>