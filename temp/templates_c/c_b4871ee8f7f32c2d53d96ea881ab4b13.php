<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-05-12 14:12:19 KRAT */ ?>

	<?php echo tpl_function_js(array('file' => 'jcarousellite.min.js'), $this);?>
	<?php echo tpl_function_js(array('file' => 'init_carousel_controls.js'), $this);?>
	<script><?php echo '
		$(function(){
			var rtl = \'rtl\' === \'';  echo $this->_vars['_LANG']['rtl'];  echo '\';
			var idPrev, idNext;
			if(!rtl){
				idNext = \'#directionright';  echo $this->_vars['similar_rand'];  echo '\';
				idPrev = \'#directionleft';  echo $this->_vars['similar_rand'];  echo '\';
			}else{
				idNext = \'#directionleft';  echo $this->_vars['similar_rand'];  echo '\';
				idPrev = \'#directionright';  echo $this->_vars['similar_rand'];  echo '\';
			};
			$(\'.carousel_block';  echo $this->_vars['similar_rand'];  echo '\').jCarouselLite({
				rtl: rtl,
				visible: ';  echo $this->_vars['similar_visible'];  echo ',
				btnNext: idNext,
				btnPrev: idPrev,
				circular: false,
				afterEnd: function(a){
					var index = $(a[0]).index();
					carousel_controls';  echo $this->_vars['similar_rand'];  echo '.update_controls(index);
				}
			});

			carousel_controls';  echo $this->_vars['similar_rand'];  echo ' = new init_carousel_controls({
				rtl: rtl,
				carousel_images_count: ';  echo $this->_vars['similar_visible'];  echo ',
				carousel_total_images: ';  echo $this->_vars['similar_total'];  echo ',
				btnNext: idNext,
				btnPrev: idPrev
			});
		});
	</script>'; ?>

	<div id="similar_listings">
		<h2><?php echo l('header_similar_listings', 'listings', '', 'text', array()); ?></h2>
		<div class="carousel <?php if ($this->_vars['similar_total'] <= $this->_vars['similar_visible']): ?>visible<?php endif; ?>">
			<div id="directionleft<?php echo $this->_vars['similar_rand']; ?>
" class="directionleft">
				<div class="with-icon i-larr w" id="l_hover"></div>
			</div>
			<div class="carousel_block carousel_block<?php echo $this->_vars['similar_rand']; ?>
 item_<?php echo $this->_vars['similar_visible']; ?>
_info">
				<ul>
					<?php if (is_array($this->_vars['similar_listings']) and count((array)$this->_vars['similar_listings'])): foreach ((array)$this->_vars['similar_listings'] as $this->_vars['key'] => $this->_vars['item']): ?>
					<li>
						<div class="listing">
							<a href="<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'view','data' => $this->_vars['item']), $this);?>"><img src="<?php echo $this->_vars['item']['media']['photo']['thumbs']['middle']; ?>
" alt="<?php echo $this->_run_modifier($this->_run_modifier($this->_vars['item']['output_name'], 'truncate', 'plugin', 1, 30), 'escape', 'plugin', 1); ?>
" title="<?php echo $this->_run_modifier($this->_vars['item']['output_name'], 'escape', 'plugin', 1); ?>
" /></a>
							<a href="<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'view','data' => $this->_vars['item']), $this);?>"><?php echo $this->_run_modifier($this->_vars['item']['output_name'], 'truncate', 'plugin', 1, 30); ?>
</a>
							<span><?php echo $this->_vars['item']['property_type_str']; ?>
 <?php echo $this->_vars['item']['operation_type_str']; ?>
</span>
							<span><?php echo tpl_function_block(array('name' => listing_price_block,'module' => 'listings','data' => $this->_vars['item'],'template' => 'small'), $this);?></span>
						</div>
					</li>
					<?php endforeach; endif; ?>
				</ul>
			</div>
			<div id="directionright<?php echo $this->_vars['similar_rand']; ?>
" class="directionright">
				<div class="with-icon i-rarr w" id="r_hover"></div>
			</div>
			<div class="clr"></div>
		</div>
	</div>
	
