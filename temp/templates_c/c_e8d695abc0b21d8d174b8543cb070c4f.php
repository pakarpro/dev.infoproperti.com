<?php require_once('/opt/ip/system/libraries/template_lite/plugins/function.js.php'); $this->register_function("js", "tpl_function_js");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.user_select.php'); $this->register_function("user_select", "tpl_function_user_select");  require_once('/opt/ip/system/libraries/template_lite/plugins/modifier.escape.php'); $this->register_modifier("escape", "tpl_modifier_escape");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-02-26 13:24:46 KRAT */ ?>

<form action="" method="post" enctype="multipart/form-data">
<div class="edit-form n150">
<div class="row header"><?php if ($this->_vars['data']['id']):  echo l('admin_header_users_change', 'users', '', 'text', array());  else:  echo l('admin_header_users_add', 'users', '', 'text', array());  endif; ?></div>
	<div class="row">
		<div class="h"><?php echo l('field_user_type', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><b><?php echo $this->_vars['data']['user_type_str']; ?>
</b></div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_email', 'users', '', 'text', array()); ?>:&nbsp;* </div>
		<div class="v"><input type="text" name="data[email]" value="<?php echo $this->_run_modifier($this->_vars['data']['email'], 'escape', 'plugin', 1); ?>
"></div>
	</div>
	<?php if ($this->_vars['data']['id']): ?>
	<div class="row">
		<div class="h"><?php echo l('field_change_password', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><input type="checkbox" value="1" name="update_password" id="pass_change_field"></div>
	</div>
	<?php endif; ?>
	<div class="row">
		<div class="h"><?php echo l('field_password', 'users', '', 'text', array()); ?>:<?php if (! $this->_vars['data']['id']): ?>&nbsp;*<?php endif; ?> </div>
		<div class="v"><input type="password" name="data[password]" id="pass_field" <?php if ($this->_vars['data']['id']): ?>disabled<?php endif; ?>></div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_repassword', 'users', '', 'text', array()); ?>:<?php if (! $this->_vars['data']['id']): ?>&nbsp;*<?php endif; ?> </div>
		<div class="v"><input type="password" name="data[repassword]" id="repass_field" <?php if ($this->_vars['data']['id']): ?>disabled<?php endif; ?>></div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_fname', 'users', '', 'text', array()); ?>:&nbsp;* </div>
		<div class="v"><input type="text" name="data[fname]" value="<?php echo $this->_run_modifier($this->_vars['data']['fname'], 'escape', 'plugin', 1); ?>
"></div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_sname', 'users', '', 'text', array()); ?>:&nbsp;* </div>
		<div class="v"><input type="text" name="data[sname]" value="<?php echo $this->_run_modifier($this->_vars['data']['sname'], 'escape', 'plugin', 1); ?>
"></div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_phone', 'users', '', 'text', array()); ?>:&nbsp;* </div>
		<div class="v"><input type="text" name="data[phone]" value="<?php echo $this->_run_modifier($this->_vars['data']['phone'], 'escape', 'plugin', 1); ?>
" class="phone-field"></div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_icon', 'users', '', 'text', array()); ?>: </div>
		<div class="v">
			<input type="file" name="user_icon">
			<?php if ($this->_vars['data']['user_logo'] || $this->_vars['data']['user_logo_moderation']): ?>
			<br><input type="checkbox" name="user_icon_delete" value="1" id="uichb"><label for="uichb"><?php echo l('field_icon_delete', 'users', '', 'text', array()); ?></label><br>
			<?php if ($this->_vars['data']['user_logo_moderation']): ?><img src="<?php echo $this->_vars['data']['media']['user_logo_moderation']['thumbs']['middle']; ?>
"><?php else: ?><img src="<?php echo $this->_vars['data']['media']['user_logo']['thumbs']['middle']; ?>
"><?php endif; ?>
			<?php endif; ?>
		</div>
	</div>
	
	<div class="row">
		<div class="h"><?php echo l('field_company', 'users', '', 'text', array()); ?>: </div>
		<div class="v">
			<?php if ($this->_vars['data']['agent_company']): ?>				
				<?php if ($this->_vars['data']['agent_status']): ?>
					<b><?php echo $this->_vars['data']['company']['output_name']; ?>
</b>&nbsp; <a href="<?php echo $this->_vars['site_url']; ?>
admin/users/agent_get_out/<?php echo $this->_vars['data']['id']; ?>
"><?php echo l('link_agent_get_out', 'users', '', 'text', array()); ?></a>
				<?php else: ?>
					<b><?php echo $this->_vars['data']['company']['output_name']; ?>
</b>&nbsp; <?php echo $this->_vars['data']['company_not_approve']['output_name']; ?>
 <?php echo l('agent_status_not_approved', 'users', '', 'text', array()); ?>&nbsp;|&nbsp;<a href="<?php echo $this->_vars['site_url']; ?>
admin/users/agent_get_out/<?php echo $this->_vars['data']['id']; ?>
"><?php echo l('link_agent_cancel_request', 'users', '', 'text', array()); ?></a>
				<?php endif; ?>
			<?php else: ?>
				<?php echo tpl_function_user_select(array('max' => 1,'var_name' => 'data[agent_company]','user_type' => 'company','template' => "company"), $this);?>
			<?php endif; ?>
		</div>
	</div>
	
	<div class="row">
		<div class="h"><?php echo l('field_contact_email', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><input type="text" name="data[contact_email]" value="<?php echo $this->_run_modifier($this->_vars['data']['contact_email'], 'escape', 'plugin', 1); ?>
"></div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_contact_phone', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><input type="text" name="data[contact_phone]" value="<?php echo $this->_run_modifier($this->_vars['data']['contact_phone'], 'escape', 'plugin', 1); ?>
" class="phone-field"></div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_contact_info', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><textarea rows="5" cols="80" name="data[contact_info]"><?php echo $this->_run_modifier($this->_vars['data']['contact_info'], 'escape', 'plugin', 1); ?>
</textarea></div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_twitter', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><input type="text" name="data[twitter]" value="<?php echo $this->_run_modifier($this->_vars['data']['twitter'], 'escape', 'plugin', 1); ?>
"></div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_facebook', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><input type="text" name="data[facebook]" value="<?php echo $this->_run_modifier($this->_vars['data']['facebook'], 'escape', 'plugin', 1); ?>
"></div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_vkontakte', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><input type="text" name="data[vkontakte]" value="<?php echo $this->_run_modifier($this->_vars['data']['vkontakte'], 'escape', 'plugin', 1); ?>
"></div>
	</div>
	<?php if ($this->_vars['use_services']): ?>
	<div class="row">
		<div class="h"><?php echo l('field_services', 'users', '', 'text', array()); ?>: <br><br><br>
			<div class="select_actions">
				<a href="javascript:void(0)" id="services_select_all"><?php echo l('select_all', 'start', '', 'text', array()); ?></a>
				<a href="javascript:void(0)" id="services_unselect_all"><?php echo l('unselect_all', 'start', '', 'text', array()); ?></a>
			</div> 
		</div>
		<div class="v">
			<?php if ($this->_vars['use_featured']): ?>
			<input type="hidden" name="services[featured]" value="0">
			<input type="checkbox" name="services[featured]" value="1" id="service_featured" class="width6" <?php if ($this->_vars['data']['is_featured']): ?>checked<?php endif; ?>>
			<label for="service_featured" class="width6"><?php echo l('field_service_featured_agent', 'users', '', 'text', array()); ?></label>
			<?php endif; ?>
			<?php if ($this->_vars['use_show_logo']): ?>
			<input type="hidden" name="services[show_logo]" value="0">
			<input type="checkbox" name="services[show_logo]" value="1" id="service_show_logo" class="width6" <?php if ($this->_vars['data']['is_show_logo']): ?>checked<?php endif; ?>>
			<label for="service_show_logo" class="width6"><?php echo l('field_service_show_logo', 'users', '', 'text', array()); ?></label>
			<?php endif; ?>
		</div>
		<script><?php echo '
			$(function(){
				$(\'#services_select_all\').bind(\'click\', function(){
					$(\'input[name^=services]\').attr(\'checked\', \'checked\');
				});
				$(\'#services_unselect_all\').bind(\'click\', function(){
					$(\'input[name^=services]\').removeAttr(\'checked\');
				});
			});
		'; ?>
</script>
	</div>
	<?php endif; ?>
	<div class="row">
		<div class="h"><?php echo l('field_confirm', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><input type="checkbox" name="data[confirm]" value="1" <?php if ($this->_vars['data']['confirm']): ?>checked<?php endif; ?>></div>
	</div>

</div>
<div class="btn"><div class="l"><input type="submit" name="btn_save" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div></div>
<a class="cancel" href="<?php echo $this->_vars['back_url']; ?>
"><?php echo l('btn_cancel', 'start', '', 'text', array()); ?></a>
</form>
<script type='text/javascript'><?php echo '
	$(function(){
		$("div.row:odd").addClass("zebra");
		$("#pass_change_field").click(function(){
			if(this.checked){
				$("#pass_field").removeAttr("disabled");
				$("#repass_field").removeAttr("disabled");
			}else{
				$("#pass_field").attr(\'disabled\', \'disabled\'); $("#repass_field").attr(\'disabled\', \'disabled\');
			}
		});
	});
'; ?>
</script>
<?php if ($this->_vars['phone_format']): ?>
<?php echo tpl_function_js(array('file' => 'jquery.maskedinput.min.js'), $this);?>
<script><?php echo '
$(function(){
	$(\'.phone-field\').mask(\'';  echo $this->_vars['phone_format'];  echo '\');
});
'; ?>
</script>
<?php endif; ?>
