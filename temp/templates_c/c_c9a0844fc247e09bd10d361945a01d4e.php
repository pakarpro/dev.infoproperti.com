<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-02 15:51:01 KRAT */ ?>

<div class="region-box">
	<input type="button" class="with-icon-small i-search no-hover search-btn" id="country_open_<?php echo $this->_vars['country_helper_data']['rand']; ?>
" name="submit" />
	<span><input type="text" name="region_name" id="country_text_<?php echo $this->_vars['country_helper_data']['rand']; ?>
" autocomplete="off" value="<?php echo $this->_vars['country_helper_data']['location_text']; ?>
" placeholder="<?php echo l('field_search_country', 'listings', '', 'text', array()); ?>"></span>
	<input type="hidden" name="<?php echo $this->_vars['country_helper_data']['var_country_name']; ?>
" id="country_hidden_<?php echo $this->_vars['country_helper_data']['rand']; ?>
" value="<?php echo $this->_vars['country_helper_data']['country']['code']; ?>
">
	<input type="hidden" name="<?php echo $this->_vars['country_helper_data']['var_region_name']; ?>
" id="region_hidden_<?php echo $this->_vars['country_helper_data']['rand']; ?>
" value="<?php echo $this->_vars['country_helper_data']['region']['id']; ?>
">
	<input type="hidden" name="<?php echo $this->_vars['country_helper_data']['var_city_name']; ?>
" id="city_hidden_<?php echo $this->_vars['country_helper_data']['rand']; ?>
" value="<?php echo $this->_vars['country_helper_data']['city']['id']; ?>
">
</div>

<?php echo tpl_function_js(array('module' => countries,'file' => 'country-input.js'), $this);?>
<script type='text/javascript'>
<?php if ($this->_vars['country_helper_data']['var_js_name']): ?>var <?php echo $this->_vars['country_helper_data']['var_js_name']; ?>
;<?php endif;  echo '
$(function(){
'; ?>
var region_<?php echo $this->_vars['country_helper_data']['rand']; ?>
 = <?php echo 'new countryInput({
		siteUrl: \'';  echo $this->_vars['site_url'];  echo '\',
		rand: \'';  echo $this->_vars['country_helper_data']['rand'];  echo '\',
		id_country: \'';  echo $this->_vars['country_helper_data']['country']['code'];  echo '\',
		id_region: \'';  echo $this->_vars['country_helper_data']['region']['id'];  echo '\',
		id_city: \'';  echo $this->_vars['country_helper_data']['city']['id'];  echo '\',
		select_type: \'';  echo $this->_vars['country_helper_data']['select_type'];  echo '\'
	});
});
'; ?>
</script>