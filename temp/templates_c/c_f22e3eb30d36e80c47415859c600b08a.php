<?php require_once('/opt/ip/system/libraries/template_lite/plugins/function.block.php'); $this->register_function("block", "tpl_function_block");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-02-29 09:20:21 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array('load_type' => 'ui'));
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<?php echo tpl_function_block(array('name' => show_social_networks_like,'module' => social_networking,'func_param' => true), $this);?>


<script type="text/javascript">
<?php echo '
	function format(n, currency)
	{
		return currency + " " + n.toFixed(0).replace(/./g, function(c, i, a) {
			return i > 0 && c !== "," && (a.length - i) % 3 === 0 ? "." + c : c;
		});
	}
'; ?>

</script>


<div id="compare-container" class="clearfix">
<div id="comp-title">
	<span class="td-grey comp-td">Tipe Properti</span>
	<span class="comp-td">Harga</span>
	<span class="td-grey comp-td">Harga/m2</span>	
	<span class="comp-td">Komplek</span>
	<span class="td-grey comp-td">Luas Bangunan</span>
	<span class="comp-td">Luas Tanah</span>
	<span class="td-grey comp-td">Daya Listrik</span>	
	<span class="comp-td">Kamar Tidur</span>
	<span class="td-grey comp-td">Kamar Mandi</span>
	<span class="comp-td">Garasi</span>
	<span class="td-grey comp-td">Interior</span>
	<span class="comp-td">Pengembang</span>
</div>


<?php if (is_array($this->_vars['listings']) and count((array)$this->_vars['listings'])): foreach ((array)$this->_vars['listings'] as $this->_vars['listing']): ?>
	<div class="inner-compare-box">
		<div class="a-link clearfix">
			<a href="<?php echo $this->_vars['site_url']; ?>
listings/save/<?php echo $this->_vars['listing']['id']; ?>
" class="w-icon-fav">Save to favorite</a>
		</div>
	
    <img class="comp-thumb" src="<?php echo $this->_vars['base_url']; ?>
uploads/listing-search-photo/<?php echo $this->_vars['listing']['id']; ?>
/middle-<?php echo $this->_vars['listing']['logo_image']; ?>
" title="<?php echo $this->_vars['listing']['address']; ?>
">
    
	<div class="comp-detail"> 
		<!-- Tipe Properti :-->
		<span class="td-grey comp-td">
		<?php if ($this->_vars['listing']['id_category'] == 1 && $this->_vars['listing']['property_type'] == 1): ?>
			Apartemen
			<?php elseif ($this->_vars['listing']['id_category'] == 1 && $this->_vars['listing']['property_type'] == 4): ?>
			Rumah
			<?php elseif ($this->_vars['listing']['id_category'] == 1 && $this->_vars['listing']['property_type'] == 10): ?>
			Tanah
			<?php elseif ($this->_vars['listing']['id_category'] == 1 && $this->_vars['listing']['property_type'] == 2): ?>
			Kamar
			<?php elseif ($this->_vars['listing']['id_category'] == 1 && $this->_vars['listing']['property_type'] == 9): ?>
			Lain-lain
		<?php endif; ?>
		</span>
		
		<!-- Harga :-->
		<span class="comp-td"><?php echo ''; ?>
 <script> document.write(format(<?php echo $this->_vars['listing']['price']; ?>
, "Rp. ") +",-"); </script> <?php echo ''; ?>
</span>
		
		<!-- Harga/m2 :-->
		<span class="td-grey comp-td"><?php echo ''; ?>
 <script> document.write(format(<?php echo $this->_vars['listing']['price']; ?>
, "Rp. ") +",-"); </script> <?php echo ''; ?>
</span>
		
		<!-- Lokasi :-->
		<span class="comp-td comp-loc">
			<?php if ($this->_vars['listing']['address'] == ''): ?>
				-
			<?php else: ?>
				<?php echo $this->_vars['listing']['address']; ?>

			<?php endif; ?>
		</span>
		
		<?php if ($this->_vars['listing']['id_type'] == 1): ?> 
            <!-- MOD : Foreach from residential with type sale -->
			<?php if (is_array($this->_vars['residential']) and count((array)$this->_vars['residential'])): foreach ((array)$this->_vars['residential'] as $this->_vars['residen']): ?>
				<?php if ($this->_vars['listing']['id'] == $this->_vars['residen']['id_listing']): ?>
					<!--Luas Bangunan :-->
					<span class="td-grey comp-td">
						<?php if ($this->_vars['residen']['fe_field71'] == ''): ?>
							-
						<?php else: ?>
							<?php echo $this->_vars['residen']['fe_field71']; ?>
 sq.m
						<?php endif; ?>
					</span>
					
					<!--Luas Tanah :-->
					<span class="comp-td">
						<?php if ($this->_vars['residen']['fe_field74'] == ''): ?>
							-
						<?php else: ?>
							<?php echo $this->_vars['residen']['fe_field74']; ?>
 sq.m
						<?php endif; ?>
					</span>
					
					<!--Daya Listrik :-->
					<span class="td-grey comp-td">
						<?php if ($this->_vars['residen']['fe_field73'] == 0): ?>
							-
						<?php elseif ($this->_vars['residen']['fe_field73'] == 1): ?>
							1300 Watt
						<?php elseif ($this->_vars['residen']['fe_field73'] == 2): ?>
							2200 Watt
						<?php elseif ($this->_vars['residen']['fe_field73'] == 3): ?>
							3500 Watt
						<?php elseif ($this->_vars['residen']['fe_field73'] == 4): ?>
							4400 Watt
						<?php elseif ($this->_vars['residen']['fe_field73'] == 5): ?>
							5500 Watt
						<?php elseif ($this->_vars['residen']['fe_field73'] == 6): ?>
							6600 Watt
						<?php else: ?>
							> 6600 Watt <!-- original is Lainnya -->
						<?php endif; ?>
					</span>
					
					<!--bedroom-->
					<div class="clearfix">
                        <?php if ($this->_vars['residen']['fe_bd_rooms_1'] == 0): ?>
                            <span class="comp-td float-left">-</span>
                        <?php else: ?>
                            <span class="comp-td float-left"><?php echo $this->_vars['residen']['fe_bd_rooms_1']; ?>
</span>
                            <img class="comp-icon float-left" src="<?php echo $this->_vars['base_url']; ?>
application/views/default/img/inner-icons/icon-bedroom.png" />
                        <?php endif; ?>
					</div>
					
					<!--bathroom :-->
					<div class="clearfix td-grey">
                        <?php if ($this->_vars['residen']['fe_bth_rooms_1'] == 0): ?>
                            <span class="td-grey comp-td float-left">-</span>
                        <?php else: ?>
                            <span class="td-grey comp-td float-left"><?php echo $this->_vars['residen']['fe_bth_rooms_1']; ?>
</span>
                            <img class="comp-icon float-left" src="<?php echo $this->_vars['base_url']; ?>
application/views/default/img/inner-icons/icon-bathroom.png" />
                        <?php endif; ?>
					</div>
					
					<!--garage :-->
					<div class="clearfix">
                        <?php if ($this->_vars['residen']['fe_garages_1'] == 0): ?>
                            <span class="comp-td float-left">-</span>
                        <?php else: ?>
                            <span class="comp-td float-left"><?php echo $this->_vars['residen']['fe_garages_1']; ?>
</span>
                            <img class="comp-icon float-left" src="<?php echo $this->_vars['base_url']; ?>
application/views/default/img/inner-icons/icon-garage.png" />
                        <?php endif; ?>
					</div>
					
					<!--Interior :-->
					<span class="td-grey comp-td">
                        <?php if ($this->_vars['residen']['fe_furniture_1'] == 1): ?>
                            Furnished
                        <?php elseif ($this->_vars['residen']['fe_furniture_1'] == 2): ?>
                            Semi-Furnished 
                        <?php elseif ($this->_vars['residen']['fe_furniture_1'] == 3): ?>
                            Unfurnished 
                        <?php else: ?>
                            -
                        <?php endif; ?>
					</span>
				<?php endif; ?>
			<?php endforeach; endif; ?>
            <!-- End MOD : Foreach from residential with type sale -->
			
            <!-- MOD : Foreach from commercial with type sale -->
			<?php if (is_array($this->_vars['commercial']) and count((array)$this->_vars['commercial'])): foreach ((array)$this->_vars['commercial'] as $this->_vars['commer']): ?>
			   <?php if ($this->_vars['listing']['id'] == $this->_vars['commer']['id_listing']): ?>
					<!--Luas Bangunan :-->
					<span class="td-grey comp-td">
						<?php if ($this->_vars['commer']['fe_field71'] == ''): ?>
							-
						<?php else: ?>
							<?php echo $this->_vars['commer']['fe_field71']; ?>
 sq.m
						<?php endif; ?>
					</span>
					
					<!--Luas Tanah :-->
					<span class="comp-td">
						<?php if ($this->_vars['commer']['fe_field74'] == ''): ?>
							-
						<?php else: ?>
							<?php echo $this->_vars['commer']['fe_field74']; ?>
 sq.m
						<?php endif; ?>
					</span>
					
					<!--Daya Listrik :-->
					<span class="td-grey comp-td">
						<?php if ($this->_vars['commer']['fe_field73'] == 0): ?>
							-
						<?php elseif ($this->_vars['commer']['fe_field73'] == 1): ?>
							1300 Watt
						<?php elseif ($this->_vars['commer']['fe_field73'] == 2): ?>
							2200 Watt
						<?php elseif ($this->_vars['commer']['fe_field73'] == 3): ?>
							3500 Watt
						<?php elseif ($this->_vars['commer']['fe_field73'] == 4): ?>
							4400 Watt
						<?php elseif ($this->_vars['commer']['fe_field73'] == 5): ?>
							5500 Watt
						<?php elseif ($this->_vars['commer']['fe_field73'] == 6): ?>
							6600 Watt
						<?php else: ?>
							> 6600 Watt <!-- original is Lainnya -->
						<?php endif; ?>
					</span>
					
					<!--bedroom-->
					<div class="clearfix">
                        <?php if ($this->_vars['commer']['fe_bd_rooms_1'] == 0): ?>
                            <span class="comp-td float-left">-</span>
                        <?php else: ?>
                            <span class="comp-td float-left"><?php echo $this->_vars['commer']['fe_bd_rooms_1']; ?>
</span>
                            <img class="comp-icon float-left" src="<?php echo $this->_vars['base_url']; ?>
application/views/default/img/inner-icons/icon-bedroom.png" />
                        <?php endif; ?>
					</div>
					
					<!--bathroom :-->
					<div class="clearfix td-grey">
                        <?php if ($this->_vars['commer']['fe_bth_rooms_1'] == 0): ?>
                            <span class="td-grey comp-td float-left">-</span>
                        <?php else: ?>
                            <span class="td-grey comp-td float-left"><?php echo $this->_vars['commer']['fe_bth_rooms_1']; ?>
</span>
                            <img class="comp-icon float-left" src="<?php echo $this->_vars['base_url']; ?>
application/views/default/img/inner-icons/icon-bathroom.png" />
                        <?php endif; ?>
					</div>
					
					<!--garage :-->
					<div class="clearfix">
                        <?php if ($this->_vars['commer']['fe_garages_1'] == 0): ?>
                            <span class="comp-td float-left">-</span>
                        <?php else: ?>
                            <span class="comp-td float-left"><?php echo $this->_vars['commer']['fe_garages_1']; ?>
</span>
                            <img class="comp-icon float-left" src="<?php echo $this->_vars['base_url']; ?>
application/views/default/img/inner-icons/icon-garage.png" />
                        <?php endif; ?>
					</div>
                    
					<!--Interior :-->
					<span class="td-grey comp-td">
                        <?php if ($this->_vars['commer']['fe_furniture_1'] == 1): ?>
                            Furnished
                        <?php elseif ($this->_vars['commer']['fe_furniture_1'] == 2): ?>
                            Semi-Furnished 
                        <?php elseif ($this->_vars['commer']['fe_furniture_1'] == 3): ?>
                            Unfurnished 
                        <?php else: ?>
                            -
                        <?php endif; ?>
					</span>
                    
				<!-- MOD : Originally coded by Teguh, temporarily disabled
				    <div class="clearfix">
						<span class="comp-td float-left"><?php echo $this->_vars['commer']['fe_bd_rooms_1']; ?>
</span><img class="comp-icon float-left" src="<?php echo $this->_vars['base_url']; ?>
application/views/default/img/inner-icons/icon-bedroom.png" />
					</div>
					<div class="clearfix td-grey">
						<span class="td-grey comp-td float-left"><?php echo $this->_vars['commer']['fe_bth_rooms_1']; ?>
</span><img class="comp-icon float-left" src="<?php echo $this->_vars['base_url']; ?>
application/views/default/img/inner-icons/icon-bathroom.png" />
					</div>
					<div class="clearfix">
						<span class="comp-td float-left"><?php echo $this->_vars['commer']['fe_garages_1']; ?>
</span><img class="comp-icon float-left" src="<?php echo $this->_vars['base_url']; ?>
application/views/default/img/inner-icons/icon-garage.png" />
					</div>

					<!--Luas Bangunan :
					<span class="td-grey comp-td"><?php echo $this->_vars['commer']['fe_field71']; ?>
 sq.m </span>
					
					<!--Luas Tanah :
					<span class="comp-td"><?php echo $this->_vars['commer']['fe_field74']; ?>
 sq.m </span>
					
					<!--Daya Listrik :
					<span class="td-grey comp-td"><?php echo $this->_vars['commer']['fe_field73']; ?>
 </span>
					
					<!--Interior :
					<span class="comp-td">
						<?php if ($this->_vars['commer']['fe_furniture_1'] == 1): ?>
							Furnished
						<?php elseif ($this->_vars['commer']['fe_furniture_1'] == 2): ?>
							Semi-Furnished
						<?php elseif ($this->_vars['commer']['fe_furniture_1'] == 3): ?>
							Unfurnished
						 <?php else: ?>
							-
						<?php endif; ?>
					</span>
                -->
				<?php endif; ?>
			<?php endforeach; endif; ?>
            <!-- End MOD : Foreach from commercial with type sale -->

		<?php elseif ($this->_vars['listing']['id_type'] == 3): ?>
		
			<?php if (is_array($this->_vars['residential']) and count((array)$this->_vars['residential'])): foreach ((array)$this->_vars['residential'] as $this->_vars['residen']): ?>
				<?php if ($this->_vars['listing']['id'] == $this->_vars['residen']['id_listing']): ?>
				   
					<div class="clearfix td-grey">
						<span class="td-grey comp-td float-left"><?php echo $this->_vars['residen']['fe_bd_rooms_4']; ?>
</span><img class="comp-icon float-left" src="<?php echo $this->_vars['base_url']; ?>
application/views/default/img/inner-icons/icon-bedroom.png" />
					</div>
					<div class="clearfix">
						<span class="comp-td float-left"><?php echo $this->_vars['residen']['fe_bth_rooms_4']; ?>
</span><img class="comp-icon float-left" src="<?php echo $this->_vars['base_url']; ?>
application/views/default/img/inner-icons/icon-bathroom.png" />
					</div>
					<div class="clearfix td-grey">
						<span class="td-grey comp-td float-left"><?php echo $this->_vars['residen']['fe_garages_4']; ?>
</span><img class="comp-icon float-left" src="<?php echo $this->_vars['base_url']; ?>
application/views/default/img/inner-icons/icon-garage.png" />
					</div>
					
					<!--Luas Bangunan :-->
					<span class="comp-td"><?php echo $this->_vars['listing']['square']; ?>
 sq.m</span>
					
					<!--Interior :--> 
					<span class="td-grey comp-td">
					<?php if ($this->_vars['residen']['fe_furniture_4'] == 1): ?>
						Furnished
					<?php elseif ($this->_vars['residen']['fe_furniture_4'] == 2): ?>
						Semi-Furnished
					<?php elseif ($this->_vars['residen']['fe_furniture_4'] == 3): ?>
						Unfurnished
					<?php else: ?>
						-
					<?php endif; ?>
					</span>
				<?php endif; ?>
			<?php endforeach; endif; ?>
			
			<?php if (is_array($this->_vars['commercial']) and count((array)$this->_vars['commercial'])): foreach ((array)$this->_vars['commercial'] as $this->_vars['commer']): ?>
			   <?php if ($this->_vars['listing']['id'] == $this->_vars['commer']['id_listing']): ?>
				
				<div class="clearfix">
					<span class="comp-td float-left"><?php echo $this->_vars['commer']['fe_bd_rooms_4']; ?>
</span><img class="comp-icon float-left" src="<?php echo $this->_vars['base_url']; ?>
application/views/default/img/inner-icons/icon-bedroom.png" />
				</div>
				
				<div class="clearfix td-grey">
					<span class="td-grey comp-td float-left"><?php echo $this->_vars['commer']['fe_bth_rooms_4']; ?>
</span><img class="comp-icon float-left" src="<?php echo $this->_vars['base_url']; ?>
application/views/default/img/inner-icons/icon-bathroom.png" />
				</div>
				
				<div class="clearfix">
					<span class="comp-td float-left"><?php echo $this->_vars['commer']['fe_garages_4']; ?>
</span><img class="comp-icon float-left" src="<?php echo $this->_vars['base_url']; ?>
application/views/default/img/inner-icons/icon-garage.png" />
				</div>
				
				<!--Luas Bangunan :-->
				<span class="td-grey comp-td"><?php echo $this->_vars['listing']['square']; ?>
 sq.m</span>
				
				<!--Interior :-->
				<span class="comp-td">
					<?php if ($this->_vars['commer']['fe_furniture_4'] == 1): ?>
						Furnished
						<?php elseif ($this->_vars['commer']['fe_furniture_4'] == 2): ?>
						Semi-Furnished
						<?php elseif ($this->_vars['commer']['fe_furniture_4'] == 3): ?>
						Unfurnished
						<?php else: ?>
						-
					<?php endif; ?>
				</span>
				<?php endif; ?>
			<?php endforeach; endif; ?>
		<?php endif; ?>    
		
		<!--Pengembang :-->
		<?php if (is_array($this->_vars['develop']) and count((array)$this->_vars['develop'])): foreach ((array)$this->_vars['develop'] as $this->_vars['developer']): ?>
			<?php if ($this->_vars['developer']['id'] == $this->_vars['listing']['id_user']): ?>
				<span class="comp-td">
					<?php if ($this->_vars['developer']['company_name'] != ""): ?>
						<?php echo $this->_vars['developer']['company_name']; ?>

					 <?php else: ?>
						-
					<?php endif; ?>
				</span>
			<?php endif; ?>
		 <?php endforeach; endif; ?>

		<div class="a-link clearfix">
			<a class="" href="<?php echo $this->_vars['site_url']; ?>
listing-Propertidijual-in-Indonesia-id-<?php echo $this->_vars['listing']['id']; ?>
-overview-operation-sale-no">View Detail</a> <br /> <br /> <br /> <br />
		</a>
	</div><!-- END .comp-detail -->
	
	<!-- Others Listing -->
	<div class="comp-detail">
		<h1>Listing serupa</h1>

		<?php if (is_array($this->_vars['similar']) and count((array)$this->_vars['similar'])): foreach ((array)$this->_vars['similar'] as $this->_vars['simlisting']): ?>
			<?php if ($this->_vars['simlisting']['id_user'] == $this->_vars['listing']['id_user']): ?>
				<img src="<?php echo $this->_vars['base_url']; ?>
uploads/listing-search-photo/<?php echo $this->_vars['simlisting']['id']; ?>
/middle-<?php echo $this->_vars['simlisting']['logo_image']; ?>
" title="<?php echo $this->_vars['simlisting']['address']; ?>
"> <br /> <br />
					<?php if ($this->_vars['simlisting']['id_category'] == 1 && $this->_vars['simlisting']['property_type'] == 1): ?>
							Apartment Dijual<br />
						<?php elseif ($this->_vars['simlisting']['id_category'] == 1 && $this->_vars['simlisting']['property_type'] == 4): ?>
							Rumah Dijual<br />
						<?php elseif ($this->_vars['simlisting']['id_category'] == 1 && $this->_vars['simlisting']['property_type'] == 10): ?>
							Tanah Dijual<br />
						<?php elseif ($this->_vars['simlisting']['id_category'] == 1 && $this->_vars['simlisting']['property_type'] == 2): ?>
							Kamar Dijual<br />
						<?php elseif ($this->_vars['simlisting']['id_category'] == 1 && $this->_vars['simlisting']['property_type'] == 9): ?>
							Lain-lain<br />
						<?php endif; ?> 
					Harga : <?php echo ''; ?>
 <script> document.write(format(<?php echo $this->_vars['simlisting']['price']; ?>
, "Rp. ") +",-"); </script> <?php echo ''; ?>
<br />
					Lokasi : <?php echo $this->_vars['simlisting']['address']; ?>
<br />
					
					<?php if ($this->_vars['simlisting']['id_type'] == 1): ?> 
					
						<?php if (is_array($this->_vars['residential']) and count((array)$this->_vars['residential'])): foreach ((array)$this->_vars['residential'] as $this->_vars['residen']): ?>
									
							<?php if ($this->_vars['simlisting']['id'] == $this->_vars['residen']['id_simlisting']): ?>
							   
								<?php echo $this->_vars['residen']['fe_bd_rooms_1']; ?>
 KT <br />
								<?php echo $this->_vars['residen']['fe_bth_rooms_1']; ?>
KM <br />
								LB <?php echo $this->_vars['residen']['fe_field71']; ?>
 sq.m <br />
								LT <?php echo $this->_vars['residen']['fe_field74']; ?>
 sq.m <br />
										  
							<?php endif; ?>
							
						<?php endforeach; endif; ?>
						
						<?php if (is_array($this->_vars['commercial']) and count((array)$this->_vars['commercial'])): foreach ((array)$this->_vars['commercial'] as $this->_vars['commer']): ?>
									
						   <?php if ($this->_vars['simlisting']['id'] == $this->_vars['commer']['id_simlisting']): ?>
							   <ul class="feat-icon">
									<li class="simlisting-icon bedroom"> : <?php echo $this->_vars['commer']['fe_bd_rooms_1']; ?>
</li>
									<li class="simlisting-icon bathroom"> : <?php echo $this->_vars['commer']['fe_bth_rooms_1']; ?>
</li>
									<li class="simlisting-icon garages"> : <?php echo $this->_vars['commer']['fe_garages_1']; ?>
</li>
								</ul>
									LB <?php echo $this->_vars['commer']['fe_field71']; ?>
 sq.m <br />
									LT <?php echo $this->_vars['commer']['fe_field74']; ?>
 sq.m <br />
							  
							<?php endif; ?>
							
						<?php endforeach; endif; ?>
						
					<?php elseif ($this->_vars['simlisting']['id_type'] == 3): ?>
						<?php if (is_array($this->_vars['residential']) and count((array)$this->_vars['residential'])): foreach ((array)$this->_vars['residential'] as $this->_vars['residen']): ?>
							<?php if ($this->_vars['simlisting']['id'] == $this->_vars['residen']['id_simlisting']): ?>
								<ul class="feat-icon">									
									<li class="simlisting-icon bedroom"> : <?php echo $this->_vars['residen']['fe_bd_rooms_4']; ?>
</li>
									<li class="simlisting-icon bathroom"> : <?php echo $this->_vars['residen']['fe_bth_rooms_4']; ?>
</li>
									<li class="simlisting-icon garages"> : <?php echo $this->_vars['residen']['fe_garages_4']; ?>
</li>
								</ul>
									LB <?php echo $this->_vars['simlisting']['square']; ?>
 sq.m <br /><br/>
							<?php endif; ?>
						<?php endforeach; endif; ?>
						<?php if (is_array($this->_vars['commercial']) and count((array)$this->_vars['commercial'])): foreach ((array)$this->_vars['commercial'] as $this->_vars['commer']): ?>
						   <?php if ($this->_vars['simlisting']['id'] == $this->_vars['commer']['id_simlisting']): ?>
								<ul class="feat-icon">
									<li class="simlisting-icon bedroom"> : <?php echo $this->_vars['commer']['fe_bd_rooms_4']; ?>
</li>
									<li class="simlisting-icon bathroom"> : <?php echo $this->_vars['commer']['fe_bth_rooms_4']; ?>
</li>
									<li class="simlisting-icon garages"> : <?php echo $this->_vars['commer']['fe_garages_4']; ?>
</li>
								</ul>
									LB <?php echo $this->_vars['simlisting']['square']; ?>
 sq.m <br /><br/>
							<?php endif; ?>
						<?php endforeach; endif; ?>
					<?php endif; ?>    
					<a href="<?php echo $this->_vars['site_url']; ?>
simlisting-Propertidijual-in-Indonesia-id-<?php echo $this->_vars['simlisting']['id']; ?>
-overview-operation-sale-no">View Detail</a> <br /> <br /> <br /> <br />
				 <?php endif; ?>         
			<?php endforeach; endif; ?>
		</div>
	</div>
</div><!-- end inner compare box -->
    
<?php endforeach; endif; ?>
</div><!-- end #compare-container -->
	
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>