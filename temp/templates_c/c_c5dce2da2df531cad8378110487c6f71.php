<?php require_once('/opt/ip/system/libraries/template_lite/plugins/function.seolink.php'); $this->register_function("seolink", "tpl_function_seolink");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.block.php'); $this->register_function("block", "tpl_function_block");  require_once('/opt/ip/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-01-04 16:45:46 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
  $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->module_path. "start". $this->module_templates.  $this->get_current_theme_gid('', '"start"'). "left_panel.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<div class="rc">
	<div class="content-block">
        <h1><?php echo l('header_service_settings', 'services', '', 'text', array()); ?>: <?php echo $this->_vars['data']['name']; ?>
</h1>
    
        <div class="content-value">
            
        
            <form method="post" action="">
                <div class="edit_block">
                    <div class="payment_table">
                        <table>
                        <?php if (is_array($this->_vars['data']['template']['data_admin_array']) and count((array)$this->_vars['data']['template']['data_admin_array'])): foreach ((array)$this->_vars['data']['template']['data_admin_array'] as $this->_vars['item']): ?>
                            <tr>
                                <td><?php echo $this->_vars['item']['name']; ?>
:</td>
                                <td class="value">
                                <?php if ($this->_vars['item']['type'] == 'string' || $this->_vars['item']['type'] == 'int' || $this->_vars['item']['type'] == 'text'): ?>
                                    <?php echo $this->_vars['item']['value']; ?>

                                <?php elseif ($this->_vars['item']['type'] == 'price'): ?>
                                    <?php echo tpl_function_block(array('name' => currency_format_output,'module' => start,'value' => $this->_vars['item']['value'],'cur_gid' => $this->_vars['base_currency']['gid']), $this);?>
                                <?php elseif ($this->_vars['item']['type'] == 'checkbox'): ?>
                                    <?php if ($this->_vars['item']['value'] == '1'):  echo l('yes_checkbox_value', 'services', '', 'text', array());  else:  echo l('no_checkbox_value', 'services', '', 'text', array());  endif; ?>
                                <?php endif; ?>
                                </td>
                            </tr>
                        <?php endforeach; endif; ?>
                 
                        <?php if (is_array($this->_vars['data']['template']['data_user_array']) and count((array)$this->_vars['data']['template']['data_user_array'])): foreach ((array)$this->_vars['data']['template']['data_user_array'] as $this->_vars['item']): ?>
                        <?php if ($this->_vars['item']['type'] == 'hidden'): ?>
                        <input type="hidden" value="<?php echo $this->_vars['item']['value']; ?>
" name="data_user[<?php echo $this->_vars['item']['gid']; ?>
]">
                        <?php else: ?>
                            <tr>
                                <td><?php echo $this->_vars['item']['name']; ?>
:</td>
                                <?php if ($this->_vars['item']['type'] == 'string'): ?>
                                <td class="value"><input type="text" value="<?php echo $this->_vars['item']['value']; ?>
" name="data_user[<?php echo $this->_vars['item']['gid']; ?>
]"></td>
                                <?php elseif ($this->_vars['item']['type'] == 'int'): ?>
                                <td class="value"><input type="text" value="<?php echo $this->_vars['item']['value']; ?>
" name="data_user[<?php echo $this->_vars['item']['gid']; ?>
]" class="short"></td>
                                <?php elseif ($this->_vars['item']['type'] == 'price'): ?>
                                <td class="value"><input type="text" value="<?php echo $this->_vars['item']['value']; ?>
" name="data_user[<?php echo $this->_vars['item']['gid']; ?>
]" class="short"> <?php echo tpl_function_block(array('name' => currency_format_output,'module' => start,'cur_gid' => $this->_vars['base_currency']['gid']), $this);?></td>
                                <?php elseif ($this->_vars['item']['type'] == 'text'): ?>
                                <td class="value"class="value"><textarea name="data_user[<?php echo $this->_vars['item']['gid']; ?>
]"><?php echo $this->_vars['item']['value']; ?>
</textarea></td>
                                <?php elseif ($this->_vars['item']['type'] == 'checkbox'): ?>
                                <td class="value"><input type="checkbox" value="1" name="data_user[<?php echo $this->_vars['item']['gid']; ?>
]" <?php if ($this->_vars['item']['value'] == '1'): ?>checked<?php endif; ?>></td>
                                <?php endif; ?>
                            </tr>
                        <?php endif; ?>
                        <?php endforeach; endif; ?>
                        
                        <?php if ($this->_vars['data']['template']['price_type'] == '2'): ?>
                            <tr>
                                <td><?php echo l('field_your_price', 'services', '', 'text', array()); ?>:</td>
                                <td class="value"><input type="text" value="<?php echo $this->_vars['data']['price']; ?>
" name="price" class="short"> <b><?php echo tpl_function_block(array('name' => currency_format_output,'module' => start,'cur_gid' => $this->_vars['base_currency']['gid']), $this);?></b></td>
                            </tr>
                        <?php elseif ($this->_vars['data']['template']['price_type'] == '3'): ?>
                            <tr>
                                <td><?php echo l('field_price', 'services', '', 'text', array()); ?>:</td>
                                <td class="value"><input type="hidden" value="<?php echo $this->_vars['data']['price']; ?>
" name="price" class="short"><b><?php echo tpl_function_block(array('name' => currency_format_output,'module' => start,'value' => $this->_vars['data']['price'],'cur_gid' => $this->_vars['base_currency']['gid']), $this);?></b></td>
                            </tr>
                        <?php else: ?>
                            <tr>
                                <td><?php echo l('field_price', 'services', '', 'text', array()); ?>:</td>
                                <td class="value"><b><?php echo tpl_function_block(array('name' => currency_format_output,'module' => start,'value' => $this->_vars['data']['price'],'cur_gid' => $this->_vars['base_currency']['gid']), $this);?></b></td>
                            </tr>
                        <?php endif; ?>
                        </table>
                    </div>
                </div>
                
                <?php if ($this->_vars['data']['gid'] == 'listings_slider_service'): ?>
                	<?php if ($this->_vars['data']['count_photo'] == '0'): ?>
                    	<h2 class="line top bottom">Anda belum mengupload gambar untuk listing Anda. Silahkan klik <a href="<?php echo $this->_vars['site_url']; ?>
listings/edit/<?php echo $this->_vars['item']['value']; ?>
/gallery">link</a> berikut untuk mengupload gambar.</h2>
                    <?php else: ?>
                        <?php if ($this->_vars['data']['free_activate']): ?>
                        <input type="submit" class='btn' value="<?php echo l('btn_activate_free', 'services', '', 'button', array()); ?>" name="btn_account">
                        <?php else: ?>
                            <?php if (( $this->_vars['data']['pay_type'] == 1 || $this->_vars['data']['pay_type'] == 2 ) && $this->_vars['is_module_installed']): ?>
                                <h2 class="line top bottom"><?php echo l('account_payment', 'services', '', 'text', array()); ?></h2>
                                <?php if ($this->_vars['data']['disable_account_pay']):  echo l('error_account_less_then_service_price', 'services', '', 'text', array()); ?> <a href="<?php echo tpl_function_seolink(array('module' => 'users','method' => 'account'), $this);?>"><?php echo l("link_add_founds", 'services', '', 'text', array()); ?></a>
                                <?php else: ?>
                                    <?php echo l('on_your_account_now', 'services', '', 'text', array()); ?>: <b><?php echo tpl_function_block(array('name' => currency_format_output,'module' => start,'value' => $this->_vars['data']['user_account'],'cur_gid' => $this->_vars['base_currency']['gid']), $this);?></b>
                                    <div class="b outside">
                                        <input type="submit" value="<?php echo l('btn_pay_account', 'services', '', 'button', array()); ?>" name="btn_account">
                                    </div>
                                <?php endif; ?>
                            <?php endif; ?>
                            
                            <?php if ($this->_vars['data']['pay_type'] == 2 || $this->_vars['data']['pay_type'] == 3): ?>
                                <h2 class="line top bottom"><?php echo l('payment_systems', 'services', '', 'text', array()); ?></h2>
                                <?php if ($this->_vars['billing_systems']): ?>
                                <?php echo l('error_select_payment_system', 'services', '', 'text', array()); ?>&nbsp;*<br>
                                <select name="system_gid" id="system_gid"><option value="">...</option><?php if (is_array($this->_vars['billing_systems']) and count((array)$this->_vars['billing_systems'])): foreach ((array)$this->_vars['billing_systems'] as $this->_vars['item']): ?><option value="<?php echo $this->_vars['item']['gid']; ?>
"><?php echo $this->_vars['item']['name']; ?>
</option><?php endforeach; endif; ?></select>
                                <?php if (is_array($this->_vars['billing_systems']) and count((array)$this->_vars['billing_systems'])): foreach ((array)$this->_vars['billing_systems'] as $this->_vars['item']): ?>
                                <div class="hide" id="details_<?php echo $this->_vars['item']['gid']; ?>
">
                                <br><?php echo l('field_info_data', 'payments', '', 'text', array()); ?>:<br>
                                <?php echo $this->_vars['item']['info_data']; ?>

                                </div>
                                <?php endforeach; endif; ?>
                                <div class="b outside">
                                    <input type="submit" value="<?php echo l('btn_pay_systems', 'services', '', 'button', array()); ?>" name="btn_system" class="btn">
                                    <a class="btn-link" href="<?php echo $this->_vars['site_url']; ?>
users_services/my_services">
                                        <ins class="with-icon i-larr"></ins>
                                        <?php echo l('back_to_payment_services', 'services', '', 'text', array()); ?>
                                    </a>
                                </div>
                                <script><?php echo '
                                    $(function(){
                                        $(\'#system_gid\').bind(\'change\', function(){
                                            ';  if (is_array($this->_vars['billing_systems']) and count((array)$this->_vars['billing_systems'])): foreach ((array)$this->_vars['billing_systems'] as $this->_vars['item']):  echo '
                                            $(\'#details_';  echo $this->_vars['item']['gid'];  echo '\').hide();
                                            ';  if ($this->_vars['item']['info_data']):  echo '
                                            if($(this).val() == \'';  echo $this->_vars['item']['gid'];  echo '\') 
                                                $(\'#details_';  echo $this->_vars['item']['gid'];  echo '\').show();
                                            ';  endif;  echo '
                                            ';  endforeach; endif;  echo '
                                        });
                                    });
                                '; ?>
</script>
                                <?php elseif ($this->_vars['data']['pay_type'] == 3): ?>
                                <?php echo l('error_empty_billing_system_list', 'service', '', 'text', array()); ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endif; ?>  <!-- end of photo count -->
					
                <?php else: ?>
                    
					<?php if ($this->_vars['data']['free_activate']): ?>
                    <input type="submit" class='btn' value="<?php echo l('btn_activate_free', 'services', '', 'button', array()); ?>" name="btn_account">
                    <?php else: ?>
                        <?php if (( $this->_vars['data']['pay_type'] == 1 || $this->_vars['data']['pay_type'] == 2 ) && $this->_vars['is_module_installed']): ?>
                            <h2 class="line top bottom"><?php echo l('account_payment', 'services', '', 'text', array()); ?></h2>
                            <?php if ($this->_vars['data']['disable_account_pay']):  echo l('error_account_less_then_service_price', 'services', '', 'text', array()); ?> <a href="<?php echo tpl_function_seolink(array('module' => 'users','method' => 'account'), $this);?>"><?php echo l("link_add_founds", 'services', '', 'text', array()); ?></a>
                            <?php else: ?>
                                <?php echo l('on_your_account_now', 'services', '', 'text', array()); ?>: <b><?php echo tpl_function_block(array('name' => currency_format_output,'module' => start,'value' => $this->_vars['data']['user_account'],'cur_gid' => $this->_vars['base_currency']['gid']), $this);?></b>
                                <div class="b outside">
                                    <input type="submit" value="<?php echo l('btn_pay_account', 'services', '', 'button', array()); ?>" name="btn_account">
                                </div>
                            <?php endif; ?>
                        <?php endif; ?>
                        
                        <?php if ($this->_vars['data']['pay_type'] == 2 || $this->_vars['data']['pay_type'] == 3): ?>
                            <h2 class="line top bottom"><?php echo l('payment_systems', 'services', '', 'text', array()); ?></h2>
                            <?php if ($this->_vars['billing_systems']): ?>
                            <?php echo l('error_select_payment_system', 'services', '', 'text', array()); ?>&nbsp;*<br>
                            <select name="system_gid" id="system_gid"><option value="">...</option><?php if (is_array($this->_vars['billing_systems']) and count((array)$this->_vars['billing_systems'])): foreach ((array)$this->_vars['billing_systems'] as $this->_vars['item']): ?><option value="<?php echo $this->_vars['item']['gid']; ?>
"><?php echo $this->_vars['item']['name']; ?>
</option><?php endforeach; endif; ?></select>
                            <?php if (is_array($this->_vars['billing_systems']) and count((array)$this->_vars['billing_systems'])): foreach ((array)$this->_vars['billing_systems'] as $this->_vars['item']): ?>
                            <div class="hide" id="details_<?php echo $this->_vars['item']['gid']; ?>
">
                            <br><?php echo l('field_info_data', 'payments', '', 'text', array()); ?>:<br>
                            <?php echo $this->_vars['item']['info_data']; ?>

                            </div>
                            <?php endforeach; endif; ?>
                            <div class="b outside">
                                <input type="submit" value="<?php echo l('btn_pay_systems', 'services', '', 'button', array()); ?>" name="btn_system" class="btn">
                                <a class="btn-link" href="<?php echo $this->_vars['site_url']; ?>
users_services/my_services">
                                    <ins class="with-icon i-larr"></ins>
                                    <?php echo l('back_to_payment_services', 'services', '', 'text', array()); ?>
                                </a>
                            </div>
                            <script><?php echo '
                                $(function(){
                                    $(\'#system_gid\').bind(\'change\', function(){
                                        ';  if (is_array($this->_vars['billing_systems']) and count((array)$this->_vars['billing_systems'])): foreach ((array)$this->_vars['billing_systems'] as $this->_vars['item']):  echo '
                                        $(\'#details_';  echo $this->_vars['item']['gid'];  echo '\').hide();
                                        ';  if ($this->_vars['item']['info_data']):  echo '
                                        if($(this).val() == \'';  echo $this->_vars['item']['gid'];  echo '\') 
                                            $(\'#details_';  echo $this->_vars['item']['gid'];  echo '\').show();
                                        ';  endif;  echo '
                                        ';  endforeach; endif;  echo '
                                    });
                                });
                            '; ?>
</script>
                            <?php elseif ($this->_vars['data']['pay_type'] == 3): ?>
                            <?php echo l('error_empty_billing_system_list', 'service', '', 'text', array()); ?>
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endif; ?>
                <?php endif; ?> <!-- end of service gid type -->
            </form>
        </div>
        <!-- end of div with class content-value -->
	</div>
    <!-- end of div with class content-block -->
</div>
<div class="clr"></div>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
