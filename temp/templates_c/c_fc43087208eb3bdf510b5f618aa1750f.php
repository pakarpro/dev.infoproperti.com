<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.escape.php'); $this->register_modifier("escape", "tpl_modifier_escape");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.helper.php'); $this->register_function("helper", "tpl_function_helper");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2014-06-12 14:19:35 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
  echo tpl_function_helper(array('func_name' => get_admin_level1_menu,'helper_name' => menu,'func_param' => 'admin_reviews_menu'), $this);?>
<div class="actions">&nbsp;</div>

<form method="post" action="<?php echo $this->_vars['data']['action']; ?>
" name="save_form" enctype="multipart/form-data">
	<div class="edit-form n250">
		<div class="row header"><?php echo l('admin_header_settings_edit', 'reviews', '', 'text', array()); ?></div>
		<div class="row zebra">
			<div class="h"><?php echo l('settings_use_alerts', 'reviews', '', 'text', array()); ?>:</div>
			<div class="v">
				<input type="hidden" name="reviews_use_alerts" value="0">
				<input type="checkbox" name="reviews_use_alerts" value="1" <?php if ($this->_vars['settings_data']['reviews_use_alerts']): ?>checked<?php endif; ?> class="short">
			</div>
		</div>
		<div class="row">
			<div class="h"><?php echo l('settings_alert_name', 'reviews', '', 'text', array()); ?>:</div>
			<div class="v">
				<input type="text" name="reviews_alert_name" value="<?php echo $this->_run_modifier($this->_vars['settings_data']['reviews_alert_name'], 'escape', 'plugin', 1); ?>
" class="middle">
			</div>
		</div>
		<div class="row zebra">
			<div class="h"><?php echo l('settings_alert_email', 'reviews', '', 'text', array()); ?>:</div>
			<div class="v">
				<input type="text" name="reviews_alert_email" value="<?php echo $this->_run_modifier($this->_vars['settings_data']['reviews_alert_email'], 'escape', 'plugin', 1); ?>
" class="middle">
			</div>
		</div>
	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div></div>
</form>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>