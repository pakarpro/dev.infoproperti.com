<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-05-12 05:17:08 KRAT */ ?>

<link href="<?php echo $this->_vars['site_root']; ?>
application/modules/uploads/views/default/css/style-<?php echo $this->_vars['_LANG']['rtl']; ?>
.css" rel="stylesheet" type="text/css" />
<?php echo tpl_function_js(array('file' => 'uploader.js'), $this);?>

<?php if ($this->_vars['use_drop_zone']): ?>
<div id="drop_zone_<?php echo $this->_vars['uploader_rand']; ?>
" class="drop_zone hide">
	<p><?php echo l('load_files_by_drag_drop', 'uploads', '', 'text', array()); ?> &nbsp; <span><?php echo l('btn_choose_file', 'uploads', '', 'text', array()); ?><input type="file" name="<?php echo $this->_vars['field_name']; ?>
" multiple="true" id="upload_file_<?php echo $this->_vars['uploader_rand']; ?>
"></span></p>
</div>
<?php endif; ?>
<input type="file" name="<?php echo $this->_vars['field_name']; ?>
" id="upload_file_simple_<?php echo $this->_vars['uploader_rand']; ?>
" class="simple_uploader fleft <?php if ($this->_vars['use_drop_zone']): ?>hide<?php endif; ?>">
<input type="submit" class='btn' value="<?php echo l('btn_add', 'start', '', 'button', array()); ?>" id="btn_upload_<?php echo $this->_vars['uploader_rand']; ?>
">

<div class="clr"></div>

<?php if ($this->_vars['use_drop_zone']): ?>
<script><?php echo '
	var uploader_';  echo $this->_vars['uploader_rand'];  echo ';
	$(function(){
		uploader_';  echo $this->_vars['uploader_rand'];  echo ' = new uploader({
			siteUrl: \'';  echo $this->_vars['site_url'];  echo '\',
			uploadUrl: \'';  echo $this->_vars['upload_url'];  echo '\',
			zoneId: \'drop_zone_';  echo $this->_vars['uploader_rand'];  echo '\',
			fileId: \'upload_file_';  echo $this->_vars['uploader_rand'];  echo '\',
			';  if ($this->_vars['uploader_form_id']): ?>formId: '<?php echo $this->_vars['uploader_form_id']; ?>
',<?php endif;  echo '
			filebarId: \'filebar_';  echo $this->_vars['uploader_rand'];  echo '\',
			sendType: \'file\',
			sendId: \'btn_upload_';  echo $this->_vars['uploader_rand'];  echo '\',
			maxFileSize: ';  echo $this->_vars['max_file_size'];  echo ',
			mimeType:  [\'image/jpg\',\'image/png\',\'image/jpeg\',\'image/gif\', \'video/x-flv\',\'video/flv\',\'video/x-matroska\',\'application/octet-stream\',\'video/x-msvideo\',\'video/avi\',\'video/msvideo\',\'video/mpeg\',\'video/x-ms-asf\'],
			fieldName: \'';  echo $this->_vars['field_name'];  echo '\',
			';  if ($this->_vars['uploader_callback']): ?>callback: <?php $this->_tag_stack[] = array('tpl_block_strip', array()); tpl_block_strip(array(), null, $this); ob_start();  echo $this->_vars['uploader_callback'];  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_strip($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>,<?php endif;  echo '
			createThumb: true,
			thumbWidth: 100,
			thumbHeight: 100,
			thumbCrop: false,
			thumbJpeg: false,
			thumbBg: \'transparent\',
			fileListInZone: true
		});
		if(uploader_';  echo $this->_vars['uploader_rand'];  echo '.checkFileApi()){
			$(\'#drop_zone_';  echo $this->_vars['uploader_rand'];  echo '\').show();
		}else{
			$(\'#upload_file_simple_';  echo $this->_vars['uploader_rand'];  echo '\').after(\'<input type="hidden" name="no_ajax" value="1">\').show();
			
			$(\'#btn_upload_';  echo $this->_vars['uploader_rand'];  echo '\').unbind().bind(\'click\', function(){
				';  if ($this->_vars['uploader_form_id']): ?>$('#<?php echo $this->_vars['uploader_form_id']; ?>
').attr('action', '<?php echo $this->_vars['site_url'];  echo $this->_vars['upload_url']; ?>
').submit();<?php endif;  echo '
				return true;
			});
		}
	});
'; ?>
</script>
<?php endif; ?>
