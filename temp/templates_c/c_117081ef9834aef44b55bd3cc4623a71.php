<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-02 14:45:57 KRAT */ ?>

	<?php switch($this->_vars['template']): case 'button':  ?>
		<?php $this->assign('popup_form', 1); ?>
		<div class="b outside"><input type="submit" name="booking_btn" value="<?php echo l('btn_order', 'listings', '', 'button', array()); ?>" id="booking_btn<?php echo $this->_vars['rand']; ?>
"></div>
	<?php break; case 'icon':  ?>
		<?php $this->assign('popup_form', 1); ?>
		<a href="#" id="booking_btn<?php echo $this->_vars['rand']; ?>
" class="btn-link link-r-margin" title="<?php echo l('link_booking_listing', 'listings', '', 'button', array()); ?>"><ins class="with-icon i-calendar"></ins></a>
	<?php break; case 'link':  ?>
		<?php $this->assign('popup_form', 1); ?>
		<a href="#" id="booking_btn<?php echo $this->_vars['rand']; ?>
"><?php echo l('link_booking_listing', 'listings', '', 'text', array()); ?></a>	
	<?php break; case 'form':  ?>
		<?php $this->assign('popup_form', 0); ?>
		<?php $this->assign('full_form', 1); ?>
		<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->module_path. "listings". $this->module_templates.  $this->get_current_theme_gid('"default"', '"listings"'). "order_form.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
	<?php break; default:  ?>
		<?php $this->assign('popup_form', 0); ?>
		<?php $this->assign('full_form', 0); ?>
		<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->module_path. "listings". $this->module_templates.  $this->get_current_theme_gid('"default"', '"listings"'). "order_form.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
  break; endswitch;  echo $this->_vars['separator']; ?>

<?php echo tpl_function_js(array('file' => 'booking-form.js','module' => 'listings'), $this);?>
<script><?php echo '
	$(function(){
		new bookingForm({
			siteUrl: \'';  echo $this->_vars['site_url'];  echo '\',
			bookingBtn: \'booking_btn';  echo $this->_vars['rand'];  echo '\',
			isPopup: ';  echo $this->_vars['popup_form'];  echo ',
			';  if ($this->_vars['show_link']): ?>showCalendarLink: true,<?php endif;  echo '
			cFormId: \'';  if ($this->_vars['popup_form']): ?>order_form<?php else: ?>period_form<?php echo $this->_vars['rand'];  endif;  echo '\',
			listingId: \'';  echo $this->_vars['booking_listing']['id'];  echo '\',
			priceType: \'';  echo $this->_vars['booking_listing']['price_period'];  echo '\',
			';  if ($this->_vars['is_guest']): ?>displayLogin: true,<?php endif;  echo '
			';  if ($this->_vars['show_price']): ?>showPrice: true,<?php endif;  echo '
			';  if ($this->_vars['noSavePeriod']): ?>isSavePeriod: false,<?php endif;  echo '
		});
	});
'; ?>
</script>
