<?php require_once('/opt/ip/system/libraries/template_lite/plugins/function.js.php'); $this->register_function("js", "tpl_function_js");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.block.php'); $this->register_function("block", "tpl_function_block");  require_once('/opt/ip/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  require_once('/opt/ip/system/libraries/template_lite/plugins/block.strip.php'); $this->register_block("strip", "tpl_block_strip");  require_once('/opt/ip/system/libraries/template_lite/plugins/block.capture.php'); $this->register_block("capture", "tpl_block_capture");  require_once('/opt/ip/system/libraries/template_lite/plugins/modifier.escape.php'); $this->register_modifier("escape", "tpl_modifier_escape");  require_once('/opt/ip/system/libraries/template_lite/plugins/modifier.truncate.php'); $this->register_modifier("truncate", "tpl_modifier_truncate");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.seolink.php'); $this->register_function("seolink", "tpl_function_seolink");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.pagination.php'); $this->register_function("pagination", "tpl_function_pagination");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.sorter.php'); $this->register_function("sorter", "tpl_function_sorter");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-12-01 14:37:08 KRAT */ ?>

	<?php if ($this->_vars['listings']): ?>
	<div class="sorter line" id="sorter_block">
		<?php echo tpl_function_sorter(array('links' => $this->_vars['sort_data']['links'],'order' => $this->_vars['sort_data']['order'],'direction' => $this->_vars['sort_data']['direction'],'url' => $this->_vars['sort_data']['url']), $this);?>
		<div class="fright" id="pages_block_1"><?php echo tpl_function_pagination(array('data' => $this->_vars['page_data'],'type' => 'cute'), $this);?></div>
	</div>
	<?php endif; ?>

	<div class="listing-block">
	<?php if (is_array($this->_vars['listings']) and count((array)$this->_vars['listings'])): foreach ((array)$this->_vars['listings'] as $this->_vars['item']): ?>
		<div id="item-block-<?php echo $this->_vars['item']['id']; ?>
" class="item listing">
			<h3><a href="<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'view','data' => $this->_vars['item']), $this);?>"><?php echo $this->_run_modifier($this->_vars['item']['output_name'], 'truncate', 'plugin', 1, 100); ?>
</a></h3>
			<div class="image">
				<a href="<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'view','data' => $this->_vars['item']), $this);?>">
					<img src="<?php echo $this->_vars['item']['media']['photo']['thumbs']['middle']; ?>
" title="<?php echo $this->_run_modifier($this->_vars['item']['output_name'], 'escape', 'plugin', 1); ?>
">
					<?php if ($this->_vars['item']['photo_count'] || $this->_vars['item']['is_vtour']): ?>
					<div class="photo-info">
						<div class="panel">
							<?php if ($this->_vars['item']['photo_count']): ?><span class="btn-link"><ins class="with-icon-small w i-photo"></ins> <?php echo $this->_vars['item']['photo_count']; ?>
</span><?php endif; ?>
							<?php if ($this->_vars['item']['is_vtour']): ?><span class="btn-link"><ins class="with-icon-small w i-vtour"></ins> 360&deg;</span><?php endif; ?>
						</div>
						<div class="background"></div>
					</div>
					<?php endif; ?>
				</a>
			</div>
			<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'status_info')); tpl_block_capture(array('assign' => 'status_info'), null, $this); ob_start();  $this->_tag_stack[] = array('tpl_block_strip', array()); tpl_block_strip(array(), null, $this); ob_start(); ?>
				<?php if ($this->_vars['item']['is_lift_up'] || $this->_vars['location_filter'] == 'country' && $this->_vars['item']['is_lift_up_country'] || $this->_vars['location_filter'] == 'region' && ( $this->_vars['item']['is_lift_up_country'] || $this->_vars['item']['is_lift_up_region'] ) || $this->_vars['location_filter'] == 'city' && ( $this->_vars['item']['is_lift_up_country'] || $this->_vars['item']['is_lift_up_region'] || $this->_vars['item']['is_lift_up_city'] ) || $this->_vars['sort_data']['order'] != 'default' && ( $this->_vars['item']['is_lift_up'] || $this->_vars['item']['is_lift_up_country'] || $this->_vars['item']['is_lift_up_region'] || $this->_vars['item']['is_lift_up_city'] )): ?>
					<?php echo l('status_lift_up', 'listings', '', 'text', array()); ?>
				<?php elseif ($this->_vars['item']['is_featured']): ?>
					<?php echo l('status_featured', 'listings', '', 'text', array()); ?>
				<?php endif; ?>
			<?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_strip($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack);  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
			<div class="body">
				<?php 
$this->assign('no_info_str', l('no_information', 'start', '', 'text', array()));
 ?>
				<h3><?php echo tpl_function_block(array('name' => listing_price_block,'module' => 'listings','data' => $this->_vars['item']), $this);?></h3>
				<div class="t-1">
					<?php echo $this->_vars['item']['property_type_str']; ?>
 <?php echo $this->_vars['item']['operation_type_str']; ?>

					<br><?php echo $this->_run_modifier($this->_vars['item']['square_output'], 'truncate', 'plugin', 1, 30); ?>

					<?php if ($this->_run_modifier($this->_vars['item']['date_open'], 'strtotime', 'PHP', 1) >= $this->_vars['tstamp']): ?><br><?php echo l('text_date_open_status', 'listings', '', 'text', array());  endif; ?>
					<?php if ($this->_vars['item']['sold']): ?><br><span class="status_text"><?php echo l('text_sold', 'listings', '', 'text', array()); ?></span><?php endif; ?>
					<?php if ($this->_vars['status_info']): ?><br><span class="status_text"><?php echo $this->_vars['status_info']; ?>
</span><?php endif; ?>
				</div>
				<div class="t-2">
				</div>
				<div class="t-3">
					<?php echo $this->_run_modifier($this->_vars['item']['user']['output_name'], 'truncate', 'plugin', 1, 50); ?>

				</div>
				<div class="t-4">
					<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'user_logo')); tpl_block_capture(array('assign' => 'user_logo'), null, $this); ob_start(); ?>
						<?php if ($this->_vars['item']['user']['is_show_logo']): ?>
						<img src="<?php echo $this->_vars['item']['user']['media']['user_logo']['thumbs']['small']; ?>
" title="<?php echo $this->_vars['item']['user']['output_name']; ?>
">
						<?php else: ?>
						<img src="<?php echo $this->_vars['item']['user']['media']['default_logo']['thumbs']['small']; ?>
" title="<?php echo $this->_vars['item']['user']['output_name']; ?>
">
						<?php endif; ?>
					<?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
					<?php if ($this->_vars['item']['user']['status']): ?>
					<a href="<?php echo tpl_function_seolink(array('module' => 'users','method' => 'view','data' => $this->_vars['item']['user']), $this);?>"><?php echo $this->_vars['user_logo']; ?>
</a>
					<?php else: ?>
					<?php echo $this->_vars['user_logo']; ?>

					<?php endif; ?>
				</div>
			</div>
			<div class="clr"></div>
			<?php if ($this->_vars['item']['headline']): ?><p class="headline" title="<?php echo $this->_vars['item']['headline']; ?>
"><?php echo $this->_run_modifier($this->_vars['item']['headline'], 'truncate', 'plugin', 1, 100); ?>
</p><?php endif; ?>
			<div class="actions">
				<a href="<?php echo $this->_vars['site_url']; ?>
listings/delete_saved/<?php echo $this->_vars['item']['id']; ?>
" onclick="javascript: if(!confirm('<?php echo l('note_delete_listing', 'listings', '', 'js', array()); ?>')) return false;"><?php echo l('link_delete_saved', 'listings', '', 'text', array()); ?></a> |
				<a href="<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'view','data' => $this->_vars['item']), $this);?>"><?php echo l('link_details', 'listings', '', 'text', array()); ?></a>
			</div>
		</div>
	<?php endforeach; else: ?>
		<div class="item empty"><?php echo l('no_listings', 'listings', '', 'text', array()); ?></div>
	<?php endif; ?>
	</div>
	<?php if ($this->_vars['listings']): ?><div id="pages_block_2"><?php echo tpl_function_pagination(array('data' => $this->_vars['page_data'],'type' => 'full'), $this);?></div><?php endif; ?>
	<?php echo tpl_function_js(array('file' => 'change_link_action.js'), $this);?>
	<script><?php echo '
		var rMenu;
		$(function(){
			link_action = new change_link_action({
				siteUrl: \'';  echo $this->_vars['site_url'];  echo '\',
				viewUrl: \'';  echo $this->_vars['site_url'];  echo 'listings/delete_saved/';  echo $this->_vars['listing_id'];  echo '\',
				viewAjaxUrl: \'';  echo $this->_vars['site_url'];  echo 'listings/ajax_delete_saved/';  echo $this->_vars['listing_id'];  echo '\',
				listBlockParam: \'.del_saved\',
				successCallBack: function(obj){
					$.get(\'';  echo $this->_vars['site_url'];  echo 'listings/ajax_saved\', {}, function(data){
						$(\'#listings_block\').html(data);
					});
				}
			});
		});
	'; ?>
</script>
