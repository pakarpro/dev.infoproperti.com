<?php require_once('/opt/ip/system/libraries/template_lite/plugins/function.country_select.php'); $this->register_function("country_select", "tpl_function_country_select");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.seolink.php'); $this->register_function("seolink", "tpl_function_seolink");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-02-06 14:59:35 KRAT */ ?>

<?php if ($this->_vars['action'] == 'personal'): ?>
<h1><?php echo l('table_header_personal', 'users', '', 'text', array()); ?></h1>
<div class="edit_block">
<form action="" method="post" enctype="multipart/form-data">
	<div class="r">
		<div class="f"><?php echo l('field_fname', 'users', '', 'text', array()); ?>:&nbsp;<span class="ast">*</span> </div>
		<div class="v"><input type="text" name="data[fname]" value="<?php echo $this->_run_modifier($this->_vars['data']['fname'], 'escape', 'plugin', 1); ?>
"></div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_sname', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><input type="text" name="data[sname]" value="<?php echo $this->_run_modifier($this->_vars['data']['sname'], 'escape', 'plugin', 1); ?>
"></div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_phone', 'users', '', 'text', array()); ?>:&nbsp;<span class="ast">*</span> </div>
		<div class="v"><input type="text" name="data[phone]" value="<?php echo $this->_run_modifier($this->_vars['data']['phone'], 'escape', 'plugin', 1); ?>
" id="phone"></div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_company', 'users', '', 'text', array()); ?>:&nbsp;<span class="ast">*</span> </div>
		<div class="v"><input type="text" name="data[company_name]" value="<?php echo $this->_run_modifier($this->_vars['data']['company_name'], 'escape', 'plugin', 1); ?>
"></div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_icon', 'users', '', 'text', array()); ?>: </div>
		<div class="v">
			<input type="file" name="user_icon">
			<?php if ($this->_vars['data']['user_logo'] || $this->_vars['data']['user_logo_moderation']): ?>
			<br><input type="checkbox" name="user_icon_delete" value="1" id="uichb"><label for="uichb"><?php echo l('field_icon_delete', 'users', '', 'text', array()); ?></label><br>
			<?php if ($this->_vars['data']['user_logo_moderation']): ?><img src="<?php echo $this->_vars['data']['media']['user_logo_moderation']['thumbs']['middle']; ?>
"><?php else: ?><img src="<?php echo $this->_vars['data']['media']['user_logo']['thumbs']['middle']; ?>
"><?php endif; ?>
			<?php endif; ?>
		</div>
	</div>
	<div class="r">
		<div class="f">&nbsp;</div>
		<div class="v">
			<input type="submit" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>" name="btn_register" class="btn" />
			<a href="<?php echo tpl_function_seolink(array('module' => 'users','method' => 'profile'), $this);?>" class="btn-link fleft"><ins class="with-icon i-larr"></ins><?php echo l('back_to_my_profile', 'users', '', 'text', array()); ?></a>
		</div>
	</div>
	</form>
</div>	
<?php endif; ?>

<?php if ($this->_vars['action'] == 'contact'): ?>
<h1><?php echo l('table_header_contact', 'users', '', 'text', array()); ?></h1>
<div class="edit_block">
<form action="" method="post" enctype="multipart/form-data">
	<div class="r">
		<div class="f"><?php echo l('field_contact_email', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><input type="text" name="data[contact_email]" value="<?php echo $this->_run_modifier($this->_vars['data']['contact_email'], 'escape', 'plugin', 1); ?>
"></div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_contact_phone', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><input type="text" name="data[contact_phone]" value="<?php echo $this->_run_modifier($this->_vars['data']['contact_phone'], 'escape', 'plugin', 1); ?>
" class="phone-field"></div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_contact_info', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><textarea rows="5" cols="80" name="data[contact_info]"><?php echo $this->_run_modifier($this->_vars['data']['contact_info'], 'escape', 'plugin', 1); ?>
</textarea></div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_web_url', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><input type="text" name="data[web_url]" value="<?php echo $this->_run_modifier($this->_vars['data']['web_url'], 'escape', 'plugin', 1); ?>
"></div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_facebook', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><input type="text" name="data[facebook]" value="<?php echo $this->_run_modifier($this->_vars['data']['facebook'], 'escape', 'plugin', 1); ?>
"></div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_twitter', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><input type="text" name="data[twitter]" value="<?php echo $this->_run_modifier($this->_vars['data']['twitter'], 'escape', 'plugin', 1); ?>
"></div>
	</div>
    <!--
	<div class="r">
		<div class="f"><?php echo l('field_vkontakte', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><input type="text" name="data[vkontakte]" value="<?php echo $this->_run_modifier($this->_vars['data']['vkontakte'], 'escape', 'plugin', 1); ?>
"></div>
	</div>
    -->
	<div class="r">
		<div class="f"><?php echo l('field_working_days', 'users', '', 'text', array()); ?>: </div>
		<div class="v">
			<?php if (is_array($this->_vars['weekdays']['option']) and count((array)$this->_vars['weekdays']['option'])): foreach ((array)$this->_vars['weekdays']['option'] as $this->_vars['key'] => $this->_vars['item']): ?>
			<input type="checkbox" name="data[working_days][]" value="<?php echo $this->_run_modifier($this->_vars['key'], 'escape', 'plugin', 1); ?>
" id="working_days_<?php echo $this->_run_modifier($this->_vars['key'], 'escape', 'plugin', 1); ?>
" <?php if ($this->_vars['data']['working_days'] && $this->_run_modifier($this->_vars['key'], 'in_array', 'PHP', 1, $this->_vars['data']['working_days'])): ?>checked="checked"<?php endif; ?> />
			<label for="working_days_<?php echo $this->_run_modifier($this->_vars['key'], 'escape', 'plugin', 1); ?>
"><?php echo $this->_vars['item']; ?>
</label>
			<?php endforeach; endif; ?>
		</div>
	</div>	
	<div class="r">
		<div class="f"><?php echo l('field_working_hours', 'users', '', 'text', array()); ?>: </div>
		<div class="v">
			<?php echo l('text_from', 'users', '', 'text', array()); ?>:
			<select name="data[working_hours_begin]" class="middle">
				<option value="" <?php if (! $this->_vars['data']['workings_hours_begin']): ?>selected<?php endif; ?>><?php echo $this->_vars['dayhours']['header']; ?>
</option>
				<?php if (is_array($this->_vars['dayhours']['option']) and count((array)$this->_vars['dayhours']['option'])): foreach ((array)$this->_vars['dayhours']['option'] as $this->_vars['key'] => $this->_vars['item']): ?>
				<option value="<?php echo $this->_run_modifier($this->_vars['key'], 'escape', 'plugin', 1); ?>
" <?php if ($this->_vars['data']['working_hours_begin'] == $this->_vars['key']): ?>selected<?php endif; ?>><?php echo $this->_vars['item']; ?>
</div>
				<?php endforeach; endif; ?>
			</select>
			<?php echo l('text_till', 'users', '', 'text', array()); ?>:
			<select name="data[working_hours_end]" class="middle">
				<option value="" <?php if (! $this->_vars['data']['workings_hours_end']): ?>selected<?php endif; ?>><?php echo $this->_vars['dayhours']['header']; ?>
</option>
				<?php if (is_array($this->_vars['dayhours']['option']) and count((array)$this->_vars['dayhours']['option'])): foreach ((array)$this->_vars['dayhours']['option'] as $this->_vars['key'] => $this->_vars['item']): ?>
				<option value="<?php echo $this->_run_modifier($this->_vars['key'], 'escape', 'plugin', 1); ?>
" <?php if ($this->_vars['data']['working_hours_end'] == $this->_vars['key']): ?>selected<?php endif; ?>><?php echo $this->_vars['item']; ?>
</div>
				<?php endforeach; endif; ?>
			</select>
		</div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_lunch_time', 'users', '', 'text', array()); ?>: </div>
		<div class="v">
			<?php echo l('text_from', 'users', '', 'text', array()); ?>:
			<select name="data[lunch_time_begin]" class="middle">
				<option value="" <?php if (! $this->_vars['data']['lunch_time_begin']): ?>selected<?php endif; ?>><?php echo $this->_vars['dayhours']['header']; ?>
</option>
				<?php if (is_array($this->_vars['dayhours']['option']) and count((array)$this->_vars['dayhours']['option'])): foreach ((array)$this->_vars['dayhours']['option'] as $this->_vars['key'] => $this->_vars['item']): ?>
				<option value="<?php echo $this->_run_modifier($this->_vars['key'], 'escape', 'plugin', 1); ?>
" <?php if ($this->_vars['data']['lunch_time_begin'] == $this->_vars['key']): ?>selected<?php endif; ?>><?php echo $this->_vars['item']; ?>
</div>
				<?php endforeach; endif; ?>
			</select>
			<?php echo l('text_till', 'users', '', 'text', array()); ?>:
			<select name="data[lunch_time_end]" class="middle">
				<option value="" <?php if (! $this->_vars['data']['lunch_time_end']): ?>selected<?php endif; ?>><?php echo $this->_vars['dayhours']['header']; ?>
</option>
				<?php if (is_array($this->_vars['dayhours']['option']) and count((array)$this->_vars['dayhours']['option'])): foreach ((array)$this->_vars['dayhours']['option'] as $this->_vars['key'] => $this->_vars['item']): ?>
				<option value="<?php echo $this->_run_modifier($this->_vars['key'], 'escape', 'plugin', 1); ?>
" <?php if ($this->_vars['data']['lunch_time_end'] == $this->_vars['key']): ?>selected<?php endif; ?>><?php echo $this->_vars['item']; ?>
</div>
				<?php endforeach; endif; ?>
			</select>
		</div>
	</div>
	<div class="r">
		<div class="f">&nbsp;</div>
		<div class="v">
			<input type="submit" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>" name="btn_register" class="btn" />
			<a href="<?php echo tpl_function_seolink(array('module' => 'users','method' => 'profile'), $this);?>" class="btn-link fleft"><ins class="with-icon i-larr"></ins><?php echo l('back_to_my_profile', 'users', '', 'text', array()); ?></a>
		</div>
	</div>
	</form>
</div>	
<?php endif; ?>

<?php if ($this->_vars['action'] == 'location'): ?>
<h1><?php echo l('table_header_location', 'users', '', 'text', array()); ?></h1>
<div class="edit_block">
	<form action="" method="post" enctype="multipart/form-data">
	<div class="r">
		<div class="f"><?php echo l('field_region', 'users', '', 'text', array()); ?>: </div>
		<div class="v">
			<?php echo tpl_function_country_select(array('select_type' => 'city','id_country' => $this->_vars['data']['id_country'],'id_region' => $this->_vars['data']['id_region'],'id_city' => $this->_vars['data']['id_city'],'var_country' => 'data[id_country]','var_region' => 'data[id_region]','var_city' => 'data[id_city]','var_country_name' => 'country_name','var_region_name' => 'region_name','var_city_name' => 'city_name'), $this);?>
		</div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_address', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><input type="text" name="data[address]" value="<?php echo $this->_run_modifier($this->_vars['data']['address'], 'escape', 'plugin', 1); ?>
"></div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_postal_code', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><input type="text" name="data[postal_code]" value="<?php echo $this->_run_modifier($this->_vars['data']['postal_code'], 'escape', 'plugin', 1); ?>
"></div>
	</div>	
	<div class="r">
		<div class="f"><?php echo l('field_contact_map', 'users', '', 'text', array()); ?>:</div>
		<div class="v"><?php echo tpl_function_block(array('name' => show_default_map,'module' => geomap,'object_id' => $this->_vars['data']['id'],'gid' => 'user_profile','markers' => $this->_vars['markers'],'settings' => $this->_vars['map_settings'],'map_id' => user_map,'width' => '630','height' => '400'), $this);?></div>
	</div>
	<div class="r">
		<div class="f">&nbsp;</div>
		<div class="v">
			<input type="submit" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>" name="btn_register" class="btn" />
			<a href="<?php echo tpl_function_seolink(array('module' => 'users','method' => 'profile'), $this);?>" class="btn-link fleft"><ins class="with-icon i-larr"></ins><?php echo l('back_to_my_profile', 'users', '', 'text', array()); ?></a>
		</div>
	</div>
	<input type="hidden" name="map[view_type]" value="<?php echo $this->_vars['user_map_settings']['view_type']; ?>
" id="map_type">
	<input type="hidden" name="map[zoom]" value="<?php echo $this->_vars['user_map_settings']['zoom']; ?>
" id="map_zoom">
	<input type="hidden" name="data[lat]" value="<?php echo $this->_run_modifier($this->_vars['data']['lat'], 'escape', 'plugin', 1); ?>
" id="lat">
	<input type="hidden" name="data[lon]" value="<?php echo $this->_run_modifier($this->_vars['data']['lon'], 'escape', 'plugin', 1); ?>
" id="lon">
	</form>
</div>
<?php echo tpl_function_block(array('name' => geomap_load_geocoder,'module' => 'geomap'), $this);?>
<script><?php echo '
	function update_coordinates(country, region, city, address, postal_code){
		if(typeof(geocoder) != \'undefined\'){
			var location = geocoder.getLocationFromAddress(country, region, city, address, postal_code);
			geocoder.geocodeLocation(location, function(latitude, longitude){
				$(\'#lat\').val(latitude);
				$(\'#lon\').val(longitude);
				user_map.moveMarkers(latitude, longitude);
			});	
		}
	}
	$(function(){
		var location_change_wait = 0;
		var country_old = \'';  echo $this->_vars['data']['id_country'];  echo '\';
		var region_old = \'';  echo $this->_vars['data']['id_region'];  echo '\';
		var city_old = \'';  echo $this->_vars['data']['id_city'];  echo '\';
		var address_old = \'';  echo $this->_run_modifier('\'', 'str_replace', 'PHP', 1, '\\\'', $this->_vars['data']['address']);  echo '\';
		var postal_code_old = \'';  echo $this->_run_modifier("'", 'str_replace', 'PHP', 1, "\'", $this->_vars['data']['postal_code']);  echo '\';
		
		$(\'input[name=data\\\\[id_city\\\\]]\').bind(\'change\', function(){
			var city = $(this).val();
			if(city == 0) return;
			location_change_wait++;
			check_address_updated();
		});
		
		$(\'input[name=data\\\\[address\\\\]], input[name=data\\\\[postal_code\\\\]]\').bind(\'keypress\', function(){
			location_change_wait++;
			setTimeout(check_address_updated, 1000);
		});
		
		function check_address_updated(){
			location_change_wait--;
			if(location_change_wait) return;
			var country = $(\'input[name=data\\\\[id_country\\\\]]\').val();
			var region = $(\'input[name=data\\\\[id_region\\\\]]\').val();
			var city = $(\'input[name=data\\\\[id_city\\\\]]\').val();
			var address = $(\'input[name=data\\\\[address\\\\]]\').val();
			var postal_code = $(\'input[name=data\\\\[postal_code\\\\]]\').val();
			if(country == country_old && region == region_old && 
				city == city_old && address == address_old && postal_code == postal_code_old) return;
			country_old = country;
			region_old = region;
			city_old = city;
			address_old = address;
			postal_code_old = postal_code;
			var country_name = $(\'input[name=data\\\\[id_country\\\\]]\').attr(\'data-name\');
			var region_name = $(\'input[name=data\\\\[id_region\\\\]]\').attr(\'data-name\');
			var city_name = $(\'input[name=data\\\\[id_city\\\\]]\').attr(\'data-name\');
			update_coordinates(country_name, region_name, city_name, address, postal_code);
		}
	});
'; ?>
</script>
<?php endif; ?>

<?php if ($this->_vars['action'] == 'subscriptions'): ?>
<h1><?php echo l('table_header_subscriptions', 'users', '', 'text', array()); ?></h1>
<div class="edit_block">
	<form action="" method="post" enctype="multipart/form-data">
	<?php echo tpl_function_helper(array('func_name' => get_user_subscriptions_form,'module' => subscriptions,'func_param' => profile), $this);?>
	<div class="r">
		<div class="f">&nbsp;</div>
		<div class="v">
			<input type="submit" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>" name="btn_register" class="btn" />
			<a href="<?php echo tpl_function_seolink(array('module' => 'users','method' => 'profile'), $this);?>" class="btn-link fleft"><ins class="with-icon i-larr"></ins><?php echo l('back_to_my_profile', 'users', '', 'text', array()); ?></a>
		</div>
	</div>
	</form>
</div>
<?php endif; ?>

<?php if ($this->_vars['phone_format']): ?>
<?php echo tpl_function_js(array('file' => 'jquery.maskedinput.min.js'), $this);?>
<script><?php echo '
$(function(){
	$(\'#phone\').mask(\'';  echo $this->_vars['phone_format'];  echo '\');
});
'; ?>
</script>
<?php endif; ?>
<script><?php echo '
if(typeof(get_user_type_data) == \'undefined\'){
	function get_user_type_data(type){
		$(\'#map_type\').val(type);
	}
}
if(typeof(get_user_zoom_data) == \'undefined\'){
	function get_user_zoom_data(zoom){
		$(\'#map_zoom\').val(zoom);
	}
}
if(typeof(get_user_drag_data) == \'undefined\'){
	function get_user_drag_data(point_gid, lat, lon){
		$(\'#lat\').val(lat);
		$(\'#lon\').val(lon);
	}
}
'; ?>
</script>
