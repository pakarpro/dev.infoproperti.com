<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2014-06-24 07:37:07 KRAT */ ?>

<h2><?php echo l('header_'.$this->_vars['type'].'_categories_search_block', 'listings', '', 'text', array()); ?></h2>
<div class="listing-property_types-block"><div class="inside">
	<?php if (is_array($this->_vars['listings_categories']) and count((array)$this->_vars['listings_categories'])): foreach ((array)$this->_vars['listings_categories'] as $this->_vars['categories']): ?>
	<ul>
		<?php if (is_array($this->_vars['categories']) and count((array)$this->_vars['categories'])): foreach ((array)$this->_vars['categories'] as $this->_vars['key'] => $this->_vars['item']): ?>
		<li class="<?php if ($this->_vars['item']['group']): ?>group<?php endif; ?> <?php if ($this->_vars['item']['empty']): ?>empty<?php endif; ?>">
			<?php if ($this->_vars['item']['group']): ?>
			<h3><?php echo $this->_vars['item']['name']; ?>
</h3>
			<?php elseif ($this->_vars['item']['statistics']['listings_active']): ?>
			<a href="<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'category','data' => $this->_vars['item']['data']), $this);?>"><?php echo $this->_vars['item']['name']; ?>
 (<?php echo $this->_vars['item']['statistics']['listings_active']; ?>
)</a>
			<?php else: ?>
			<?php echo $this->_vars['item']['name']; ?>
	
			<?php endif; ?>
		</li>
		<?php endforeach; endif; ?>
	</ul>
	<?php endforeach; endif; ?>
</div></div>
