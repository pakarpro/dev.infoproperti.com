<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.checkbox.php'); $this->register_function("checkbox", "tpl_function_checkbox");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/block.capture.php'); $this->register_block("capture", "tpl_block_capture");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.ld.php'); $this->register_function("ld", "tpl_function_ld");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.date_format.php'); $this->register_modifier("date_format", "tpl_modifier_date_format");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.country_input.php'); $this->register_function("country_input", "tpl_function_country_input");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.selectbox.php'); $this->register_function("selectbox", "tpl_function_selectbox");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/block.strip.php'); $this->register_block("strip", "tpl_block_strip");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-05-12 14:13:48 KRAT */ ?>

<script> 
	var selects_<?php echo $this->_vars['form_settings']['rand']; ?>
 = []; 
	var checkboxes_<?php echo $this->_vars['form_settings']['rand']; ?>
 = []; 
	var form_selects_<?php echo $this->_vars['form_settings']['rand']; ?>
 = []; 
	var form_checkboxes_<?php echo $this->_vars['form_settings']['rand']; ?>
 = []; 
	var selectObject_<?php echo $this->_vars['form_settings']['rand']; ?>
;
	var checkboxObject_<?php echo $this->_vars['form_settings']['rand']; ?>
;
	var formSelectObject_<?php echo $this->_vars['form_settings']['rand']; ?>
;
	var formCheckboxObject_<?php echo $this->_vars['form_settings']['rand']; ?>
;
</script>
<!-- Inline Search -->
<div class="hr-line">
	<img src="<?php echo $this->_vars['site_root']; ?>
application/views/default/custom-banner/inner-banner-1.jpg" width="980" height="90" />
</div>
<form action="" method="<?php if ($this->_vars['listings_page_data']['type'] == 'line'): ?>GET<?php else: ?>POST<?php endif; ?>" id="main_search_form_<?php echo $this->_vars['form_settings']['rand']; ?>
">
<div class="search-form <?php echo $this->_run_modifier($this->_vars['listings_page_data']['type'], 'escape', 'plugin', 1); ?>
">
	<div class="inside">
	<h2 id="inner-search-title">Temukan properti <span class="italic">ideal</span> anda</h2>
	<?php if ($this->_vars['data']['id_category'] && $this->_vars['data']['property_type']): ?>
		<?php $this->assign('property_type_value', $this->_vars['data']['id_category'].'_'.$this->_vars['data']['property_type']); ?>
	<?php else: ?>
		<?php $this->assign('property_type_value', $this->_vars['data']['id_category']); ?>
	<?php endif; ?>
	<?php if ($this->_vars['listings_page_data']['type'] == 'line'): ?>
		<?php 
$this->assign('default_select_lang', l('field_search_category', 'listings', '', 'text', array()));
 ?>
		<div id="line-search-form-<?php echo $this->_vars['form_settings']['rand']; ?>
">
			<?php $this->_tag_stack[] = array('tpl_block_strip', array()); tpl_block_strip(array(), null, $this); ob_start(); ?>
			<span>
			<input type="text" name="filters[keyword]" value="<?php echo $this->_run_modifier($this->_vars['data']['keyword'], 'escape', 'plugin', 1); ?>
" placeholder="<?php echo l('field_search_country', 'listings', '', 'text', array()); ?>">
			<?php echo tpl_function_selectbox(array('input' => 'filters[category]','id' => 'id_category_select_'.$this->_vars['form_settings']['rand'],'value' => $this->_vars['property_types'],'subvalue' => $this->_vars['property_items'],'selected' => $this->_vars['property_type_value'],'default' => $this->_vars['default_select_lang']), $this);?>
			<script> selectDropdownClass_<?php echo $this->_vars['form_settings']['rand']; ?>
 = 'dropdown right'; selects_<?php echo $this->_vars['form_settings']['rand']; ?>
.push('id_category_select_<?php echo $this->_vars['form_settings']['rand']; ?>
'); </script>
			<input type="submit" class="search with-icon-small i-search w no-hover" id="main_search_button_<?php echo $this->_vars['form_settings']['rand']; ?>
" value="">
			</span>
			<!-- <a href="<?php echo $this->_vars['search_action_link']; ?>
" class="options"><?php echo l('link_more_options', 'start', '', 'text', array()); ?></a> -->
			<?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_strip($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
		</div>
	<?php elseif ($this->_vars['listings_page_data']['type'] == 'sliderx'):  /* rename the "slider" to "sliderx" to disable standard/mini search form */ ?>
		<?php 
$this->assign('default_select_lang', l('select_default', 'start', '', 'text', array()));
 ?>
		<div class="btn-block">
			<div id="search-preresult-<?php echo $this->_vars['form_settings']['rand']; ?>
" class="preload"></div>
			<div class="search-btn">
				<ins class="with-icon-small i-lupe"></ins>
				<input type="submit" id="main_search_button_<?php echo $this->_vars['form_settings']['rand']; ?>
" name="search_button" value="<?php echo l('btn_search', 'start', '', 'button', array()); ?>">
			</div>
		</div>
		<div class="fields-block">
			<div id="slider-search-form-<?php echo $this->_vars['form_settings']['rand']; ?>
">
				<div>
					<div class="search-field country">
					<?php 
$this->assign('location_default', l('field_search_country', 'listings', '', 'text', array()));
 ?>
					<?php echo tpl_function_country_input(array('select_type' => 'city','id_country' => $this->_vars['data']['id_country'],'id_region' => $this->_vars['data']['id_region'],'id_city' => $this->_vars['data']['id_city'],'default' => $this->_vars['location_default'],'var_country' => 'filters[id_country]','var_region' => 'filters[id_region]','var_city' => 'filters[id_city]'), $this);?>
				</div>				
				 <div class="search-field category">
					<?php 
$this->assign('property_type_default', l('field_search_category', 'listings', '', 'text', array()));
 ?>
					<?php echo tpl_function_selectbox(array('input' => 'filters[category]','id' => 'id_category_select_'.$this->_vars['form_settings']['rand'],'value' => $this->_vars['property_types'],'subvalue' => $this->_vars['property_items'],'selected' => $this->_vars['property_type_value'],'default' => $this->_vars['property_type_default']), $this);?>
					<script> selects_<?php echo $this->_vars['form_settings']['rand']; ?>
.push('id_category_select_<?php echo $this->_vars['form_settings']['rand']; ?>
'); </script>
				</div>
			</div>
			</div>
			<div class="clr"></div>
		</div>
		<input type="hidden" name="filters[type]" value="<?php echo $this->_run_modifier($this->_vars['form_settings']['object'], 'escape', 'plugin', 1); ?>
" id="operation_type<?php echo $this->_vars['form_settings']['rand']; ?>
">
		<input type="hidden" name="new" value="1">
	<?php else: ?>
		<?php 
$this->assign('default_select_lang', l('field_search_category', 'listings', '', 'text', array()));
 ?>
		<div class="btn-block">
			<div id="search-preresult-<?php echo $this->_vars['form_settings']['rand']; ?>
" class="preload"></div>
			<div class="search-btn">
				<ins class="with-icon-small i-lupe"></ins>
				<input type="submit" id="main_search_button_<?php echo $this->_vars['form_settings']['rand']; ?>
" name="search_button" value="<?php echo l('btn_search', 'start', '', 'button', array()); ?>">
			</div>
			<?php /* *********************** DISABLE THE MORE OPTION AND LESS OPTIONS LINKS ON THE SEARCH BAR ********************************
			<a href="#" id="more-options-link-<?php echo $this->_vars['form_settings']['rand']; ?>
" <?php if ($this->_vars['listings_page_data']['type'] != 'short'): ?>class="hidex"<?php endif; ?>><?php echo l('link_more_options', 'start', '', 'text', array()); ?></a>
			<a href="#" id="less-options-link-<?php echo $this->_vars['form_settings']['rand']; ?>
" <?php if ($this->_vars['listings_page_data']['type'] == 'short'): ?>class="hidex"<?php endif; ?>><?php echo l('link_less_options', 'start', '', 'text', array()); ?></a>
			*/?>
		</div>
		<div class="fields-block">
			<div id="short-search-form-<?php echo $this->_vars['form_settings']['rand']; ?>
">
				<div class="search-field country">
					<p><?php echo l('field_search_country', 'listings', '', 'text', array()); ?></p>
					<?php echo tpl_function_country_input(array('select_type' => 'city','id_country' => $this->_vars['data']['id_country'],'id_region' => $this->_vars['data']['id_region'],'id_city' => $this->_vars['data']['id_city'],'var_country' => 'filters[id_country]','var_region' => 'filters[id_region]','var_city' => 'filters[id_city]'), $this);?>
				</div>				
				<div class="search-field category">
					<p><?php echo l('field_search_category', 'listings', '', 'text', array()); ?></p>
					<?php echo tpl_function_selectbox(array('input' => 'filters[category]','id' => 'id_category_select_'.$this->_vars['form_settings']['rand'],'value' => $this->_vars['property_types'],'subvalue' => $this->_vars['property_items'],'selected' => $this->_vars['property_type_value'],'default' => $this->_vars['default_select_lang']), $this);?>
					<script>selects_<?php echo $this->_vars['form_settings']['rand']; ?>
.push('id_category_select_<?php echo $this->_vars['form_settings']['rand']; ?>
');</script>
					<script><?php echo '
							
						$(function(){
							$(\'#id_category_select_';  echo $this->_vars['form_settings']['rand'];  echo '\').bind(\'change\', function(){
								$.ajax({
									//origin source
									url: \'';  echo $this->_vars['site_url']; ?>
listings/ajax_get_main_search_form/<?php echo $this->_vars['form_settings']['rand'];  echo '\',
									type: \'POST\',
									data: {type: \'';  echo $this->_vars['form_settings']['object'];  echo '\', category: $(this).val()},
									cache: false,
									dataType: \'json\',
									success: function(data){
										form_selects_';  echo $this->_vars['form_settings']['rand'];  echo ' = [];
										form_checkboxes_';  echo $this->_vars['form_settings']['rand'];  echo ' = [];
										$(\'#main_search_form_';  echo $this->_vars['form_settings']['rand'];  echo '\').find(\'.custom\').remove();
										if(formSelectObject_';  echo $this->_vars['form_settings']['rand'];  echo ') formSelectObject_';  echo $this->_vars['form_settings']['rand'];  echo '.clear();
										$(\'#short-search-form-';  echo $this->_vars['form_settings']['rand'];  echo '\').append(data.short);	
										$(\'#full-search-form-';  echo $this->_vars['form_settings']['rand'];  echo '\').prepend(data.full);	
										formSelectObject_';  echo $this->_vars['form_settings']['rand'];  echo ' = new selectBox({elementsIDs: form_selects_';  echo $this->_vars['form_settings']['rand'];  echo ', selectDropdownClass: \'dropdown\'});
										formCheckboxObject_';  echo $this->_vars['form_settings']['rand'];  echo ' = new checkBox({elementsIDs: form_checkboxes_';  echo $this->_vars['form_settings']['rand'];  echo '});
									}
								});
							});
						});
					'; ?>
</script>
				</div>
				<div class="clearfix">&nbsp;</div>
				
				<?php /* disable the booking move in and out calendar box ***********************
				<div class="search-field periodbox <?php if ($this->_vars['form_settings']['object'] != 'rent' && $this->_vars['form_settings']['object'] != 'lease'): ?>hide<?php endif; ?>" id="booking_date_start">
					<p><?php echo l('field_booking_date_start', 'listings', '', 'text', array()); ?></p>
					<input type="text" name="booking_date_start" value="<?php if ($this->_run_modifier($this->_vars['data']['booking_date_start'], 'strtotime', 'PHP', 1) > 0):  echo $this->_run_modifier($this->_run_modifier($this->_run_modifier($this->_vars['data']['booking_date_start'], 'strtotime', 'PHP', 1), 'date_format', 'plugin', 1, $this->_vars['listings_page_data']['date_format']), 'escape', 'plugin', 1);  endif; ?>" id="date_start<?php echo $this->_vars['form_settings']['rand']; ?>
" class="short">
					<input type="hidden" name="filters[booking_date_start]" value="<?php if ($this->_run_modifier($this->_vars['data']['booking_date_start'], 'strtotime', 'PHP', 1) > 0):  echo $this->_run_modifier($this->_run_modifier($this->_run_modifier($this->_vars['data']['booking_date_start'], 'strtotime', 'PHP', 1), 'date_format', 'plugin', 1, $this->_vars['listings_page_data']['date_format']), 'escape', 'plugin', 1);  endif; ?>" id="alt_date_start<?php echo $this->_vars['form_settings']['rand']; ?>
">
					<script><?php echo '
						$(function(){
							$(\'#date_start';  echo $this->_vars['form_settings']['rand'];  echo '\').datepicker({dateFormat: \'';  echo $this->_vars['listings_page_data']['datepicker_date_format'];  echo '\', altFormat: \'';  echo $this->_vars['listings_page_data']['datepicker_alt_format'];  echo '\', altField: \'#alt_date_start';  echo $this->_vars['form_settings']['rand'];  echo '\', showOn: \'both\'});
						});
					'; ?>
</script>
				</div>
				<div class="search-field periodbox <?php if ($this->_vars['form_settings']['object'] != 'rent' && $this->_vars['form_settings']['object'] != 'lease'): ?>hide<?php endif; ?>" id="booking_date_end">
					<p><?php echo l('field_booking_date_end', 'listings', '', 'text', array()); ?></p>
					<input type="text" name="booking_date_end" value="<?php if ($this->_run_modifier($this->_vars['data']['booking_date_end'], 'strtotime', 'PHP', 1) > 0):  echo $this->_run_modifier($this->_run_modifier($this->_run_modifier($this->_vars['data']['booking_date_end'], 'strtotime', 'PHP', 1), 'date_format', 'plugin', 1, $this->_vars['listings_page_data']['date_format']), 'escape', 'plugin', 1);  endif; ?>" id="date_end<?php echo $this->_vars['form_settings']['rand']; ?>
" class="short">
					<input type="hidden" name="filters[booking_date_end]" value="<?php if ($this->_run_modifier($this->_vars['data']['booking_date_end'], 'strtotime', 'PHP', 1) > 0):  echo $this->_run_modifier($this->_run_modifier($this->_run_modifier($this->_vars['data']['booking_date_end'], 'strtotime', 'PHP', 1), 'date_format', 'plugin', 1, $this->_vars['listings_page_data']['date_format']), 'escape', 'plugin', 1);  endif; ?>" id="alt_date_end<?php echo $this->_vars['form_settings']['rand']; ?>
">
					<script><?php echo '
						$(function(){
							$(\'#date_end';  echo $this->_vars['form_settings']['rand'];  echo '\').datepicker({dateFormat: \'';  echo $this->_vars['listings_page_data']['datepicker_date_format'];  echo '\', altFormat: \'';  echo $this->_vars['listings_page_data']['datepicker_alt_format'];  echo '\', altField: \'#alt_date_end';  echo $this->_vars['form_settings']['rand'];  echo '\', showOn: \'both\'});
						});
					'; ?>
</script>
				</div>	
				*************************************************************************** */?>
				
				<div class="search-field guests-box <?php if ($this->_vars['form_settings']['object'] != 'rent' && $this->_vars['form_settings']['object'] != 'lease'): ?>hide<?php endif; ?>" id="booking_guests">
					<p><?php echo l('field_booking_guests', 'listings', '', 'text', array()); ?></p>
					<?php echo tpl_function_ld(array('i' => 'booking_guests','gid' => 'listings','assign' => 'booking_guests'), $this);?>
					<?php echo tpl_function_selectbox(array('input' => 'filters[booking_guests]','id' => 'booking_guests_select_'.$this->_vars['form_settings']['rand'],'value' => $this->_vars['booking_guests']['option'],'selected' => $this->_vars['data']['booking_guests'],'default' => $this->_vars['booking_guests']['header']), $this);?>
					<script> selects_<?php echo $this->_vars['form_settings']['rand']; ?>
.push('booking_guests_select_<?php echo $this->_vars['form_settings']['rand']; ?>
'); </script>
				</div>

				<?php if ($this->_vars['form_settings']['object'] != 'rent' && $this->_vars['form_settings']['object'] != 'lease'): ?>
					<?php echo $this->_vars['main_search_extend_form']; ?>

				<?php endif; ?>
				
			</div>
			
			<?php /* *********** HIDDEN form start below here ******************* */?>
			<?php /* ************************************************************ */?>
			<div id="full-search-form-<?php echo $this->_vars['form_settings']['rand']; ?>
" <?php if ($this->_vars['listings_page_data']['type'] == 'short'): ?>class="hide"<?php endif; ?> style="display:block;">
				<?php /* The Original Price-Range in put was here... */?>
				<div class="search-field price-range">
					<p><?php echo l('field_price_range', 'listings', '', 'text', array()); ?></p>
					<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'price_min')); tpl_block_capture(array('assign' => 'price_min'), null, $this); ob_start(); ?> <input type="text" placeholder="Harga Min." name="filters[price_min]" class="<?php if ($this->_vars['form_settings']['object'] != 'rent' && $this->_vars['form_settings']['object'] != 'lease'): ?>short<?php else: ?>short<?php endif; ?>" value="<?php echo $this->_run_modifier($this->_vars['data']['price_min'], 'escape', 'plugin', 1); ?>
"> <?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
					<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'price_max')); tpl_block_capture(array('assign' => 'price_max'), null, $this); ob_start(); ?> <input type="text" placeholder="Harga Max." name="filters[price_max]" class="<?php if ($this->_vars['form_settings']['object'] != 'rent' && $this->_vars['form_settings']['object'] != 'lease'): ?>short<?php else: ?>short<?php endif; ?>" value="<?php echo $this->_run_modifier($this->_vars['data']['price_max'], 'escape', 'plugin', 1); ?>
"> <?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
					<?php echo tpl_function_block(array('name' => 'currency_output','module' => 'start','value' => $this->_vars['price_min']), $this);?> &nbsp;<?php echo l('text_to', 'listings', '', 'text', array()); ?>&nbsp;
					<?php echo tpl_function_block(array('name' => 'currency_output','module' => 'start','value' => $this->_vars['price_max']), $this);?>
					<input type="hidden" name="short_name" value="filters[price]">
				</div>	
				
				<?php if ($this->_vars['form_settings']['object'] == 'rent' || $this->_vars['form_settings']['object'] == 'lease'): ?>
					<?php echo $this->_vars['main_search_extend_form']; ?>

				<?php endif; ?>
					
				
				
				
				<?php echo $this->_vars['main_search_full_form']; ?>


				
				<?php if ($this->_vars['listings_page_data']['use_advanced']): ?>
				<?php /* disable for now *****************
				<div class="search-field">
					<p><?php echo l('field_postal_code', 'listings', '', 'text', array()); ?>:</p>
					<input type="text" name="filters[zip]" value="<?php echo $this->_run_modifier($this->_vars['data']['zip'], 'escape', 'plugin', 1); ?>
">
				</div> ******************************* */?>
		
				<?php /* disable for now *****************
					div class="search-field">
					<p><?php echo l('field_radius', 'listings', '', 'text', array()); ?>:</p>
					<?php 
$this->assign('default_radius_select', l('text_radius_select', 'listings', '', 'text', array()));
 ?>
					<?php echo tpl_function_selectbox(array('input' => 'filters[radius]','id' => 'radius_select_'.$this->_vars['form_settings']['rand'],'value' => $this->_vars['radius_data']['option'],'selected' => $this->_vars['data']['radius'],'default' => $this->_vars['default_radius_select']), $this);?>
					<script>selects_<?php echo $this->_vars['form_settings']['rand']; ?>
.push('radius_select_<?php echo $this->_vars['form_settings']['rand']; ?>
');</script>
				</div> ******************************* */?>
				
				<?php /* disable for now *****************
				<div class="search-field">
					<p><?php echo l('field_id', 'listings', '', 'text', array()); ?></p>
					<input type="text" name="filters[id]" value="<?php echo $this->_run_modifier($this->_vars['data']['id'], 'escape', 'plugin', 1); ?>
">
				</div> ******************************* */?>
				
				<?php /* disable for now ********************
				<div class="search-field checkboxes">
					<p>&nbsp;</p>
					<?php if ($this->_vars['form_settings']['object'] != 'buy' && $this->_vars['form_settings']['object'] != 'lease'): ?>
					<?php 
$this->assign('with_photo_value', l('field_with_photo', 'listings', '', 'text', array()));
 ?>
					<?php echo tpl_function_checkbox(array('input' => 'filters[with_photo]','id' => 'with_photo_select_'.$this->_vars['form_settings']['rand'],'value' => $this->_vars['with_photo_value'],'selected' => $this->_vars['data']['with_photo']), $this);?>
					<script>checkboxes_<?php echo $this->_vars['form_settings']['rand']; ?>
.push('with_photo_select_<?php echo $this->_vars['form_settings']['rand']; ?>
');</script>
					<?php endif; ?>
					
					<?php 
$this->assign('open_house_value', l('field_open_house', 'listings', '', 'text', array()));
 ?>
					<?php echo tpl_function_checkbox(array('input' => 'filters[by_open_house]','id' => 'open_house_select_'.$this->_vars['form_settings']['rand'],'value' => $this->_vars['open_house_value'],'selected' => $this->_vars['data']['open_house']), $this);?>
					<script>checkboxes_<?php echo $this->_vars['form_settings']['rand']; ?>
.push('open_house_select_<?php echo $this->_vars['form_settings']['rand']; ?>
');</script>
					
					<?php 
$this->assign('by_private_value', l('field_by_private', 'listings', '', 'text', array()));
 ?>
					<?php echo tpl_function_checkbox(array('input' => 'filters[by_private]','id' => 'by_private_select_'.$this->_vars['form_settings']['rand'],'value' => $this->_vars['by_private_value'],'selected' => $this->_vars['data']['by_private']), $this);?>
					<script> checkboxes_<?php echo $this->_vars['form_settings']['rand']; ?>
.push('by_private_select_<?php echo $this->_vars['form_settings']['rand']; ?>
'); </script>
				</div> *********************************/?>
				
				<?php endif; ?>
			</div>
			<input type="hidden" name="filters[type]" value="<?php echo $this->_run_modifier($this->_vars['form_settings']['object'], 'escape', 'plugin', 1); ?>
">
			<input type="hidden" name="form" value="main_search_form">
			<input type="hidden" name="new" value="1">
		</div>
	<?php endif; ?>
	</div>
</div>
</form>
<script><?php echo '
	$(function(){
		if(selectObject_';  echo $this->_vars['form_settings']['rand'];  echo ') selectObject_';  echo $this->_vars['form_settings']['rand'];  echo '.clear();
		selectObject_';  echo $this->_vars['form_settings']['rand'];  echo ' = new selectBox({elementsIDs: selects_';  echo $this->_vars['form_settings']['rand'];  echo ', selectDropdownClass: \'dropdown\'});
		checkboxObject_';  echo $this->_vars['form_settings']['rand'];  echo ' = new checkBox({elementsIDs: checkboxes_';  echo $this->_vars['form_settings']['rand'];  echo '});
		if(formSelectObject_';  echo $this->_vars['form_settings']['rand'];  echo ') formSelectObject_';  echo $this->_vars['form_settings']['rand'];  echo '.clear();
		formSelectObject_';  echo $this->_vars['form_settings']['rand'];  echo ' = new selectBox({elementsIDs: form_selects_';  echo $this->_vars['form_settings']['rand'];  echo ', selectDropdownClass: \'dropdown\'});
		formCheckboxObject_';  echo $this->_vars['form_settings']['rand'];  echo ' = new checkBox({elementsIDs: form_checkboxes_';  echo $this->_vars['form_settings']['rand'];  echo '});
		
		$(\'#main_search_form_';  echo $this->_vars['form_settings']['rand'];  echo '\').bind(\'submit\', function(){
			var url = \'';  echo $this->_vars['site_url']; ?>
listings/ajax_search<?php echo '\';
			$.post(url, $(this).serialize(), function(data){
				window.location.href = data;
			});
			return false;
		});
	});
'; ?>
</script>
