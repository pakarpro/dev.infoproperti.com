<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-02 15:28:17 KRAT */ ?>

<select name="<?php echo $this->_vars['properties_helper_data']['name']; ?>
" <?php if ($this->_vars['properties_helper_data']['id']): ?>id="<?php echo $this->_vars['properties_helper_data']['id']; ?>
"<?php endif; ?>>

    <?php if (! $this->_vars['properties_helper_data']['id_category']): ?>
	<option value="0"><?php echo l('select_empty_option', 'properties', '', 'text', array()); ?></option>

	<?php if (is_array($this->_vars['properties_helper_data']['data']) and count((array)$this->_vars['properties_helper_data']['data'])): foreach ((array)$this->_vars['properties_helper_data']['data'] as $this->_vars['item']): ?>
	    <?php if ($this->_vars['properties_helper_data']['cat_select_available']): ?>
		<option value="<?php echo $this->_vars['item']['value']; ?>
"<?php if ($this->_vars['item']['value'] == $this->_vars['properties_helper_data']['selected']): ?>selected<?php endif; ?>><?php echo $this->_vars['item']['name']; ?>
</option>
		<?php if (is_array($this->_vars['item']['options']) and count((array)$this->_vars['item']['options'])): foreach ((array)$this->_vars['item']['options'] as $this->_vars['option']): ?><option value="<?php echo $this->_vars['option']['value']; ?>
"<?php if ($this->_vars['option']['value'] == $this->_vars['properties_helper_data']['selected']): ?>selected<?php endif; ?>>&nbsp;&nbsp;--&nbsp;&nbsp;<?php echo $this->_vars['option']['name']; ?>
</option><?php endforeach; endif; ?>
	    <?php else: ?>
		<optgroup label="<?php echo $this->_vars['item']['name']; ?>
">
		<?php if (is_array($this->_vars['item']['options']) and count((array)$this->_vars['item']['options'])): foreach ((array)$this->_vars['item']['options'] as $this->_vars['option']): ?><option value="<?php echo $this->_vars['option']['value']; ?>
"<?php if ($this->_vars['option']['value'] == $this->_vars['properties_helper_data']['selected']): ?>selected<?php endif; ?>><?php echo $this->_vars['option']['name']; ?>
</option><?php endforeach; endif; ?>
		</optgroup>
	    <?php endif; ?>
	<?php endforeach; endif; ?>
    <?php else: ?>
	<option value="0"><?php echo $this->_vars['properties_helper_data']['category_str']; ?>
</option>

	<?php if (is_array($this->_vars['properties_helper_data']['data']) and count((array)$this->_vars['properties_helper_data']['data'])): foreach ((array)$this->_vars['properties_helper_data']['data'] as $this->_vars['item']): ?>
		<?php if (is_array($this->_vars['item']['options']) and count((array)$this->_vars['item']['options'])): foreach ((array)$this->_vars['item']['options'] as $this->_vars['option']): ?><option value="<?php echo $this->_vars['option']['value']; ?>
" <?php if ($this->_vars['option']['value'] == $this->_vars['properties_helper_data']['selected']): ?>selected<?php endif; ?>><?php echo $this->_vars['option']['name']; ?>
</option><?php endforeach; endif; ?>
	<?php endforeach; endif; ?>
    <?php endif; ?>

</select>
