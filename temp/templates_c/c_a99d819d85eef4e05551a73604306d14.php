<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-02 15:55:28 KRAT */ ?>

		</div>
	</div>	
	<?php echo tpl_function_helper(array('func_name' => show_banner_place,'module' => banners,'func_param' => 'bottom-banner'), $this);?>
	<div class="footer">
		<div class="content">
			<?php echo tpl_function_menu(array('gid' => 'user_footer_menu'), $this);?>
			<div class="copyright">&copy;&nbsp;2014&nbsp;Infoproperti.com</div>
		</div>
	</div>

	<?php echo tpl_function_helper(array('func_name' => lang_editor,'module' => languages), $this);?>
	<?php echo tpl_function_helper(array('func_name' => seo_traker,'helper_name' => seo_module,'module' => seo,'func_param' => 'footer'), $this);?>

	
<script><?php echo '
$( "li#social-links" ).click(function() {
  $( "ul#social-contents" ).slideToggle( "fast" );
});

$( "h2.finance-tool-btn" ).click(function() {
  $( "div#fin-tools" ).slideToggle( "fast" );
});

$( "button.close-btn" ).click(function() {
  $( "div#fin-tools" ).slideToggle( "fast" );
});

$(".xcloseBtn").click(function() {
	$("#autogen_error_block").slideToggle("fast");
});

$(document).ready(function () {
   $(\'.feat-comp\').bxSlider({
	   auto: false,
	   autoHover:true,
	   touchEnabled: false,
	   autoControls: false,
	   pager: true,
	   minSlides: 4,
	   maxSlides: 4,
	   slideWidth: 220,
	   startSlide: 0
	});
});

'; ?>
</script>

<script><?php echo '
  (function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,\'script\',\'//www.google-analytics.com/analytics.js\',\'ga\');

  ga(\'create\', \'UA-56118169-1\', \'auto\');
  ga(\'send\', \'pageview\');

'; ?>
</script>


</body>
</html>
