<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2014-06-12 14:06:08 KRAT */ ?>

<div class="row">
	<div class="h"><?php echo l('field_link', 'banners', '', 'text', array()); ?>:&nbsp;* </div>
	<div class="v"><?php if ($this->_vars['data']['user_id'] == 0): ?><input type="text" value="<?php echo $this->_vars['data']['link']; ?>
" name="link" class="long"><?php else: ?><input type="hidden" name='link' value="<?php echo $this->_vars['data']['link']; ?>
"><?php echo $this->_vars['data']['link'];  endif; ?></div>
</div>
<div class="row">
	<div class="h"><?php echo l('field_alt_text', 'banners', '', 'text', array()); ?>:&nbsp;* </div>
	<div class="v"><?php if ($this->_vars['data']['user_id'] == 0): ?><input type="text" value="<?php echo $this->_vars['data']['alt_text']; ?>
" name="alt_text" class="long"><?php else: ?><input type="hidden" name='alt_text' value="<?php echo $this->_vars['data']['alt_text']; ?>
"><?php echo $this->_vars['data']['alt_text'];  endif; ?></div>
</div>
<?php if ($this->_vars['data']['user_id'] == 0): ?>
<div class="row">
	<div class="h"><?php echo l('field_number_of_clicks', 'banners', '', 'text', array()); ?>: </div>
	<div class="v"><input type="text" value="<?php echo $this->_vars['data']['number_of_clicks']; ?>
" name="number_of_clicks" class="short"></div>
</div>
<div class="row">
	<div class="h"><?php echo l('field_number_of_views', 'banners', '', 'text', array()); ?>: </div>
	<div class="v"><input type="text" value="<?php echo $this->_vars['data']['number_of_views']; ?>
" name="number_of_views" class="short"></div>
</div>
<div class="row">
	<div class="h"><?php echo l('field_expiration_date', 'banners', '', 'text', array()); ?>: </div>
	<div class="v">
		<input type="checkbox" name="expiration_date_on" value="1" <?php if ($this->_vars['data']['expiration_date_on']): ?>checked<?php endif; ?>>
		<input type="text" value="<?php echo $this->_vars['data']['expiration_date']; ?>
" name="expiration_date_visible" class="datepicker" id="expiration_date">
		<input type="hidden" value="<?php echo $this->_vars['data']['expiration_date']; ?>
" name="expiration_date" id="expiration_date_hide">
	</div>
</div>
<div class="row">
	<div class="h"><?php echo l('field_new_window', 'banners', '', 'text', array()); ?>: </div>
	<div class="v"><input type="checkbox" value="1" name="new_window"<?php if ($this->_vars['data']['new_window'] == 1): ?>checked<?php endif; ?>></div>
</div>
<?php endif; ?>
<div class="row">
	<div class="h"><?php echo l('field_image', 'banners', '', 'text', array()); ?>:&nbsp;* </div>
	<div class="v">
			<?php if ($this->_vars['data']['user_id'] == 0): ?>
				<input type="file" value="" name="banner_image_file">
				<?php if ($this->_vars['data']['banner_image']): ?>
				<br>
				<a href="<?php echo $this->_vars['data']['media']['banner_image']['file_url']; ?>
" id="view_banner" target="blank"><?php echo l('btn_preview', 'start', '', 'text', array()); ?></a>&nbsp;&nbsp;
				<input type="checkbox" name="banner_image_delete" value="1" id="banner_image_delete"> <label for="banner_image_delete"><?php echo l('field_image_delete', 'banners', '', 'text', array()); ?></label>
				<?php endif; ?>
			<?php else: ?>
				<a href="<?php echo $this->_vars['data']['media']['banner_image']['file_url']; ?>
" id="view_banner" target="blank"><?php echo l('btn_preview', 'start', '', 'text', array()); ?></a>
			<?php endif; ?>
	</div>
</div>
<script type="text/javascript"><?php echo '
	$(function(){
		admin_banners.initImageForm();
	});
'; ?>
</script>
