<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-02 15:55:12 KRAT */ ?>

<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'footer_menu')); tpl_block_capture(array('assign' => 'footer_menu'), null, $this); ob_start(); ?>
	<?php if (is_array($this->_vars['menu']) and count((array)$this->_vars['menu'])): foreach ((array)$this->_vars['menu'] as $this->_vars['key'] => $this->_vars['item']): ?>
	<li <?php if ($this->_vars['item']['active']): ?>class="active"<?php endif; ?>>
		<?php if ($this->_vars['item']['link']): ?>
		<a href="<?php echo $this->_vars['item']['link']; ?>
"><?php echo $this->_vars['item']['value']; ?>
</a>
		<?php else: ?>
		<?php echo $this->_vars['item']['value']; ?>

		<?php endif; ?>
		<?php if ($this->_vars['item']['sub']): ?>
		<?php $this->assign('subitems', '1'); ?>
		<ul>
			<?php if (is_array($this->_vars['item']['sub']) and count((array)$this->_vars['item']['sub'])): foreach ((array)$this->_vars['item']['sub'] as $this->_vars['subitem']): ?><li><a href="<?php echo $this->_vars['subitem']['link']; ?>
"><?php echo $this->_vars['subitem']['value']; ?>
</a></li><?php endforeach; endif; ?>	
		</ul>
		<?php endif; ?>
	</li>
	<?php endforeach; endif;  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>

<ul <?php if ($this->_vars['subitems']): ?>class="with-groups"<?php endif; ?>>
	<?php echo $this->_vars['footer_menu']; ?>

</ul>
