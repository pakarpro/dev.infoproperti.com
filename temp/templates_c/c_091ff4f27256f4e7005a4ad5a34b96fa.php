<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.helper.php'); $this->register_function("helper", "tpl_function_helper");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2014-12-12 14:25:55 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
  echo tpl_function_helper(array('func_name' => get_admin_level1_menu,'helper_name' => menu,'func_param' => 'admin_seo_menu'), $this);?>
<div class="actions">&nbsp;</div>
<form method="post" action="<?php echo $this->_vars['data']['action']; ?>
" name="save_form">
	<div class="edit-form n150">
		<div class="row header"><?php echo l('admin_header_seo_tracker_editing', 'seo', '', 'text', array()); ?></div>
		<div class="row">
			<div class="h"><?php echo l('field_use_ga_tracker', 'seo', '', 'text', array()); ?>: </div>
			<div class="v">
				<input type="radio" value="1" <?php if ($this->_vars['data']['seo_ga_default_activate']): ?>checked<?php endif; ?> name="seo_ga_default_activate" id="ga_active_yes" onclick="javascript: check_tag('ga_active', this);"> 
				<label for="ga_active_yes"><?php echo l('use_ga_tracker_yes', 'seo', '', 'text', array()); ?></label>&nbsp;
				<input type="radio" value="0" <?php if (! $this->_vars['data']['seo_ga_default_activate']): ?>checked<?php endif; ?> name="seo_ga_default_activate" id="ga_active_no" onclick="javascript: check_tag('ga_active', this);"> 
				<label for="ga_active_no"><?php echo l('use_ga_tracker_no', 'seo', '', 'text', array()); ?></label>
			</div>
		</div>

		<div class="row zebra">
			<div class="h"><?php echo l('field_ga_account_id', 'seo', '', 'text', array()); ?>: </div>
			<div class="v"><input type="text" value="<?php echo $this->_vars['data']['seo_ga_default_account_id']; ?>
" name="seo_ga_default_account_id" id="input_ga_active" <?php if (! $this->_vars['data']['seo_ga_default_activate']): ?>disabled<?php endif; ?>></div>
		</div>

		<div class="row">
			<div class="h"><?php echo l('field_code_placement', 'seo', '', 'text', array()); ?>: </div>
			<div class="v">
				<input type="radio" value="top" <?php if ($this->_vars['data']['seo_ga_default_placement'] == 'top'): ?>checked<?php endif; ?> name="seo_ga_default_placement" id="ga_top"> 
				<label for="ga_top"><?php echo l('tracker_top', 'seo', '', 'text', array()); ?></label>&nbsp;
				<input type="radio" value="footer" <?php if ($this->_vars['data']['seo_ga_default_placement'] == 'footer'): ?>checked<?php endif; ?> name="seo_ga_default_placement" id="ga_footer"> 
				<label for="ga_footer"><?php echo l('tracker_footer', 'seo', '', 'text', array()); ?></label>
			</div>
		</div>
		<div class="row zebra">
			<div class="h">&nbsp;</div>
			<div class="v"><?php echo l('ga_tracker_hint', 'seo', '', 'text', array()); ?></div>
		</div>
		<!-- other trackers -->

		<div class="row">
			<div class="h"><?php echo l('field_use_other_tracker', 'seo', '', 'text', array()); ?>: </div>
			<div class="v">
				<input type="radio" value="1" <?php if ($this->_vars['data']['seo_ga_manual_activate']): ?>checked<?php endif; ?> name="seo_ga_manual_activate" id="other_active_yes" onclick="javascript: check_tag('other_active', this);"> 
				<label for="other_active_yes"><?php echo l('use_ga_tracker_yes', 'seo', '', 'text', array()); ?></label>&nbsp;
				<input type="radio" value="0" <?php if (! $this->_vars['data']['seo_ga_manual_activate']): ?>checked<?php endif; ?> name="seo_ga_manual_activate" id="other_active_no" onclick="javascript: check_tag('other_active', this);"> 
				<label for="other_active_no"><?php echo l('use_ga_tracker_no', 'seo', '', 'text', array()); ?></label>
			</div>
		</div>
		<div class="row zebra">
			<div class="h"><?php echo l('field_code_placement', 'seo', '', 'text', array()); ?>: </div>
			<div class="v">
				<input type="radio" value="top" <?php if ($this->_vars['data']['seo_ga_manual_placement'] == 'top'): ?>checked<?php endif; ?> name="seo_ga_manual_placement" id="other_top"> 
				<label for="other_top"><?php echo l('tracker_top', 'seo', '', 'text', array()); ?></label>&nbsp;
				<input type="radio" value="footer" <?php if ($this->_vars['data']['seo_ga_manual_placement'] == 'footer'): ?>checked<?php endif; ?> name="seo_ga_manual_placement" id="other_footer"> 
				<label for="other_footer"><?php echo l('tracker_footer', 'seo', '', 'text', array()); ?></label>
			</div>
		</div>
		<div class="row">
			<div class="h"><?php echo l('field_other_code', 'seo', '', 'text', array()); ?>: </div>
			<div class="v"><textarea name="seo_ga_manual_tracker_code" id="input_other_active" style="height: 170px" <?php if (! $this->_vars['data']['seo_ga_manual_activate']): ?>disabled<?php endif; ?>><?php echo $this->_vars['data']['seo_ga_manual_tracker_code']; ?>
</textarea></div>
		</div>


		<div class="row zebra">
			<div class="h">&nbsp;</div>
			<div class="v"><?php echo l('other_tracker_hint', 'seo', '', 'text', array()); ?></div>
		</div>
	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div></div>
	<a class="cancel" href="<?php echo $this->_vars['site_url']; ?>
admin/seo/tracker"><?php echo l('btn_cancel', 'start', '', 'text', array()); ?></a>
</form>
<div class="clr"></div>
<script>
<?php echo '
	function check_tag(gid, object){
		if(object.checked){
			$(\'#input_\'+gid).removeAttr("disabled");
		}else{
			$(\'#input_\'+gid).attr(\'disabled\', \'disabled\');
		}
	}
'; ?>

</script>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>