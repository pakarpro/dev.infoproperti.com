<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2014-08-07 17:42:45 KRAT */ ?>

<?php if ($this->_vars['listings']):  echo tpl_function_js(array('file' => 'dualslider/jquery.dualSlider.0.3.min.js'), $this); echo tpl_function_js(array('file' => 'dualslider/jquery.timers-1.2.js'), $this);?>
<div class="slider_<?php echo $this->_run_modifier($this->_vars['view'], 'escape', 'plugin', 1); ?>
">
	<div class="slider_wrapper">
		<div class="slider_wrapper2">
			<div class="slider">
		
				<div class="carousel">		
					<div class="backgrounds_wrapper">
						<div class="backgrounds_wrapper2">
							<div class="backgrounds_wrapper3">
								<div class="backgrounds">						
									<?php if (is_array($this->_vars['listings']) and count((array)$this->_vars['listings'])): foreach ((array)$this->_vars['listings'] as $this->_vars['key'] => $this->_vars['item']): ?>
									<div class="item item_<?php echo $this->_run_modifier($this->_vars['key'], 'escape', 'plugin', 1); ?>
" style="background: url('<?php echo $this->_vars['item']['media']['slider']['thumbs'][$this->_vars['view']]; ?>
') no-repeat;"></div>							
									<?php endforeach; endif; ?>
								</div>
							</div>	
						</div>
					</div>
						
					<?php if ($this->_vars['listings_page_data']['count'] > 1): ?>
					<div class="paging_wrapper">
						<div class="paging_wrapper2">
							<div class="paging">
								<a id="previous_item" class="previous" alt="<?php echo l('nav_prev', 'start', '', 'button', array()); ?>" title="<?php echo l('nav_prev', 'start', '', 'button', array()); ?>"><?php echo l('nav_prev', 'start', '', 'text', array()); ?></a>
								<a id="next_item" class="next" alt="<?php echo l('nav_next', 'start', '', 'button', array()); ?>" title="<?php echo l('nav_next', 'start', '', 'button', array()); ?>"><?php echo l('nav_next', 'start', '', 'text', array()); ?></a>
							</div>
						</div>
					</div>	
					<?php endif; ?>
					
					<div class="panel">
						<div class="details_wrapper">
							<div class="details">
								<?php if (is_array($this->_vars['listings']) and count((array)$this->_vars['listings'])): foreach ((array)$this->_vars['listings'] as $this->_vars['key'] => $this->_vars['item']): ?>
								<div class="listing detail">
									<a href="<?php echo tpl_function_seolink(array('module' => 'users','method' => 'view','data' => $this->_vars['item']['user']), $this);?>"><img src="<?php echo $this->_vars['item']['user']['media']['user_logo']['thumbs']['small']; ?>
" alt="<?php echo $this->_run_modifier($this->_vars['item']['user']['output_name'], 'escape', 'plugin', 1); ?>
"></a>
									<a href="<?php echo tpl_function_seolink(array('module' => 'users','method' => 'view','data' => $this->_vars['item']['user']), $this);?>"><?php echo $this->_run_modifier($this->_vars['item']['user']['output_name'], 'truncate', 'plugin', 1, 50); ?>
</a>							
									<a href="<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'view','data' => $this->_vars['item']), $this);?>"><?php if ($this->_vars['view'] == '654_395'):  echo $this->_run_modifier($this->_vars['item']['output_name'], 'truncate', 'plugin', 1, 50);  else:  echo $this->_run_modifier($this->_vars['item']['output_name'], 'truncate', 'plugin', 1, 100);  endif; ?></a>
									<span><?php echo $this->_vars['item']['property_type_str']; ?>
 <?php echo $this->_vars['item']['operation_type_str']; ?>
</span>
									<span><?php echo tpl_function_block(array('name' => listing_price_block,'module' => 'listings','data' => $this->_vars['item'],'template' => 'small'), $this);?></span>
								</div>
								<?php endforeach; endif; ?>
							
							</div>
							<div class="details_background"></div>
						</div>
					</div>
				</div>				
			</div>
		</div>
	</div>
</div>
<script><?php echo '
	$(function(){
		$(".carousel").dualSlider({
			auto: ';  if ($this->_vars['listings_page_data']['slider_auto']): ?>true<?php else: ?>false<?php endif;  echo ',
			autoDelay: ';  if ($this->_vars['listings_page_data']['slider_auto']):  echo $this->_vars['listings_page_data']['rotation'];  else: ?>false<?php endif;  echo ',
			easingCarousel: "swing",
			easingDetails: "swing",
			durationCarousel: 700,
			durationDetails: 300,
			widthsliderimage: $(".carousel .backgrounds .item").width(),
			';  if ($this->_vars['_LANG']['rtl'] == 'rtl'): ?>rtl: true,<?php endif;  echo '
		});
		$(\'.slider .backgrounds .item\').bind(\'click\', function(){
			var next = $(\'#next_item\');
			if(next.css(\'display\') != \'none\') next.trigger(\'click\');
		});
	});
'; ?>
</script>
<?php endif; ?>

