<?php require_once('/opt/ip/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-05-28 10:46:16 KRAT */ ?>

<div class="actions" >
	<a href="#" onclick="javascript: mb.edit_folder(0); return false;" class="btn-link fright"><ins class="with-icon i-expand"></ins><span><?php echo l('add_new_folder', 'mailbox', '', 'text', array()); ?></span></a>
	<a href="<?php echo $this->_vars['site_url']; ?>
mailbox/index" class="btn-link fleft"><ins class="with-icon i-larr"></ins><?php echo l('btn_back', 'start', '', 'text', array()); ?></a>
</div>
<div class="clr"></div>
<div>	
		<table class="list">
		<tr id="sorter_block">
			<th><?php echo l('field_folder', 'mailbox', '', 'text', array()); ?></th>		
			<th class="w70"><?php echo l('field_chats', 'mailbox', '', 'text', array()); ?></th>		
			<th class="w100"><?php echo l('actions', 'mailbox', '', 'text', array()); ?></th>	
		</tr>
		<?php if (is_array($this->_vars['folders']) and count((array)$this->_vars['folders'])): foreach ((array)$this->_vars['folders'] as $this->_vars['item']): ?>
		<tr>
			<td><?php echo $this->_vars['item']['name']; ?>
</td>
			<td><?php echo $this->_vars['item']['chats']; ?>
</td>
			<td class="center">
			<?php if ($this->_vars['item']['id_user']): ?>
				<a href="#" onclick="javascript: if(confirm('<?php echo l('note_delete_folder', 'mailbox', '', 'js', array()); ?>')) <?php echo '{'; ?>
 mb.delete_folder(<?php echo $this->_vars['item']['id']; ?>
); <?php echo '}'; ?>
 return false;" class="btn-link fright"><ins class="with-icon i-delete"></ins></a>
				<a href="#" onclick="javascript: mb.edit_folder(<?php echo $this->_vars['item']['id']; ?>
); return false;" class="btn-link fright"><ins class="with-icon i-edit"></ins></a>
			<?php else: ?>
			<i><?php echo l('text_default_folder', 'mailbox', '', 'text', array()); ?></i>
			<?php endif; ?>
			</td>
		</tr>
		<?php endforeach; else: ?>
			<tr>
				<td colspan="5" class="empty"><?php echo l('no_folders', 'mailbox', '', 'text', array()); ?></td>
			</tr>
		<?php endif; ?>
		</table>	

</div>
<div id="pages_block_2"><?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "pagination.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?></div>
