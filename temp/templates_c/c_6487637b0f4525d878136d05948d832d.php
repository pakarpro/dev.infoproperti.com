<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-02-06 14:48:48 KRAT */ ?>

<p class="header-comment">
<?php echo l("text_register_as", 'users', '', 'text', array()); ?>: <b><?php echo $this->_vars['data']['user_type_str']; ?>
</b><br>
<?php echo l("field_date_created", 'users', '', 'text', array()); ?>: <b><?php echo $this->_run_modifier($this->_vars['data']['date_created'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']); ?>
</b>
</p>

<h2 class="line top bottom linked">
	<?php echo l('table_header_personal', 'users', '', 'text', array()); ?>
	<a class="btn-link fright" href="<?php echo $this->_vars['site_url']; ?>
users/profile/personal/"><ins class="with-icon i-edit"></ins></a>
</h2>
<div class="view-section">
	<div class="r">
		<div class="f"><?php echo l('field_fname', 'users', '', 'text', array()); ?>:</div>
		<div class="v"><?php echo $this->_vars['data']['fname']; ?>
</div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_sname', 'users', '', 'text', array()); ?>:</div>
		<div class="v"><?php echo $this->_vars['data']['sname']; ?>
</div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_phone', 'users', '', 'text', array()); ?>:</div>
		<div class="v"><?php echo $this->_vars['data']['phone']; ?>
</div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_company', 'users', '', 'text', array()); ?>:</div>
		<div class="v"><?php echo $this->_vars['data']['company_name']; ?>
</div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_icon', 'users', '', 'text', array()); ?>:</div>
		<div class="v">
		<?php if ($this->_vars['data']['user_logo_moderation']): ?>
		<img src="<?php echo $this->_vars['data']['media']['user_logo_moderation']['thumbs']['middle']; ?>
" alt="<?php echo $this->_run_modifier($this->_run_modifier($this->_vars['data']['output_name'], 'truncate', 'plugin', 1, 30), 'escape', 'plugin', 1); ?>
" /><?php else: ?>
		<img src="<?php echo $this->_vars['data']['media']['user_logo']['thumbs']['middle']; ?>
" alt="<?php echo $this->_run_modifier($this->_vars['data']['output_name'], 'escape', 'plugin', 1); ?>
" />
		<?php endif; ?>
		</div>
	</div>
</div>


<h2 class="line top bottom linked">
	<?php echo l('table_header_contact', 'users', '', 'text', array()); ?>
	<a class="btn-link fright" href="<?php echo $this->_vars['site_url']; ?>
users/profile/contact/"><ins class="with-icon i-edit"></ins></a>
</h2>
<div class="view-section">
	<div class="r">
		<div class="f"><?php echo l('field_contact_email', 'users', '', 'text', array()); ?>:</div>
		<div class="v"><?php if ($this->_vars['data']['contact_email']):  echo $this->_vars['data']['contact_email'];  else:  echo l('no_information', 'users', '', 'text', array());  endif; ?></div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_contact_phone', 'users', '', 'text', array()); ?>:</div>
		<div class="v"><?php if ($this->_vars['data']['contact_phone']):  echo $this->_vars['data']['contact_phone'];  else:  echo l('no_information', 'users', '', 'text', array());  endif; ?></div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_contact_info', 'users', '', 'text', array()); ?>:</div>
		<div class="v"><?php if ($this->_vars['data']['contact_info']):  echo $this->_run_modifier($this->_vars['data']['contact_info'], 'nl2br', 'PHP', 1);  else:  echo l('no_information', 'users', '', 'text', array());  endif; ?></div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_web_url', 'users', '', 'text', array()); ?>:</div>
		<div class="v"><?php if ($this->_vars['data']['web_url']):  echo $this->_vars['data']['web_url'];  else:  echo l('no_information', 'users', '', 'text', array());  endif; ?></div>
	</div>
	
	<div class="r">
		<div class="f"><?php echo l('field_facebook', 'users', '', 'text', array()); ?>:</div>
		<div class="v"><?php if ($this->_vars['data']['facebook']):  echo $this->_vars['data']['facebook'];  else:  echo l('no_information', 'users', '', 'text', array());  endif; ?></div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_twitter', 'users', '', 'text', array()); ?>:</div>
		<div class="v"><?php if ($this->_vars['data']['twitter']):  echo $this->_vars['data']['twitter'];  else:  echo l('no_information', 'users', '', 'text', array());  endif; ?></div>
	</div>
	<?php /* disable for now
	<div class="r">
		<div class="f"><?php echo l('field_vkontakte', 'users', '', 'text', array()); ?>:</div>
		<div class="v"><?php if ($this->_vars['data']['vkontakte']):  echo $this->_vars['data']['vkontakte'];  else:  echo l('no_information', 'users', '', 'text', array());  endif; ?></div>
	</div>
	*/ ?>
	<div class="r">
		<div class="f"><?php echo l('field_working_days', 'users', '', 'text', array()); ?>:</div>
		<div class="v"><?php if ($this->_vars['data']['working_days_str']):  echo $this->_vars['data']['working_days_str'];  else:  echo l('no_information', 'users', '', 'text', array());  endif; ?>
		</div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_working_hours', 'users', '', 'text', array()); ?>:</div>
		<div class="v"><?php if ($this->_vars['data']['working_hours_str']):  echo $this->_vars['data']['working_hours_str'];  else:  echo l('no_information', 'users', '', 'text', array());  endif; ?>
		</div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_lunch_time', 'users', '', 'text', array()); ?>:</div>
		<div class="v">
			<?php if ($this->_vars['data']['lunch_time_str']):  echo $this->_vars['data']['lunch_time_str'];  else:  echo l('no_information', 'users', '', 'text', array());  endif; ?></div>
	</div>
	
</div>
<h2 class="line top bottom linked">
	<?php echo l('table_header_location', 'users', '', 'text', array()); ?>
	<a class="btn-link fright" href="<?php echo $this->_vars['site_url']; ?>
users/profile/location/"><ins class="with-icon i-edit"></ins></a>
</h2>
<div class="view-section">
	<div class="r">
		<div class="f"><?php echo l('field_location', 'users', '', 'text', array()); ?>:</div>
		<div class="v"><?php if ($this->_vars['data']['location']):  echo $this->_vars['data']['location'];  else:  echo l('no_information', 'users', '', 'text', array());  endif; ?></div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_postal_code', 'users', '', 'text', array()); ?>:</div>
		<div class="v"><?php if ($this->_vars['data']['postal_code']):  echo $this->_vars['data']['postal_code'];  else:  echo l('no_information', 'users', '', 'text', array());  endif; ?></div>
	</div>
	<?php if ($this->_vars['data']['id_country'] && $this->_vars['data']['id_region'] && $this->_vars['data']['id_city']): ?>
	<div class="r">
		<div class="f"><?php echo l('field_contact_map', 'users', '', 'text', array()); ?>:</div>
		<div class="v"><?php echo tpl_function_block(array('name' => show_default_map,'module' => geomap,'object_id' => $this->_vars['data']['id'],'gid' => 'user_profile','markers' => $this->_vars['markers'],'settings' => $this->_vars['map_settings'],'width' => '559','height' => '400'), $this);?></div>
	</div>
	<?php endif; ?>	
</div>
<?php echo tpl_function_helper(array('func_name' => get_user_subscriptions_list,'module' => subscriptions), $this);?>
