<?php require_once('/opt/ip/system/libraries/template_lite/plugins/function.breadcrumbs.php'); $this->register_function("breadcrumbs", "tpl_function_breadcrumbs");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.start_search_form.php'); $this->register_function("start_search_form", "tpl_function_start_search_form");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.menu.php'); $this->register_function("menu", "tpl_function_menu");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-02 15:54:36 KRAT */ ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html DIR="<?php echo $this->_vars['_LANG']['rtl']; ?>
">
<head>
	<meta name="alexaVerifyID" content="CbAsMUFljkRZ0p5Dw3R7wCKjB9U"/>
	<meta name="msvalidate.01" content="DB03CDF777924BCFEEA97FE8A36E329A" />
	<meta name="google-site-verification" content="bpvQb7SNgF0hvwHRwIWurxJKSvGRnnWi32KVCBOqtSE" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="expires" content="0">
	<meta http-equiv="pragma" content="no-cache">
	<meta name="revisit-after" content="3 days">
	<meta name="robot" content="All">
	<?php echo tpl_function_seotag(array('tag' => 'title|description|keyword'), $this);?>
	<?php echo tpl_function_helper(array('func_name' => css,'helper_name' => theme,'func_param' => $this->_vars['load_type']), $this);?>
	<?php /* <link type="text/css" href="<?php echo $this->_vars['site_url']; ?>
application/views/default/sets/ipblue/css/general-ltr.css">
	<link type="text/css" href="<?php echo $this->_vars['site_url']; ?>
application/views/default/sets/ipblue/css/style-ltr.css">
	<link type="text/css" href="<?php echo $this->_vars['site_url']; ?>
application/views/default/sets/ipblue/css/print-ltr.css"> */ ?>
   
	<script type="text/javascript">
		var site_url = '<?php echo $this->_vars['site_url']; ?>
';
		var site_rtl_settings = '<?php echo $this->_vars['_LANG']['rtl']; ?>
';
		var site_error_position = 'center';
	</script>
	
	<link rel="shortcut icon" href="<?php echo $this->_vars['site_root']; ?>
favicon.ico">
	<?php echo tpl_function_helper(array('func_name' => js,'helper_name' => theme,'func_param' => $this->_vars['load_type']), $this);?>

	<?php echo tpl_function_helper(array('func_name' => banner_initialize,'module' => banners), $this);?>
	<?php echo tpl_function_helper(array('func_name' => show_social_networks_head,'module' => social_networking), $this);?>
	
	<?php if ($this->_vars['_LANG']['rtl'] == 'rtl'): ?><!--[if IE]><link rel="stylesheet" type="text/css" href="<?php echo $this->_vars['site_root']; ?>
application/views/default/ie-rtl-fix.css" /><![endif]--><?php endif; ?>
	<link rel='stylesheet' type='text/css' media='screen, projection' href='<?php echo $this->_vars['site_root']; ?>
application/views/default/css/jquery.bxslider.css' />
	<script type='text/javascript' src='<?php echo $this->_vars['site_root']; ?>
application/js/jquery.bxslider.js'></script>
	
</head>
<body>

<?php echo tpl_function_helper(array('func_name' => seo_traker,'helper_name' => seo_module,'module' => seo,'func_param' => 'top'), $this);?>

<?php echo tpl_function_helper(array('func_name' => demo_panel,'helper_name' => start,'func_param' => 'user'), $this);?>
<?php if ($this->_vars['display_brouser_error']): ?>
	<?php echo tpl_function_helper(array('func_name' => available_brousers,'helper_name' => start), $this);?>
<?php endif; ?>

	<div id="error_block"><?php if (is_array($this->_vars['_PREDEFINED']['error']) and count((array)$this->_vars['_PREDEFINED']['error'])): foreach ((array)$this->_vars['_PREDEFINED']['error'] as $this->_vars['item']):  echo $this->_vars['item']; ?>
<br><?php endforeach; endif; ?></div>
	<div id="info_block"><?php if (is_array($this->_vars['_PREDEFINED']['info']) and count((array)$this->_vars['_PREDEFINED']['info'])): foreach ((array)$this->_vars['_PREDEFINED']['info'] as $this->_vars['item']):  echo $this->_vars['item']; ?>
<br><?php endforeach; endif; ?></div>
	<div id="success_block"><?php if (is_array($this->_vars['_PREDEFINED']['success']) and count((array)$this->_vars['_PREDEFINED']['success'])): foreach ((array)$this->_vars['_PREDEFINED']['success'] as $this->_vars['item']):  echo $this->_vars['item']; ?>
<br><?php endforeach; endif; ?></div>
	
	<div class="header clearfix">
		<div class="content-head clearfix">
			<script><?php echo '
				$(function(){
					$(\'.top_menu ul>li\').each(function(i, item){
						var element = $(item);
						var submenu = element.find(\'.sub_menu_block\');
						if(submenu.length == 0) return;
						element.children(\'a\').bind(\'touchstart\', function(){
							if(element.hasClass(\'hover\')){
								element.removeClass(\'hover\');
							}else{
								$(\'.top_menu ul>li\').removeClass(\'hover\');
								element.addClass(\'hover\');
							}
							return false;
						});
						element.children(\'a\').bind(\'touchenter\', function(){
							element.removeClass(\'hover\');
							element.trigger(\'hover\');
							return false;
						}).bind(\'touchleave\', function(){
							element.removeClass(\'hover\').trigger(\'blur\');
							return false;
						});
					});
				});
			'; ?>
</script>
			<div class="logo">
				<?php /* DISABLE TEMPORARILY AND REPLACE WITH NEW LOGO AND CODE
				<a href="<?php echo $this->_vars['site_url']; ?>
"><img src="<?php echo $this->_vars['base_url'];  echo $this->_vars['logo_settings']['path']; ?>
" border="0" alt="Welcome to Infoproperti.com" width="<?php echo $this->_vars['logo_settings']['width']; ?>
" height="<?php echo $this->_vars['logo_settings']['height']; ?>
"></a>
				*/?>
				<a href="<?php echo $this->_vars['site_url']; ?>
"><img src="<?php echo $this->_vars['base_url'];  echo $this->_vars['logo_settings']['path']; ?>
" border="0" alt="Welcome to Infoproperti.com" width="" height=""></a>
			</div>
	
			<div class="top_menu">
				<div class="container980">
					<?php if ($this->_vars['auth_type'] == 'user'): ?>
					<?php echo tpl_function_menu(array('gid' => $this->_vars['user_session_data']['user_type'].'_main_menu','template' => 'user_main_menu'), $this);?>
					<?php else: ?>
					<?php echo tpl_function_menu(array('gid' => 'guest_main_menu','template' => 'user_main_menu'), $this);?>
					<?php endif; ?>
				</div>
			</div>
			
			<?php /* disabled and move the content to modules/menu/view/default/user_main_menu.tpl 
			<div id="social-media">
				<ul class="clearfix">
					<li><a id="fb" class="socmed-icons" href="https://www.facebook.com/inproperti" target="blank">Join Infoproperti on Facebook</a></li>
					<li><a id="twitter" class="socmed-icons" href="https://twitter.com/inproperti" target="blank">Join Infoproperti on Twitter</a></li>
					<li><a id="gplus" class="socmed-icons" href="https://plus.google.com/u/0/111307853610134749510/about" target="blank">Join Infoproperti on Google Plus</a></li>
					<li><a id="youtube" class="socmed-icons" href="http://www.youtube.com/channel/UCpY67JNdkJ8xxxeacwXJT4Q" target="blank">Join Infoproperti on Youtube</a></li>
					<!-- <li><a id="pinterest" class="socmed-icons" href="#" target="blank">Join Infoproperti on Pinterest</a></li>-->
					<li><a id="linkedin" class="socmed-icons" href="https://id.linkedin.com/in/infoproperti" target="blank">Join Infoproperti on linkedIn</a></li>
					<li><a id="instagram" class="socmed-icons" href="#">Join Infoproperti on Instagram</a></li>
				</ul>
			</div>
			*/ ?>
			
			<div id="top-login">
				<div id="login-register" class="clearfix">
					<h3 class="dark-blue">Log in / Sign up</h3>
					<form id="login-form" action="<?php echo $this->_vars['site_url']; ?>
users/login" method="post" >
						<!--<div class="f"><?php echo l('field_email', 'users', '', 'text', array()); ?>:&nbsp;*</div>-->
						<div class="input-entry">
							<span>Email:*</span>
							<input class="top-login-form-input" type="text" name="email" <?php if ($this->_vars['DEMO_MODE']): ?>value="<?php echo $this->_run_modifier($this->_vars['demo_user_type_login_settings']['login'], 'escape', 'plugin', 1); ?>
"<?php endif; ?>>
						</div>
						<!--<div class="f"><?php echo l('field_password', 'users', '', 'text', array()); ?>:&nbsp;*</div>-->
						<div class="input-entry">
							<span>Password:*</span>
							<input class="top-login-form-input" type="password" name="password" <?php if ($this->_vars['DEMO_MODE']): ?>value="<?php echo $this->_run_modifier($this->_vars['demo_user_type_login_settings']['password'], 'escape', 'plugin', 1); ?>
"<?php endif; ?>>
							<span class="v-link"><a href="<?php echo $this->_vars['site_url']; ?>
users/restore"><?php echo l('link_restore', 'users', '', 'text', array()); ?></a></span>
						</div>
						
						
						
						
						<input class="signin-btn" type="submit" value="<?php echo l('btn_login', 'start', '', 'button', array()); ?>" name="logbtn">
					</form>
					
					<p>
					Pendatang baru di situs ini? Situs kami akan membantu bisnis anda! Buat akun anda untuk mengunakan fitur penuh situs ini.<br /><br />
					Register sebagai:
					<ul id="reg-type">
						<li><a href="<?php echo $this->_vars['site_url']; ?>
registration-as-private-person">Perorangan/Pribadi</a></li>
						<li><a href="<?php echo $this->_vars['site_url']; ?>
registration-as-company">Perusahaan</a></li>
						<li class="no-margin"><a href="<?php echo $this->_vars['site_url']; ?>
registration-as-agent">Agen</a></li>
					</ul>	
					</p>
				</div>
			</div>
			
			<div class="header-menu">
				<ul>
					<?php echo tpl_function_block(array('name' => users_lang_select,'module' => users), $this);?>
					<?php echo tpl_function_block(array('name' => site_currency_select,'module' => users), $this);?>
					<?php echo tpl_function_block(array('name' => auth_links,'module' => users), $this);?>
					<?php echo tpl_function_block(array('name' => post_listing_button,'module' => listings), $this);?>
				</ul>
			</div>
			<script><?php echo '
				$( "a#ajax_login_link" ).click(function() {
				  $( "div#login-register" ).slideToggle( "fast" );
				});  
			'; ?>
</script>
			
		</div>
	</div>
	<div class="clr"></div>
	
	<div class="main">
			
		<div <?php if ($this->_vars['header_type'] == 'index'): ?>id="index-pg"<?php endif; ?> <?php if ($this->_vars['header_type'] != 'index'): ?>id="general-pg"<?php endif; ?> class="content clearfix">
			<?php if ($this->_vars['header_type'] != 'index'): ?>
			<?php echo tpl_function_start_search_form(array('type' => 'line','show_data' => 1), $this);?>
			<?php endif; ?>
			
			<?php echo tpl_function_breadcrumbs(array(), $this);?>