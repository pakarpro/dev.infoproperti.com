<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-05-12 14:13:48 KRAT */ ?>

<li>
	<a href="<?php echo $this->_vars['site_url']; ?>
listings/edit" id="post_listing_btn" class="post-btn"><?php echo l('link_add_listing', 'listings', '', 'text', array()); ?>&nbsp;<span class="plus">+</span></a>
	<?php if ($this->_vars['is_guest']): ?>
	<script><?php echo '
		$(function(){
			$(\'#post_listing_btn\').bind(\'click\', function(){
				$(\'html, body\').animate({
					scrollTop: $("#ajax_login_link").offset().top
				}, 2000);
				$("#ajax_login_link").click();
				return false;
			});
		});
	'; ?>
</script>
	<?php endif; ?>
</li>
