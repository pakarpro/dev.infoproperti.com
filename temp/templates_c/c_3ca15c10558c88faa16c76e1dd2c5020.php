<?php require_once('/opt/ip/system/libraries/template_lite/plugins/modifier.date_format.php'); $this->register_modifier("date_format", "tpl_modifier_date_format");  require_once('/opt/ip/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-09-11 10:04:14 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<div class="actions">
	<ul>
		<?php if ($this->_vars['data']['allow_send'] && ! $this->_vars['data']['send']): ?>
		<li><div class="l"><a href="<?php echo $this->_vars['site_url']; ?>
admin/contact/send/1/<?php echo $this->_vars['data']['id']; ?>
"><?php echo l('link_send', 'contact', '', 'text', array()); ?></a></div></li>
		<?php endif; ?>
		<li><div class="l"><a href="<?php echo $this->_vars['site_url']; ?>
admin/contact/delete/<?php echo $this->_vars['data']['id']; ?>
" onclick="javascript: if(!confirm('<?php echo l('note_delete_contact', 'contact', '', 'js', array()); ?>')) return false;"><?php echo l('btn_delete', 'start', '', 'text', array()); ?></a></div></li>
	</ul>
	&nbsp;
</div>
<div class="edit-form n150">
	<div class="row header"><?php echo l('admin_header_contact_show', 'contact', '', 'text', array()); ?></div>
	<div class="row">
		<div class="h"><?php echo l('field_contact_sender', 'contact', '', 'text', array()); ?>: </div>
		<div class="v"><?php echo $this->_vars['data']['sender']; ?>
</div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_contact_email', 'contact', '', 'text', array()); ?>: </div>
		<div class="v"><?php echo $this->_vars['data']['email']; ?>
</div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_contact_phone', 'contact', '', 'text', array()); ?>: </div>
		<div class="v"><?php echo $this->_vars['data']['phone']; ?>
</div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_contact_message', 'contact', '', 'text', array()); ?>: </div>
		<div class="v"><?php echo $this->_vars['data']['message']; ?>
</div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_contact_date_add', 'contact', '', 'text', array()); ?>: </div>
		<div class="v"><?php echo $this->_run_modifier($this->_vars['data']['date_add'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']); ?>
</div>
	</div>
	<?php if ($this->_vars['data']['allow_send'] || $this->_vars['data']['send']): ?>
	<div class="row">
		<div class="h"><?php echo l('field_contact_send_status', 'contact', '', 'text', array()); ?>: </div>
		<div class="v"><?php if ($this->_vars['data']['send']):  echo l('field_contact_send', 'contact', '', 'text', array());  else:  echo l('field_contact_nosend', 'contact', '', 'text', array());  endif; ?></div>	
	</div>
	<?php endif; ?>
</div>
<a class="cancel" href="<?php echo $this->_vars['site_url']; ?>
admin/contact/index/<?php echo $this->_vars['filter']; ?>
"><?php echo l('btn_cancel', 'start', '', 'text', array()); ?></a>
<div class="clr"></div>
<script><?php echo '
$(function(){
	$("div.row:odd").addClass("zebra");
});
'; ?>
</script>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
