<?php require_once('/opt/ip/system/libraries/template_lite/plugins/function.pagination.php'); $this->register_function("pagination", "tpl_function_pagination");  require_once('/opt/ip/system/libraries/template_lite/plugins/modifier.lower.php'); $this->register_modifier("lower", "tpl_modifier_lower");  require_once('/opt/ip/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-12-15 13:37:52 KRAT */ ?>

<div class="fleft actions hide" id="grouping-btn">
		<a href="#" onclick="javascript: if(confirm('<?php echo l('note_delete_chats', 'mailbox', '', 'js', array()); ?>')) <?php echo '{'; ?>
 mb.delete_chats(0); <?php echo '}'; ?>
 return false;" class="btn-link"><ins class="with-icon i-delete"></ins></a>
		<a href="#" onclick="javascript: mb.open_move_form(0); return false;" class="btn-link"><ins class="with-icon i-folder"></ins></a>
</div>
<div class="clr"></div>
<div>	
		<table class="list">
		<tr id="sorter_block">
			<th class="w30"><?php if ($this->_vars['chats']): ?><input type="checkbox" class="grouping_all"><?php else: ?>&nbsp;<?php endif; ?></th>		
			<th><?php echo l('field_nickname', 'mailbox', '', 'text', array()); ?></th>		
			<th class="w150"><a href="#" orderkey="new" class="link-sorter"><?php echo l('field_new_messages', 'mailbox', '', 'text', array());  if ($this->_vars['page_data']['order'] == 'new'): ?><ins class="fright i-sorter with-icon-small <?php echo $this->_run_modifier($this->_vars['page_data']['order_direction'], 'lower', 'plugin', 1); ?>
"></ins><?php endif; ?></a></th>		
			<th class="w125"><a href="#" orderkey="total" class="link-sorter"><?php echo l('field_all_messages', 'mailbox', '', 'text', array());  if ($this->_vars['page_data']['order'] == 'total'): ?><ins class="fright i-sorter with-icon-small <?php echo $this->_run_modifier($this->_vars['page_data']['order_direction'], 'lower', 'plugin', 1); ?>
"></ins><?php endif; ?></a></th>		
			<th class="w100"><?php echo l('actions', 'mailbox', '', 'text', array()); ?></th>	
		</tr>
		<?php if (is_array($this->_vars['chats']) and count((array)$this->_vars['chats'])): foreach ((array)$this->_vars['chats'] as $this->_vars['item']): ?>
		<tr>
			<td><input type="checkbox" class="grouping chat_item" value="<?php echo $this->_vars['item']['id']; ?>
"></td>
			<td><?php echo $this->_vars['item']['user_data']['output_name']; ?>
</td>
			<td><?php echo $this->_vars['item']['new']; ?>
</td>
			<td><?php echo $this->_vars['item']['total']; ?>
</td>
			<td>
				<a href="#" onclick="javascript: if(confirm('<?php echo l('note_delete_chat', 'mailbox', '', 'js', array()); ?>')) <?php echo '{'; ?>
 mb.delete_chats(<?php echo $this->_vars['item']['id']; ?>
); <?php echo '}'; ?>
 return false;" class="btn-link fright"><ins class="with-icon i-delete"></ins></a>
				<a href="<?php echo $this->_vars['site_url']; ?>
mailbox/chat/<?php echo $this->_vars['item']['user_data']['id']; ?>
" class="btn-link fright"><ins class="with-icon i-eye"></ins></a>
			</td>
		</tr>
		<?php endforeach; else: ?>
			<tr>
				<td colspan="5" class="empty"><?php echo l('no_chats', 'mailbox', '', 'text', array()); ?></td>
			</tr>
		<?php endif; ?>
		</table>	

</div>
<div id="pages_block_2"><?php echo tpl_function_pagination(array('data' => $this->_vars['page_data'],'type' => 'full'), $this);?></div>
