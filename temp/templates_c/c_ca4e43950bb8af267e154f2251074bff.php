<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.block.php'); $this->register_function("block", "tpl_function_block");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2014-10-15 00:32:30 KRAT */ ?>

<div class="load_content_controller">
	<h1><?php echo l('header_add_funds', 'users_payments', '', 'text', array()); ?></h1>
	<div class="inside">
	<div class="edit-form n100">
		<div class="row">
			<div class="h"><?php echo l('field_amount', 'users_payments', '', 'text', array()); ?>: </div>
			<div class="v"><input type="text" value="" name="amount" class="short" id="add_fund_amount"> <?php echo tpl_function_block(array('name' => currency_output,'module' => start), $this);?></div>
		</div>
	</div>
	<div class="btn"><div class="l"><input type="button" name="btn_save" value="<?php echo l('btn_add', 'start', '', 'button', array()); ?>" onclick="javascript: send_add_funds();"></div></div>
	<div class="clr"></div>
	</div>
</div>
