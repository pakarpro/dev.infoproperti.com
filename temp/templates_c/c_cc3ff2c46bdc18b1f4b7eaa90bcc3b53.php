<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-02 15:55:32 KRAT */ ?>

	<!-- user_info right panel -->
	<div class="user_info">
		<h2><?php echo l('header_provided_by', 'listings', '', 'text', array()); ?></h2>
		<?php if ($this->_vars['user']['status']): ?>
		<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'user_actions')); tpl_block_capture(array('assign' => 'user_actions'), null, $this); ob_start();  $this->_tag_stack[] = array('tpl_block_strip', array()); tpl_block_strip(array(), null, $this); ob_start(); ?>
			<?php echo tpl_function_block(array('name' => 'button_contact','module' => 'mailbox','user_id' => $this->_vars['user']['id'],'user_type' => $this->_vars['user']['user_type']), $this);?>
			<?php if ($this->_vars['show_all_listings']):  echo tpl_function_block(array('name' => 'user_listings_button','module' => 'listings','user' => $this->_vars['user']), $this); endif; ?>
			<?php echo tpl_function_block(array('name' => 'send_review_block','module' => 'reviews','object_id' => $this->_vars['user']['id'],'type_gid' => 'users_object','responder_id' => $this->_vars['user']['id'],'is_owner' => $this->_vars['is_user_owner']), $this);?>
			<?php echo tpl_function_block(array('name' => 'mark_as_spam_block','module' => 'spam','object_id' => $this->_vars['user']['id'],'type_gid' => 'users_object','is_owner' => $this->_vars['is_user_owner']), $this);?>
		<?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_strip($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack);  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
		<?php if ($this->_vars['user_actions']): ?>
		<div class="actions noPrint">
			<?php echo $this->_vars['user_actions']; ?>

		</div>
		<?php endif; ?>
		<?php endif; ?>
		
		<div class="clr"></div>	
		<div class="image">
			<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'user_logo')); tpl_block_capture(array('assign' => 'user_logo'), null, $this); ob_start(); ?>
				<img src="<?php echo $this->_vars['user']['media']['user_logo']['thumbs']['big']; ?>
" title="<?php echo $this->_run_modifier($this->_vars['user']['output_name'], 'escape', 'plugin', 1); ?>
" alt="<?php echo $this->_run_modifier($this->_run_modifier($this->_vars['user']['output_name'], 'truncate', 'plugin', 1, 30), 'escape', 'plugin', 1); ?>
">
			<?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
			<?php if ($this->_vars['user']['status']): ?>
			<a href="<?php echo tpl_function_seolink(array('module' => 'users','method' => 'view','data' => $this->_vars['user']), $this);?>"><?php echo $this->_vars['user_logo']; ?>
</a><?php else:  echo $this->_vars['user_logo'];  endif; ?>
		</div>
		<h3><?php if ($this->_vars['user']['status']): ?><a href="<?php echo tpl_function_seolink(array('module' => 'users','method' => 'view','data' => $this->_vars['user']), $this);?>"><?php echo $this->_run_modifier($this->_vars['user']['output_name'], 'truncate', 'plugin', 1, 30); ?>
</a><?php else:  echo $this->_run_modifier($this->_vars['user']['output_name'], 'truncate', 'plugin', 1, 30);  endif; ?>, <?php echo tpl_function_ld_option(array('i' => 'user_type','gid' => 'users','option' => $this->_vars['user']['user_type']), $this);?></h3>
		<?php if ($this->_vars['user']['status']): ?>
		<div id="user_block">
			<div class="tabs tab-size-15 noPrint">
				<ul id="user_sections">
					<li id="ui_contacts" sgid="contacts" class="active"><a href="<?php echo $this->_vars['site_url']; ?>
listings/user/<?php echo $this->_vars['user']['id']; ?>
/<?php echo $this->_vars['sgid']; ?>
"><?php echo l('filter_section_info', 'listings', '', 'text', array()); ?></a></li>
					<?php if ($this->_vars['user']['user_type'] == 'company'): ?><li id="ui_map_info" sgid="map_info" class="<?php if ($this->_vars['section_gid'] == 'map_info'): ?>active<?php endif; ?>"><a href="<?php echo $this->_vars['site_url']; ?>
listings/user/<?php echo $this->_vars['user']['id']; ?>
/<?php echo $this->_vars['sgid']; ?>
"><?php echo l('filter_section_map', 'listings', '', 'text', array()); ?></a></li><?php endif; ?>
					<?php if ($this->_vars['show_contact_form']): ?><li id="ui_contact" sgid="contact" class="<?php if ($this->_vars['section_gid'] == 'contact'): ?>active<?php endif; ?>"><a href="<?php echo $this->_vars['site_url']; ?>
listings/user/<?php echo $this->_vars['user']['id']; ?>
/<?php echo $this->_vars['sgid']; ?>
"><?php echo l('filter_section_contact', 'listings', '', 'text', array()); ?></a></li><?php endif; ?>
				</ul>
			</div>
			<div id="content_ui_contacts" class="view-section print_block">
			    <?php echo tpl_function_block(array('name' => view_user_block,'module' => users,'user' => $this->_vars['user'],'template' => 'small'), $this);?>
				<p><a href="<?php echo $this->_vars['site_url']; ?>
contact_us/index/" class="underline bold">Click here</a>, if you are the owner of this venture for customization and full benefits of our services.</p>
			</div>
			<div id="content_ui_contact" class="view-section hide noPrint"><?php echo tpl_function_block(array('name' => show_contact_form,'module' => contact,'user_id' => $this->_vars['user']['id']), $this);?></div>
			<?php if ($this->_vars['user']['user_type'] == 'company'): ?>
			    <div id="content_ui_map_info" class="view-section<?php if ($this->_vars['section_gid'] != 'map'): ?> hide<?php endif; ?> noPrint">
				<?php echo tpl_function_block(array('name' => show_default_map,'module' => geomap,'id_user' => $this->_vars['user_id'],'markers' => $this->_vars['markers'],'settings' => $this->_vars['map_settings'],'width' => '300','height' => '300','only_load_scripts' => 1), $this);?>
			    </div>
			<?php endif; ?>
		</div>
		
		<?php echo tpl_function_js(array('module' => users,'file' => 'users-menu.js'), $this);?>
		<?php echo tpl_function_js(array('module' => users_services,'file' => 'available_view.js'), $this);?>
		<script><?php echo '
			var rMenu;
			$(function(){
				rMenu = new usersMenu({
					siteUrl: \'';  echo $this->_vars['site_url'];  echo '\',
					idUser: \'';  echo $this->_vars['user']['id'];  echo '\',
					tabPrefix: \'ui\',
					CurrentSection: \'ui_contacts\',
					template: \'small\',
					'; ?>
available_view: new available_view(),<?php echo '
				});
			});
		'; ?>
</script>		
		<?php endif; ?>
	</div>	
