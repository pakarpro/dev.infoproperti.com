<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-02 15:54:48 KRAT */ ?>

<?php if ($this->_vars['user_id']): ?>
<li class="user_top_menu_block">
	<a href="<?php echo $this->_vars['site_url']; ?>
users/profile"><img src="<?php echo $this->_run_modifier($this->_vars['user_logo'], 'escape', 'plugin', 1); ?>
" title="<?php echo $this->_run_modifier($this->_vars['user_name'], 'escape', 'plugin', 1); ?>
" width="28" height="28"></a> <a href="<?php echo $this->_vars['site_url']; ?>
users/profile"><?php echo $this->_run_modifier($this->_run_modifier($this->_vars['user_name'], 'truncate', 'plugin', 1, 25), 'escape', 'plugin', 1); ?>
<div class="arrow"></div></a>
	<?php echo tpl_function_menu(array('gid' => $this->_vars['user_type'].'_top_menu','template' => 'user_top_menu'), $this);?>
</li>
<script><?php echo '
	$(function(){
		$(\'.user_top_menu_block\').each(function(i, item){
			var element = $(item);
			var submenu = element.find(\'ul\');
			if(submenu.length == 0) return;
			element.children(\'a\').bind(\'touchstart\', function(){
				element.toggleClass(\'hover\');
				return false;
			});
			element.children(\'a\').bind(\'touchenter\', function(){
				element.removeClass(\'hover\');
				element.trigger(\'hover\');
				return false;
			}).bind(\'touchleave\', function(){
				element.removeClass(\'hover\').trigger(\'blur\');
				return false;
			});
		});
	});
'; ?>
</script>
<?php else: ?>
<li><a href="#" onclick="javascript: ; return false;" id="ajax_login_link"><?php echo l('header_login', 'users', '', 'text', array()); ?></a></li>
<!-- <li><a href="<?php echo $this->_vars['site_url']; ?>
users/login_form" onclick="javascript: ; return false;" id="ajax_login_link"><button id="loginsignup-btn"><?php echo l('header_login', 'users', '', 'text', array()); ?></button></a></li>-->
<script>
var user_ajax_login;
var login_ajax_url = '<?php echo $this->_vars['site_url']; ?>
users/ajax_login_form';
<?php echo '
$(function(){
	$(\'#ajax_login_linkx\').click(function(){
		ajax_login_form(login_ajax_url);
		return false;
	});
	user_ajax_login = new loadingContent({
		loadBlockWidth: \'520px\',
		linkerObjID: \'ajax_login_link\',
		loadBlockLeftType: \'right\',
		loadBlockTopType: \'bottom\'
	});
});
function ajax_login_form(url){
	$.ajax({
		url: url,
		cache: false,
		success: function(data){
			user_ajax_login.show_load_block(data);
		}
	});
}
'; ?>

</script>
<?php endif; ?>
