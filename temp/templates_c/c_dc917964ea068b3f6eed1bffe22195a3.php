<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-05-12 14:12:19 KRAT */ ?>

	<?php switch($this->_vars['template']): case 'button':  ?>
		<a id="contact_link" href="javascript:void(0);" class="btn-link" title="<?php echo l('link_contact_'.$this->_vars['user_type'], 'mailbox', '', 'button', array()); ?>"><ins class="with-icon i-mail"></ins></a><span class="btn-text link-r-margin"><?php echo l('btn_contact', 'mailbox', '', 'text', array()); ?></span>
	<?php break; case 'icon':  ?>
		<a id="contact_link" href="javascript:void(0);" class="btn-link link-r-margin" title="<?php echo l('link_contact_'.$this->_vars['user_type'], 'mailbox', '', 'button', array()); ?>"><ins class="with-icon i-mail"></ins></a>
<?php break; endswitch; ?>
<?php echo tpl_function_js(array('module' => mailbox,'file' => 'message_link.js'), $this);?>
<?php echo tpl_function_js(array('module' => users_services,'file' => 'available_view.js'), $this);?>
<script type='text/javascript'>
	<?php echo '
	$(function(){
		message_link({
			siteUrl: \'';  echo $this->_vars['site_url'];  echo '\',
			idContact: \'';  echo $this->_vars['user_id'];  echo '\',
			';  if ($this->_vars['is_guest']): ?>display_login: true,<?php endif;  echo '
			'; ?>
available_view: new available_view(),<?php echo '
		});
	});
	'; ?>

</script>
