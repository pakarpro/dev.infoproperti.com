<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.escape.php'); $this->register_modifier("escape", "tpl_modifier_escape");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.counter.php'); $this->register_function("counter", "tpl_function_counter");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-05-08 09:56:22 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<div class="menu-level2">
	<ul>
		<li<?php if ($this->_vars['section'] == 'overview'): ?> class="active"<?php endif; ?>><div class="l"><a href="<?php echo $this->_vars['site_url']; ?>
admin/start/settings/overview"><?php echo l('sett_overview_item', 'start', '', 'text', array()); ?></a></div></li>
		<li<?php if ($this->_vars['section'] == 'numerics'): ?> class="active"<?php endif; ?>><div class="l"><a href="<?php echo $this->_vars['site_url']; ?>
admin/start/settings/numerics"><?php echo l('sett_numerics_item', 'start', '', 'text', array()); ?></a></div></li>
		<li<?php if ($this->_vars['section'] == 'formats'): ?> class="active"<?php endif; ?>><div class="l"><a href="<?php echo $this->_vars['site_url']; ?>
admin/start/settings/formats"><?php echo l('sett_formats_item', 'start', '', 'text', array()); ?></a></div></li>
		<li<?php if ($this->_vars['section'] == 'widgets'): ?> class="active"<?php endif; ?>><div class="l"><a href="<?php echo $this->_vars['site_url']; ?>
admin/start/settings/widgets"><?php echo l('sett_widgets_item', 'start', '', 'text', array()); ?></a></div></li>
		<?php if (is_array($this->_vars['other_settings']) and count((array)$this->_vars['other_settings'])): foreach ((array)$this->_vars['other_settings'] as $this->_vars['key'] => $this->_vars['item']): ?>
		<li<?php if ($this->_vars['key'] == $this->_vars['section']): ?> class="active"<?php endif; ?>><div class="l"><a href="<?php echo $this->_vars['site_url']; ?>
admin/start/settings/<?php echo $this->_vars['key']; ?>
"><?php echo l('sett_'.$this->_vars['key'].'_item', 'start', '', 'text', array()); ?></a></div></li>
		<?php endforeach; endif; ?>
	</ul>
	&nbsp;
</div>
<div class="actions">&nbsp;</div>
<?php if ($this->_vars['section'] == 'overview'): ?>
<div class="right-side">
	<table cellspacing="0" cellpadding="0" class="data" width="100%">
	<?php if (is_array($this->_vars['settings_data']['other']) and count((array)$this->_vars['settings_data']['other'])): foreach ((array)$this->_vars['settings_data']['other'] as $this->_vars['module'] => $this->_vars['module_data']): ?>
	<tr>
		<th class="first" colspan=2><?php echo $this->_vars['module_data']['name']; ?>
</th>
	</tr>
	<?php if (is_array($this->_vars['module_data']['vars']) and count((array)$this->_vars['module_data']['vars'])): foreach ((array)$this->_vars['module_data']['vars'] as $this->_vars['key'] => $this->_vars['item']): ?>
	<tr<?php if (!($this->_vars['key'] % 2)): ?> class="zebra"<?php endif; ?>>
		<td class="first"><?php echo $this->_vars['item']['field_name']; ?>
</td>
		<td>
			<?php if ($this->_vars['item']['type'] == 'checkbox'): ?>
				<?php if ($this->_vars['item']['value']): ?>
					<?php echo l('option_checkbox_yes', 'start', '', 'text', array()); ?>
				<?php else: ?>
					<?php echo l('option_checkbox_no', 'start', '', 'text', array()); ?>
				<?php endif; ?>
			<?php else: ?>
				<?php echo $this->_vars['item']['value']; ?>

			<?php endif; ?>
		</td>
	</tr>
	<?php endforeach; endif; ?>
	<?php endforeach; endif; ?>
	</table>
</div>

<div class="left-side">
	<?php echo tpl_function_counter(array('print' => false,'assign' => counter,'start' => 0), $this);?>
	<table cellspacing="0" cellpadding="0" class="data" width="100%">
	<tr>
		<th class="first" colspan=2><?php echo l('sett_numerics_item', 'start', '', 'text', array()); ?></th>
	</tr>
	<?php if (is_array($this->_vars['settings_data']['numerics']) and count((array)$this->_vars['settings_data']['numerics'])): foreach ((array)$this->_vars['settings_data']['numerics'] as $this->_vars['module'] => $this->_vars['module_data']): ?>
	<?php if (is_array($this->_vars['module_data']['vars']) and count((array)$this->_vars['module_data']['vars'])): foreach ((array)$this->_vars['module_data']['vars'] as $this->_vars['key'] => $this->_vars['item']): ?>
	<?php echo tpl_function_counter(array('print' => false,'assign' => counter), $this);?>
	<tr<?php if (!($this->_vars['counter'] % 2)): ?> class="zebra"<?php endif; ?>>
		<td class="first"><?php echo $this->_vars['item']['field_name']; ?>
</td>
		<td class="w100">
			<?php if ($this->_vars['item']['type'] == 'checkbox'): ?>
				<?php if ($this->_vars['item']['value']): ?>
					<?php echo l('option_checkbox_yes', 'start', '', 'text', array()); ?>
				<?php else: ?>
					<?php echo l('option_checkbox_no', 'start', '', 'text', array()); ?>
				<?php endif; ?>
			<?php else: ?>
				<?php echo $this->_vars['item']['value']; ?>

			<?php endif; ?>
		</td>
	</tr>
	<?php endforeach; endif; ?>
	<?php endforeach; endif; ?>
	<tr>
		<th class="first" colspan=2><?php echo l('sett_formats_item', 'start', '', 'text', array()); ?></th>
	</tr>
	<?php if (is_array($this->_vars['settings_data']['formats']) and count((array)$this->_vars['settings_data']['formats'])): foreach ((array)$this->_vars['settings_data']['formats'] as $this->_vars['module'] => $this->_vars['module_data']): ?>
	<?php if (is_array($this->_vars['module_data']['vars']) and count((array)$this->_vars['module_data']['vars'])): foreach ((array)$this->_vars['module_data']['vars'] as $this->_vars['key'] => $this->_vars['item']): ?>
	<?php echo tpl_function_counter(array('print' => false,'assign' => counter), $this);?>
	<tr<?php if (!($this->_vars['counter'] % 2)): ?> class="zebra"<?php endif; ?>>
		<td class="first"><?php echo $this->_vars['item']['field_name']; ?>
</td>
		<td class="w100">
			<?php if ($this->_vars['item']['type'] == 'checkbox'): ?>
				<?php if ($this->_vars['item']['value']): ?>
					<?php echo l('option_checkbox_yes', 'start', '', 'text', array()); ?>
				<?php else: ?>
					<?php echo l('option_checkbox_no', 'start', '', 'text', array()); ?>
				<?php endif; ?>
			<?php else: ?>
				<?php echo $this->_vars['item']['value']; ?>

			<?php endif; ?>
		</td>
	</tr>
	<?php endforeach; endif; ?>
	<?php endforeach; endif; ?>
	<tr>
		<th class="first" colspan=2><?php echo l('sett_widgets_item', 'start', '', 'text', array()); ?></th>
	</tr>
	<?php if (is_array($this->_vars['settings_data']['widgets']) and count((array)$this->_vars['settings_data']['widgets'])): foreach ((array)$this->_vars['settings_data']['widgets'] as $this->_vars['module'] => $this->_vars['module_data']): ?>	
	<?php if (is_array($this->_vars['module_data']['vars']) and count((array)$this->_vars['module_data']['vars'])): foreach ((array)$this->_vars['module_data']['vars'] as $this->_vars['key'] => $this->_vars['item']): ?>
	<?php echo tpl_function_counter(array('print' => false,'assign' => counter), $this);?>
	<tr<?php if (!($this->_vars['counter'] % 2)): ?> class="zebra"<?php endif; ?>>
		<td class="first"><?php echo $this->_vars['item']['field_name']; ?>
</td>
		<td class="w100">
			<?php if ($this->_vars['item']['type'] == 'checkbox'): ?>
				<?php if ($this->_vars['item']['value']): ?>
					<?php echo l('option_checkbox_yes', 'start', '', 'text', array()); ?>
				<?php else: ?>
					<?php echo l('option_checkbox_no', 'start', '', 'text', array()); ?>
				<?php endif; ?>
			<?php else: ?>
				<?php echo $this->_vars['item']['value']; ?>

			<?php endif; ?>
		</td>
	</tr>
	<?php endforeach; endif; ?>
	<?php endforeach; endif; ?>
	</table>
</div>
<div class="clr"><a class="cancel" href="<?php echo $this->_vars['site_url']; ?>
admin/start/menu/system-items"><?php echo l('btn_cancel', 'start', '', 'text', array()); ?></a></div>


<?php elseif ($this->_vars['section'] == 'numerics' || $this->_vars['section'] == 'formats' || $this->_vars['section'] == 'widgets'): ?>
<form method="post" action="<?php echo $this->_vars['data']['action']; ?>
" name="save_form" enctype="multipart/form-data">
	<div class="edit-form n250">
		<?php echo tpl_function_counter(array('print' => false,'assign' => counter,'start' => 0), $this);?>
		<?php if (is_array($this->_vars['settings_data']) and count((array)$this->_vars['settings_data'])): foreach ((array)$this->_vars['settings_data'] as $this->_vars['module'] => $this->_vars['module_data']): ?>
		
		<?php if (is_array($this->_vars['module_data']['vars']) and count((array)$this->_vars['module_data']['vars'])): foreach ((array)$this->_vars['module_data']['vars'] as $this->_vars['key'] => $this->_vars['item']): ?>
		<?php echo tpl_function_counter(array('print' => false,'assign' => counter), $this);?>
		<div class="row<?php if (!($this->_vars['counter'] % 2)): ?> zebra<?php endif; ?>">
			<div class="h"><?php echo $this->_vars['item']['field_name']; ?>
:</div>
			<div class="v">
				<?php if ($this->_vars['item']['type'] == 'checkbox'): ?>
				<input type="hidden" name="settings[<?php echo $this->_vars['module']; ?>
][<?php echo $this->_vars['item']['field']; ?>
]" value="0">
				<input type="checkbox" name="settings[<?php echo $this->_vars['module']; ?>
][<?php echo $this->_vars['item']['field']; ?>
]" value="1" <?php if ($this->_vars['item']['value']): ?>checked<?php endif; ?> class="short">
				<?php elseif ($this->_vars['item']['type'] == 'int'): ?>
				<input type="text" name="settings[<?php echo $this->_vars['module']; ?>
][<?php echo $this->_vars['item']['field']; ?>
]" value="<?php echo $this->_run_modifier($this->_vars['item']['value'], 'escape', 'plugin', 1); ?>
" class="short">
				<?php elseif ($this->_vars['item']['type'] == 'text'): ?>
				<input type="text" name="settings[<?php echo $this->_vars['module']; ?>
][<?php echo $this->_vars['item']['field']; ?>
]" value="<?php echo $this->_run_modifier($this->_vars['item']['value'], 'escape', 'plugin', 1); ?>
" class="middle">
				<?php elseif ($this->_vars['item']['type'] == 'textarea'): ?>
				<textarea name="settings[<?php echo $this->_vars['module']; ?>
][<?php echo $this->_vars['item']['field']; ?>
]" rows="5" cols="80"><?php echo $this->_run_modifier($this->_vars['item']['value'], 'escape', 'plugin', 1); ?>
"</textarea>
				<?php else: ?>
				<input type="text" name="settings[<?php echo $this->_vars['module']; ?>
][<?php echo $this->_vars['item']['field']; ?>
]" value="<?php echo $this->_run_modifier($this->_vars['item']['value'], 'escape', 'plugin', 1); ?>
" class="short">
				<?php endif; ?>
			</div>
		</div>
		<?php endforeach; endif; ?>
		<?php endforeach; endif; ?>
	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div></div>
	<a class="cancel" href="<?php echo $this->_vars['site_url']; ?>
admin/start/menu/system-items"><?php echo l('btn_cancel', 'start', '', 'text', array()); ?></a>
</form>
<?php else: ?>
<form method="post" action="<?php echo $this->_vars['data']['action']; ?>
" name="save_form" enctype="multipart/form-data">
	<div class="edit-form n250">
		<div class="row header"><?php echo $this->_vars['settings_data']['name']; ?>
</div>
		<?php if (is_array($this->_vars['settings_data']['vars']) and count((array)$this->_vars['settings_data']['vars'])): foreach ((array)$this->_vars['settings_data']['vars'] as $this->_vars['key'] => $this->_vars['item']): ?>
		<?php if (! ( $this->_vars['section'] == 'listings' && $this->_vars['item']['field'] == 'admin_moderation_emails' )): ?>
		<div class="row<?php if (!($this->_vars['key'] % 2)): ?> zebra<?php endif; ?>">
		<?php if ($this->_vars['section'] == 'countries'): ?>
			<div class="h"><?php echo $this->_vars['item']['field_name']; ?>
:</div>
			<div class="v"><input type="text" name="settings[<?php echo $this->_vars['item']['field']; ?>
]" value="<?php echo $this->_run_modifier($this->_vars['item']['value'], 'escape', 'plugin', 1); ?>
"><br><i><?php echo l($this->_vars['item']['field'].'_settings_descr', 'countries', '', 'text', array()); ?></i></div>
		<?php else: ?>
			<div class="h"><?php echo $this->_vars['item']['field_name']; ?>
:</div>
			<div class="v">
				<?php if ($this->_vars['item']['type'] == 'checkbox'): ?>
				<input type="hidden" name="settings[<?php echo $this->_vars['item']['field']; ?>
]" value="0">
				<input type="checkbox" name="settings[<?php echo $this->_vars['item']['field']; ?>
]" value="1" <?php if ($this->_vars['item']['value']): ?>checked<?php endif; ?> class="short" <?php if ($this->_vars['section'] == 'listings' && $this->_vars['item']['field'] == 'moderation_send_mail'): ?>id="moderation_send_mail"<?php endif; ?>>
				<?php if ($this->_vars['section'] == 'listings' && $this->_vars['item']['field'] == 'moderation_send_mail'): ?>
				&nbsp;&nbsp;
				<?php echo $this->_vars['admin_moderation_emails']['field_name']; ?>

				<input type="text" name="settings[admin_moderation_emails]" value="<?php echo $this->_run_modifier($this->_vars['admin_moderation_emails']['value'], 'escape', 'plugin', 1); ?>
" id="admin_moderation_emails" <?php if (! $this->_vars['item']['value']): ?>disabled<?php endif; ?>> 
				<script><?php echo '
					$(function(){
						$("div.row:not(.hide):even").addClass("zebra");
						$(\'#moderation_send_mail\').bind(\'change\', function(){
							if(this.checked){
								$(\'#admin_moderation_emails\').removeAttr(\'disabled\');
							}else{
								$(\'#admin_moderation_emails\').attr(\'disabled\', \'disabled\');
							}
						});
					});
				'; ?>
</script>
				<?php endif; ?>
				<?php elseif ($this->_vars['item']['type'] == 'int'): ?>
				<input type="text" name="settings[<?php echo $this->_vars['item']['field']; ?>
]" value="<?php echo $this->_run_modifier($this->_vars['item']['value'], 'escape', 'plugin', 1); ?>
" class="short">
				<?php elseif ($this->_vars['item']['type'] == 'text'): ?>
				<input type="text" name="settings[<?php echo $this->_vars['item']['field']; ?>
]" value="<?php echo $this->_run_modifier($this->_vars['item']['value'], 'escape', 'plugin', 1); ?>
" class="middle">
				<?php elseif ($this->_vars['item']['type'] == 'textarea'): ?>
				<textarea name="settings[<?php echo $this->_vars['item']['field']; ?>
]" rows="5" cols="80"><?php echo $this->_run_modifier($this->_vars['item']['value'], 'escape', 'plugin', 1); ?>
"</textarea>
				<?php else: ?>
				<input type="text" name="settings[<?php echo $this->_vars['item']['field']; ?>
]" value="<?php echo $this->_run_modifier($this->_vars['item']['value'], 'escape', 'plugin', 1); ?>
" class="short">
				<?php endif; ?>
			</div>
		<?php endif; ?>
		</div>
		<?php endif; ?>
		<?php endforeach; endif; ?>
	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div></div>
	<a class="cancel" href="<?php echo $this->_vars['site_url']; ?>
admin/start/menu/system-items"><?php echo l('btn_cancel', 'start', '', 'text', array()); ?></a>
</form>
<?php endif; ?>
<div class="clr"></div>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
