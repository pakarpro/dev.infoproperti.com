<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-02 12:58:11 KRAT */ ?>

	<?php if ($this->_vars['view_mode'] == 'map'): ?>
		<?php $this->assign('current_user_id', $this->_vars['user']['id']); ?>
			<div class="user-block">
				<div class="item no-hover no-border user">
					<h3><a href="<?php echo tpl_function_seolink(array('module' => 'users','method' => 'view','data' => $this->_vars['user']), $this);?>" title="<?php echo $this->_run_modifier($this->_vars['user']['output_name'], 'escape', 'plugin', 1); ?>
"><?php echo $this->_run_modifier($this->_vars['user']['output_name'], 'truncate', 'plugin', 1, 50); ?>
</a>, <?php echo l($this->_vars['user']['user_type'], 'users', '', 'text', array()); ?></h3>
					<div class="image">
						<a href="<?php echo tpl_function_seolink(array('module' => 'users','method' => 'view','data' => $this->_vars['user']), $this);?>"><img src="<?php echo $this->_vars['user']['media']['user_logo']['thumbs']['middle']; ?>
" title="<?php echo $this->_run_modifier($this->_vars['user']['output_name'], 'escape', 'plugin', 1); ?>
"></a>
					</div>
					<div class="body">
						<?php if ($this->_vars['user_type'] == "agent" && $this->_vars['user']['company']): ?><h3><?php echo $this->_run_modifier($this->_vars['user']['company']['output_name'], 'truncate', 'plugin', 1, 50); ?>
</h3><?php endif; ?>
						<?php if ($this->_vars['is_contact'] || $this->_vars['users_for_contact'][$this->_vars['current_user_id']]): ?><h3><?php echo $this->_vars['user']['phone']; ?>
</h3><?php endif; ?>
						<div class="t-1"></div>
						<div class="t-2">
							<span class="status_text"><?php if ($this->_vars['user']['is_featured']):  echo l('status_featured', 'users', '', 'text', array());  endif; ?></span>
						</div>		
						<div class="t-3">
							<?php if ($this->_vars['user']['user_type'] == 'company' && $this->_vars['user']['agent_count']): ?><span><?php echo l('field_agent_count', 'users', '', 'text', array()); ?>:</span> <?php echo $this->_vars['user']['agent_count']; ?>
<br><?php endif; ?>
							<?php if ($this->_vars['user']['listings_for_sale_count']): ?><span><?php echo l('field_listings_for_sale', 'users', '', 'text', array()); ?>:</span> <?php echo $this->_vars['user']['listings_for_sale_count']; ?>
<br><?php endif; ?>
							<?php if ($this->_vars['user']['listings_for_rent_count']): ?><span><?php echo l('field_listings_for_rent', 'users', '', 'text', array()); ?>:</span> <?php echo $this->_vars['user']['listings_for_buy_count']; ?>
<br><?php endif; ?>
						</div>
						<div class="t-4">
														<span><?php echo l('field_rate', 'users', '', 'text', array()); ?>:</span> <?php echo $this->_vars['user']['review_sorter']; ?>
<br>
							<span><?php echo l('field_reviews_count', 'users', '', 'text', array()); ?>:</span> <?php echo $this->_vars['user']['review_count']; ?>
<br>
													</div>
					</div>
					<div class="clr"></div>
				</div>
			</div>
	<?php else: ?>
		<a href="<?php echo tpl_function_seolink(array('module' => 'users','method' => 'view','data' => $this->_vars['user']), $this);?>" title="<?php echo $this->_run_modifier($this->_vars['user']['output_name'], 'escape', 'plugin', 1); ?>
"><?php echo $this->_run_modifier($this->_vars['user']['output_name'], 'truncate', 'plugin', 1, 20); ?>
</a>, <?php echo l($this->_vars['user']['user_type'], 'users', '', 'text', array()); ?>
	<?php endif; ?>
