<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-03-17 18:02:39 KRAT */ ?>

<div class="content-block">
	<h2><?php echo l('featured_user_services', 'users_services', '', 'text', array()); ?></h2>
	<p class="header-comment">
	<?php if ($this->_vars['is_user_featured']['is_featured']): ?>
		<?php echo l('your_featured_period_till', 'users_services', '', 'text', array()); ?>: <?php echo $this->_run_modifier($this->_vars['is_user_featured']['featured_end_date'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']); ?>
<br />
		<?php echo l('add_more_featured_period', 'users_services', '', 'text', array()); ?><br />
	<?php else: ?>
		<?php echo l('create_featured_period', 'users_services', '', 'text', array()); ?>
	<?php endif; ?>
	</p>
	<div class="r">
		<div class="b"><input type="button" class='btn' value="<?php echo l('btn_apply', 'start', '', 'button', array()); ?>" onclick="document.location.href='<?php echo $this->_vars['site_url']; ?>
users_services/apply_service/<?php echo $this->_vars['user_id']; ?>
/agent_featured_services'"></div>
	</div>
</div>

