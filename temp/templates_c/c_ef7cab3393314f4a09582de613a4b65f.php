<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-01 17:25:18 KRAT */ ?>

<?php if (count ( $this->_vars['services'] ) > 0): ?>
<div class="line top">
<span class="btn-text"><?php echo l('you_can_auth', 'users', '', 'text', array()); ?></span>
<?php if (is_array($this->_vars['services']) and count((array)$this->_vars['services'])): foreach ((array)$this->_vars['services'] as $this->_vars['item']): ?>
	<a href="<?php echo $this->_vars['site_url']; ?>
users_connections/oauth_login/<?php echo $this->_vars['item']['id']; ?>
" class="btn-link"><ins class="with-icon i-<?php echo $this->_vars['item']['gid']; ?>
 link-margin"></ins></a>
<?php endforeach; endif; ?>
</div>
<?php endif; ?>