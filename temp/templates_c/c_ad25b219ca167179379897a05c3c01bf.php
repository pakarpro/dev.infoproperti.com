<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.date_format.php'); $this->register_modifier("date_format", "tpl_modifier_date_format");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.escape.php'); $this->register_modifier("escape", "tpl_modifier_escape");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.js.php'); $this->register_function("js", "tpl_function_js");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/block.capture.php'); $this->register_block("capture", "tpl_block_capture");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-05-12 08:51:37 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

	<div class="content-block">
		<div class="edit_block">

			<h1><?php if ($this->_vars['data']['id']):  echo l('header_listing_edit', 'listings', '', 'text', array());  else:  echo l('header_add_listing', 'listings', '', 'text', array());  endif; ?></h1>

			<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->module_path. "listings". $this->module_templates.  $this->get_current_theme_gid('', '"listings"'). "my_listings_menu.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

			<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => "service_block")); tpl_block_capture(array('assign' => "service_block"), null, $this); ob_start(); ?>
				<?php if ($this->_vars['page_data']['use_services']): ?>
					<?php if ($this->_vars['page_data']['user_services']): ?>
					<select name="id_user_service">
					<?php if (is_array($this->_vars['page_data']['user_services']) and count((array)$this->_vars['page_data']['user_services'])): foreach ((array)$this->_vars['page_data']['user_services'] as $this->_vars['item']): ?>
					<?php $this->assign('service_name', $this->_vars['item']['service_name']); ?>
					<option value="<?php echo $this->_vars['item']['id']; ?>
"><?php echo l("service_string_name_" . $this->_vars['service_name'] . "", 'listings', '', 'text', array()); ?> (<?php echo l('listing_service_post_left', 'listings', '', 'text', array()); ?>: <?php echo $this->_vars['item']['post_count']; ?>
/<?php echo $this->_vars['item']['post_period']; ?>
 <?php echo l('listing_service_days', 'listings', '', 'text', array()); ?>)</option>
					<?php endforeach; endif; ?>
					</select><br><br>
					<input type="submit" name="btn_activate" value="<?php echo l('btn_activate', 'listings', '', 'button', array()); ?>">
					<?php else: ?>
					<a href="<?php echo $this->_vars['site_url']; ?>
users_services/index/post" target="blank"><?php echo l('listing_service_link_buy', 'listings', '', 'text', array()); ?></a>
					<?php endif; ?>
				<?php else: ?>
				<p><?php echo l('listing_service_active_period', 'listings', '', 'text', array()); ?>: <b><?php echo $this->_vars['page_data']['default_activation_period']; ?>
 <?php echo l('listing_service_days', 'listings', '', 'text', array()); ?></b>. </p>
				<input type="submit" name="btn_activate" value="<?php echo l('btn_activate', 'listings', '', 'button', array()); ?>">
				<?php endif; ?>
			<?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
			<?php echo tpl_function_js(array('module' => listings,'file' => 'listings-edit-steps.js'), $this);?>
			<script><?php echo '
			$(function(){
				new listingsEditSteps();
			});
			'; ?>
</script>

			<form method="post" action="<?php echo $this->_run_modifier($this->_vars['data']['action'], 'escape', 'plugin', 1); ?>
" name="save_form" id="save_form" enctype="multipart/form-data">
			
			<div class="rollup-box">
				<div class="title">
					<div class="btn-link"><ins class="with-icon i-collapse"></ins></div>
					<h2><?php echo l('header_activation_listing', 'listings', '', 'text', array()); ?></h2>
				</div>
				<div class="cont">

				<?php if ($this->_vars['data']['status'] == '1'): ?>
					<p>
						<?php echo l('listing_active_up_to', 'listings', '', 'text', array()); ?>: <u><?php echo $this->_run_modifier($this->_vars['data']['date_expire'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']); ?>
</u>. <br>
						<?php echo l('deactivation_text_1', 'listings', '', 'text', array()); ?>
					</p>
					<input type="submit" name="btn_deactivate" value="<?php echo l('btn_deactivate', 'listings', '', 'button', array()); ?>">
					<?php if ($this->_vars['data']['operation_type'] == 'sale'): ?>
						<?php if ($this->_vars['data']['sold']): ?>
						<input type="submit" name="btn_delete_sold" value="<?php echo l('btn_delete_sold', 'listings', '', 'button', array()); ?>">
						<?php else: ?>
						<input type="submit" name="btn_make_sold" value="<?php echo l('btn_make_sold', 'listings', '', 'button', array()); ?>">
						<?php endif; ?>
					<?php endif; ?>
					<br>
					<?php if ($this->_vars['page_data']['use_services']): ?>
					<p><?php echo l('activation_text_0', 'listings', '', 'text', array()); ?></p>
					<?php echo $this->_vars['service_block']; ?>

					<?php endif; ?>
				<?php elseif ($this->_vars['data']['status'] == '0' && $this->_vars['page_data']['can_reactivate']): ?>
					<p><?php echo l('activation_text_1', 'listings', '', 'text', array()); ?></p>
					<input type="submit" name="btn_reactivate" value="<?php echo l('btn_reactivate', 'listings', '', 'button', array()); ?>"><br>
				<?php elseif ($this->_vars['data']['status'] == '0' && $this->_vars['data']['initial_moderation'] == '1'): ?>
					<p><?php echo l('activation_text_1', 'listings', '', 'text', array()); ?></p>
					<?php echo $this->_vars['service_block']; ?>

				<?php elseif ($this->_vars['data']['status'] == '0' && $this->_vars['data']['initial_moderation'] == '0' && $this->_vars['data']['initial_activity'] == '0'): ?>
					<p><?php echo l('activation_text_2', 'listings', '', 'text', array()); ?></p>
					<?php echo $this->_vars['service_block']; ?>

				<?php else: ?>
					<p><?php echo l('activation_text_3', 'listings', '', 'text', array()); ?></p>
				<?php endif; ?>
				</div>
			</div>
			<div class="b outside">
				<input type="button" name="btn_previous" value="<?php echo l('nav_prev', 'start', '', 'button', array()); ?>" class="btn hide" id="edit_listings_prev" />
				<input type="button" name="btn_next" value="<?php echo l('nav_next', 'start', '', 'button', array()); ?>" class="btn hide" id="edit_listings_next" />
				<a href="<?php echo $this->_vars['site_url']; ?>
listings/my" class="btn-link"><ins class="with-icon i-larr"></ins><?php echo l('link_back_to_my_listings', 'listings', '', 'text', array()); ?></a>
			</div>
			</form>
			<div class="clr"></div>
		</div>
	</div>

<div class="clr"></div>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
