<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-12-01 14:37:08 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

<div class="rc">
	<div class="content-block">
		<h1><?php echo l('header_my_saved_listings', 'listings', '', 'text', array()); ?> (<?php echo $this->_run_modifier($this->_vars['saved_count'], 'intval', 'PHP', 1); ?>
)</h1>
		
		<div class="tabs tab-size-15 noPrint">
			<ul id="user_listing_sections">
				<?php if (is_array($this->_vars['operation_types']) and count((array)$this->_vars['operation_types'])): foreach ((array)$this->_vars['operation_types'] as $this->_vars['tgid']): ?>
				<li id="m_<?php echo $this->_vars['tgid']; ?>
" sgid="<?php echo $this->_vars['tgid']; ?>
" class="<?php if ($this->_vars['current_operation_type'] == $this->_vars['tgid']): ?>active<?php endif; ?>"><a href="<?php echo $this->_vars['site_url']; ?>
listings/saved/<?php echo $this->_vars['tgid']; ?>
"><?php echo l('operation_search_'.$this->_vars['tgid'], 'listings', '', 'text', array()); ?> (<?php echo $this->_run_modifier($this->_vars['items_count'][$this->_vars['tgid']], 'intval', 'PHP', 1); ?>
)</a></li>
				<?php endforeach; endif; ?>
			</ul>
		</div>
		
		<div id="listings_block"><?php echo $this->_vars['block']; ?>
</div>
		<?php echo tpl_function_js(array('module' => listings,'file' => 'listings-list.js'), $this);?>
		<script><?php echo '
		$(function(){
			lList = new listingsList({
				siteUrl: \'';  echo $this->_vars['site_url'];  echo '\',
				listAjaxUrl: \''; ?>
listings/ajax_saved<?php echo '\',
				sectionId: \'user_listing_sections\',
				operationType: \'';  echo $this->_vars['current_operation_type'];  echo '\',
				order: \'';  echo $this->_vars['order'];  echo '\',
				orderDirection: \'';  echo $this->_vars['order_direction'];  echo '\',
				page: ';  echo $this->_vars['page'];  echo ',
				tIds: [\'pages_block_1\', \'pages_block_2\', \'sorter_block\'],
			});
		});
		'; ?>
</script>
	</div>
</div>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->module_path. "start". $this->module_templates.  $this->get_current_theme_gid('', '"start"'). "left_panel.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

<div class="clr"></div>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

