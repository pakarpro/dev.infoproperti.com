<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.helper.php'); $this->register_function("helper", "tpl_function_helper");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/block.capture.php'); $this->register_block("capture", "tpl_block_capture");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-05-12 11:45:00 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'right_block')); tpl_block_capture(array('assign' => 'right_block'), null, $this); ob_start(); ?>
	<?php if ($this->_vars['use_map_in_search']):  echo tpl_function_block(array('name' => show_default_map,'module' => geomap,'id_user' => $this->_vars['data']['id'],'gid' => 'user_search','markers' => $this->_vars['markers'],'settings' => $this->_vars['map_settings'],'width' => '300','height' => '300','map_id' => 'users_map_container'), $this); endif; ?>
	<?php echo tpl_function_helper(array('func_name' => show_banner_place,'module' => banners,'func_param' => 'right-banner'), $this); $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>

<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'content_block')); tpl_block_capture(array('assign' => 'content_block'), null, $this); ob_start(); ?>
<div class="rc">
	
	<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'main_block')); tpl_block_capture(array('assign' => 'main_block'), null, $this); ob_start(); ?>
	<div class="content-block">
		<?php if ($this->_vars['use_map']): ?><div id="map_mode_link"><?php echo tpl_function_helper(array('func_name' => show_map_view,'module' => geomap,'func_param' => $this->_vars['site_url'].'users/set_view_mode/map'), $this);?></div><?php endif; ?>
		
		<h1><?php echo l('header_users_result', 'users', '', 'text', array()); ?> - <span id="total_rows"><?php echo $this->_vars['page_data']['total_rows']; ?>
</span> <?php echo l('header_users_found', 'users', '', 'text', array()); ?></h1>
		
		<div class="tabs tab-size-15 hide noPrint">
			<ul id="search_users_sections">
				<?php if (is_array($this->_vars['user_types']) and count((array)$this->_vars['user_types'])): foreach ((array)$this->_vars['user_types'] as $this->_vars['tgid']): ?>
				<li id="m_<?php echo $this->_vars['tgid']; ?>
" sgid="<?php echo $this->_vars['tgid']; ?>
" class="<?php if ($this->_vars['current_user_type'] == $this->_vars['tgid']): ?>active<?php endif; ?>"><a href="<?php echo $this->_run_modifier($this->_vars['menu_action_link'], 'replace', 'plugin', 1, '[user_type]', $this->_vars['tgid']); ?>
"><?php echo l($this->_vars['tgid'], 'users', '', 'text', array()); ?></a></li>
				<?php endforeach; endif; ?>
			</ul>
		</div>
		
		<div id="users_block"><?php echo $this->_vars['block']; ?>
</div>
		<?php echo tpl_function_js(array('module' => users,'file' => 'users-list.js'), $this);?>
		<script><?php echo '
			$(function(){
				new usersList({
					siteUrl: \'';  echo $this->_vars['site_url'];  echo '\',
					listAjaxUrl: \''; ?>
users/ajax_users<?php echo '\',
					userType: \'';  echo $this->_vars['current_user_type'];  echo '\',
					sectionId: \'search_users_sections\',
					order: \'';  echo $this->_vars['order'];  echo '\',
					orderDirection: \'';  echo $this->_vars['order_direction'];  echo '\',
					page: ';  echo $this->_vars['page'];  echo ',
					tIds: [\'pages_block_1\', \'pages_block_2\', \'sorter_block\']
				});
			});
		'; ?>
</script>
	</div>
	<?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
	
	<?php if ($this->_run_modifier($this->_vars['right_block'], 'trim', 'PHP', 1)): ?>
	<div class="rc-1">
		<?php echo $this->_vars['main_block']; ?>

	</div>
	<div class="rc-2">
		<?php echo $this->_vars['right_block']; ?>

	</div>
	<?php else: ?>
		<?php echo $this->_vars['main_block']; ?>

	<?php endif; ?>
</div>

<div class="lc">
	<div class="inside account_menu">
		<?php echo tpl_function_block(array('name' => users_search_block,'module' => users), $this);?>
		<?php if ($this->_vars['use_poll_in_search']):  echo tpl_function_block(array('name' => show_poll_place_block,'module' => polls,'one_poll_place' => 0), $this); endif; ?>
		<?php echo tpl_function_helper(array('func_name' => show_banner_place,'module' => banners,'func_param' => 'big-left-banner'), $this);?>
		<?php echo tpl_function_helper(array('func_name' => show_banner_place,'module' => banners,'func_param' => 'left-banner'), $this);?>
	</div>
</div>

<div class="clr"></div>
<?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>

<?php if ($this->_run_modifier($this->_vars['right_block'], 'trim', 'PHP', 1)): ?>
<div class="rc_wrapper">
	<div class="panel">
		<div class="inside">
			<?php echo $this->_vars['content_block']; ?>

		</div>
	</div>
</div>
<?php else: ?>
	<?php echo $this->_vars['content_block']; ?>

<?php endif; ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
