<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-02-26 13:16:36 KRAT */ ?>

<div id="user_select_<?php echo $this->_vars['select_data']['rand']; ?>
" class="user-select">
	<span id="user_text_<?php echo $this->_vars['select_data']['rand']; ?>
">
	<?php if (is_array($this->_vars['select_data']['selected']) and count((array)$this->_vars['select_data']['selected'])): foreach ((array)$this->_vars['select_data']['selected'] as $this->_vars['item']): ?>
	<?php echo $this->_run_modifier($this->_vars['item']['output_name'], 'truncate', 'plugin', 1, 100); ?>
 <?php if ($this->_vars['select_data']['max_select'] != 1): ?><br><?php endif; ?><input type="hidden" name="<?php echo $this->_vars['select_data']['var_name'];  if ($this->_vars['select_data']['max_select'] != 1): ?>[]<?php endif; ?>" value="<?php echo $this->_vars['item']['id']; ?>
">
	<?php endforeach; endif; ?>
	</span>
	<a href="#" id="user_link_<?php echo $this->_vars['select_data']['rand']; ?>
"><?php echo l('link_manage_users', 'users', '', 'text', array()); ?></a><?php if ($this->_vars['select_data']['max_select'] > 1): ?> <i>(<?php echo l('max_user_select', 'users', '', 'text', array()); ?>: <?php echo $this->_vars['select_data']['max_select']; ?>
)</i><?php endif; ?><br>
	<div class="clr"></div>
</div>
<?php echo tpl_function_js(array('module' => users,'file' => 'users-select.js'), $this);?>
<script type='text/javascript'><?php echo '
$(function(){
	new usersSelect({
		siteUrl: \'';  echo $this->_vars['site_url'];  echo '\',	
		selected_items: [';  echo $this->_vars['select_data']['selected_str'];  echo '],
		max: \'';  echo $this->_vars['select_data']['max_select'];  echo '\',
		var_name: \'';  echo $this->_vars['select_data']['var_name'];  echo '\',
		template: \'';  echo $this->_vars['select_data']['template'];  echo '\',
		params: {';  if (is_array($this->_vars['select_data']['params']) and count((array)$this->_vars['select_data']['params'])): foreach ((array)$this->_vars['select_data']['params'] as $this->_vars['key'] => $this->_vars['item']):  echo $this->_vars['key']; ?>
:'<?php echo $this->_vars['item']; ?>
',<?php endforeach; endif;  echo '},
		rand: \'';  echo $this->_vars['select_data']['rand'];  echo '\',
		
	});
});
'; ?>
</script>
