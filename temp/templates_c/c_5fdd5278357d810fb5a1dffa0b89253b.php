<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.date_format.php'); $this->register_modifier("date_format", "tpl_modifier_date_format");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.truncate.php'); $this->register_modifier("truncate", "tpl_modifier_truncate");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.counter.php'); $this->register_function("counter", "tpl_function_counter");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2014-06-12 14:11:11 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<div class="actions">
	<?php if ($this->_vars['allow_add']): ?>
	<ul>
		<li><div class="l"><a href="<?php echo $this->_vars['site_url']; ?>
admin/upload_gallery/edit"><?php echo l('link_add_gallery', 'upload_gallery', '', 'text', array()); ?></a></div></li>
	</ul>
	<?php endif; ?>
	&nbsp;
</div>

<table cellspacing="0" cellpadding="0" class="data" width="100%">
<tr>
	<th class="first"><?php echo l('field_gid', 'upload_gallery', '', 'text', array()); ?></th>
	<th><?php echo l('field_name', 'upload_gallery', '', 'text', array()); ?></th>
	<th class="w100"><?php echo l('field_max_items', 'upload_gallery', '', 'text', array()); ?></th>
	<th class="w100"><?php echo l('field_use_moderation', 'upload_gallery', '', 'text', array()); ?></th>
	<th class="w150"><?php echo l('field_date_add', 'upload_gallery', '', 'text', array()); ?></th>
	<th class="w50">&nbsp;</th>
</tr>
<?php if (is_array($this->_vars['types']) and count((array)$this->_vars['types'])): foreach ((array)$this->_vars['types'] as $this->_vars['item']):  echo tpl_function_counter(array('print' => false,'assign' => counter), $this);?>
<tr<?php if (!($this->_vars['counter'] % 2)): ?> class="zebra"<?php endif; ?>>
	<td class="first center"><?php echo $this->_vars['item']['gid']; ?>
</td>
	<td class="center"><?php echo $this->_run_modifier($this->_vars['item']['name'], 'truncate', 'plugin', 1, 100); ?>
</td>
	<td class="center"><?php if ($this->_vars['item']['max_items_count'] == 0):  echo l('items_count_unlimit', 'upload_gallery', '', 'text', array());  else:  echo $this->_vars['item']['max_items_count'];  endif; ?></td>
	<td class="center"><?php if ($this->_vars['item']['use_moderation']):  echo l('moderation_on', 'upload_gallery', '', 'text', array());  else:  echo l('moderation_off', 'upload_gallery', '', 'text', array());  endif; ?></td>
	<td class="center"><?php echo $this->_run_modifier($this->_vars['item']['date_add'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']); ?>
</td>
	<td class="icons">
		<a href="<?php echo $this->_vars['site_url']; ?>
admin/upload_gallery/edit/<?php echo $this->_vars['item']['id']; ?>
"><img src="<?php echo $this->_vars['site_url'];  echo $this->_vars['img_folder']; ?>
icon-edit.png" width="16" height="16" border="0" alt="<?php echo l('link_edit_type', 'upload_gallery', '', 'button', array()); ?>" title="<?php echo l('link_edit_type', 'upload_gallery', '', 'button', array()); ?>"></a>
		<?php if ($this->_vars['allow_add']): ?>
		<a href="<?php echo $this->_vars['site_url']; ?>
admin/upload_gallery/delete/<?php echo $this->_vars['item']['id']; ?>
" onclick="javascript: if(!confirm('<?php echo l('note_delete_type', 'upload_gallery', '', 'js', array()); ?>')) return false;"><img src="<?php echo $this->_vars['site_url'];  echo $this->_vars['img_folder']; ?>
icon-delete.gif" width="16" height="16" border="0" alt="<?php echo l('link_delete_type', 'upload_gallery', '', 'button', array()); ?>" title="<?php echo l('link_delete_type', 'upload_gallery', '', 'button', array()); ?>"></a>
		<?php endif; ?>
	</td>
</tr>
<?php endforeach; else: ?>
<tr><td colspan="6" class="center"><?php echo l('no_upload_gallery', 'upload_gallery', '', 'text', array()); ?></td></tr>
<?php endif; ?>
</table>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "pagination.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
  $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
