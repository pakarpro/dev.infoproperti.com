<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.escape.php'); $this->register_modifier("escape", "tpl_modifier_escape");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-02-23 15:00:01 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->module_path. "start". $this->module_templates.  $this->get_current_theme_gid('', '"start"'). "left_panel.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

<div class="rc">
	<div class="content-block">

		<h1><?php echo l('header_banner_form', 'banners', '', 'text', array()); ?></h1>
		<div class="content-value">
			<form method="post" action="" name="save_form" enctype="multipart/form-data">
			<div class="edit_block">
				<div class="r">
					<div class="f"><?php echo l('field_name', 'banners', '', 'text', array()); ?>:&nbsp;* </div>
					<div class="v"><input type="text" value="<?php echo $this->_run_modifier($this->_vars['data']['name'], 'escape', 'plugin', 1); ?>
" name="name"></div>
				</div>
				<div class="r">
					<div class="f"><?php echo l('banner_place', 'banners', '', 'text', array()); ?>:&nbsp;* </div>
					<div class="v">
						<select id="banner_place" name="banner_place_id">
							<?php if (is_array($this->_vars['places']) and count((array)$this->_vars['places'])): foreach ((array)$this->_vars['places'] as $this->_vars['place']): ?>
							<option value="<?php echo $this->_vars['place']['id']; ?>
" <?php if ($this->_vars['place']['id'] == $this->_vars['data']['banner_place_id']): ?>selected<?php endif; ?>><?php echo $this->_vars['place']['name']; ?>
 (<?php echo $this->_vars['place']['width']; ?>
x<?php echo $this->_vars['place']['height']; ?>
)</option>
							<?php endforeach; endif; ?>
						</select>
					</div>
				</div>
				<div class="r">
					<div class="f"><?php echo l('field_link', 'banners', '', 'text', array()); ?>:&nbsp;* </div>
					<div class="v"><input type="text" value="<?php echo $this->_run_modifier($this->_vars['data']['link'], 'escape', 'plugin', 1); ?>
" name="link" class="long"></div>
				</div>
				<div class="r">
					<div class="f"><?php echo l('field_alt_text', 'banners', '', 'text', array()); ?>:&nbsp;* </div>
					<div class="v"><input type="text" value="<?php echo $this->_run_modifier($this->_vars['data']['alt_text'], 'escape', 'plugin', 1); ?>
" name="alt_text" class="long"></div>
				</div>
				<div class="r">
					<div class="f"><?php echo l('field_image', 'banners', '', 'text', array()); ?>:&nbsp;* </div>
					<div class="v">
						<input type="file" value="" name="banner_image_file">
					</div>
				</div>

			</div>
			<div class="b">
				<input type="submit" class='btn' value="<?php echo l('btn_send', 'start', '', 'button', array()); ?>" name="btn_save">
			</div>
			</form>
			<div class="b outside">
				<a href="<?php echo $this->_vars['site_url']; ?>
banners/my" class="btn-link"><ins class="with-icon i-larr"></ins><?php echo l('link_back_to_my_banners', 'banners', '', 'text', array()); ?></a>
			</div>
		</div>
	</div>
</div>
<div class="clr"></div>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
