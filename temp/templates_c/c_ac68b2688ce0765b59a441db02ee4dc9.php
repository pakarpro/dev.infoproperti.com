<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-05-12 13:55:36 KRAT */ ?>

<?php if ($this->_run_modifier($this->_vars['currencies'], 'count', 'PHP', 1) > 1): ?>
<li>
	<select onchange="javascript: load_currency(this.value);">
		<?php if (is_array($this->_vars['currencies']) and count((array)$this->_vars['currencies'])): foreach ((array)$this->_vars['currencies'] as $this->_vars['item']): ?><option value="<?php echo $this->_vars['item']['id']; ?>
" <?php if ($this->_vars['item']['is_default']): ?> selected<?php endif; ?>><?php echo $this->_vars['item']['gid']; ?>
</option><?php endforeach; endif; ?>
	</select>
	<script><?php echo '
		var currency_url = \'';  echo $this->_vars['site_url']; ?>
users/change_currency/<?php echo '\';
		function load_currency(value){
			location.href = currency_url + value;
		}
	'; ?>
</script>
</li>
<?php endif; ?>
