<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.block.php'); $this->register_function("block", "tpl_function_block");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.seolink.php'); $this->register_function("seolink", "tpl_function_seolink");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.date_format.php'); $this->register_modifier("date_format", "tpl_modifier_date_format");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.seotag.php'); $this->register_function("seotag", "tpl_function_seotag");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-05-12 13:22:11 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->module_path. "start". $this->module_templates.  $this->get_current_theme_gid('', '"start"'). "news_panel.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<!-- news detail view -->
<div class="rc">
	<div class="content-block">
		<h1><?php echo tpl_function_seotag(array('tag' => 'header_text'), $this);?></h1>
		<div class="news-view">
			<span class="date"><?php echo $this->_run_modifier($this->_vars['data']['date_add'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']); ?>
</span>
			<?php if ($this->_vars['data']['img']): ?>
			<img src="<?php echo $this->_vars['data']['media']['img']['thumbs']['big']; ?>
" align="left" />
			<div class="clr"></div>
			<?php endif; ?>
			<?php if (! $this->_vars['data']['content']):  echo $this->_vars['data']['annotation'];  else:  echo $this->_vars['data']['content'];  endif; ?>
			<?php if ($this->_vars['data']['video_content']['embed']): ?>
				<p><?php echo $this->_vars['data']['video_content']['embed']; ?>
</p>
			<?php endif; ?>

			<?php if ($this->_vars['data']['feed_link']):  echo l('feed_source', 'news', '', 'text', array()); ?>: <a href="<?php echo $this->_vars['data']['feed_link']; ?>
"><?php echo $this->_vars['data']['feed']['title']; ?>
</a><?php endif; ?>
			<div class="clr"></div>
			<br>
			<div class="a-link">
				<a href="<?php echo tpl_function_seolink(array('module' => 'news','method' => 'index'), $this);?>"><?php echo l('link_back_to_news', 'news', '', 'text', array()); ?></a>
			</div>
		</div>
	</div>

	<?php echo tpl_function_block(array('name' => show_social_networks_like,'module' => social_networking), $this);?>
	<?php echo tpl_function_block(array('name' => show_social_networks_share,'module' => social_networking), $this);?>
	<?php echo tpl_function_block(array('name' => show_social_networks_comments,'module' => social_networking), $this);?>
</div>
<div class="clr"></div>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
