<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-05-12 05:16:56 KRAT */ ?>

<?php if (! $this->_vars['geomap_js_loaded']): ?>
<script src="http://maps.google.com/maps/api/js?v=3.8&libraries=places<?php if ($this->_vars['map_reg_key']): ?>&key=<?php echo $this->_vars['map_reg_key'];  endif; ?>&sensor=true"></script>
<?php echo tpl_function_js(array('module' => geomap,'file' => 'googlemapsv3.js'), $this); endif; ?>
<script><?php echo '
var geocoder;
$(function(){
	geocoder = new GoogleMapsv3_Geocoder({});
});
'; ?>
</script>
