<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-02 15:28:17 KRAT */ ?>

		<?php $this->assign('field_gid', $this->_vars['field']['field']['gid']); ?>			
		<?php if ($this->_vars['field']['field']['type'] == 'select'): ?>
			<?php 
$this->assign('default_select_lang', l('select_default', 'start', '', 'text', array()));
 ?>
							<?php switch($this->_vars['field']['settings']['search_type']): case 'one':  ?>
											<?php switch($this->_vars['field']['settings']['view_type']): case 'select':  ?>
							<select name="data[<?php echo $this->_vars['field_gid']; ?>
]">
								<option value=""><?php echo l('select_default', 'start', '', 'text', array()); ?></option>
								<?php if (is_array($this->_vars['field']['field_content']['options']['option']) and count((array)$this->_vars['field']['field_content']['options']['option'])): foreach ((array)$this->_vars['field']['field_content']['options']['option'] as $this->_vars['key'] => $this->_vars['item']): ?>
								<option value="<?php echo $this->_run_modifier($this->_vars['key'], 'escape', 'plugin', 1); ?>
" <?php if ($this->_vars['key'] == $this->_vars['data'][$this->_vars['field_gid']]): ?>selected<?php endif; ?>><?php echo $this->_vars['item']; ?>
</option>
								<?php endforeach; endif; ?>
							</select>
						<?php break; case 'radio':  ?>
							<?php if (is_array($this->_vars['field']['field_content']['options']['option']) and count((array)$this->_vars['field']['field_content']['options']['option'])): foreach ((array)$this->_vars['field']['field_content']['options']['option'] as $this->_vars['key'] => $this->_vars['item']): ?>
							<input type="radio" name="data[<?php echo $this->_vars['field']['field']['gid']; ?>
]" value="<?php echo $this->_run_modifier($this->_vars['key'], 'escape', 'plugin', 1); ?>
" id="<?php echo $this->_vars['field']['field']['gid']; ?>
_select_<?php echo $this->_run_modifier($this->_vars['key'], 'escape', 'plugin', 1);  echo $this->_vars['form_rand']; ?>
" <?php if ($this->_run_modifier($this->_vars['data'][$this->_vars['field_name']], 'is_array', 'PHP', 1) && $this->_run_modifier($this->_vars['key'], 'in_array', 'PHP', 1, $this->_vars['data'][$this->_vars['field_name']])): ?>checked<?php endif; ?> /> <label for="<?php echo $this->_vars['field']['field']['gid']; ?>
_select_<?php echo $this->_run_modifier($this->_vars['key'], 'escape', 'plugin', 1);  echo $this->_vars['form_rand']; ?>
"><?php echo $this->_vars['item']; ?>
</label><br>
							<?php endforeach; endif; ?>
					<?php break; endswitch; ?>
				<?php break; case 'many':  ?>
											<?php switch($this->_vars['field']['settings']['view_type']): case 'select':  ?>
							<select name="data[<?php echo $this->_vars['field']['field']['gid']; ?>
]" multiple>
								<option value="">...</option>
								<?php if (is_array($this->_vars['field']['field_content']['options']['option']) and count((array)$this->_vars['field']['field_content']['options']['option'])): foreach ((array)$this->_vars['field']['field_content']['options']['option'] as $this->_vars['key'] => $this->_vars['item']): ?>
								<option value="<?php echo $this->_run_modifier($this->_vars['key'], 'escape', 'plugin', 1); ?>
" <?php if ($this->_vars['key'] == $this->_vars['data'][$this->_vars['field_name']]): ?>selected<?php endif; ?>><?php echo $this->_vars['item']; ?>
</option>
								<?php endforeach; endif; ?>
							</select>
						<?php break; case 'radio':  ?>
							<input type="hidden" name="data[<?php echo $this->_vars['field_gid']; ?>
][]" value="0">
							<?php if (is_array($this->_vars['field']['field_content']['options']['option']) and count((array)$this->_vars['field']['field_content']['options']['option'])): foreach ((array)$this->_vars['field']['field_content']['options']['option'] as $this->_vars['key'] => $this->_vars['item']): ?>
							<input type="checkbox" name="data[<?php echo $this->_vars['field_gid']; ?>
][]" value="<?php echo $this->_run_modifier($this->_vars['key'], 'escape', 'plugin', 1); ?>
" id="<?php echo $this->_vars['field_gid']; ?>
_select_<?php echo $this->_run_modifier($this->_vars['key'], 'escape', 'plugin', 1);  echo $this->_vars['form_rand']; ?>
" <?php if ($this->_run_modifier($this->_vars['data'][$this->_vars['field_gid']], 'is_array', 'PHP', 1) && $this->_run_modifier($this->_vars['key'], 'in_array', 'PHP', 1, $this->_vars['data'][$this->_vars['field_gid']])): ?>checked<?php endif; ?>> 
							<label for="<?php echo $this->_vars['field_gid']; ?>
_select_<?php echo $this->_run_modifier($this->_vars['key'], 'escape', 'plugin', 1);  echo $this->_vars['form_rand']; ?>
"><?php echo $this->_vars['item']; ?>
</label><br>
							<?php endforeach; endif; ?>
						<?php break; case 'slider':  ?>
							<link rel="stylesheet" href="<?php echo $this->_vars['site_root'];  echo $this->_vars['js_folder']; ?>
jquery-ui/jquery-ui.custom.css" type="text/css" />
							<link rel="stylesheet" href="<?php echo $this->_vars['site_root'];  echo $this->_vars['js_folder']; ?>
slider/css/ui.slider.extras.css" type="text/css" />
							<?php echo tpl_function_js(array('file' => 'slider/jquery.ui.slider-'.$this->_vars['_LANG']['rtl'].'.js'), $this);?>
							<?php echo tpl_function_js(array('file' => 'slider/selectToUISlider.jQuery.js'), $this);?>
							<script><?php echo '
								$(function(){				
									$('; ?>
'select#<?php echo $this->_vars['field_gid']; ?>
_min<?php echo $this->_vars['form_rand']; ?>
, select#<?php echo $this->_vars['field_gid']; ?>
_max<?php echo $this->_vars['form_rand']; ?>
'<?php echo ').selectToUISlider({
										labels : 2,
										tooltip: false,
										tooltipSrc : \'text\',
										labelSrc: \'text\',
										isRTL: ';  if ($this->_vars['_LANG']['rtl'] == 'rtl'): ?>true<?php else: ?>false<?php endif;  echo ',
									});
								});
							'; ?>
</script>
					
							<?php if ($this->_vars['field']['field_content']['settings_data_array']['empty_option']): ?>
								<?php $this->assign('min_value', 0); ?>
							<?php else: ?>
								<?php if (is_array($this->_vars['field']['field_content']['options']['option']) and count((array)$this->_vars['field']['field_content']['options']['option'])): foreach ((array)$this->_vars['field']['field_content']['options']['option'] as $this->_vars['key'] => $this->_vars['item']): ?>
									<?php $this->assign('min_value', $this->_vars['key']); ?>
									<?php break; ?>
								<?php endforeach; endif; ?>
							<?php endif; ?>
					
							<?php if (is_array($this->_vars['field']['field_content']['options']['option']) and count((array)$this->_vars['field']['field_content']['options']['option'])): foreach ((array)$this->_vars['field']['field_content']['options']['option'] as $this->_vars['key'] => $this->_vars['item']): ?>
								<?php $this->assign('max_value', $this->_vars['key']); ?>
							<?php endforeach; endif; ?>
					
							<?php if ($this->_vars['data'][$this->_vars['field_gid']]['range']['min']): ?>
								<?php $this->assign('current_min_value', $this->_vars['data'][$this->_vars['field_gid']]['range']['min']); ?>
							<?php else: ?>
								<?php $this->assign('current_min_value', $this->_vars['min_value']); ?>
							<?php endif; ?>
					
							<?php if ($this->_vars['data'][$this->_vars['field_gid']]['range']['max']): ?>
								<?php $this->assign('current_max_value', $this->_vars['data'][$this->_vars['field_gid']]['range']['max']); ?>
							<?php else: ?>
								<?php $this->assign('current_max_value', $this->_vars['max_value']); ?>
							<?php endif; ?>
					
							<div class="select-slider">
								<div class="r">
									<div class="f">
										Min
									</div>
									<div class="v">
										<select id="<?php echo $this->_vars['field_gid']; ?>
_min<?php echo $this->_vars['form_rand']; ?>
" name="data[<?php echo $this->_vars['field_gid']; ?>
][range][min]" class="">	
											<?php if ($this->_vars['field']['field_content']['settings_data_array']['empty_option']): ?><option value="0">0</option><?php endif; ?>
											<?php if (is_array($this->_vars['field']['field_content']['options']['option']) and count((array)$this->_vars['field']['field_content']['options']['option'])): foreach ((array)$this->_vars['field']['field_content']['options']['option'] as $this->_vars['key'] => $this->_vars['item']): ?>
											<option value="<?php echo $this->_run_modifier($this->_vars['key'], 'escape', 'plugin', 1); ?>
" <?php if ($this->_vars['key'] == $this->_vars['current_min_value']): ?>selected<?php endif; ?>><?php echo $this->_vars['item']; ?>
</option>
											<?php endforeach; endif; ?>
										</select>
									</div>
								</div>
								
								<div class="r btm-border">
									<div class="f">
										Max
									</div>
									<div class="v">
										<select id="<?php echo $this->_vars['field_gid']; ?>
_max<?php echo $this->_vars['form_rand']; ?>
" name="data[<?php echo $this->_vars['field_gid']; ?>
][range][max]" class="">
											<?php if ($this->_vars['field']['field_content']['settings_data']['empty_option']): ?><option value="0">0</option><?php endif; ?>
											<?php if (is_array($this->_vars['field']['field_content']['options']['option']) and count((array)$this->_vars['field']['field_content']['options']['option'])): foreach ((array)$this->_vars['field']['field_content']['options']['option'] as $this->_vars['key'] => $this->_vars['item']): ?>
											<option value="<?php echo $this->_run_modifier($this->_vars['key'], 'escape', 'plugin', 1); ?>
" <?php if ($this->_vars['key'] == $this->_vars['current_max_value']): ?>selected<?php endif; ?>><?php echo $this->_vars['item']; ?>
</option>
											<?php endforeach; endif; ?>
										</select>
									</div>
								</div>
								
								<?php /* disabled temporarily to remove text value
								<div class="vals">
									<div id="<?php echo $this->_vars['field_gid']; ?>
_min<?php echo $this->_vars['form_rand']; ?>
_selected_val" class="fleft"><?php if ($this->_vars['field']['field_content']['options']['option'][$this->_vars['current_min_value']]):  echo $this->_vars['field']['field_content']['options']['option'][$this->_vars['current_min_value']];  else: ?>0<?php endif; ?></div>
									<div id="<?php echo $this->_vars['field_gid']; ?>
_max<?php echo $this->_vars['form_rand']; ?>
_selected_val" class="fright"><?php if ($this->_vars['field']['field_content']['options']['option'][$this->_vars['current_max_value']]):  echo $this->_vars['field']['field_content']['options']['option'][$this->_vars['current_max_value']];  else: ?>0<?php endif; ?></div>
								</div>
								*/?>
								<div class="clr"></div>
							</div>
					
							<input type="hidden" name="field_editor_range[<?php echo $this->_vars['field_gid']; ?>
][min]" value="<?php echo $this->_run_modifier($this->_vars['min_value'], 'escape', 'plugin', 1); ?>
">
							<input type="hidden" name="field_editor_range[<?php echo $this->_vars['field_gid']; ?>
][max]" value="<?php echo $this->_run_modifier($this->_vars['max_value'], 'escape', 'plugin', 1); ?>
">
					<?php break; endswitch; ?>
			<?php break; endswitch; ?>
		<?php elseif ($this->_vars['field']['field']['type'] == 'multiselect'): ?>
			<?php 
$this->assign('default_select_lang', l('select_default', 'start', '', 'text', array()));
 ?>
			
			<?php if (is_array($this->_vars['field']['field_content']['options']['option']) and count((array)$this->_vars['field']['field_content']['options']['option'])): foreach ((array)$this->_vars['field']['field_content']['options']['option'] as $this->_vars['key'] => $this->_vars['item']): ?>
			<input type="checkbox" name="data[<?php echo $this->_vars['field_gid']; ?>
][]" value="<?php echo $this->_run_modifier($this->_vars['key'], 'escape', 'plugin', 1); ?>
" id="<?php echo $this->_vars['field_gid']; ?>
_select_<?php echo $this->_run_modifier($this->_vars['key'], 'escape', 'plugin', 1);  echo $this->_vars['form_rand']; ?>
" <?php if ($this->_run_modifier($this->_vars['data'][$this->_vars['field_gid']], 'is_array', 'PHP', 1) && $this->_run_modifier($this->_vars['key'], 'in_array', 'PHP', 1, $this->_vars['data'][$this->_vars['field_gid']])): ?>checked<?php endif; ?>> 
			<label for="<?php echo $this->_vars['field_gid']; ?>
_select_<?php echo $this->_run_modifier($this->_vars['key'], 'escape', 'plugin', 1);  echo $this->_vars['form_rand']; ?>
"><?php echo $this->_vars['item']; ?>
</label>
			<?php endforeach; endif; ?>		
		<?php elseif ($this->_vars['field']['field']['type'] == 'text'): ?>
			<?php if ($this->_vars['field']['settings']['search_type'] == 'number' && $this->_vars['field']['settings']['view_type'] == 'range'): ?>
				<input type="text" name="data[<?php echo $this->_vars['field_gid']; ?>
][range][min]" class="short" value="<?php echo $this->_run_modifier($this->_vars['data'][$this->_vars['field_gid']]['range']['min'], 'escape', 'plugin', 1); ?>
"> &nbsp;-&nbsp;
				<input type="text" name="data[<?php echo $this->_vars['field_gid']; ?>
][range][max]" class="short" value="<?php echo $this->_run_modifier($this->_vars['data'][$this->_vars['field_gid']]['range']['max'], 'escape', 'plugin', 1); ?>
">
			<?php elseif ($this->_vars['field']['settings']['search_type'] == 'number'): ?>
				<input type="text" name="data[<?php echo $this->_vars['field_gid']; ?>
]" value="<?php echo $this->_run_modifier($this->_vars['data'][$this->_vars['field_gid']], 'escape', 'plugin', 1); ?>
">
			<?php else: ?>
				<input type="text" name="data[<?php echo $this->_vars['field_gid']; ?>
]" value="<?php echo $this->_run_modifier($this->_vars['data'][$this->_vars['field_gid']], 'escape', 'plugin', 1); ?>
">
			<?php endif; ?>
		<?php elseif ($this->_vars['field']['field']['type'] == 'textarea'): ?>
			<input type="text" name="data[<?php echo $this->_vars['field_gid']; ?>
]" value="<?php echo $this->_run_modifier($this->_vars['data'][$this->_vars['field_gid']], 'escape', 'plugin', 1); ?>
">
		<?php elseif ($this->_vars['field']['field']['type'] == 'checkbox'): ?>	
			<input type="checkbox" name="data[<?php echo $this->_vars['field_gid']; ?>
]" value="1" <?php if ($this->_vars['data'][$this->_vars['field_gid']]): ?>checked<?php endif; ?>>
		<?php endif; ?>	

