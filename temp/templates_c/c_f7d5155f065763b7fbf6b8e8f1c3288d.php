<?php require_once('/opt/ip/system/libraries/template_lite/plugins/function.block.php'); $this->register_function("block", "tpl_function_block");  require_once('/opt/ip/system/libraries/template_lite/plugins/modifier.date_format.php'); $this->register_modifier("date_format", "tpl_modifier_date_format");  require_once('/opt/ip/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-01-04 16:45:51 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
  $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->module_path. "start". $this->module_templates.  $this->get_current_theme_gid('', '"start"'). "left_panel.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<div class="rc">
	<div class="content-block">
	<h1><?php echo l('header_my_payments_statistic', 'payments', '', 'text', array()); ?></h1>

	
	<table class="list">
	<tr>
		<th class="w30">ID</th>
		<th class="w150"><?php echo l('field_date_add', 'payments', '', 'text', array()); ?></th>
		<th><?php echo l('field_amount', 'payments', '', 'text', array()); ?></th>
		<th><?php echo l('field_payment_type', 'payments', '', 'text', array()); ?></th>
		<th><?php echo l('field_billing_type', 'payments', '', 'text', array()); ?></th>
		<th><?php echo l('field_status', 'payments', '', 'text', array()); ?></th>
	</tr>
	<?php if (is_array($this->_vars['payments']) and count((array)$this->_vars['payments'])): foreach ((array)$this->_vars['payments'] as $this->_vars['key'] => $this->_vars['item']): ?>
	<tr>
		<td><?php echo $this->_vars['item']['id']; ?>
</td>
		<td><?php echo $this->_run_modifier($this->_vars['item']['date_add'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']); ?>
</td>
		<td><?php echo tpl_function_block(array('name' => currency_format_output,'module' => start,'value' => $this->_vars['item']['amount'],'cur_gid' => $this->_vars['item']['currency_gid'],'use_gid' => '1'), $this);?></td>
		<td><?php echo l('payment_type_name_'.$this->_vars['item']['payment_type_gid'], 'payments', '', 'text', array()); ?> (<?php echo $this->_vars['item']['payment_data']['name']; ?>
)</td>
		<td><?php echo $this->_vars['item']['system_gid']; ?>
</td>
		<td>
			<?php if ($this->_vars['item']['status'] == '1'): ?>
				<font class="success"><?php echo l('payment_status_approved', 'payments', '', 'text', array()); ?></font>
			<?php elseif ($this->_vars['item']['status'] == '-1'): ?>
				<font class="error"><?php echo l('payment_status_declined', 'payments', '', 'text', array()); ?></font>
			<?php else: ?>
				<?php echo l('payment_status_wait', 'payments', '', 'text', array()); ?>
			<?php endif; ?>
		</td>
	</tr>
	<?php endforeach; else: ?>
	<tr>
		<td class="center" colspan="6"><?php echo l('no_payments', 'payments', '', 'text', array()); ?></td>
	</tr>
	<?php endif; ?>
	</table>
	<div id="pages_block_2"><?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "pagination.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?></div>

	</div>
</div>
<div class="clr"></div>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
