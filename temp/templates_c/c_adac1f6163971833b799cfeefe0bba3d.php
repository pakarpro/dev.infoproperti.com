<?php require_once('/opt/ip/system/libraries/template_lite/plugins/function.js.php'); $this->register_function("js", "tpl_function_js");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-12-01 14:40:51 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array('load_type' => 'ui'));
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
  $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->module_path. "start". $this->module_templates.  $this->get_current_theme_gid('', '"start"'). "left_panel.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<div class="rc">
	<div class="content-block">
		<h1><?php echo l('text_profile_visitors', 'users', '', 'text', array()); ?> (<?php echo $this->_vars['users_count']; ?>
)</h1>
		<div id="users_block"><?php echo $this->_vars['block']; ?>
</div>
		<?php echo tpl_function_js(array('module' => users,'file' => 'users-list.js'), $this);?>
		<script><?php echo '
		$(function(){
			new usersList({
				siteUrl: \'';  echo $this->_vars['site_url'];  echo '\',
				listAjaxUrl: \'users/ajax_visitors\',
				order: \'';  echo $this->_vars['order'];  echo '\',
				orderDirection: \'';  echo $this->_vars['order_direction'];  echo '\',
				page: ';  echo $this->_vars['page'];  echo ',
				tIds: [\'pages_block_1\', \'pages_block_2\', \'sorter_block\']
			});
		});
		'; ?>
</script>
	</div>
</div>
<div class="clr"></div>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
