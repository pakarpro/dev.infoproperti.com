<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-02-25 10:15:23 KRAT */ ?>

<p class="header-comment">
<?php echo l("text_register_as", 'users', '', 'text', array()); ?>: <b><?php echo $this->_vars['data']['user_type_str']; ?>
</b><br>
<?php echo l("field_date_created", 'users', '', 'text', array()); ?>: <b><?php echo $this->_run_modifier($this->_vars['data']['date_created'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']); ?>
</b>
</p>

<h2 class="line top bottom linked">
	<?php echo l('table_header_personal', 'users', '', 'text', array()); ?>
	<a class="btn-link fright" href="<?php echo $this->_vars['site_url']; ?>
users/profile/personal/"><ins class="with-icon i-edit"></ins></a>
</h2>
<div class="view-section">
	<div class="r">
		<div class="f"><?php echo l('field_fname', 'users', '', 'text', array()); ?>:&nbsp;*</div>
		<div class="v"><?php echo $this->_vars['data']['fname']; ?>
</div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_sname', 'users', '', 'text', array()); ?>:&nbsp;*</div>
		<div class="v"><?php echo $this->_vars['data']['sname']; ?>
</div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_phone', 'users', '', 'text', array()); ?>:&nbsp;*</div>
		<div class="v"><?php if ($this->_vars['data']['phone']):  echo $this->_vars['data']['phone'];  else:  echo l('no_information', 'users', '', 'text', array());  endif; ?></div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_icon', 'users', '', 'text', array()); ?>:</div>
		<div class="v">
		<?php if ($this->_vars['data']['user_logo_moderation']): ?>
		<img src="<?php echo $this->_vars['data']['media']['user_logo_moderation']['thumbs']['middle']; ?>
" alt="<?php echo $this->_run_modifier($this->_run_modifier($this->_vars['data']['output_name'], 'truncate', 'plugin', 1, 30), 'escape', 'plugin', 1); ?>
">
		<?php else: ?>
		<img src="<?php echo $this->_vars['data']['media']['user_logo']['thumbs']['middle']; ?>
" alt="<?php echo $this->_run_modifier($this->_vars['data']['output_name'], 'escape', 'plugin', 1); ?>
">
		<?php endif; ?>
		</div>
	</div>
</div>

<h2 class="line top bottom linked">
	<?php echo l('table_header_company', 'users', '', 'text', array()); ?>
	<a class="btn-link fright" href="<?php echo $this->_vars['site_url']; ?>
users/profile/company/"><ins class="with-icon i-edit"></ins></a>
</h2>
<div class="view-section">
	<?php if ($this->_vars['data']['agent_company']): ?>
	<div class="r">
		<div class="f"><?php echo l('field_company', 'users', '', 'text', array()); ?>:</div>
		<div class="v"><?php echo $this->_vars['data']['company']['output_name']; ?>
</div>
	</div>
		<?php if (! $this->_vars['data']['agent_status']): ?>
		<div class="r">
			<div class="f"><?php echo l('field_company_request_status', 'users', '', 'text', array()); ?>:</div>
			<div class="v"><?php echo l('awaiting_aproval_since', 'users', '', 'text', array()); ?> <?php echo $this->_run_modifier($this->_vars['data']['agent_date'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']); ?>
</div>
		</div>
		<?php else: ?>
		<?php if ($this->_vars['data']['company']['contact_email']): ?>
		<div class="r">
			<div class="f"><?php echo l('field_contact_email', 'users', '', 'text', array()); ?>:</div>
			<div class="v"><?php echo $this->_vars['data']['company']['contact_email']; ?>
</div>
		</div>
		<?php endif; ?>
		<?php if ($this->_vars['data']['company']['contact_phone']): ?>
		<div class="r">
			<div class="f"><?php echo l('field_contact_phone', 'users', '', 'text', array()); ?>:</div>
			<div class="v"><?php echo $this->_vars['data']['company']['contact_phone']; ?>
</div>
		</div>
		<?php endif; ?>
		<?php if ($this->_vars['data']['company']['contact_info']): ?>
		<div class="r">
			<div class="f"><?php echo l('field_contact_info', 'users', '', 'text', array()); ?>:</div>
			<div class="v"><?php echo $this->_run_modifier($this->_vars['data']['company']['contact_info'], 'nl2br', 'PHP', 1); ?>
</div>
		</div>
		<?php endif; ?>
		<?php if ($this->_vars['data']['company']['web_url']): ?>
		<div class="r">
			<div class="f"><?php echo l('field_web_url', 'users', '', 'text', array()); ?>:</div>
			<div class="v"><?php echo $this->_vars['data']['company']['web_url']; ?>
</div>
		</div>
		<?php endif; ?>
		<?php if ($this->_vars['data']['company']['facebook']): ?>
		<div class="r">
			<div class="f"><?php echo l('field_facebook', 'users', '', 'text', array()); ?>:</div>
			<div class="v"><?php echo $this->_vars['data']['company']['facebook']; ?>
</div>
		</div>
		<?php endif; ?>
		<?php if ($this->_vars['data']['company']['twitter']): ?>
		<div class="r">
			<div class="f"><?php echo l('field_twitter', 'users', '', 'text', array()); ?>:</div>
			<div class="v"><?php echo $this->_vars['data']['company']['twitter']; ?>
</div>
		</div>
		<?php endif; ?>
		<?php if ($this->_vars['data']['company']['vkontakte']): ?>
		<?php /* disable for now
		<div class="r">
			<div class="f"><?php echo l('field_vkontakte', 'users', '', 'text', array()); ?>:</div>
			<div class="v"><?php echo $this->_vars['data']['company']['vkontakte']; ?>
</div>
		</div>
		*/?>
		<?php endif; ?>
		<?php endif; ?>
	<?php else: ?>
	<div class="r">
		<div class="f"><?php echo l('field_company', 'users', '', 'text', array()); ?>:</div>
		<div class="v"><?php echo l('no_information', 'users', '', 'text', array()); ?></div>
	</div>
	<?php endif; ?>

</div>

<h2 class="line top bottom linked">
	<?php echo l('table_header_contact', 'users', '', 'text', array()); ?>
	<a class="btn-link fright" href="<?php echo $this->_vars['site_url']; ?>
users/profile/contact/"><ins class="with-icon i-edit"></ins></a>
</h2>
<div class="view-section">
	<div class="r">
		<div class="f"><?php echo l('field_contact_email', 'users', '', 'text', array()); ?>:</div>
		<div class="v"><?php if ($this->_vars['data']['contact_email']):  echo $this->_vars['data']['contact_email'];  else:  echo l('no_information', 'users', '', 'text', array());  endif; ?></div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_contact_phone', 'users', '', 'text', array()); ?>:</div>
		<div class="v"><?php if ($this->_vars['data']['contact_phone']):  echo $this->_vars['data']['contact_phone'];  else:  echo l('no_information', 'users', '', 'text', array());  endif; ?></div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_contact_info', 'users', '', 'text', array()); ?>:</div>
		<div class="v"><?php if ($this->_vars['data']['contact_info']):  echo $this->_run_modifier($this->_vars['data']['contact_info'], 'nl2br', 'PHP', 1);  else:  echo l('no_information', 'users', '', 'text', array());  endif; ?></div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_facebook', 'users', '', 'text', array()); ?>:</div>
		<div class="v"><?php if ($this->_vars['data']['facebook']):  echo $this->_vars['data']['facebook'];  else:  echo l('no_information', 'users', '', 'text', array());  endif; ?></div>
	</div>
	<div class="r">
		<div class="f"><?php echo l('field_twitter', 'users', '', 'text', array()); ?>:</div>
		<div class="v"><?php if ($this->_vars['data']['twitter']):  echo $this->_vars['data']['twitter'];  else:  echo l('no_information', 'users', '', 'text', array());  endif; ?></div>
	</div>
	<?php /* disable for now
	<div class="r">
		<div class="f"><?php echo l('field_vkontakte', 'users', '', 'text', array()); ?>:</div>
		<div class="v"><?php if ($this->_vars['data']['vkontakte']):  echo $this->_vars['data']['vkontakte'];  else:  echo l('no_information', 'users', '', 'text', array());  endif; ?></div>
	</div>
	*/ ?>
</div>

<?php echo tpl_function_helper(array('func_name' => get_user_subscriptions_list,'module' => subscriptions), $this);?>
