<?php require_once('/opt/ip/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-02-27 10:21:56 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->module_path. "start". $this->module_templates.  $this->get_current_theme_gid('', '"start"'). "left_panel.tpl", array('hide_poll' => "true"));
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<div class="rc">
	<div class="content-block">
		<h1><?php echo l('polls_results', 'polls', '', 'text', array()); ?></h1>
		<?php if (is_array($this->_vars['polls']) and count((array)$this->_vars['polls'])): foreach ((array)$this->_vars['polls'] as $this->_vars['key'] => $this->_vars['item']): ?>
			<div id="poll_<?php echo $this->_vars['key']; ?>
" class="poll_question_link">
				<h2>
					<?php if ($this->_vars['language']): ?>
						<?php if ($this->_vars['item']['question'][$this->_vars['language']]): ?>
							<?php echo $this->_vars['item']['question'][$this->_vars['language']]; ?>

						<?php else: ?>
							<?php echo $this->_vars['item']['question']['default']; ?>

						<?php endif; ?>
					<?php else: ?>
						<?php if ($this->_vars['item']['question'][$this->_vars['cur_lang']]): ?>
							<?php echo $this->_vars['item']['question'][$this->_vars['cur_lang']]; ?>

						<?php else: ?>
							<?php echo $this->_vars['item']['question']['default']; ?>

						<?php endif; ?>
					<?php endif; ?>
					<div class="fright">
					<a class="btn-link"><ins class="with-icon i-toggle down"></ins></a>
					</div>
				</h2>

			</div>
			<div class="poll_results_content"></div>
		<?php endforeach; endif; ?>
		<?php echo '
			<script type="text/javascript">
				$(function() {
					if (\'undefined\' != typeof(PollsList)) {
						new PollsList({
							siteUrl: \'';  echo $this->_vars['site_url'];  echo '\'
						});
					}
				});
			</script>
		'; ?>

	</div>
</div>
<div class="clr"></div>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
