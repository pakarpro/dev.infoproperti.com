<?php require_once('/opt/ip/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-02-29 16:30:13 WIB */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<!-- MOD -->
<script type='text/javascript' src='http://pakarpro.com/application/js/jquery-ui.custom.min.js'></script>
<script type='text/javascript' src='http://pakarpro.com/application/js/jquery.jeditable.mini.js'></script>
<LINK href='http://pakarpro.com/application/js/jquery-ui/jquery-ui.custom.css' rel='stylesheet' type='text/css'>


<!-- end of mod -->
<form method="post" action="<?php echo $this->_vars['data']['action']; ?>
" name="save_form" enctype="multipart/form-data">
	<div class="edit-form n150">
		<div class="row header"><?php if ($this->_vars['data']['id']):  echo l('admin_header_news_change', 'news', '', 'text', array());  else:  echo l('admin_header_news_add', 'news', '', 'text', array());  endif; ?></div>
		<div class="row">
			<div class="h"><?php echo l('field_name', 'news', '', 'text', array()); ?>:&nbsp;* </div>
			<div class="v"><input type="text" value="<?php echo $this->_vars['data']['name']; ?>
" name="name" class="long"></div>
		</div>
		<div class="row">
			<div class="h"><?php echo l('field_gid', 'news', '', 'text', array()); ?>:&nbsp;* </div>
			<div class="v"><input type="text" value="<?php echo $this->_vars['data']['gid']; ?>
" name="gid" class="long"></div>
		</div>
        <!-- mod for tags -->
		<div class="row">
			<div class="h"><?php echo l('field_news_tags', 'news', '', 'text', array()); ?>:&nbsp; </div>
			<div class="v"><input type="text" value="" name="tags" class="long"></div>
		</div>
		<div class="row">
			<div class="h"></div>
			<div class="v">
            	<?php if (is_array($this->_vars['existing_tag']) and count((array)$this->_vars['existing_tag'])): foreach ((array)$this->_vars['existing_tag'] as $this->_vars['item']): ?>
                <div style="min-height:10px; display:inline-block; position:relative; margin-right:5px; background-color:white; border-bottom:1px solid black;">
                	<div style="position:relative; float:left; margin-right:5px;">
                		<?php echo $this->_vars['item']['tag_name']; ?>
                    
                    </div>
                    <a href="<?php echo $this->_vars['site_url']; ?>
admin/news/delete_news_tag/<?php echo $this->_vars['item']['news_id']; ?>
/<?php echo $this->_vars['item']['tag_id']; ?>
">
                        <div style="position:relative; float:right; text-align:right;">
                                X
                        </div>
                    </a>                                        
                </div>
                <?php endforeach; endif; ?>
            </div>
		</div>        
        <!-- end of mod -->        
		<div class="row">
			<div class="h"><?php echo l('field_news_lang', 'news', '', 'text', array()); ?>: </div>
			<div class="v"><select name="id_lang"><?php if (is_array($this->_vars['languages']) and count((array)$this->_vars['languages'])): foreach ((array)$this->_vars['languages'] as $this->_vars['item']): ?><option value="<?php echo $this->_vars['item']['id']; ?>
"<?php if ($this->_vars['item']['id'] == $this->_vars['data']['id_lang']): ?> selected<?php endif; ?>><?php echo $this->_vars['item']['name']; ?>
</option><?php endforeach; endif; ?></select></div>
		</div>
		<div class="row">
			<div class="h"><?php echo l('field_icon', 'news', '', 'text', array()); ?>: </div>
			<div class="v">
				<input type="file" name="news_icon">
				<?php if ($this->_vars['data']['img']): ?>
				<br><img src="<?php echo $this->_vars['data']['media']['img']['thumbs']['small']; ?>
"  hspace="2" vspace="2" />
				<br><input type="checkbox" name="news_icon_delete" value="1" id="uichb"><label for="uichb"><?php echo l('field_icon_delete', 'news', '', 'text', array()); ?></label>
				<?php endif; ?>
			</div>
		</div>
		<div class="row">
			<div class="h"><?php echo l('field_video', 'news', '', 'text', array()); ?>: </div>
			<div class="v">
				<input type="file" name="news_video">
				<?php if ($this->_vars['data']['video']): ?>
					<br><?php echo l('field_video_status', 'news', '', 'text', array()); ?>: 
					<?php if ($this->_vars['data']['video_data']['status'] == 'end' && $this->_vars['data']['video_data']['errors']): ?>
						<font color="red"><?php if (is_array($this->_vars['data']['video_data']['errors']) and count((array)$this->_vars['data']['video_data']['errors'])): foreach ((array)$this->_vars['data']['video_data']['errors'] as $this->_vars['item']):  echo $this->_vars['item']; ?>
<br><?php endforeach; endif; ?></font>
					<?php elseif ($this->_vars['data']['video_data']['status'] == 'end'): ?>	<font color="green"><?php echo l('field_video_status_end', 'news', '', 'text', array()); ?></font><br>
					<?php elseif ($this->_vars['data']['video_data']['status'] == 'images'): ?>	<font color="yellow"><?php echo l('field_video_status_images', 'news', '', 'text', array()); ?></font><br>
					<?php elseif ($this->_vars['data']['video_data']['status'] == 'waiting'): ?> <font color="yellow"><?php echo l('field_video_status_waiting', 'news', '', 'text', array()); ?></font><br>
					<?php elseif ($this->_vars['data']['video_data']['status'] == 'start'): ?> <font color="yellow"><?php echo l('field_video_status_start', 'news', '', 'text', array()); ?></font><br>
					<?php endif; ?>
					<?php if ($this->_vars['data']['video_content']['thumbs']['small']): ?>
					<br><img src="<?php echo $this->_vars['data']['video_content']['thumbs']['small']; ?>
"  hspace="2" vspace="2" />
					<?php endif; ?>
					<br><input type="checkbox" name="news_video_delete" value="1" id="uvchb"><label for="uvchb"><?php echo l('field_video_delete', 'news', '', 'text', array()); ?></label>
				<?php endif; ?>
			</div>
		</div>
		<div class="row">
			<div class="h"><?php echo l('field_annotation', 'news', '', 'text', array()); ?>: </div>
			<div class="v"><textarea name="annotation"><?php echo $this->_vars['data']['annotation']; ?>
</textarea><br><i><?php echo l('field_annotation_text', 'news', '', 'text', array()); ?></i></div>
		</div>
		<div class="row">
			<div class="h"><?php echo l('field_content', 'news', '', 'text', array()); ?>: </div>
			<div class="v"><?php echo $this->_vars['data']['content_fck']; ?>
&nbsp;</div>
		</div>
		<!--MOD FOR SCHEDULING-->
		<div class="row">
			<div class="h">Scheduling</div>
			<div class="v">
            	<input type="checkbox" name="scheduling_flag" <?php if ($this->_vars['data']['scheduling'] && $this->_vars['data']['scheduling'] != '0000-00-00'): ?>checked='checked'<?php endif; ?> value="1"><label>Use scheduling</label><br />
				<input type="text" name="scheduling" id="scheduling" <?php if ($this->_vars['data']['scheduling'] != '0000-00-00'): ?>value="<?php echo $this->_vars['data']['scheduling']; ?>
"<?php endif; ?> class="middle" /> 
				<input type="hidden" name="scheduling_hidden" <?php if ($this->_vars['data']['scheduling'] != '0000-00-00'): ?>value="<?php echo $this->_vars['data']['scheduling']; ?>
"<?php endif; ?> id="scheduling_hidden">
                <select name="scheduling_time">
                	<option value="<?php echo $this->_vars['current_time']; ?>
"><?php echo $this->_vars['current_time']; ?>
</option>
                	<option value="00:00:00">00:00:00</option>
                    <option value="00:30:00">00:30:00</option>
                    <option value="01:00:00">01:00:00</option>
                    <option value="01:30:00">01:30:00</option>
                	<option value="02:00:00">02:00:00</option>
                    <option value="02:30:00">02:30:00</option>
                    <option value="03:00:00">03:00:00</option>
                    <option value="03:30:00">03:30:00</option>
                	<option value="04:00:00">04:00:00</option>
                    <option value="04:30:00">04:30:00</option>
                    <option value="05:00:00">05:00:00</option>
                    <option value="05:30:00">05:30:00</option>
                	<option value="06:00:00">06:00:00</option>
                    <option value="06:30:00">06:30:00</option>
                    <option value="07:00:00">07:00:00</option>
                    <option value="07:30:00">07:30:00</option>
                	<option value="08:00:00">08:00:00</option>
                    <option value="08:30:00">08:30:00</option>
                    <option value="09:00:00">09:00:00</option>
                    <option value="09:30:00">09:30:00</option>
                	<option value="10:00:00">10:00:00</option>
                    <option value="10:30:00">10:30:00</option>
                    <option value="11:00:00">11:00:00</option>
                    <option value="11:30:00">11:30:00</option>          
                	<option value="12:00:00">12:00:00</option>
                    <option value="12:30:00">12:30:00</option>
                    <option value="13:00:00">13:00:00</option>
                    <option value="13:30:00">13:30:00</option>
                	<option value="14:00:00">14:00:00</option>
                    <option value="14:30:00">14:30:00</option>
                    <option value="15:00:00">15:00:00</option>
                    <option value="15:30:00">15:30:00</option>
                	<option value="16:00:00">16:00:00</option>
                    <option value="16:30:00">16:30:00</option>
                    <option value="17:00:00">17:00:00</option>
                    <option value="17:30:00">17:30:00</option>
                	<option value="18:00:00">18:00:00</option>
                    <option value="18:30:00">18:30:00</option>
                    <option value="19:00:00">19:00:00</option>
                    <option value="19:30:00">19:30:00</option>
                	<option value="20:00:00">20:00:00</option>
                    <option value="20:30:00">20:30:00</option>
                    <option value="21:00:00">21:00:00</option>
                    <option value="21:30:00">21:30:00</option>
                	<option value="22:00:00">22:00:00</option>
                    <option value="22:30:00">22:30:00</option>
                    <option value="23:00:00">23:00:00</option>
                    <option value="23:30:00">23:30:00</option>                                                  
                </select>
                
				<script><?php echo '
					$(function(){
						$(\'#scheduling\').datepicker({dateFormat: \'';  echo $this->_vars['page_data']['datepicker_date_format'];  echo '\', altFormat: \'';  echo $this->_vars['page_data']['datepicker_alt_format'];  echo '\', altField: \'#scheduling_hidden\', showOn: \'both\'});
					});
				'; ?>
</script>
			</div>
		</div>
        <!--END OF SCHEDULING MOD-->        
	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div></div>
	<a class="cancel" href="<?php echo $this->_vars['site_url']; ?>
admin/news"><?php echo l('btn_cancel', 'start', '', 'text', array()); ?></a>
</form>
<div class="clr"></div>
<script><?php echo '
$(function(){
	$("div.row:odd").addClass("zebra");
});
'; ?>
</script>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
