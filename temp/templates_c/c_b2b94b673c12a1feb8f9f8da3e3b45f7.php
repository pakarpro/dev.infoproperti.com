<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-02-25 10:21:33 KRAT */ ?>

	<div class="photo-info-area">
		<div class="photo-area">
			<img src="<?php echo $this->_vars['photo']['media']['thumbs']['200_200']; ?>
" hspace="3" id="listing_photo_<?php echo $this->_vars['photo']['id']; ?>
" />
			<div class="photo-info">
				<div class="panel">
					<?php if ($this->_vars['photo']['status']): ?>
					<font class="stat-active"><?php echo l('photo_active', 'listings', '', 'text', array()); ?></font>
					<?php else: ?>
					<font class="stat-moder"><?php echo l('photo_moderate', 'listings', '', 'text', array()); ?></font>
					<?php endif; ?>
					<?php if ($this->_vars['photo']['comment']): ?><br><?php echo $this->_run_modifier($this->_vars['photo']['comment'], 'truncate', 'plugin', 1, 50, '...', true);  endif; ?>
				</div>
				<div class="background"></div>
			</div>
		</div>
		<div class="action" onclick="javascript: gUpload.open_edit_form(<?php echo $this->_vars['photo']['id']; ?>
);">
			<div class="btn-link"><ins class="with-icon w i-edit no-hover"></ins></div>
			<div class="background"></div>
		</div>
		<div class="canvas hide"></div>
		<div class="clr"></div>
	</div>
