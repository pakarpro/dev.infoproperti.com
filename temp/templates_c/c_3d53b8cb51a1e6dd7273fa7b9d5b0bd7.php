<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.js.php'); $this->register_function("js", "tpl_function_js");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.date_format.php'); $this->register_modifier("date_format", "tpl_modifier_date_format");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.truncate.php'); $this->register_modifier("truncate", "tpl_modifier_truncate");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/block.capture.php'); $this->register_block("capture", "tpl_block_capture");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.counter.php'); $this->register_function("counter", "tpl_function_counter");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.lower.php'); $this->register_modifier("lower", "tpl_modifier_lower");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.user_select.php'); $this->register_function("user_select", "tpl_function_user_select");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.country_select.php'); $this->register_function("country_select", "tpl_function_country_select");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.block.php'); $this->register_function("block", "tpl_function_block");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.escape.php'); $this->register_modifier("escape", "tpl_modifier_escape");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.helper.php'); $this->register_function("helper", "tpl_function_helper");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-05-08 09:57:14 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
  echo tpl_function_helper(array('func_name' => get_admin_level1_menu,'helper_name' => menu,'func_param' => 'admin_listings_menu'), $this);?>
<div class="actions">
	<ul>
		<?php if (! $this->_vars['is_operation_types_disabled']): ?><li><div class="l"><a href="<?php echo $this->_vars['site_url']; ?>
admin/listings/edit"><?php echo l('link_add_listing', 'listings', '', 'text', array()); ?></a></div></li><?php endif; ?>
	</ul>
	&nbsp;
</div>

<div class="menu-level3">
	<ul>
		<li class="<?php if ($this->_vars['filter'] == 'all'): ?>active<?php endif;  if (! $this->_vars['filter_data']['all']): ?> hide<?php endif; ?>"><a href="<?php echo $this->_vars['site_url']; ?>
admin/listings/all"><?php echo l('filter_all_listings', 'listings', '', 'text', array()); ?> (<?php echo $this->_vars['filter_data']['all']; ?>
)</a></li>
		<li class="<?php if ($this->_vars['filter'] == 'not_active'): ?>active<?php endif;  if (! $this->_vars['filter_data']['not_active']): ?> hide<?php endif; ?>"><a href="<?php echo $this->_vars['site_url']; ?>
admin/listings/not_active"><?php echo l('filter_not_active_listings', 'listings', '', 'text', array()); ?> (<?php echo $this->_vars['filter_data']['not_active']; ?>
)</a></li>
		<li class="<?php if ($this->_vars['filter'] == 'active'): ?>active<?php endif;  if (! $this->_vars['filter_data']['active']): ?> hide<?php endif; ?>"><a href="<?php echo $this->_vars['site_url']; ?>
admin/listings/active"><?php echo l('filter_active_listings', 'listings', '', 'text', array()); ?> (<?php echo $this->_vars['filter_data']['active']; ?>
)</a></li>
	</ul>
	&nbsp;
</div>
	
<form method="post" action="" name="save_form" enctype="multipart/form-data">
<div class="filter-form">
	<div class="form">
			<div class="row">
				<div class="h"><?php echo l('field_listing_type', 'listings', '', 'text', array()); ?>:</div>
				<div class="v">
					<select name="type" id="type" class="long">
						<option value="">...</option>
						<?php if (is_array($this->_vars['operation_types']) and count((array)$this->_vars['operation_types'])): foreach ((array)$this->_vars['operation_types'] as $this->_vars['item']): ?>
						<option value="<?php echo $this->_run_modifier($this->_vars['item'], 'escape', 'plugin', 1); ?>
" <?php if ($this->_vars['item'] == $this->_vars['page_data']['filter']['type']): ?>selected<?php endif; ?>><?php echo l('operation_search_'.$this->_vars['item'], 'listings', '', 'text', array()); ?></option>
						<?php endforeach; endif; ?>
					</select>
				</div>
			</div>

			<?php if ($this->_vars['page_data']['filter']['property_type']): ?>
				<?php $this->assign('category', $this->_vars['page_data']['filter']['id_category'].'_'.$this->_vars['page_data']['filter']['property_type']); ?>
			<?php else: ?>
				<?php $this->assign('category', $this->_vars['page_data']['filter']['id_category']); ?>
			<?php endif; ?>
			<div class="row">
				<div class="h"><?php echo l('field_category', 'listings', '', 'text', array()); ?>:</div>
				<div class="v"><?php echo tpl_function_block(array('name' => 'properties_select','module' => 'properties','var_name' => 'category','js_var_name' => 'category','selected' => $this->_vars['category'],'cat_select' => true), $this);?></div>
			</div>
		
			<div class="row">
				<div class="h"><?php echo l('field_country', 'listings', '', 'text', array()); ?>:</div>
				<div class="v"><?php echo tpl_function_country_select(array('select_type' => 'city','id_country' => $this->_vars['page_data']['filter']['id_country'],'id_region' => $this->_vars['page_data']['filter']['id_region'],'id_city' => $this->_vars['page_data']['filter']['id_city']), $this);?></div>
			</div>
			<div class="row">
				<div class="h"><?php echo l('field_owner', 'listings', '', 'text', array()); ?>:</div>
				<div class="v"><?php echo tpl_function_user_select(array('selected' => $this->_vars['page_data']['filter']['id_user'],'max' => 1,'var_name' => 'id_user'), $this);?></div>
			</div>
			<div class="row">
				<div class="h">
					<input type="submit" name="filter-submit" value="<?php echo l('header_filters', 'listings', '', 'button', array()); ?>">
					<input type="submit" name="filter-reset" value="<?php echo l('header_reset', 'listings', '', 'button', array()); ?>">
				</div>
			</div>
	</div>
</div>
</form>

<form id="listings_form" action="" method="post">
<table cellspacing="0" cellpadding="0" class="data" width="100%">
<tr>
	<th><?php echo l('field_photo', 'listings', '', 'text', array()); ?></th>
	<th class="w200"><?php echo l('field_description', 'listings', '', 'text', array()); ?></th>
	<th class="w150"><?php echo l('field_user_data', 'listings', '', 'text', array()); ?></th>
		<th class="w30"><a href="<?php echo $this->_vars['sort_links']['reviews']; ?>
"<?php if ($this->_vars['order'] == 'reviews'): ?> class="<?php echo $this->_run_modifier($this->_vars['order_direction'], 'lower', 'plugin', 1); ?>
"<?php endif; ?>><?php echo l('field_reviews_count', 'listings', '', 'text', array()); ?></a></th>
		<th class="w150"><a href="<?php echo $this->_vars['sort_links']['date_created']; ?>
"<?php if ($this->_vars['order'] == 'date_created'): ?> class="<?php echo $this->_run_modifier($this->_vars['order_direction'], 'lower', 'plugin', 1); ?>
"<?php endif; ?>><?php echo l('field_date_created', 'listings', '', 'text', array()); ?></a></th>
	<th class="w150">&nbsp;</th>
</tr>
<?php if (is_array($this->_vars['listings']) and count((array)$this->_vars['listings'])): foreach ((array)$this->_vars['listings'] as $this->_vars['item']):  echo tpl_function_counter(array('print' => false,'assign' => counter), $this); $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'property_output')); tpl_block_capture(array('assign' => 'property_output'), null, $this); ob_start(); ?>
	<?php echo $this->_vars['item']['property_type_str']; ?>
 <?php echo $this->_vars['item']['operation_type_str']; ?>

<?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack);  $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'price_output')); tpl_block_capture(array('assign' => 'price_output'), null, $this); ob_start(); ?>
	<?php echo tpl_function_block(array('name' => listing_price_block,'module' => 'listings','data' => $this->_vars['item'],'template' => 'small'), $this); $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
<tr<?php if (!($this->_vars['counter'] % 2)): ?> class="zebra"<?php endif; ?>>
	<td><img src="<?php echo $this->_vars['item']['media']['photo']['thumbs']['small']; ?>
" alt="<?php echo $this->_run_modifier($this->_run_modifier($this->_vars['item']['output_name'], 'truncate', 'plugin', 1, 20), 'escape', 'plugin', 1); ?>
"></td>
	<td><?php echo $this->_run_modifier($this->_vars['item']['output_name'], 'truncate', 'plugin', 1, 23); ?>
<br><?php echo $this->_run_modifier($this->_vars['property_output'], 'truncate', 'plugin', 1, 23); ?>
<br><?php echo $this->_vars['price_output']; ?>
</td>
	<td><?php echo $this->_run_modifier($this->_vars['item']['user']['output_name'], 'truncate', 'plugin', 1, 30); ?>
</td>
		<td class="center"><a href="<?php echo $this->_vars['site_url']; ?>
admin/reviews/index/listings_object/<?php echo $this->_vars['item']['id']; ?>
"><?php echo $this->_vars['item']['review_count']; ?>
</a></td>
		<td class="center"><?php echo $this->_run_modifier($this->_vars['item']['date_created'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']); ?>
</td>
	<td class="icons">
		<?php if ($this->_vars['item']['status']): ?>
		<a href="<?php echo $this->_vars['site_url']; ?>
admin/listings/activate/<?php echo $this->_vars['item']['id']; ?>
/0"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-full.png" width="16" height="16" border="0" alt="<?php echo l('link_deactivate_listing', 'listings', '', 'button', array()); ?>" title="<?php echo l('link_deactivate_listing', 'listings', '', 'button', array()); ?>"></a>
		<?php else: ?>
		<a href="<?php echo $this->_vars['site_url']; ?>
admin/listings/activate/<?php echo $this->_vars['item']['id']; ?>
/1"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-empty.png" width="16" height="16" border="0" alt="<?php echo l('link_activate_listing', 'listings', '', 'button', array()); ?>" title="<?php echo l('link_activate_listing', 'listings', '', 'button', array()); ?>"></a>
		<?php endif; ?>
		<a href="<?php echo $this->_vars['site_url']; ?>
admin/listings/edit/<?php echo $this->_vars['item']['id']; ?>
"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-edit.png" width="16" height="16" border="0" alt="<?php echo l('link_edit_listing', 'listings', '', 'button', array()); ?>" title="<?php echo l('link_edit_listing', 'listings', '', 'button', array()); ?>"></a>
		<a href="<?php echo $this->_vars['site_url']; ?>
admin/listings/delete/<?php echo $this->_vars['item']['id']; ?>
" onclick="javascript: if(!confirm('<?php echo l('note_delete_listing', 'listings', '', 'js', array()); ?>')) return false;"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-delete.png" width="16" height="16" border="0" alt="<?php echo l('link_delete_listing', 'listings', '', 'button', array()); ?>" title="<?php echo l('link_delete_listing', 'listings', '', 'button', array()); ?>"></a>
	</td>
</tr>
<?php endforeach; else:  $this->assign('colspan', 6);  $this->assign('colspan', $this->_vars['colspan']); ?><tr><td colspan="<?php echo $this->_vars['colspan']; ?>
" class="center"><?php echo l('no_listings', 'listings', '', 'text', array()); ?></td></tr>
<?php endif; ?>
</table>
</form>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "pagination.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

<?php echo tpl_function_js(array('file' => 'easyTooltip.min.js'), $this);?>
<script><?php echo '
var reload_link = "';  echo $this->_vars['site_url']; ?>
admin/listings<?php echo '";
var order = \'';  echo $this->_vars['order'];  echo '\';
var loading_content;
var order_direction = \'';  echo $this->_vars['order_direction'];  echo '\';
$(function(){
	$(".tooltip").each(function(){
		$(this).easyTooltip({
			useElement: \'span_\'+$(this).attr(\'id\')
		});
	});
});
function reload_this_page(value){
	var link = reload_link + \'/\' + value + \'/\' + order + \'/\' + order_direction;
	location.href=link;
}

'; ?>
</script>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
