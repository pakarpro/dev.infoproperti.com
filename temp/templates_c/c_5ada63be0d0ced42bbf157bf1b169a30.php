<?php require_once('/opt/ip/system/libraries/template_lite/plugins/function.js.php'); $this->register_function("js", "tpl_function_js");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-02-26 13:26:42 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->module_path. "start". $this->module_templates.  $this->get_current_theme_gid('', '"start"'). "left_panel.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

<div class="rc">
	<div class="content-block">
		<h1><?php echo l('header_my_listings', 'listings', '', 'text', array()); ?> (<?php echo $this->_vars['listings_count_sum']; ?>
)</h1>

		<div class="search-links">
			<?php echo l('link_search_in_my_listings', 'listings', '', 'text', array()); ?>
			<div class="edit_block">			
				<?php if (! $this->_vars['noshow_add_button']): ?><a class="btn-link fright my-list-add-listing" href="<?php echo $this->_vars['site_url']; ?>
listings/edit"><ins class="with-icon i-expand"></ins><span><?php echo l('link_add_listing', 'listings', '', 'text', array()); ?></span></a><?php endif; ?>			
				<form action="" method="post" enctype="multipart/form-data" id="listings_search_form">
				<div class="r">
					<input type="text" name="data[keyword]" value="<?php echo $this->_run_modifier($this->_vars['page_data']['keyword'], 'escape', 'plugin', 1); ?>
" id="listings_search" autocomplete="off" />
				</div>
				</form>
			</div>
		</div>

		<div class="tabs tab-size-15 noPrint">
			<ul id="my_listings_sections">
				<?php if (is_array($this->_vars['operation_types']) and count((array)$this->_vars['operation_types'])): foreach ((array)$this->_vars['operation_types'] as $this->_vars['tgid']): ?>
				<li id="m_<?php echo $this->_vars['tgid']; ?>
" sgid="<?php echo $this->_vars['tgid']; ?>
" class="<?php if ($this->_vars['current_operation_type'] == $this->_vars['tgid']): ?>active<?php endif; ?>"><a href="<?php echo $this->_vars['site_url']; ?>
listings/my/<?php echo $this->_vars['tgid']; ?>
"><?php echo l('operation_search_'.$this->_vars['tgid'], 'listings', '', 'text', array()); ?> (<?php echo $this->_vars['listings_count'][$this->_vars['tgid']]; ?>
)</a></li>
				<?php endforeach; endif; ?>
			</ul>
		</div>

		<div id="listings_block"><?php echo $this->_vars['block']; ?>
</div>
		
		<?php echo tpl_function_js(array('module' => users_services,'file' => 'available_view.js'), $this);?>
		<?php echo tpl_function_js(array('module' => listings,'file' => 'listings-list.js'), $this);?>
		<script><?php echo '
		$(function(){
			new listingsList({
				siteUrl: \'';  echo $this->_vars['site_url'];  echo '\',
				listAjaxUrl: \''; ?>
listings/ajax_my<?php echo '\',
				sectionId: \'my_listings_sections\',
				operationType: \'';  echo $this->_vars['current_operation_type'];  echo '\',
				order: \'';  echo $this->_vars['order'];  echo '\',
				orderDirection: \'';  echo $this->_vars['order_direction'];  echo '\',
				page: ';  echo $this->_vars['page'];  echo ',
				tIds: [\'pages_block_1\', \'pages_block_2\', \'sorter_block\'],
			});
		});
		'; ?>
</script>
	</div>
</div>
<div class="clr"></div>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
