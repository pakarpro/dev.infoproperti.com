<?php require_once('/opt/ip/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-02-29 15:43:26 KRAT */ ?>

<?php if ($this->_vars['groups']): ?>
<div class="row">
	<div class="h"><?php echo l('field_groups', 'banners', '', 'text', array()); ?>: </div>
	<div class="v">
	<?php if (is_array($this->_vars['groups']) and count((array)$this->_vars['groups'])): foreach ((array)$this->_vars['groups'] as $this->_vars['group']): ?>
		<input type="checkbox" name="banner_groups[]" value="<?php echo $this->_vars['group']['id']; ?>
" <?php if ($this->_vars['banner_groups'] && $this->_run_modifier($this->_vars['group']['id'], 'in_array', 'PHP', 1, $this->_vars['banner_groups'])): ?>checked<?php endif; ?> id="groups_<?php echo $this->_vars['group']['id']; ?>
" /><label  for="groups_<?php echo $this->_vars['group']['id']; ?>
"><?php echo $this->_vars['group']['name']; ?>
</label><br/>
	<?php endforeach; endif; ?>
	</div>
</div>
<?php endif; ?>