<?php require_once('/opt/ip/system/libraries/template_lite/plugins/modifier.escape.php'); $this->register_modifier("escape", "tpl_modifier_escape");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.ld.php'); $this->register_function("ld", "tpl_function_ld");  require_once('/opt/ip/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-12-14 15:54:40 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
  $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->module_path. "start". $this->module_templates.  $this->get_current_theme_gid('', '"start"'). "left_panel.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<div class="rc">
	<div class="content-block">
		<h1><?php echo l('text_deactivate_profile', 'users', '', 'text', array()); ?></h1>
		<?php echo l('text_deactivate_profile_tooltip', 'users', '', 'text', array()); ?><br><br>
		<div class="content-value">
			<div class="edit_block">
			<form action="" method="post">
				<div class="r">
					<div class="f"><?php echo l('field_deactivated_reason', 'users', '', 'text', array()); ?>:&nbsp;* </div>
					<div class="v">
						<select name="reason_id">
							<?php echo tpl_function_ld(array('i' => 'deactivated_reason_object','gid' => 'users','assign' => 'reasons'), $this);?>
							<?php if (is_array($this->_vars['reasons']['option']) and count((array)$this->_vars['reasons']['option'])): foreach ((array)$this->_vars['reasons']['option'] as $this->_vars['key'] => $this->_vars['item']): ?>
							<option value="<?php echo $this->_run_modifier($this->_vars['key'], 'escape', 'plugin', 1); ?>
" <?php if ($this->_vars['data']['id_reason'] == $this->_vars['key']): ?>selected="selected"<?php endif; ?>><?php echo $this->_vars['item']; ?>
</option>
							<?php endforeach; endif; ?>
						</select>
					</div>
				</div>
				<div class="r">
					<div class="f"><?php echo l('field_deactivated_message', 'users', '', 'text', array()); ?>: </div>
					<div class="v"><textarea rows="5" cols="8" name="message"><?php echo $this->_run_modifier($this->_vars['data']['message'], 'escape', 'plugin', 1); ?>
</textarea></div>
				</div>
				<div class="r">
					<div class="f">&nbsp;</div>
					<div class="v"><input type="submit" class="btn" value="<?php echo l('btn_send', 'start', '', 'button', array()); ?>" name="btn_save"></div>
				</div>
			</form>
			</div>
		</div>
	</div>
</div>
<div class="clr"></div>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
