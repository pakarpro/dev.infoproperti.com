<?php require_once('/opt/ip/system/libraries/template_lite/plugins/modifier.escape.php'); $this->register_modifier("escape", "tpl_modifier_escape");  require_once('/opt/ip/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.js.php'); $this->register_function("js", "tpl_function_js");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-02-29 15:43:16 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array('load_type' => 'ui'));
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<?php echo tpl_function_js(array('module' => banners,'file' => 'admin_banner.js'), $this);?>
<script type="text/javascript"><?php echo '
	var admin_banners;
	$(function(){
		$("div.row:odd").addClass("zebra");
		admin_banners =  new AdminBanners({
			siteUrl: \'';  echo $this->_vars['site_url'];  echo '\',
			banner_id: \'';  echo $this->_vars['data']['id'];  echo '\',
			init_banner_form: true
		});
	});
'; ?>
</script>

<form method="post" action="<?php echo $this->_vars['data']['action']; ?>
" name="save_form" enctype="multipart/form-data">
	<div class="edit-form n150">
		<div class="row header"><?php if ($this->_vars['data']['id']):  echo l('admin_header_banner_change', 'banners', '', 'text', array());  else:  echo l('admin_header_banners_add', 'banners', '', 'text', array());  endif; ?></div>
		<div class="row">
			<div class="h"><?php echo l('field_name', 'banners', '', 'text', array()); ?>:&nbsp;* </div>
			<div class="v"><input type="text" value="<?php echo $this->_run_modifier($this->_vars['data']['name'], 'escape', 'plugin', 1); ?>
" name="name"></div>
		</div>
		<?php if ($this->_vars['data']['user_id'] == 0): ?>
		<div class="row">
			<div class="h"><?php echo l('field_status', 'banners', '', 'text', array()); ?>: </div>
			<div class="v"><input type="checkbox" value="1" name="status" <?php if ($this->_vars['data']['status']): ?>checked<?php endif; ?>></div>
		</div>
		<?php endif; ?>
		<div class="row">
			<div class="h"><?php echo l('banner_place', 'banners', '', 'text', array()); ?>:&nbsp;* </div>
			<div class="v">
			<?php if ($this->_vars['data']['user_id'] == 0): ?>
				<select id="banner_place" name="banner_place_id">
					<option value="" <?php if ($this->_vars['data']['banner_place_id']): ?>selected<?php endif; ?>>...</option>
					<?php if (is_array($this->_vars['places']) and count((array)$this->_vars['places'])): foreach ((array)$this->_vars['places'] as $this->_vars['place']): ?>
					<option value="<?php echo $this->_vars['place']['id']; ?>
" <?php if ($this->_vars['place']['id'] == $this->_vars['data']['banner_place_id']): ?>selected<?php endif; ?>><?php echo $this->_vars['place']['name']; ?>
 (<?php echo $this->_vars['place']['width']; ?>
x<?php echo $this->_vars['place']['height']; ?>
)</option>
					<?php endforeach; endif; ?>
				</select>
			<?php else: ?>
				<input type="hidden" name='banner_place_id' value="<?php echo $this->_run_modifier($this->_vars['data']['banner_place_id'], 'escape', 'plugin', 1); ?>
">
				<?php if ($this->_vars['data']['banner_place_id']): ?>
					<?php if (is_array($this->_vars['places']) and count((array)$this->_vars['places'])): foreach ((array)$this->_vars['places'] as $this->_vars['place']): ?>
						<?php if ($this->_vars['place']['id'] == $this->_vars['data']['banner_place_id']): ?>
							<?php echo $this->_vars['place']['name']; ?>
 (<?php echo $this->_vars['place']['width']; ?>
x<?php echo $this->_vars['place']['height']; ?>
)
						<?php endif; ?>
					<?php endforeach; endif; ?>
				<?php else: ?>
				...
				<?php endif; ?>
			<?php endif; ?>
			</div>
		</div>

		<div id="banner_groups"><?php echo $this->_vars['banner_place_block']; ?>
</div>

		<div class="row">
			<div class="h"><?php echo l('field_type', 'banners', '', 'text', array()); ?>:&nbsp;* </div>
			<div class="v">
			<?php if ($this->_vars['data']['user_id'] == 0): ?>
				<select name="banner_type" id="banner_type">
				<option value="0">...</option>
				<?php if (is_array($this->_vars['banner_type_lang']['option']) and count((array)$this->_vars['banner_type_lang']['option'])): foreach ((array)$this->_vars['banner_type_lang']['option'] as $this->_vars['key'] => $this->_vars['item']): ?><option value="<?php echo $this->_vars['key']; ?>
"<?php if ($this->_vars['key'] == $this->_vars['data']['banner_type']): ?> selected<?php endif; ?>><?php echo $this->_vars['item']; ?>
</option><?php endforeach; endif; ?>
				</select>
			<?php else: ?>
				<input type="hidden" name='banner_type' value="<?php echo $this->_run_modifier($this->_vars['data']['banner_type'], 'escape', 'plugin', 1); ?>
">
				<?php if (is_array($this->_vars['banner_type_lang']['option']) and count((array)$this->_vars['banner_type_lang']['option'])): foreach ((array)$this->_vars['banner_type_lang']['option'] as $this->_vars['key'] => $this->_vars['item']): ?>
					<?php if ($this->_vars['key'] == $this->_vars['data']['banner_type']):  echo $this->_vars['item'];  endif; ?>
				<?php endforeach; endif; ?>
			<?php endif; ?>
			</div>
		</div>

		<div id="second_form"><?php echo $this->_vars['banner_type_block']; ?>
</div>

		<div id="result"></div>

	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div></div>
	<a class="cancel" href="<?php echo $this->_vars['site_url']; ?>
admin/banners"><?php echo l('btn_cancel', 'start', '', 'text', array()); ?></a>
</form>


<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
