<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-02 14:30:54 KRAT */ ?>

<?php if ($this->_vars['stat_contact']['index_method'] && $this->_vars['stat_contact']['data']['contact_make_copies']): ?>
<table cellspacing="0" cellpadding="0" class="data" width="100%">
	<tr>
		<th class="first" colspan=2><?php echo l('stat_header_contact', 'contact', '', 'text', array()); ?></th>
	</tr>
	<tr>
		<td class="first"><a href="<?php echo $this->_vars['site_url']; ?>
admin/contact/index/all"><?php echo l('stat_header_contact_all', 'contact', '', 'text', array()); ?></a></td>
		<td class="w30"><a href="<?php echo $this->_vars['site_url']; ?>
admin/contact/index/all"><?php echo $this->_vars['stat_contact']['data']['all']; ?>
</a></td>
	</tr>
	<tr class="zebra">
		<td class="first"><a href="<?php echo $this->_vars['site_url']; ?>
admin/contact/index/unread"><?php echo l('stat_header_contact_unread', 'contact', '', 'text', array()); ?></a></td>
		<td class="w30"><a href="<?php echo $this->_vars['site_url']; ?>
admin/contact/index/unread"><?php echo $this->_vars['stat_contact']['data']['unread']; ?>
</a></td>
	</tr>
	<?php if ($this->_vars['stat_contact']['data']['contact_need_approve']): ?>
	<tr>
		<td class="first"><a href="<?php echo $this->_vars['site_url']; ?>
admin/contact/index/need_approve"><?php echo l('stat_header_contact_need_approve', 'contact', '', 'text', array()); ?></a></td>
		<td class="w30"><a href="<?php echo $this->_vars['site_url']; ?>
admin/contact/index/need_approve"><?php echo $this->_vars['stat_contact']['data']['need_approve']; ?>
</a></td>
	</tr>
	<?php endif; ?>
</table>
<?php endif; ?>
