<?php require_once('/opt/ip/system/libraries/template_lite/plugins/function.helper.php'); $this->register_function("helper", "tpl_function_helper");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.js.php'); $this->register_function("js", "tpl_function_js");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-02-27 10:49:26 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

<div class="rc">
	<div class="content-block">
		<div id="list_mode_link"><a href="<?php echo $this->_vars['site_url']; ?>
users/set_view_mode/list" class="btn-link fright" title="<?php echo l('link_view_list', 'users', '', 'button', array()); ?>"><ins class="with-icon i-list"></ins></a></div>
		
		<h1><?php echo l('header_users_result', 'users', '', 'text', array()); ?> - <span id="total_rows"><?php echo $this->_vars['page_data']['total_rows']; ?>
</span> <?php echo l('header_users_found', 'users', '', 'text', array()); ?></h1>

		<div class="tabs tab-size-15 hide noPrint">
			<ul id="search_users_sections">
				<?php if (is_array($this->_vars['user_types']) and count((array)$this->_vars['user_types'])): foreach ((array)$this->_vars['user_types'] as $this->_vars['tgid']): ?>
				<li id="m_<?php echo $this->_vars['tgid']; ?>
" sgid="<?php echo $this->_vars['tgid']; ?>
" class="<?php if ($this->_vars['current_user_type'] == $this->_vars['tgid']): ?>active<?php endif; ?>"><a href="<?php echo $this->_run_modifier($this->_vars['menu_action_link'], 'replace', 'plugin', 1, '[user_type]', $this->_vars['tgid']); ?>
"><?php echo l($this->_vars['tgid'], 'users', '', 'text', array()); ?></a></li>
				<?php endforeach; endif; ?>
			</ul>
		</div>

		<?php if ($this->_vars['users']): ?>
		<div class="sorter line" id="sorter_block">
			<?php echo tpl_function_sorter(array('links' => $this->_vars['sort_data']['links'],'order' => $this->_vars['sort_data']['order'],'direction' => $this->_vars['sort_data']['direction'],'url' => $this->_vars['sort_data']['url']), $this);?>
			<div class="fright" id="pages_block_1"><?php echo tpl_function_pagination(array('data' => $this->_vars['page_data'],'type' => 'cute'), $this);?></div>
		</div>
		<?php endif; ?>
	
		<div id="users_map">
		<?php if ($this->_vars['users']): ?>
		<?php echo tpl_function_block(array('name' => show_default_map,'module' => geomap,'id_user' => $this->_vars['data']['id'],'gid' => 'user_search','settings' => $this->_vars['map_settings'],'width' => '755','height' => '400','map_id' => 'users_map_full_container'), $this);?>
		<?php echo $this->_vars['block']; ?>

		<?php else: ?>
		<div class="item empty"><?php echo l('empty_users', 'users', '', 'text', array()); ?></div>
		<?php endif; ?>
		</div>

		<?php if ($this->_vars['users']): ?><div id="pages_block_2"><?php echo tpl_function_pagination(array('data' => $this->_vars['page_data'],'type' => 'full'), $this);?></div><?php endif; ?>
		
		<?php echo tpl_function_js(array('module' => users,'file' => 'users-map.js'), $this);?>
		<script><?php echo '
		var usersMap;
		$(function(){
			usersMap = new usersMap({
				siteUrl: \'';  echo $this->_vars['site_url'];  echo '\',
				mapAjaxUrl: \''; ?>
users/ajax_users<?php echo '\',
				userType: \'';  echo $this->_vars['current_user_type'];  echo '\',
				sectionId: \'search_users_sections\',
				order: \'';  echo $this->_vars['order'];  echo '\',
				orderDirection: \'';  echo $this->_vars['order_direction'];  echo '\',
				page: ';  echo $this->_vars['page'];  echo ',
				tIds: [\'pages_block_1\', \'pages_block_2\', \'sorter_block\']
			});
		});
		'; ?>
</script>
	</div>
</div>

<div class="lc">
	<div class="inside account_menu">
		<?php echo tpl_function_block(array('name' => users_search_block,'module' => users), $this);?>
		<?php if ($this->_vars['use_poll_in_search']):  echo tpl_function_block(array('name' => show_poll_place_block,'module' => polls,'one_poll_place' => 0), $this); endif; ?>
		<?php echo tpl_function_helper(array('func_name' => show_banner_place,'module' => banners,'func_param' => 'big-left-banner'), $this);?>
		<?php echo tpl_function_helper(array('func_name' => show_banner_place,'module' => banners,'func_param' => 'left-banner'), $this);?>
	</div>
</div>

<div class="clr"></div>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
