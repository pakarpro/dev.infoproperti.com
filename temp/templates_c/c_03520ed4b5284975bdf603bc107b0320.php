<?php require_once('/opt/ip/system/libraries/template_lite/plugins/function.ld_option.php'); $this->register_function("ld_option", "tpl_function_ld_option");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.counter.php'); $this->register_function("counter", "tpl_function_counter");  require_once('/opt/ip/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.helper.php'); $this->register_function("helper", "tpl_function_helper");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-05-20 09:59:15 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
  echo tpl_function_helper(array('func_name' => get_admin_level1_menu,'helper_name' => menu,'func_param' => 'admin_users_types_menu'), $this);?>
<div class="actions">&nbsp;</div>

<form action="" method="post">
<table cellspacing="0" cellpadding="0" class="data" width="100%" id="login_settings">
<tr>
	<th class="first"><?php echo l('field_user_type', 'users', '', 'text', array()); ?></th>
	<th class="w100"><?php echo l('field_login_enabled', 'users', '', 'text', array()); ?></th>
	<th class="w100"><?php echo l('field_register_enabled', 'users', '', 'text', array()); ?></th>
	<th class="w100"><?php echo l('field_register_mail_confirm', 'users', '', 'text', array()); ?></th>
	<th class="w100"><?php echo l('field_search_enabled', 'users', '', 'text', array()); ?></th>
</tr>
<?php if (is_array($this->_vars['data']) and count((array)$this->_vars['data'])): foreach ((array)$this->_vars['data'] as $this->_vars['key'] => $this->_vars['item']):  echo tpl_function_counter(array('print' => false,'assign' => counter), $this);?>
<tr<?php if (!($this->_vars['counter'] % 2)): ?> class="zebra"<?php endif; ?>>
	<td><?php echo tpl_function_ld_option(array('i' => 'user_type','gid' => 'users','option' => $this->_vars['key']), $this);?>: </td>
	<td align="center">
		<input type="hidden" name="data[<?php echo $this->_vars['key']; ?>
_login_enabled]" value="0" />
		<input type="checkbox" name="data[<?php echo $this->_vars['key']; ?>
_login_enabled]" utype="<?php echo $this->_vars['key']; ?>
" atype="login" id="login_<?php echo $this->_vars['key']; ?>
" value="1" autocomplete="off" <?php if ($this->_vars['item']['login_enabled']): ?>checked<?php endif; ?> />
	</td>	
	<td align="center">
		<input type="hidden" name="data[<?php echo $this->_vars['key']; ?>
_register_enabled]" value="0" />
		<input type="checkbox" name="data[<?php echo $this->_vars['key']; ?>
_register_enabled]" utype="<?php echo $this->_vars['key']; ?>
" atype="register" id="register_<?php echo $this->_vars['key']; ?>
" value="1" autocomplete="off" <?php if ($this->_vars['item']['register_enabled']): ?>checked<?php endif; ?> <?php if (! $this->_vars['item']['login_enabled']): ?>disabled<?php endif; ?> />
	</td>
	<td align="center">
		<input type="hidden" name="data[<?php echo $this->_vars['key']; ?>
_register_mail_confirm]" value="0" />
		<input type="checkbox" name="data[<?php echo $this->_vars['key']; ?>
_register_mail_confirm]" utype="<?php echo $this->_vars['key']; ?>
" atype="mail_confirm" id="mail_confirm_<?php echo $this->_vars['key']; ?>
" value="1" autocomplete="off" <?php if ($this->_vars['item']['register_mail_confirm']): ?>checked<?php endif; ?> <?php if (! $this->_vars['item']['register_enabled'] || ! $this->_vars['item']['login_enabled']): ?>disabled<?php endif; ?> />
	</td>
	<td align="center">
		<input type="hidden" name="data[<?php echo $this->_vars['key']; ?>
_search_enabled]" value="0" />
		<input type="checkbox" name="data[<?php echo $this->_vars['key']; ?>
_search_enabled]" utype="<?php echo $this->_vars['key']; ?>
" atype="search_enabled" id="search_enabled_<?php echo $this->_vars['key']; ?>
" value="1" autocomplete="off" <?php if ($this->_vars['item']['search_enabled']): ?>checked<?php endif; ?> />
	</td>
</tr>			
<?php endforeach; endif; ?>
</table>

<div class="btn"><div class="l"><input type="submit" name="btn_auth_save" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div></div>
	
</form>
<div class="clr"></div>

<script type="text/javascript"><?php echo '
$(function(){
	$(\'#login_settings\').find(\'input[type=checkbox][atype=login]\').bind(\'click\', function(){
		var user_type = $(this).attr(\'utype\');
		var checkbox = $(\'#register_\'+user_type);
		if($(this).is(\':checked\')){
			checkbox.removeAttr(\'disabled\');
		}else{
			checkbox.removeAttr(\'checked\');
			checkbox.attr(\'disabled\', \'disabled\');
		}
		checkbox.trigger(\'click\');
	});
	
	$(\'#login_settings\').find(\'input[type=checkbox][atype=register]\').bind(\'click\', function(){
		var user_type = $(this).attr(\'utype\');
		var checkbox = $(\'#mail_confirm_\'+user_type);
		if($(this).is(\':checked\')){
			checkbox.removeAttr(\'disabled\');
		}else{
			checkbox.removeAttr(\'checked\');
			checkbox.attr(\'disabled\', \'disabled\');
		}
	});
});
'; ?>
</script>


<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>