<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.js.php'); $this->register_function("js", "tpl_function_js");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.helper.php'); $this->register_function("helper", "tpl_function_helper");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.replace.php'); $this->register_modifier("replace", "tpl_modifier_replace");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-05-12 14:11:03 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

<!-- user-listing-list : view all listing -->
<div class="rc_wrapper">
<div class="panel">
<div class="inside">

<div class="lc-1 view-all-listing" id="view_listing">
	<div class="content-block">
		<h1><?php echo l('header_listings_result', 'listings', '', 'text', array()); ?> - <span id="total_rows"><?php echo $this->_vars['page_data']['total_rows']; ?>
</span> <?php echo l('header_listings_found', 'listings', '', 'text', array()); ?></h1>
		
		<div class="tabs tab-size-15 noPrint">
			<ul id="user_listing_sections">
				<?php if (is_array($this->_vars['operation_types']) and count((array)$this->_vars['operation_types'])): foreach ((array)$this->_vars['operation_types'] as $this->_vars['tgid']): ?>
				<li id="m_<?php echo $this->_vars['tgid']; ?>
" sgid="<?php echo $this->_vars['tgid']; ?>
" class="<?php if ($this->_vars['current_operation_type'] == $this->_vars['tgid']): ?>active<?php endif; ?>"><a href="<?php echo $this->_run_modifier($this->_vars['menu_action_link'], 'replace', 'plugin', 1, '[operation_type]', $this->_vars['tgid']); ?>
/modified/DESC/1"><?php echo l('operation_search_'.$this->_vars['tgid'], 'listings', '', 'text', array()); ?></a></li>
				<?php endforeach; endif; ?>
			</ul>
			<div id="map_link"><?php echo tpl_function_helper(array('func_name' => show_map_view,'module' => geomap,'func_param' => $this->_vars['site_url'].'listings/set_view_mode/map'), $this);?></div>
		</div>
		
		
		<div class="user_listings clearfix">
			<div id="listings_block" class="user_listings clearfix">
                <?php echo $this->_vars['block']; ?>

			</div>
		</div>
		
		<?php echo tpl_function_js(array('module' => listings,'file' => 'listings-list.js'), $this);?>
		<script><?php echo '
		$(function(){
			new listingsList({
				siteUrl: \'';  echo $this->_vars['site_url'];  echo '\',
				listAjaxUrl: \''; ?>
listings/ajax_user/<?php echo $this->_vars['user']['id'];  echo '\',
				sectionId: \'user_listing_sections\',
				operationType: \'';  echo $this->_vars['current_operation_type'];  echo '\',
				order: \'';  echo $this->_vars['order'];  echo '\',
				orderDirection: \'';  echo $this->_vars['order_direction'];  echo '\',
				page: ';  echo $this->_vars['page'];  echo ',
				tIds: [\'pages_block_1\', \'pages_block_2\', \'sorter_block\'],
			});
		});
		'; ?>
</script>
	</div>
	
</div>
<div class="rc-2 right-panel">
	<?php echo tpl_function_block(array('name' => "user_info",'module' => "users",'user' => $this->_vars['user']), $this);?>
	<?php echo tpl_function_helper(array('func_name' => show_banner_place,'module' => banners,'func_param' => 'right-banner'), $this);?>
	<?php echo tpl_function_helper(array('func_name' => 'show_mortgage_calc','module' => 'listings'), $this);?>
</div>

<div class="clr"></div>

</div>
</div>
</div>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
