<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.js.php'); $this->register_function("js", "tpl_function_js");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/block.capture.php'); $this->register_block("capture", "tpl_block_capture");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-02-16 14:52:49 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->module_path. "start". $this->module_templates.  $this->get_current_theme_gid('', '"start"'). "left_panel.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

<div class="rc">
	<div class="content-block">
		<h1><?php echo l('header_my_orders', 'listings', '', 'text', array()); ?> (<?php echo $this->_vars['orders_count_sum']; ?>
)</h1>

		<div class="search-links">
			<div class="edit_block">
				<form action="" method="post" enctype="multipart/form-data" id="orders_search_form">
					<div class="r">
						<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'listings_select_callback')); tpl_block_capture(array('assign' => 'listings_select_callback'), null, $this); ob_start();  echo '
							function(data){
								ordersList.search({data: {listings: data}});
							}
						';  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
						<?php echo tpl_function_block(array('name' => 'listing_select','module' => 'listings','selected' => $this->_vars['selected'],'max' => 1,'var_name' => 'id_listing','id_user' => $this->_vars['user_id'],'operation_type' => 'rent','callback' => $this->_vars['listings_select_callback']), $this);?>
					</div>
				</form>
			</div>
		</div>
		
		<div class="tabs tab-size-15 noPrint">
			<ul id="orders_sections">
				<li id="m_wait" sgid="wait" class="<?php if ($this->_vars['status'] == 'wait'): ?>active<?php endif; ?>"><a href="<?php echo $this->_vars['site_url']; ?>
listings/orders/wait"><?php echo l('booking_status_wait', 'listings', '', 'text', array()); ?> (<span id="section_wait"><?php echo $this->_vars['orders_count']['wait']; ?>
</span>)</a></li>
				<li id="m_approved" sgid="approve" class="<?php if ($this->_vars['status'] == 'approve'): ?>active<?php endif; ?>"><a href="<?php echo $this->_vars['site_url']; ?>
listings/orders/approve"><?php echo l('booking_status_approved', 'listings', '', 'text', array()); ?> (<span id="section_approve"><?php echo $this->_vars['orders_count']['approve']; ?>
</span>)</a></li>
				<li id="m_declined" sgid="decline" class="<?php if ($this->_vars['status'] == 'decline'): ?>active<?php endif; ?>"><a href="<?php echo $this->_vars['site_url']; ?>
listings/orders/decline"><?php echo l('booking_status_declined', 'listings', '', 'text', array()); ?> (<span id="section_decline"><?php echo $this->_vars['orders_count']['decline']; ?>
</span>)</a></li>
			</ul>
		</div>

		<div id="orders_block"><?php echo $this->_vars['block']; ?>
</div>
		
		<?php echo tpl_function_js(array('module' => listings,'file' => 'listings-order.js'), $this);?>
		<?php echo tpl_function_js(array('module' => listings,'file' => 'listings-list.js'), $this);?>
		<script><?php echo '
			var ordersList;
			var orders;
			$(function(){
				ordersList = new listingsList({
					siteUrl: \'';  echo $this->_vars['site_url'];  echo '\',
					listAjaxUrl: \''; ?>
listings/ajax_orders<?php echo '\',
					sectionId: \'orders_sections\',
					listBlockId: \'orders_block\',
					sFormId: \'orders_search_form\',
					operationType: \'';  echo $this->_vars['status'];  echo '\',
					order: \'';  echo $this->_vars['order'];  echo '\',
					orderDirection: \'';  echo $this->_vars['order_direction'];  echo '\',
					page: ';  echo $this->_vars['page_data']['cur_page'];  echo ',
					tIds: [\'pages_block_1\', \'pages_block_2\', \'sorter_block\'],
				});
				orders = new listingsOrder({
					siteUrl: \'';  echo $this->_vars['site_url'];  echo '\',
					sectionId: \'orders_sections\',
					waitCnt: ';  echo $this->_vars['orders_count']['wait'];  echo ',
					approveCnt: ';  echo $this->_vars['orders_count']['approve'];  echo ',
					declineCnt: ';  echo $this->_vars['orders_count']['decline'];  echo ',
				});
			});
		'; ?>
</script>
	</div>
</div>
<div class="clr"></div>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
