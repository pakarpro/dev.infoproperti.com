<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-05-06 11:57:02 KRAT */ ?>

<div class="load_content_controller">
	<h1><?php echo l('header_user_select', 'users', '', 'text', array()); ?></h1>
	<div class="inside">	
		<?php if ($this->_vars['select_data']['max_select'] != 1): ?>
		<b><?php echo l('header_users_selected', 'users', '', 'text', array()); ?>:</b><br>
		<ul id="user_selected_items" class="user-items-selected">
		<?php if (is_array($this->_vars['select_data']['selected']) and count((array)$this->_vars['select_data']['selected'])): foreach ((array)$this->_vars['select_data']['selected'] as $this->_vars['item']): ?>
		<li><div class="user-block"><input type="checkbox" name="remove_users[]" value="<?php echo $this->_vars['item']['id']; ?>
" checked><?php echo $this->_vars['item']['nickname']; ?>
</div></li>
		<?php endforeach; endif; ?>
		</ul>
		<div class="clr"></div><br>
		<?php endif; ?>
		<b><?php echo l('header_user_find', 'users', '', 'text', array()); ?>:</b><br>
		<input type="text" id="user_search" class="controller-search">
		<ul class="controller-items" id="user_select_items"></ul>
	
		<div class="controller-actions">
			<div id="user_page" class="fright"></div>
			<div><a href="#" id="user_close_link"><?php echo l('btn_close', 'start', '', 'text', array()); ?></a></div>
		</div>
	</div>
</div>
