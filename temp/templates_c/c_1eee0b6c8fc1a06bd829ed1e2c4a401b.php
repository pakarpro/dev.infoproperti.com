<?php require_once('/opt/ip/system/libraries/template_lite/plugins/function.counter.php'); $this->register_function("counter", "tpl_function_counter");  require_once('/opt/ip/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.helper.php'); $this->register_function("helper", "tpl_function_helper");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-02-29 16:18:24 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<?php echo tpl_function_helper(array('func_name' => get_admin_level1_menu,'helper_name' => menu,'func_param' => 'admin_news_menu'), $this);?>
<div class="actions">
	<ul>
		<li><div class="l"><a href="<?php echo $this->_vars['site_url']; ?>
admin/news/edit"><?php echo l('link_add_news', 'news', '', 'text', array()); ?></a></div></li>
	</ul>
	&nbsp;
</div>

<div class="menu-level3">
	<ul>
		<?php if (is_array($this->_vars['languages']) and count((array)$this->_vars['languages'])): foreach ((array)$this->_vars['languages'] as $this->_vars['lid'] => $this->_vars['item']): ?>
		<li class="<?php if ($this->_vars['lid'] == $this->_vars['id_lang']): ?>active<?php endif;  if (! $this->_vars['filter_data'][$this->_vars['lid']]): ?> hide<?php endif; ?>"><a href="<?php echo $this->_vars['site_url']; ?>
admin/news/index/<?php echo $this->_vars['lid']; ?>
"><?php echo $this->_vars['item']['name']; ?>
 (<?php echo $this->_vars['filter_data'][$this->_vars['lid']]; ?>
)</a></li>
		<?php endforeach; endif; ?>
	</ul>
	&nbsp;
</div>
    <form action="" method="post" enctype="multipart/form-data">
    <table>
        <tr>
        	<td><input type="submit" value="search" name="btn_save"/></td>
            <td><input type="text" name="search" value="<?php echo $this->_vars['search']; ?>
"/></td>
        </tr>
	<?php if ($this->_vars['search'] != ''): ?>
        <tr>
        	<td></td>
            <td align="right"><input type="submit" name="cancel_search" value="Cancel search"/></td>
        </tr>
    <?php endif; ?>                
    </table>
    </form>
    
<table cellspacing="0" cellpadding="0" class="data" width="100%">
<tr>
	<th class="first w150">Tags</th>
</tr>

<?php echo tpl_function_counter(array('print' => false,'assign' => counter), $this);?>
<tr<?php if (!($this->_vars['counter'] % 2)): ?> class="zebra"<?php endif; ?>>
	<td>
    <?php if (is_array($this->_vars['tags']) and count((array)$this->_vars['tags'])): foreach ((array)$this->_vars['tags'] as $this->_vars['item']): ?>
                <div style="min-height:10px; display:inline-block; position:relative; margin-right:5px; background-color:white; border-bottom:1px solid black;">
                	<div style="position:relative; float:left; margin-right:5px;">
                		<?php echo $this->_vars['item']['tag_name']; ?>
                    
                    </div>
                    <a href="<?php echo $this->_vars['site_url']; ?>
admin/news/delete_tag/<?php echo $this->_vars['item']['tag_id']; ?>
">
                        <div style="position:relative; float:right; text-align:right;">
                                X
                        </div>
                    </a>                                        
                </div>,
    <?php endforeach; else: ?>
		No tags
	<?php endif; ?>
    </td>
</tr>
</table>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "pagination.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>


<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
