<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-02-24 13:50:11 KRAT */ ?>

<div class="content-block">

	<div class="edit_block">
		<div class="r">
			<div class="f"><?php echo l('on_account_header', 'users_payments', '', 'text', array()); ?>: <b><?php echo tpl_function_block(array('name' => currency_format_output,'module' => start,'value' => $this->_vars['data']['account'],'cur_gid' => $this->_vars['base_currency']['gid']), $this);?></b></div>
		</div>
		<h2 id="account"><?php echo l('header_add_funds', 'users_payments', '', 'text', array()); ?></h1>
		<form action="<?php echo $this->_vars['site_url']; ?>
users_payments/save_payment/" method="post" >
		<div class="r">
			<div class="f"><?php echo l('field_amount', 'users_payments', '', 'text', array()); ?>:&nbsp;* </div>
			<div class="v"><input type="text" name="amount" class="short"> <?php echo tpl_function_block(array('name' => currency_output,'module' => start,'cur_gid' => $this->_vars['base_currency']['gid']), $this);?></div>
		</div>
		<?php if ($this->_vars['billing_systems']): ?>
		<div class="r">
			<div class="f"><?php echo l('field_billing', 'users_payments', '', 'text', array()); ?>:&nbsp;* </div>
			<div class="v"><select name="system_gid" id="system_gid" class="middle"><option value="">...</option><?php if (is_array($this->_vars['billing_systems']) and count((array)$this->_vars['billing_systems'])): foreach ((array)$this->_vars['billing_systems'] as $this->_vars['item']): ?><option value="<?php echo $this->_vars['item']['gid']; ?>
"><?php echo $this->_vars['item']['name']; ?>
</option><?php endforeach; endif; ?></select></div>
		</div>
		<?php if (is_array($this->_vars['billing_systems']) and count((array)$this->_vars['billing_systems'])): foreach ((array)$this->_vars['billing_systems'] as $this->_vars['item']): ?>
		<div class="r hide" id="details_<?php echo $this->_vars['item']['gid']; ?>
">
			<div class="f"><?php echo l('field_info_data', 'payments', '', 'text', array()); ?>:</div>
			<div class="v"><?php echo $this->_vars['item']['info_data']; ?>
</div>
		</div>
		<?php endforeach; endif; ?>
		<div class="r">
			<div class="b"><input type="submit" class='btn' value="<?php echo l('btn_send', 'start', '', 'button', array()); ?>" name="btn_payment_save"></div>
		</div>
		<script><?php echo '
			$(function(){
				$(\'#system_gid\').bind(\'change\', function(){
					';  if (is_array($this->_vars['billing_systems']) and count((array)$this->_vars['billing_systems'])): foreach ((array)$this->_vars['billing_systems'] as $this->_vars['item']):  echo '
					$(\'#details_';  echo $this->_vars['item']['gid'];  echo '\').hide();
					';  if ($this->_vars['item']['info_data']):  echo '
					if($(this).val() == \'';  echo $this->_vars['item']['gid'];  echo '\') 
						$(\'#details_';  echo $this->_vars['item']['gid'];  echo '\').show();
					';  endif;  echo '
					';  endforeach; endif;  echo '
				});
			});
		'; ?>
</script>
		<?php else: ?>
		<div class="r">
			<i><?php echo l('error_empty_billing_system_list', 'users_payments', '', 'text', array()); ?></i>
		</div>
		<?php endif; ?>
		</form>
	</div>
</div>
