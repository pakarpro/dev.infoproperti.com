<?php require_once('/opt/ip/system/libraries/template_lite/plugins/function.ld.php'); $this->register_function("ld", "tpl_function_ld");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-02 14:30:12 KRAT */ ?>

<div id="calendar" class="cal-type-<?php echo $this->_vars['listing']['price_period']; ?>
 noPrint">
	<div class="calendar-nav">
		<div class="fleft">
							<?php switch($this->_vars['listing']['price_period']): case '1':  ?>
					<a href="#" class="btn-link" id="prev_month" title="<?php echo l('link_month_prev', 'listings', '', 'button', array()); ?>"><ins class="with-icon i-larr"></ins></a>
				<?php break; case '2':  ?>
					<a href="#" class="btn-link" id="prev_year" title="<?php echo l('link_year_prev', 'listings', '', 'button', array()); ?>"><ins class="with-icon i-larr"></ins></a> 
			<?php break; endswitch; ?>
		</div>
		<div class="fright">
							<?php switch($this->_vars['listing']['price_period']): case '1':  ?>
					<a href="#" class="btn-link fright" id="next_month" title="<?php echo l('link_month_next', 'listings', '', 'button', array()); ?>"><ins class="with-icon i-rarr"></ins></a>
				<?php break; case '2':  ?>
					<a href="#" class="btn-link fright" id="next_year" title="<?php echo l('link_year_next', 'listings', '', 'button', array()); ?>"><ins class="with-icon i-rarr"></ins></a>
			<?php break; endswitch; ?>
		</div>
		<div class="clr"></div>
	</div>
	
	<div id="calendar_block">
		<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->module_path. "listings". $this->module_templates.  $this->get_current_theme_gid('"default"', '"listings"'). "calendar_block.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
	</div>
	
	<div class="statusbar">
		<?php echo tpl_function_ld(array('i' => 'booking_status','gid' => 'listings','assign' => 'booking_status'), $this);?>
		<?php if (is_array($this->_vars['booking_status']['option']) and count((array)$this->_vars['booking_status']['option'])): foreach ((array)$this->_vars['booking_status']['option'] as $this->_vars['key2'] => $this->_vars['item2']): ?>
		<?php if ($this->_vars['key2'] == 'open' || $this->_vars['key2'] == 'book'): ?>
		<span class="<?php echo $this->_vars['key2']; ?>
"></span> <?php echo $this->_vars['item2']; ?>

		<?php endif; ?>
		<?php endforeach; endif; ?>
	</div>
</div>
<?php echo tpl_function_js(array('file' => 'booking-form.js','module' => 'listings'), $this); echo tpl_function_js(array('file' => 'listings-calendar.js','module' => 'listings'), $this);?>
<script><?php echo '
	var listings_calendar';  echo $this->_vars['rand'];  echo ';
	$(function(){
		listings_calendar';  echo $this->_vars['rand'];  echo ' = new listingsCalendar({
			siteUrl: \'';  echo $this->_vars['site_url'];  echo '\',
			listingId: ';  echo $this->_vars['listing']['id'];  echo ',
			count: ';  echo $this->_vars['count'];  echo ',
			month: ';  echo $this->_vars['m'];  echo ',
			year: ';  echo $this->_vars['y'];  echo ',
		});
	});
'; ?>
</script>

<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'calendar_periods')); tpl_block_capture(array('assign' => 'calendar_periods'), null, $this); ob_start(); ?>
<div class="calendar_periods edit_block">
	<div class="rollup-box">
	<h2><?php echo l('header_calendar_periods', 'listings', '', 'text', array()); ?></h2>
	<table class="list" id="listing_periods">
		<tr>
			<th class="w200"><?php echo l('field_booking_period', 'listings', '', 'text', array()); ?></th>
			<th class="w30"><?php echo l('field_booking_guests', 'listings', '', 'text', array()); ?></th>
			<th class="w70"><?php echo l('field_booking_price', 'listings', '', 'text', array()); ?>, <?php echo $this->_vars['current_price_currency']['abbr']; ?>
 <?php echo tpl_function_ld_option(array('i' => 'price_period','gid' => 'listings','option' => $this->_vars['data']['price_period']), $this);?></th>
			<th class="w100"><?php echo l('field_booking_comment', 'listings', '', 'text', array()); ?></th>
			<?php if ($this->_vars['edit']): ?><th class="w100">&nbsp;</th><?php endif; ?>
		</tr>
		<?php $this->assign('is_opened', 0); ?>
		<?php if (is_array($this->_vars['data']['booking']['periods']) and count((array)$this->_vars['data']['booking']['periods'])): foreach ((array)$this->_vars['data']['booking']['periods'] as $this->_vars['item']): ?>
		<?php if ($this->_vars['item']['id_user'] == $this->_vars['item']['id_owner'] && $this->_vars['item']['status'] == 'open'): ?>
		<?php $this->assign('is_opened', 1); ?>
		<?php $this->assign('period', $this->_vars['item']); ?>
		<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->module_path. "listings". $this->module_templates.  $this->get_current_theme_gid('', '"listings"'). "calendar_period.tpl", array('themes' => "default"));
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
		<?php endif; ?>
		<?php endforeach; endif; ?>
		<?php if (! $this->_vars['is_opened']): ?><tr><td colspan="<?php if ($this->_vars['edit']): ?>5<?php else: ?>4<?php endif; ?>" class="center gray_italic"><?php echo l('no_periods', 'listings', '', 'text', array()); ?></td></tr><?php endif; ?>
	</table>
	</div>
	
	<?php if ($this->_vars['edit']): ?>	
	<div class="b outside noPrint">
		<input type="button" value="<?php echo l('btn_add', 'start', '', 'button', array()); ?>" name="btn_period" id="btn_period">
	</div>
	<script><?php echo '
		$(function(){
			new bookingForm({
				siteUrl: \'';  echo $this->_vars['site_url'];  echo '\',
				listingId: \'';  echo $this->_vars['data']['id'];  echo '\',
				bookingBtn: \'btn_period\',
				cFormId: \'period_form\',
				isSavePeriod: false,
				urlGetForm: \'listings/ajax_period_form/\',
				urlSaveForm: \'listings/ajax_save_period/\',
				calendar: listings_calendar';  echo $this->_vars['rand'];  echo ',
				successCallback: function(id, data, calendar){
					var periods = $(\'#listing_periods\');
					var row = periods.find(\'tr:nth-child(2)\').first();
					if(row.children().length == 1) row.remove();
					periods.find(\'tr:first-child\').after(data);
					window[\'period_\'+id].set_calendar(calendar);
				},
			});
		});
	'; ?>
</script>
	<?php endif; ?>
	
	<?php if ($this->_vars['edit']): ?>	
	<div class="rollup-box noPrint">
	
	<h2><?php echo l('header_calendar_booked', 'listings', '', 'text', array()); ?></h2>
	<table class="list" id="listing_booked">
		<tr>
			<th class="w200"><?php echo l('field_booking_period', 'listings', '', 'text', array()); ?></th>
			<th class="w30"><?php echo l('field_booking_guests', 'listings', '', 'text', array()); ?></th>
			<th class="w70">&nbsp;</th>
			<th class="w100"><?php echo l('field_booking_comment', 'listings', '', 'text', array()); ?></th>
			<th class="w100">&nbsp;</th>
		</tr>
		<?php $this->assign('is_booked', 0); ?>
		<?php if (is_array($this->_vars['data']['booking']['periods']) and count((array)$this->_vars['data']['booking']['periods'])): foreach ((array)$this->_vars['data']['booking']['periods'] as $this->_vars['item']): ?>
		<?php if ($this->_vars['item']['id_user'] == $this->_vars['item']['id_owner'] && $this->_vars['item']['status'] == 'book'): ?>
		<?php $this->assign('is_booked', 1); ?>
		<?php $this->assign('period', $this->_vars['item']); ?>
		<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->module_path. "listings". $this->module_templates.  $this->get_current_theme_gid('', '"listings"'). "calendar_period.tpl", array('themes' => "default"));
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
		<?php endif; ?>
		<?php endforeach; endif; ?>
		<?php if (! $this->_vars['is_booked']): ?><tr><td colspan="<?php if ($this->_vars['edit']): ?>5<?php else: ?>4<?php endif; ?>" class="center gray_italic"><?php echo l('no_periods', 'listings', '', 'text', array()); ?></td></tr><?php endif; ?>	
	</table>
	
	</div>
					
	<div class="b outside noPrint">
		<input type="button" value="<?php echo l('btn_add', 'start', '', 'button', array()); ?>" name="btn_order" id="btn_order">
	</div>
	<script><?php echo '
		$(function(){
			new bookingForm({
				siteUrl: \'';  echo $this->_vars['site_url'];  echo '\',
				listingId: \'';  echo $this->_vars['data']['id'];  echo '\',
				bookingBtn: \'btn_order\',
				isSavePeriod: false,
				urlGetForm: \'listings/ajax_order_form/\',
				urlSaveForm: \'listings/ajax_save_order/\',
				urlGetPrice: \'listings/ajax_get_booking_price/\',
				calendar: listings_calendar';  echo $this->_vars['rand'];  echo ',
				successCallback: function(id, data, calendar){
					var periods = $(\'#listing_booked\');
					var row = periods.find(\'tr:nth-child(2)\').first();
					if(row.children().length == 1) row.remove();
					periods.find(\'tr:first-child\').after(data);
					window[\'period_\'+id].set_calendar(calendar);
				},
			});
		});
	'; ?>
</script>
	<?php endif; ?>
</div>
<?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack);  if ($this->_vars['edit'] || $this->_vars['is_opened']):  echo $this->_vars['calendar_periods'];  endif; ?>
