<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-02-26 13:16:36 KRAT */ ?>

<?php if ($this->_vars['data']['price_negotiated']): ?>
	<?php echo l('text_negotiated_price_'.$this->_vars['data']['operation_type'], 'listings', '', 'text', array());  else: ?>
			<?php switch($this->_vars['data']['operation_type']): case 'sale':  ?>
			<?php if ($this->_vars['data']['price_reduced'] > 0): ?>
				<?php if ($this->_vars['template'] == 'small'): ?>
					<?php echo tpl_function_block(array('name' => 'currency_format_output','module' => 'start','value' => $this->_vars['data']['price_reduced'],'cur_gid' => $this->_vars['data']['gid_currency']), $this);?>
				<?php else: ?>
					<div class="fleft"><?php echo tpl_function_block(array('name' => 'currency_format_output','module' => 'start','value' => $this->_vars['data']['price_reduced'],'cur_gid' => $this->_vars['data']['gid_currency']), $this);?></div>&nbsp;
					<del><?php echo tpl_function_block(array('name' => 'currency_format_output','module' => 'start','value' => $this->_vars['data']['price'],'cur_gid' => $this->_vars['data']['gid_currency']), $this);?></del>
				<?php endif; ?>
			<?php else: ?>
				<?php echo tpl_function_block(array('name' => 'currency_format_output','module' => 'start','value' => $this->_vars['data']['price'],'cur_gid' => $this->_vars['data']['gid_currency']), $this);?>
			<?php endif; ?>
		<?php break; case 'buy':  ?>
			<?php if ($this->_vars['data']['price'] > 0):  echo l('text_price_from', 'listings', '', 'text', array()); ?> <?php echo tpl_function_block(array('name' => 'currency_format_output','module' => 'start','value' => $this->_vars['data']['price'],'cur_gid' => $this->_vars['data']['gid_currency']), $this); endif; ?>
			<?php if ($this->_vars['data']['price_max'] > 0):  echo l('text_price_to', 'listings', '', 'text', array()); ?> <?php echo tpl_function_block(array('name' => 'currency_format_output','module' => 'start','value' => $this->_vars['data']['price_max'],'cur_gid' => $this->_vars['data']['gid_currency']), $this); endif; ?>
		<?php break; case 'rent':  ?>
			<?php if ($this->_vars['data']['price_reduced'] > 0): ?>
				<?php if ($this->_vars['template'] == 'small'): ?>
					<?php echo tpl_function_block(array('name' => 'currency_format_output','module' => 'start','value' => $this->_vars['data']['price_reduced'],'cur_gid' => $this->_vars['data']['gid_currency']), $this);?>
				<?php else: ?>
					<div class="fleft"><?php echo tpl_function_block(array('name' => 'currency_format_output','module' => 'start','value' => $this->_vars['data']['price_reduced'],'cur_gid' => $this->_vars['data']['gid_currency']), $this);?></div>&nbsp;
					<del><?php echo tpl_function_block(array('name' => 'currency_format_output','module' => 'start','value' => $this->_vars['data']['price'],'cur_gid' => $this->_vars['data']['gid_currency']), $this);?></del>
				<?php endif; ?>
			<?php else: ?>
				<?php echo tpl_function_block(array('name' => 'currency_format_output','module' => 'start','value' => $this->_vars['data']['price'],'cur_gid' => $this->_vars['data']['gid_currency']), $this);?>
			<?php endif; ?>
			
			<?php echo $this->_vars['data']['price_period_str']; ?>

		<?php break; case 'lease':  ?>
			<?php if ($this->_vars['data']['price'] > 0):  echo l('text_price_from', 'listings', '', 'text', array()); ?> <?php echo tpl_function_block(array('name' => 'currency_format_output','module' => 'start','value' => $this->_vars['data']['price'],'cur_gid' => $this->_vars['data']['gid_currency']), $this); endif; ?>
			<?php if ($this->_vars['data']['price_max'] > 0):  echo l('text_price_to', 'listings', '', 'text', array()); ?> <?php echo tpl_function_block(array('name' => 'currency_format_output','module' => 'start','value' => $this->_vars['data']['price_max'],'cur_gid' => $this->_vars['data']['gid_currency']), $this); endif; ?>		
			
			<?php echo $this->_vars['data']['price_period_str']; ?>

	<?php break; endswitch;  endif; ?>
