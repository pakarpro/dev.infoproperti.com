<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.escape.php'); $this->register_modifier("escape", "tpl_modifier_escape");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2014-08-05 11:50:19 KRAT */ ?>

<div class="content-block load_content">
	<h1><?php echo l('header_spam_form', 'spam', '', 'text', array()); ?></h1>
	<div class="inside edit_block">
		<form id="spam_form" action="" method="POST">
			<div class="r">
				<?php if ($this->_vars['data']['form_type'] == 'select_text'): ?>
				<div class="f"><?php echo l('field_spam_reason', 'spam', '', 'text', array()); ?>&nbsp;*</div>
				<div class="v">
					<select name="data[id_reason]">
						<?php if (is_array($this->_vars['reasons']['option']) and count((array)$this->_vars['reasons']['option'])): foreach ((array)$this->_vars['reasons']['option'] as $this->_vars['key'] => $this->_vars['item']): ?><option value="<?php echo $this->_run_modifier($this->_vars['key'], 'escape', 'plugin', 1); ?>
"><?php echo $this->_vars['item']; ?>
</option><?php endforeach; endif; ?>
					</select>
				</div>
			</div>
			<div class="r">
				<div class="f"><?php echo l('field_spam_message', 'spam', '', 'text', array()); ?>&nbsp;(<?php echo l('field_spam_optional', 'spam', '', 'text', array()); ?>)</div>
				<div class="v"><textarea name="data[message]" rows="5" cols="23"></textarea></div>
				<?php endif; ?>
			</div>
			<div class="r"><input type="button" value="<?php echo l('btn_send', 'start', '', 'button', array()); ?>" id="close_btn" /></div>
			<input type="hidden" name="type_gid" value="<?php echo $this->_vars['data']['gid']; ?>
" />
			<input type="hidden" name="object_id" value="<?php echo $this->_vars['object_id']; ?>
" />
			<?php if ($this->_vars['is_spam_owner']): ?><input type="hidden" name="is_owner" value="1"><?php endif; ?>
		</form>
	</div>
</div>

