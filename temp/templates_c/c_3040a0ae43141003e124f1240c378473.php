<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.date_format.php'); $this->register_modifier("date_format", "tpl_modifier_date_format");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.truncate.php'); $this->register_modifier("truncate", "tpl_modifier_truncate");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.counter.php'); $this->register_function("counter", "tpl_function_counter");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.lower.php'); $this->register_modifier("lower", "tpl_modifier_lower");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.helper.php'); $this->register_function("helper", "tpl_function_helper");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-03-09 12:43:31 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
  echo tpl_function_helper(array('func_name' => get_admin_level1_menu,'helper_name' => menu,'func_param' => 'admin_users_settings_menu'), $this);?>

<div class="actions">
	<ul>
		<li id="delete_all"><div class="l"><a href="<?php echo $this->_vars['site_url']; ?>
admin/users/deactivated_alerts_delete"><?php echo l('btn_delete', 'start', '', 'text', array()); ?></a></div></li>
	</ul>
	&nbsp;
</div>

<form id="deactivated_alerts_form" action="" method="post">
<table cellspacing="0" cellpadding="0" class="data" width="100%">
	<tr>
		<th class="first w20"><input type="checkbox" id="grouping_all"></th>
		<th class="w100"><a href="<?php echo $this->_vars['sort_links']['name']; ?>
"<?php if ($this->_vars['order'] == 'name'): ?> class="<?php echo $this->_run_modifier($this->_vars['order_direction'], 'lower', 'plugin', 1); ?>
"<?php endif; ?>><?php echo l('field_deactivated_alert_name', 'users', '', 'text', array()); ?></a></th>
		<th class="w100"><a href="<?php echo $this->_vars['sort_links']['email']; ?>
"<?php if ($this->_vars['order'] == 'email'): ?> class="<?php echo $this->_run_modifier($this->_vars['order_direction'], 'lower', 'plugin', 1); ?>
"<?php endif; ?>><?php echo l('field_deactivated_alert_email', 'users', '', 'text', array()); ?></a></th>
		<th class="w100"><a href="<?php echo $this->_vars['sort_links']['id_reason']; ?>
"<?php if ($this->_vars['order'] == 'id_reason'): ?> class="<?php echo $this->_run_modifier($this->_vars['order_direction'], 'lower', 'plugin', 1); ?>
"<?php endif; ?>><?php echo l('field_deactivated_alert_reason', 'users', '', 'text', array()); ?></a></th>
		<th class="w50"><a href="<?php echo $this->_vars['sort_links']['date_add']; ?>
"<?php if ($this->_vars['order'] == 'date_add'): ?> class="<?php echo $this->_run_modifier($this->_vars['order_direction'], 'lower', 'plugin', 1); ?>
"<?php endif; ?>><?php echo l('field_deactivated_alert_date_add', 'users', '', 'text', array()); ?></a></th>
		<th class="w70">&nbsp;</th>
	</tr>
	<?php if (is_array($this->_vars['alerts']) and count((array)$this->_vars['alerts'])): foreach ((array)$this->_vars['alerts'] as $this->_vars['item']): ?>
		<?php echo tpl_function_counter(array('print' => false,'assign' => counter), $this);?>
		<tr<?php if (!($this->_vars['counter'] % 2)): ?> class="zebra"<?php endif; ?>>				
			<td class="center"><input type="checkbox" name="ids[]" class="grouping" value="<?php echo $this->_vars['item']['id']; ?>
" /></td>
			<td><?php if (! $this->_vars['item']['status']): ?><b><?php endif;  echo $this->_run_modifier($this->_vars['item']['name'], 'truncate', 'plugin', 1, 100);  if (! $this->_vars['item']['status']): ?></b><?php endif; ?></td>
			<td><?php if (! $this->_vars['item']['status']): ?><b><?php endif;  echo $this->_run_modifier($this->_vars['item']['email'], 'truncate', 'plugin', 1, 50);  if (! $this->_vars['item']['status']): ?></b><?php endif; ?></td>
			<td><?php if (! $this->_vars['item']['status']): ?><b><?php endif;  echo $this->_run_modifier($this->_vars['item']['reason'], 'truncate', 'plugin', 1, 50);  if (! $this->_vars['item']['status']): ?></b><?php endif; ?></td>
			<td class="center"><?php if (! $this->_vars['item']['status']): ?><b><?php endif;  echo $this->_run_modifier($this->_vars['item']['date_add'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']);  if (! $this->_vars['item']['status']): ?></b><?php endif; ?></td>
			<td class="icons">	
				<a href="<?php echo $this->_vars['site_url']; ?>
admin/users/deactivated_alerts_show/<?php echo $this->_vars['item']['id']; ?>
"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-view.png" width="16" height="16" border="0" alt="<?php echo l('link_alerts_show', 'spam', '', 'button', array()); ?>" title="<?php echo l('link_deactivate_show', 'users', '', 'button', array()); ?>"></a>
				<a href="<?php echo $this->_vars['site_url']; ?>
admin/users/deactivated_alerts_delete/<?php echo $this->_vars['item']['id']; ?>
" onclick="javascript: if(!confirm('<?php echo l('note_deactivated_alert_delete', 'users', '', 'js', array()); ?>')) return false;"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-delete.png" width="16" height="16" border="0" alt="<?php echo l('link_deactivated_alerts_delete', 'users', '', 'text', array()); ?>" title="<?php echo l('link_deactivated_alerts_delete', 'users', '', 'text', array()); ?>"></a>
			</td>
		</tr>
	<?php endforeach; else: ?>
		<tr><td colspan="6" class="center"><?php echo l('no_deactivated_alerts', 'users', '', 'text', array()); ?></td></tr>
	<?php endif; ?>
</table>
</form>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "pagination.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

<script><?php echo '
var reload_link = "';  echo $this->_vars['site_url']; ?>
admin/users/deactivated_alerts/<?php echo '";
var order = \'';  echo $this->_vars['order'];  echo '\';
var loading_content;
var order_direction = \'';  echo $this->_vars['order_direction'];  echo '\';
$(function(){
	$(\'#grouping_all\').bind(\'click\', function(){
		var checked = $(this).is(\':checked\');
		if(checked){
			$(\'input[type=checkbox].grouping\').attr(\'checked\', \'checked\');
		}else{
			$(\'input[type=checkbox].grouping\').removeAttr(\'checked\');
		}
	});
	$(\'#delete_all\').bind(\'click\', function(){
		if(!$(\'input[type=checkbox].grouping\').is(\':checked\')) return false;
		if(this.id == \'delete_all\' && !confirm(\'';  echo l('note_deactivated_alerts_delete_all', 'users', '', 'js', array());  echo '\')) return false;
		$(\'#deactivated_alerts_form\').attr(\'action\', $(this).find(\'a\').attr(\'href\')).submit();		
		return false;
	});

});
function reload_this_page(value){
	var link = reload_link + value + \'/\' + order + \'/\' + order_direction;
	location.href=link;
}
'; ?>
</script>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
