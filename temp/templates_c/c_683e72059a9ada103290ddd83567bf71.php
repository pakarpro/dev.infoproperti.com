<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.escape.php'); $this->register_modifier("escape", "tpl_modifier_escape");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.helper.php'); $this->register_function("helper", "tpl_function_helper");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-03-09 12:43:41 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
  echo tpl_function_helper(array('func_name' => get_admin_level1_menu,'helper_name' => menu,'func_param' => 'admin_users_settings_menu'), $this);?>
<div class="actions">&nbsp;</div>
<form method="post" action="<?php echo $this->_vars['site_url']; ?>
admin/users/deactivated_settings" name="save_form">
	<div class="edit-form n150">		
		<div class="row header"><?php echo l('admin_header_deactivated_settings', 'users', '', 'text', array()); ?></div>
		<div class="row">
			<div class="h"><label for="deactivated_send_alert"><?php echo l('field_deactivated_send_alert', 'users', '', 'text', array()); ?></label>: </div>
			<div class="v">
				<input type="hidden" name="deactivated_send_alert" value="0" /> 
				<input type="checkbox" name="deactivated_send_alert" value="1" id="deactivated_send_alert" <?php if ($this->_vars['data']['deactivated_send_alert']): ?>checked="checked"<?php endif; ?> /> 
			</div>
		</div>		
		<div class="row zebra">
			<div class="h"><label for="deactivated_send_mail"><?php echo l('field_deactivated_send_mail', 'users', '', 'text', array()); ?></label>: </div>
			<div class="v">
				<input type="hidden" name="deactivated_send_mail" value="0" /> 
				<input type="checkbox" name="deactivated_send_mail" value="1" id="deactivated_send_mail" <?php if ($this->_vars['data']['deactivated_send_mail']): ?>checked="checked"<?php endif; ?> /> 
				&nbsp;&nbsp;
				<?php echo l('field_deactivated_email', 'users', '', 'text', array()); ?>
				<input type="text" name="deactivated_email" value="<?php echo $this->_run_modifier($this->_vars['data']['deactivated_email'], 'escape', 'plugin', 1); ?>
" id="deactivated_email_row" <?php if (! $this->_vars['data']['deactivated_send_mail']): ?>disabled<?php endif; ?> /> 
			</div>
		</div>
	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div></div>
</form>
<div class="clr"></div>
<script><?php echo '
$(function(){
	$(\'#deactivated_send_mail\').bind(\'change\', function(){
		if(this.checked){
			$(\'#deactivated_email_row\').removeAttr(\'disabled\');
		}else{
			$(\'#deactivated_email_row\').attr(\'disabled\', \'disabled\');
		}
	});
});
'; ?>
</script>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
