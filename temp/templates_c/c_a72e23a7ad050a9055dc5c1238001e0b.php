<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-02 15:51:02 KRAT */ ?>

	<?php if ($this->_vars['main_search_form_data']): ?>
		<?php if (is_array($this->_vars['main_search_form_data']) and count((array)$this->_vars['main_search_form_data'])): foreach ((array)$this->_vars['main_search_form_data'] as $this->_vars['item']): ?>
		<?php if ($this->_vars['item']['type'] == 'section'): ?>
			<?php if (is_array($this->_vars['item']['section']['fields']) and count((array)$this->_vars['item']['section']['fields'])): foreach ((array)$this->_vars['item']['section']['fields'] as $this->_vars['key'] => $this->_vars['field']): ?>
			<div class="search-field custom <?php echo $this->_vars['field']['field']['type']; ?>
 <?php echo $this->_vars['field']['settings']['search_type']; ?>
">
				<p><?php if ($this->_vars['field']['field']['type'] != 'checkbox'):  echo $this->_vars['field']['field_content']['name'];  else: ?>&nbsp;<?php endif; ?></p>
				<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->module_path. "listings". $this->module_templates.  $this->get_current_theme_gid('', '"listings"'). "helper_main_search_field_block.tpl", array('field' => $this->_vars['field']));
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
				<input type="hidden" name="short_name" value="fe<?php echo $this->_vars['item']['field_content']['id']; ?>
">
			</div>
			<?php endforeach; endif; ?>
		<?php else: ?>
			<div class="search-field custom <?php echo $this->_vars['item']['field']['type']; ?>
 <?php echo $this->_vars['item']['settings']['search_type']; ?>
">
				<p><?php if ($this->_vars['item']['field']['type'] != 'checkbox'):  echo $this->_vars['item']['field_content']['name'];  else: ?>&nbsp;<?php endif; ?></p>
				<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->module_path. "listings". $this->module_templates.  $this->get_current_theme_gid('', '"listings"'). "helper_main_search_field_block.tpl", array('field' => $this->_vars['item'],'field_gid' => $this->_vars['item']['gid']));
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
				<input type="hidden" name="short_name" value="fe<?php echo $this->_vars['item']['field_content']['id']; ?>
">
			</div>
		<?php endif; ?>
		<?php endforeach; endif; ?>
	<?php endif; ?>
