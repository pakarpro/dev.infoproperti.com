<?php require_once('/opt/ip/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-01-19 16:23:40 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<form method="post" action="<?php echo $this->_vars['data']['action']; ?>
" name="save_form">
	<div class="edit-form n150">
		<div class="row header"><?php echo l('admin_header_global_seo_settings_editing', 'seo', '', 'text', array()); ?> : <?php if ($this->_vars['controller'] == 'admin'):  echo l('default_seo_admin_field', 'seo', '', 'text', array());  else:  echo l('default_seo_user_field', 'seo', '', 'text', array());  endif; ?></div>
		<div class="row">
			<div class="h"><?php echo l('field_title_default', 'seo', '', 'text', array()); ?>: </div>
			<div class="v"><?php echo $this->_vars['default_settings']['title']; ?>
&nbsp;</div>
		</div>
		<div class="row zebra">
			<div class="h"><?php echo l('field_title_default_use', 'seo', '', 'text', array()); ?>: </div>
			<div class="v"><input type="checkbox" value="1" name="default_title" <?php if ($this->_vars['default_settings']['default_title']): ?>checked="checked"<?php endif; ?> id="default-title"></div>
		</div>
		<?php if (is_array($this->_vars['languages']) and count((array)$this->_vars['languages'])): foreach ((array)$this->_vars['languages'] as $this->_vars['key'] => $this->_vars['item']): ?>
		<div class="row">
			<div class="h"><?php echo l('field_title', 'seo', '', 'text', array()); ?>(<?php echo $this->_vars['item']['name']; ?>
): </div>
			<div class="v"><input type="text" value="<?php echo $this->_vars['user_settings']['title'][$this->_vars['key']]; ?>
" name="title[<?php echo $this->_vars['key']; ?>
]" <?php if ($this->_vars['default_settings']['default_title']): ?>disabled<?php endif; ?> class="long input-default-title"></div>
		</div>
		<?php endforeach; endif; ?>
		<br>

		<div class="row">
			<div class="h"><?php echo l('field_keyword_default', 'seo', '', 'text', array()); ?>: </div>
			<div class="v"><?php echo $this->_vars['default_settings']['keyword']; ?>
&nbsp;</div>
		</div>
		<div class="row zebra">
			<div class="h"><?php echo l('field_keyword_default_use', 'seo', '', 'text', array()); ?>: </div>
			<div class="v"><input type="checkbox" value="1" name="default_keyword" <?php if ($this->_vars['default_settings']['default_keyword']): ?>checked="checked"<?php endif; ?> id="default-keyword"></div>
		</div>
		<?php if (is_array($this->_vars['languages']) and count((array)$this->_vars['languages'])): foreach ((array)$this->_vars['languages'] as $this->_vars['key'] => $this->_vars['item']): ?>
		<div class="row">
			<div class="h"><?php echo l('field_keyword', 'seo', '', 'text', array()); ?>(<?php echo $this->_vars['item']['name']; ?>
): </div>
			<div class="v"><textarea name="keyword[<?php echo $this->_vars['key']; ?>
]" <?php if ($this->_vars['default_settings']['default_keyword']): ?>disabled<?php endif; ?> class="input-default-keyword"><?php echo $this->_vars['user_settings']['keyword'][$this->_vars['key']]; ?>
</textarea></div>
		</div>
		<?php endforeach; endif; ?>
		<br>
	
		<div class="row">
			<div class="h"><?php echo l('field_description_default', 'seo', '', 'text', array()); ?>: </div>
			<div class="v"><?php echo $this->_vars['default_settings']['description']; ?>
&nbsp;</div>
		</div>
		<div class="row zebra">
			<div class="h"><?php echo l('field_description_default_use', 'seo', '', 'text', array()); ?>: </div>
			<div class="v"><input type="checkbox" value="1" name="default_description" <?php if ($this->_vars['default_settings']['default_description']): ?>checked="checked"<?php endif; ?> id="default-description"></div>
		</div>
		<?php if (is_array($this->_vars['languages']) and count((array)$this->_vars['languages'])): foreach ((array)$this->_vars['languages'] as $this->_vars['key'] => $this->_vars['item']): ?>
		<div class="row">
			<div class="h"><?php echo l('field_description', 'seo', '', 'text', array()); ?>(<?php echo $this->_vars['item']['name']; ?>
): </div>
			<div class="v"><textarea name="description[<?php echo $this->_vars['key']; ?>
]" <?php if ($this->_vars['default_settings']['default_description']): ?>disabled<?php endif; ?> class="input-default-description"><?php echo $this->_vars['user_settings']['description'][$this->_vars['key']]; ?>
</textarea></div>
		</div>
		<?php endforeach; endif; ?>
		<div class="row zebra">
			<div class="h"><?php echo l('field_lang_in_url', 'seo', '', 'text', array()); ?>: </div>
			<div class="v">
				<input type="checkbox" value="1" name="lang_in_url" <?php if ($this->_vars['default_settings']['lang_in_url']): ?>checked="checked"<?php endif; ?> id="lang_in_url">
			</div>
		</div>
	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div></div>
	<a class="cancel" href="<?php echo $this->_vars['site_url']; ?>
admin/seo/index"><?php echo l('btn_cancel', 'start', '', 'text', array()); ?></a>
</form>
<div class="clr"></div>
<script>
<?php echo '
$(function(){
	$(\':checkbox\').bind(\'change\', function(){
		var id = $(this).attr(\'id\');
		if(this.checked){
			$(\'.input-\'+id).attr(\'disabled\', \'disabled\');
		}else{
			$(\'.input-\'+id).removeAttr("disabled");
		}
	});

});
'; ?>

</script>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>