<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2014-07-04 16:18:10 KRAT */ ?>

<div class="load_content_controller">
	<h1><?php echo l('header_listing_select', 'listings', '', 'text', array()); ?></h1>
	<div class="inside">	
		<?php if ($this->_vars['select_data']['max_select'] != 1): ?>
		<b><?php echo l('header_listings_selected', 'listings', '', 'text', array()); ?>:</b><br>
		<ul id="listing_selected_items" class="listing-items-selected">
		<?php if (is_array($this->_vars['select_data']['selected']) and count((array)$this->_vars['select_data']['selected'])): foreach ((array)$this->_vars['select_data']['selected'] as $this->_vars['item']): ?>
		<li><div class="listing-block"><input type="checkbox" name="remove_listings[]" value="<?php echo $this->_vars['item']['id']; ?>
" checked><?php echo $this->_vars['item']['output_name']; ?>
</div></li>
		<?php endforeach; endif; ?>
		</ul>
		<div class="clr"></div><br>
		<?php endif; ?>
		<b><?php echo l('header_listing_find', 'listings', '', 'text', array()); ?>:</b><br>
		<input type="text" id="listing_search" class="controller-search">
		<ul class="controller-items" id="listing_select_items"></ul>
	
		<div class="controller-actions">
			<div id="listing_page" class="fright"></div>
			<div><a href="#" id="listing_close_link"><?php echo l('btn_close', 'start', '', 'text', array()); ?></a></div>
		</div>
	</div>
</div>
