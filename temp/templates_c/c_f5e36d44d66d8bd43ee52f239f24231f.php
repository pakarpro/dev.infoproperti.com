<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-03-02 15:54:36 KRAT */ ?>

<?php if (! $this->_vars['geomap_js_loaded'] && ! $this->_vars['only_load_content']): ?>
<link href="<?php echo $this->_vars['site_root']; ?>
application/modules/geomap/views/default/css/googlev3.css" rel="stylesheet" type="text/css">
<script src="http://maps.google.com/maps/api/js?v=3.8&libraries=places<?php if ($this->_vars['map_reg_key']): ?>&key=<?php echo $this->_vars['map_reg_key'];  endif; ?>&sensor=true"></script>
<?php echo tpl_function_js(array('module' => geomap,'file' => 'googlemapsv3.js'), $this); endif;  if (! $this->_vars['only_load_scripts']):  if (! $this->_vars['map_id']):  $this->assign('map_id', 'map_'.$this->_vars['rand']);  endif; ?>
<div id="<?php echo $this->_vars['map_id']; ?>
" class="<?php echo $this->_vars['view_settings']['class']; ?>
 map_container">&nbsp;<?php echo l('loading', 'geomap', '', 'text', array()); ?></div>
<?php if ($this->_vars['settings']['use_panorama'] && ! $this->_vars['view_settings']['panorama_container']): ?>
<div id="pano_container_<?php echo $this->_vars['rand']; ?>
" class="pano_container"></div>
<?php endif;  if ($this->_vars['settings']['use_router']): ?>
<div id="routes_<?php echo $this->_vars['rand']; ?>
" class="routes"></div>
<div id="route_links_<?php echo $this->_vars['rand']; ?>
" class="routes-btn">
	<a href="javascript:void(0);" id="add_route_btn_<?php echo $this->_vars['rand']; ?>
"><?php echo l('route_add', 'geomap', '', 'text', array()); ?></a>
	<a href="javascript:void(0);" id="remove_route_btn_<?php echo $this->_vars['rand']; ?>
" class="hide"><?php echo l('route_delete', 'geomap', '', 'text', array()); ?></a>
</div>
<?php endif; ?>
<script><?php echo '
var ';  echo $this->_vars['map_id'];  echo ';
$(function(){
	';  echo $this->_vars['map_id'];  echo ' = new GoogleMapsv3({
		'; ?>

		map_container: '<?php echo $this->_vars['map_id']; ?>
',
		default_zoom: <?php echo $this->_vars['settings']['zoom']; ?>
,
		<?php if ($this->_vars['settings']['view_type']): ?>default_map_type: <?php echo $this->_vars['settings']['view_type']; ?>
,<?php endif; ?>
		width: <?php echo $this->_vars['view_settings']['width']; ?>
,
		height: <?php echo $this->_vars['view_settings']['height']; ?>
,
		lat: <?php echo $this->_vars['settings']['lat']; ?>
,
		lon: <?php echo $this->_vars['settings']['lon']; ?>
,
		<?php if (! $this->_vars['view_settings']['disable_smart_zoom'] && $this->_vars['settings']['use_smart_zoom']): ?>use_smart_zoom: true,<?php endif; ?>
		<?php if ($this->_vars['settings']['use_type_selector']): ?>use_type_selector: true,<?php endif; ?>
		<?php if ($this->_vars['settings']['use_panorama'] && ! $this->_vars['view_settings']['panorama_disable']): ?>
		use_panorama: true,
		panorama_container: '<?php if ($this->_vars['view_settings']['panorama_container']):  echo $this->_vars['view_settings']['panorama_container'];  else: ?>pano_container_<?php echo $this->_vars['rand'];  endif; ?>',
		<?php endif; ?>
		<?php if ($this->_vars['settings']['use_searchbox']): ?>use_searchbox: true,<?php endif; ?>
		<?php if ($this->_vars['settings']['use_search_radius']): ?>use_search_radius: true,<?php endif; ?>
		<?php if ($this->_vars['settings']['use_show_details']): ?>use_show_details: true,<?php endif; ?>
		<?php if ($this->_vars['view_settings']['zoom_listener']): ?>zoom_listener: <?php echo $this->_vars['view_settings']['zoom_listener']; ?>
,<?php endif; ?>
		<?php if ($this->_vars['view_settings']['type_listener']): ?>type_listener: <?php echo $this->_vars['view_settings']['type_listener']; ?>
,<?php endif; ?>
		<?php if ($this->_vars['view_settings']['geocode_listener']): ?>geocode_listener: <?php echo $this->_vars['view_settings']['geocode_listener']; ?>
,<?php endif; ?>
		<?php if ($this->_vars['settings']['media']['icon']): ?>icon: '<?php echo $this->_vars['settings']['media']['icon']['thumbs']['small']; ?>
',<?php endif; ?>
		<?php if ($this->_vars['settings']['use_router']): ?>
		use_router: true,
		routes_container: 'routes_<?php echo $this->_vars['rand']; ?>
',
		<?php endif; ?>
		<?php if ($this->_vars['settings']['use_amenities'] && $this->_run_modifier($this->_vars['settings']['amenities'], 'count', 'PHP', 1)): ?>
		use_amenities: true,
		amenities: ['<?php echo $this->_run_modifier("','", 'implode', 'PHP', 1, $this->_vars['settings']['amenities']); ?>
'],
		amenities_names: <?php echo $this->_vars['amenities_names_str']; ?>
,
		<?php endif; ?>
		<?php echo '
	});
	
	';  if ($this->_vars['settings']['use_router']):  echo '
	$(\'#add_route_btn_';  echo $this->_vars['rand'];  echo '\').bind(\'click\', function(){
		$(\'#add_route_btn_';  echo $this->_vars['rand'];  echo '\').hide()
		$(\'#remove_route_btn_';  echo $this->_vars['rand'];  echo '\').show();
		';  echo $this->_vars['map_id'];  echo '.createRoute();
	});
	
	$(\'#remove_route_btn_';  echo $this->_vars['rand'];  echo '\').bind(\'click\', function(){
		$(\'#remove_route_btn_';  echo $this->_vars['rand'];  echo '\').hide()
		$(\'#add_route_btn_';  echo $this->_vars['rand'];  echo '\').show();
		';  echo $this->_vars['map_id'];  echo '.deleteRoute();
	});
	';  endif;  echo '
	
	';  if (is_array($this->_vars['markers']) and count((array)$this->_vars['markers'])): foreach ((array)$this->_vars['markers'] as $this->_vars['item']):  echo '
		';  echo $this->_vars['map_id'];  echo '.addMarker(';  echo $this->_vars['item']['lat'];  echo ', ';  echo $this->_vars['item']['lon'];  echo ', {
			'; ?>

			<?php if ($this->_vars['item']['gid']): ?>gid: '<?php echo $this->_vars['item']['gid']; ?>
',<?php endif; ?>
			<?php if ($this->_vars['item']['dragging']): ?>draggable: true, 
			<?php if ($this->_vars['view_settings']['drag_listener']): ?>drag_listener: <?php echo $this->_vars['view_settings']['drag_listener']; ?>
,<?php endif; ?>
			<?php endif; ?>
			<?php if ($this->_vars['item']['info']): ?>info: '<?php $this->_tag_stack[] = array('tpl_block_strip', array()); tpl_block_strip(array(), null, $this); ob_start();  echo $this->_run_modifier('\'', 'str_replace', 'PHP', 1, '\\\'', $this->_vars['item']['info']);  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_strip($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>',<?php endif; ?>
			<?php echo '
		});
	';  endforeach; endif;  echo '
});
'; ?>
</script>
<?php endif; ?>
