<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.json_encode.php'); $this->register_function("json_encode", "tpl_function_json_encode");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/function.counter.php'); $this->register_function("counter", "tpl_function_counter");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-03-17 16:26:03 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array('load_type' => 'ui|editable'));
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<form method="post" action="<?php echo $this->_vars['data']['action']; ?>
" name="save_form">
	<div class="edit-form n150">
		<div class="row header"><?php echo l('admin_header_seo_settings_editing', 'seo', '', 'text', array()); ?></div>
		<div class="row">
			<div class="h"><?php echo l('field_default_link', 'seo', '', 'text', array()); ?>: </div>
			<div class="v"><?php echo $this->_vars['site_url'];  echo $this->_vars['module_gid']; ?>
/<?php echo $this->_vars['method'];  if (is_array($this->_vars['default_settings']['url_vars']) and count((array)$this->_vars['default_settings']['url_vars'])): foreach ((array)$this->_vars['default_settings']['url_vars'] as $this->_vars['key'] => $this->_vars['item']): ?>/[<?php echo $this->_vars['key']; ?>
]<?php endforeach; endif; ?></div>
		</div>
		<div class="row zebra">
			<div class="h"><?php echo l('url_manager', 'seo', '', 'text', array()); ?>: </div>
			<div class="v">
				<input type="hidden" name="url_template_data" id="url_data" value="">
				<ul class="url-creator" id="url_block"></ul>
				<div class="clr"></div>
				<div id="url_text_edit" class="url-form hide">
					<b><?php echo l('action_edit_url_block', 'seo', '', 'text', array()); ?>:</b>
					<div class="row zebra">
						<div class="h"><?php echo l('field_url_block_type', 'seo', '', 'text', array()); ?>:</div>
						<div class="v"><?php echo l('field_url_block_type_text', 'seo', '', 'text', array()); ?></div>
					</div>
					<div class="row zebra">
						<div class="h"><?php echo l('field_url_block_value', 'seo', '', 'text', array()); ?></div>
						<div class="v"><input type="text" value="" id="text_block_value_edit"></div>
					</div>
					<div class="row zebra">
						<div class="h">&nbsp;</div>
						<div class="v">
							<input type="button" id="text_block_save" name="add-block" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>">
							<input type="button" id="text_block_delete" name="delete-block" value="<?php echo l('btn_delete', 'start', '', 'button', array()); ?>">
						</div>
					</div>
				</div>
				<br>
				<div id="url_tpl_edit" class="url-form hide">
					<b><?php echo l('action_edit_url_block', 'seo', '', 'text', array()); ?>:</b>
					<div class="row zebra">
						<div class="h"><?php echo l('field_url_block_type', 'seo', '', 'text', array()); ?>:</div>
						<div class="v"><?php echo l('field_url_block_type_tpl', 'seo', '', 'text', array()); ?></div>
					</div>
					<div class="row zebra">
						<div class="h"><?php echo l('field_url_block_replacement', 'seo', '', 'text', array()); ?>:</div>
						<div class="v" id="tpl_block_var_name">&nbsp;</div>
					</div>
					<div class="row zebra">
						<div class="h"><?php echo l('field_url_block_default', 'seo', '', 'text', array()); ?>:</div>
						<div class="v"><input type="text" value="" id="tpl_block_var_default"></div>
					</div>
					<div class="row zebra">
						<div class="h">&nbsp;</div>
						<div class="v">
							<input type="button" id="tpl_block_save" name="add-block" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>">
							<input type="button" id="tpl_block_delete" name="delete-block" value="<?php echo l('btn_delete', 'start', '', 'button', array()); ?>">
						</div>
					</div>
				</div>

				<div class="url-form">
					<div class="row">
						<div class="h"><?php echo l('link_add_url_block_text', 'seo', '', 'text', array()); ?>:</div>
						<div class="v"><input type="text" id="text_block_value"> <input type="button" name="add-block" value="<?php echo l('btn_add', 'start', '', 'button', array()); ?>"  onclick="javascript: urlCreator.save_block('', 'text', $('#text_block_value').val(), '', '', '');"></div>
					</div>
				</div>
				<?php if ($this->_vars['default_settings']['url_vars']): ?>
				<div class="url-form">
					<div class="row">
						<div class="h"><?php echo l('link_add_url_block_tpl', 'seo', '', 'text', array()); ?>:</div>
						<div class="v">
						<?php if (is_array($this->_vars['default_settings']['url_vars']) and count((array)$this->_vars['default_settings']['url_vars'])): foreach ((array)$this->_vars['default_settings']['url_vars'] as $this->_vars['key'] => $this->_vars['item']):  if ($this->_vars['defcounter'] > 0): ?><br><span class="or"><?php echo l('link_or', 'seo', '', 'text', array()); ?></span><br><?php endif; ?>
						<?php echo tpl_function_counter(array('id' => 'mandatory','print' => false,'assign' => defcounter), $this);?>
						[<select id="var-<?php echo $this->_vars['key']; ?>
"><?php if (is_array($this->_vars['item']) and count((array)$this->_vars['item'])): foreach ((array)$this->_vars['item'] as $this->_vars['varname'] => $this->_vars['type']): ?><option value="<?php echo $this->_vars['type']; ?>
"><?php echo $this->_vars['varname']; ?>
</option><?php endforeach; endif; ?></select>|<input type="text" class="short" id="vardef-<?php echo $this->_vars['key']; ?>
">]
						<input type="button" name="add-block" value="<?php echo l('btn_add', 'start', '', 'button', array()); ?>" onclick="javascript: urlCreator.save_block('', 'tpl', $('#vardef-<?php echo $this->_vars['key']; ?>
').val(), '<?php echo $this->_vars['defcounter']; ?>
', $('#var-<?php echo $this->_vars['key']; ?>
').val(), $('#var-<?php echo $this->_vars['key']; ?>
 > option:selected').text());">
						<?php endforeach; endif; ?>
						</div>
					</div>
				</div>
				<?php endif; ?>
				<?php if ($this->_vars['default_settings']['optional']): ?>
				<div class="url-form">
					<div class="row">
						<div class="h"><?php echo l('link_add_url_block_opt', 'seo', '', 'text', array()); ?>:</div>
						<div class="v">
						<?php if (is_array($this->_vars['default_settings']['optional']) and count((array)$this->_vars['default_settings']['optional'])): foreach ((array)$this->_vars['default_settings']['optional'] as $this->_vars['key'] => $this->_vars['item']):  if ($this->_vars['optcounter'] > 0): ?><br><span class="or"><?php echo l('link_or', 'seo', '', 'text', array()); ?></span><br><?php endif; ?>
						<?php echo tpl_function_counter(array('id' => 'optional','print' => false,'assign' => optcounter), $this);?>
						[<select id="opt-<?php echo $this->_vars['key']; ?>
"><?php if (is_array($this->_vars['item']) and count((array)$this->_vars['item'])): foreach ((array)$this->_vars['item'] as $this->_vars['varname'] => $this->_vars['type']): ?><option value="<?php echo $this->_vars['type']; ?>
"><?php echo $this->_vars['varname']; ?>
</option><?php endforeach; endif; ?></select>|<input type="text" class="short" id="optdef-<?php echo $this->_vars['key']; ?>
">]
						<input type="button" name="add-block" value="<?php echo l('btn_add', 'start', '', 'button', array()); ?>" onclick="javascript: urlCreator.save_block('', 'opt', $('#optdef-<?php echo $this->_vars['key']; ?>
').val(), '<?php echo $this->_vars['optcounter']; ?>
', $('#opt-<?php echo $this->_vars['key']; ?>
').val(), $('#opt-<?php echo $this->_vars['key']; ?>
 > option:selected').text());">
						<?php endforeach; endif; ?>
						</div>
					</div>
				</div>
				<?php endif; ?>
			</div>
			<div class="clr"></div>
		</div>

		<div class="row">
			<div class="h">&nbsp;</div>
			<div class="v"><?php echo l('url_manager_text', 'seo', '', 'text', array()); ?></div>
		</div>

	</div>
	<br>

	<div class="edit-form n150">
		<?php if ($this->_vars['default_settings']['templates']): ?>
		<div class="row zebra">
			<div class="h"><?php echo l('field_templates', 'seo', '', 'text', array()); ?>: </div>
			<div class="v">
				<?php if (is_array($this->_vars['default_settings']['templates']) and count((array)$this->_vars['default_settings']['templates'])): foreach ((array)$this->_vars['default_settings']['templates'] as $this->_vars['item']): ?><b>[<?php echo $this->_vars['item']; ?>
<span class="hide_text">|<?php echo l('default_value', 'seo', '', 'text', array()); ?></span>]</b> <?php endforeach; endif; ?><br><br><i><?php echo l('field_templates_text', 'seo', '', 'text', array()); ?></i>
			</div>
		</div>
		<br>
		<?php endif; ?>
		<div class="row">
			<div class="h"><?php echo l('field_title_default', 'seo', '', 'text', array()); ?>: </div>
			<div class="v">
			<?php if (is_array($this->_vars['languages']) and count((array)$this->_vars['languages'])): foreach ((array)$this->_vars['languages'] as $this->_vars['key'] => $this->_vars['item']): ?>
				<b><?php echo $this->_vars['item']['name']; ?>
:</b> <?php echo $this->_vars['default_settings']['title'][$this->_vars['key']]; ?>
<br>
			<?php endforeach; endif; ?>&nbsp;
			</div>
		</div>
		<div class="row zebra">
			<div class="h"><?php echo l('field_title_default_use', 'seo', '', 'text', array()); ?>: </div>
			<div class="v"><input type="checkbox" value="1" name="default_title" <?php if ($this->_vars['default_settings']['default_title']): ?>checked<?php endif; ?> id="default_title" class="checked-tags" checked-param="title"></div>
		</div>
		<?php if (is_array($this->_vars['languages']) and count((array)$this->_vars['languages'])): foreach ((array)$this->_vars['languages'] as $this->_vars['key'] => $this->_vars['item']): ?>
		<div class="row">
			<div class="h"><?php echo l('field_title', 'seo', '', 'text', array()); ?>(<?php echo $this->_vars['item']['name']; ?>
): </div>
			<div class="v"><input type="text" value="<?php echo $this->_vars['user_settings']['title'][$this->_vars['key']]; ?>
" name="title[<?php echo $this->_vars['key']; ?>
]" <?php if ($this->_vars['default_settings']['default_title']): ?>disabled<?php endif; ?> class="long input-title"></div>
		</div>
		<?php endforeach; endif; ?>
		<br>

		<div class="row">
			<div class="h"><?php echo l('field_keyword_default', 'seo', '', 'text', array()); ?>: </div>
			<div class="v">
			<?php if (is_array($this->_vars['languages']) and count((array)$this->_vars['languages'])): foreach ((array)$this->_vars['languages'] as $this->_vars['key'] => $this->_vars['item']): ?>
				<b><?php echo $this->_vars['item']['name']; ?>
:</b> <?php echo $this->_vars['default_settings']['keyword'][$this->_vars['key']]; ?>
<br>
			<?php endforeach; endif; ?>&nbsp;
			</div>
		</div>
		<div class="row zebra">
			<div class="h"><?php echo l('field_keyword_default_use', 'seo', '', 'text', array()); ?>: </div>
			<div class="v"><input type="checkbox" value="1" name="default_keyword" <?php if ($this->_vars['default_settings']['default_keyword']): ?>checked<?php endif; ?> id="default_keyword"  class="checked-tags" checked-param="keyword"></div>
		</div>
		<?php if (is_array($this->_vars['languages']) and count((array)$this->_vars['languages'])): foreach ((array)$this->_vars['languages'] as $this->_vars['key'] => $this->_vars['item']): ?>
		<div class="row">
			<div class="h"><?php echo l('field_keyword', 'seo', '', 'text', array()); ?>(<?php echo $this->_vars['item']['name']; ?>
): </div>
			<div class="v"><textarea name="keyword[<?php echo $this->_vars['key']; ?>
]" <?php if ($this->_vars['default_settings']['default_keyword']): ?>disabled<?php endif; ?> class="input-keyword"><?php echo $this->_vars['user_settings']['keyword'][$this->_vars['key']]; ?>
</textarea></div>
		</div>
		<?php endforeach; endif; ?>
		<br>
	
		<div class="row">
			<div class="h"><?php echo l('field_description_default', 'seo', '', 'text', array()); ?>: </div>
			<div class="v">
			<?php if (is_array($this->_vars['languages']) and count((array)$this->_vars['languages'])): foreach ((array)$this->_vars['languages'] as $this->_vars['key'] => $this->_vars['item']): ?>
				<b><?php echo $this->_vars['item']['name']; ?>
:</b> <?php echo $this->_vars['default_settings']['description'][$this->_vars['key']]; ?>
<br>
			<?php endforeach; endif; ?>&nbsp;
			</div>
		</div>
		<div class="row zebra">
			<div class="h"><?php echo l('field_description_default_use', 'seo', '', 'text', array()); ?>: </div>
			<div class="v"><input type="checkbox" value="1" name="default_description" <?php if ($this->_vars['default_settings']['default_description']): ?>checked<?php endif; ?> id="default_description" class="checked-tags" checked-param="description"></div>
		</div>
		<?php if (is_array($this->_vars['languages']) and count((array)$this->_vars['languages'])): foreach ((array)$this->_vars['languages'] as $this->_vars['key'] => $this->_vars['item']): ?>
		<div class="row">
			<div class="h"><?php echo l('field_description', 'seo', '', 'text', array()); ?>(<?php echo $this->_vars['item']['name']; ?>
): </div>
			<div class="v"><textarea name="description[<?php echo $this->_vars['key']; ?>
]" <?php if ($this->_vars['default_settings']['default_description']): ?>disabled<?php endif; ?> class="input-description"><?php echo $this->_vars['user_settings']['description'][$this->_vars['key']]; ?>
</textarea></div>
		</div>
		<?php endforeach; endif; ?>
		<br>
	
		<div class="row">
			<div class="h"><?php echo l('field_header_default', 'seo', '', 'text', array()); ?>: </div>
			<div class="v">
			<?php if (is_array($this->_vars['languages']) and count((array)$this->_vars['languages'])): foreach ((array)$this->_vars['languages'] as $this->_vars['key'] => $this->_vars['item']): ?>
				<b><?php echo $this->_vars['item']['name']; ?>
:</b> <?php echo $this->_vars['default_settings']['header'][$this->_vars['key']]; ?>
<br>
			<?php endforeach; endif; ?>&nbsp;
			</div>
		</div>
		<div class="row zebra">
			<div class="h"><?php echo l('field_header_default_use', 'seo', '', 'text', array()); ?>: </div>
			<div class="v"><input type="checkbox" value="1" name="default_header" <?php if ($this->_vars['default_settings']['default_header']): ?>checked<?php endif; ?> id="default_header" class="checked-tags" checked-param="header"></div>
		</div>
		<?php if (is_array($this->_vars['languages']) and count((array)$this->_vars['languages'])): foreach ((array)$this->_vars['languages'] as $this->_vars['key'] => $this->_vars['item']): ?>
		<div class="row">
			<div class="h"><?php echo l('field_header', 'seo', '', 'text', array()); ?>(<?php echo $this->_vars['item']['name']; ?>
): </div>
			<div class="v"><input type="text" name="header[<?php echo $this->_vars['key']; ?>
]" <?php if ($this->_vars['default_settings']['default_header']): ?>disabled<?php endif; ?> class="long input-header" value="<?php echo $this->_vars['user_settings']['header'][$this->_vars['key']]; ?>
"></div>
		</div>
		<?php endforeach; endif; ?>

	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div></div>
	<a class="cancel" href="<?php echo $this->_vars['site_url']; ?>
admin/seo/listing/<?php echo $this->_vars['module_gid']; ?>
"><?php echo l('btn_cancel', 'start', '', 'text', array()); ?></a>
</form>
<div class="clr"></div>
<script>
<?php echo '
	var urlCreator;

	$(function(){
		urlCreator = new seoUrlCreator({
			siteUrl: \'';  echo $this->_vars['site_url'];  echo '\', 
			data: ';  echo tpl_function_json_encode(array('data' => $this->_vars['default_settings']['url_template_data']), $this); echo '
		});

		$(\'.checked-tags\').bind(\'click\', function(){
			if($(this).is(\':checked\')){
				$(\'.input-\'+$(this).attr(\'checked-param\')).attr(\'disabled\', \'disabled\');
			}else{
				$(\'.input-\'+$(this).attr(\'checked-param\')).removeAttr("disabled");
			}
		});
	});
'; ?>
</script>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
