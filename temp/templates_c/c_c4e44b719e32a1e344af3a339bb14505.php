<?php require_once('/opt/ip/system/libraries/template_lite/plugins/function.js.php'); $this->register_function("js", "tpl_function_js");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.ld_option.php'); $this->register_function("ld_option", "tpl_function_ld_option");  require_once('/opt/ip/system/libraries/template_lite/plugins/modifier.date_format.php'); $this->register_modifier("date_format", "tpl_modifier_date_format");  require_once('/opt/ip/system/libraries/template_lite/plugins/modifier.truncate.php'); $this->register_modifier("truncate", "tpl_modifier_truncate");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.counter.php'); $this->register_function("counter", "tpl_function_counter");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.user_select.php'); $this->register_function("user_select", "tpl_function_user_select");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.block.php'); $this->register_function("block", "tpl_function_block");  require_once('/opt/ip/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  require_once('/opt/ip/system/libraries/template_lite/plugins/function.helper.php'); $this->register_function("helper", "tpl_function_helper");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2015-08-18 11:46:02 KRAT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array('load_type' => 'ui'));
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
  echo tpl_function_helper(array('func_name' => get_admin_level1_menu,'helper_name' => menu,'func_param' => 'admin_listings_menu'), $this);?>
<div class="actions">
	&nbsp;
</div>

<div class="menu-level3">
	<ul>
		<li class="<?php if ($this->_vars['status'] == 'wait'): ?>active<?php endif;  if (! $this->_vars['status_data']['wait']): ?> hide<?php endif; ?>"><a href="<?php echo $this->_vars['site_url']; ?>
admin/listings/orders/wait"><?php echo l('booking_status_wait', 'listings', '', 'text', array()); ?> (<?php echo $this->_vars['status_data']['wait']; ?>
)</a></li>
		<li class="<?php if ($this->_vars['status'] == 'approve'): ?>active<?php endif;  if (! $this->_vars['status_data']['approve']): ?> hide<?php endif; ?>"><a href="<?php echo $this->_vars['site_url']; ?>
admin/listings/orders/approve"><?php echo l('booking_status_approved', 'listings', '', 'text', array()); ?> (<?php echo $this->_vars['status_data']['approve']; ?>
)</a></li>
		<li class="<?php if ($this->_vars['status'] == 'decline'): ?>active<?php endif;  if (! $this->_vars['status_data']['decline']): ?> hide<?php endif; ?>"><a href="<?php echo $this->_vars['site_url']; ?>
admin/listings/orders/decline"><?php echo l('booking_status_declined', 'listings', '', 'text', array()); ?> (<?php echo $this->_vars['status_data']['decline']; ?>
)</a></li>		
	</ul>
	&nbsp;
</div>


<form method="post" action="" name="save_form" enctype="multipart/form-data">
<div class="filter-form">
	<div class="form">
			<div class="row">
				<div class="h"><?php echo l('field_booking_listing', 'listings', '', 'text', array()); ?>:</div>
				<div class="v"><?php echo tpl_function_block(array('name' => 'listing_select','module' => 'listings','selected' => $this->_vars['filters']['listings'],'max' => 10,'var_name' => 'id_listing'), $this);?></div>
			</div>
			<div class="row">
				<div class="h"><?php echo l('field_owner', 'listings', '', 'text', array()); ?>:</div>
				<div class="v"><?php echo tpl_function_user_select(array('selected' => $this->_vars['filters']['owners'],'max' => 1,'var_name' => 'id_owner'), $this);?></div>
			</div>
			<div class="row">
				<div class="h"><?php echo l('field_booking_user', 'listings', '', 'text', array()); ?>:</div>
				<div class="v"><?php echo tpl_function_user_select(array('selected' => $this->_vars['filters']['users'],'max' => 1,'var_name' => 'id_user'), $this);?></div>
			</div>
			<div class="row">
				<div class="h">
					<input type="submit" name="filter-submit" value="<?php echo l('header_filters', 'listings', '', 'button', array()); ?>">
					<input type="submit" name="filter-reset" value="<?php echo l('header_reset', 'listings', '', 'button', array()); ?>">
				</div>
			</div>
	</div>
</div>
</form>


<form id="listings_form" action="" method="post">
<table cellspacing="0" cellpadding="0" class="data" width="100%">
<tr>
	<th class="w200"><?php echo l('field_booking_listing', 'listings', '', 'text', array()); ?></th>
	<th class="w200"><?php echo l('field_booking_user', 'listings', '', 'text', array()); ?></th>
	<th class="w100"><?php echo l('field_booking_date_start', 'listings', '', 'text', array()); ?></th>
	<th class="w100"><?php echo l('field_booking_date_end', 'listings', '', 'text', array()); ?></th>
	<th class="w150">&nbsp;</th>
</tr>
<?php if (is_array($this->_vars['orders']) and count((array)$this->_vars['orders'])): foreach ((array)$this->_vars['orders'] as $this->_vars['item']):  echo tpl_function_counter(array('print' => false,'assign' => counter), $this);?>
<tr>
	<td><?php echo $this->_run_modifier($this->_vars['item']['listing']['output_name'], 'truncate', 'plugin', 1, 100); ?>
</td>
	<td><?php echo $this->_run_modifier($this->_vars['item']['user']['output_name'], 'truncate', 'plugin', 1, 100); ?>
</td>
	<td class="center">
					<?php switch($this->_vars['item']['listing']['price_period']): case '1':  ?>
				<?php echo $this->_run_modifier($this->_vars['item']['date_start'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']); ?>

			<?php break; case '2':  ?>
				<?php echo tpl_function_ld_option(array('i' => 'month-names','gid' => 'start','option' => $this->_run_modifier($this->_vars['item']['date_start'], 'date_format', 'plugin', 1, '%m')), $this);?>
				<?php echo $this->_run_modifier($this->_vars['item']['date_start'], 'date_format', 'plugin', 1, '%Y'); ?>

		<?php break; endswitch; ?>
	</td>
	<td class="center">
					<?php switch($this->_vars['item']['listing']['price_period']): case '1':  ?>
				<?php echo $this->_run_modifier($this->_vars['item']['date_end'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']); ?>

			<?php break; case '2':  ?>
				<?php echo tpl_function_ld_option(array('i' => 'month-names','gid' => 'start','option' => $this->_run_modifier($this->_vars['item']['date_end'], 'date_format', 'plugin', 1, '%m')), $this);?>
				<?php echo $this->_run_modifier($this->_vars['item']['date_end'], 'date_format', 'plugin', 1, '%Y'); ?>

		<?php break; endswitch; ?>
	</td>
	<td class="icons">
		<?php if ($this->_vars['item']['status'] == 'wait'): ?>
		<a href="<?php echo $this->_vars['site_url']; ?>
admin/listings/order_approve/<?php echo $this->_vars['item']['id']; ?>
"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-approve.png" width="16" height="16" border="0" alt="<?php echo l('link_order_approve', 'listings', '', 'button', array()); ?>" title="<?php echo l('link_order_approve', 'listings', '', 'button', array()); ?>"></a>
		<a href="<?php echo $this->_vars['site_url']; ?>
admin/listings/order_decline/<?php echo $this->_vars['item']['id']; ?>
"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-decline.png" width="16" height="16" border="0" alt="<?php echo l('link_order_decline', 'listings', '', 'button', array()); ?>" title="<?php echo l('link_order_decline', 'listings', '', 'button', array()); ?>"></a>
		<?php endif; ?>
		<a href="<?php echo $this->_vars['site_url']; ?>
admin/listings/order_delete/<?php echo $this->_vars['item']['id']; ?>
" onclick="javascript: if(!confirm('<?php echo l('note_delete_listing', 'listings', '', 'js', array()); ?>')) return false;"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-delete.png" width="16" height="16" border="0" alt="<?php echo l('link_delete_listing', 'listings', '', 'button', array()); ?>" title="<?php echo l('link_delete_listing', 'listings', '', 'button', array()); ?>"></a>
	</td>
</tr>
<?php endforeach; else: ?>
<tr><td colspan="6" class="center"><?php echo l('no_listings', 'listings', '', 'text', array()); ?></td></tr>
<?php endif; ?>
</table>
</form>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "pagination.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

<?php echo tpl_function_js(array('file' => 'easyTooltip.min.js'), $this);?>
<script><?php echo '
var reload_link = "';  echo $this->_vars['site_url']; ?>
admin/listings<?php echo '";
var order = \'';  echo $this->_vars['order'];  echo '\';
var loading_content;
var order_direction = \'';  echo $this->_vars['order_direction'];  echo '\';
$(function(){
	$(".tooltip").each(function(){
		$(this).easyTooltip({
			useElement: \'span_\'+$(this).attr(\'id\')
		});
	});
});
function reload_this_page(value){
	var link = reload_link + \'/\' + value + \'/\' + order + \'/\' + order_direction;
	location.href=link;
}

'; ?>
</script>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
