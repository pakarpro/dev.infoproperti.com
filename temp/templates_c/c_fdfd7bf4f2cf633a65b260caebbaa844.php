<?php require_once('/home/demo/install/main/system/libraries/template_lite/plugins/modifier.escape.php'); $this->register_modifier("escape", "tpl_modifier_escape");  require_once('/home/demo/install/main/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2014-03-17 09:33:32 CXT */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<form method="post" action="<?php echo $this->_vars['data']['action']; ?>
" name="save_form" enctype="multipart/form-data">
	<div class="edit-form n150">
		<div class="row header"><?php if ($this->_vars['data']['id']):  echo l('admin_header_currency_change', 'payments', '', 'text', array());  else:  echo l('admin_header_currency_add', 'payments', '', 'text', array());  endif; ?></div>
		<div class="row">
			<div class="h"><?php echo l('field_currency_gid', 'payments', '', 'text', array()); ?>:&nbsp;* </div>
			<div class="v"><input type="text" id="gid" name="gid" value="<?php echo $this->_vars['data']['gid']; ?>
" class="short"></div>
		</div>
		<div class="row">
			<div class="h"><?php echo l('field_currency_name', 'payments', '', 'text', array()); ?>:&nbsp;* </div>
			<div class="v"><input type="text" id="name" name="name" value="<?php echo $this->_vars['data']['name']; ?>
"></div>
		</div>
		<div class="row">
			<div class="h"><?php echo l('field_currency_abbr', 'payments', '', 'text', array()); ?>:&nbsp;* </div>
			<div class="v"><input type="text" id="abbr" name="abbr" value="<?php echo $this->_vars['data']['abbr']; ?>
" class="short"></div>
		</div>
		<?php if ($this->_vars['data']['id'] && $this->_vars['data']['gid'] != $this->_vars['base_currency']): ?>
		<div class="row">
			<div class="h"><?php echo l('field_per_base', 'payments', '', 'text', array()); ?> <?php echo $this->_vars['base_currency']; ?>
:</div>
			<div class="v"><input type="text" id="per_base" name="per_base" value="<?php echo $this->_run_modifier($this->_vars['data']['per_base'], 'escape', 'plugin', 1); ?>
"></div>
		</div>
		<?php endif; ?>
		<div class="row">
			<div class="h"><?php echo l('field_decimal_separator', 'payments', '', 'text', array()); ?>:&nbsp;*</div>
			<div class="v">
				<?php if (is_array($this->_vars['format']['dec_sep']) and count((array)$this->_vars['format']['dec_sep'])): foreach ((array)$this->_vars['format']['dec_sep'] as $this->_vars['key'] => $this->_vars['dec_sep']): ?>
					<span><input id="dec_sep_<?php echo $this->_vars['key']; ?>
" value="<?php echo $this->_vars['dec_sep']; ?>
" name="dec_sep" type="radio" <?php if (! $this->_vars['format']['used']['dec_sep'] || $this->_vars['format']['used']['dec_sep'] == $this->_vars['dec_sep']): ?>checked="checked"<?php endif; ?> /><label for="dec_sep_<?php echo $this->_vars['key']; ?>
"><?php echo l('field_decimal_separator_'.$this->_vars['key'], 'payments', '', 'text', array()); ?></label></span>
				<?php endforeach; endif; ?>
			</div>
		</div>
		<div class="row">
			<div class="h"><?php echo l('field_groups_separator', 'payments', '', 'text', array()); ?>:&nbsp;*</div>
			<div class="v">
				<?php if (is_array($this->_vars['format']['gr_sep']) and count((array)$this->_vars['format']['gr_sep'])): foreach ((array)$this->_vars['format']['gr_sep'] as $this->_vars['key'] => $this->_vars['gr_sep']): ?>
					<span><input id="gr_sep_<?php echo $this->_vars['key']; ?>
" value="<?php echo $this->_vars['gr_sep']; ?>
" name="gr_sep" type="radio" <?php if (! $this->_vars['format']['used']['gr_sep'] || $this->_vars['format']['used']['gr_sep'] == $this->_vars['gr_sep']): ?>checked="checked"<?php endif; ?> /><label for="gr_sep_<?php echo $this->_vars['key']; ?>
"><?php echo l('field_groups_separator_'.$this->_vars['key'], 'payments', '', 'text', array()); ?></label></span>
				<?php endforeach; endif; ?>
			</div>
		</div>
		<div class="row">
			<div class="h"><?php echo l('field_decimal_part', 'payments', '', 'text', array()); ?>:&nbsp;*</div>
			<div class="v">
				<?php if (is_array($this->_vars['format']['dec_part']) and count((array)$this->_vars['format']['dec_part'])): foreach ((array)$this->_vars['format']['dec_part'] as $this->_vars['key'] => $this->_vars['dec_part']): ?>
					<span><input id="dec_part_<?php echo $this->_vars['key']; ?>
" value="<?php echo $this->_vars['dec_part']; ?>
" name="dec_part" type="radio" <?php if (! $this->_vars['format']['used']['dec_part'] || $this->_vars['format']['used']['dec_part'] == $this->_vars['dec_part']): ?>checked="checked"<?php endif; ?> /><label for="dec_part_<?php echo $this->_vars['key']; ?>
"><?php echo l('field_decimal_part_'.$this->_vars['key'], 'payments', '', 'text', array()); ?></label></span>
				<?php endforeach; endif; ?>
			</div>
		</div>
		<div class="row">
			<div class="h"><?php echo l('field_currency_format', 'payments', '', 'text', array()); ?>:&nbsp;*</div>
			<div class="v" id="templates">
				<?php if (is_array($this->_vars['format']['template']) and count((array)$this->_vars['format']['template'])): foreach ((array)$this->_vars['format']['template'] as $this->_vars['key'] => $this->_vars['template_cur']): ?>
					<span><input type="radio" name="template" id="template_<?php echo $this->_vars['key']; ?>
" value="<?php echo $this->_vars['template_cur']; ?>
" <?php if (! $this->_vars['format']['used']['template'] || $this->_vars['format']['used']['template'] == $this->_vars['template_cur']): ?>checked="checked"<?php endif; ?> /><label for="template_<?php echo $this->_vars['key']; ?>
"></label></span>
				<?php endforeach; endif; ?>
			</div>
		</div>		
	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div></div>
	<a class="cancel" href="<?php echo $this->_vars['site_url']; ?>
admin/payments/settings"><?php echo l('btn_cancel', 'start', '', 'text', array()); ?></a>
</form>
<div class="clr"></div>
<script><?php echo '
$(function(){
	new adminPayments();
});
'; ?>
</script>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
