<?php require_once('/opt/ip/system/libraries/template_lite/plugins/compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-02-29 10:04:30 KRAT */ ?>

<div class="load_content_controller">
	<h1>
	<?php if ($this->_vars['type'] == 'country'):  echo l('header_country_select', 'countries', '', 'text', array()); ?>
	<?php elseif ($this->_vars['type'] == 'region'):  echo l('header_region_select', 'countries', '', 'text', array()); ?>
	<?php elseif ($this->_vars['type'] == 'city'):  echo l('header_city_select', 'countries', '', 'text', array()); ?>
	<?php endif; ?></h1>
	<div class="inside">
	<?php if ($this->_vars['type'] == 'region'): ?><div class="crumb"><?php echo $this->_vars['data']['country']['name']; ?>
</div>
	<?php elseif ($this->_vars['type'] == 'city'): ?><div class="crumb"><?php echo $this->_vars['data']['country']['name']; ?>
 > <?php echo $this->_vars['data']['region']['name']; ?>
</div>
	<?php endif; ?>
	<?php if ($this->_vars['type'] == 'city'): ?><input type="text" id="city_search" class="controller-search"><?php endif; ?>
		<ul class="controller-items" id="country_select_items"></ul>
	
		<div class="controller-actions">
			<div id="city_page" class="fright"></div>
			<div>
			<?php if ($this->_vars['type'] == 'region'): ?><a href="#" id="country_select_back" class="btn-link"><ins class="with-icon i-larr no-hover"></ins><?php echo l('link_select_another_country', 'countries', '', 'text', array()); ?></a>
			<?php elseif ($this->_vars['type'] == 'city'): ?><a href="#" id="country_select_back" class="btn-link"><ins class="with-icon i-larr no-hover"></ins><?php echo l('link_select_another_region', 'countries', '', 'text', array()); ?></a>
			<?php endif; ?>
			</div>
			<div class="fright">
				<a href="javascript:void(0);" id="country_select_close" class="btn-link"><ins class="with-icon i-larr no-hover"></ins><?php echo l('link_close', 'countries', '', 'text', array()); ?></a>
			</div>
		</div>

	</div>
</div>
