<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

//core
//$product_libraries[] = "Openid";	// Don't delete
$product_libraries[] = "Cronparser";
$product_libraries[] = "Whois";
$product_libraries[] = "Googlepr";
$product_libraries[] = "Simplepie";
$product_libraries[] = "Rssfeed";
$product_libraries[] = "Zend";
$product_libraries[] = "VideoEmbed";
$product_libraries[] = "dompdf";
$product_libraries[] = "Translit";
$product_libraries[] = "Array2XML";
$product_libraries[] = "simple_html_dom";
$product_libraries[] = "qrcode";
$product_libraries[] = "jwt";

$product_modules[] = "menu";
$product_modules[] = "start";
$product_modules[] = "ausers";
$product_modules[] = "notifications";
$product_modules[] = "seo";
$product_modules[] = "linker";
$product_modules[] = "site_map";
$product_modules[] = "cronjob";
$product_modules[] = "social_networking";
$product_modules[] = "users_connections";
$product_modules[] = "dynamic_blocks";
$product_modules[] = "uploads";
$product_modules[] = "field_editor";
$product_modules[] = "file_uploads";
$product_modules[] = "video_uploads";
$product_modules[] = "properties";

$product_modules[] = "contact_us";
$product_modules[] = "geomap";
$product_modules[] = "moderation";
$product_modules[] = "languages";
$product_modules[] = "themes";
$product_modules[] = "countries";
$product_modules[] = "users";

$product_modules[] = "payments";
$product_modules[] = "users_payments";
$product_modules[] = "services";
$product_modules[] = "users_services";

$product_modules[] = "content";
$product_modules[] = "subscriptions";
$product_modules[] = "banners";
$product_modules[] = "news";
$product_modules[] = "polls";
$product_modules[] = "mailbox";
$product_modules[] = "mail_list";
$product_modules[] = "get_token";
$product_modules[] = "contact";
$product_modules[] = "spam";
$product_modules[] = "reviews";
$product_modules[] = "upload_gallery";
$product_modules[] = "listings";
$product_modules[] = "export";
$product_modules[] = "import";
