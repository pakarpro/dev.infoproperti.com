<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include dirname(__FILE__)."/ckeditor/fckeditor.php";

function create_editor($id, $content, $width=800, $height=400, $toolbar='Default'){
	$editor = new FCKeditor($id);
	$editor->BasePath = "/".SITE_SUBFOLDER."system/plugins/ckeditor/";
	$editor->Width = $width;
	$editor->Height = $height;
	$editor->ToolbarSet = $toolbar;
	$editor->Value = $content;
	return $editor->CreateHtml();
}
