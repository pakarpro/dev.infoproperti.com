<?php

/**
 * Template_Lite  function plugin
 *
 */
function tpl_function_start_search_form($params, &$tpl) {
	$tpl->CI->load->helper('start');
    
	if(empty($params['object'])) $params['object'] = 'sale';
	if(empty($params['type'])) $params['type'] = 'line';
	if(!isset($params['show_data'])) $params['show_data'] = false;
    
	return main_search_form($params['object'], $params['type'], $params['show_data']);
}

