<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

define('SEO_MODULES_TABLE', DB_PREFIX.'seo_modules');
define('SEO_SETTINGS_TABLE', DB_PREFIX.'seo_settings');

/**
* PG Themes Model
* 
* @package PG_Core
* @subpackage themes
* @category	model
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/

class CI_Pg_seo {

	/**
	 * link to CodeIgniter object
	 * @var object
	 */
	var $CI;
	var $DB;
	var $use_db=false;
	var $use_seo_links_rewrite = true;

	var $reg_exp_literal_whole = '^[\pL\pN\pM\pZ,\'\!\@\^\&\*\(\)\+\-\!\/_=:\.]+$';
	var $reg_exp_numeric_whole = '^[\pN]+$';
	var $reg_exp_last_whole = '^[\pL\pN\pM\pZ,\'\!\@\^\&\*\(\)\+\-\!\/_=:\.]*$';

	var $reg_exp_literal = '[\pL\pN\pM\pZ,\'\!\@\^\&\*\(\)\+\-\!\/_=:\.]+';
	var $reg_exp_numeric = '[\pN]+';
	var $reg_exp_last = '[\pL\pN\pM\pZ,\'\!\@\^\&\*\(\)\+\-\!\/_=:\.]*';
	var $reg_exp_literal_last = '[\pL\pN\pM\pZ,\'\!\@\^\&\*\(\)\+\-\!\/_=:\.]*';
	var $reg_exp_numeric_last = '[\pN]*';

	var $not_reg_exp_literal = '[^\pL\pN\pM\pZ,\'\!\@\^\&\*\(\)\+\-\!\/_=:\.]+';
	var $not_reg_exp_numeric = '[^\pN]+';
	var $not_reg_exp_last = '[^\pL\pN\pM\pZ,\'\!\@\^\&\*\(\)\+\-\!\/_=:\.]*';

	var $seo_tags_html_cache = array();
	/**
	 * Default settings, preinstalled settings (if install module not installed and/or database settings not valid)
	 * @var array
	 */
	var $default_settings=array(
		"admin" => array(
			"controller" => "admin",
			"module_gid" => "",
			"method" => "",
			"title" => "",
			"keyword" => "",
			"description" => "",
			"header" => "",
			"templates" => array(),
			"url_template" => "",
			"lang_in_url" => ""
		),
		"user" => array(
			"controller" => "user",
			"module_gid" => "",
			"method" => "",
			"title" => "",
			"keyword" => "",
			"description" => "",
			"header" => "",
			"templates" => array(),
			"url_template" => "",
			"lang_in_url" => ""
		)
	);

	var $seo_dynamic_data = array();

	private $lang_prefix = '';

	private $seo_module_cache = array();
	private $seo_key_cache = array();
	private $url_scheme_cache = array();
	
	private $settings_cache = array();

	private $module_settings_cache = array();

	/**
	 * Constructor
	 *
	 * @return CI_PG_Theme Object
	 */
	function CI_PG_Seo(){
		$this->CI =& get_instance();
		if(INSTALL_MODULE_DONE){
			$this->use_db = true;
			$this->DB = &$this->CI->db;
			$this->preload_settings_cache();
			$this->preload_modules_cache();
		}
		
		$global_templates = array(
			
		);
		$this->set_seo_data($global_templates);
	}

	function get_global_default_settings($controller='user'){
		return $this->default_settings[$controller];
	}
	
	//// settings cache functions
	function preload_settings_cache(){
		$this->DB->select('id, controller, module_gid, method, default_title, default_description, default_keyword, default_header, url_template, lang_in_url')->from(SEO_SETTINGS_TABLE);
		$results = $this->DB->get()->result_array();
		foreach($results as $result){
			$this->settings_cache[] = $result;
		}
	}
	
	function clear_settings_cache(){
		$this->settings_cache = array();
	}
	
	function get_settings_from_cache($controller='user', $module_gid='', $method=''){
		if(empty($this->settings_cache)){
			$this->preload_settings_cache();
		}
		if(!empty($this->settings_cache)){
			foreach($this->settings_cache as $settings){
				if($settings['controller'] == $controller && $settings['module_gid'] == $module_gid && $settings['method'] == $method ){
					return $settings;
				}
			}
		}
		return array();
	}
	
	function get_all_settings_from_cache($controller='user', $module_gid='', $method=''){
		$return = array();
		if(empty($this->settings_cache)){
			$this->preload_settings_cache();
		}
		if(!empty($this->settings_cache)){
			foreach($this->settings_cache as $settings){
				$allow_controller = $allow_module = $allow_method = false;
				if(!$controller || ($controller && $settings['controller'] == $controller)){
					$allow_controller = true;
				}
				if(!$module_gid || ($module_gid && $settings['module_gid'] == $module_gid)){
					$allow_module = true;
				}
				if(!$method || ($method && $settings['method'] == $method)){
					$allow_method = true;
				}
				
				if($allow_controller && $allow_module && $allow_method ){
					$return[] = $settings;
				}
			}
		}

		return $return;
	}

	/*
	 * Return 1 entry from base 
	 * if $method='' - returns general settings for module, if $module_gid='' - general settings for controller 
	 * If $lang_ids is empty return settings for current lang, else settings array for selected languages
	 *
	 */
	function get_settings($controller='user', $module_gid='', $method='', $lang_ids=array()){
		if(!$this->use_db) return false;

		$settings = $this->get_settings_from_cache($controller, $module_gid, $method);
		
		if(!empty($settings)){
			$tag_types = array('title', 'description', 'keyword', 'header');
			foreach($tag_types as $tag_type){
				if(!$settings['default_'.$tag_type]){
					if(!empty($lang_ids)){
						foreach($lang_ids as $lang_id){
							$settings[$tag_type][$lang_id] = $this->CI->pg_language->get_string("seo_texts_".$settings['module_gid'], $settings["controller"]."_".$settings["method"]."_".$tag_type, $lang_id);
						}
					}else{
						$settings[$tag_type] = $this->CI->pg_language->get_string("seo_texts_".$settings['module_gid'], $settings["controller"]."_".$settings["method"]."_".$tag_type);
					}
				}
			}
		}

		return $settings;
	}

	/*
	 * Return settings array for not empty parametrs
	 * f.e. $module_gid='' will be returned all entries for $module_gid 
	 *
	 */
	function get_all_settings($controller='user', $module_gid='', $method='', $lang_ids=array()){
		if(!$this->use_db) return false;
		
		$settings = array();
		$results = $this->get_all_settings_from_cache($controller, $module_gid, $method);

		if(!empty($results)){
			$tag_types = array('title', 'description', 'keyword', 'header');
			foreach($results as $result){
				foreach($tag_types as $tag_type){
					if(!$result['default_'.$tag_type]){
						if(!empty($lang_ids)){
							foreach($lang_ids as $lang_id){
								$result[$tag_type][$lang_id] = $this->CI->pg_language->get_string("seo_texts_".$result['module_gid'], $result["controller"]."_".$result["method"]."_".$tag_type, $lang_id);
							}
						}else{
							$result[$tag_type] = $this->CI->pg_language->get_string("seo_texts_".$result['module_gid'], $result["controller"]."_".$result["method"]."_".$tag_type);
						}
					}
				}
				$settings[] = $result;
			}
		}
		return $settings;
	}

	/*
	 * Save settings for $controller, $module_gid, $method in base
	 *
	 */
	function set_settings($controller, $module_gid, $method, $data){
		$settings = $this->get_settings($controller, $module_gid, $method);
		$sett_data = array(
			'controller' => $controller, 
			'module_gid' => $module_gid, 
			'method' => $method, 
			'default_title' => strval($data["default_title"]), 
			'default_description' => strval($data["default_description"]), 
			'default_keyword' => strval($data["default_keyword"]),
			'default_header' => strval($data["default_header"]),
			'url_template' => strval($data["url_template"]),
			'lang_in_url' => intval($data["lang_in_url"])
		);
		if(!empty($settings)){
			$this->DB->where('controller', $controller);
			$this->DB->where('module_gid', $module_gid);
			$this->DB->where('method', $method);
			$this->DB->update(SEO_SETTINGS_TABLE, $sett_data);
		}else{
			$this->DB->insert(SEO_SETTINGS_TABLE, $sett_data);
		}

		if(!$data["default_title"] && !empty($data["title"])){
			foreach($data["title"] as $lang_id => $title){
				$this->CI->pg_language->pages->set_string("seo_texts_".$module_gid, $controller."_".$method."_title", $title, $lang_id);
			}
		}

		if(!$data["default_keyword"] && !empty($data["keyword"])){
			foreach($data["keyword"] as $lang_id => $keyword){
				$this->CI->pg_language->pages->set_string("seo_texts_".$module_gid, $controller."_".$method."_keyword", $keyword, $lang_id);
			}
		}

		if(!$data["default_description"] && !empty($data["description"])){
			foreach($data["description"] as $lang_id => $description){
				$this->CI->pg_language->pages->set_string("seo_texts_".$module_gid, $controller."_".$method."_description", $description, $lang_id);
			}
		}

		if(!$data["default_header"] && !empty($data["header"])){
			foreach($data["header"] as $lang_id => $description){
				$this->CI->pg_language->pages->set_string("seo_texts_".$module_gid, $controller."_".$method."_header", $description, $lang_id);
			}
		}
		
		$this->clear_settings_cache();
		return;
	}

	/*
	 * Get seo data using module methods 
	 *
	 */
	function get_default_settings($controller, $module_gid, $method='', $lang_id=''){
		if(!$this->use_db) return false;

		//// if in admin area dont use module settings
		if($controller == 'admin'){
			return false;
		}
		
		if(!empty($method) && empty($lang_id) && isset($this->module_settings_cache[$controller][$module_gid][$method])){
			return $this->module_settings_cache[$controller][$module_gid][$method];
		}

		$module_data = $this->get_seo_module_by_gid($module_gid);

		if(empty($module_data)){
			return false;
		}

		$this->CI->load->model($module_data["module_gid"]."/models/".$module_data["model_name"]);
		$settings = $this->CI->$module_data["model_name"]->$module_data["get_settings_method"]($method, $lang_id);
		if(empty($method)){
			$this->module_settings_cache[$controller][$module_gid] = $settings;
		}elseif(empty($lang_id)){
			$this->module_settings_cache[$controller][$module_gid][$method] = $settings;
		}

		return $settings;
	}

	public function get_lang_prefix() {
		return $this->lang_prefix;
	}

	public function set_lang_prefix($controller = 'user') {
		$settings = $this->get_settings_from_cache($controller);
		if($settings['lang_in_url']) {
			$lang_code = $this->CI->pg_language->get_lang_code_by_id($this->CI->pg_language->current_lang_id);
			$this->lang_prefix = $lang_code . '/';
		}
		return true;
	}

	///// seo cache module methods
	function preload_modules_cache(){
		$this->DB->select('id, module_gid, model_name, get_settings_method, get_rewrite_vars_method, get_sitemap_urls_method')->from(SEO_MODULES_TABLE);
		$results = $this->DB->get()->result_array();
		foreach($results as $result){
			$this->seo_module_cache[$result["module_gid"]] = $result;
		}
	}
	
	function get_seo_module_from_cache($module_gid){
		if(empty($this->seo_module_cache)){
			$this->preload_modules_cache();
		}
		if(!empty($this->seo_module_cache[$module_gid])){
			return $this->seo_module_cache[$module_gid];
		}
		return array();
		
	}
	
	function clear_seo_module_cache(){
		$this->seo_module_cache = array();
	}
		
	///// seo module methods
	function get_module_rewrite_var($controller, $module_gid, $method, $var_from, $var_to, $value){
		$module_data = $this->get_seo_module_by_gid($module_gid);

		if(empty($module_data)){
			return false;
		}
	
		$this->CI->load->model($module_data["module_gid"]."/models/".$module_data["model_name"]);
		$value = $this->CI->$module_data["model_name"]->$module_data["get_rewrite_vars_method"]($var_from, $var_to, $value);
		return $value;
	}

	function get_seo_module_by_gid($module_gid){
		return $this->get_seo_module_from_cache($module_gid);
	}

	function get_seo_modules(){
		unset($this->seo_module_cache);
		$this->DB->select('id, module_gid, model_name, get_settings_method, get_rewrite_vars_method, get_sitemap_urls_method')->from(SEO_MODULES_TABLE)->order_by("module_gid ASC");
		$results = $this->CI->db->get()->result_array();
		if(!empty($results)){
			foreach($results as $r){
				$this->seo_module_cache[$r["module_gid"]] = $r;
			}
		}
		return $this->seo_module_cache;
	}

	function set_seo_module($module_gid, $data=array()){
		$module_data = $this->get_seo_module_by_gid($module_gid);
		if(empty($module_data)){
			$this->DB->insert(SEO_MODULES_TABLE, $data);
		}else{
			$this->DB->where("module_gid", $module_gid);
			$this->DB->update(SEO_MODULES_TABLE, $data);
		}
		$this->clear_seo_module_cache();
	}

	function delete_seo_module($module_gid){
		$this->DB->where("module_gid", $module_gid);
		$this->DB->delete(SEO_MODULES_TABLE);

		$this->DB->where("module_gid", $module_gid);
		$this->DB->delete(SEO_SETTINGS_TABLE);
		
		$this->clear_seo_module_cache();
	}

	function parse_seo_data($settings){
		if(!empty($settings['templates'])){
			foreach($settings['templates'] as $tag){
				$value = (!empty($this->seo_dynamic_data[$tag]))?$this->seo_dynamic_data[$tag]:"";
				$pattern = "/\[".$tag."(\|([^\]]*))?\]/i";
				$replace = (!empty($value))?$value:"$2";
				
				if(strlen($settings["title"])){
					$settings["title"] = preg_replace($pattern, $replace, $settings["title"]);
				}
				if(strlen($settings["description"])){
					$settings["description"] = preg_replace($pattern, $replace, $settings["description"]);
				}
				if(strlen($settings["keyword"])){
					$settings["keyword"] = preg_replace($pattern, $replace, $settings["keyword"]);
				}
				if(strlen($settings["header"])){
					$settings["header"] = preg_replace($pattern, $replace, $settings["header"]);
				}
			}
		}
		return $settings;
	}

	function session_seo_tags_html($controller, $module_gid, $method){
		if(empty($this->seo_tags_html_cache[$controller][$module_gid][$method])){
			$default_data = $this->get_default_settings($controller, $module_gid, $method);

			if(empty($default_data)){
				$default_data = $this->get_global_default_settings($controller);
				$module_gid = $method ="";
			}

			$user_settings = $this->get_settings($controller, $module_gid, $method);
			if(!empty($user_settings)){
				if(!$user_settings["default_title"]){
					$default_data["title"] = $user_settings["title"];
				}
				if(!$user_settings["default_description"]){
					$default_data["description"] = $user_settings["description"];
				}
				if(!$user_settings["default_keyword"]){
					$default_data["keyword"] = $user_settings["keyword"];
				}
				if(!$user_settings["default_header"]){
					$default_data["header"] = $user_settings["header"];
				}
			}

			$default_data = $this->parse_seo_data($default_data);
			$html["title"] = '<title>'.$default_data["title"].'</title>'."\n";
			$html["description"] = '<meta name="Description" content="'.addslashes($default_data["description"]).'">'."\n";
			$html["keyword"] = '<meta name="Keywords" content="'.addslashes($default_data["keyword"]).'">'."\n";
			$html["header"] = '<h1>'.$default_data["header"].'</h1>'."\n";
			$html["header_text"] = $default_data["header"];

			$this->seo_tags_html_cache[$controller][$module_gid][$method] = $html;
		}
		return $this->seo_tags_html_cache[$controller][$module_gid][$method];
	}

	function set_seo_data($data){
		foreach($data as $key=>$value){
			$this->seo_dynamic_data[$key] = $value;
		}
	}

	function validate_url_data($module_gid, $method, $data, $url_data){
		$return = array("errors"=> array(), "data" => array());
		$num_vars = array();
		if(empty($data) || !array($data)){
			$return["data"]["url_template"] = "";
		}else{
			$prev_block_type = 'text';
			$error_invalid_text_delimiter = false;

			foreach($data as $key=>$block){
				if($block["type"] == "text"){
				
					if(empty($block["value"]) || !preg_match('/'.$this->reg_exp_literal_whole.'/i', $block["value"] )){
						$return["errors"][] = l('error_url_text_block_invalid', 'seo')." (".$block["value"].")";
						$block["value"] = preg_replace("/".$this->not_reg_exp_literal."/i", "", $block["value"]);
					}
					$data[$key]["value"] = trim(strtolower($block["value"]));
					
					if(empty($data[$key]["value"])){
						unset($data[$key]);	
					}else{
						$prev_block_type = $block["type"];
					}
				}elseif($block["type"] == "tpl" || $block["type"] == "opt"){
					
					$reg_exp = ($block["var_type"]=="literal")?$this->reg_exp_literal_whole:$this->reg_exp_numeric_whole;
					$not_reg_exp = ($block["var_type"]=="literal")?$this->not_reg_exp_literal:$this->not_reg_exp_numeric;


					if(!empty($block["var_default"]) && !preg_match('/'.$reg_exp.'/i', $block["var_default"] )){
						$return["errors"][] = l('error_url_tpl_block_default_invalid', 'seo');
						$block["var_default"] = preg_replace("/".$not_reg_exp."/i", "", $block["var_default"]);
					}
					$data[$key]["var_default"] = strtolower($block["var_default"]);
					if(!in_array($block["var_num"], $num_vars) && $block["type"] == "tpl"){
						$num_vars[] = $block["var_num"];
					}
					if($prev_block_type != "text"){
						$error_invalid_text_delimiter = true;
					}
					$prev_block_type = $block["type"];
				}
			}
			$temp = $data;
			unset($data);
			foreach($temp as $block){
				$data[] = $block;
			}			
			
			///// tpl blocks delimiters is invalid ?
			if($error_invalid_text_delimiter){
				$return["errors"][] = l('error_url_text_delim_invalid', 'seo');
			}

			////// all templates are used?
			if(count($num_vars)<count($url_data)){
				$return["errors"][] = l('error_url_var_num_invalid', 'seo');
			}

			///// if first block not a text
			if($data[0]["type"] != "text" || empty($data[0]["value"])){
				$return["errors"][] = l('error_url_first_block_text', 'seo');
			}else{
				///// get module folder in first part
				$parts = explode("/", $data[0]["value"]);
				if(count($parts) > 1 && $parts[0] != $module_gid && $this->CI->pg_module->get_module_by_gid($parts[0])){
					$return["errors"][] = l('error_url_first_block_module', 'seo');
				}
			}
			$return["data"]["url_template"] = $this->url_template_transform($module_gid, $method, $data, 'js', "base");
		}
		return $return;
	}

	function url_template_transform($module, $method, $data, $from, $to){
		if($from == "base"){
			$parsed = array();
			$reg_exp = "/(\[text:(".$this->reg_exp_literal.")\])|(\[tpl:(".$this->reg_exp_numeric."):(".$this->reg_exp_literal."):(numeric|literal):([\w\-_\d]*)\])|(\[opt:(".$this->reg_exp_literal."):(numeric|literal):([\w\-_\d]*)\])/i";
			preg_match_all($reg_exp, $data, $matches, PREG_SET_ORDER);
			foreach($matches as $match){
				if(!empty($match[1])){
					$parsed[] = array(
						"type" => "text",
						"value" => $match[2],
					);
				}elseif(!empty($match[3])){
					$parsed[] = array(
						"type" => "tpl",
						"var_num" => $match[4],
						"var_name" => $match[5],
						"var_type" => $match[6],
						"var_default" => $match[7],
					);
				}else{
					$parsed[] = array(
						"type" => "opt",
						"var_num" => 0,
						"var_name" => $match[9],
						"var_type" => $match[10],
						"var_default" => $match[11],
					);
				}
			}
		}

		if($from == "xml"){
			$parsed = array();
			$reg_exp = "/(\[text\|(".$this->reg_exp_literal.")\])|(\[tpl\|(".$this->reg_exp_numeric.")\|(".$this->reg_exp_literal.")\|(".$this->reg_exp_literal.")\])|(\[opt\|(".$this->reg_exp_literal.")\|(".$this->reg_exp_literal.")\])/i";
			preg_match_all($reg_exp, $data, $matches, PREG_SET_ORDER);
			foreach($matches as $match){
				if(!empty($match[1])){
					$parsed[] = array(
						"type" => "text",
						"value" => $match[2],
					);
				}elseif(!empty($match[3])){
					$parsed[] = array(
						"type" => "tpl",
						"var_num" => $match[4],
						"var_name" => $match[5],
						"var_type" => $match[6],
					);
				}else{
					$parsed[] = array(
						"type" => "opt",
						"var_num" => 0,
						"var_name" => $match[8],
						"var_type" => $match[9],
					);
				}
			}
		}

		if($from == "base" && $to=="scheme"){
			$url = "";
			foreach($parsed as $match){
				if($match["type"] == "text"){
					$url .= $match["value"];
				}else{
					$url .= "[".$match["var_name"]."]";
				}
			}
			return $url;

		}elseif($from == "base" && $to=="js"){
			return $parsed;

		}elseif($from == "base" && $to=="xml"){
			$link = "";
			foreach($parsed as $match){
				if($match["type"] == "text"){
					$link .= "[text|".$match["value"]."]";
				}elseif($match["type"] == "tpl"){
					$link .= "[tpl|".$match["var_num"]."|".$match["var_name"]."|".$match["var_type"]."]";
				}else{
					$link .= "[opt|".$match["var_name"]."|".$match["var_type"]."]";
				}
			}
			return $link;
		}elseif($from == "js" && $to=="base"){
			$url = "";
			foreach($data as $block){
				if($block["type"] == 'tpl'){
					$url .= '[tpl:'.$block["var_num"].':'.$block["var_name"].':'.$block["var_type"].':'.$block["var_default"].']';
				}elseif($block["type"] == 'opt'){
					$url .= '[opt:'.$block["var_name"].':'.$block["var_type"].':'.$block["var_default"].']';
				}else{
					$url .= '[text:'.$block["value"].']';
				}
			}
			return $url;
		
		}elseif($from == "xml" && $to=="rule"){
			$rule = "";
			$redirect = $module."/".$method;
			$exp_index = 1;
			$vars_order = array();
			$last_part = "text";

			foreach($parsed as $match){
				$last_part = $match["type"];
				if($match["type"] == "text"){
					$rule .= $match["value"];
				}elseif($match["type"] == "tpl"){
					if($match["var_type"] == "literal"){
						$rule .= "(".$this->reg_exp_literal.")";
					}else{
						$rule .= "(".$this->reg_exp_numeric.")";
					}
					$vars_order[$match['var_num']] = array("num"=>$exp_index, "name"=>$match["var_name"]);
					$exp_index++;
				}else{
					if($match["var_type"] == "literal"){
						$rule .= "(".$this->reg_exp_literal_last.")";
					}else{
						$rule .= "(".$this->reg_exp_numeric_last.")";
					}
					$exp_index++;
				}
			}

			$max_patern_num = 0;
			if(!empty($vars_order)){
				ksort($vars_order);
				foreach($vars_order as $var_num => $pattern){
					$redirect .= '/'.$pattern["name"].':$'.$pattern["num"];
					if($pattern["num"] > $max_patern_num) $max_patern_num = $pattern["num"];
				}
			}
			
			if($last_part == "text"){
				/// if last part is text  => add regexp for transfer additional params
				$rule .= "(".$this->reg_exp_last.")";
				$redirect .= '$'.($max_patern_num+1);
			}
			$str = '$route["'.$rule.'"]="'.$redirect.'";';
			return $str;
		}
	}

	function get_settings_urls($module, $method){
		if(!isset($this->url_scheme_cache[$module])){
			$results = $this->get_all_settings_from_cache('', $module, '');
			if(!empty($results)){
				foreach($results as $result){
					$this->url_scheme_cache[$result["module_gid"]][$result["method"]] = $result["url_template"];
				}
			}else{
				$this->url_scheme_cache[$module] = array();
			}
		}
		return isset($this->url_scheme_cache[$module][$method])?$this->url_scheme_cache[$module][$method]:"";
	}

	///// links rewrite methods
	function create_url($module, $method, $str, $is_admin){
		$link = "";
		if(is_array($str)){
			$data = $str;
		}else{
			$temp = explode("|", $str);
			if(!$is_admin) $settings = $this->get_default_settings('user', $module, $method);
			if(!empty($settings["url_vars"])){
				$index = 0;
				foreach($settings["url_vars"] as $var_name => $replaces){
					if(isset($temp[$index])){
						$data[$var_name] = $temp[$index];
						foreach($replaces as $replace => $replace_type){
							$data[$replace] = $temp[$index];
						}
					}
					$index++;
				}
			}else{
				$data = $temp;
			}
		}

		if($this->use_seo_links_rewrite && !$is_admin){
			$url_scheme = $this->get_settings_urls($module, $method);
			if(!empty($url_scheme)){
				$link = $link2 = site_url();
				$parts = $this->url_template_transform($module, $method, $url_scheme, 'base', 'js');

				foreach($parts as $part){
					if($part["type"] == "text"){
						$link .= $part["value"];
					}elseif($part["type"] == "opt"){
						if($part['var_type'] == 'literal'){
							$regex = $this->reg_exp_last;	
						}else{
							$regex = $this->reg_exp_numeric;	
						}
						$value = (!empty($data[$part["var_name"]])) ? $data[$part["var_name"]] : $part["var_default"];
						$arr = array();
						preg_match_all("/".$regex."/ui", html_entity_decode($value), $arr);
						$link .= urlencode(implode('', $arr[0]));
					}else{
						$link .= (!empty($data[$part["var_name"]])) ? $data[$part["var_name"]] : $part["var_default"];
					}
				}
			}
		}

		if(empty($link)){
			if(empty($settings) && !$is_admin) $settings = $this->get_default_settings('user', $module, $method);
			$link = site_url() . ($is_admin ? "admin/" : "") . $module . "/" . $method;
			if(!empty($settings["url_vars"])){
				foreach($settings["url_vars"] as $var_name => $replaces){
					$link .= "/" . $data[$var_name];
				}
			}else{
				foreach($data as $segment){
					$link .= "/" . $segment;
				}
			}
		}
		
		$link = preg_replace('/\/{2,}$/', '/', $link);
		return $link;
	}

	function rewrite_url_vars($module, $method){
	
	}

}
