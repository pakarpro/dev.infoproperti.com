<?php

$config["editor_type"]["residential_sale"] = array(
	"gid" => "residential_sale",
	"module" => "listings",
	"name" => "Residential realty (for sale)",	
	"tables" => array(
		"listings_residential" => DB_PREFIX."listings_residential",
		"listings_residential_moderation" => DB_PREFIX."listings_residential_moderation",
	),
	"field_prefix" => 'fe_',
	'fulltext_use' => true,
	'fulltext_field' => array(
		"listings_residential" => 'search_field',
	),
	'fulltext_foreign_key' => 'id_listing',
	'fulltext_model' => "Listings_model",
	'fulltext_callback' => "get_fulltext_data",
);

$config["editor_type"]["commercial_sale"] = array(
	"gid" => "commercial_sale",
	"module" => "listings",
	"name" => "Commerce realty (for sale)",	
	"tables" => array(
		"listings_commercial" => DB_PREFIX."listings_commercial",
		"listings_commercial_moderation" => DB_PREFIX."listings_commercial_moderation",
	),
	"field_prefix" => 'fe_',
	'fulltext_use' => true,
	'fulltext_field' => array(
		"listings_commercial" => 'search_field',
	),
	'fulltext_foreign_key' => 'id_listing',
	'fulltext_model' => "Listings_model",
	'fulltext_callback' => "get_fulltext_data",
);

$config["editor_type"]["lot_and_land_sale"] = array(
	"gid" => "lot_and_land_sale",
	"module" => "listings",
	"name" => "Lots and lands (for sale)",	
	"tables" => array(
		"listings_lot_and_land" => DB_PREFIX."listings_lot_and_land",
		"listings_lot_and_land_moderation" => DB_PREFIX."listings_lot_and_land_moderation",
	),
	"field_prefix" => 'fe_',
	'fulltext_use' => true,
	'fulltext_field' => array(
		'listings_lot_and_land' => 'search_field',
	),
	'fulltext_foreign_key' => 'id_listing',
	'fulltext_model' => "Listings_model",
	'fulltext_callback' => "get_fulltext_data",
);

/*$config["editor_type"]["residential_buy"] = array(
	"gid" => "residential_buy",
	"module" => "listings",
	"name" => "Residential realty (for buy)",	
	"tables" => array(
		"listings_residential" => DB_PREFIX."listings_residential",
		"listings_residential_moderation" => DB_PREFIX."listings_residential_moderation",
	),
	"field_prefix" => 'fe_',
	'fulltext_use' => true,
	'fulltext_field' => array(
		"listings_residential" => 'search_field',
	),
	'fulltext_foreign_key' => 'id_listing',
	'fulltext_model' => "Listings_model",
	'fulltext_callback' => "get_fulltext_data",
);

$config["editor_type"]["commercial_buy"] = array(
	"gid" => "commercial_buy",
	"module" => "listings",
	"name" => "Commerce realty (for buy)",	
	"tables" => array(
		"listings_commercial" => DB_PREFIX."listings_commercial",
		"listings_commercial_moderation" => DB_PREFIX."listings_commercial_moderation",
	),
	"field_prefix" => 'fe_',
	'fulltext_use' => true,
	'fulltext_field' => array(
		"listings_commercial" => 'search_field',
	),
	'fulltext_foreign_key' => 'id_listing',
	'fulltext_model' => "Listings_model",
	'fulltext_callback' => "get_fulltext_data",
);

$config["editor_type"]["lot_and_land_buy"] = array(
	"gid" => "lot_and_land_buy",
	"module" => "listings",
	"name" => "Lots and lands (for buy)",	
	"tables" => array(
		"listings_lot_and_land" => DB_PREFIX."listings_lot_and_land",
		"listings_lot_and_land_moderation" => DB_PREFIX."listings_lot_and_land_moderation",
	),
	"field_prefix" => 'fe_',
	'fulltext_use' => true,
	'fulltext_field' => array(
		'listings_lot_and_land' => 'search_field',
	),
	'fulltext_foreign_key' => 'id_listing',
	'fulltext_model' => "Listings_model",
	'fulltext_callback' => "get_fulltext_data",
);*/

$config["editor_type"]["residential_rent"] = array(
	"gid" => "residential_rent",
	"module" => "listings",
	"name" => "Residential realty (for rent)",	
	"tables" => array(
		"listings_residential_rent" => DB_PREFIX."listings_residential",
		"listings_residential_rent_moderation" => DB_PREFIX."listings_residential_moderation",
	),
	"field_prefix" => 'fe_',
	'fulltext_use' => true,
	'fulltext_field' => array(
		"listings_residential_rent" => 'search_field',
	),
	'fulltext_foreign_key' => 'id_listing',
	'fulltext_model' => "Listings_model",
	'fulltext_callback' => "get_fulltext_data",
);

$config["editor_type"]["commercial_rent"] = array(
	"gid" => "commercial_rent",
	"module" => "listings",
	"name" => "Commerce realty (for rent)",	
	"tables" => array(
		"listings_commercial_rent" => DB_PREFIX."listings_commercial",
		"listings_commercial_rent_moderation" => DB_PREFIX."listings_commercial_moderation",
	),
	"field_prefix" => 'fe_',
	'fulltext_use' => true,
	'fulltext_field' => array(
		"listings_commercial_rent" => 'search_field',
	),
	'fulltext_foreign_key' => 'id_listing',
	'fulltext_model' => "Listings_model",
	'fulltext_callback' => "get_fulltext_data",
);

$config["editor_type"]["lot_and_land_rent"] = array(
	"gid" => "lot_and_land_rent",
	"module" => "listings",
	"name" => "Lots and lands (for rent)",	
	"tables" => array(
		"listings_lot_and_land_rent" => DB_PREFIX."listings_lot_and_land",
		"listings_lot_and_land_rent_moderation" => DB_PREFIX."listings_lot_and_land_moderation",
	),
	"field_prefix" => 'fe_',
	'fulltext_use' => true,
	'fulltext_field' => array(
		'listings_lot_and_land_rent' => 'search_field',
	),
	'fulltext_foreign_key' => 'id_listing',
	'fulltext_model' => "Listings_model",
	'fulltext_callback' => "get_fulltext_data",
);

/*$config["editor_type"]["residential_lease"] = array(
	"gid" => "residential_lease",
	"module" => "listings",
	"name" => "Residential realty (for lease)",	
	"tables" => array(
		"listings_residential" => DB_PREFIX."listings_residential",
		"listings_residential_moderation" => DB_PREFIX."listings_residential_moderation",
	),
	"field_prefix" => 'fe_',
	'fulltext_use' => true,
	'fulltext_field' => array(
		"listings_residential" => 'search_field',
	),
	'fulltext_foreign_key' => 'id_listing',
	'fulltext_model' => "Listings_model",
	'fulltext_callback' => "get_fulltext_data",
);

$config["editor_type"]["commercial_lease"] = array(
	"gid" => "commercial_buy",
	"module" => "listings",
	"name" => "Commerce realty (for lease)",	
	"tables" => array(
		"listings_commercial" => DB_PREFIX."listings_commercial",
		"listings_commercial_moderation" => DB_PREFIX."listings_commercial_moderation",
	),
	"field_prefix" => 'fe_',
	'fulltext_use' => true,
	'fulltext_field' => array(
		"listings_commercial" => 'search_field',
	),
	'fulltext_foreign_key' => 'id_listing',
	'fulltext_model' => "Listings_model",
	'fulltext_callback' => "get_fulltext_data",
);

$config["editor_type"]["lot_and_land_lease"] = array(
	"gid" => "lot_and_land_lease",
	"module" => "listings",
	"name" => "Lots and lands (for lease)",	
	"tables" => array(
		"listings_lot_and_land" => DB_PREFIX."listings_lot_and_land",
		"listings_lot_and_land_moderation" => DB_PREFIX."listings_lot_and_land_moderation",
	),
	"field_prefix" => 'fe_',
	'fulltext_use' => true,
	'fulltext_field' => array(
		'listings_lot_and_land' => 'search_field',
	),
	'fulltext_foreign_key' => 'id_listing',
	'fulltext_model' => "Listings_model",
	'fulltext_callback' => "get_fulltext_data",
);*/
