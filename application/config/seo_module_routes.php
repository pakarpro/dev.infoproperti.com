<?php
#kpr mod
$route["listings-kpr-([\pL\pN\pM\pZ,'\!\@\^\&\*\(\)\+\-\!\/_=:\.]+)"]="listings/kpr/$1";
$route["kpr-([\pL\pN\pM\pZ,'\!\@\^\&\*\(\)\+\-\!\/_=:\.]+)-([\pL\pN\pM\pZ,'\!\@\^\&\*\(\)\+\-\!\/_=:\.]+)-id-([\pL\pN\pM\pZ,'\!\@\^\&\*\(\)\+\-\!\/_=:\.]+)"]="listings/kpr_bank/$1/$2/$3";
$route["listings-kpr_get_bank"]="listings/get_bank";
#end of kpr mod
$route["listing-([\pL\pN\pM\pZ,'\!\@\^\&\*\(\)\+\-\!\/_=:\.]*)-in-([\pL\pN\pM\pZ,'\!\@\^\&\*\(\)\+\-\!\/_=:\.]*)-id-([\pN]+)-([\pL\pN\pM\pZ,'\!\@\^\&\*\(\)\+\-\!\/_=:\.]+)-operation-([\pL\pN\pM\pZ,'\!\@\^\&\*\(\)\+\-\!\/_=:\.]*)-([\pL\pN\pM\pZ,'\!\@\^\&\*\(\)\+\-\!\/_=:\.]+)"]="listings/view/id:$3/section:$4/pdf:$6";
$route["listings-index-([\pL\pN\pM\pZ,'\!\@\^\&\*\(\)\+\-\!\/_=:\.]+)"]="listings/index/operation_type:$1";
$route["listings-by-([\pL\pN\pM\pZ,'\!\@\^\&\*\(\)\+\-\!\/_=:\.]+)-([\pL\pN\pM\pZ,'\!\@\^\&\*\(\)\+\-\!\/_=:\.]+)-([\pL\pN\pM\pZ,'\!\@\^\&\*\(\)\+\-\!\/_=:\.]+)"]="listings/category/category:$1/property:$2/operation_type:$3";
$route["listings-in-([\pL\pN\pM\pZ,'\!\@\^\&\*\(\)\+\-\!\/_=:\.]+)-([\pL\pN\pM\pZ,'\!\@\^\&\*\(\)\+\-\!\/_=:\.]+)-([\pL\pN\pM\pZ,'\!\@\^\&\*\(\)\+\-\!\/_=:\.]+)-([\pL\pN\pM\pZ,'\!\@\^\&\*\(\)\+\-\!\/_=:\.]+)"]="listings/location/country:$1/region:$2/city:$3/operation_type:$4";
$route["listings-from-([\pL\pN\pM\pZ,'\!\@\^\&\*\(\)\+\-\!\/_=:\.]*)-([\pN]+)-([\pL\pN\pM\pZ,'\!\@\^\&\*\(\)\+\-\!\/_=:\.]+)"]="listings/user/id_user:$2/operation_type:$3";
$route["listings-search-([\pL\pN\pM\pZ,'\!\@\^\&\*\(\)\+\-\!\/_=:\.]+)-([\pL\pN\pM\pZ,'\!\@\^\&\*\(\)\+\-\!\/_=:\.]+)"]="listings/search/keyword:$2/operation_type:$1";
$route["listings-by-open_house-([\pL\pN\pM\pZ,'\!\@\^\&\*\(\)\+\-\!\/_=:\.]+)"]="listings/open_house/operation_type:$1";
$route["listings-by-privates-([\pL\pN\pM\pZ,'\!\@\^\&\*\(\)\+\-\!\/_=:\.]+)"]="listings/privates/operation_type:$1";
$route["listings-by-agents-([\pL\pN\pM\pZ,'\!\@\^\&\*\(\)\+\-\!\/_=:\.]+)"]="listings/agents/operation_type:$1";
$route["listings-by-discount-([\pL\pN\pM\pZ,'\!\@\^\&\*\(\)\+\-\!\/_=:\.]+)"]="listings/discount/operation_type:$1";
$route["recommended-properties([\pL\pN\pM\pZ,'\!\@\^\&\*\(\)\+\-\!\/_=:\.]*)"]="listings/wish_lists$1";
$route["recommended-properties([\pL\pN\pM\pZ,'\!\@\^\&\*\(\)\+\-\!\/_=:\.]*)"]="listings/wish_list$1";
#mod for comparison
$route["compare-listings-id-([\pN]+)-([\pN]+)-([\pN]+)-([\pN]+)"]="listings/comparison/id:$1/id:$2/id:$3/id:$4";
$route["compare-listings-id-([\pN]+)-([\pN]+)-([\pN]+)"]="listings/comparison/id:$1/id:$2/id:$3";
$route["compare-listings-id-([\pN]+)-([\pN]+)"]="listings/comparison/id:$1/id:$2";
$route["compare-listings-id-([\pN]+)"]="listings/comparison/id:$1";
#end mod for comparison
$route["user-([\pL\pN\pM\pZ,'\!\@\^\&\*\(\)\+\-\!\/_=:\.]*)-([\pL\pN\pM\pZ,'\!\@\^\&\*\(\)\+\-\!\/_=:\.]*)-([\pN]+)-([\pL\pN\pM\pZ,'\!\@\^\&\*\(\)\+\-\!\/_=:\.]+)-([\pL\pN\pM\pZ,'\!\@\^\&\*\(\)\+\-\!\/_=:\.]+)"]="users/view/id:$3/section:$4/pdf:$5";
$route["users-index-([\pL\pN\pM\pZ,'\!\@\^\&\*\(\)\+\-\!\/_=:\.]+)"]="users/index/user_type:$1";
$route["users-search-([\pL\pN\pM\pZ,'\!\@\^\&\*\(\)\+\-\!\/_=:\.]+)-([\pL\pN\pM\pZ,'\!\@\^\&\*\(\)\+\-\!\/_=:\.]+)"]="users/search/user_type:$1/keyword:$2";
$route["registration-as-private-person([\pL\pN\pM\pZ,'\!\@\^\&\*\(\)\+\-\!\/_=:\.]*)"]="users/reg_private$1";
$route["registration-as-company([\pL\pN\pM\pZ,'\!\@\^\&\*\(\)\+\-\!\/_=:\.]*)"]="users/reg_company$1";
$route["registration-as-agent([\pL\pN\pM\pZ,'\!\@\^\&\*\(\)\+\-\!\/_=:\.]*)"]="users/reg_agent$1";
#mod for news#
$route["rumahdijual([\pL\pN\pM\pZ,'\!\@\^\&\*\(\)\+\-\!\/_=:\.]*)"]="start/index$1";
$route["berita/rumahmurah([\pL\pN\pM\pZ,'\!\@\^\&\*\(\)\+\-\!\/_=:\.]*)"]="news/index$1";
$route["berita-([\pL\pN\pM\pZ,'\!\@\^\&\*\(\)\+\-\!\/_=:\.]+)"]="news/view/gid:$1";
#end of mod#

#mod for news cms#
$route["news-add_news_edit"]="news/edit";
#end of mod#
