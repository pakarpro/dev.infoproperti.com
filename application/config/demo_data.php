<?php if (!defined('BASEPATH')) 	exit('No direct script access allowed');

$config["login_settings"] = array(
	'admin' => array(
		'login' => "admin",
		'password' => "admin1"
	),
	'private' => array(
		'login' => "amanda@gmail.com",
		'password' => "123456"
	),
	'company' => array(
		'login' => "pg.jake@gmail.com",
		'password' => "123456"
	),
	'agent' => array(
		'login' => "andrew@domain.com",
		'password' => "123456"
	),
	'modinstaller' => array(
		'login' => "modinstaller",
		'password' => "123456"
	),
	'default' => array(
		'login' => "pg.jake@gmail.com",
		'password' => "123456"
	)
);

