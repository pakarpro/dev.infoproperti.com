<?php
/**
 * Hook
 * set default css and js files
 */
if ( ! function_exists('show_dbdata')){
	define("SHOW_PROFILING_QUERIES", true);
	define("SHOW_PROFILING_SLOWESTS", false);
	define("SHOW_PROFILING_QUERIES_REPEAT", false);

	function show_dbdata(){
		if(!USE_PROFILING){
			return;
		}

		global $start_exec_time;
		$result["all_time"] = getmicrotime() - $start_exec_time;

		$CI = &get_instance();
		$queries = $CI->db->queries;
		$query_times = $CI->db->query_times;
		
		$all_time = 0;
		$queries_index_cache = array();
		$queries_times_cache = array();
		$index = 1;
		foreach((array)$queries as $key => $query){
			$query = preg_replace("/\n/i", " ", $query);
			$md5 = md5($query);

			if(isset($queries_index_cache[$md5]) && $tIndex = $queries_index_cache[$md5]){
				$result["queries"][$tIndex]["time"][] = $query_times[$key];
				$result["queries"][$tIndex]["used"]++;
			}else{
				$result["queries"][$index]["query"] = $query;
				$result["queries"][$index]["used"] = 1;
				$result["queries"][$index]["time"][] = $query_times[$key];
				$queries_index_cache[$md5] = $index;
				$index++;
			}
			
			if(!isset($queries_times_cache[$md5]) || $queries_times_cache[$md5] < $query_times[$key])  $queries_times_cache[$md5] = $query_times[$key];
			

			$all_time += $query_times[$key];
		}
		foreach((array)$result["queries"] as $key => $query_data){
			$result["queries"][$key]["all_time"] = array_sum($query_data["time"]);
		}
		$result["query_all_time"] = $all_time;
		arsort($queries_times_cache);
		$slowests = array_slice($queries_times_cache, 0, 5);
		
		echo "<pre>";
		echo "<b>Unique queries count:</b> ".count($result["queries"])."<br>";
		echo "<b>Queries time:</b> ".$result["query_all_time"]."<br>";
		echo "<b>All time:</b> ".$result["all_time"]."<br>";

		if(SHOW_PROFILING_SLOWESTS){
			echo "<b>Slowest queries: </b><br>";
			foreach($slowests as $md5 => $time){
				echo "&nbsp;&nbsp;&nbsp;".$result["queries"][$queries_index_cache[$md5]]["query"].": ".$time."<br>";
			}
		}

		if(SHOW_PROFILING_QUERIES){
			echo "queries: ".print_r($result["queries"], true)."<br>";
		}
		
		if(SHOW_PROFILING_QUERIES_REPEAT){
			$repeat = array();
			foreach($result["queries"] as $query){
				if($query['used'] > 1) $repeat[] = $query;
			}
			echo "queries: ".print_r($repeat, true)."<br>";
		}
		
		echo "</pre>";
	}
}
