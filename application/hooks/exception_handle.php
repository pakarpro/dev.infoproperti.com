<?php

if (! defined("BASEPATH")) exit("No direct script access allowed");

function exception_handle(){
	set_exception_handler("_pg_exception_handler");	
}

function _pg_exception_handler($exception){
	$CI = &get_instance();	

	if(!isset($CI->config)){
		$CI->config = & load_class('Config');
	}

	if(!isset($CI->load)){
		
		// Assign all the class objects that were instantiated by the
		// front controller to local class variables so that CI can be
		// run as one big super object.
		$classes = array(
			'config'	=> 'Config',
			'input'		=> 'Input',
			'benchmark'	=> 'Benchmark',
			'uri'		=> 'URI',
			'output'	=> 'Output',
			'lang'		=> 'Language',
			'router'	=> 'Router'
		);
	
		foreach ($classes as $var => $class){
			$CI->$var =& load_class($class);
		}
	
		// In PHP 5 the Loader class is run as a discreet
		// class.  In PHP 4 it extends the Controller
		if (floor(phpversion()) >= 5){
			$CI->load =& load_class('Loader');
			$CI->load->_ci_autoloader();
		}else{
			$CI->_ci_autoloader();
		
			// sync up the objects since PHP4 was working from a copy
			foreach (array_keys(get_object_vars($CI)) as $attribute){
				if (is_object($CI->$attribute)){
					$CI->load->$attribute =& $CI->$attribute;
				}
			}
		}
	}
	
	if(!isset($CI->template_lite)){
		$CI->load->library("template_lite");
	}
	
	if(!isset($CI->pg_exceptions)){
		$CI->load->library("exceptions");
		$CI->load->library("pg_exceptions");
	}
	
	// Выводим текст об исключении на экран
    $CI->pg_exceptions->process($exception);
		
	exit;
}

function _pg_error_handler($severity, $message, $filepath, $line){
	
	
}