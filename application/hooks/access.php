<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if ( ! function_exists('check_access')){

	function check_access(){
		if(INSTALL_MODULE_DONE){

			$CI = &get_instance();
		
			$module = $CI->router->fetch_class();
			$controller = $CI->router->fetch_class(true);
			$method = $CI->router->fetch_method();

			$access = $CI->pg_module->get_module_method_access($module, $controller, $method);
			
			$is_private_method = (substr($method, 0, 1) == '_')?true:false;
			if(!$CI->pg_module->is_module_installed($module) && $module != 'install'){
				show_404();
			}
			
			if(!$access && !$is_private_method && $CI->pg_module->is_module_method_exists($module, $controller, $method)){
				$access = 2;
				$CI->pg_module->set_module_methods_access($module, $controller, array($method=>$access));
			}

			if(!$access){
				show_404();
			}

			$auth_type = $CI->session->userdata("auth_type");

			switch($auth_type){
				case "module": $allow = ($access<=1 || $access==4)?true:false; break;
				case "admin": $allow = ($access<=1 || $access==3)?true:false; break;
				case "user": $allow = ($access<=2)?true:false; break;
				default: $allow = ($access<=1)?true:false;
			}

			if(!$allow){
				show_404();
			}
		}
	}

}
