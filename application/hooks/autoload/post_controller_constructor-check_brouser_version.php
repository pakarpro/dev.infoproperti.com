<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('check_brouser_version')){

	function check_brouser_version(){
		if(INSTALL_MODULE_DONE){
			$CI = &get_instance();
			$CI->load->library('user_agent');
			if ($CI->agent->is_browser()){
				$CI->config->load('available_brousers', TRUE);
				$version = $CI->config->item($CI->agent->browser(), 'available_brousers');
				if ($CI->agent->version() < $version){
					$CI->template_lite->assign('display_brouser_error', 1);
					//$CI->session->set_userdata("already_display_brouser_error", '1');
				}
			}
		}
		return;
	}

}
