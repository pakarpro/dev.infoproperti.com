<?php
/**
* Add css and js files on any page
*
* @package PG_Core
* @subpackage application
* @category	helpers
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Mikhail Makeev <mmakeev@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2009-12-02 15:07:07 +0300 () $ $Author: irina $
**/

if ( ! function_exists('css'))
{
	function css($load_type='')
	{
		$CI = &get_instance();
		$load_type_array = explode("|", $load_type);

		if(is_array($load_type_array) && in_array("ui", $load_type_array)){
			$CI->pg_theme->add_css('application/js/jquery-ui/jquery-ui.custom.css');
		}
		
		///// preview mode
		if( $CI->input->get('template_preview_mode')){
			$preview_theme = '';
			$preview_scheme = '';
			//$preview_theme = $_SESSION["preview_theme"];
			//$preview_scheme = $_SESSION["preview_scheme"];
		}else{
			$preview_theme = '';
			$preview_scheme = '';
		}

		$css_html = $CI->pg_theme->get_include_css_code($preview_theme, $preview_scheme );
		echo $css_html;
	}
}

if ( ! function_exists('js'))
{
	function js($load_type='')
	{
		$CI = &get_instance();
		$load_type_array = explode("|", $load_type);

		$CI->pg_theme->add_js('jquery.min.js');
		$CI->pg_theme->add_js('functions.js');
		$CI->pg_theme->add_js('errors.js');
		$CI->pg_theme->add_js('loading.js');
		$CI->pg_theme->add_js('loading_content.js');

		if(is_array($load_type_array) && in_array("ui", $load_type_array)){
			$CI->pg_theme->add_js('jquery-ui.custom.min.js');
			// Dateppicker langs
			$lang = $CI->pg_language->get_lang_by_id($CI->pg_language->current_lang_id);
			$CI->pg_theme->add_js("datepicker-langs/jquery.ui.datepicker-{$lang['code']}.js");
		}
		if(is_array($load_type_array) && in_array("editable", $load_type_array)){
			$CI->pg_theme->add_js('jquery.jeditable.mini.js');
		}

		$css_html = $CI->pg_theme->get_include_js_code();
		echo $css_html;
	}
}
