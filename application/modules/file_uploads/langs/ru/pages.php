<?php

$install_lang["actions"] = "Действия";
$install_lang["admin_header_config_add"] = "Загрузки";
$install_lang["admin_header_config_change"] = "Редактировать файл";
$install_lang["admin_header_configs_list"] = "Загрузки";
$install_lang["archives"] = "Архивы";
$install_lang["audio"] = "Аудио";
$install_lang["documents"] = "Документы";
$install_lang["error_empty_file_formats"] = "Не указан формат файлов";
$install_lang["error_gid_invalid"] = "Неверное системное имя";
$install_lang["error_title_invalid"] = "Неверный заголовок";
$install_lang["field_file_formats"] = "Форматы";
$install_lang["field_gid"] = "Системное имя";
$install_lang["field_max_size"] = "Max размер";
$install_lang["field_name"] = "Название";
$install_lang["field_name_format"] = "Название файла";
$install_lang["graphics"] = "Графика";
$install_lang["images"] = "Изображения";
$install_lang["int_unlimit_condition"] = "(0 - без ограничений)";
$install_lang["link_add_config"] = "Добавить настройку";
$install_lang["link_delete_config"] = "Удалить настройку";
$install_lang["link_edit_config"] = "Редактировать настройку";
$install_lang["no_configs"] = "Настройки отсутствуют";
$install_lang["note_delete_config"] = "Вы уверены, что хотите удалить эту настройку?";
$install_lang["others"] = "Прочее";
$install_lang["success_added_config"] = "Настройка добавлена";
$install_lang["success_deleted_config"] = "Настройка удалена";
$install_lang["success_updated_config"] = "Настройка сохранена";
$install_lang["test"] = "Тест";
$install_lang["video"] = "Видео";

