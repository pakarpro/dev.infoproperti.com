<?php

$install_lang["actions"] = "Actions";
$install_lang["admin_header_config_add"] = "File uploads";
$install_lang["admin_header_config_change"] = "Edit file";
$install_lang["admin_header_configs_list"] = "Files uploads";
$install_lang["archives"] = "Archives";
$install_lang["audio"] = "Audio";
$install_lang["documents"] = "Documents";
$install_lang["error_empty_file_formats"] = "Empty file formats";
$install_lang["error_gid_invalid"] = "Invalid keyword";
$install_lang["error_title_invalid"] = "Invalid title";
$install_lang["field_file_formats"] = "Formats";
$install_lang["field_gid"] = "Setting keyword";
$install_lang["field_max_size"] = "Max size";
$install_lang["field_name"] = "Setting name";
$install_lang["field_name_format"] = "File name";
$install_lang["graphics"] = "Graphics";
$install_lang["images"] = "Images";
$install_lang["int_unlimit_condition"] = "(0 - unlimited)";
$install_lang["link_add_config"] = "Add setting";
$install_lang["link_delete_config"] = "Delete setting";
$install_lang["link_edit_config"] = "Edit setting";
$install_lang["no_configs"] = "No settings";
$install_lang["note_delete_config"] = "Are you sure you want to delete the setting?";
$install_lang["others"] = "Others";
$install_lang["success_added_config"] = "Files upload setting is successfully added";
$install_lang["success_deleted_config"] = "Files upload setting is deleted";
$install_lang["success_updated_config"] = "Files upload setting is successfully saved";
$install_lang["test"] = "Test";
$install_lang["video"] = "Video";

