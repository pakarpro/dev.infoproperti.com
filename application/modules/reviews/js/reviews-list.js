function ReviewsList(optionArr){
	this.properties = {
		siteUrl: '',
		listAjaxUrl: 'reviews/ajax_get_reviews',
		listBlockId: 'reviews_list',
		order: 'date_add',
		orderDirection: 'DESC',
		page: 1,
		errorObj: new Errors(),
		tIds: []
	}

	var _self = this;

	this.Init = function(options){
		_self.properties = $.extend(_self.properties, options);
		_self.init_links();
	}

	this.init_links = function(){
		if(_self.properties.tIds.length){
			for(var index in _self.properties.tIds){
				var id = _self.properties.tIds[index];
				$('#'+id+' select').live('change', function(){
					_self.properties.order = $(this).val();
					_self.loading_block();
					return false;
				});
				$('#'+id+' input[name=sorter_btn]').live('click', function(){
					if(_self.properties.orderDirection == 'ASC'){
						_self.properties.orderDirection = 'DESC';
					}else{
						_self.properties.orderDirection = 'ASC';
					}
					_self.loading_block();
					return false;
				});
				$('#'+id+'>.pages a[data-page]').live('click', function(){
					_self.properties.page = $(this).attr('data-page');
					_self.loading_block();
					return false;
				});				
			}
		}
	}

	this.loading_block = function(url){
		if(!url){
			url = _self.properties.siteUrl + _self.properties.listAjaxUrl + '/' +
				  _self.properties.order + '/' + _self.properties.orderDirection + '/' + 
				  _self.properties.page;
		}
		$.ajax({
			url: url, 
			type: 'GET',
			cache: false,
			success: function(data){
				$('#'+_self.properties.listBlockId).html(data);
			}
		});
	}
	
	_self.Init(optionArr);
}
