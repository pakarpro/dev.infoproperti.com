<?php 

/**
* Reviews user side controller
* 
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/
class Reviews extends Controller{
	
	/**
	 * Constructor
	 *
	 * @return Contact
	 */
	function __construct(){
		parent::Controller();

		$this->load->model("Reviews_model");
	}
	
	/**
	 * Send review 
	 */
	public function ajax_send_review(){
		if($this->session->userdata("auth_type") != "user"){
			$return["error"] = l('error_guest', 'reviews');
			echo json_encode($return);
			return;
		}
			
		$post_data = $this->input->post("data", true);
		$post_data["id_object"] = intval($this->input->post("object_id", true));
		$post_data["gid_type"] = $this->input->post("type_gid", true);
		$post_data["id_poster"] = intval($this->session->userdata("user_id", true));
		$post_data["id_responder"] = $this->input->post("responder_id", true);
		$post_data["rating_data"] = $this->input->post("rating_data", true);
		$post_data["agree"] = intval($this->input->post("agree", true));
		
		$is_owner = $this->input->post("is_owner", true);
		if($is_owner){
			$return["error"] = l('error_is_owner', 'reviews');
			echo json_encode($return);
			return;
		}

		$validate_data = $this->Reviews_model->validate_review(null, $post_data);		
		if(!$post_data["agree"]){
			$validate_data["errors"][] = l("error_empty_agree", "reviews");
		}
		if(!empty($validate_data["errors"])){
			$return["error"] = implode("<br>", $validate_data["errors"]);
			echo json_encode($return);
			return;
		}else{			
			$id = $this->Reviews_model->save_review(null, $validate_data["data"]);
			
			$review = $this->Reviews_model->get_review_by_id($id, true);

			$this->load->model("Notifications_model");

			$data = array(
				"user" 		=> $review["responder"]["output_name"],
				"poster" 	=> $review["poster"]["output_name"],
				"type" 	 	=> $review["type"]["output_name"],
				"object_id" => $review["id_object"],
				"review" 	=> $review["message"],
			);
			$this->Notifications_model->send_notification($review['responder']['email'], $this->Reviews_model->notification_user_review_gid, $data);

			//send notification
			if($this->pg_module->get_module_config('reviews', 'reviews_use_alerts')){
				$email = $this->pg_module->get_module_config('reviews', 'reviews_alert_email');				
				$data = array(
					"user" 		=> $review["responder"]["output_name"],	
					"poster" 	=> $review["poster"]["output_name"],
					"type" 	 	=> $review["type"]["output_name"],
					"object_id" => $review["id_object"],
					"review" 	=> $review["message"],
				);				
				$this->Notifications_model->send_notification($email, $this->Reviews_model->notification_auser_review_gid, $data);
			}
	
			$return['id'] = $id;
			$return["success"] = l("success_reviews_created", "reviews");
			echo json_encode($return);
			return;
		}
	}
	
	/**
	 * Get reviews
	 * @param string $type_gid
	 * @param integer $object_id
	 * @param string $order
	 * @param string $order_direction
	 * @param integer $page
	 */
	public function ajax_get_reviews($type_gid=null, $object_id=null, $order=null, $order_direction=null, $page=null){
		$this->load->helper('reviews');
		echo get_reviews_list($type_gid, $object_id, $order, $order_direction, $page);
	}
	
	/**
	 * Return review form by ajax
	 */
	public function ajax_get_form(){
		$this->load->model("reviews/models/Reviews_type_model");
	
		$type_gid = $this->input->post("type_gid", true);	
		$type = $this->Reviews_type_model->get_type($type_gid, true);
		$this->template_lite->assign("type", $type);
		
		$object_id = intval($this->input->post("object_id", true));
		$this->template_lite->assign("object_id", $object_id);
		
		$responder_id = intval($this->input->post("responder_id", true));
		$this->template_lite->assign("responder_id", $responder_id);		
		
		$is_owner = $this->input->post("is_owner", true);
		$this->template_lite->assign("is_review_owner", $is_owner);
		
		$rand = $this->input->post("rand", true);	
		$this->template_lite->assign("rand", $rand);
		
		$this->template_lite->view("send_review_form");
		return;
	}
	
	/**
	 * Reply to review
	 * @param integer $review_id
	 */
	public function ajax_reply($review_id){
		$this->Reviews_model->set_format_settings(array('use_format'=>false));
		$review = $this->Reviews_model->get_review_by_id($review_id);
		$this->Reviews_model->set_format_settings(array('use_format'=>true));
		
		$user_id = $this->session->userdata("user_id", true);
		if($review["id_responder"] != $user_id){
			$return["error"] = l('error_reply', 'reviews');
			echo json_encode($return);
			exit;
		}
	
		$post_data = $this->input->post("data", true);
		$post_data["date_answer"] = date("Y-m-d H:i:s");
		$validate_data = $this->Reviews_model->validate_reply($review_id, $post_data);
		if(!empty($validate_data["errors"])){
			$return["error"] = implode("<br>", $validate_data["errors"]);
			echo json_encode($return);
			exit;
		}else{
			$this->Reviews_model->save_review($review_id, $validate_data["data"]);
			
			$review = $this->Reviews_model->get_review_by_id($review_id, true);
			
			$this->load->model('Notifications_model');
			
			$data = array(
				'user' 		=> $review['poster']['output_name'],
				'responder' => $review['responder']['output_name'],
				'type' 	 	=> $review['type']['output_name'],
				'object_id' => $review['id_object'],
				'review' 	=> $review['message'],
				'comment'	=> $review['answer'],
			);
			$this->Notifications_model->send_notification($review['responder']['email'], $this->Reviews_model->notification_user_reply_gid, $data);
			
			if($this->pg_module->get_module_config('reviews', 'reviews_use_alerts')){
				$email = $this->pg_module->get_module_config('reviews', 'reviews_alert_email');				
				$data = array(
					'user' 	=> $this->pg_module->get_module_config('reviews', 'reviews_alert_name'),	
					'responder' => $review['responder']['output_name'],
					'type' 	 	=> $review['type']['output_name'],
					'object_id' => $review['id_object'],
					'review' 	=> $review['message'],
					'comment'	=> $review['answer'],
				);				
				$this->Notifications_model->send_notification($email, $this->Reviews_model->notification_auser_reply_gid, $data);
			}
			
			$this->config->load("date_formats", TRUE);
			$date_format = $this->config->item("date_format_date_time_literal", "date_formats");
			$return["answer"] = $validate_data["data"]["answer"];
			$return["date_answer"] = date($date_format, strtotime($post_data["date_answer"]));
			$return["success"] = l("success_reviews_replied", "reviews");			
			echo json_encode($return);
			return;
		}
	}
	
	/**
	 * Return visitors block
	 * @param string $order
	 * @param string $order_direction
	 * @param integer $page
	 */
	private function _visitors_block($review_type, $field_name, $order="date_created", $order_direction="DESC", $page=1){
		$current_settings = isset($_SESSION["reviews_visitors_list"])?$_SESSION["reviews_visitors_list"]:array();
		if(!isset($current_settings["order"]))
			$current_settings["order"] = "date_add";
		if(!isset($current_settings["order_direction"]))
			$current_settings["order_direction"] = "DESC";
		if(!isset($current_settings["page"]))
			$current_settings["page"] = 1;
		if(!isset($current_settings["filters"]))
			$current_settings["filters"] = array();

		$visit_type = $field_name == 'id_responder' ? 'visitors' : 'visits';

		$filters = $current_settings["filters"];
		if($review_type != 'all') $filters['type'] = $review_type;
		
		if(!$order) $order = $current_settings["order"];
		$this->template_lite->assign("order", $order);
		$current_settings["order"] = $order;

		if (!$order_direction) $order_direction = $current_settings["order_direction"];
		$this->template_lite->assign("order_direction", $order_direction);
		$current_settings["order_direction"] = $order_direction;

		if(!$page) $page = $current_settings["page"];
		$items_on_page = $this->pg_module->get_module_config("start", "index_items_per_page");
	
		$sort_data = array(
			"url" => site_url()."reviews/".$visit_type.'/'.$review_type,
			"order" => $order,
			"direction" => $order_direction,
			"links" => array(
				"poster" => l("field_reviews_poster", "reviews"),
				"date_add" => l("field_reviews_date_add", "reviews"),
			)
		);
		$this->template_lite->assign("sort_data", $sort_data);
	
		$reviews_count = $this->Reviews_model->get_reviews_count_by_filters($filters);
		if($reviews_count > 0){
			$order_array = array($order => $order_direction, 'id'=>$order_direction);
			$visitors = $this->Reviews_model->get_reviews_list_by_filters($filters, $page, $items_on_page, $order_array);
			$reviews_for_search = array();		
			foreach($visitors as $value){
				$reviews_for_search[] = $value['id'];
			}
			$reviews_by_ids = array();
			$results = $this->Reviews_model->get_reviews_list_by_filters(array('ids'=>$reviews_for_search));
			foreach($results as $result){
				$reviews_by_ids[$result['id']] = $result;
			}
			$reviews = array();
			foreach($reviews_for_search as $review_id){
				$reviews[$review_id] = isset($reviews_by_ids[$review_id]) ? $reviews_by_ids[$review_id] :
					$this->Reviews_model->format_default_review($review_id);
			}
			$this->template_lite->assign("reviews", $reviews);	
		}
		$this->template_lite->assign('reviews_count', $reviews_count);
	
		$this->load->helper("sort_order");
		$page = get_exists_page_number($page, $reviews_count, $items_on_page);
		$current_settings["page"] = $page;

		$this->load->helper("navigation");
		$this->config->load("date_formats", TRUE);
		$url = site_url().'reviews/'.$visit_type.'/'.$review_type.'/'.$order.'/'.$order_direction.'/';
		$page_data = get_user_pages_data($url, $reviews_count, $items_on_page, $page, "briefPage");
		$page_data["date_format"] = $this->config->item("st_format_date_time_literal", "date_formats");
		$this->template_lite->assign("page_data", $page_data);
		
		$_SESSION["reviews_visitors_list"] = $current_settings;
		
		return $this->template_lite->fetch("visitors_block", "user", "reviews");
	}
	
	/**
	 * Render visitors list action
	 * @param string $order
	 * @param string $order_description
	 * @param integer $page
	 */
	public function visitors($review_type='all', $order="date_add", $order_direction="DESC", $page=1){
		$current_settings = isset($_SESSION["reviews_visitors_list"])?$_SESSION["reviews_visitors_list"]:array();
		
		$user_id = intval($this->session->userdata("user_id"));
		$filters = array('responder'=>$user_id);
		
		$current_settings["order"] = $order;
		$current_settings["order_direction"] = $order_direction;
		$current_settings["page"] = $page;
		$current_settings["filters"] = $filters;	
		$_SESSION["reviews_visitors_list"] = $current_settings;
	
		$review_block = $this->_visitors_block($review_type, 'id_responder', $order, $order_direction, $page);
		$this->template_lite->assign("block", $review_block);
		
		$this->template_lite->assign('review_type', $review_type);
		
		$this->load->model("Menu_model");
		$this->Menu_model->breadcrumbs_set_active(l('stat_reviews_visitors_'.$review_type, "reviews"));
		
		$this->template_lite->view("visitors", "user", "reviews");
	}
	
	/**
	 * Render visitors list action by ajax
	 * @param string $order
	 * @param string $order_description
	 * @param integer $page
	 */
	public function ajax_visitors($review_type='all', $order="date_add", $order_direction="DESC", $page=1){
		$current_settings = isset($_SESSION["reviews_visitors_list"])?$_SESSION["reviews_visitors_list"]:array();
		
		$user_id = intval($this->session->userdata("user_id"));
		$filters = array('responder'=>$user_id);
		
		$current_settings["order"] = $order;
		$current_settings["order_direction"] = $order_direction;
		$current_settings["page"] = $page;
		$current_settings["filters"] = $filters;	
		$_SESSION["reviews_visitors_list"] = $current_settings;
	
		echo $this->_visitors_block($review_type, 'id_responder', $order, $order_direction, $page);
	}
	
	/**
	 * Render visits list action
	 * @param string $order
	 * @param string $order_description
	 * @param integer $page
	 */
	public function visits($review_type='all', $order="date_add", $order_direction="DESC", $page=1){
		$current_settings = isset($_SESSION["reviews_visitors_list"])?$_SESSION["reviews_visitors_list"]:array();
		
		$user_id = intval($this->session->userdata("user_id"));
		$filters = array('poster'=>$user_id);
		
		$current_settings["order"] = $order;
		$current_settings["order_direction"] = $order_direction;
		$current_settings["page"] = $page;
		$current_settings["filters"] = $filters;		
		$_SESSION["reviews_visitors_list"] = $current_settings;
	
		$this->template_lite->assign('is_review_owner', 1);
	
		$review_block = $this->_visitors_block($review_type, 'id_poster', $order, $order_direction, $page);
		$this->template_lite->assign("block", $review_block);
		
		$this->template_lite->assign('review_type', $review_type);
		
		$this->load->model("Menu_model");
		$this->Menu_model->breadcrumbs_set_active(l('stat_reviews_visits_'.$review_type, "reviews"));
		
		$this->template_lite->view("visits", "user", "reviews");
	}
	
	/**
	 * Render visits list action by ajax
	 * @param string $order
	 * @param string $order_description
	 * @param integer $page
	 */
	public function ajax_visits($review_type='all', $order="date_add", $order_direction="DESC", $page=1){
		$current_settings = isset($_SESSION["reviews_visitors_list"])?$_SESSION["reviews_visitors_list"]:array();
		
		$user_id = intval($this->session->userdata("user_id"));
		$filters = array('poster'=>$user_id);
		
		$current_settings["order"] = $order;
		$current_settings["order_direction"] = $order_direction;
		$current_settings["page"] = $page;
		$current_settings["filters"] = $filters;		
		$_SESSION["reviews_visitors_list"] = $current_settings;
	
		$this->template_lite->assign('is_review_owner', 1);
	
		echo $this->_visitors_block($review_type, 'id_poster', $order, $order_direction, $page);
	}
}
