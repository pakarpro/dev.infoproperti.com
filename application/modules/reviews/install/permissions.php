<?php

$_permissions["admin_reviews"]["ajax_types_rate_delete"] = "3";
$_permissions["admin_reviews"]["delete"] = "3";
$_permissions["admin_reviews"]["edit"] = "3";
$_permissions["admin_reviews"]["index"] = "3";
$_permissions["admin_reviews"]["settings"] = "3";
$_permissions["admin_reviews"]["show"] = "3";
$_permissions["admin_reviews"]["types"] = "3";
$_permissions["admin_reviews"]["types_edit"] = "3";

$_permissions["api_reviews"]["reply"] = "2";
$_permissions["api_reviews"]["get_review"] = "1";
$_permissions["api_reviews"]["send_review"] = "1";

$_permissions["reviews"]["ajax_get_form"] = "1";
$_permissions["reviews"]["ajax_get_reviews"] = "1";
$_permissions["reviews"]["ajax_send_review"] = "1";

$_permissions["reviews"]["visitors"] = "2";
$_permissions["reviews"]["ajax_visitors"] = "2";
$_permissions["reviews"]["visits"] = "2";
$_permissions["reviews"]["ajax_visits"] = "2";
