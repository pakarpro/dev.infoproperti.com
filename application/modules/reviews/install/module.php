<?php
$module["module"] = "reviews";
$module["install_name"] = "Reviews";
$module["install_descr"] = "User reviews of profiles & listings";
$module["version"] = "1.01";
$module["files"] = array(
	array("file", "read", "application/modules/reviews/controllers/admin_reviews.php"),
	array("file", "read", "application/modules/reviews/controllers/api_reviews.php"),
	array("file", "read", "application/modules/reviews/controllers/reviews.php"),
	array("file", "read", "application/modules/reviews/helpers/reviews_helper.php"),
	array("file", "read", "application/modules/reviews/install/module.php"),
	array("file", "read", "application/modules/reviews/install/permissions.php"),
	array("file", "read", "application/modules/reviews/install/settings.php"),
	array("file", "read", "application/modules/reviews/install/structure_deinstall.sql"),
	array("file", "read", "application/modules/reviews/install/structure_install.sql"),
	array("file", "read", "application/modules/reviews/js/reviews.js"),
	array("file", "read", "application/modules/reviews/js/reviews-form.js"),
	array("file", "read", "application/modules/reviews/js/reviews-list.js"),
	array("file", "read", "application/modules/reviews/models/reviews_install_model.php"),
	array("file", "read", "application/modules/reviews/models/reviews_model.php"),
	array("file", "read", "application/modules/reviews/models/reviews_type_model.php"),
	array("file", "read", "application/modules/reviews/views/admin/css/stars.png"),
	array("file", "read", "application/modules/reviews/views/admin/css/style-ltr.css"),
	array("file", "read", "application/modules/reviews/views/admin/css/style-rtl.css"),
	array("file", "read", "application/modules/reviews/views/admin/helper_admin_home_block.tpl"),
	array("file", "read", "application/modules/reviews/views/admin/helper_home_reviews_block.tpl"),
	array("file", "read", "application/modules/reviews/views/admin/reviews_edit.tpl"),
	array("file", "read", "application/modules/reviews/views/admin/reviews_list.tpl"),
	array("file", "read", "application/modules/reviews/views/admin/reviews_view.tpl"),
	array("file", "read", "application/modules/reviews/views/admin/settings.tpl"),
	array("file", "read", "application/modules/reviews/views/admin/types_edit.tpl"),
	array("file", "read", "application/modules/reviews/views/admin/types_list.tpl"),
	array("file", "read", "application/modules/reviews/views/default/css/style-ltr.css"),
	array("file", "read", "application/modules/reviews/views/default/css/style-rtl.css"),
	array("file", "read", "application/modules/reviews/views/default/css/stars.png"),
	array("file", "read", "application/modules/reviews/views/default/helper_get_rate.tpl"),
	array("file", "read", "application/modules/reviews/views/default/helper_get_reviews.tpl"),
	array("file", "read", "application/modules/reviews/views/default/helper_reviews_visitors.tpl"),
	array("file", "read", "application/modules/reviews/views/default/helper_reviews_visits.tpl"),
	array("file", "read", "application/modules/reviews/views/default/helper_send_review.tpl"),
	array("file", "read", "application/modules/reviews/views/default/review_form.tpl"),
	array("file", "read", "application/modules/reviews/views/default/reviews_list.tpl"),	
	array("file", "read", "application/modules/reviews/views/default/send_review_form.tpl"),
	array("file", "read", "application/modules/reviews/views/default/visitors_block.tpl"),
	array("file", "read", "application/modules/reviews/views/default/visitors.tpl"),
	array("file", "read", "application/modules/reviews/views/default/visits.tpl"),
	array("file", "dir", "application/modules/reviews/langs"),
);
$module["dependencies"] = array(
	"start" => array("version"=>"1.01"),
	"menu" => array("version"=>"1.01"),
	"notifications" => array("version"=>"1.03"),
);

$module["linked_modules"] = array(
	"install" => array(
		"menu"			=> "install_menu",
		"notifications"	=> "install_notifications",
		"spam"			=> "install_spam",
		"moderation"	=> "install_moderation",
		"ausers"		=> "install_ausers",
	),
	"deinstall" => array(
		"menu"			=> "deinstall_menu",
		"notifications"	=> "deinstall_notifications",
		"spam"			=> "deinstall_spam",
		"moderation"	=> "deinstall_moderation",
		"ausers"		=> "deinstall_ausers",
	)
);
