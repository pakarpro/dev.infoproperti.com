DROP TABLE IF EXISTS `[prefix]reviews`;
CREATE TABLE IF NOT EXISTS `[prefix]reviews` (
  `id` int(3) NOT NULL auto_increment,
  `gid_type` varchar(20) NOT NULL,
  `id_object` int(11) NOT NULL,
  `id_poster` int(11) NOT NULL,
  `id_responder` int(11) NOT NULL,
  `rating_data` text NULL,
  `message` text NULL,
  `answer` text NOT NULL,
  `date_add` DATETIME NOT NULL,
  `date_answer` DATETIME NOT NULL,
  `banned` tinyint(1) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `gid_type` (`gid_type`, `id_object`, `id_poster`),
  KEY `date_add` (`date_add`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `[prefix]reviews_types`;
CREATE TABLE IF NOT EXISTS `[prefix]reviews_types` (
  `id` int(3) NOT NULL auto_increment,
  `gid` varchar(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `rate_type` varchar(20) NOT NULL,
  `module` varchar(20) NOT NULL,
  `model` varchar(20) NOT NULL,
  `callback` varchar(255) NOT NULL,
  `date_add` DATETIME NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `gid` (`gid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
