<?php

/**
* Reviews helpers
* 
* @package PG_RealEstate
* @subpackage application
* @category	helpers
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Mikhail Makeev <mmakeev@pilotgroup.net>
* @version $Revision: 68 $ $Date: 2010-01-11 16:02:23 +0300 (Пн, 11 янв 2010) $ $Author: irina $
**/
if(!function_exists("send_review_block")){
	/**
	 * Show reviews button
	 * @param array $data
	 * @return html
	 */
	function send_review_block($data){
		$CI = &get_instance();
		$tpl = &$CI->template_lite;
	
		if(!isset($data["object_id"]) || !intval($data["object_id"]) ||
		   !isset($data["type_gid"]) || !$data["type_gid"])
		   return "";
		   
		static $is_login_enabled = null;
		if(is_null($is_login_enabled)){
			$CI->load->helper('users');
			$is_login_enabled = is_login_enabled();
		}
		if(!$is_login_enabled) return '';
				   
		if(isset($data["is_owner"]) && !empty($data["is_owner"])) return '';
		   
		$CI->load->model("Reviews_model");
		$CI->load->model("reviews/models/Reviews_type_model");
		
		$type = $CI->Reviews_type_model->get_type($data["type_gid"], true);
		
		$tpl->assign("type", $type);
		$tpl->assign("object_id", $data["object_id"]);
	
		$data["responder_id"] = isset($data["responder_id"]) ? intval($data["responder_id"]) : 0;
		$tpl->assign("responder_id", $data["responder_id"]);
		
		if(!isset($data["template"])) $data["template"] = "button";
		$tpl->assign("template", $data["template"]);
		
		$is_guest = $CI->session->userdata("auth_type") != "user";
		$CI->template_lite->assign("is_guest", $is_guest);
		
		if($CI->session->userdata("auth_type") == 'user'){
			$user_id = $CI->session->userdata("user_id");
			$is_send = $CI->Reviews_model->is_review_from_poster($type["gid"], $user_id, $data["object_id"]);
			$CI->template_lite->assign('is_send', $is_send);
		}else{
			$CI->template_lite->assign('is_send', 0);
		}
		
		$success = '';
		if(!isset($data["success"])) $success = $data["success"];
		$CI->template_lite->assign("success", $data["success"]);
		
		$CI->template_lite->assign('rand', rand(100000, 999999));
				
		$html = $tpl->fetch("helper_send_review", "user", "reviews");
		return $html;
	}
}

if(!function_exists("get_rate_block")){
	/**
	 * Get rate block
	 * @param array $data
	 * @return html
	 */
	function get_rate_block($data){
		$CI = &get_instance();
		$tpl = &$CI->template_lite;
		$CI->load->model("reviews/models/Reviews_type_model");
	
		if(isset($data['type']) && !empty($data['type'])){
			$type = $data['type'];
		}elseif(isset($data["type_gid"]) && !empty($data["type_gid"])){
			$type = $CI->Reviews_type_model->get_type($data["type_gid"], true);
		}
		
		if(!$type) return "";		
		$tpl->assign("type", $type);
			
		$template = isset($data["template"]) ? $data["template"] : "extended";
		$tpl->assign("template", $template);
		
		$read_only = isset($data["read_only"]) ? true : false;
		$tpl->assign("read_only", $read_only);

		$ratings = array_keys($type["values"][$type["rate_type"]]);

		foreach($ratings as $rating){
			$rating_data[$rating] = isset($data["rating_data"][$rating]) ? $data["rating_data"][$rating] : 0;
			if(isset($data["rating_data_".$rating])){
				$rating_data[$rating] = $data["rating_data_".$rating];
			}
		}		
		$tpl->assign("rating_data", $rating_data);
		
		$show_label = isset($data["show_label"]) ? true : false;
		$tpl->assign("show_label", $show_label);
		
		$rand = rand(100000, 999999);
		$tpl->assign("rand", $rand);
		
		$symbolType = $type["rate_type"] == "stars" ? "previous" : "equal";
		$tpl->assign("symbolType", $symbolType);
		
		$html = $tpl->fetch("helper_get_rate", 'user', "reviews");
		return $html;
	}
}

if(!function_exists("get_reviews_block")){
	/**
	 * Get reviews list
	 * @param array $data
	 * @return html
	 */
	function get_reviews_block($data){
		$CI = &get_instance();
		
		if(!isset($data["object_id"]) || !intval($data["object_id"]) ||
		   !isset($data["type_gid"]) || !$data["type_gid"])
		   return "";
	   
		$CI->template_lite->assign("review_type_gid", $data["type_gid"]);
		$CI->template_lite->assign("review_object_id", $data["object_id"]);
		
		$block = get_reviews_list($data["type_gid"], $data["object_id"]);
		$CI->template_lite->assign('block', $block);
	
		$current_settings = isset($_SESSION["reviews_list"]) ? $_SESSION["reviews_list"] : array();
		if(!isset($current_settings["order"])) $current_settings["order"] = "date_add";
		$CI->template_lite->assign('order', $current_settings["order"]);
		
		if(!isset($current_settings["order_direction"])) $current_settings["order_direction"] = "DESC";
		$CI->template_lite->assign('order_direction', $current_settings["order_direction"]);
		
		if(!isset($current_settings["page"])) $current_settings["page"] = 1;
		$CI->template_lite->assign('page', $current_settings["page"]);
		
		$html = $CI->template_lite->fetch("helper_get_reviews", "user", "reviews");
		return $html;
	}
}

if(!function_exists("get_reviews_list")){
	/**
	 * Get reviews sorter
	 * @return array
	 */
	function get_reviews_list($type_gid=null, $object_id=null, $order=null, $order_direction=null, $page=null){
		$CI = &get_instance();
		$CI->load->model('Reviews_model');
		
		$attrs = array();

		$current_settings = isset($_SESSION["reviews_list"]) ? $_SESSION["reviews_list"] : array();
		if(!isset($current_settings["order"])) $current_settings["order"] = "date_add";
		if(!isset($current_settings["order_direction"])) $current_settings["order_direction"] = "DESC";
		if(!isset($current_settings["page"])) $current_settings["page"] = 1;
		
		if(!$order) $order = $current_settings["order"];
		$CI->template_lite->assign("order", $order);
		$current_settings["order"] = $order;

		if(!$order_direction) $order_direction = $current_settings["order_direction"];
		$CI->template_lite->assign("order_direction", $order_direction);
		$current_settings["order_direction"] = $order_direction;

		$filters = array('type'=>$type_gid, 'object'=>$object_id);

		$reviews_count = $CI->Reviews_model->get_reviews_count_by_filters($filters);
		
		if(!$page) $page = $current_settings["page"];
		$items_on_page = $CI->pg_module->get_module_config("start", "index_items_per_page");
	
		$sort_data = array(
			"url" => site_url()."reviews/get_reviews/".$type_gid."/".$object_id,
			"order" => $order,
			"direction" => $order_direction,
			"links" => array(
				"poster" => l("field_reviews_poster", "reviews"),
				"date_add" => l("field_reviews_date_add", "reviews"),
			)
		);
		$CI->template_lite->assign("sort_data", $sort_data);
		
		$CI->load->helper("sort_order");
		$page = get_exists_page_number($page, $reviews_count, $items_on_page);
		$current_settings["page"] = $page;

		$_SESSION["reviews_list"] = $current_settings;
		
		if($reviews_count > 0){
			if($order == "poster") $order = "id_poster";
			$reviews = $CI->Reviews_model->get_reviews_list_by_filters($filters, $page, $items_on_page, array($order => $order_direction));
			$CI->template_lite->assign("reviews", $reviews);
		}

		$CI->load->helper("navigation");
		$CI->config->load("date_formats", TRUE);
		$url = site_url()."reviews/get_reviews/".$type_gid."/".$object_id."/".$order."/".$order_direction."/";
		$page_data = get_user_pages_data($url, $reviews_count, $items_on_page, $page, "briefPage");
		$page_data["date_format"] = $CI->config->item("st_format_date_time_literal", "date_formats");
		$CI->template_lite->assign("page_data", $page_data);
		$CI->template_lite->assign("object_id", $object_id);
		
		$CI->template_lite->assign("user_id", intval($CI->session->userdata("user_id", true)));
		
		return $CI->template_lite->fetch("reviews_list", "user", "reviews");
	}
}

if(!function_exists("get_reviews_sorter")){
	/**
	 * Get reviews sorter
	 * @return array
	 */
	function get_reviews_sorter(){
		return array(
			"review_sorter"=>l("field_reviews_rate", "reviews"),
			"review_count"=>l("field_reviews_count", "reviews"),
		);
	}
}

if(!function_exists("get_reviews_fields")){
	/**
	 * Get reviews sorter
	 * @return array
	 */
	function get_reviews_fields(){
		return array("review_data", "review_count", "review_sorter", "review_type");
	}
}

if(!function_exists("admin_home_reviews_block")){
	/**
	 * Return homepage statistics
	 */
	function admin_home_reviews_block(){
		$CI = & get_instance();

		$auth_type = $CI->session->userdata("auth_type");
		if($auth_type != "admin") return "";

		$user_type = $CI->session->userdata("user_type");

		$show = true;

		$stat_reviews = array(
			"index_method" => true,
		);

		if($user_type == "moderator"){
			$show = false;
			$CI->load->model("Ausers_model");
			$methods_reviews = $CI->Ausers_model->get_module_methods("reviews");
			if((is_array($methods_reviews) && !in_array("index", $methods_reviews))){
				$show = true;
			}else{
				$permission_data = $CI->session->userdata("permission_data");
				if((isset($permission_data["reviews"]["index"]) && $permission_data["reviews"]["index"] == 1)){
					$show = true;
					$stat_reviews["index_method"] = (bool)$permission_data["reviews"]["index"];
				}
			}
		}

		if(!$show){
			return "";
		}

		$CI->load->model("Reviews_model");
		$CI->load->model("reviews/models/Reviews_type_model");
		
		$review_types = $CI->Reviews_type_model->get_types();
		
		if(empty($review_types)) return '';
		
		$stat_reviews['types'] = $review_types;
		foreach($review_types as $review_type){
			$stat_reviews['type_'.$review_type['gid']] = $CI->Reviews_model->get_reviews_count_by_filters(array('type'=>$review_type['gid']));
		}
		
		$CI->template_lite->assign("stat_reviews", $stat_reviews);
		return $CI->template_lite->fetch("helper_admin_home_block", "admin", "reviews");
	}
}

if(!function_exists("reviews_visitors_block")){
	/**
	 * Return reviews visitors
	 */
	function reviews_visitors_block($params){
		$CI = & get_instance();

		$user_id = intval($CI->session->userdata("user_id"));

		$CI->load->model("reviews/models/Reviews_model");
		$CI->load->model("reviews/models/Reviews_type_model");
		
		$types = $CI->Reviews_type_model->get_types();
		foreach($types as $type){
			if($params['type'] == $type['gid']){
				$CI->template_lite->assign('type', $type['gid']);
				
				$visitors_count_week = $CI->Reviews_model->get_reviews_count_by_filters(array('type'=>$type['gid'], 'responder'=>$user_id, 'week'=>1));
				$CI->template_lite->assign("visitors_count_week", $visitors_count_week);
		
				$visitors_count_month = $CI->Reviews_model->get_reviews_count_by_filters(array('type'=>$type['gid'], 'responder'=>$user_id, 'month'=>1));
				$CI->template_lite->assign("visitors_count_month", $visitors_count_month);
		
				return $CI->template_lite->fetch("helper_reviews_visitors", "user", "reviews");
			}
		}
		
		return '';
	}
}

if(!function_exists("reviews_visits_block")){
	/**
	 * Return reviews visits
	 */
	function reviews_visits_block($params){
		$CI = & get_instance();

		$user_id = intval($CI->session->userdata("user_id"));

		$CI->load->model("reviews/models/Reviews_model");
		$CI->load->model("reviews/models/Reviews_type_model");
		
		$types = $CI->Reviews_type_model->get_types();
		
		foreach($types as $type){
			if($params['type'] == $type['gid']){
				$CI->template_lite->assign('type', $type['gid']);
				
				$visits_count_week = $CI->Reviews_model->get_reviews_count_by_filters(array('type'=>$type['gid'], 'poster'=>$user_id, 'week'=>1));
				$CI->template_lite->assign("visits_count_week", $visits_count_week);
		
				$visits_count_month = $CI->Reviews_model->get_reviews_count_by_filters(array('type'=>$type['gid'], 'poster'=>$user_id, 'month'=>1));
				$CI->template_lite->assign("visits_count_month", $visits_count_month);
		
				return $CI->template_lite->fetch("helper_reviews_visits", "user", "reviews");
			}
		}
		
		return '';
	}
}
