{include file="header.tpl" load_type='ui'}
{include file="left_panel.tpl" module="start"}
<div class="rc">
	<div class="content-block">
		<h1>{l i='stat_reviews_visits_'+$review_type gid='reviews'} ({$reviews_count})</h1>
		<div id="reviews_list">{$block}</div>
		{js module=reviews file='reviews-list.js'}
		<script>{literal}
		$(function(){
			new ReviewsList({
				siteUrl: '{/literal}{$site_url}{literal}',
				listAjaxUrl: 'reviews/ajax_visitors/{/literal}{$review_type}{literal}',
				order: 'date_add',
				orderDirection: 'DESC',
				tIds: ['pages_block_1', 'pages_block_2', 'sorter_block']
			});
		});
		{/literal}</script>
	</div>
</div>
<div class="clr"></div>
{include file="footer.tpl"}
