{js module=reviews file='reviews-list.js'}
<div id="reviews_list">{$block}</div>
<script>{literal}
$(function(){
	new ReviewsList({
		siteUrl: '{/literal}{$site_url}{literal}', 
		listAjaxUrl: '{/literal}reviews/ajax_get_reviews/{$review_type_gid}/{$review_object_id}{literal}',
		order: '{/literal}{$order}{literal}',
		orderDirection: '{/literal}{$order_direction}{literal}',
		page: '{/literal}{$page}{literal}',
		tIds: ['pages_block_1', 'pages_block_2', 'sorter_block'],
	});
});
{/literal}</script>
