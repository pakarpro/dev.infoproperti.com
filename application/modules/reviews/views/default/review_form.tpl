	<form id="reviews_form_{$rand}" action="" method="POST">
	<div class="r">
		{block name=get_rate_block module=reviews type_gid=$type.gid template='extended' show_label='true'}
	</div>
	<div class="r">
		<div class="f">{l i='field_reviews_message' gid='reviews'}&nbsp;*:</div>
		<div class="v"><textarea name="data[message]" rows="5" cols="23"></textarea></div>
	</div>
	<div class="r">
		<input type="checkbox" name="agree" value="1" id="review_agree" /> <label for="review_agree">{l i='field_reviews_agree' gid='reviews'}</label>
	</div>
	<div class="r"><input type="submit" value="{l i='btn_send' gid='start' type='button'}" id="close_btn" /></div>
	<input type="hidden" name="type_gid" value="{$type.gid}">
	<input type="hidden" name="object_id" value="{$object_id}">
	<input type="hidden" name="responder_id" value="{$responder_id}">
	{if $is_review_owner}<input type="hidden" name="is_owner" value="1">{/if}
	</form>
