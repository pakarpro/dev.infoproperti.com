{include file="header.tpl"}
{helper func_name=get_admin_level1_menu helper_name=menu func_param='admin_reviews_menu'}
<div class="actions">&nbsp;</div>

<div class="menu-level3">
	<ul>
		{foreach item=item key=key from=$types}
		<li class="{if $type_gid eq $key}active{/if}"><a href="{$site_url}admin/reviews/index/{$key}">{$item}</a></li>
		{/foreach}
	</ul>
	&nbsp;
</div>
<table cellspacing="0" cellpadding="0" class="data" width="100%">
	<tr>
		<th class="first w100">{l i='field_reviews_poster' gid='reviews'}</th>
		<th class="w150">{l i='field_reviews_rate' gid='reviews'}</th>
		<th>{l i='field_reviews_message' gid='reviews'}</th>
		<th><a href="{$sort_links.date_add}"{if $order eq 'date_add'} class="{$order_direction|lower}"{/if}>{l i='field_reviews_date_add' gid='reviews'}</a></th>
		<th class="w70">&nbsp;</th>
	</tr>
	{foreach item=item from=$reviews}
	{counter print=false assign=counter}
	<tr{if $counter is div by 2} class="zebra"{/if}>				
		<td class="center">{$item.poster.output_name|truncate:50}</td>
		<td>{block name=get_rate_block module=reviews rating_data=$item.rating_data type_gid=$item.gid_type template='mini' read_only='true' mode='admin'}</td>
		<td>{$item.message|truncate:100}</td>
		<td class="center">{$item.date_add|date_format:$page_data.date_format}</td>
		<td class="icons">
			<a href="{$site_url}admin/reviews/edit/{$item.id}"><img src="{$site_root}{$img_folder}icon-edit.png" width="16" height="16" border="0" alt="{l i='link_reviews_edit' gid='reviews' type='button'}" title="{l i='link_reviews_edit' gid='reviews' type='button'}"></a>
			<a href="{$site_url}admin/reviews/delete/{$item.id}" onclick="javascript: if(!confirm('{l i='note_reviews_delete' gid='reviews' type='js'}')) return false;"><img src="{$site_root}{$img_folder}icon-delete.png" width="16" height="16" border="0" alt="{l i='link_reviews_delete' gid='reviews' type='button'}" title="{l i='link_reviews_delete' gid='reviews' type='button'}"></a>
		</td>
	</tr>
	{foreachelse}
	<tr><td colspan="5" class="center">{l i='no_reviews' gid='reviews'}</td></tr>
	{/foreach}
</table>
{include file="pagination.tpl"}

{include file="footer.tpl"}
