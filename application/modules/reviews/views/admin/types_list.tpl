{include file="header.tpl"}
{helper func_name=get_admin_level1_menu helper_name=menu func_param='admin_reviews_menu'}
<div class="actions">&nbsp;</div>

<form id="types_form" action="" method="post">
<table cellspacing="0" cellpadding="0" class="data" width="100%">
	<tr>
		<th class="first">{l i='field_types_name' gid='reviews'}</th>
		<th class="w100">{l i='field_types_rate_type' gid='reviews'}</th>
		<th class="w150"><a href="{$sort_links.date_add}"{if $order eq 'date_add'} class="{$order_direction|lower}"{/if}>{l i='field_types_date_add' gid='reviews'}</a></th>
		<th class="w50">&nbsp;</th>
	</tr>
	{foreach item=item from=$types}
	{counter print=false assign=counter}
	<tr{if $counter is div by 2} class="zebra"{/if}>				
		<td class="center">{$item.name}</td>
		<td class="center">{$item.rate_type_output}</td>
		<td class="center">{$item.date_add|date_format:$date_format}</td>
		<td class="icons">				
			<a href="{$site_url}admin/reviews/types_edit/{$item.id}"><img src="{$site_root}{$img_folder}icon-edit.png" width="16" height="16" border="0" alt="{l i='link_types_edit' gid='reviews' type='button'}" title="{l i='link_types_edit' gid='reviews' type='button'}"></a>
		</td>
	</tr>
	{foreachelse}
	<tr><td colspan="6" class="center">{l i='no_types' gid='reviews'}</td></tr>
	{/foreach}
</table>
</form>
{include file="pagination.tpl"}
{include file="footer.tpl"}
