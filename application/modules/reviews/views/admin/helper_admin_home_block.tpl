	<table cellspacing="0" cellpadding="0" class="data" width="100%">
	<tr>
		<th class="first" colspan="2">{l i='stat_header_reviews' gid='reviews'}</th>
	</tr>
	{if $stat_reviews.index_method}
	{foreach item=item from=$stat_reviews.types}
	{assign var='type_gid' value='type_'+$item.gid}
	{counter print=false assign=counter}
	<tr {if !($counter is div by 2)}class="zebra"{/if}>
		<td class="first"><a href="{$site_url}admin/reviews/index/{$item.gid}">{l i='stat_header_reviews_'+$item.gid gid='reviews'}</a></td>
		<td class="w50 center"><a href="{$site_url}admin/reviews/index/{$item.gid}">{$stat_reviews[$type_gid]}</a></td>
	</tr>
	{/foreach}
	{/if}
	</table>

