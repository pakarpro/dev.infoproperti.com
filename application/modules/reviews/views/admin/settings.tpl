{include file="header.tpl"}
{helper func_name=get_admin_level1_menu helper_name=menu func_param='admin_reviews_menu'}
<div class="actions">&nbsp;</div>

<form method="post" action="{$data.action}" name="save_form" enctype="multipart/form-data">
	<div class="edit-form n250">
		<div class="row header">{l i='admin_header_settings_edit' gid='reviews'}</div>
		<div class="row zebra">
			<div class="h">{l i='settings_use_alerts' gid='reviews'}:</div>
			<div class="v">
				<input type="hidden" name="reviews_use_alerts" value="0">
				<input type="checkbox" name="reviews_use_alerts" value="1" {if $settings_data.reviews_use_alerts}checked{/if} class="short">
			</div>
		</div>
		<div class="row">
			<div class="h">{l i='settings_alert_name' gid='reviews'}:</div>
			<div class="v">
				<input type="text" name="reviews_alert_name" value="{$settings_data.reviews_alert_name|escape}" class="middle">
			</div>
		</div>
		<div class="row zebra">
			<div class="h">{l i='settings_alert_email' gid='reviews'}:</div>
			<div class="v">
				<input type="text" name="reviews_alert_email" value="{$settings_data.reviews_alert_email|escape}" class="middle">
			</div>
		</div>
	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="{l i='btn_save' gid='start' type='button'}"></div></div>
</form>

{include file="footer.tpl"}