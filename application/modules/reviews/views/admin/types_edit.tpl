{include file="header.tpl" load_type='editable|ui'}
{js file='admin-multilevel-sorter.js'}
<form method="post" action="" name="save_form" class="ds_items">
	{assign var=rate_type value='rate_type_'`$data.rate_type`}
	<div class="edit-form n150">
		<div class="row header">{l i='admin_header_types_edit' gid='reviews'}</div>
		<div class="row">
			<div class="h">{l i='field_types_name' gid='reviews'}: </div>
			<div class="v"><input type="text" name="data[name]" value="{$data.name|escape}"></div>
		</div>
		<div class="row">
			<div class="h">{l i='field_types_rate_type' gid='reviews'}: </div>
			<div class="v">
				<select name="data[rate_type]" id="rate-type-select">
					{foreach item=item key=key from=$rate_types_settings}
					{if $key eq $data.rate_type}{assign var=current_type_settings value=$item}{/if}
					<option value="{$key|escape}" {if $key eq $data.rate_type}selected{/if}>{$item.name}</option>
					{/foreach}
				</select>
			</div>
		</div>
		{if $data.rate_type}
		{assign var=rate_type value=$data.rate_type}
		<div class="row">
			<div class="h">{l i='field_types_main_rate' gid='reviews'}:</div>
			<div class="v">
				<ol id="main_rate_list" class="main_rate_list a-list">
					<li>
						<div class="a-block">
							<div class="a-item"><p>{l i='field_types_rate_header' gid='reviews'}: </p>
							
							{foreach item=lang_item key=lang_id from=$langs}
							{assign var=lang_prefix value='values_'`$lang_id`}
							<input type="{if $lang_id eq $current_lang_id}text{else}hidden{/if}" name="rate[main][header][{$lang_id}]" value="{$data[$lang_prefix][$rate_type].main.header|escape}" lang-editor="value" lang-editor-type="main-header" lang-editor-lid="{$lang_id}">
							{/foreach}
							
							<a href="#" lang-editor="button" lang-editor-type="main-header"><img src="{$site_root}{$img_folder}icon-translate.png" width="16" height="16" alt="{l i='note_types_translate' gid='reviews' type='button'}" title="{l i='note_types_translate' gid='reviews' type='button'}"></a>
							</div>					
									
							{foreach item=answer from=$current_type_settings.answers}
							<div class="a-item">
								<div class="p">{block name=get_rate_block module=reviews rating_data_main=$answer type=$data template='mini' read_only="true"}: </div>
							
								{foreach item=lang_item key=lang_id from=$langs}
								{assign var=lang_prefix value='values_'`$lang_id`}
								<input type="{if $lang_id eq $current_lang_id}text{else}hidden{/if}" name="rate[main][votes][{$answer}][{$lang_id}]" value="{$data[$lang_prefix][$rate_type].main.votes[$answer]|escape}" lang-editor="value" lang-editor-type="main-votes-{$answer}" lang-editor-lid="{$lang_id}">
								{/foreach}

								<a href="#" lang-editor="button" lang-editor-type="main-votes-{$answer}"><img src="{$site_root}{$img_folder}icon-translate.png" width="16" height="16" alt="{l i='note_types_translate' gid='reviews' type='button'}" title="{l i='note_types_translate' gid='reviews' type='button'}"></a>
							</div>
							{/foreach}
							<input type="hidden" name="ratings[]" value="main">
						</div>
					</li>
				</ol>
			</div>
		</div>
		<div class="row">
			<div class="h">{l i='field_types_dop_rate' gid='reviews'}:</div>
			<div class="v">
			
				<div class="add-link"><a href="#" id="dop_rate_add">{l i='dop_rate_add_btn' gid='reviews'}</a></div>
				
				<ol id="dop_rate_list" class="dop_rate_list a-list sort connected" name="parent_0">
					{foreach item=item key=key from=$data.values[$rate_type]}
					{if $key ne 'main'}
					<li id="item_{$key}">
						<div id="a-block-{$key}" class="a-block">
							<div class="icons">
								<a href='#' onclick="if(confirm('{l i='note_types_dop_rate_delete' gid='reviews' type='js'}')) mlSorter.deleteItem('{$key}'); return false;"><img src="{$site_root}{$img_folder}icon-delete.png" width="16" height="16" alt="{l i='note_types_dop_rate_delete' gid='reviews' type='button'}" title="{l i='note_types_dop_rate_delete' gid='reviews' type='button'}"></a>
							</div>
							<div class="a-item">
								<p>{l i='field_types_rate_header' gid='reviews'}:</p>
								{foreach item=lang_item key=lang_id from=$langs}
								{assign var=lang_prefix value='values_'`$lang_id`}
								<input type="{if $lang_id eq $current_lang_id}text{else}hidden{/if}" name="rate[{$key}][header][{$lang_id}]" value="{$data[$lang_prefix][$rate_type][$key].header|escape}" lang-editor="value" lang-editor-type="{$key}-header" lang-editor-lid="{$lang_id}">
								{/foreach}
								<a href="#" lang-editor="button" lang-editor-type="{$key}-header"><img src="{$site_root}{$img_folder}icon-translate.png" width="16" height="16" alt="{l i='note_types_translate' gid='reviews' type='button'}" title="{l i='note_types_translate' gid='reviews' type='button'}"></a>
							</div>
							<div class="clr"></div>
							
							{foreach item=answer from=$current_type_settings.answers}
							<div class="a-item"><div class="p">{block name=get_rate_block module=reviews rating_data_main=$answer type=$data template='mini' read_only="true"}: </div>
								{foreach item=lang_item key=lang_id from=$langs}
								{assign var=lang_prefix value='values_'`$lang_id`}
								<input type="{if $lang_id eq $current_lang_id}text{else}hidden{/if}" name="rate[{$key}][votes][{$answer}][{$lang_id}]" value="{$data[$lang_prefix][$rate_type][$key].votes[$answer]|escape}" lang-editor="value" lang-editor-type="{$key}-votes-{$answer}" lang-editor-lid="{$lang_id}">
								{/foreach}
								<a href="#" lang-editor="button" lang-editor-type="{$key}-votes-{$answer}"><img src="{$site_root}{$img_folder}icon-translate.png" width="16" height="16" alt="{l i='note_types_translate' gid='reviews' type='button'}" title="{l i='note_types_translate' gid='reviews' type='button'}"></a>
							</div>
							{/foreach}
							<input type="hidden" name="ratings[]" value="{$key|escape}">
						</div>
					</li>
					{/if}
					{/foreach}
				</ol>
			</div>
		</div>
		{/if}		
	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="{l i='btn_save' gid='start' type='button'}"></div></div>
	<a class="cancel" href="{$site_url}admin/reviews/types">{l i='btn_cancel' gid='start'}</a>
</form>
<div class="clr"></div>

<div class="hide" id="dop_rate_example">

	<div id="a-block-dop__NUM__" class="a-block">
		<div class="icons">
			<a href='#' delete-link="1" delete-num="__NUM__"><img src="{$site_root}{$img_folder}icon-delete.png" width="16" height="16" alt="{l i='note_types_dop_rate_delete' gid='reviews' type='button'}" title="{l i='note_types_dop_rate_delete' gid='reviews' type='button'}"></a>
		</div>
		<div class="a-item">
			<p>{l i='field_types_rate_header' gid='reviews'}:</p>
			{foreach item=lang_item key=lang_id from=$langs}
			{assign var=lang_prefix value='values_'`$lang_id`}
			<input type="{if $lang_id eq $current_lang_id}text{else}hidden{/if}" name="rate[dop__NUM__][header][{$lang_id}]" value="{$data[$lang_prefix][$rate_type].dop__NUM__.header|escape}" lang-editor="value" lang-editor-type="dop__NUM__-header" lang-editor-lid="{$lang_id}">
			{/foreach}
			<a href="#" lang-editor="button" lang-editor-type="dop__NUM__-header"><img src="{$site_root}{$img_folder}icon-translate.png" width="16" height="16" alt="{l i='note_types_translate' gid='reviews' type='button'}" title="{l i='note_types_translate' gid='reviews' type='button'}"></a>
		</div>
		<div class="clr"></div>
							
		{foreach item=answer from=$current_type_settings.answers}
		<div class="a-item"><div class="p">{block name=get_rate_block module=reviews rating_data_main=$answer type=$data template='mini' read_only="true"}: </div>
		{foreach item=lang_item key=lang_id from=$langs}
		{assign var=lang_prefix value='values_'`$lang_id`}
		<input type="{if $lang_id eq $current_lang_id}text{else}hidden{/if}" name="rate[dop__NUM__][votes][{$answer}][{$lang_id}]" value="{$data[$lang_prefix][$rate_type].dop__NUM__.votes[$answer]|escape}" lang-editor="value" lang-editor-type="dop__NUM__-votes-{$answer}" lang-editor-lid="{$lang_id}">
		{/foreach}
		<a href="#" lang-editor="button" lang-editor-type="dop__NUM__-votes-{$answer}"><img src="{$site_root}{$img_folder}icon-translate.png" width="16" height="16" alt="{l i='note_types_translate' gid='reviews' type='button'}" title="{l i='note_types_translate' gid='reviews' type='button'}"></a>
		</div>
		{/foreach}
		<input type="hidden" name="ratings[]" value="dop__NUM__">
	</div>
</div>
{block name=lang_inline_editor module=start}
{if $data.rate_type}
<script>{literal}
var mlSorter;
$(function(){
	{/literal}
	{assign var=max_dop value=0}
	{foreach item=item key=key from=$data.values[$rate_type]}	
		{assign var=value value=$key|substr:3}
		{if $value > $max_dop}{assign var=max_dop value=$value}{/if}
	{/foreach}
	{literal}
	var dop_rate_count = '{/literal}{$max_dop}{literal}';
	$('#dop_rate_add').bind('click', function(){
		var content = $('#dop_rate_example').html();
		dop_rate_count++;
		content = content.replace(/__NUM__/g, dop_rate_count);
		$('#dop_rate_list').append('<li id="dop_rate_li_'+dop_rate_count+'">'+content+'</li>');
		$('#dop_rate_li_'+dop_rate_count).find('a[delete-link=1]').bind('click', function(){
			$('#dop_rate_li_'+$(this).attr('delete-num')).remove();
			return false;
		});
		
		if(inlineEditor != undefined){
			inlineEditor.reload_buttons();
		}
		return false;
	});
	
	mlSorter = new multilevelSorter({
		siteUrl: '{/literal}{$site_url}{literal}',
		urlDeleteItem: 'admin/reviews/ajax_types_rate_delete/{/literal}{$data.id}{literal}/',
	});
	
	$('#rate-type-select').bind('change', function(){
		location.href = '{/literal}{$site_url}admin/reviews/types_edit/{$data.id}/{literal}'+$(this).val();
		return false;
	});
	
	$("div.row:odd").addClass("zebra");
});
{/literal}</script>
{/if}
{include file="footer.tpl"}
