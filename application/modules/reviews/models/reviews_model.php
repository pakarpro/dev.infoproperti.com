<?php
/**
* Review Model
* 
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/
define("REVIEWS_TABLE", DB_PREFIX."reviews");

class Reviews_model extends Model{

	/**
	 * link to CodeIgniter object
	 * @var object
	 */
	private $CI;

	/**
	 * link to DataBase object
	 * @var object
	 */
	private $DB;

	/**
	 * Table fields
	 * @var array
	 */
	var $fields = array(
		"id",
		"gid_type",
		"id_object",
		"id_poster",
		"id_responder",
		"rating_data",
		"message",
		"answer",
		"date_add",
		"date_answer",
		"banned",
	);
	
	/**
	 * Format settings
	 * @var array
	 */
	private $format_settings = array(
		"use_format"  	=> true,
		"get_poster"  	=> true,
		"get_responder" => true,
		"get_type"	  	=> true,
		"get_object"  	=> false,
	);
	
	/**
	 * Moderation type
	 * @var string
	 */
	private $moderation_type = "reviews";

	/**
	 * Cache array for used types
	 * @var array
	 */
	private $reviews_types = array();

	/**
	 * GUID of notification on review object to user
	 * @var string
	 */
	public $notification_user_review_gid = "user_reviews_object";
	
	/**
	 * GUID of notification on review reply to user
	 * @var string
	 */
	public $notification_user_reply_gid = "user_reviews_reply";
	
	/**
	 * GUID of notification on review object to administrators
	 * @var string
	 */
	public $notification_auser_review_gid = "auser_reviews_object";
	
	/**
	 * GUID of notification on review reply to administrators
	 * @var string
	 */
	public $notification_auser_reply_gid = "auser_reviews_reply";
	
	/**
	 * Constructor
	 *
	 * return Review object
	 * required Review_type_model
	 */
	public function __construct(){
		parent::Model();
		$this->CI = &get_instance();
		$this->DB = &$this->CI->db;
		$this->CI->load->model("reviews/models/Reviews_type_model");
	}

	/**
	 * Get review type by GID
	 * @param string $type_gid
	 * @return array
	 */
	public function get_type_by_gid($type_gid){
		if(!isset($this->types[$type_gid])){
			$type_data = $this->CI->Reviews_type_model->get_type($type_gid);
			if(!is_array($type_data) || !count($type_data)) return false;
			$this->reviews_types[$type_data["id"]] = $type_data;
			$this->reviews_types[$type_gid] = $type_data;
		}

		if(is_array($this->reviews_types[$type_gid]) && count($this->reviews_types[$type_gid])){
			return $this->reviews_types[$type_gid];
		}else{
			return false;
		}
	}


	/**
	 * Get review by ID
	 * @param integer $id review ID
	 * @param 
	 */ 
	public function get_review_by_id($id, $formatted=false){
		$id = intval($id);
		
		$this->DB->select(implode(", ", $this->fields))->from(REVIEWS_TABLE)->where("id", $id);
		
		//_compile_select;
		$result = $this->DB->get()->result();
		if(!empty($result)){
			$rt = array(get_object_vars($result[0]));
			if($formatted) $rt = $this->format_review($rt);
			return $rt[0];
		}else
			return false;
	}
	
	/**
	 * Save/create review
	 * @param array $data
	 * @return boolean
	 */
	public function save_review($id, $data){
		if(empty($data)) return false;

		if(isset($data["gid_type"])){
			$type = $this->Reviews_type_model->get_type($data["gid_type"]);
			if(!$type) return false;
		}elseif(!$id){
			return false;
		}
		
		if($id){
			$this->DB->where("id", $id);
			$this->DB->update(REVIEWS_TABLE, $data);
		}else{
			$data["date_add"] = date("Y-m-d H:i:s");
			$this->DB->insert(REVIEWS_TABLE, $data);
			$id = $this->DB->insert_id();
		}

		if(!isset($data["gid_type"]) || !isset($data["id_object"])){ 
			$review = $this->get_review_by_id($id);
			$data['gid_type'] = $review['gid_type'];
			$data['id_object'] = $review['id_object'];
			if(!$type) $type = $this->Reviews_type_model->get_type($data["gid_type"]);
		}

		if($type["module"] && $type["model"] && $type["callback"]){
			$model_name = ucfirst($type["model"])."_model";
			$this->CI->load->model($type["module"]."/models/".$model_name, $model_name);
			$this->CI->{$model_name}->{$type["callback"]}('update', $this->calculate_rating($data["gid_type"], $data["id_object"]));
		}
		
		return $id;
	}
	
	/**
	 * Remove review by ID
	 * @param integer $id review ID
	 */ 
	public function delete_review($id){
		$review = $this->get_review_by_id($id, true);
		if(!$review) return "";		
		$type = $this->Reviews_type_model->get_type($review["gid_type"]);
		
		$this->DB->where("id", $id);
		$this->DB->delete(REVIEWS_TABLE);
		
		if($type && $type["module"] && $type["model"] && $type["callback"]){
			$model_name = ucfirst($type["model"])."_model";
			$this->CI->load->model($type["module"]."/models/".$model_name, $model_name, true);
			$this->CI->{$model_name}->{$type["callback"]}('update', $this->calculate_rating($review["gid_type"], $review["id_object"]));
		}
		
		return "";
	}	
	
	/**
	 * Return types filter
	 * @param string $object
	 * @return array
	 */
	private function _get_reviews_by_type($object){
		if(!$object) return array();
		$params["where"]["gid_type"] = $object;
		return $params;
	}
	
	/**
	 * Return objects filter
	 * @param integer $object
	 * @return array
	 */
	private function _get_reviews_by_object($object){
		if(!$object) return array();
		$params["where"]["id_object"] = $object;
		return $params;
	}
	
	/**
	 * Return posters filter
	 * @param integer $poster
	 * @return array
	 */
	private function _get_reviews_by_poster($poster){
		if(!$poster) return array();
		$params["where"]["id_poster"] = $poster;
		return $params;
	}
	
	/**
	 * Return objects filter
	 * @param integer $responder
	 * @return array
	 */
	private function _get_reviews_by_responder($responder){
		if(!$responder) return array();
		$params["where"]["id_responder"] = $responder;
		return $params;
	}
	
	/**
	 * Return week filter
	 * @param integer $poster
	 * @return array
	 */
	private function _get_reviews_by_week($active){
		if(!$poster) return array();
		$params["where_sql"] = '(date_add >= DATE_SUB(CURDATE(), INTERVAL 1 WEEK))';
		return $params;
	}
	
	/**
	 * Return month filter
	 * @param integer $poster
	 * @return array
	 */
	private function _get_reviews_by_month($active){
		if(!$active) return array();
		$params["where_sql"] = '(date_add >= DATE_SUB(CURDATE(), INTERVAL 1 MONTH))';
		return $params;
	}
	
	/**
	 * Return ids filter
	 * @param integer $poster
	 * @return array
	 */
	private function _get_reviews_by_ids($ids){
		if(empty($ids)) return array();
		$params["where_in"]["id"] = $ids;
		return $params;
	}
	
	/**
	 * Return reviews as array
	 * @param integer $page
	 * @param string $limits 
	 * @param array $order_by
	 * @param array $params
	 * @return array
	 */
	private function _get_reviews_list($page=null, $limits=null, $order_by=null, $params=array()){
		$this->DB->select(implode(", ", $this->fields));
		$this->DB->from(REVIEWS_TABLE);
		
		if(isset($params["where"]) && is_array($params["where"]) && count($params["where"])){
			foreach($params["where"] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])){
			foreach($params["where_in"] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])){
			foreach($params["where_sql"] as $value){
				$this->DB->where($value);
			}
		}

		if (is_array($order_by) && count($order_by) > 0){
			foreach ($order_by as $field => $dir){
				if (in_array($field, $this->fields)){
					$this->DB->order_by($field." ".$dir);
				}
			}
		} else if ($order_by){
			$this->DB->order_by($order_by);
		}

		if(!is_null($page)){
			$page = intval($page)?intval($page):1;
			$this->DB->limit($limits, $limits*($page-1));
		}

		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			foreach($results as $r){
				$data[] = $r;
			}
			return $this->format_review($data);
		}
		return array();
	}
	
	/**
	 * Return number of reviews
	 * @param array $params
	 * @return integer
	 */
	private function _get_reviews_count($params=null){
		$this->DB->select("COUNT(*) AS cnt");
		$this->DB->from(REVIEWS_TABLE);

		if(isset($params["where"]) && is_array($params["where"]) && count($params["where"])){
			foreach($params["where"] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])){
			foreach($params["where_in"] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])){
			foreach($params["where_sql"] as $value){
				$this->DB->where($value);
			}
		}

		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			return intval($results[0]["cnt"]);
		}
		return 0;
	}
	
	/**
	 * Return reviews as array
	 * @param integer $page
	 * @param string $limits 
	 * @param array $order_by
	 * @return array
	 */
	public function get_reviews_list($page=null, $limits=null, $order_by=null){
		return $this->_get_reviews_list($page, $limits, $order_by);
	}
	
	/**
	 * Return reviews as array
	 * @return integer
	 */
	public function get_reviews_count(){
		return $this->_get_reviews_count();
	}
	
	/**
	 * Return reviews as array
	 * @param array $filters
	 * @param integer $page
	 * @param string $limits 
	 * @param array $order_by
	 * @return array
	 */
	public function get_reviews_list_by_filters($filters=array(), $page=null, $limits=null, $order_by=null){
		$params = array();
		foreach($filters as $filter=>$value){
			$params = array_merge_recursive($params, $this->{"_get_reviews_by_".$filter}($value));
		}
		return $this->_get_reviews_list($page, $limits, $order_by, $params);
	}
	
	/**
	 * Return reviews as array
	 * @param array $filters
	 * @return integer
	 */
	public function get_reviews_count_by_filters($filters=array()){
		$params = array();
		foreach($filters as $filter=>$value){
			$params = array_merge_recursive($params, $this->{"_get_reviews_by_".$filter}($value));
		}
		return $this->_get_reviews_count($params);
	}
	
	/**
	 * Validate review
	 * @param integer $id
	 * @param array $data
	 * @return array
	 */
	public function validate_review($id, $data){
		$return = array("errors"=> array(), "data" => array());
		
		// object
		if(isset($data["id_object"])){
			$return["data"]["id_object"] = intval($data['id_object']);
			if(empty($return["data"]["id_object"])) $return["errors"][] = l("error_empty_object", "reviews");
		}elseif(!$id){
			$return["errors"][] = l("error_empty_object", "reviews");
		}		
		
		// poster
		if(isset($data["id_poster"])){
			$return["data"]["id_poster"] = intval($data['id_poster']);
			if(empty($return["data"]["id_poster"])) $return["errors"][] = l("error_empty_poster", "reviews");
		}elseif(!$id){
			$return["errors"][] = l("error_empty_poster", "reviews");
		}

		// type gid
		if(isset($data["gid_type"])){
			$return["data"]["gid_type"] = trim(strip_tags($data['gid_type']));
			
			$type = $this->Reviews_type_model->get_type($return["data"]["gid_type"]);
			if(empty($type)){
				$return["errors"][] = l("error_invalid_type", "reviews");
			}else{
				$check = $this->is_review_from_poster($type["gid"], $return["data"]["id_poster"], $return["data"]["id_object"]);
				if($check) $return["errors"][] = l("error_review_exists", "reviews");
			}
		}elseif(!$id){
			$return["errors"][] = l("error_empty_type", "reviews");
		}

		// main rate
		if(isset($data["rating_data"]["main"])){
			$return["data"]["rating_data"]["main"] = trim(strip_tags($data["rating_data"]["main"]));
			if(empty($return["data"]["rating_data"]["main"])) $return["errors"][] = l("error_empty_rating", "reviews");
		}elseif(!$id){
			$return["errors"][] = l("error_empty_rating", "reviews");
		}
		
		if(isset($data["rating_data"])){
			$return["data"]["rating_data"] = serialize($data["rating_data"]);
		}

		// message
		if(isset($data["message"])){
			if(!empty($data["message"])) $data["message"] = trim(strip_tags($data["message"]));
			if(!empty($data["message"])){
				$return["data"]["message"] = $data["message"];
				$this->CI->load->model("moderation/models/Moderation_badwords_model");
				$bw_count = $this->CI->Moderation_badwords_model->check_badwords($this->moderation_type, $return["data"]["message"]);
				if($bw_count) $return["errors"][] = l("error_badwords_message", "reviews");
			}else{
				$return["errors"][] = l("error_empty_message", "reviews");
			}
		}elseif(!$id){
			$return["errors"][] = l("error_empty_message", "reviews");
		}
		
		// answer
		if(isset($data["answer"])){
			if(!empty($data["answer"])) $data["answer"] = trim(strip_tags($data["answer"]));
			if(!empty($data["answer"])){
				$return["data"]["answer"] = $data["answer"];
				$this->CI->load->model("moderation/models/Moderation_badwords_model");
				$bw_count = $this->CI->Moderation_badwords_model->check_badwords($this->moderation_type, $return["data"]["answer"]);
				if($bw_count) $return["errors"][] = l("error_badwords_message", "reviews");
			}else{
				$return["data"]["answer"] = $data["answer"];
			}
		}
		
		if(isset($data["date_answer"])){
			$data["date_answer"] = strtotime($data["date_answer"]);
			$return['data']["date_answer"] = $data["date_answer"] ? date("Y-m-d H:i:s", $data["date_answer"]) : '0000-00-00 00:00:00';
		}

		// other
		if(isset($data["id_responder"])){
			$return["data"]["id_responder"] = intval($data['id_responder']);
		}

		if(isset($data["banned"])){
			$return["data"]["banned"] = intval($data['banned']);
		}

		return $return;
	}
	
	/**
	 * Validate review
	 * @param integer $id
	 * @param array $data
	 * @return array
	 */
	public function validate_reply($id, $data){
		$return = array("errors"=> array(), "data" => array());
		
		// answer
		if(isset($data["answer"])){
			if(!empty($data["answer"])) $data["answer"] = trim(strip_tags($data["answer"]));
			if(!empty($data["answer"])){
				$return["data"]["answer"] = $data["answer"];
				$this->CI->load->model("moderation/models/Moderation_badwords_model");
				$bw_count = $this->CI->Moderation_badwords_model->check_badwords($this->moderation_type, $return["data"]["answer"]);
				if($bw_count) $return["errors"][] = l("error_badwords_message", "reviews");
			}else{
				$return["errors"][] = l("error_empty_answer", "reviews");
			}
		}
		
		if(isset($data["date_answer"])){
			$data["date_answer"] = strtotime($data["date_answer"]);
			$return['data']["date_answer"] = $data["date_answer"] ? date("Y-m-d H:i:s", $data["date_answer"]) : '0000-00-00 00:00:00';
		}

		return $return;
	}
	
	/**
	 * Check exists review to object from porter
	 * @param string $type_gid
	 * @param integer $poster_id
	 * @param integer $object_id
	 * @return boolean
	 */
	public function is_review_from_poster($type_gid, $poster_id, $object_id){
		$params = array();
		$params["where"]["gid_type"] = $type_gid;
		$params["where"]["id_poster"] = $poster_id;
		$params["where"]["id_object"] = $object_id;
		return $this->_get_reviews_count($params) > 0;
	}
	
	/**
	 * Change format settings
	 * @param string $name parameter name
	 * @param mixed $value parameter value
	 */
	public function set_format_settings($name, $value=false){
		if(!is_array($name)) $name = array($name=>$value);
		if(empty($name)) return;
		foreach($name as $key => $item)	$this->format_settings[$key] = $item;
	}
	
	/**
	 * Format review
	 * @param array $data
	 * @return array
	 */
	public function format_review($data){
		if(!$this->format_settings["use_format"]){
			return $data;
		}
		
		$types_is_loaded = false;

		$users_search = array();
		$types_search = array();
		$objects_search = array();
		
		foreach($data as $key=>$review){
			$data[$key] = $review;			
			//get_poster
			if($this->format_settings["get_poster"]){
				$users_search[] = $review["id_poster"];
			}
			
			if($this->format_settings["get_responder"]){
				$users_search[] = $review["id_responder"];
			}
			
			//get form
			if($this->format_settings["get_type"]){
				$types_search[] = $review["gid_type"];
			}			
			//get object
			if($this->format_settings["get_object"]){
				$objects_search[$review["gid_type"]][] = $review["id_object"];
			}
			$data[$key]["rating_data"] = (array)unserialize($review["rating_data"]);
			$data[$key]["message"] = nl2br($review["message"]);			
			$data[$key]["answer"] = nl2br($review["answer"]);			
		}
		
		if(($this->format_settings["get_poster"] || $this->format_settings["get_responder"]) && !empty($users_search)){
			$this->CI->load->model("Users_model");
			$users_data = $this->CI->Users_model->get_users_list_by_key(null, null, null, array(), $users_search);
			foreach($data as $key=>$review){
				if($this->format_settings["get_poster"]){
					$data[$key]["poster"] = (isset($users_data[$review["id_poster"]])) ? 
						$users_data[$review["id_poster"]] : $this->CI->Users_model->format_default_user($review["id_poster"]);
				}
				if($this->format_settings["get_responder"]){
					$data[$key]["responder"] = (isset($users_data[$review["id_responder"]])) ? 
						$users_data[$review["id_responder"]] : $this->CI->Users_model->format_default_user($review["id_responder"]);
				}
			}
		}
		
		if($this->format_settings["get_type"] && !empty($types_search)){
			$types_data = $this->CI->Reviews_type_model->get_types($types_search);
			foreach($types_data as $type_data){
				$this->reviews_types[$type_data["id"]] = $type_data;
				$this->reviews_types[$type_data["gid"]] = $type_data;
			}
			foreach($data as $key=>$review){
				$data[$key]["type"] = (isset($this->reviews_types[$review["gid_type"]])) ? 
					$this->reviews_types[$review["gid_type"]] : 
					$this->CI->Reviews_type_model->format_default_type($review["gid_type"]);
			}
			$types_is_loaded = true;
		}
		
		if($this->format_settings["get_object"] && !empty($objects_search)){
			if(!$types_is_loaded){
				$types_data = $this->CI->Reviews_type_model->get_types($types_search);
				foreach($types_data as $type_data){
					$this->reviews_types[$type_data["id"]] = $type_data;
					$this->reviews_types[$type_data["gid"]] = $type_data;
				}
				$types_is_loaded = true;
			}
			$objects_data = array();
			foreach($this->reviews_types as $type_data){
				if(!isset($objects_search[$type_data["id"]]) || !$type_data["module"] || !$type_data["model"] || !$type_data["callback"])
					continue;
				try{
					$model_name = ucfirst($type_data["model"])."_model";
					$this->CI->load->model($type_data["module"]."/models/".$model_name, $model_name);
					$objects_data[$type_data["id"]] = $this->CI->{$model_name}->{$type_data["callback"]}(
						"get_object", $objects_search[$type_data["id"]]);
				}catch(Exception $e){
				}
			}
			foreach($data as $key=>$review){
				$data[$key]["object"] = isset($objects_data[$review["gid_type"]][$review["id_object"]]) ? 
					$objects_data[$review["gid_type"]][$review["id_object"]] : "";
			}
		}
		
		return $data;
	}
	
	/**
	 * Format default review
	 * @param integer $id listing identifier
	 * @return array
	 */
	public function format_default_review($id){
		$data["output_name"] = "Review is deleted";
		return $data;
	}
	
	/**
	 * Calculate rating
	 * @param string $type_gid
	 * @param integer $object_id
	 * @return array
	 */
	public function calculate_rating($type_gid, $id_object){
		$type = $this->Reviews_type_model->get_type($type_gid, true);
		$z = 1.64485;
		$z2 = $z*$z;
		$votes = array_keys($type["values"][$type["rate_type"]]["main"]["votes"]);
		$v_min = min($votes);
		$v_width = max($votes)-$v_min;		
		
		$reviews_total = 0;
		$reviews_count = 0;
		$reviews_data = array();
		
		$reviews = $this->get_reviews_list_by_filters(array("type"=>$type_gid, "object"=>$id_object));
		foreach($reviews as $review){
			$votes = array();
			$headers = array();
			foreach($review["rating_data"] as $key=>$value){
				if(!isset($type["values"][$type["rate_type"]][$key]["votes"][$value])) continue;
				$headers[$key] = $type["values"][$type["rate_type"]][$key]["header"];
				$votes[$key] = array($value=>$type["values"][$type["rate_type"]][$key]["votes"][$value]);
			}
			$reviews_data[] = array("headers"=>$headers, "votes"=>$votes);
			$reviews_total += $review["rating_data"]["main"];
			$reviews_count++;
		}
				
		//вычисляем новый рейтинг
		if($reviews_count > 0){
			$phat = ($reviews_total-$reviews_count*$v_min)/$v_width/$reviews_count;
			$rating = ($phat+$z2/(2*$reviews_count)-$z*sqrt(($phat*(1-$phat)+$z2/(4*$reviews_count))/$reviews_count))/(1+$z2/$reviews_count);
			$rating = $rating*$v_width+$v_min;
		}else{
			$rating = 0;
		}
		return array("type_gid"=>$type_gid, "id_object"=>$id_object, "review_sorter"=>$rating, "review_count"=>$reviews_count, "review_data"=>$reviews_data);
	}
	
	/**
	 * Install languages
	 * @param array $data
	 * @param array $langs_file
	 * @param array $langs_ids
	 */
	public function update_langs($data, $langs_file, $langs_ids){
		$this->CI->pg_language->pages->set_string_langs(
			"reviews", 
			"stat_header_reviews_".$data["gid"], 
			$langs_file["stat_header_reviews_".$data["gid"]], 
			$langs_ids);
			
		$this->CI->pg_language->pages->set_string_langs(
			"reviews", 
			"stat_reviews_visitors_".$data["gid"], 
			$langs_file["stat_reviews_visitors_".$data["gid"]], 
			$langs_ids);
		
		$this->CI->pg_language->pages->set_string_langs(
			"reviews", 
			"stat_reviews_visits_".$data["gid"], 
			$langs_file["stat_reviews_visits_".$data["gid"]], 
			$langs_ids);	
			
		return true;
	}
	
	/**
	 * Return languages data for rate types 
	 * @param string $rate_type rate type GUID
	 * @param array $rate_data rate data
	 * @param integer $lang_id language identifier
	 */
	private function _export_langs($rate_type, $rate_data, $lang_id){
		$return = array();
		foreach((array)$rate_data as $gid=>$data){
			if(is_array($data)){
				$return = array_merge($return, $this->_export_langs($rate_type.'_'.$gid, $data, $lang_id));
			}else{
				$return[$rate_type.'_'.$gid] = array($lang_id => $data);
			}
		}
		return $return;
	}

	/**
	 * Return languages data
	 * @param string $type_gid
	 * @param array $langs_ids
	 */
	public function export_langs($type_gid, $langs_ids=null){
		$langs = (array)$this->CI->pg_language->export_langs("reviews", array('stat_header_reviews_'.$type_gid), $langs_ids);
		$types = $this->CI->Reviews_type_model->get_types();	
		foreach((array)$langs_ids as $lang_id){
			foreach($types as $type){
				if($type['gid'] == $type_gid && isset($type["values_".$lang_id]) && is_array($type["values_".$lang_id])){
					foreach($type["values_".$lang_id] as $rate_type=>$rate_data){
						$langs = array_merge($langs, $this->_export_langs($rate_type, $rate_data, $lang_id));
					}
				}
			}
		}
		return $langs;
	}
	
	/**
	 * Add rating languages
	 * @param array $fields
	 */
	public function lang_dedicate_module_callback_add($fields){
		$this->Reviews_type_model->add_column($fields);
	}
	
	/**
	 * Remove rating language
	 * @param array $fields
	 */
	public function lang_dedicate_module_callback_delete($fields){
		$this->Reviews_type_model->delete_column($fields);
	}	
	
	/**
	 * Add new rate type
	 * @param string $type_gid
	 * @param array $types_data
	 * @param integer $lang_id
	 */
	public function add_rate_type($type_gid, $types_data, $lang_id){
		$types = $this->CI->Reviews_type_model->get_types();	
		foreach($types as $type){
			if(!isset($type["values_".$lang_id][$type_gid])){
				$type["values_".$lang_id][$type_gid] = $types_data;
			}
			$validate_data = $this->CI->Reviews_type_model->validate_type($type['id'], $type);
			$this->CI->Reviews_type_model->save_type($type['id'], $validate_data["data"]);
		}
	}
	
	/**
	 * Callback for spam module
	 * @param string $action action name
	 * @param integer $data review identifier
	 * @return string
	 */
	public function spam_callback($action, $data){
		switch($action){
			case "ban":
				$this->save_review((int)$data, array("banned"=>1));
				return "banned";
			break;
			case "unban":
			$this->save_review((int)$data, array("banned"=>0));
				return "unbanned";
			break;
			case "delete":
				$this->delete_review((int)$data);
				return "removed";
			break;
			case 'get_content':
				if(empty($data)) return array();		
				$reviews = $this->get_reviews_list_by_filters(array('ids'=>(array)$data));
				$return = array();
				foreach($reviews as $review){
					$return[$review['id']] = $review['message'].($review['answer'] ? '<br><br>'.$review['answer'] : '');
				}
				return $return;
			break;
			case 'get_link':
				if(empty($data)) return array();		
				$reviews = $this->get_reviews_list_by_filters(array('ids'=>(array)$data));
				$return = array();
				foreach($reviews as $review){
					$return[$review['id']] = site_url().'admin/reviews/edit/'.$review['id'];
				}
				return $return;
			break;
			case 'get_object':
				if(empty($data)) return array();	
				$reviews = $this->get_reviews_list_by_filters(array('ids'=>(array)$data));
				$return = array();
				foreach($reviews as $review){
					$return[$review['id']] = $review;
				}
				return $return;
			break;
		}
	}
}
