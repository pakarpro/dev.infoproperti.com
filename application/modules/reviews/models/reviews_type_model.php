<?php
/**
* Review type Model
*
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/

define("REVIEWS_TYPES_TABLE", DB_PREFIX."reviews_types");

class Reviews_type_model extends Model{
	/**
	 * link to CodeIgniter object
	 * @var object
	 */
	private $CI;
	/**
	 * link to DataBase object
	 * @var object
	 */
	private $DB;

	/**
	 * Table fields
	 * @var array
	 */
	var $fields = array(
		"id",
		"gid",
		"name",
		"rate_type",
		"module",
		"model",
		"callback",
		"date_add",
	);
	
	/**
	 * Format settings
	 * @var array
	 */
	private $format_settings = array(
		"use_format" => true,
		"get_rate_type" => true,
		"get_rate_type_values" => true,
	);

	public $rating_type_settings = array(
		'stars' => array('answers' => array(1, 2, 3, 4, 5) ),
		'hands' => array('answers' => array(1, 5)),
	);
	
	public $rating_type_ld = 'rate_type';
	
	/**
	 * Types cache
	 * @var array
	 */
	private $type_cache = array();
	
	/**
	 * Format types cache
	 * @var array
	 */
	private $type_format_cache = array();
	
	/**
	 * Constructor
	 *
	 * @return Linker_type Object
	 */
	function __construct(){
		parent::Model();
		$this->CI = & get_instance();
		$this->DB = &$this->CI->db;		
		foreach($this->CI->pg_language->languages as $id=>$value){
			$this->fields[] = "values_".$value["id"];
		}
	}

	/**
	 * Get type by ID/GID
	 * @param integer $id
	 * @param boolean $formatted
	 * @return mixed
	 */
	public function get_type($type_id, $formatted=false){
		$field = "id";
		if(!intval($type_id)){
			$field = "gid";
			$type_id= preg_replace("/[^a-z_]/", "", strtolower($type_id));
		}
		if(!$type_id) return false;
		
		if($formatted){
			if(isset($this->type_format_cache[$type_id])) return $this->type_format_cache[$type_id];
		}else{
			if(isset($this->type_cache[$type_id])) return $this->type_cache[$type_id];
		}
		
		$this->DB->select(implode(", ", $this->fields));
		$this->DB->from(REVIEWS_TYPES_TABLE);
		$this->DB->where($field, $type_id);

		//_compile_select;
		$result = $this->DB->get()->result();
		if(!empty($result)){
			$rt = get_object_vars($result[0]);
			$this->type_cache[$rt['id']] = $this->type_cache[$rt['gid']] = $rt;
			if($formatted){
				$rt = current($this->format_type(array($rt)));
				$this->type_format_cache[$rt['id']] = $this->type_format_cache[$rt['gid']] = $rt;
			}
			return $rt;
		}else
			return false;
	}

	/**
	 * Save type
	 * @param array $data
	 * @return boolean
	 */
	public function save_type($id, $data){
		if(empty($data)) return false;
		if(!$id){
			$data["date_add"] = date("Y-m-d H:i:s");
			$this->DB->insert(REVIEWS_TYPES_TABLE, $data);
			$id = $this->DB->insert_id();
		}else{
			$this->DB->where("id", $id);
			$this->DB->update(REVIEWS_TYPES_TABLE, $data);
		}		
		return $id;
	}

	/**
	 * Remove type by ID or GID
	 * @param mixed $id integer ID / string GID
	 */
	public function delete_type($type_id){
		$type = $this->get_type($type_id);		
		$this->DB->where("id", $type["id"]);
		$this->DB->delete(REVIEWS_TYPES_TABLE); 
		return;
	}
	
	/**
	 * Return all types as array
	 * @param array $filter_object_ids
	 * @param boolean $formatted
	 * @return array
	 */
	public function get_types($filter_object_ids=null, $formatted=true){
		$this->DB->select(implode(", ", $this->fields));
		$this->DB->from(REVIEWS_TYPES_TABLE);
		
		if(is_array($filter_object_ids)){
			foreach($filter_object_ids as $value){
				$this->DB->where_in("gid", $value);
			}
		}
		
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			$data = array();
			foreach($results as $r){
				$this->type_cache[$r['id']] = $this->type_cache[$r['gid']] = $data[$r['id']] = $r;
			}
			if($formatted){
				$data = $this->format_type($data);
				foreach($data as $r){
					$this->type_format_cache[$r['id']] = $this->type_format_cache[$r['gid']] = $r;
				}
			}
			return $data;
		}
		return array();
	}
	
	/**
	 * Validate type
	 * @param array $data
	 * @param boolean $required
	 * @return array
	 */
	public function validate_type($id, $data){
		$return = array("errors"=> array(), "data" => array());
		
		if(isset($data['gid'])){
			$return["data"]["gid"] = trim(strip_tags($data['gid']));
			if(empty($return["data"]["gid"])){
				$return["errors"][] = l("error_empty_type_gid", "reviews");
			}
		}
		
		if(isset($data['rate_type'])){
			$return["data"]["rate_type"] = trim(strip_tags($data['rate_type']));
			if(empty($return["data"]["rate_type"])){
				$return["errors"][] = l("error_empty_rate_type", "reviews");
			}
		}
		if(isset($data['name'])){
			$return["data"]["name"] = trim(strip_tags($data['name']));
		}

		if(isset($data['module'])){
			$return["data"]["module"] = trim(strip_tags($data['module']));
		}

		if(isset($data['model'])){
			$return["data"]["model"] = trim(strip_tags($data['model']));
		}

		if(isset($data['callback'])){
			$return['data']["callback"] = trim(strip_tags($data['callback']));
		}
	
		$default_lang_id = $this->CI->pg_language->current_lang_id;
		if(isset($data['values_'.$default_lang_id])){
			$values_errors = false;
			foreach((array)$data['values_'.$default_lang_id] as $rates_key=>$rates_arr){
				foreach((array)$rates_arr as $rate_key=>$rate_arr){
					$return['data']['values_'.$default_lang_id][$rates_key][$rate_key]['header'] = trim(strip_tags($rate_arr['header']));
					if(empty($return['data']['values_'.$default_lang_id][$rates_key][$rate_key]['header'])){
						$return['errors'][] = l('error_empty_review_type_value', 'reviews');
						$values_errors = true;
						break;
					}
					if($values_errors) break;
		
					foreach((array)$rate_arr['votes'] as $vote_key=>$vote_value){			
						$return['data']['values_'.$default_lang_id][$rates_key][$rate_key]['votes'][$vote_key] = trim(strip_tags($vote_value));
						if(empty($return['data']['values_'.$default_lang_id][$rates_key][$rate_key]['votes'][$vote_key])){
							$return['errors'][] = l('error_empty_review_type_value', 'reviews');
							$values_errors = true;
							break;
						}
					}
					if($values_errors) break;
				}
			}
			foreach($this->pg_language->languages as $lid=>$lang){
				if($values_errors) break;
				if($lid == $default_lang_id) continue;
				$return['data']['values_'.$lid] = array();
				foreach((array)$data['values_'.$lid] as $rates_key=>$rates_arr){
					$return['data']['values_'.$lid][$rates_key] = array();
					foreach((array)$rates_arr as $rate_key=>$rate_arr){
						$return['data']['values_'.$lid][$rates_key][$rate_key] = array();
						if(!isset($rate_arr['header']) || empty($rate_arr['header'])){
							$return['data']['values_'.$lid][$rates_key][$rate_key]['header'] = $return['data']['values_'.$default_lang_id][$rates_key][$rate_key]['header'];
						}else{
							$return['data']['values_'.$lid][$rates_key][$rate_key]['header'] = trim(strip_tags($rate_arr['header']));
							if(empty($return['data']['values_'.$lid][$rates_key][$rate_key]['header'])){
								$return['errors'][] = l('error_empty_review_type_value', 'listings');
								$values_errors = true;
								break;
							}
						}
						$return['data']['values_'.$lid][$rates_key][$rate_key]['votes'] = array();
						foreach((array)$return['data']['values_'.$default_lang_id][$rates_key][$rate_key]['votes'] as $vote_key=>$vote_value){
							if(!isset($rate_arr['votes'][$vote_key]) || empty($rate_arr['votes'][$vote_key])){
								$return['data']['values_'.$lid][$rates_key][$rate_key]['votes'][$vote_key] = $return['data']['values_'.$default_lang_id][$rates_key][$rate_key]['votes'][$vote_key];
							}else{
								$return['data']['values_'.$lid][$rates_key][$rate_key]['votes'][$vote_key] = trim(strip_tags($rate_arr['votes'][$vote_key]));
								if(empty($return['data']['values_'.$lid][$rates_key][$rate_key]['votes'][$vote_key])){
									$return['errors'][] = l('error_empty_review_type_value', 'listings');
									$values_errors = true;
									break;
								}
							}
						}
						if($values_errors) break;
					}
					if($values_errors) break;
				}
				if($values_errors) break;
				$return['data']['values_'.$lid] = serialize($return['data']['values_'.$lid]);
			}
			$return['data']['values_'.$default_lang_id] = serialize($return['data']['values_'.$default_lang_id]);
		}

		return $return;
	}
	
	/**
	 * Format type
	 * @param array $data
	 * @return array
	 */
	public function format_type($data){
		if(!$this->format_settings["use_format"]){
			return $data;
		}
		
		if($this->format_settings["get_rate_type"]){
			$type_settings = $this->format_rating_type_settings();
			$lang_id = $this->CI->pg_language->current_lang_id;
			foreach($data as $key=>$type){
				$data[$key] = $type;
				$data[$key]["values"] = isset($data[$key]["values_".$lang_id]) ? (array)unserialize($data[$key]["values_".$lang_id]) : array();
				$data[$key]["rate_type_output"] = $type_settings[$data[$key]["rate_type"]]['name'];				
			}
		}
		
		if($this->format_settings["get_rate_type_values"]){
			foreach($this->CI->pg_language->languages as $lang_id=>$lang_item){
				foreach($data as $key=>$type){
					$data[$key] = $type;
					$data[$key]["values_".$lang_id] = isset($data[$key]["values_".$lang_id]) ? (array)unserialize($data[$key]["values_".$lang_id]) : array();
				}
			}
		}
		
		foreach($data as $key=>$type){
			$data[$key]["output_name"] = $data[$key]["gid"];
			$module_data = $this->CI->pg_module->get_module_by_gid($data[$key]["module"]);
			$data[$key]["output_model_name"] = $module_data["module_name"];
		}
		
		return $data;
	}
	
	/**
	 * Format default type
	 * @param string $data
	 * @return array
	 */
	public function format_default_type($type_id){
		$return = array();
		foreach ($this->fields as $field){
			$return[$field] = "-";
		}
		$return["output_name"] = "-";
		return $return;
	}
	
	/**
	 * Set format settings
	 * @param array $data
	 * @return array
	 */
	public function set_format_settings($data){
		foreach($data as $key=>$value){
			if(isset($this->format_settings[$key]))
				$this->format_settings[$key] = $value;
		}
	}
	
	/**
	 * Add rating languages
	 * @param array $fields
	 */
	public function lang_dedicate_module_callback_add($lang_id=false){
		if(!$lang_id) return;
		$this->CI->load->dbforge();
		$fields["values_".$lang_id] = array(
			"type" => "TEXT",
			"null" => TRUE,
		);
		$default_lang_id = $this->CI->pg_language->get_default_lang_id();
		$table_query = $this->CI->db->get(REVIEWS_TYPES_TABLE);
		$exists_fields = $table_query->list_fields();
		$this->CI->dbforge->add_column(REVIEWS_TYPES_TABLE, $fields);		
		if(in_array("values_" . $default_lang_id, $exists_fields)){
			$this->CI->db->set('values_' . $lang_id, 'values_' . $default_lang_id, false);
			$this->CI->db->update(REVIEWS_TYPES_TABLE);
		}
		
	}
	
	/**
	 * Remove rating language
	 * @param array $fields
	 */
	public function lang_dedicate_module_callback_delete($lang_id=false){
		if(!$lang_id) return;
		$field_name = "values_" . $lang_id;
		$this->CI->load->dbforge();
		$table_query = $this->CI->db->get(REVIEWS_TYPES_TABLE);
		if (in_array("values_" . $lang_id, $table_query->list_fields())){
			$this->CI->dbforge->drop_column(REVIEWS_TYPES_TABLE, $field_name);
		}
	}	
	
	/**
	 * Add new rate type
	 * @param string $rate_type_gid
	 * @param array $rate_type_data
	 * @param integer $lang_id
	 */
	public function add_rate_type($rate_type_gid, $rate_type_data, $lang_id){
		$types = $this->get_types();
		foreach($types as $type){
			if (!isset($type["values_".$lang_id][$rtgid])){
				$type["values_".$lang_id][$rtgid] = $rate_type_data;
			}
			$this->save_type($type);
		}
	}
	
	/**
	 * Add column to database
	 * @param array $fields
	 */
	public function add_column($fields){
		$this->CI->load->dbforge();
		$table_query = $this->CI->db->get(REVIEWS_TYPES_TABLE);
		$table_fields = $this->CI->db->get(REVIEWS_TYPES_TABLE)->list_fields();
		foreach($fields as $field_name=>$field_data){
			if(!in_array($field_name, $table_fields)){
				$this->CI->dbforge->add_column(REVIEWS_TYPES_TABLE, array($field_name=>$field_data));
			}
		}
	}
	
	/**
	 * Remove column from database
	 * @param string $name
	 */
	public function delete_column($fields){
		$this->CI->load->dbforge();
		$table_fields = $this->CI->db->get(REVIEWS_TYPES_TABLE)->list_fields();
		foreach($fields as $field_name){
			if(in_array($field_name, $table_fields)){
				$this->CI->dbforge->drop_column(REVIEWS_TYPES_TABLE, $field_name);
			}
		}
	}
	
	public function format_rating_type_settings(){
		$langs = ld($this->rating_type_ld, 'reviews');
		foreach($this->rating_type_settings as $rating_gid => $data){
			$this->rating_type_settings[$rating_gid]['name'] = $langs['option'][$rating_gid];
		}
		return $this->rating_type_settings;
	}
}
