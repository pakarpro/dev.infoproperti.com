<?php

$install_lang["notification_auser_reviews_object"] = "New review (for administrators)";
$install_lang["notification_auser_reviews_reply"] = "Comment to review (for administrators)";
$install_lang["notification_user_reviews_object"] = "New review";
$install_lang["notification_user_reviews_reply"] = "Comment to review";
$install_lang["tpl_auser_reviews_object_content"] = "Hello admin,\n\nThere is a new review on [domain]. Access administration panel > Feedbacks > Reviews and browse \"Users\" and \"Listings\" tabs to view it.\n\nBest regards,\n[name_from]";
$install_lang["tpl_auser_reviews_object_subject"] = "[domain] | New review";
$install_lang["tpl_auser_reviews_reply_content"] = "Hello admin,\n\nThere is a comment to review.\n\Review: [review]\n\nComment: [comment]\n\nBest regards,\n[name_from]";
$install_lang["tpl_auser_reviews_reply_subject"] = "[domain] | Comment to review";
$install_lang["tpl_user_reviews_object_content"] = "Hello [user],\n\nThere is a new review.\n\nReview from: [poster]\n\nMessage: [review]\n\nBest regards,\n[name_from]";
$install_lang["tpl_user_reviews_object_subject"] = "[domain] | New review of [type] (ID=[object_id])";
$install_lang["tpl_user_reviews_reply_content"] = "Hello [user],\n\nThere is a comment to your review.\n\nYour review: [review]\nComment: [comment]\n\nBest regards,\n[name_from]";
$install_lang["tpl_user_reviews_reply_subject"] = "[domain] | Comment to your review";
