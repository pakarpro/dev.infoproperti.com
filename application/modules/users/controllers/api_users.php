<?php
/**
* Users user side controller
*
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Mikhail Chernov <mchernov@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: mchernov $
**/

Class Api_Users extends Controller{
	
	/**
	 * Use email confirmation
	 * @var boolean
	 */
	var $use_email_confirmation = true;
	
	/**
	 * Constructor
	 */
	public function __construct(){
		parent::Controller();
		$this->load->model("Users_model");
	}
	
	/**
	 * Confirm account
	 */
	public function confirm(){
		$code = trim(strip_tags($this->input->get_post("code", true)));
		$this->Users_model->set_format_settings('gate_safe', false);
		$user = $this->Users_model->get_user_by_confirm_code($code);
		$usersss = $this->Users_model->get_user_by_confirm_code($code);
		$this->Users_model->set_format_settings('gate_safe', true);
		if(empty($user)){
			$this->set_api_content("errors", l("error_user_no_exists", "users"));
			return;
		}elseif($user["confirm"] != 0){ #modification# validation
			$this->set_api_content("errors", l("error_user_already_confirm", "users"));
			return;
		}else{
			$data["confirm"] = 1; 
			$this->Users_model->save_user($user["id"], $data);
			$this->set_api_content("messages", l("success_confirm", "users"));

			$this->load->model("users/models/Auth_model");
			$auth_data = $this->Auth_model->login($user["id"]);
			if(!empty($auth_data["errors"])){
				$this->set_api_content("errors", $auth_data["errors"]);
				return;
			}else{
				$token = $this->session->sess_create_token();
				$this->set_api_content("data", array("token" => $token));
			}
		}
	}
	
	/**
	 * Reseore password
	 */
	public function restore(){
		$email = strip_tags($this->input->get_post("email", true));
		$this->Users_model->set_format_settings('gate_safe', false);
		$user_data = $this->Users_model->get_user_by_email($email);
		$this->Users_model->set_format_settings('gate_safe', true);
		$error = "";
		if(empty($user_data) || !$user_data["id"]){
			$this->set_api_content("errors", l("error_user_no_exists", "users"));
			return;
		}elseif($user_data["status"] == 0){
			$this->set_api_content("errors", l("error_user_is_blocked", "users"));
			return;
		}elseif($user_data["confirm"] != 0){
			$this->set_api_content("errors", l("error_unconfirmed_user", "users"));
			return;
		}else{
			
			$user_id = $user_data["id"];
			$user_data["password"] = $user_data["repassword"] = $new_password = substr(md5(date("Y-m-d H:i:s") . $user_data["email"]), 0, 6);
			$validate_data = $this->Users_model->validate_user($user_data["id"], $data);
			
			if(!empty($validate_data['errors'])){
				$this->set_api_content("messages", implode('<br>', $validate_data['errors']));
			}else{
				$this->Users_model->save_user($user_data["id"], $validate_data['data']);
				$this->load->model("Notifications_model");
				$return = $this->Notifications_model->send_notification($user_data["email"], "users_fogot_password", $user_data);		
				$this->set_api_content("messages", l("success_restore_mail_sent", "users"));
			}
		}
	}
	
	/**
	 * Save password
	 */
	public function save_password(){
		$user_id = $this->session->userdata("user_id");
		$user_type = $this->Users_model->get_user_type_by_id($user_id);
		$validate_method = "validate_".$user_type;

		$this->load->model("users/models/Auth_model");
		$this->Auth_model->update_user_session_data($user_id);
		
		$post_data = array(
			"password" => $this->input->get_post("password", true),
			"repassword" => $this->input->get_post("password", true),
		);
		$validate_data = $this->Users_model->$validate_method($user_id, $post_data);

		if(!empty($validate_data["errors"])){
			$this->set_api_content("errors", $validate_data["errors"]);
			return;
		}else{
			$save_data = $validate_data["data"];
			$save_password = $save_data["password"];
			$save_data["password"] = $this->Users_model->encode_password($save_data["password"]);
			$user_id = $this->Users_model->save_user($user_id, $save_data);

			///// send notification
			$this->load->model("Notifications_model");
			$user_data = $this->Users_model->get_user_by_id($user_id);
			$user_data["password"] = $save_password;
			$return = $this->Notifications_model->send_notification($user_data["email"], "users_change_password", $user_data);

			$this->set_api_content("messages", l("success_user_updated", "users"));
		}
	}
	
	/**
	 * Save email
	 */
	public function save_email(){
//		if($this->session->userdata("auth_type") != "user"){
//			$this->set_api_content("errors", l("error_invalid_user_type", "users"));
//		}
		$user_id = $this->session->userdata("user_id");
		$user_type = $this->Users_model->get_user_type_by_id($user_id);
		$validate_method = "validate_".$user_type;

		$this->load->model("users/models/Auth_model");
		$this->Auth_model->update_user_session_data($user_id);
		
		$post_data = array(
			"email" => $this->input->get_post("email", true),
		);
		$validate_data = $this->Users_model->$validate_method($user_id, $post_data);

		if(!empty($validate_data["errors"])){
			$this->set_api_content("errors", $validate_data["errors"]);
			return;
		}else{
			$user_data = $this->Users_model->get_user_by_id($user_id);
			$save_data = $validate_data["data"];
			$user_id = $this->Users_model->save_user($user_id, $save_data);

			///// send notification
			$this->load->model("Notifications_model");
			$user_data["new_email"] = $save_data["email"];
			$this->Notifications_model->send_notification($user_data["email"], "users_change_email", $user_data);
			$this->Notifications_model->send_notification($user_data["new_email"], "users_change_email", $user_data);

			$this->load->model("users/models/Auth_model");
			$this->Auth_model->update_user_session_data($user_id);

			$this->set_api_content("messages", l("success_user_updated", "users"));
		}
	}
	
	/**
	 * Render user profile action
	 */
	public function profile($action="view"){
		$user_id = $this->session->userdata("user_id");
		
		$this->Users_model->set_format_settings(array('get_safe'=>false, 'get_company'=>true));
		$data = $this->Users_model->get_user_by_id($user_id);
		$this->Users_model->set_format_settings(array('get_safe'=>true, 'get_company'=>false));

		if(!$data){
			$this->set_api_content('error', l('api_error_not_access', 'users'));
			return;
		}

		$user_type = $data['user_type'];

		if($this->input->get_post("save")){

			$post_data = $this->input->get_post("data", true);
			
			$validate_method = "validate_".$user_type;
			$validate_data = $this->Users_model->$validate_method($user_id, $post_data, "user_icon");
			if(isset($_FILES["user_icon"]) && is_array($_FILES["user_icon"]) && is_uploaded_file($_FILES["user_icon"]["tmp_name"])){
				$this->CI->load->model("Uploads_model");
				$validate_image = $this->CI->Uploads_model->validate_upload($this->upload_config_id, "user_icon");
				$validate_date["errors"] = array_merge($validate_data["errors"], $validate_image["error"]);
			}
			if(!empty($validate_data["errors"])){
				$this->set_api_content("errors", $validate_data["errors"]);
				return;
			}else{
				if($action != "subscriptions"){
					if ($this->input->post("user_icon_delete")){
						$this->load->model("Uploads_model");
						if($data["user_logo_moderation"]){
							$this->Uploads_model->delete_upload($this->Users_model->upload_config_id, $user_id."/", $data["user_logo_moderation"]);
							$validate_data["data"]["user_logo_moderation"] = "";
							$this->load->model("Moderation_model");
							$this->Moderation_model->delete_moderation_item_by_obj($this->Users_model->moderation_type, $user_id);
						}elseif($data["user_logo"]){
							$this->Uploads_model->delete_upload($this->Users_model->upload_config_id, $user_id."/", $data["user_logo"]);
							$validate_data["data"]["user_logo"] = "";
						}
					}
					if($this->Users_model->save_user($user_id, $validate_data["data"], "user_icon")){
						$this->set_api_content("messages", l("success_update_user", "users"));
					}
				}else{
					////save user subscribers
					$user_subscriptions_list = $this->input->post("user_subscriptions_list", true);
					$this->load->model("subscriptions/models/Subscriptions_users_model");
					$this->Subscriptions_users_model->save_user_subscriptions($user_id, $user_subscriptions_list);
				}

				$this->load->model("users/models/Auth_model");
				$this->Auth_model->update_user_session_data($user_id);

				//redirect(site_url()."users/profile");
			}
		}

		if(!$action) $action = "view";

		$data = $this->Users_model->format_user($data);

		$this->config->load("date_formats", TRUE);
		$page_data["date_format"] = $this->config->item("st_format_date_literal", "date_formats");
		$this->set_api_content("data", $data);
	}
	
	/**
	public function register($user_type=null){
		if(!$user_type){
			$user_types = $this->Users_model->get_user_types();
			$user_type = current($user_types);
		}
		$post_data = $this->input->post("data", true);

		$validate_method = 'validate_' . $user_type;
		$validate_data = $this->Users_model->$validate_method(null, $post_data, 'user_icon');
		if (!empty($validate_data["errors"])) {
			$this->set_api_content('errors', $validate_data["errors"]);
			$this->session->sess_destroy();
			return;
		}
		$data = $validate_data["data"];
		$data["status"] = 1;
		if ($this->use_email_confirmation) {
			$data["confirm"] = 0;
			$data["confirm_code"] = substr($this->Users_model->encode_confirm(date("Y-m-d H:i:s") . $data["nickname"]), 0, 10);
		} else {
			$data["confirm"] = 1;
			$data["confirm_code"] = "";
		}
		$saved_password = $data["password"];
		$data["password"] = $this->Users_model->encode_password($data["password"]);
		$data["lang_id"] = $this->pg_language->get_default_lang_id();
		
		$user_id = $this->Users_model->save_user(null, $data, 'user_icon');
		$this->set_api_content('messages', l('success_user_updated', 'users'));
		
		////save user subscribers
		if($this->pg_module->is_module_installed("subscriptions")){
			$this->load->model("subscriptions/models/Subscriptions_users_model");
			$this->Subscriptions_users_model->save_user_subscriptions($user_id, $_REQUEST['user_subscriptions_list']);
		}

		$this->load->model('Notifications_model');
		$data["password"] = $saved_password;
		if ($this->use_email_confirmation) {
			$link = site_url() . "users/confirm/" . $data["confirm_code"];
			$data["confirm_block"] = str_replace("[link]", $link, l('confirm_block_text', 'users'));
		}
		$return = $this->Notifications_model->send_notification($data["email"], 'users_registration', $data);
		
		if (!$this->use_email_confirmation) {
			$this->load->model("users/models/Auth_model");
			$auth_data = $this->Auth_model->login($user_id);
			if (!empty($auth_data["errors"])) {
				$this->set_api_content('errors', $auth_data["errors"]);
				$this->session->sess_destroy();
			}  else {
				$token = $this->session->sess_create_token();
				$this->set_api_content('data', array('token' => $token));
			}
		} else {
			$this->set_api_content('errors', l('info_please_checkout_mailbox', 'users'));
		}
	}*/
	
	/**
	 * Render view profile action
	 * @param integer $user_id
	 */
	public function view($user_id){
		if(!$user_id){
			$this->set_api_content('error', l('api_error_not_access', 'users')); 
			return;
		}
		
		$this->Users_model->set_format_settings('get_contact', true);
		$user = $this->Users_model->get_user_by_id($user_id);
		$this->Users_model->set_format_settings('get_contact', false);
		
		if(!$user){
			$this->set_api_content('error', l('api_error_not_access', 'users'));
			return;
		}
		
		if($this->session->userdata('auth_type') == 'user'){
			$auth_id = $this->session->userdata('user_id');
		}else{
			$auth_id = 0;
		}
		
		if(!$user['status'] && $auth_id != $user_id){
			$this->set_api_content('error', l('api_error_not_access', 'users'));
			return;
		}
		
		$data = $this->Users_model->get_show_data(array($user));
		$data = array_shift($data);
		
		$this->Users_model->check_views_count($user_id);
		
		$this->set_api_content('data', (array)$data);
	}
	
	/**
	 * Process request selected agents action
	 * @param string $action
	 * @param integer $agent_id	 
	 */
	public function my_agents_request($action="approve", $agent_id=null){
		$errors = false;
		$messages = array();
		if(!$agent_id){
			$agent_id = $this->input->post("ids");
		}		
		if(!empty($agent_id)){
			$company_id = $this->session->userdata("user_id");
			foreach((array)$agent_id as $i){
				$error = $this->Users_model->{$action."_agent"}($company_id, $i);
				if($error){
					$errors = true;
					$messages[] = $error;
				}else{
					$messages[] = l("success_".$action."_agent", "users");
				}
			}
			if($errors){
				$this->set_api_content("errors", implode("<br>", $messages));
			}else{
				$this->set_api_content("messages", implode("<br>", $messages));
			}
		}
	}

	/**
	 * Remove selected agents action
	 * @param integer $id
	 */
	public function my_agents_delete($agent_id=null){
		$errors = false;
		$messages = array();
		if(!$agent_id){
			$agent_id = $this->input->post("ids");
		}		
		if(!empty($agent_id)){
			$company_id = $this->session->userdata("user_id");
			foreach((array)$agent_id as $i){
				$error = $this->Users_model->delete_agent($company_id, $i);
				if($error){
					$errors = true;
					$messages[] = $error;
				}else{
					$messages[] = l("success_deleted_agent", "users");
				}
			}
			if($error){
				$this->set_api_content("errors", implode("<br>", $messages));
			}else{
				$this->set_api_content("messages", implode("<br>", $messages));
			}
		}
	}
	
	/**
	 * Get out from company
	 */
	public function agent_get_out(){
		$user_id = $this->session->userdata("user_id");
		$user = $this->Users_model->get_user_by_id($user_id);
		$error = $this->Users_model->delete_agent($user["agent_company"], $user_id);
		if($error){
			$this->set_api_content("errors", $error);
		}else{
			$this->set_api_content("messages", l("success_agent_get_out", "users"));
		}
	}
	
	/**
	 * Return account
	 */
	public function account(){
		if($this->session->userdata("auth_type") != "user"){
			$this->set_api_content('error', l('api_error_not_access', 'users'));
			return;
		}
		
		$user_id = $this->session->userdata("user_id");

		$this->load->model("users/models/Auth_model");
		$this->Auth_model->update_user_session_data($user_id);

		$data = $this->Users_model->get_user_by_id($user_id);
		$this->set_api_content('account', $data['account']);
	}
	
	/**
	 * Render index list action
	 * @param string $user_type type of user
	 * @param string $order order sorting
	 * @param string $order_direction order direction
	 * @param integer $page page of results
	 */
	public function index($user_type=null, $order='date_created', $order_direction='DESC', $page=1){
		$current_settings = isset($_SESSION["users_list"])?$_SESSION["users_list"]:array();
		if(!isset($current_settings["order"]))
			$current_settings["order"] = "date_created";
		if(!isset($current_settings["order_direction"]))
			$current_settings["order_direction"] = "DESC";
		if(!isset($current_settings["page"]))
			$current_settings["page"] = 1;
		if(!isset($current_settings["data"]))
			$current_settings["data"] = array();

		if($current_settings['data']['type'] != $user_type){
			$current_settings['data']['type'] = $user_type;
			$current_settings['page'] = 1;
		}
		$this->template_lite->assign('user_type', $user_type);
	
		$filters = $current_settings['data'];
		$filters['active'] = 1;
		
		if($this->session->userdata("auth_type") == "user")
			$filters['not_user'] = $this->session->userdata("user_id");

		$order = trim(strip_tags($order));
		if(!empty($order)) $current_settings['order'] = $order;
		$order = $current_settings['order'];

		$order_direction = strtoupper(trim(strip_tags($order_direction)));
		if(!empty($order_direction)) $current_settings['order_direction'] = $order_direction;
		$order_direction = $current_settings['order_direction'];
		
		if(!empty($page)) $current_settings['page'] = $page;
		$page = $current_settings['page'];
		
		if($order_direction != 'ASC') $order_direction = 'DESC';
		
		$_SESSION['users_pagination'] = array(
			'order' => $order,
			'order_direction' => $order_direction,
			'page' => $page,
			'data' => $filters,
		);
		
		$view_mode = isset($_SESSION['users_view_mode']) ? $_SESSION['users_view_mode'] : 'list';
		
		$items_on_page = $this->pg_module->get_module_config("start", "index_items_per_page");
		
		$use_map_in_search = true;
		if($user_type == 'company'){
		    if($view_mode == 'list'){
				$use_map_in_search = $this->pg_module->get_module_config('users', 'use_map_in_search');
			}
		}
	 
		$users = array();
		$users_count = $this->Users_model->get_users_count_by_filters($filters);
		if($users_count > 0){
			$order_array = array();
			if($order == 'name'){
				if($user_type == 'company'){
					$order_array = array('company_name' => $order_direction);
				}else{
					$order_array = array("CONCAT(fname, ' ', sname)" => $order_direction);
				}
			}else{
				$order_array = array($order => $order_direction);
			}
		
			$this->Users_model->set_format_settings('get_contact', true);
			$users = $this->Users_model->get_users_list_by_filters($filters, $page, $items_on_page, $order_array);
			$this->Users_model->set_format_settings('get_contact', false);
		}
	
		$_SESSION['users_list'] = $current_settings;
		
		$this->set_api_content('users', $users);
		$this->set_api_content('users_count', $users_count);
		$this->set_api_content('settings', $current_settings);
	}
	
	/**
	 * Search users
	 */
	public function search($user_type=null, $keyword='', $order='date_created', $order_direction='DESC', $page=1){
		$user_types = $user_types_default = $this->Users_model->get_user_types();
		if(empty($user_types)){
			$this->set_api_content('data', array());
		}
		
		foreach($user_types as $key=>$value){
			$search_enabled = $this->pg_module->get_module_config("users", $value."_search_enabled");
			if(!$search_enabled){
				unset($user_types[$key]);
			}
		}
		
		$search_disabled = false;
		if(empty($user_types)){
			$search_disabled = true;
			$user_types = $user_types_default;
		}
		$this->set_api_content->assign('user_types', $user_types);
		
		$current_settings = isset($_SESSION['users_list']) ? $_SESSION['users_list'] : array();
		$current_settings["order"] = "date_created";
		$current_settings["order_direction"] = "DESC";
		$current_settings["page"] = 1;
		$current_settings["data"] = array();
		
		$user_type = $this->input->post("user_type");
		if($user_type) $current_settings['data']['type'] = $user_type;
		
		$country = strval($this->input->post("id_country"));
		if($country) $current_settings["data"]["country"] = $country;
		
		$region = intval($this->input->post("id_region"));
		if($region) $current_settings["data"]["region"] = $region;
		
		$city = intval($this->input->post("id_city"));
		if($city) $current_settings["data"]["city"] = $city;
		
		$zip = $this->input->post("zip");
		$zip = trim(strip_tags($zip));
		if(!empty($zip)) $current_settings["data"]["zip"] = intval($zip);
		
		$keyword = $this->input->post("keyword");
		$keyword = trim(strip_tags($keyword));
		if(!empty($keyword)) $current_settings["data"]["keyword"] = $keyword;
	
		$_SESSION['users_list'] = $current_settings;
		
		$this->set_api_content('data', $current_search);
	}
}
