<?php
/**
* Users admin side controller
*
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/

Class Admin_Users extends Controller{
	
	/**
	 * Filters
	 * @var array
	 */
	private $filter_arr = array('all', 'not_active', 'active', 'not_confirm', 'news_privilege');

	/**
	 * Constructor
	 */
	public function Admin_Users(){
		parent::Controller();
		$this->load->model("Users_model");
		$this->load->model("Menu_model");
		$this->Menu_model->set_menu_active_item("admin_menu", "users_menu_item");
	}

	/**
	 * Render list of users action
	 * @param string $user_type
	 * @param string $filter
	 * @param string $order_direction
	 * @param integer $page
	 */
	public function index($user_type=null, $filter="all", $order="email", $order_direction="ASC", $page=1){
		#mod for user search#
		if($this->input->post("cancel_search"))
		{
			$this->session->unset_userdata("search-user");	
		}
		
		if($this->input->post("btn_save"))
		{
			$search = $this->input->post("search-user");
			$this->session->set_userdata("search-user", $search);	
		}
		$this->template_lite->assign("search", $this->session->userdata("search-user"));
		#end of mod#
		$current_settings = isset($_SESSION["users_list"])?$_SESSION["users_list"]:array();
		//if(!isset($current_settings["group_id"])) $current_settings["group_id"] = 0;
		if(!isset($current_settings["user_type"])) $current_settings["user_type"] = $user_type;
		if(!isset($current_settings["filter"])) $current_settings["filter"] = $filter;
		if(!isset($current_settings["order"])) $current_settings["order"] = $order;
		if(!isset($current_settings["order_direction"])) $current_settings["order_direction"] = $order_direction;
		if(!isset($current_settings["page"])) $current_settings["page"] = $page;

		/*if(!isset($group_id)) $group_id = $current_settings["group_id"];
		$this->template_lite->assign('group_id', $group_id);
		$current_settings["group_id"] = $group_id;

		if($group_id>0){
			$attrs["where"]['group_id'] = $search_params["where"]["group_id"] = $group_id;
		}*/
		
		$user_types = $this->Users_model->get_user_types();
		
		if(!in_array($filter, $this->filter_arr)) $filter = $this->filter_arr[0];
		
		if(!$user_type) $user_type = $current_settings["user_type"];
		
		if(!in_array($user_type, $user_types)) $user_type = current($user_types);
		$this->template_lite->assign("user_type", $user_type);
		$current_settings["user_type"] = $user_type;
		foreach($this->filter_arr as $value){
			$filters = array();
			$filters["type"] = $user_type;
			#mod for user search#
			$filters["email"] = $this->session->userdata("search-user");
			$filters["fname"] = $this->session->userdata("search-user");
			$filters["sname"] = $this->session->userdata("search-user");
			#end of mod#			
			if($value != "all")	$filters[$value] = "1";
			$filter_data[$value] = $this->Users_model->get_users_count_by_filters($filters);
		}
		
		$current_settings["filter"] = $filter;

		$this->template_lite->assign("filter", $filter);
		$this->template_lite->assign("filters", $this->filter_arr);
		$this->template_lite->assign("filter_data", $filter_data);
		$current_settings["page"] = $page;

		if(!$order) $order = $current_settings["order"];
		$this->template_lite->assign("order", $order);
		$current_settings["order"] = $order;

		if(!$order_direction) $order_direction = $current_settings["order_direction"];
		$this->template_lite->assign("order_direction", $order_direction);
		$current_settings["order_direction"] = $order_direction;

		$users_count = $filter_data[$filter];

		if(!$page) $page = $current_settings["page"];
		$items_on_page = $this->pg_module->get_module_config("start", "admin_items_per_page");
		$this->load->helper("sort_order");
		$page = get_exists_page_number($page, $users_count, $items_on_page);
		$current_settings["page"] = $page;

		$_SESSION["users_list"] = $current_settings;

		$sort_links = array(
			"name" => site_url()."admin/users/index/".$user_type."/".$filter."/name/".(($order!="name" xor $order_direction=="DESC")?"ASC":"DESC"),
			"email" => site_url()."admin/users/index/".$user_type."/".$filter."/email/".(($order!="email" xor $order_direction=="DESC")?"ASC":"DESC"),
			"account" => site_url()."admin/users/index/".$user_type."/".$filter."/account/".(($order!="account" xor $order_direction=="DESC")?"ASC":"DESC"),
			"date_created" => site_url()."admin/users/index/".$user_type."/".$filter."/date_created/".(($order!="date_created" xor $order_direction=="DESC")?"ASC":"DESC"),
		);

		$this->template_lite->assign("sort_links", $sort_links);
		
		if($order == "name"){
			switch($user_type){
				case "company": $order = "company_name"; break;
				default: $order = "fname, sname";
				break;
			}
		}; 

		if($users_count > 0){
			$filters = array();
			$filters["type"] = $user_type;
			#mod for user search#
			$filters["email"] = $this->session->userdata("search-user");
			$filters["fname"] = $this->session->userdata("search-user");
			$filters["sname"] = $this->session->userdata("search-user");
			#end of mod#			
			if($filter != "all") $filters[$filter] = "1";
			$this->Users_model->set_format_settings('get_safe', false);
			$users = $this->Users_model->get_users_list_by_filters($filters, $page, $items_on_page, array($order => $order_direction));
			$this->Users_model->set_format_settings('get_safe', true);
			$this->template_lite->assign("users", $users);
		}
		$this->load->helper("navigation");
		$this->config->load("date_formats", TRUE);
		$url = site_url()."admin/users/index/".$user_type."/".$filter."/".$order."/".$order_direction."/";
		$page_data = get_admin_pages_data($url, $users_count, $items_on_page, $page, "briefPage");
		$page_data["date_format"] = $this->config->item("st_format_date_time_literal", "date_formats");
		$this->template_lite->assign("page_data", $page_data);

		$this->load->model("users/models/Groups_model");
		$groups = $this->Groups_model->get_groups_list();
		$this->template_lite->assign("groups", $groups);
		
		$this->Menu_model->set_menu_active_item("admin_users_types_menu", "users_types_".$user_type."_item");
		$this->system_messages->set_data("header", l("admin_header_users_list", "users"));
		$this->template_lite->view("list");
	}
	
	/**
	 * Render edit user action
	 * @param integer $user_id
	 */
	public function edit($user_id, $user_type=null){
		if($user_id){
			$this->Users_model->set_format_settings(array('get_safe'=>false, 'get_company'=>true));
			$data = $this->Users_model->get_user_by_id($user_id);
			$this->Users_model->set_format_settings(array('get_safe'=>true, 'get_company'=>false));
		}else{
			$data["lang_id"] = $this->pg_language->current_lang_id;
			$data["user_type"] = $user_type;
			$data = $this->Users_model->format_user($data);
		}

		if($this->input->post("btn_save")){
			$post_data = $this->input->post("data", true);
			$post_data["user_type"] = $data['user_type'];
			$post_data["id_country"] = $this->input->post("id_country", true);
			$post_data["id_region"] = $this->input->get_post("id_region", true);
			$post_data["id_city"] = $this->input->get_post("id_city", true);
			$post_data["services"] = $this->input->post("services", true);			
			
			$agent_company = $post_data["agent_company"];
			unset($post_data["agent_company"]);
			
			if($user_id){
				$post_data["id"] = $user_id;
				if(!intval($this->input->post("update_password"))){
					unset($post_data["password"]);
					unset($post_data["repassword"]);
				}
				
				if($this->input->post("user_icon_delete")){
					$this->Users_model->delete_logo($user_id);
				}
			}
			
			if($post_data['services']['featured'] != $data['is_featured']){
				if($post_data['services']['featured']){
					$post_data["featured_end_date"] = date("Y-m-d H:i:s", time()+$this->Users_model->service_period_default*24*60*60);	
				}else{
					$post_data["featured_end_date"] = "0000-00-00 00:00:00";
				}
			}
			
			if($post_data['services']['show_logo'] != $data['is_show_logo']){
				if($post_data['services']['show_logo']){
					$post_data["show_logo_end_date"] = date("Y-m-d H:i:s", time()+$this->Users_model->service_period_default*24*60*60);	
				}else{
					$post_data["show_logo_end_date"] = "0000-00-00 00:00:00";
				}
			}
			
			$validate_data = $this->Users_model->validate_user($user_id, $post_data, 'user_icon');
			if(!empty($validate_data["errors"])){
				$this->system_messages->add_message("error", implode("<br>", $validate_data["errors"]));
			}else{
				$save_data = $validate_data["data"];
				if(!$user_id){
					$save_data['user_type'] = $data['user_type'];
					$save_data['lang_id'] = $this->pg_language->current_lang_id;
				}
				$user_id = $this->Users_model->save_user($user_id, $save_data, 'user_icon', false);		
			
				if($agent_company){
					$this->Users_model->save_agent_company($user_id, $agent_company, false);
					$this->Users_model->save_agent_company_status($user_id, $agent_company);
					
					$this->load->model("Linker_model");
					$this->Linker_model->add_link("users_contacts", $user_id, $agent_company);
					$this->Linker_model->add_link("users_contacts", $agent_company, $user_id);
				}
		
				$this->load->model('geomap/models/Geomap_model');
				$this->load->model('geomap/models/Geomap_settings_model');
		
				$map_gid = $this->Geomap_model->get_default_driver_gid();
				
				$post_settings = $this->input->post('map', true);
				$post_settings['lat'] = $validate_data['data']['lat'];
				$post_settings['lon'] = $validate_data['data']['lon'];
				
				$validate_data = $this->Geomap_settings_model->validate_settings($post_settings);
				if(empty($validate_data["errors"])){
					$this->Geomap_settings_model->save_settings($map_gid, 0, $user_id, 'user_profile', $validate_data["data"]);
				}
		
				if(!$data['id']){
					$this->load->model("Notifications_model");
					$user_data = $save_data;
					$user_data['password'] = $post_data['password'];
					$this->Notifications_model->send_notification($user_data['email'], "user_account_create_by_admin", $user_data);
				}
				
				$this->system_messages->add_message("success", l("success_".($user_id ? "update" : "add")."_user", "users"));
				$cur_set = $_SESSION["users_list"];
				$url = site_url()."admin/users/index/".$cur_set["user_type"]."/".$cur_set["filter"]."/".$cur_set["order"]."/".$cur_set["order_direction"]."/".$cur_set["page"]."";
				redirect($url);
			}
			
			$data = array_merge($data, $post_data);
		}

		$this->template_lite->assign("user_gender_lang", ld("user_gender", "users"));
		$this->template_lite->assign("langs", $this->pg_language->languages);
		$this->template_lite->assign("data", $data);
		$this->template_lite->assign("user_type", $data['user_type']);

		$this->load->model("users/models/Users_deactivated_reason_model");
		
		if(!$lang_id || !array_key_exists($lang_id, $this->pg_language->languages)){
			$lang_id = $this->pg_language->current_lang_id;
		}

		$weekdays = $this->pg_language->ds->get_reference("start", "weekday-names");
		$this->template_lite->assign("weekdays", $weekdays);
		
		$dayhours = $this->pg_language->ds->get_reference("start", "dayhour-names");
		$this->template_lite->assign("dayhours", $dayhours);
		
		if($data["user_type"] == 'company'){
			$markers = array(
				array( 
					"gid" => $data["id"], 
					"country" => $data["country"],
					"region" => $data["region"],
					"city" => $data["city"],
					"address" => $data["address"],
					"postal_code" => $data["postal_code"],
					"lat" => (float)$data["lat"], 
					"lon" => (float)$data["lon"], 
					"info" => ($data['location'] ? $data['location'] : $data["output_name"].', '.l($data['user_type'], 'users')),
					"dragging" => true,
				),
			);
			$this->template_lite->assign("markers", $markers);

			$current_language = $this->pg_language->get_lang_by_id($this->pg_language->current_lang_id);
			$view_settings = array(
				"class" => "",
				"disable_smart_zoom" => 1,
				"zoom_listener" => "get_user_zoom_data", 
				"type_listener" => "get_user_type_data",
				"drag_listener" => "get_user_drag_data",
				"lang" => $current_language["code"],
			);
			$this->template_lite->assign("map_settings", $view_settings);		
		
			if($user_id){
				$this->load->model('geomap/models/Geomap_model');
				$this->load->model('geomap/models/Geomap_settings_model');
				$map_gid = $this->Geomap_model->get_default_driver_gid();
				$map_settings = $this->Geomap_settings_model->get_settings($map_gid, 0, $user_id, 'user_profile');
				$this->template_lite->assign('user_map_settings', $map_settings);
			}
		}
		
		$use_phone_format = $this->pg_module->get_module_config("start", "use_phone_format");
		if($use_phone_format){
			$phone_format = $this->pg_module->get_module_config("start", "phone_format");
			$this->template_lite->assign('phone_format', $phone_format);
		}
		
		///// check pay services exists
		if($this->pg_module->is_module_installed("users_services")){
			$this->load->model('Users_services_model');
			$users_services = $this->Users_services_model->get_active_services($data["user_type"]);
			foreach($users_services as $service){
				$this->template_lite->assign("use_".str_replace(array($data["user_type"].'_', '_services'), '', $service["gid"]), $service["status"]);
			}
			$this->template_lite->assign("use_services", !empty($users_services));
		}else{
			$this->template_lite->assign("use_services", false);
		}
		
		$cur_set = $_SESSION["users_list"];
		$back_url = site_url()."admin/users/index/".$cur_set["user_type"]."/".$cur_set["filter"]."/".$cur_set["order"]."/".$cur_set["order_direction"]."/".$cur_set["page"]."";
		$this->template_lite->assign("back_url", $back_url);
		$this->system_messages->set_data("header", l("admin_header_users_list", "users"));
		$this->template_lite->view("edit_form");
	}
	
	/**
	 * Render add user action
	 * @param string $user_type
	 */
	public function add($user_type=null){
		if(!$user_type){
			$user_types = $this->Users_model->get_user_types();
			$user_type = current($user_types);
		}
		$this->edit(null, $user_type);
	}

	/**
	 * Remove user action
	 * @param integer $user_id
	 */
	public function delete($user_id){
		if(!empty($user_id)){
			$this->Users_model->delete_user($user_id);
			$this->system_messages->add_message("success", l("success_delete_user", "users"));
		}
		$cur_set = $_SESSION["users_list"];
		$url = site_url()."admin/users/index/".$cur_set["user_type"]."/".$cur_set["filter"]."/".$cur_set["order"]."/".$cur_set["order_direction"]."/".$cur_set["page"]."";
		redirect($url);
	}

	/**
	 * Activate user action
	 * @param integer $user_id
	 * @param integer $status
	 */
	public function activate($user_id, $status=0){
		if(!empty($user_id)){
			$this->Users_model->activate_user($user_id, $status);
			$this->system_messages->add_message("success", l("success_".($status ? "activate" : "deactivate")."_user", "users"));
		}
		$cur_set = $_SESSION["users_list"];
		$url = site_url()."admin/users/index/".$cur_set["user_type"]."/".$cur_set["filter"]."/".$cur_set["order"]."/".$cur_set["order_direction"]."/".$cur_set["page"]."";
		redirect($url);
	}

	/**
	 * Change group of user
	 * @param integer $user_id
	 * @param string $group_gid
	 */
	public function ajax_change_user_group($user_id, $group_gid){
		$this->load->model("users/models/Groups_model");
		$group_gid = strval($group_gid);
		$group_data = $this->Groups_model->get_group_by_gid($group_gid);
		$group_id = (!empty($group_data))?$group_data["id"]:0;

		$user_id = intval($user_id);
		if($user_id && $group_id){
			$save_data["group_id"] = $group_id;
			$user_id = $this->Users_model->save_user($user_id, $save_data);
		}
	}

	/**
	 * Change group of users
	 * @param string $group_gid
	 */
	public function ajax_change_users_group($group_gid){
		$this->load->model("users/models/Groups_model");
		$group_gid = strval($group_gid);
		$group_data = $this->Groups_model->get_group_by_gid($group_gid);
		$group_id = (!empty($group_data))?$group_data["id"]:0;

		$user_ids = $this->input->post("user_ids");
		if(!empty($user_ids) && $group_id){
			$save_data["group_id"] = $group_id;
			foreach($user_ids as $user_id){
				$this->Users_model->save_user($user_id, $save_data);
			}
			$this->system_messages->add_message("success", l("error_user_successfully_change_group", "users"));
		}
	}

	/**
	 * Return groups as array
	 * @param integer $page
	 */
	public function groups($page=1){
		$this->load->model("users/models/Groups_model");

		$attrs = array();
		$current_settings["page"] = $page;
		if(!isset($current_settings["page"])) $current_settings["page"] = 1;

		$group_count = $this->Groups_model->get_groups_count();

		if(!$page) $page = $current_settings["page"];
		$items_on_page = $this->pg_module->get_module_config("start", "admin_items_per_page");
		$this->load->helper("sort_order");
		$page = get_exists_page_number($page, $group_count, $items_on_page);
		$current_settings["page"] = $page;

		$_SESSION["groups_list"] = $current_settings;

		if ($group_count > 0){
			$groups = $this->Groups_model->get_groups_list( $page, $items_on_page, array("date_created" => "DESC"));
			$this->template_lite->assign("groups", $groups);
		}
		
		$this->load->helper("navigation");
		$this->config->load("date_formats", TRUE);
		$url = site_url()."admin/users/groups/";
		$page_data = get_admin_pages_data($url, $group_count, $items_on_page, $page, "briefPage");
		$page_data["date_format"] = $this->config->item("st_format_date_time_literal", "date_formats");
		$this->template_lite->assign("page_data", $page_data);

		$this->Menu_model->set_menu_active_item("admin_users_menu", "groups_list_item");
		$this->system_messages->set_data("header", l("admin_header_groups_list", "users"));
		$this->template_lite->view("groups_list");
	}

	/**
	 * Edit group action
	 */
	public function group_edit($group_id=null){
		$this->load->model("users/models/Groups_model");
		if($group_id){
			$data = $this->Groups_model->get_group_by_id($group_id);
		}else{
			$data = array();
		}

		if($this->input->post("btn_save")){
			$post_data = array(
				"gid" => $this->input->post("gid", true),
			);

			$langs_data = $this->input->post("langs", true);
			$validate_data = $this->Groups_model->validate_group($group_id, $post_data);
			if(!empty($validate_data["errors"])){
				$this->system_messages->add_message("error", $validate_data["errors"]);
				$data = array_merge($data, $validate_data["data"]);
			}else{
				$data = $validate_data["data"];
				$group_id = $this->Groups_model->save_group($group_id, $data, $langs_data);
				$this->system_messages->add_message("success", l("success_".($group_id ? "update" : "add")."_group", "users"));
				$url = site_url()."admin/users/groups";
				redirect($url);
			}
		}

		$data = $this->Groups_model->format_group($data);
		$data["langs"] = $this->Groups_model->_get_group_string_data($group_id);

		$this->template_lite->assign("languages", $this->pg_language->languages);
		$this->template_lite->assign("data", $data);

		$this->system_messages->set_data("header", l("admin_header_groups_edit", "users"));
		$this->template_lite->view("group_edit_form");
	}

	/**
	 * Make group as default
	 */
	public function group_set_default($group_id){
		if(!empty($group_id)){
			$this->load->model("users/models/Groups_model");
			$this->Groups_model->set_default($group_id);
			$this->system_messages->add_message("success", l("success_defaulted_group", "users"));
		}
		$current_settings = $_SESSION["groups_list"];
		$url = site_url()."admin/users/groups/".$current_settings["page"];
		redirect($url);
	}

	/**
	 * Remove group
	 * @param integer $group_id
	 */
	public function group_delete($group_id){
		if(!empty($group_id)){
			$this->load->model("users/models/Groups_model");
			$group_data = $this->Groups_model->get_group_by_id($group_id);
			if($group_data["is_default"]){
				$this->system_messages->add_message("error", l("error_cant_delete_default_group", "users"));
			}else{
				$this->Groups_model->delete_group($group_id);
				$this->system_messages->add_message("success", l("success_delete_group", "users"));
			}
		}
		$current_settings = $_SESSION["groups_list"];
		$url = site_url()."admin/users/groups/".$current_settings["page"]."";
		redirect($url);
	}

	/**
	 * Render users types settings 
	 * @param string $user_type
	 * @param string $name
	 * @param integer $value
	 */
	public function types_settings($user_type=null, $name=null, $value=null){
		
		if($this->input->post("btn_auth_save")){
			$post_data = $this->input->post('data', true);
			$validate_data = $this->Users_model->validate_settings($post_data);
			
			if(!empty($validate_data["errors"])){
				$this->system_messages->add_message("error", $validate_data["errors"]);
			}else{
				foreach($validate_data["data"] as $setting=>$value){
					$this->pg_module->set_module_config("users", $setting, $value);
				}
				
				///// menu updates
				$this->load->model("Menu_model");
				$menu = $this->Menu_model->get_menu_by_gid("company_account_menu");
				$menu_item = $this->Menu_model->get_menu_item_by_gid("my_agents_item", $menu["id"]);
				$this->Menu_model->save_menu_item($menu_item["id"], array("status"=>$validate_data["data"]["agent_login_enabled"] ? 1 : 0));
				
				// menu updates professionals
				$menu = $this->Menu_model->get_menu_by_gid("guest_main_menu");
				$menu_item = $this->Menu_model->get_menu_item_by_gid("main-menu-pro-agent-item", $menu["id"]);
				$this->Menu_model->save_menu_item($menu_item["id"], array("status"=>$validate_data["data"]["agent_search_enabled"] ? 1 : 0));
				$menu_item = $this->Menu_model->get_menu_item_by_gid("main-menu-pro-company-item", $menu["id"]);
				$this->Menu_model->save_menu_item($menu_item["id"], array("status"=>$validate_data["data"]["company_search_enabled"] ? 1 : 0));
				$menu_item = $this->Menu_model->get_menu_item_by_gid("main-menu-pro-private-item", $menu["id"]);
				$this->Menu_model->save_menu_item($menu_item["id"], array("status"=>$validate_data["data"]["private_search_enabled"] ? 1 : 0));
				
				$menu = $this->Menu_model->get_menu_by_gid("private_main_menu");
				$menu_item = $this->Menu_model->get_menu_item_by_gid("private-main-menu-pro-agent-item", $menu["id"]);
				$this->Menu_model->save_menu_item($menu_item["id"], array("status"=>$validate_data["data"]["agent_search_enabled"] ? 1 : 0));
				$menu_item = $this->Menu_model->get_menu_item_by_gid("private-main-menu-pro-company-item", $menu["id"]);
				$this->Menu_model->save_menu_item($menu_item["id"], array("status"=>$validate_data["data"]["company_search_enabled"] ? 1 : 0));
				$menu_item = $this->Menu_model->get_menu_item_by_gid("private-main-menu-pro-private-item", $menu["id"]);
				$this->Menu_model->save_menu_item($menu_item["id"], array("status"=>$validate_data["data"]["private_search_enabled"] ? 1 : 0));
				
				$menu = $this->Menu_model->get_menu_by_gid("company_main_menu");
				$menu_item = $this->Menu_model->get_menu_item_by_gid("company-main-menu-pro-agent-item", $menu["id"]);
				$this->Menu_model->save_menu_item($menu_item["id"], array("status"=>$validate_data["data"]["agent_search_enabled"] ? 1 : 0));
				$menu_item = $this->Menu_model->get_menu_item_by_gid("company-main-menu-pro-company-item", $menu["id"]);
				$this->Menu_model->save_menu_item($menu_item["id"], array("status"=>$validate_data["data"]["company_search_enabled"] ? 1 : 0));
				$menu_item = $this->Menu_model->get_menu_item_by_gid("company-main-menu-pro-private-item", $menu["id"]);
				$this->Menu_model->save_menu_item($menu_item["id"], array("status"=>$validate_data["data"]["private_search_enabled"] ? 1 : 0));
				
				$menu = $this->Menu_model->get_menu_by_gid("agent_main_menu");
				$menu_item = $this->Menu_model->get_menu_item_by_gid("agent-main-menu-pro-agent-item", $menu["id"]);
				$this->Menu_model->save_menu_item($menu_item["id"], array("status"=>$validate_data["data"]["agent_search_enabled"] ? 1 : 0));
				$menu_item = $this->Menu_model->get_menu_item_by_gid("agent-main-menu-pro-company-item", $menu["id"]);
				$this->Menu_model->save_menu_item($menu_item["id"], array("status"=>$validate_data["data"]["company_search_enabled"] ? 1 : 0));
				$menu_item = $this->Menu_model->get_menu_item_by_gid("agent-main-menu-pro-private-item", $menu["id"]);
				$this->Menu_model->save_menu_item($menu_item["id"], array("status"=>$validate_data["data"]["private_search_enabled"] ? 1 : 0));
				
				if(!$validate_data["data"]["private_search_enabled"] && !$validate_data["data"]["company_search_enabled"] && !$validate_data["data"]["agent_search_enabled"]){
				    $menu = $this->Menu_model->get_menu_by_gid("guest_main_menu");
				    $menu_item = $this->Menu_model->get_menu_item_by_gid("main-menu-pro-item", $menu["id"]);
				    $this->Menu_model->save_menu_item($menu_item["id"], array("status"=>0));
				    
				    $menu = $this->Menu_model->get_menu_by_gid("private_main_menu");
				    $menu_item = $this->Menu_model->get_menu_item_by_gid("private-main-menu-pro-item", $menu["id"]);
				    $this->Menu_model->save_menu_item($menu_item["id"], array("status"=>0));
				    
				    $menu = $this->Menu_model->get_menu_by_gid("company_main_menu");
				    $menu_item = $this->Menu_model->get_menu_item_by_gid("company-main-menu-pro-item", $menu["id"]);
				    $this->Menu_model->save_menu_item($menu_item["id"], array("status"=>0));
				    
				    $menu = $this->Menu_model->get_menu_by_gid("agent_main_menu");
				    $menu_item = $this->Menu_model->get_menu_item_by_gid("agent-main-menu-pro-item", $menu["id"]);
				    $this->Menu_model->save_menu_item($menu_item["id"], array("status"=>0));
				}
				else
				{
				    $menu = $this->Menu_model->get_menu_by_gid("guest_main_menu");
				    $menu_item = $this->Menu_model->get_menu_item_by_gid("main-menu-pro-item", $menu["id"]);
				    $this->Menu_model->save_menu_item($menu_item["id"], array("status"=>1));
				    
				    $menu = $this->Menu_model->get_menu_by_gid("private_main_menu");
				    $menu_item = $this->Menu_model->get_menu_item_by_gid("private-main-menu-pro-item", $menu["id"]);
				    $this->Menu_model->save_menu_item($menu_item["id"], array("status"=>1));
				    
				    $menu = $this->Menu_model->get_menu_by_gid("company_main_menu");
				    $menu_item = $this->Menu_model->get_menu_item_by_gid("company-main-menu-pro-item", $menu["id"]);
				    $this->Menu_model->save_menu_item($menu_item["id"], array("status"=>1));
				    
				    $menu = $this->Menu_model->get_menu_by_gid("agent_main_menu");
				    $menu_item = $this->Menu_model->get_menu_item_by_gid("agent-main-menu-pro-item", $menu["id"]);
				    $this->Menu_model->save_menu_item($menu_item["id"], array("status"=>1));
				}
				
				// -----------------------
				
				$this->system_messages->add_message("success", l("success_settings_saved", "users"));
			}
		}

		$this->Menu_model->set_menu_active_item("admin_menu", "users_menu_item");
		$this->Menu_model->set_menu_active_item("admin_users_type_menu", "users_type_settings_item");
		
		$user_types = $this->Users_model->get_user_types();
		foreach($user_types as $type){
			$data[$type]["login_enabled"]	     = $this->pg_module->get_module_config("users", $type."_login_enabled") ? 1 : 0;
			$data[$type]["register_enabled"]     = $this->pg_module->get_module_config("users", $type."_register_enabled") ? 1: 0;
			$data[$type]["register_mail_confirm"]= $this->pg_module->get_module_config("users", $type."_register_mail_confirm") ? 1: 0;
			$data[$type]["search_enabled"]	     = $this->pg_module->get_module_config("users", $type."_search_enabled") ? 1: 0;
		}
		
		$this->template_lite->assign("data", $data);		
		$this->system_messages->set_data("header", l("admin_header_users_list", "users"));
		
		$this->template_lite->view("types_settings");
	}
	
	/**
	 * Render users settings
	 */
	public function settings(){
		$this->deactivated_settings();
	}
	
	/**
	 * Render deactivated settings
	 */
	public function deactivated_settings(){
		$this->Menu_model->set_menu_active_item("admin_menu", "system-items");
		$this->Menu_model->set_menu_active_item("admin_users_settings_menu", "users_deactivated_settings_item");
		
		if($this->input->post("btn_save")){
			$this->load->model("users/models/Users_deactivated_alert_model");
			
			$post_data["deactivated_send_mail"] = $this->input->post("deactivated_send_mail") ? 1 : 0;
			$post_data["deactivated_send_alert"] = $this->input->post("deactivated_send_alert") ? 1: 0;
			$post_data["deactivated_email"] = $this->input->post("deactivated_email");

			$validate_data = $this->Users_deactivated_alert_model->validate_settings($post_data);
			if(!empty($validate_data["errors"])){
				$this->system_messages->add_message("error", $validate_data["errors"]);
				$data = $validate_data["data"];
			}else{
				foreach($validate_data["data"] as $setting=>$value){
					$this->pg_module->set_module_config("users", $setting, $value);
				}
				$this->system_messages->add_message("success", l("success_settings_saved", "users"));
				$data = $validate_data["data"];
			}
		}else{
			$data["deactivated_send_mail"] = $this->pg_module->get_module_config("users", "deactivated_send_mail");
			$data["deactivated_send_alert"] = $this->pg_module->get_module_config("users", "deactivated_send_alert");
			$data["deactivated_email"] = $this->pg_module->get_module_config("users", "deactivated_email");
		}
		
		$this->template_lite->assign("data", $data);		
		$this->system_messages->set_data("header", l("admin_header_deactivated_alerts_list", "users"));
		
		$this->template_lite->view("deactivated_settings");
	}
	
	/**
	 * Render deactivated alerts list page
	 * @param string $order
	 * @param string $order_direction
	 * @param integer $page
	 */
	public function deactivated_alerts($order=null, $order_direction=null, $page=null){
		$this->Menu_model->set_menu_active_item("admin_menu", "feedbacks-items");
		$this->Menu_model->set_menu_active_item("admin_users_settings_menu", "deactivated_alerts_item");
		
		$this->load->model("users/models/Users_deactivated_alert_model");
		
		$current_settings = $_SESSION["users_deactivated_alerts_list"];
		if(!isset($current_settings["order"])) $current_settings["order"] = "date_add";
		if(!isset($current_settings["order_direction"])) $current_settings["order_direction"] = "DESC";
		if(!isset($current_settings["page"])) $current_settings["page"] = 1;

		if(!$order) $order = $current_settings["order"];
		if(!$order_direction) $order_direction = $current_settings["order_direction"];
		if(!$page) $page = $current_settings["page"];
		
		$current_settings["page"] = $page;		
			
		if(!$order) $order = $current_settings["order"];
		if($order == "reason") $order = "id_reason";
		$this->template_lite->assign("order", $order);
		$current_settings["order"] = $order;


		if(!$order_direction) $order_direction = $current_settings["order_direction"];
		$this->template_lite->assign("order_direction", $order_direction);
		$current_settings["order_direction"] = $order_direction;

		$alerts_count = $this->Users_deactivated_alert_model->get_alerts_count();
		
		if(!$page) $page = $current_settings["page"];
		$items_on_page = $this->pg_module->get_module_config("start", "admin_items_per_page");
			
		$this->load->helper("sort_order");
		$page = get_exists_page_number($page, $alerts_count, $items_on_page);
		$current_settings["page"] = $page;

		$_SESSION["users_deactivated_alert_list"] = $current_settings;

		$sort_links = array(
			"name" => site_url()."admin/users/deactivated_alerts/name/".(($order != "name" xor $order_direction == "DESC") ? "ASC" : "DESC"),
			"email" => site_url()."admin/users/deactivated_alerts/email/".(($order != "email" xor $order_direction == "DESC") ? "ASC" : "DESC"),
			"id_reason" => site_url()."admin/users/deactivated_alerts/reason/".(($order != "id_reason" xor $order_direction == "DESC") ? "ASC" : "DESC"),
			"date_add" => site_url()."admin/users/deactivated_alerts/date_add/".(($order != "date_add" xor $order_direction == "DESC") ? "ASC" : "DESC"),
		);
		
		$this->template_lite->assign("sort_links", $sort_links);

		if($alerts_count > 0){
			$this->Users_deactivated_alert_model->set_format_settings(array("get_reason"=>true));
			$alerts = $this->Users_deactivated_alert_model->get_alerts_list($page, $items_on_page, array($order => $order_direction));
			$this->template_lite->assign("alerts", $alerts);
		}
		$this->load->helper("navigation");
		$this->config->load("date_formats", TRUE);
		$url = site_url()."admin/users/deactivated_alerts/".$order."/".$order_direction."/";
		$page_data = get_admin_pages_data($url, $alerts_count, $items_on_page, $page, "briefPage");
		$page_data["date_format"] = $this->config->item("st_format_date_time_literal", "date_formats");
		$this->template_lite->assign("page_data", $page_data);
		
		$this->system_messages->set_data("header", l("admin_header_deactivated_alerts_list", "users"));
		$this->template_lite->view("deactivated_alerts_list");
	}

	/**
	 * Render view deactivated alert page
	 * @param integer $id
	 * @return void
	 */
	public function deactivated_alerts_show($id){
		$this->Menu_model->set_menu_active_item("admin_menu", "feedbacks-items");
		
		$this->load->model("users/models/Users_deactivated_alert_model");
		$this->Users_deactivated_alert_model->set_format_settings(array("get_reason"=>true));
		$data = $this->Users_deactivated_alert_model->get_alert_by_id($id, true);
		$this->template_lite->assign("data", $data);
		
		$this->Users_deactivated_alert_model->mark_as_read($id);
		
		$this->load->helper("navigation");
		$this->config->load("date_formats", TRUE);
		$date_format = $this->config->item("st_format_date_time_literal", "date_formats");
		$this->template_lite->assign("date_format", $date_format);
		$this->system_messages->set_data("header", l("admin_header_deactivated_alerts_list", "users"));
		$this->template_lite->view("deactivated_alert_view");
	}

	/**
	 * Remove deactivated alert action
	 * @param string $action
	 * @param integer $idd
	 */
	public function deactivated_alerts_delete($ids=null){
		$errors = false;
		$messages = array();
		if(!$ids) $ids = $this->input->post("ids");
		if(!empty($ids)){
			$this->load->model("users/models/Users_deactivated_alert_model");
			foreach((array)$ids as $id){
				$error = $this->Users_deactivated_alert_model->delete_alert($id);
				if($error){
					$errors = true;
					$messages[] = $error;
				}else{
					$messages[] = l("success_deleted_deactivated_alert", "users");
				}
			}
			if($errors){
				$this->system_messages->add_message("error", implode("<br>", $messages));				
			}else{
				$this->system_messages->add_message("success", implode("<br>", $messages));
			}
		}
		
		$url = site_url()."admin/users/deactivated_alerts";
		redirect($url);
	}
	
	/**
	 * Render deactivated reasons settings
	 * @param integer $lang_id
	 */
	public function deactivated_reasons($lang_id=null){
		$this->Menu_model->set_menu_active_item("admin_menu", "system-items");
		$this->Menu_model->set_menu_active_item("admin_users_settings_menu", "users_deactivated_reasons_item");
		
		$this->load->model("users/models/Users_deactivated_reason_model");
		
		if(!$lang_id || !array_key_exists($lang_id, $this->pg_language->languages)){
			$lang_id = $this->pg_language->current_lang_id;
		}

		$reference = $this->pg_language->ds->get_reference($this->Users_deactivated_reason_model->module_gid, $this->Users_deactivated_reason_model->content[0], $lang_id);
		
		$this->system_messages->set_data("header", l("admin_header_deactivated_reasons", "users"));

		$this->template_lite->assign("langs", $this->pg_language->languages);
		$this->template_lite->assign("current_lang_id", $lang_id);
		$this->template_lite->assign("reference", $reference);
		$this->template_lite->assign("module_gid", $this->Users_deactivated_reason_model->module_gid);
		
		$this->system_messages->set_data("header", l("admin_header_deactivated_alerts_list", "users"));
		$this->template_lite->view("deactivated_reasons");
	}
	
	/**
	 * Render deactivated reason edit page
	 * @param integer $lang_id
	 * @param string $option_gid
	 */
	public function deactivated_reason_edit($lang_id, $option_gid=null){
		$this->load->model("users/models/Users_deactivated_reason_model");
	
		if(!$lang_id || !array_key_exists($lang_id, $this->pg_language->languages)) {
			$lang_id = $this->pg_language->current_lang_id;
		}

		$reference = $this->pg_language->ds->get_reference($this->Users_deactivated_reason_model->module_gid, $this->Users_deactivated_reason_model->content[0], $lang_id);
		if($option_gid){
			$add_flag = false;
			foreach($this->pg_language->languages as $lid => $lang){
				$r = $this->pg_language->ds->get_reference($this->Users_deactivated_reason_model->module_gid, $this->Users_deactivated_reason_model->content[0], $lid);
				$lang_data[$lid] = $r["option"][$option_gid];
			}
		}else{
			$option_gid = "";
			$lang_data = array();
			$add_flag = true;
		}

		if($this->input->post("btn_save")){
			$lang_data = $this->input->post("lang_data", true);

			$validate_data = $this->Users_deactivated_reason_model->validate_reason($option_gid, $lang_data);
			if(!empty($validate_data["errors"])){
				$this->system_messages->add_message('error', $validate_data["errors"]);
			}else{
				$this->Users_deactivated_reason_model->save_reason($option_gid, $validate_data["langs"]);
				if($add_flag){
					$this->system_messages->add_message("success", l("success_reason_created", "users"));
				}else{
					$this->system_messages->add_message("success", l("success_reason_updated", "users"));
				}
				$url = site_url()."admin/users/deactivated_reasons/".$lang_id;
				redirect($url);
			}
		}

		$this->template_lite->assign("lang_data", $lang_data);
		$this->template_lite->assign("langs", $this->pg_language->languages);
		$this->template_lite->assign("current_lang_id", $lang_id);
		$this->template_lite->assign("current_module_gid", $this->Users_deactivated_reason_model->module_gid);
		$this->template_lite->assign("option_gid", $option_gid);
		$this->template_lite->assign("add_flag", $add_flag);

		$this->system_messages->set_data("header", l("admin_header_deactivated_alerts_list", "users"));
		$this->Menu_model->set_menu_active_item("admin_users_settings_menu", "users_deactivated_reasons_item");
		$this->Menu_model->set_menu_active_item("admin_menu", "system-items");
		$this->template_lite->view("deactivated_reasons_edit");
	}
	
	/**
	 * Save sorting order of reasons options list
	 */
	public function ajax_deactivated_reason_save_sorter(){
		$this->load->model("users/models/Users_deactivated_reason_model");
		
		$sorter = $this->input->post("sorter");
		foreach($sorter as $parent_str => $items_data){
			foreach($items_data as $item_str =>$sort_index){
				$option_gid = str_replace("item_", "", $item_str);
				$sorter_data[$sort_index] = $option_gid;
			}
		}

		if(empty($sorter_data)) return;
		ksort($sorter_data);
		$this->pg_language->ds->set_reference_sorter($this->Users_deactivated_reason_model->module_gid, $this->Users_deactivated_reason_model->content[0], $sorter_data);
		return;
	}
	
	/**
	 * Remove reason from options list
	 * @param string $option_id
	 */
	public function ajax_deactivated_reason_delete($option_gid){
		$this->load->model("users/models/Users_deactivated_reason_model");
		
		if($option_gid){
			foreach($this->pg_language->languages as $lid => $lang){
				$reference = $this->pg_language->ds->get_reference($this->Users_deactivated_reason_model->module_gid, $this->Users_deactivated_reason_model->content[0], $lid);
				if(isset($reference["option"][$option_gid])){
					unset($reference["option"][$option_gid]);
					$this->pg_language->ds->set_module_reference($this->Users_deactivated_reason_model->module_gid, $this->Users_deactivated_reason_model->content[0], $reference, $lid);
				}
			}
			$this->system_messages->add_message("success", l("success_reason_deleted", "users"));
		}
		return;
	}
	
	/**
	 * Process request selected agents action
	 * @param integer $company_id
	 * @param boolean $approve
	 * @param integer $agent_id	 
	 */
	public function agents_request($company_id, $approve=1, $agent_id=null){
		$errors = false;
		$messages = array();
		if(!$agent_id){
			$agent_id = $this->input->post("ids");
		}		
		if(!empty($agent_id)){
			$action = $approve ? "approve" : "decline";
			foreach((array)$agent_id as $i){
				$error = $this->Users_model->{$approve."_agent"}($company_id, $i);
				if($error){
					$errors = true;
					$messages[] = $error;
				}else{
					$messages[] = l("success_".$action."_agent", "users");
				}
			}
			if($errors){
				$this->system_messages->add_message("error", implode("<br>", $messages));				
			}else{
				$this->system_messages->add_message("success", implode("<br>", $messages));
			}
		}
		$current_settings = $_SESSION["users_agents_list"];
		$url = site_url()."admin/users/agents/".$company_id;
		if($current_settings["order"]){
			$url .= "/".$current_settings["order"];
			if($current_settings["order_direction"]){
				$url .= "/".$current_settings["order_direction"];
				if($current_settings["page"]){
					$url .= "/".$current_settings["page"];
				}
			}
		}
		redirect($url);
	}
	
	/**
	 * Remove selected agents action
	 * @param integer $company_id
	 * @param integer $id
	 */
	public function agents_delete($company_id, $agent_id=null){
		$errors = false;
		$messages = array();
		if(!$agent_id){
			$agent_id = $this->input->post("ids");
		}		
		if(!empty($agent_id)){
			foreach((array)$agent_id as $i){
				$error = $this->Users_model->delete_agent($company_id, $i);
				if($error){
					$errors = true;
					$messages[] = $error;
				}else{
					$messages[] = l("success_deleted_agent", "users");
				}
			}
			if($errors){
				$this->system_messages->add_message("error", implode("<br>", $messages));				
			}else{
				$this->system_messages->add_message("success", implode("<br>", $messages));
			}
		}
		$current_settings = $_SESSION["users_agents_list"];
		$url = site_url()."admin/users/agents/".$company_id;
		if($current_settings["order"]){
			$url .= "/".$current_settings["order"];
			if($current_settings["order_direction"]){
				$url .= "/".$current_settings["order_direction"];
				if($current_settings["page"]){
					$url .= "/".$current_settings["page"];
				}
			}
		}
		redirect($url);
	}
	
	/**
	 * Get out from company
	 * @param integer $user_id user identifier
	 */
	public function agent_get_out($user_id){
		$user = $this->Users_model->get_user_by_id($user_id);
		$error = $this->Users_model->delete_agent($user["agent_company"], $user_id);
		if($error){
			$this->system_messages->add_message("error", $error);	
		}elseif($user["agent_status"]){
			$this->system_messages->add_message("success", l("success_agent_get_out_by_admin", "users"));
		}else{	
			$this->system_messages->add_message("success", l("success_agent_cancel_request_by_admin", "users"));
		}			
		$url = site_url()."admin/users/edit/".$user_id;
		redirect($url);
	}
	
	/**
	 * Set map type for user
	 */
	public function ajax_set_map($user_id, $param){
		$user = $this->Users_model->get_user_by_id($user_id);
		if(!$user) return;
		
		$this->load->model('geomap/models/Geomap_model');
		$this->load->model('geomap/models/Geomap_settings_model');
		
		switch($param){
			case 'type': $settings['view_type'] = $this->input->post('type', true); break;
			case 'zoom': $settings['zoom'] = $this->input->post('zoom', true); break;
			case 'coordinates':
				$settings['lat'] = $this->input->post('lat', true);
				$settings['lon'] = $this->input->post('lon', true);
			break;
		}
		
		if(!$settings) return;
		
		$validate_data = $this->Geomap_settings_model->validate_settings($settings);
		if(empty($validate_data["errors"])){
			$map_gid = $this->Geomap_model->get_default_driver_gid();
			$this->Geomap_settings_model->save_settings($map_gid, 0, $user_id, 'user_profile', $validate_data["data"]);
		}

		if($param == 'coordinates'){
			$data['lat'] = $this->input->post('lat', true);
			$data['lon'] = $this->input->post('lon', true);
			$this->Users_model->save_user($user_id, $data);
		}
		return;
	}
	
	/**
	 * Set user coordinates after geocode
	 * @param integer $user_id user identifier
	 */
	public function ajax_set_geocode_coordinates($user_id){
		$user = $this->Users_model->get_user_by_id($user_id);
		
		if(!$user) return;
		
		if((float)$user['lat'] > 0 || (float)$user['lot'] > 0) return;
		
		$data['lat'] = $this->input->post('lat');
		$data['lon'] = $this->input->post('lon');
	
		$validate_data = $this->Users_model->validate_user($data);
		if(!empty($validate_data["errors"])){
			echo json_encode(array('error' => implode('<br>', $validate_data["errors"])));
			exit;
		}else{
			$this->Users_model->save_user($user_id, $validate_data['data']);
		}
	}
	#MOD FOR NEWS CMS#
	public function news_privilege($id, $news_privilege=0){
		$this->load->model("users/models/users_model");
		$id = intval($id);
		if(!empty($id)){
			$data["news_privilege"] = intval($news_privilege);
			$this->users_model->save_user($id, $data);
			$this->system_messages->add_message('success', ($status)?l('news_privilege_granted', 'users'):l('news_privilege_revoked', 'users'));
		}
		$cur_set = $_SESSION["users_list"];
		$url = site_url()."admin/users/index/".$cur_set["user_type"]."/".$cur_set["filter"]."/".$cur_set["order"]."/".$cur_set["order_direction"]."/".$cur_set["page"]."";
		redirect($url);
	}
	#END OF MOD#
	
}
