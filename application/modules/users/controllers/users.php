<?php
/**
* Users user side controller
*
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/

Class Users extends Controller{

	/**
	 * Constructor
	 */
	public function __construct(){
		parent::Controller();
		$this->load->model("Users_model");
		$this->load->model("Menu_model");
	}

	/**
	 * Return login form by ajax action
	 */
	public function ajax_login_form(){
		$this->load->helper('users');
		if(!is_login_enabled()) return "";
		$user_types = array();
		foreach($this->Users_model->type_arr as $user_type){
			if($this->pg_module->get_module_config("users", $user_type."_register_enabled")){
				$user_types[] = $user_type;
			}
		}
		$this->template_lite->assign("user_types", $user_types);
		$this->template_lite->view("ajax_login_form");
	}

	/**
	 * Render login form action
	 */
	public function login_form(){
		if($this->session->userdata("auth_type") == "user"){
			redirect();
		}
		$this->load->helper('users');
		if(!is_login_enabled()) return "";

		$this->Menu_model->breadcrumbs_set_active(l("header_login", "users"));
		$this->template_lite->view("login_form");
	}

	/**
	 * Login action
	 */
	public function login(){
		$errors = array();
		$this->load->model("users/models/Auth_model");

		$data = array(
			"email" => trim(strip_tags($this->input->post("email", true))),
			"password" => trim(strip_tags($this->input->post("password", true))),
			//"user_open_id" => trim(strip_tags($this->input->post("user_open_id", true))), // Don't delete (openid)
		);
		
		$validate = $this->Auth_model->validate_login_data($data);
		if(!empty($validate["errors"])){
			$errors = $validate["errors"];
		}
		// Don't delete (openid)
		/*elseif(isset($validate["data"]["user_open_id"])){
			$open_id_return = $this->Auth_model->open_id_request($validate["data"]["user_open_id"]);
			if($open_id_return !== true){
				$errors = $open_id_return["errors"];
			}else{
				return;
			}
		}*/else{
			$login_return = $this->Auth_model->login_by_email_password($validate["data"]["email"], $this->Users_model->encode_password($validate["data"]["password"]));
			if(!empty($login_return["errors"])){
				$errors = $login_return["errors"];
			}
		}

		if(!empty($errors)){
			foreach ($errors as $error) $this->system_messages->add_message("error", $error);
			redirect(site_url());
		}

		if(strpos($_SERVER["HTTP_REFERER"], site_url()) !== false && !strstr(str_replace(site_url(), '', $_SERVER["HTTP_REFERER"]), 'start/error')){
			redirect($_SERVER["HTTP_REFERER"]);
		}else{
			redirect(site_url()."start/homepage");
		}
	}

	/**
	 * Logout action
	 */ 
	public function logout(){
		$this->load->model("users/models/Auth_model");
		$this->Auth_model->logoff();
		redirect();
	}

	/**
	 * Change language action
	 * @param integer $lang_id
	 */
	public function change_language($lang_id){
		$lang_id = intval($lang_id);
		
		$old_code = $this->pg_language->languages[$this->pg_language->current_lang_id]["code"];
		$this->pg_language->current_lang_id = $lang_id;
		$code = $this->pg_language->languages[$lang_id]["code"];
		$_SERVER["HTTP_REFERER"] = str_replace("/".$old_code."/", "/".$code."/", $_SERVER["HTTP_REFERER"]);
		$site_url = str_replace("/".$code."/", "", site_url());
		
		$this->session->set_userdata("lang_id", $lang_id);

		if($this->session->userdata("auth_type") == "user"){
			$user_id = $this->session->userdata("user_id");
			$save_data["lang_id"] = $lang_id;
			$this->Users_model->save_user($user_id, $save_data);
		}
		
		if(strpos($_SERVER["HTTP_REFERER"], site_url()) !== false){
			redirect($_SERVER["HTTP_REFERER"]);
		}else{
			redirect();
		}
	}

	/**
	 * Render account action
	 */
	public function account(){
		if($this->session->userdata("auth_type") != "user"){show_404();return;}
		$user_id = $this->session->userdata("user_id");

		$this->load->model("users/models/Auth_model");
		$this->Auth_model->update_user_session_data($user_id);

		$data = $this->Users_model->get_user_by_id($user_id);
		$this->pg_seo->set_seo_data($data);

		/// breadcrumbs
		$user_type = $this->session->userdata("user_type");
		$this->Menu_model->breadcrumbs_set_parent($user_type."_my_account_balance_item");

		$this->template_lite->assign("user_id", $user_id);
		$this->template_lite->assign("data", $data);
		$this->template_lite->view("account");
	}
	
	public function account_services(){
		if($this->session->userdata("auth_type") != "user"){show_404();return;}
		$user_id = $this->session->userdata("user_id");

		/// breadcrumbs
		$user_type = $this->session->userdata("user_type");
		$this->Menu_model->breadcrumbs_set_parent($user_type."_my_profile_services_item");

		$this->template_lite->assign("user_id", $user_id);
		$this->template_lite->view("account_services");
	}

	/**
	 * Don't delete (openid)
	 * OpenID response
	 */
	/*function openid_response(){
		$this->load->model("users/models/Auth_model");
		$open_id_return = $this->Auth_model->open_id_response();
		if(!empty($open_id_return["errors"])){
			foreach($open_id_return["errors"] as $error){
				$this->system_messages->add_message('error', $error);
			}
		}
		redirect();
	}*/

	/**
	 * Render profile of user action
	 */
	public function profile($action="view"){
		$user_id = $this->session->userdata("user_id");
		
		$this->Users_model->set_format_settings(array('get_safe'=>false, 'get_company'=>true));
		$data = $this->Users_model->get_user_by_id($user_id);
		$this->Users_model->set_format_settings(array('get_safe'=>true, 'get_company'=>false));
		
		$user_type = $data['user_type'];

		if($this->input->post("btn_register")){
			$post_data = $this->input->post("data", true);
			$post_data['user_type'] = $user_type;
		
			$validate_data = $this->Users_model->validate_user($user_id, $post_data, 'user_icon');
			if(!empty($validate_data["errors"])){
				$this->system_messages->add_message("error", $validate_data["errors"]);
			}else{
				switch($action){
					case 'subscriptions':
						if($this->pg_module->is_module_installed("subscriptions")){
							// Save user subscribers
							$user_subscriptions_list = $this->input->post("user_subscriptions_list", true);
							$this->load->model("subscriptions/models/Subscriptions_users_model");
							$this->Subscriptions_users_model->save_user_subscriptions($user_id, $user_subscriptions_list);
						}
					break;
					case 'company':
						$save_company_status = $this->Users_model->save_agent_company($user_id, $validate_data['data']["agent_company"]);
						if($save_company_status) $this->system_messages->add_message("success", l("success_update_agent_company", "users"));
					break;
					case 'location':
						$this->Users_model->save_user($user_id, $validate_data["data"], 'user_icon', true);
						
						$this->load->model('geomap/models/Geomap_model');
						$this->load->model('geomap/models/Geomap_settings_model');
						$map_gid = $this->Geomap_model->get_default_driver_gid();
						$post_settings = $this->input->post('map', true);
						$post_settings['lat'] = $validate_data['data']['lat'];
						$post_settings['lon'] = $validate_data['data']['lon'];
						$validate_data = $this->Geomap_settings_model->validate_settings($post_settings);
						if(!empty($validate_data["errors"])) break;
						$this->Geomap_settings_model->save_settings($map_gid, 0, $user_id, 'user_profile', $validate_data["data"]);
						
						$this->system_messages->add_message("success", l("success_update_user", "users"));
					break;
					default:
						if($this->input->post("user_icon_delete")){
							$this->Users_model->delete_logo($user_id);
						}
						$this->Users_model->save_user($user_id, $validate_data["data"], 'user_icon', true);
						$user = $this->Users_model->get_user_by_id($user_id);
						$this->session->set_userdata('logo', $user['media'][!empty($user_data['user_logo_moderation'])?'user_logo_moderation':'user_logo']['thumbs']['small']);
						$this->system_messages->add_message("success", l("success_update_user", "users"));
					break;
				}

				$this->load->model("users/models/Auth_model");
				$this->Auth_model->update_user_session_data($user_id);

				redirect(site_url()."users/profile");
			}
		}

		if(!$action){
			$action = "view";
		}
		$this->template_lite->assign("action", $action);

		//$data["user_type"] = l($user_type, "users");
		$this->pg_seo->set_seo_data($data);

		if($user_type == "company"){
			$markers = array(
				array( 
					"country" => $data["country"], 
					"region" => $data["region"], 
					"city" => $data["city"], 
					"address" => $data["address"], 
					"postal_code" => $data["postal_code"], 
					"lat" => (float)$data["lat"], 
					"lon" => (float)$data["lon"], 
					"info" => ($data['location'] ? $data['location'] : $data["output_name"].', '.l($data['user_type'], 'users')),
					"dragging" => ($action == "location")?true:false,
				),
			);

			$current_language = $this->pg_language->get_lang_by_id($this->pg_language->current_lang_id);
			$view_settings = array(
				"width" => ($action == "location" ? "660" : "565"),
				"height" => "250", 
				"lang" => $current_language["code"],
				"disable_smart_zoom" => 1,
			);
			if($action == "location"){
				$view_settings['zoom_listener'] = 'get_user_zoom_data'; 
				$view_settings['type_listener'] = 'get_user_type_data'; 
				$view_settings['drag_listener'] = 'get_user_drag_data'; 
				
				$this->load->model('geomap/models/Geomap_model');
				$this->load->model('geomap/models/Geomap_settings_model');
				
				$map_gid = $this->Geomap_model->get_default_driver_gid();
				$map_settings = $this->Geomap_settings_model->get_settings($map_gid, 0, $user_id, 'user_profile');
				$this->template_lite->assign('user_map_settings', $map_settings);
			}
			$this->template_lite->assign("map_settings", $view_settings);
			$this->template_lite->assign("markers", $markers);
		}
		
		/// breadcrumbs
		$this->Menu_model->breadcrumbs_set_parent($user_type."_my_profile_item");

		$this->config->load("date_formats", true);
		$page_data["date_format"] = $this->config->item("st_format_date_literal", "date_formats");

		$use_phone_format = $this->pg_module->get_module_config("start", "use_phone_format");
		if($use_phone_format){
			$phone_format = $this->pg_module->get_module_config("start", "phone_format");
			$this->template_lite->assign('phone_format', $phone_format);
		}
		
		$weekdays = $this->pg_language->ds->get_reference("start", "weekday-names");
		$this->template_lite->assign("weekdays", $weekdays);
		
		$dayhours = $this->pg_language->ds->get_reference("start", "dayhour-names");
		$this->template_lite->assign("dayhours", $dayhours);
		
		$this->template_lite->assign("page_data", $page_data);
		$this->template_lite->assign("data", $data);
		$this->template_lite->assign("user_type", $user_type);
		$this->template_lite->assign("user_id", $user_id);
		$this->template_lite->assign("edit_mode", 1);
		$this->template_lite->view("profile");
	}

	public function change_reg_data(){
		$user_id = $this->session->userdata("user_id");
		$this->Users_model->set_format_settings('get_safe', false);
		$data = $this->Users_model->get_user_by_id($user_id);
		$this->Users_model->set_format_settings('get_safe', true);
		
		if($this->input->post("btn_passw_save")){
			$post_data["password_old"] = $this->input->post("password_old", true);
			$post_data["password"] = $this->input->post("password", true);
			$post_data["repassword"] = $this->input->post("repassword", true);

			$validate_data = $this->Users_model->validate_user($user_id, $post_data);
			if(!empty($validate_data["errors"])){
				$this->system_messages->add_message("error", $validate_data["errors"]);
			}else{
				$this->Users_model->save_user($user_id, $validate_data["data"]);

				///// send notification
				$this->load->model("Notifications_model");
				$user_data = $this->Users_model->get_user_by_id($user_id);
				$user_data["password"] = $post_data["password"];
				$return = $this->Notifications_model->send_notification($user_data["email"], "users_change_password", $user_data);

				$this->system_messages->add_message("success", l("success_user_updated", "users"));
				redirect(site_url()."users/change_reg_data");
			}
		}

		if($this->input->post("btn_email_save")){
			$post_data["email"] = $this->input->post("email", true);
			
			$validate_data = $this->Users_model->validate_user($user_id, $post_data);
			if(!empty($validate_data["errors"])){
				$this->system_messages->add_message("error", $validate_data["errors"]);
			}elseif($post_data["email"] == $data['email']){
				$this->system_messages->add_message("error", l('error_emails_are_equal', 'users'));
			}else{
				if($data['email']){
					$validate_data["data"]["email_restore"] = $data['email'];
					$validate_data["data"]["email_restore_code"] = substr($this->Users_model->encode_confirm(date("Y-m-d H:i:s") . $data["email"]), 0, 10);
				}

				$this->Users_model->save_user($user_id, $validate_data["data"]);

				$this->load->model("users/models/Auth_model");
				$this->Auth_model->update_user_session_data($user_id);

				///// send notification
				$this->load->model("Notifications_model");
				$user_data = $this->Users_model->get_user_by_id($user_id);
				$return = $this->Notifications_model->send_notification($user_data["email"], "users_change_email", $user_data);
				if($data['email']){
					$user_data['restore_link'] = site_url()."users/email_restore/".$validate_data["data"]["email_restore_code"];
					$return = $this->Notifications_model->send_notification($data["email"], "users_email_restore", $user_data);
				}

				$this->system_messages->add_message("success", l("success_user_updated", "users"));
				redirect(site_url()."users/change_reg_data");
			}
		}
		
		if($this->input->post("btn_phone_save")){
			$post_data["phone"] = $this->input->post("phone", true);
			
			$validate_data = $this->Users_model->validate_user($user_id, $post_data);
			if(!empty($validate_data["errors"])){
				$this->system_messages->add_message("error", $validate_data["errors"]);
			}elseif($post_data["phone"] == $data['phone']){
				$this->system_messages->add_message("error", l('error_phones_are_equal', 'users'));
			}else{
				if($data['phone']){
					$validate_data["data"]["phone_restore"] = $data['phone'];
					$validate_data["data"]["phone_restore_code"] = substr($this->Users_model->encode_confirm(date("Y-m-d H:i:s") . $data["phone"]), 0, 10);
				}

				$this->Users_model->save_user($user_id, $validate_data["data"]);

				$this->load->model("users/models/Auth_model");
				$this->Auth_model->update_user_session_data($user_id);

				///// send notification
				$this->load->model("Notifications_model");
				$user_data = $this->Users_model->get_user_by_id($user_id);
				$return = $this->Notifications_model->send_notification($user_data["phone"], "users_change_phone", $user_data);
				if($data['phone']){
					$user_data['restore_link'] = site_url()."users/phone_restore/".$validate_data["data"]["phone_restore_code"];
					$return = $this->Notifications_model->send_notification($data["phone"], "users_phone_restore", $user_data);
				}

				$this->system_messages->add_message("success", l("success_user_updated", "users"));
				redirect(site_url()."users/change_reg_data");
			}
		}
		
		$this->load->helper("start");
		$this->template_lite->assign('phone_format', get_phone_format());

		/// breadcrumbs
		$user_type = $this->session->userdata("user_type");
		$this->Menu_model->breadcrumbs_set_parent($user_type."_my_profile_change_regdata_item");
		
		$this->template_lite->assign("data", $data);
		$this->template_lite->view("change_reg_data");
	} 
	/**
	 * Render change user password action
	 */
	public function change_password(){	
		$user_id = $this->session->userdata("user_id");
		
		
		/// breadcrumbs
		$user_type = $this->session->userdata("user_type");
		$this->Menu_model->breadcrumbs_set_parent($user_type."_my_profile_change_password_item");
		
		$this->template_lite->view("change_password");
	}

	/**
	 * Render change user email action
	 */
	public function change_email(){	
		$user_id = $this->session->userdata("user_id");
		$data = $this->Users_model->get_user_by_id($user_id);
		
		if($this->input->post("btn_save")){
			$post_data["email"] = $this->input->post("email", true);
			
			$validate_data = $this->Users_model->validate_user($user_id, $post_data);
			if(!empty($validate_data["errors"])){
				$this->system_messages->add_message("error", $validate_data["errors"]);
			}elseif($post_data["email"] == $data['email']){
				$this->system_messages->add_message("error", l('error_emails_are_equal', 'users'));
			}else{
				if($data['email']){
					$validate_data["data"]["email_restore"] = $data['email'];
					$validate_data["data"]["email_restore_code"] = substr($this->Users_model->encode_confirm(date("Y-m-d H:i:s") . $data["email"]), 0, 10);
				}

				$this->Users_model->save_user($user_id, $validate_data["data"]);

				$this->load->model("users/models/Auth_model");
				$this->Auth_model->update_user_session_data($user_id);

				///// send notification
				$this->load->model("Notifications_model");
				$user_data = $this->Users_model->get_user_by_id($user_id);
				$return = $this->Notifications_model->send_notification($user_data["email"], "users_change_email", $user_data);
				if($data['email']){
					$user_data['restore_link'] = site_url()."users/email_restore/".$validate_data["data"]["email_restore_code"];
					$return = $this->Notifications_model->send_notification($data["email"], "users_email_restore", $user_data);
				}

				$this->system_messages->add_message("success", l("success_user_updated", "users"));
				redirect(site_url()."users/change_email");
			}
		}
		
		/// breadcrumbs
		$user_type = $this->session->userdata("user_type");
		$this->Menu_model->breadcrumbs_set_parent($user_type."_my_profile_change_email_item");
		
		$this->template_lite->assign("data", $data);
		$this->template_lite->view("change_email");
	}	
	
	/**
	 * Restore user email by code
	 * @param string $code email restore code
	 */
	public function email_restore($code){
		$code = trim(strip_tags($code));
		if($this->session->userdata('auth_type') == 'user') show_404();
		if(empty($code)) return show_404();
		$this->Users_model->set_format_settings('get_safe', false);
		$user = $this->Users_model->get_user_by_email_restore_code($code);
		$this->Users_model->set_format_settings('get_safe', true);
		if(!$user){
			$this->system_messages->add_message("error", l('error_email_restore_empty', 'users'));
		}else{
			$data['email'] = $user['email_restore'];
			$data['email_restore_code'] = '';
			$this->Users_model->save_user($user['id'], $data);
			$this->system_messages->add_message("success", l("success_user_updated", "users"));
		}
		redirect(site_url());
	}
	
	/**
	 * Restore user phone by code
	 * @param string $code phone restore code
	 */
	public function phone_restore($code){
		$code = trim(strip_tags($code));
		if($this->session->userdata('auth_type') == 'user') show_404();
		if(empty($code)) return show_404();
		$this->Users_model->set_format_settings('get_safe', false);
		$user = $this->Users_model->get_user_by_phone_restore_code($code);
		$this->Users_model->set_format_settings('get_safe', true);
		if(!$user){
			$this->system_messages->add_message("error", l('error_phone_restore_empty', 'users'));
		}else{
			$data['phone'] = $user['phone_restore'];
			$data['phone_restore_code'] = '';
			$this->Users_model->save_user($user['id'], $data);
			$this->system_messages->add_message("success", l("success_user_updated", "users"));
		}
		redirect(site_url());
	}
	
	/**
	 * Render deactivate user action
	 */
	public function deactivate_profile(){
		$user_type = $this->session->userdata("user_type");
		
		/// breadcrumbs
		$this->Menu_model->breadcrumbs_set_parent($user_type."_my_profile_deactivate_item");
		
		$user_id = $this->session->userdata("user_id");
		$user = $this->Users_model->get_user_by_id($user_id, true);
		
		$deactivated_send_mail = $this->pg_module->get_module_config("users", "deactivated_send_mail");
		
		if($this->input->post("btn_save")){
			
			$post_data["id_reason"] = $this->input->post("reason_id");
			$post_data["message"] = $this->input->post("message");
			$post_data['fname'] = $user["fname"];
			$post_data['sname'] = $user['sname'];
			$post_data["name"] = $user["output_name"];
			$post_data["email"] = $user["email"];
			$post_data["phone"] = $user["phone"];
			
			$this->load->model('users/models/Users_deactivated_alert_model');
			$validate_data = $this->Users_deactivated_alert_model->validate_alert(null, $post_data);
			if(!empty($validate_data["errors"])){
				$this->system_messages->add_message("error", $validate_data["errors"]);
				$data = $validate_data["data"];
			}else{
				$this->Users_model->delete_user($user_id);
				$deactivated_send_alert = $this->pg_module->get_module_config("users", "deactivated_send_alert");
				if($deactivated_send_alert){
					$this->Users_deactivated_alert_model->save_alert(null, $validate_data["data"]);
				}
				
				$mail_data = $validate_data["data"];
				$this->load->model("notifications/models/Notifications_model");
				$this->Notifications_model->send_notification($mail_data['email'], "users_deactivated", $validate_data["data"]);
				
				$deactivated_send_mail = $this->pg_module->get_module_config("users", "deactivated_send_mail");
				if($deactivated_send_mail){
					$deactivated_email = $this->pg_module->get_module_config("users", "deactivated_email");
					if($deactivated_email){
						$mail_data = $validate_data["data"];
						$this->load->model("notifications/models/Notifications_model");
						$this->Notifications_model->send_notification($deactivated_email, "ausers_deactivated", $validate_data["data"]);
					}
				}
				$this->system_messages->add_message("success", l("success_user_deactivated", "users"));
				$this->logout();
			}

		}else{
			$data = $validate_data["data"];
		}
		
		$this->template_lite->assign("data", $data);
		$this->template_lite->view("deactivate_profile");
	}


	/**
	 * Register as private
	 */
	public function reg_private(){
		$this->register("private");
	}
	
	/**
	 * Register as company
	 */
	public function reg_company(){
		$this->register("company");
	}
	
	/**
	 * Register as agent
	 */
	public function reg_agent(){
		$this->register("agent");
	}

	/**
	 * Register a new user
	 * @param string $user_type
	 */
	private function register($user_type){
		if($this->session->userdata("auth_type") == "user"){
			redirect(site_url()."users/profile");
		}

		if(!$this->pg_module->get_module_config("users", $user_type."_register_enabled")){show_404();return;}	
		
		$data = array();
		
		if ($this->input->post("btn_register")){
			$post_data = $this->input->post("data", true);
			$post_data["user_type"] = $user_type;  # result return as private, agent, company
			$use_email_confirmation = $this->pg_module->get_module_config("users", $user_type."_register_mail_confirm"); # result return as value = 1
			
			$validate_data = $this->Users_model->validate_user(null, $post_data); # return error messages as an array and post registration data

			$license_agreement = $this->input->post("license_agreement", true);
			if(!$license_agreement){
				$validate_data["errors"][] = l("error_license_agreement", "users");
			}
			
			$is_captcha_valid = ($this->input->post("captcha_confirmation", true) == $this->session->userdata("captcha_word")) ? 1 : 0;
			if(!$is_captcha_valid){
				$validate_data["errors"][] = l("error_invalid_captcha", "users");
			}
			
			$data = $validate_data["data"];			
			if(!empty($validate_data["errors"])){
				$this->system_messages->add_message("error", implode("<br />", $validate_data["errors"]));
			}else{
				$data["status"] = 1;
				if($use_email_confirmation){
					$data["confirm"] = 0;
					$data["confirm_code"] = substr($this->Users_model->encode_confirm(date("Y-m-d H:i:s") . $data["email"]), 0, 10);
				}else{
					$data["confirm"] = 1;
					$data["confirm_code"] = "";
				}
				$saved_password = $data["password"];
				$data["lang_id"] = $this->session->userdata("lang_id");
				if(!$data["lang_id"]) $data["lang_id"] = $this->pg_language->get_default_lang_id();

				$user_id = $this->Users_model->save_user(null, $data);		
				
				/*if($this->pg_module->is_module_installed("subscriptions")){
					// Save user subscribers
					$this->load->model("subscriptions/models/Subscriptions_users_model");
					$user_subscriptions_list = $this->input->post('user_subscriptions_list', true);
					$this->Subscriptions_users_model->save_user_subscriptions($user_id, (array)$user_subscriptions_list);
				} */
				
				//// send notification
				$this->load->model("Notifications_model");
				$data["password"] = $post_data["password"];
				if($use_email_confirmation){
					$link = site_url()."users/confirm/".$data["confirm_code"];
					$data["confirm_block"] = str_replace("[link]", $link, l("confirm_block_text", "users"));
				}
				$this->Notifications_model->send_notification($data["email"], "users_registration", $data);

				if(!$use_email_confirmation){
					$this->load->model("users/models/Auth_model");
					$auth_data = $this->Auth_model->login($user_id);
					if(!empty($auth_data["errors"])){
						$this->system_messages->add_message("error", $auth_data["errors"]);
					}
					redirect(site_url().'start/homepage');
				}else{
					$this->system_messages->add_message("info", l("info_please_checkout_mailbox", "users"));
					redirect();
				}
			}
		}

		$this->load->helper("start");
		$this->template_lite->assign('phone_format', get_phone_format());

		$this->load->plugin("captcha");
		$this->config->load("captcha_settings");
		$captcha_settings = $this->config->item("captcha_settings");
		$captcha = create_captcha($captcha_settings);
		$this->session->set_userdata("captcha_word", $captcha["word"]);
		$data["captcha_image"] = $captcha["image"];
		$data["captcha_word_length"] = strlen($captcha["word"]);

		/// breadcrumbs
		$this->Menu_model->breadcrumbs_set_active(l("header_register", "users"));

		$this->template_lite->assign("data", $data);
		$this->template_lite->assign("user_type", $user_type);
		$this->template_lite->view("register");
	}

	/**
	 * Password recovery
	 */
	public function restore(){
		if ($this->session->userdata("auth_type") == "user"){redirect();}

		if($this->input->post("btn_save")){
			$email = strip_tags($this->input->post("email", true));
			$this->Users_model->set_format_settings('gate_safe', false);
			$user_data = $this->Users_model->get_user_by_email($email);
			$this->Users_model->set_format_settings('gate_safe', true);
			$error = "";
			if(empty($user_data) || !$user_data["id"]){
				$this->system_messages->add_message("error", l("error_user_no_exists", "users"));
			}elseif($user_data["status"] == 0){ #modification# validation
				$this->system_messages->add_message("error", l("error_user_is_blocked", "users"));
			}elseif($user_data["confirm"] == 0){ #modification# validation
				$this->system_messages->add_message("error", l("error_unconfirmed_user", "users"));
			}else{
				
				$user_id = $user_data["id"];
				$save_data["password"] = $save_data["repassword"] = $user_data['password'] = substr(md5(date("Y-m-d H:i:s") . $user_data["email"]), 0, 6);
				$validate_data = $this->Users_model->validate_user($user_data["id"], $save_data);
				
				if(!empty($validate_data['errors'])){
					$this->system_messages->add_message("errors", implode('<br>', $validate_data['errors']));
				}else{
					$this->Users_model->save_user($user_data["id"], $validate_data['data']);

					$this->load->model("Notifications_model");
					$return = $this->Notifications_model->send_notification($user_data["email"], "users_fogot_password", $user_data);
					
					$this->system_messages->add_message("success", l("success_restore_mail_sent", "users"));
					redirect(site_url()."users/restore");
				}
			}
		}
		/// breadcrumbs
		$this->Menu_model->breadcrumbs_set_active(l("header_restore_password", "users"));

		$this->template_lite->view("forgot_form");
	}

	/**
	 * Confirm registration
	 * @param string $code
	 */
	public function confirm($code){
		$code = trim(strip_tags($code));
		$this->Users_model->set_format_settings('gate_safe', false);
		$user = $this->Users_model->get_user_by_confirm_code($code);
		$usersss = $this->Users_model->get_user_by_confirm_code($code);
		$this->Users_model->set_format_settings('gate_safe', true);
		if(empty($user)){
			$this->system_messages->add_message("error", l("error_user_no_exists", "users"));
			redirect();
		}elseif($user["confirm"] == 1){ #modification# validation
			$this->system_messages->add_message("error", l("error_user_already_confirm", "users"));
			redirect();
		}else{
			$data["confirm"] = 1;
			$this->Users_model->save_user($user["id"], $data);

			$this->system_messages->add_message("success", l("success_confirm", "users"));

			$this->load->model("users/models/Auth_model");
			$auth_data = $this->Auth_model->login($user["id"]);
			if(!empty($auth_data["errors"])){
				$this->system_messages->add_message("error", $auth_data["errors"]);
				redirect();
			}else{
				redirect(site_url().'start/homepage');
			}
		}
	}

	/**
	 * Get list of users by ajax action
	 * @param integer $page
	 */
	public function ajax_get_users($page=1){
		$selected = $this->input->post("selected", true);
		$user_type = $this->input->post("user_type", true);
		$search_string = trim(strip_tags($this->input->post("search", true)));
		$params = $return = array();
		$items_on_page = 100;

		if(!empty($search_string)){
			$params["where_sql"][] = "(email LIKE '%".$search_string."%' OR fname LIKE '%".$search_string."%' OR sname LIKE '%".$search_string."%' OR company_name LIKE '%".$search_string."%')";
		}
		if(!empty($selected)){
			$params["where_sql"][] = "id NOT IN (".implode($selected).")";
		}

		if($user_type){
			$params["where"]["user_type"] = $user_type;
		}
		$return["all"] = $this->Users_model->get_users_count($params);
		$return["pages"] = ceil($return["all"]/$items_on_page);
		$return["current_page"] = $page;
		$return["items"] = $this->Users_model->get_users_list($page, $items_on_page, array("email" => "asc"), $params);

		echo json_encode($return);
		return;
	}

	/**
	 * Return list of selected users by ajax action
	 */
	public function ajax_get_selected_users(){
		$selected = $this->input->post("selected", true);

		if(!empty($selected)){
			$params["where_in"]["id"] = $selected;
			$return = $this->Users_model->get_users_list(null, null, array("email" => "asc"), $params);
		}else{
			$return = array();
		}
		echo json_encode($return);
		return;
	}

	/**
	 * Return form for filtering users by ajax users
	 * @param integer $max_select
	 * @param string $template
	 */
	public function ajax_get_users_form($max_select=1, $template="default"){
		$selected = $this->input->post("selected", true);

		if(!empty($selected)){
			$data["selected"] = $this->Users_model->get_users_list(null, null, array("unique_name" => "asc"), array(), $selected, true);
		}else{
			$data["selected"] = array();
		}
		$data["max_select"] = $max_select ? $max_select : 0;

		$this->template_lite->assign("select_data", $data);
		$this->template_lite->view("ajax_user_select_form_".$template);
	}

	/**
	 * Activate contact
	 * @param integer $id_contact_user
	 * @param integer $id_user_service
	 */
	public function activate_contact($id_contact_user, $id_user_service){

	}
	
	/**
	 * Return users block
	 * @param string $order
	 * @param string $order_direction
	 * @param integer $page
	 */
	private function _users_block($user_type, $order="date_created", $order_direction="DESC", $page=1){
		$current_settings = isset($_SESSION["users_list"])?$_SESSION["users_list"]:array();
		if(!isset($current_settings["order"]))
			$current_settings["order"] = "date_created";
		if(!isset($current_settings["order_direction"]))
			$current_settings["order_direction"] = "DESC";
		if(!isset($current_settings["page"]))
			$current_settings["page"] = 1;
		if(!isset($current_settings["data"]))
			$current_settings["data"] = array();

		if($current_settings['data']['type'] != $user_type){
			$current_settings['data']['type'] = $user_type;
			$current_settings['page'] = 1;
		}
		$this->template_lite->assign('user_type', $user_type);
	
		$filters = $current_settings['data'];
		$filters['active'] = 1;
		
		if($this->session->userdata("auth_type") == "user")
			$filters['not_user'] = $this->session->userdata("user_id");

		$order = trim(strip_tags($order));
		if(!empty($order)) $current_settings['order'] = $order;
		$this->template_lite->assign('order', $order);
		$order = $current_settings['order'];

		$order_direction = strtoupper(trim(strip_tags($order_direction)));
		if(!empty($order_direction)) $current_settings['order_direction'] = $order_direction;
		$order_direction = $current_settings['order_direction'];
		$this->template_lite->assign('order_direction', $order_direction);
		
		if(!empty($page)) $current_settings['page'] = $page;
		$page = $current_settings['page'];
		
		if($order_direction != 'ASC') $order_direction = 'DESC';
		
		$_SESSION['users_pagination'] = array(
			'order' => $order,
			'order_direction' => $order_direction,
			'page' => $page,
			'data' => $filters,
		);
		
		$view_mode = isset($_SESSION['users_view_mode']) ? $_SESSION['users_view_mode'] : 'list';
		
		$items_on_page = $this->pg_module->get_module_config("start", "index_items_per_page");
		
		if(isset($filters['keyword']) && !empty($filters['keyword'])){
			$method = 'search';
			$keyword = $filters['keyword'];
		}else{
			$method = 'index';
			$keyword = '';
		}	
		
		$this->load->helper('seo');
		$action_data = array('user_type'=>$user_type, 'keyword'=>$keyword);
		$url = rewrite_link('users', $method, $action_data);
		
		$sort_data = array(
			"url" => $url,
			"order" => $order,
			"direction" => $order_direction,
			"links" => array(
				"name" => l("field_name", "users"),				
				"date_created" => l("field_date_created", "users"),
			)
		);
		if($this->pg_module->is_module_installed('reviews')){
			$this->load->helper('reviews');
			$review_link = get_reviews_sorter();
			if(is_array($review_link)) $sort_data['links'] = array_merge($review_link, $sort_data['links']);
		}
		$this->template_lite->assign('sort_data', $sort_data);
		
		$use_map_in_search = true;
		if($user_type == 'company'){
		    if($view_mode == 'list'){
				$use_map_in_search = $this->pg_module->get_module_config('users', 'use_map_in_search');
				$this->template_lite->assign('use_map_in_search', $use_map_in_search);
		    }
		    $this->template_lite->assign('use_map', true);
		}
	
		
		$users_count = $this->Users_model->get_users_count_by_filters($filters);
		if($users_count > 0){
			$order_array = array();
			if($order == 'name'){
				if($user_type == 'company'){
					$order_array = array('company_name' => $order_direction);
				}else{
					$order_array = array("CONCAT(fname, ' ', sname)" => $order_direction);
				}
			}else{
				$order_array = array($order => $order_direction);
			}
		
			if($use_map_in_search) $this->Users_model->set_format_settings('get_contact', true);
			$users = $this->Users_model->get_users_list_by_filters($filters, $page, $items_on_page, $order_array);
			$this->Users_model->set_format_settings('get_contact', false);
			$this->template_lite->assign('users', $users);		
			if($use_map_in_search){
				$markers = array();
				foreach($users as $user){
					if(!$user['is_map'] || !$user['is_contact']) continue;
					$this->template_lite->assign('view_mode', $view_mode);
					$this->template_lite->assign('user', $user);
					$info = $this->template_lite->fetch('user_map_block', 'user', 'users');
					$markers[] = array( 
						'gid' => $user['id'],
						'country' => $user['country'], 
						'region' => $user['region'], 
						'city' => $user['city'], 
						'address' => $user['address'], 
						'postal_code' => $user['postal_code'], 
						'lat' => (float)$user['lat'], 
						'lon' => (float)$user['lon'], 
						'info' => $info,
						'dragging' => false,
					);
				}
				$this->template_lite->assign('markers', $markers);
			}
		}
	
		$current_language = $this->pg_language->get_lang_by_id($this->pg_language->current_lang_id);
		$view_settings = array(
			'lang' => $current_language['code'],
		);			
		$this->template_lite->assign('map_settings', $view_settings);
	
		$this->load->helper('sort_order');
		$page = get_exists_page_number($page, $users_count, $items_on_page);
		$current_settings['page'] = $page;

		$this->load->helper('navigation');
		$this->config->load('date_formats', TRUE);
		$page_data = get_user_pages_data($url.'/'.$order.'/'.$order_direction.'/', $users_count, $items_on_page, $page, 'briefPage');
		$page_data['date_format'] = $this->config->item('st_format_date_time_literal', 'date_formats');
		$this->template_lite->assign("page_data", $page_data);
		
		$_SESSION['users_list'] = $current_settings;
		
		$this->template_lite->assign('view_mode', $view_mode);
		
		return $this->template_lite->fetch('users_block_'.$view_mode, 'user', 'users');
	}
	
	/**
	 * Render index list action
	 * @param string $user_type type of user
	 * @param string $order sorting order
	 * @param string $order_direction order direction
	 * @param integer $page page of results
	 */
	public function index($user_type=null, $order='date_created', $order_direction='DESC', $page=1){
		$this->search($user_type, '', $order, $order_direction, $page);
	}
	
	/**
	 * Render users action by ajax
	 */
	public function ajax_users($user_type=null, $order='date_created', $order_direction='DESC', $page=1){
		echo $this->_users_block($user_type, $order, $order_direction, $page);
	}
	
	/**
	 * Render search results
	 * @param string $filters keyword
	 * @param string $view_mode view mode
	 * @param string $order order field
	 * @param string $order_direction order direction
	 * @param integer $page page of results
	 */
	public function search($user_type=null, $keyword='', $order='date_created', $order_direction='DESC', $page=1){
		$user_types = $user_types_default = $this->Users_model->get_user_types();
		if(empty($user_types)) show_404();
		foreach($user_types as $key=>$value){
			$search_enabled = $this->pg_module->get_module_config("users", $value."_search_enabled");
			if(!$search_enabled){
				unset($user_types[$key]);
			}
		}
		
		$search_disabled = false;
		if(empty($user_types)){
			$search_disabled = true;
			$user_types = $user_types_default;
		}
		$this->template_lite->assign('user_types', $user_types);
		
		if($user_type && in_array($user_type, $user_types)){
			$current_settings = isset($_SESSION['users_list']) ? $_SESSION['users_list'] : array();
			
			$keyword = trim(strip_tags(urldecode($keyword)));
			if($keyword != $current_settings['data']['keyword']){
				$current_settings['order'] = 'date_created';
				$current_settings['order_direction'] = 'DESC';
				$current_settings['page'] = 1;
				$current_settings['data'] = array();
			}
	
			$current_settings['data']['type'] = $user_type;
			$current_settings['data']['keyword'] = $keyword;
			
			$_SESSION['users_list'] = $current_settings;
			
			$this->template_lite->assign('current_user_type', $user_type);
		}elseif(isset($_SESSION['users_list']['data']['type']) && 
				isset($user_types[$_SESSION['users_list']['data']['type']])){
			$user_type = $_SESSION['users_list']['data']['type'];
		}else{
			$user_type = current($user_types);
		}
		$this->template_lite->assign('current_user_type', $user_type);
	
		$user_block = $this->_users_block($user_type, $order, $order_direction, $page);
		$this->template_lite->assign("block", $user_block);
		
		$this->template_lite->assign('current_user_type', $user_type);
		
		if($search_disabled){
			switch($user_type){
				case 'private':
					$this->Menu_model->breadcrumbs_set_active(l('privates', 'users'));
				break;
				case 'company':
					$this->Menu_model->breadcrumbs_set_active(l('companies', 'users'));
				break;
				case 'agent':
					$this->Menu_model->breadcrumbs_set_active(l('agents', 'users'));
				break;
			}
		}else{
			$this->Menu_model->breadcrumbs_set_parent('main-menu-pro-'.$user_type.'-item');
		}
		
		$use_poll_in_search = $this->pg_module->get_module_config('users', 'use_poll_in_search');
		$this->template_lite->assign('use_poll_in_search', $use_poll_in_search);
		
		$current_settings = isset($_SESSION["users_list"])?$_SESSION["users_list"]:array();
		if(!isset($current_settings["order"])) $current_settings["order"] = "date_created";
		$this->template_lite->assign('order', $current_settings["order"]);
		
		if(!isset($current_settings["order_direction"])) $current_settings["order_direction"] = "DESC";
		$this->template_lite->assign('order_direction', $current_settings["order_direction"]);
		
		if(!isset($current_settings["page"])) $current_settings["page"] = 1;
		$this->template_lite->assign('page', $current_settings["page"]);
		
		$this->pg_seo->set_seo_data(array('user_type'=>$user_type, 'user_type_str'=>l($user_type, 'users')));

		$view_mode = isset($_SESSION['users_view_mode']) ? $_SESSION['users_view_mode'] : 'list';
		
		$this->template_lite->view('users_'.$view_mode, 'user', 'users');
	}
	
	/**
	 * Render users action by ajax
	 */
	public function ajax_search(){
		$current_settings = isset($_SESSION['users_list']) ? $_SESSION['users_list'] : array();
		$current_settings["order"] = "date_created";
		$current_settings["order_direction"] = "DESC";
		$current_settings["page"] = 1;
		$current_settings["data"] = array();
		
		$user_type = $this->input->post("user_type");
		if($user_type) $current_settings['data']['type'] = $user_type;
		
		$country = strval($this->input->post("id_country"));
		if($country) $current_settings["data"]["country"] = $country;
		
		$region = intval($this->input->post("id_region"));
		if($region) $current_settings["data"]["region"] = $region;
		
		$city = intval($this->input->post("id_city"));
		if($city) $current_settings["data"]["city"] = $city;
		
		$zip = $this->input->post("zip");
		$zip = trim(strip_tags($zip));
		if(!empty($zip)) $current_settings["data"]["zip"] = intval($zip);
		
		$keyword = $this->input->post("keyword");
		$keyword = trim(strip_tags($keyword));
		if(!empty($keyword)) $current_settings["data"]["keyword"] = $keyword;
	
		$_SESSION['users_list'] = $current_settings;
		
		$this->load->helper('seo');
		
		$search_action_data = $this->Users_model->get_search_link_data();
		echo rewrite_link('users', $search_action_data['keyword'] ? 'search' : 'index', $search_action_data);
	}
	
	/**
	 * Render view profile action
	 * @param integer $user_id
	 * @param string $section_gid
	 * @param boolean $pdf
	 */
	public function view($user_id, $section_gid="contacts", $pdf=false){
		if(!$user_id){show_404();return;}
	
		$pdf = $pdf != 'no' && $pdf ? true : false;

		$this->Users_model->set_format_settings(array('get_contact'=>true, 'get_company'=>true));
		$user = $this->Users_model->get_user_by_id($user_id);
		$this->Users_model->set_format_settings(array('get_contact'=>false, 'get_company'=>false));
		if(!$user){show_404();return;}
		
		if($this->session->userdata('auth_type') != 'user' || $this->session->userdata('user_id') != $user_id){
			if(!$user['status']){show_404();return;}
		}
		
		$display_sections = $this->Users_model->get_display_sections($user);
		foreach($display_sections as $display_section_gid => $used){
			if(!$used) continue;
			$user_content[$display_section_gid] = $this->_get_section_html($user, $display_section_gid);
		}
		
		
		$this->load->model('Listings_model');
		
		#MOD#
		$residen = $this->Listings_model->get_residensial();
		$commer = $this->Listings_model->get_commercial();
		
		$this->template_lite->assign('residential', $residen);
		$this->template_lite->assign('commercial', $commer);
		$this->template_lite->assign('tstamp', time());
		#MOD#
	
		$this->template_lite->assign("display_sections", $display_sections);
		$this->template_lite->assign("user_content", $user_content);
		$this->template_lite->assign("section_gid", $section_gid);

		//pagination
		$page_data = array();
		$this->config->load("date_formats", true);
		$page_data["date_format"] = $this->config->item("st_format_date_literal", "date_formats");
		$this->template_lite->assign("page_data", $page_data);
		
		$is_owner = 0;
		if($this->session->userdata("auth_type") == "user"){
			$auth_id = $this->session->userdata("user_id");
			$this->template_lite->assign('user_id', $auth_id);
			if($auth_id == $user_id) $is_owner = 1;
		}
		$this->template_lite->assign('is_user_owner', $is_owner);
		
		$this->Users_model->check_views_count($user_id);
		
		$this->Menu_model->breadcrumbs_set_parent("main-menu-pro-item");
		$this->Menu_model->breadcrumbs_set_active($user["output_name"], site_url()."users/view/".$user_id);
		
		if($pdf){
			$this->pg_theme->print_type = "pdf";
			$tpl_output = $this->template_lite->fetch("view");
			$this->Users_model->get_pdf_file($tpl_output, $user);
		}else{
			$this->pg_seo->set_seo_data($user);
			
			$this->template_lite->view("view", "user", "users");
		}
	}
	
	/**
	 * Render view section
	 * @param array $data
	 * @param string $section_gid
	 */
	private function _get_section_html($data, $section_gid="contacts"){
		$this->load->helper('users');
		$this->template_lite->assign("section_gid", $section_gid);
		$this->template_lite->assign("user", $data);
	
		switch($section_gid){
			case 'overview':
				$this->config->load("date_formats", TRUE);
				$date_format = $this->config->item("st_format_date_literal", "date_formats");
				$this->template_lite->assign("date_format", $date_format);
			break;
			case 'map':
			case 'map_info':
				$markers = array(
					array( 
						'gid' => $data['id'],
						'country' => $data['country'], 
						'region' => $data['region'], 
						'city' => $data['city'], 
						'address' => $data['address'], 
						'postal_code' => $data['postal_code'], 
						'lat' => (float)$data['lat'], 
						'lon' => (float)$data['lon'], 
						'info' => ($data['location'] ? $data['location'] : $data['output_name'].', '.l($data['user_type'], 'users')),
						'dragging' => false,
					),
				);
				$this->template_lite->assign("markers", $markers);

				$current_language = $this->pg_language->get_lang_by_id($this->pg_language->current_lang_id);
				$view_settings = array(
					'lang' => $current_language['code'],
				);
				$this->template_lite->assign('map_settings', $view_settings);
			break;
		}
	
		return $this->template_lite->fetch('view_content', 'user', 'users');
	}
	
	/**
	 * Return section content
	 * @param integer $user_id
	 * @param string $section_gid
	 */
	public function ajax_get_section($user_id, $section_gid, $template=''){
		$this->Users_model->set_format_settings('get_contact', true);
		$user = $this->Users_model->get_user_by_id($user_id);
		$this->Users_model->set_format_settings('get_contact', false);
		$this->template_lite->assign('ajax', 1);
		$this->template_lite->assign('template', $template);
		echo $this->_get_section_html($user, $section_gid);
		exit;
	}
	
	/**
	 * Render my agents list block
	 * @param integer $action
	 * @param string $order
	 * @param string $order_direction
	 * @param integer $page
	 */
	private function _my_agents_block($action="list", $order=null, $order_direction=null, $page=1){
		$company_id = $this->session->userdata("user_id");
		
		switch($action){
		    case "requests":
			$agent_status = 0;
		    break;
		    case "list":
		    default:
			$agent_status = 1;
			$action = "list";
		    break;
		}
		
		$current_settings = $_SESSION["users_agents_list"];
		if(!isset($current_settings["order"]))
			$current_settings["order"] = "date_created";
		if(!isset($current_settings["order_direction"]))
			$current_settings["order_direction"] = "DESC";
		if(!isset($current_settings["page"]))
			$current_settings["page"] = 1;

		if(!isset($current_settings["name"])) $current_settings["name"] = "";

		$name = null;
		
		if(!empty($_POST)){
		    $current_settings["name"] = trim(strip_tags($this->input->post('name', true)));
		}
		
		if(!empty($current_settings["name"])){
		    $name = $current_settings["name"];
		}
		
		$current_settings["filter"] = $agent_status;
		$current_settings["page"] = $page;		
			
		if(!$order) $order = $current_settings["order"];
		$this->template_lite->assign("order", $order);
		$current_settings["order"] = $order;


		if(!$order_direction) $order_direction = $current_settings["order_direction"];
		$this->template_lite->assign("order_direction", $order_direction);
		$current_settings["order_direction"] = $order_direction;

		$agents_count = $this->Users_model->get_agents_count($company_id, $agent_status , $name);
		
		if(!$page) $page = $current_settings["page"];
		$items_on_page = $this->pg_module->get_module_config("start", "index_items_per_page");
			
		$this->load->helper("sort_order");
		$page = get_exists_page_number($page, $agents_count, $items_on_page);
		$current_settings["page"] = $page;

		$_SESSION["users_agents_list"] = $current_settings;

		$sort_links = array(
			"name" => site_url()."users/my_agents/".$action."/name/".(($order != "name" xor $order_direction == "DESC") ? "ASC" : "DESC"),
			"email" => site_url()."users/my_agents/".$action."/email/".(($order != "email" xor $order_direction == "DESC") ? "ASC" : "DESC"),
			"phone" => site_url()."users/my_agents/".$action."/phone/".(($order != "phone" xor $order_direction == "DESC") ? "ASC" : "DESC"),
			"listings_count" => site_url()."users/my_agents/".$action."/listings_count/".(($order != "listings_count" xor $order_direction == "DESC") ? "ASC" : "DESC"),
			"date_created" => site_url()."users/my_agents/".$action."/date_created/".(($order != "date_created" xor $order_direction == "DESC") ? "ASC" : "DESC"),
		);
		$this->template_lite->assign("sort_links", $sort_links);

		if($order == "name") $order = "CONCAT(fname,' ',lname)";

		if($agents_count > 0){
		    $agents = $this->Users_model->get_agents_list($company_id, $agent_status, $page, $items_on_page, array($order => $order_direction) , true, $name);
		    $this->template_lite->assign("agents", $agents);
		}
		
		$this->template_lite->assign("agent_status", $agent_status);
		$this->template_lite->assign("action", $action);
		
		$this->load->helper("navigation");
		$this->config->load("date_formats", TRUE);
		$url = site_url()."users/my_agents/".$action."/".$order."/".$order_direction."/";
		$page_data = get_user_pages_data($url, $agents_count, $items_on_page, $page, "briefPage");
		$page_data["date_format"] = $this->config->item("st_format_date_time_literal", "date_formats");
		$page_data['name'] = $current_settings["name"];
		$this->template_lite->assign("page_data", $page_data);
		
		return $this->template_lite->fetch("my_agents_block", "user", "users");
	}
	
	/**
	 * Render my agents list action
	 * @param integer $action
	 * @param string $order
	 * @param string $order_direction
	 * @param integer $page
	 */
	public function my_agents($action=null, $order=null, $order_direction=null, $page=null){
		$user_type = $this->session->userdata("user_type");
	
		if($user_type != 'company') show_404();
		
		$current_settings = isset($_SESSION["my_agents_search"])?$_SESSION["my_agents_search"]:array();
		
		if(!$current_settings["action"]) $current_settings["action"] = 'list';
		if(!$current_settings['order']) $current_settings["order"] = 'date_modified';
		if(!$order_direction) $current_settings["order_direction"] = 'DESC';
		if(!$current_settings["page"]) $current_settings["page"] = 1;
		
		if($action) $current_settings["action"] = $action;
		if($order) $current_settings["order"] = $order;
		if($order_direction) $current_settings["order_direction"] = $order_direction;
		if($page) $current_settings["page"] = $page;
		$_SESSION["my_agents_search"] = $current_settings;
	
		$content = $this->_my_agents_block($current_settings["action"],  $current_settings["order"],  $current_settings["order_direction"],  $current_settings["page"]);
		$this->template_lite->assign("block", $content);
		
		$this->Menu_model->set_menu_active_item("company_account_menu", "company_my_agents_". $current_settings["action"]."_item");
		$this->Menu_model->breadcrumbs_set_parent("company_my_agents_". $current_settings["action"]."_item");
		
		$this->template_lite->view("my_agents_list");
	}
	
	/**
	 * Render my agents list ajax action
	 * @param integer $action
	 * @param string $order
	 * @param string $order_direction
	 * @param integer $page
	 */
	public function ajax_my_agents($action="list", $order="date_modified", $order_direction="DESC", $page=1){
		$current_settings = isset($_SESSION["my_gents"])?$_SESSION["my_agents_search"]:array();
		$current_settings["action"] = $action;
		$current_settings["order"] = $order;
		$current_settings["order_direction"] = $order_direction;
		$current_settings["page"] = $page;
		$_SESSION["my_agents_search"] = $current_settings;
		echo $this->_my_agents_block($action, $order, $order_direction, $page);
		exit;
	}	
	
	/**
	 * Process request selected agents action
	 * @param string $action
	 * @param integer $agent_id	 
	 */
	public function my_agents_request($action="approve", $agent_id=null){
		$errors = false;
		$messages = array();
		if(!$agent_id){
			$agent_id = $this->input->post("ids");
		}		
		if(!empty($agent_id)){
			$company_id = $this->session->userdata("user_id");
			foreach((array)$agent_id as $i){
				$error = $this->Users_model->{$action."_agent"}($company_id, $i);
				if($error){
					$errors = true;
					$messages[] = $error;
				}else{
					$messages[] = l("success_".$action."_agent", "users");
				}
			}
			if($errors){
				$this->system_messages->add_message("error", implode("<br>", $messages));				
			}else{
				$this->system_messages->add_message("success", implode("<br>", $messages));
			}
		}
		
		$url = site_url()."users/my_agents/".($action == 'approve' ? 'list' : 'requests');
		redirect($url);
	}
	
	/**
	 * Remove selected agents action
	 * @param integer $id
	 */
	public function my_agents_delete($agent_id=null){
		$errors = false;
		$messages = array();
		if(!$agent_id){
			$agent_id = $this->input->post("ids");
		}		
		if(!empty($agent_id)){
			$company_id = $this->session->userdata("user_id");
			foreach((array)$agent_id as $i){
				$error = $this->Users_model->delete_agent($company_id, $i);
				if($error){
					$errors = true;
					$messages[] = $error;
				}else{
					$messages[] = l("success_deleted_agent", "users");
				}
			}
			if($errors){
				$this->system_messages->add_message("error", implode("<br>", $messages));				
			}else{
				$this->system_messages->add_message("success", implode("<br>", $messages));
			}
		}
		$url = site_url()."users/my_agents/list";
		redirect($url);
	}
	
	/**
	 * Get out from company
	 */
	public function agent_get_out(){
		$user_id = $this->session->userdata("user_id");
		$user = $this->Users_model->get_user_by_id($user_id);
		$error = $this->Users_model->delete_agent($user["agent_company"], $user_id, true);
		if($error){
			$this->system_messages->add_message("error", $error);	
		}else{
			$this->system_messages->add_message("success", l("success_agent_get_out", "users"));
		}			
		$url = site_url()."users/profile";
		redirect($url);
	}
	
	/**
	 * Change currency action
	 * @param integer $currency_id
	 */
	public function change_currency($currency_id){
		$currency_id = intval($currency_id);
		$this->session->set_userdata("currency_id", $currency_id);

		if($this->session->userdata("auth_type") == "user"){
			$user_id = $this->session->userdata("user_id");
			$save_data["id_currency"] = $currency_id;
			$this->Users_model->save_user($user_id, $save_data);
		}

		if(strpos($_SERVER["HTTP_REFERER"], site_url()) !== false){
			redirect($_SERVER["HTTP_REFERER"]);
		}else{
			redirect();
		}
	}
	
	/**
	 * Set map type for user
	 */
	public function ajax_set_map($param){
		if($this->session->userdata("auth_type") != "user")	return;
		$user_id = $this->session->userdata("user_id");
		
		$user = $this->Users_model->get_user_by_id($user_id);
		if(!$user) return;
		
		$this->load->model('geomap/models/Geomap_model');
		$this->load->model('geomap/models/Geomap_settings_model');
		
		switch($param){
			case 'type': $settings['view_type'] = $this->input->post('type', true); break;
			case 'zoom': $settings['zoom'] = $this->input->post('zoom', true); break;
			case 'coordinates':
				$settings['lat'] = $this->input->post('lat', true);
				$settings['lon'] = $this->input->post('lon', true);
			break;
		}
		if(!$settings) return;
		$validate_data = $this->Geomap_settings_model->validate_settings($settings);
		if(empty($validate_data["errors"])){
			$map_gid = $this->Geomap_model->get_default_driver_gid();
			$this->Geomap_settings_model->save_settings($map_gid, 0, $user_id, 'user_profile', $validate_data["data"]);
		}
		if($param == 'coordinates'){
			$data['lat'] = $this->input->post('lat', true);
			$data['lon'] = $this->input->post('lon', true);
			$this->Users_model->save_user($user_id, $data);
		}
		return;
	}
	
	/**
	 * Set user coordinates after geocode
	 * @param integer $user_id user identifier
	 */
	public function ajax_set_geocode_coordinates($user_id){
		$user = $this->Users_model->get_user_by_id($user_id);
		
		if(!$user) return;
		
		if((float)$user['lat'] > 0 || (float)$user['lot'] > 0) return;
		
		$data['lat'] = $this->input->post('lat');
		$data['lon'] = $this->input->post('lon');
	
		$validate_data = $this->Users_model->validate_user($data);
		if(!empty($validate_data["errors"])){
			echo json_encode(array('error' => implode('<br>', $validate_data["errors"])));
			exit;
		}else{
			$this->Users_model->save_user($user_id, $validate_data['data']);
		}
	}
	
	/**
	 * Update user map
	 */
	public function ajax_update_map(){
		$user_id = $this->session->userdata("user_id");
		$data = $this->Users_model->get_user_by_id($user_id);
		
		$lat = $this->input->post('lat', true);
		$lon = $this->input->post('lon', true);
		
		$markers = array(
			array( 
				"lat" => (float)$lat, 
				"lon" => (float)$lon, 
				"info" => $data["output_name"].', '.l($data['user_type'], 'users'),
				"dragging" => true,
			),
		);

		$this->load->helper('geomap');
		echo update_default_map(array('markers'=>$markers, 'rand'=>1));
	}
	
	/**
	 * Return visitors block
	 * @param string $order
	 * @param string $order_direction
	 * @param integer $page
	 */
	private function _visitors_block($field_name, $order="date_created", $order_direction="DESC", $page=1){
		$current_settings = isset($_SESSION["users_visitors_list"])?$_SESSION["users_visitors_list"]:array();
		if(!isset($current_settings["order"]))
			$current_settings["order"] = "date_created";
		if(!isset($current_settings["order_direction"]))
			$current_settings["order_direction"] = "DESC";
		if(!isset($current_settings["page"]))
			$current_settings["page"] = 1;
		if(!isset($current_settings["filters"]))
			$current_settings["filters"] = array();

		$visit_type = $field_name == 'id_visitor' ? 'visitors' : 'visits';

		$filters = $current_settings["filters"];
		
		if(!$order) $order = $current_settings["order"];
		$this->template_lite->assign("order", $order);
		$current_settings["order"] = $order;

		if (!$order_direction) $order_direction = $current_settings["order_direction"];
		$this->template_lite->assign("order_direction", $order_direction);
		$current_settings["order_direction"] = $order_direction;

		if(!$page) $page = $current_settings["page"];
		$items_on_page = $this->pg_module->get_module_config("start", "index_items_per_page");
	
		$sort_data = array(
			"url" => site_url()."users/".$visit_type,
			"order" => $order,
			"direction" => $order_direction,
			"links" => array(
				"visits_count" => l("field_visits_count", "users"), 
				"last_visit_date" => l("field_last_visit_date", "users"),
			)
		);
		$this->template_lite->assign("sort_data", $sort_data);
		
		$this->load->model('users/models/Users_visitor_model');
		
		$users_count = $this->Users_visitor_model->get_visitors_count($filters);
		if($users_count > 0){
			$order_array = array($order => $order_direction, 'id'=>$order_direction);
			$visitors = $this->Users_visitor_model->get_visitors_list($filters, $page, $items_on_page, $order_array);
			$users_for_search = array();
			foreach($visitors as $value){
				$users_for_search[] = $value[$field_name];
			}
			$users = array();
			$results = $this->Users_model->get_users_list_by_key(null, null, null, array(), $users_for_search);
			foreach($users_for_search as $user_id){
				$users[$user_id] = isset($results[$user_id]) ? $results[$user_id] :
					$this->Users_model->format_default_user($user_id);
			}
			$this->template_lite->assign("users", $users);	
			
			$users_for_contact = array();
			if($this->pg_module->is_module_installed("users_services")){
				if($this->session->userdata("auth_type") == "user"){
					$auth_id = $this->session->userdata("user_id");
					
					$this->load->model("Users_services_model");
					foreach($visitors as $value){
						if($auth_id == $value[$field_name]){
							$users_for_contact[$value[$field_name]] = true;
						}else{
							$users_for_contact[$value[$field_name]] = 
								$this->Users_services_model->is_available_contact($auth_id, $value[$field_name]);
						}
						
					}
					$this->template_lite->assign('users_for_contact', $users_for_contact);
				}
			}else{
				$this->template_lite->assign('is_contact', true);
			}
		}
		$this->template_lite->assign('users_count', $users_count);
	
		$this->load->helper("sort_order");
		$page = get_exists_page_number($page, $users_count, $items_on_page);
		$current_settings["page"] = $page;

		$this->load->helper("navigation");
		$this->config->load("date_formats", TRUE);
		$url = site_url().'users/'.$visit_type.'/'.$order.'/'.$order_direction.'/';
		$page_data = get_user_pages_data($url, $users_count, $items_on_page, $page, "briefPage");
		$page_data["date_format"] = $this->config->item("st_format_date_time_literal", "date_formats");
		$this->template_lite->assign("page_data", $page_data);
		
		$_SESSION["users_visitors_list"] = $current_settings;
		
		return $this->template_lite->fetch("visitors_block", "user", "users");
	}
	
	/**
	 * Render visitors list action
	 * @param string $order
	 * @param string $order_description
	 * @param integer $page
	 */
	public function visitors($order="date_created", $order_direction="DESC", $page=1){
		$current_settings = isset($_SESSION["users_visitors_list"])?$_SESSION["users_visitors_list"]:array();
		
		$user_id = intval($this->session->userdata("user_id"));
		$filters = array('user'=>$user_id);
		
		$current_settings["order"] = $order;
		$current_settings["order_direction"] = $order_direction;
		$current_settings["page"] = $page;
		$current_settings["filters"] = $filters;	
		$_SESSION["users_visitors_list"] = $current_settings;
	
		$user_block = $this->_visitors_block('id_visitor', $order, $order_direction, $page);
		$this->template_lite->assign("block", $user_block);
		
		$current_settings = isset($_SESSION["users_visitors_list"])?$_SESSION["users_visitors_list"]:array();
		if(!isset($current_settings["order"])) $current_settings["order"] = "date_created";
		$this->template_lite->assign('order', $current_settings["order"]);
		
		if(!isset($current_settings["order_direction"])) $current_settings["order_direction"] = "DESC";
		$this->template_lite->assign('order_direction', $current_settings["order_direction"]);
		
		if(!isset($current_settings["page"])) $current_settings["page"] = 1;
		$this->template_lite->assign('page', $current_settings["page"]);
		
		$this->Menu_model->breadcrumbs_set_active(l("text_profile_visitors", "users"));
	
		$this->template_lite->view("visitors", "user", "users");
	}
	
	/**
	 * Render visitors list action by ajax
	 * @param string $order
	 * @param string $order_description
	 * @param integer $page
	 */
	public function ajax_visitors($order="date_created", $order_direction="DESC", $page=1){
		$current_settings = isset($_SESSION["users_visitors_list"])?$_SESSION["users_visitors_list"]:array();
		
		$user_id = intval($this->session->userdata("user_id"));
		$filters = array('user'=>$user_id);
		
		$current_settings["order"] = $order;
		$current_settings["order_direction"] = $order_direction;
		$current_settings["page"] = $page;
		$current_settings["filters"] = $filters;	
		$_SESSION["users_visitors_list"] = $current_settings;
	
		echo $user_block = $this->_visitors_block('id_visitor', $order, $order_direction, $page);
	}
	
	/**
	 * Render visits list action
	 * @param string $order
	 * @param string $order_description
	 * @param integer $page
	 */
	public function visits($order="date_created", $order_direction="DESC", $page=1){
		$current_settings = isset($_SESSION["users_visitors_list"])?$_SESSION["users_visitors_list"]:array();
		
		$user_id = intval($this->session->userdata("user_id"));
		$filters = array('visitor'=>$user_id);
		
		$current_settings["order"] = $order;
		$current_settings["order_direction"] = $order_direction;
		$current_settings["page"] = $page;
		$current_settings["filters"] = $filters;		
		$_SESSION["users_visitors_list"] = $current_settings;
	
		$user_block = $this->_visitors_block('id_user', $order, $order_direction, $page);
		$this->template_lite->assign("block", $user_block);
		
		$current_settings = isset($_SESSION["users_visitors_list"])?$_SESSION["users_visitors_list"]:array();
		if(!isset($current_settings["order"])) $current_settings["order"] = "date_created";
		$this->template_lite->assign('order', $current_settings["order"]);
		
		if(!isset($current_settings["order_direction"])) $current_settings["order_direction"] = "DESC";
		$this->template_lite->assign('order_direction', $current_settings["order_direction"]);
		
		if(!isset($current_settings["page"])) $current_settings["page"] = 1;
		$this->template_lite->assign('page', $current_settings["page"]);
		
		$this->Menu_model->breadcrumbs_set_active(l("text_profile_visits", "users"));
		
		$this->template_lite->view("visits", "user", "users");
	}
	
	/**
	 * Render visits list action by ajax
	 * @param string $order
	 * @param string $order_description
	 * @param integer $page
	 */
	public function ajax_visits($order="date_created", $order_direction="DESC", $page=1){
		$current_settings = isset($_SESSION["users_visitors_list"])?$_SESSION["users_visitors_list"]:array();
		
		$user_id = intval($this->session->userdata("user_id"));
		$filters = array('visitor'=>$user_id);
		
		$current_settings["order"] = $order;
		$current_settings["order_direction"] = $order_direction;
		$current_settings["page"] = $page;
		$current_settings["filters"] = $filters;		
		$_SESSION["users_visitors_list"] = $current_settings;
	
		echo $this->_visitors_block('id_user', $order, $order_direction, $page);
	}
	
	/**
	 * Change view mode
	 * @param string $view_mode
	 */
	public function set_view_mode($view_mode){
		if(in_array($view_mode, array('list', 'map'))){
			$_SESSION['users_view_mode'] = $view_mode;
		}
		redirect($_SERVER["HTTP_REFERER"]);
	}
}
