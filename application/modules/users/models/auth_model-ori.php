<?php
/**
* User auth model
*
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/

if (!defined("BASEPATH")) exit("No direct script access allowed");

class Auth_model extends Model{
	/**
	 * Link to CodeIgniter object
	 * @var object
	 */
	var $CI;
	
	/**
	 * Link to database object
	 * @var object
	 */
	var $DB;

	/**
	 * Constructor
	 */
	public function Auth_model(){
		parent::Model();
		$this->CI = & get_instance();
		$this->CI->load->model("Users_model");
	}

	/**
	 * Do sign in 
	 * @param integer $id
	 * @return array
	 */
	public function login($id){
		$data["errors"] = array();
		$data["user_data"] = array();
		$data["login"] = false;

		$this->CI->Users_model->set_format_settings('get_safe', false);
		$user_data = $this->CI->Users_model->get_user_by_id($id);
		$this->CI->Users_model->set_format_settings('get_safe', true);
		if(empty($user_data) || !$this->CI->pg_module->get_module_config("users", $user_data['user_type']."_login_enabled")){
			$data["errors"][] = l("error_login_invalid_data", "users");
		}else{
			if($user_data["status"] == 0){
				$data["errors"][] = l("error_disabled_user", "users");
			}elseif($user_data["confirm"] == 0){
				$data["errors"][] = l("error_unconfirmed_user", "users");
			}else{
				$data["user_data"] = $user_data;
				$data["login"] = true;

				$this->update_user_session_data($id);
			}
		}
		return $data;
	}

	/**
	 * Update data of user session
	 * @param integer $id
	 */
	public function update_user_session_data($id){
		$user_data = $this->CI->Users_model->get_user_by_id($id);
		$session = array(
			"auth_type" => "user",
			"user_id" => $user_data["id"],
			"user_type" => $user_data["user_type"],
			"name" => $user_data["output_name"],
			"email" => $user_data["email"],
			"unique_name" => $user_data["unique_name"],
			"lang_id" => $user_data["lang_id"],
			"logo" => $user_data['media'][!empty($user_data['user_logo_moderation'])?'user_logo_moderation':'user_logo']['thumbs']['small'],
		);
		$this->CI->session->set_userdata($session);
		$this->CI->template_lite->assign("user_session_data", $this->CI->session->all_userdata());
		return;
	}

	/**
	 * Do sign in by e-mail and password
	 * @param string $email
	 * @param string $password
	 * @return array
	 */
	public function login_by_email_password($email, $password){
		$user_data = $this->CI->Users_model->get_user_by_email_password($email, $password);
		if(empty($user_data)){
			$data["errors"][] = l("error_login_invalid_data", "users");
		}else{
			$data = $this->login($user_data["id"]);
		}
		return $data;
	}

	/**
	 * Request to openID
	 * Don't delete (openid)
	 * @param string $open_identifier
	 * @param boolean $policies
	 * @return boolean
	 */
	/*public function open_id_request($openid_identifier, $policies=false){
		$this->CI->load->library('openid');
		$this->CI->config->load('openid');

		if(!$policies){
			$policies = array();
		}

		$req = $this->CI->config->item("openid_required");
		$opt = $this->CI->config->item("openid_optional");
		$policy = site_url($this->CI->config->item("openid_policy"));
		$request_to = site_url($this->CI->config->item("openid_request_to"));

		$this->CI->openid->set_request_to($request_to);
		$this->CI->openid->set_trust_root(base_url());
		$this->CI->openid->set_args(null);
		$this->CI->openid->set_sreg(true, $req, $opt, $policy);
		$this->CI->openid->set_pape(true, $policies);
		$return = $this->CI->openid->authenticate($openid_identifier);
		if(strlen($return)){
			$data["errors"][] = l($return, "users");
			return $data;
		}
		return true;
	}*/

	/**
	 * Response from openID
	 * Don't delete (openid)
	 *
	 */
	/*public function open_id_response(){
		$this->CI->load->library("openid");
		$this->CI->config->load("openid");

		$request_to = site_url($this->CI->config->item("openid_request_to"));

		$this->CI->openid->set_request_to($request_to);
		$response = $this->CI->openid->getResponse();

		switch($response->status){
			case Auth_OpenID_CANCEL: $data["errors"] = l("openid_cancel", "users"); break;
			case Auth_OpenID_FAILURE: $data["errors"][] = str_replace("%s", $response->message, l("openid_failure", "users")); break;
			case Auth_OpenID_SUCCESS:
				$openid = $response->getDisplayIdentifier();
				$esc_identity = htmlspecialchars($openid, ENT_QUOTES);
				$data["data"]["openid"] = $esc_identity;
				$data["success"] = str_replace(array("%s","%t"), array($esc_identity, $esc_identity), l("openid_success", "users"));
				$sreg_resp = Auth_OpenID_SRegResponse::fromSuccessResponse($response);
				$sreg = $sreg_resp->contents();
				foreach($sreg as $key => $value){
					$data["data"][$key] = $value;
				}
				break;
		}

		if(!empty($data["success"])){

			//// get user_id by open_id
			$user_data = $this->CI->Users_model->get_user_by_open_id($data["data"]["openid"]);

			//// if user not exists - check email - create account
			if(empty($user_data)){
				$open_id_data = $this->CI->Users_model->validate_open_id_data($data["data"]);
				$validate = $this->CI->Users_model->validate_user(null, $open_id_data);
				if(!empty($validate["errors"])){
					foreach($validate["errors"] as $error){
						$data["errors"][] = $error;
					}
				}else{
					$user_id = $this->CI->Users_model->save_user(null, $validate["data"]);
				}
			}else{
				$user_id = $user_data["id"];
			}

			if($user_id){
				$user_login = $this->login($user_id);
				if ($user_login !== TRUE){
					foreach($user_login["errors"] as $error){
						$data["errors"][] = $error;
					}
				}
			}
		}
		return $data;
	}*/

	/**
	 * Log off
	 */
	public function logoff(){
		$this->CI->session->sess_destroy();
		
		$_SESSION = array();
		
		session_destroy();
		session_write_close();
	}

	/**
	 * Validate login data
	 * @param array $data
	 * @return array
	 */
	public function validate_login_data($data){
		$return = array("errors"=> array(), "data" => array());

		// Don't delete (openid)
		/*if(isset($data["user_open_id"]) && !empty($data["user_open_id"])){
			$return["data"]["user_open_id"] = trim($data["user_open_id"]);
		}else*/
		if(isset($data["email"]) && isset($data["password"])){
			$this->CI->config->load("reg_exps", TRUE);

			$email_expr =  $this->CI->config->item("email", "reg_exps");
			$return["data"]["email"] = strip_tags($data["email"]);
			if(empty($return["data"]["email"]) || !preg_match($email_expr, $return["data"]["email"])){
				$return["errors"][] = l("error_email_incorrect", "users");
			}

			$password_expr =  $this->CI->config->item("password", "reg_exps");
			$data["password"] = trim(strip_tags($data["password"]));
			if(!preg_match($password_expr, $data["password"])){
				$return["errors"][] = l("error_password_incorrect", "users");
			}else{
				$return["data"]["password"] = $data["password"];
			}
		}else{
			$return["errors"][] = l("error_login_invalid_data", "users");
		}

		return $return;
	}
}
