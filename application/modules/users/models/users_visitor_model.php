<?php
/**
* Users visitor model
*
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/

if (!defined("BASEPATH")) exit("No direct script access allowed");

define("USERS_VISITOR_TABLE", DB_PREFIX."user_profile_visitors");

class Users_visitor_model extends Model{

	/**
	 * Code Igniter object
	 * @var object
	 */
	private $CI;
	
	/**
	 * Database object
	 * @var object
	 */
	private $DB;

	/**
	 * Shared fields
	 * @var array
	 */
	private $fields = array(
		"id",
		"id_user",
		"id_visitor",
		"last_visit_date",
		"visits_count",	
	);

	/**
	 * Constructor
	 *
	 * @return users_visitor object
	 */
	public function Users_visitor_model(){
		parent::Model();
		$this->CI = & get_instance();
		$this->DB = &$this->CI->db;
	}

	/**
	 * Return visitor data from data source by id 
	 * @param integer $id record identifier
	 * @return array/false
	 */
	public function get_visitor_by_id($id){
		$result = $this->DB->select(implode(", ", $this->fields))
						   ->from(USERS_VISITOR_TABLE)
						   ->where('id', $id)
						   ->get()->result_array();
		if(empty($result)) return false;
		return $result[0];
	}
	
	/**
	 * Return visitor data from data source by id 
	 * * @param integer $user_id
	 * @param integer $visitor_id
	 * @return array/false
	 */
	public function get_visitor_by_visitor($user_id, $visitor_id){
		$result = $this->DB->select(implode(", ", $this->fields))
						   ->from(USERS_VISITOR_TABLE)
						   ->where('id_user', $user_id)
						   ->where('id_visitor', $visitor_id)
						   ->get()->result_array();
		if(empty($result)) return false;
		return $result[0];
	}
	
	/**
	 * Save visitor
	 * @param integer $id visitor identifier
	 * @param array $data
	 */
	public function save_visitor($id, $data=array()){
		if(!isset($data['last_visit_date'])) $data["last_visit_date"] = date("Y-m-d H:i:s");
		if(!$id){
			if(!isset($data['visits_count'])) $data['visits_count'] = 1;
			$this->DB->insert(USERS_VISITOR_TABLE, $data);
			$id = $this->DB->insert_id();
		}else{
			if(!isset($data['visits_count'])) $this->DB->set("visits_count", "visits_count + 1", FALSE);
			$this->DB->where("id", $id);
			$this->DB->update(USERS_VISITOR_TABLE, $data);
		}
		return $id;
	}
	
	/**
	 * Return search criteria
	 * @param array $filters filters data
	 */
	private function _get_search_criteria($filters){
		$params = array();

		$fields = array_flip($this->fields);
		foreach($filters as $filter_name=>$filter_data){
			switch($filter_name){
				// By user
				case "user":
					if(!$filter_data) continue;
					$params = array_merge_recursive($params, array("where"=>array(USERS_VISITOR_TABLE.".id_user"=>$filter_data)));
				break;
				// By visitor
				case "visitor":
					if(!$filter_data) continue;
					$params = array_merge_recursive($params, array("where"=>array(USERS_VISITOR_TABLE.".id_visitor"=>$filter_data)));
				break;
				// By week
				case "week":
					if(!$filter_data) continue;
					$params = array_merge_recursive($params, array("where_sql"=>array('('.USERS_VISITOR_TABLE.".last_visit_date >= DATE_SUB(CURDATE(), INTERVAL 1 WEEK))")));
				break;
				// By month
				case "month":
					if(!$filter_data) continue;
					$params = array_merge_recursive($params, array("where_sql"=>array('('.USERS_VISITOR_TABLE.".last_visit_date >= DATE_SUB(CURDATE(), INTERVAL 1 MONTH))")));
				break;
				default:
					if(isset($fields[$filter_name])){
						if(empty($filter_data)) break;
						$params = array_merge_recursive($params, array("where_in"=>array(USERS_VISITOR_TABLE.".".$filter_name=>$filter_data)));	
					}else{
						$params = array_merge_recursive($params, array("where"=>array(USERS_VISITOR_TABLE.".".$filter_name=>$filter_data)));	
					}
				break;
			}
		}
		
		return $params;
	}
	
	/**
	 * Return visitors as array
	 * @param integer $page
	 * @param string $limits 
	 * @param array $order_by
	 * @param array $params
	 * @return array
	 */
	private function _get_visitors_list($page=null, $limits=null, $order_by=null, $params=array()){
		$this->DB->select(USERS_VISITOR_TABLE.".".implode(", ".USERS_VISITOR_TABLE.".", $this->fields));
		$this->DB->from(USERS_VISITOR_TABLE);

		if(isset($params["join"]) && is_array($params["join"]) && count($params["join"])){
			foreach($params["join"] as $join){
				$this->DB->join($join["table"], $join["condition"], $join["type"]);
			}
		}

		if(isset($params["where"]) && is_array($params["where"]) && count($params["where"])){
			foreach($params["where"] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])){
			foreach($params["where_in"] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])){
			foreach($params["where_sql"] as $value){
				$this->DB->where($value);
			}
		}

		if(is_array($order_by) && count($order_by) > 0){
			foreach($order_by as $field => $dir){
				if(in_array($field, $this->fields)){
					$this->DB->order_by($field . " " . $dir);
				}
			}
		}elseif($order_by){
			$this->DB->order_by($order_by);
		}

		if(!is_null($page)){
			$page = intval($page)?intval($page):1;
			$this->DB->limit($limits, $limits*($page-1));
		}
	
		$results = $this->DB->get()->result_array();
		
		if(!empty($results) && is_array($results)){
			foreach($results as $r){
				$data[] = $r;
			}
			return $data;
		}
		return array();
	}
	
	/**
	 * Return number of visitors
	 * @param array $params
	 * @return integer
	 */
	private function _get_visitors_count($params=null){
		$this->DB->select("COUNT(*) AS cnt");
		$this->DB->from(USERS_VISITOR_TABLE);

		if(isset($params["join"]) && is_array($params["join"]) && count($params["join"])){
			foreach($params["join"] as $join){
				$this->DB->join($join["table"], $join["condition"], $join["type"]);
			}
		}
		
		if(isset($params["where"]) && is_array($params["where"]) && count($params["where"])){
			foreach($params["where"] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])){
			foreach($params["where_in"] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])){
			foreach($params["where_sql"] as $value){
				$this->DB->where($value);
			}
		}

		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			return intval($results[0]["cnt"]);
		}
		return 0;
	}

	/**
	 * Return list of filtered visitors
	 * @param array $filters
	 * @param integer $page
	 * @param integer $items_on_page
	 * @param string $order_by
	 * @param boolean $formatted
	 */
	public function get_visitors_list($filters=array(), $page=null, $items_on_page=null, $order_by=null){
		$params = $this->_get_search_criteria($filters);		
		return $this->_get_visitors_list($page, $items_on_page, $order_by, $params);
	}
	
	/**
	 * Return number of filtered visitors
	 * @param array $filters
	 * @return array
	 */
	public function get_visitors_count($filters=array()){
		$params = $this->_get_search_criteria($filters);
		return $this->_get_visitors_count($params);
	}
	
	/**
	 * Validate visitor
	 * @param integer $id visitor identifier
	 * @param array $data
	 * @return array
	 */
	public function validate_visitor($id, $data){
		$return = array("errors"=>array(), "data"=>array());
		
		if(isset($data['id'])){
			$return['data']['id'] = intval($data['id']);
			if(empty($return['data']['id'])) unset($return['data']['id']);
		}
		
		if(isset($data['id_user'])){
			$return['data']['id_user'] = intval($data['id_user']);
		}
		
		if(isset($data['id_visitor'])){
			$return['data']['id_visitor'] = intval($data['id_visitor']);
		}
		
		if(isset($data['last_visit_date'])){
			$value = strtotime($data['last_visit_date']);
			if($value > 0) $return['data']['last_visit_date'] = date("Y-m-d", $value);
		}
		
		if(isset($data['visit_counts'])){
			$return['data']['visit_counts'] = intval($data['visit_counts']);
		}
		
		return $return;
	}
}
