<?php
/**
* Users main model
*
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/

if (!defined("BASEPATH")) exit("No direct script access allowed");

define("USERS_TABLE", DB_PREFIX."users");
define("USER_ACCOUNT_STAT_TABLE", DB_PREFIX."user_account_list");

class Users_model extends Model{

	/**
	 * Code Igniter object
	 * @var object
	 */
	private $CI;
	
	/**
	 * Database object
	 * @var object
	 */
	private $DB;

	/**
	 * Available user type
	 * @var array
	 */
	public $type_arr = array('private', 'company', 'agent');
	
	/**
	 * Shared fields
	 * @var array
	 */
	private $fields_general = array(
		"id",
		"date_created",
		"date_modified",
		"fname",
		"sname",
		"unique_name",
		"company_name",		
		"email",
		"email_restore",
		"email_restore_code",
		'phone_restore',
		'phone_restore_code',
		"phone",
		"user_type",
		"password",
		"status",
		"confirm",
		"lang_id",
		"user_open_id",
		"user_logo",
		"user_logo_moderation",
		"group_id",
		"account",
		"views",
		/// company's agents field
		"agent_count",
		/// agents field for company data storage 
		"agent_company",
		"agent_date",
		"agent_status",
		/// services for companies and agents
		"featured_end_date",
		"show_logo_end_date",
		
		/// company's geo data
		"id_country",
		"id_region",
		"id_city",
		"address",
		"postal_code",

		"contact_email",
		"contact_phone",

		"lat",
		"lon",	
	);

	/**
	 * Shared fields
	 * @var array
	 */
	private $fields_all = array(
		/// general fields
		"id",
		"date_created",
		"date_modified",
		"fname",
		"sname",
		"unique_name",
		"company_name",		
		"email",
		"email_restore",
		"email_restore_code",
		'phone_restore',
		'phone_restore_code',
		"phone",
		"user_type",
		"password",
		"status",
		"confirm",
		"lang_id",
		"user_open_id",
		"user_logo",
		"user_logo_moderation",
		"group_id",
		"account",
		"confirm_code",
		"id_currency",
		"views",
		/// company's agents field
		"agent_count",
		/// agents field for company data storage 
		"agent_company",
		"agent_date",
		"agent_status",
		/// services for companies and agents
		"featured_end_date",
		"show_logo_end_date",
		/// contacts fields
		"contact_info",
		"contact_email",
		"contact_phone",
		"twitter",
		"facebook",
		"vkontakte",
		/// company's extended contacts fields
		"web_url", 
		"working_days",
		"working_hours_begin",
		"working_hours_end",
		"lunch_time_begin",
		"lunch_time_end",
		/// company's geo data
		"id_country",
		"id_region",
		"id_city",
		"address",
		"postal_code",

		"lat",
		"lon",		
	);	
	
	/**
	 * Safe field
	 * @var array
	 */
	private $_safe_fields = array(
		//'email', // #modification# disable this field to make the field available
		//'phone',
		//'password', // #modification# disable this field to make the field available
		//'email_restore', // #modification# disable this field to make the field available
		'email_restore_code',
		'phone_restore',
		'phone_restore_code',
		'user_open_id',
		//'confirm', // #modification# disable this field to make the field available
		'confirm_code',
	);
	
	/**
	 * Contact field
	 * @var array
	 */
	private $_contact_fields = array(
		'contact_info',
		'facebook',
		'twitter',
		'vkontakte',
		'contact_phone',
		'phone', // additional
		'contact_email',
		'location',
		'id_country',
		'id_region',
		'region_code',
		'id_city',
		'address',
		'postal_code',
		'lat',
		'lon',
		'web_url',
		'working_days',
		'working_days_str',
		'working_hours_begin',
		'working_hours_begin',
		'working_hours_str',
		'lunch_time_begin',
		'lunch_time_end',
		'lunch_time_str',
	);
	
	/**
	 * Upload gid
	 * @var string
	 */
	public $upload_config_id = "user-logo";

	/**
	 * Moderation type
	 * @var string
	 */
	private $moderation_type = "user_logo";
	
	/**
	 * Moderation type
	 * @var string
	 */
	private $moderation_type_text = "users";
	
	/**
	 * Field for view
	 * @var string
	 */
	public $form_editor_type = "user";
	
	/**
	 * Display sections
	 * @var array
	 */
	public $display_sections = array("company" => 0, "reviews" => 1, "contacts" => 1, "map" => 1);

	/**
	 * unique_name
	 * @var array
	 */
	private $user_gid_scheme = array(
		'private' => array('str' => "[fname][sname]", 'fields' => array('fname', 'sname')),
		'agent' => array('str' => "[fname][sname]", 'fields' => array('fname', 'sname')),
		'company' => array('str' => "[company_name]", 'fields' => array('company_name')),
	);
	
	/**
	 * Period of users services in days by default
	 * @var integer
	 */
	public $service_period_default = 14;

	/**
	 * Format settings
	 * @var array
	 */
	private $format_settings = array(
		'use_format' => true,
		'get_safe' => true,
		'get_contact' => false,
		'get_language' => false,
		'get_currency' => false,
		'get_group' => false,
		'get_company' => false,
	);
	
	/**
	 * Listings fields
	 * @var array
	 */
	private $fields_listings = array(
		'listings_for_sale_count', 
		'listings_for_buy_count', 
		'listings_for_rent_count', 
		'listings_for_lease_count',
	);
	
	private $fields_reviews = array(
		"review_data", 
		"review_count", 
		"review_sorter", 
		"review_type",
	);

	/**
	 * Constructor
	 *
	 * @return users object
	 */
	public function Users_model(){
		parent::Model();
		$this->CI = & get_instance();
		$this->DB = &$this->CI->db;
	
		if($this->CI->pg_module->is_module_installed("listings")){
			$this->fields_general = array_merge($this->fields_general, $this->fields_listings);
			$this->fields_all = array_merge($this->fields_all, $this->fields_listings);
		}
	
		if($this->CI->pg_module->is_module_installed("reviews")){
			$this->fields_general = array_merge($this->fields_general, $this->fields_reviews);
			$this->fields_all = array_merge($this->fields_all, $this->fields_reviews);
		}
	}
	
	/**
	 * Return available user types
	 * @return array
	 */
	public function get_user_types(){
		return $this->type_arr;
	}
	
	/**
	 * Return available user type default
	 * @return string
	 */
	public function get_user_type_default(){
		if(empty($this->type_arr)) return '';
		return current($this->type_arr);
	}

	/**
	 * Return user data from data source
	 * @param boolean $safe return only safe field
	 * @return array/false
	 */
	private function _get_user(){
		$args = func_get_args();
		$this->DB->select(implode(", ", $this->fields_all))->from(USERS_TABLE);

		$count = floor(count($args)/2);
		for($i=0; $i<$count;$i++){
			$this->DB->where($args[$i*2], $args[$i*2+1] );
		}
		
		$result = $this->DB->get()->result_array();
		if(empty($result)){
			return false;
		}else{
			$result = $this->format_users($result);
			return $result[0];
		}
	}

	/**
	 * Return user data from data source by id 
	 * @param integer $user_id user identifier
	 * @param boolean $safe return only safe field
	 * @return array/false
	 */
	public function get_user_by_id($user_id){
		return $this->_get_user("id", $user_id);
	}

	/**
	 * Return user data from data source by login
	 * @param string $login
	 * @return array/false
	 */
	public function get_user_by_login($login){
		return $this->_get_user("unique_name", $login);
	}

	/**
	 * Return user data from data source by confirm code
	 * @param string $code
	 * @return array/false
	 */
	public function get_user_by_confirm_code($code){
		return $this->_get_user("confirm_code", $code);
	}

	/**
	 * Return user data from data source by e-mail and password
	 * @param string $email
	 * @param string $password
	 * @return array/false
	 */
	public function get_user_by_email_password($email, $password){
		return $this->_get_user("email", $email, "password", $password);
	}

	/**
	 * Return user data from data source by e-mail
	 * @param string $email
	 * @return array/false
	 */
	public function get_user_by_email($email){
		return $this->_get_user("email", $email);
	}

	/**
	 * Return user data from data source by e-mail
	 * @param string $email
	 * @return array/false
	 */
	public function get_user_by_email_restore_code($code){
		return $this->_get_user("email_restore_code", $code);
	}
	
	/**
	 * Return user data from data source by phone
	 * @param string $phone
	 * @return array/false
	 */
	public function get_user_by_phone_restore_code($code){
		return $this->_get_user("phone_restore_code", $code);
	}

	/**
	 * Return user data from data source by openID
	 * @param string $open_id
	 * @return array/false
	 */
	public function get_user_by_open_id($open_id){
		return $this->_get_user("user_open_id", $user_open_id);
	}
	
	/**
	 * Check user password
	 * @param integer $user_id
	 * @param string $password
	 * @return boolean
	 */
	public function check_user_password($user_id, $password){
		return $this->_get_user("id", $user_id, "password", $password, false) ? true : false;
	}
	
	/**
	 * Code password
	 * @param string $password
	 */
	public function encode_password($password){
		return md5($password);
	}
	
	/**
	 * Encode confirm code
	 * @param string $confirm
	 */
	public function encode_confirm($confirm){
		return md5($confirm);
	}
	
	/**
	 * Return active users filter
	 * @return array
	 */
	private function _get_users_by_active($status){
		if(!$status) return array();
		$params["where"]["status"] = 1;
		$params["where"]["confirm"] = 1;
		return $params;
	}
	
	/**
	 * Return not active users filter
	 * @return array
	 */
	private function _get_users_by_not_active($status){
		if(!$status) return array();
		$params["where"]["status"] = 0;
		return $params;
	}
	
	/**
	 * Return not confirm users filter
	 * @return array
	 */
	private function _get_users_by_not_confirm($status){
		if(!$status) return array();
		$params["where"]["confirm"] = 0;
		return $params;
	}
	
	/**
	 * Return user country filter
	 * @return array
	 */
	private function _get_users_by_country($country_id){
		if(!$country_id) return array();
		$params["where"]["id_country"] = $country_id;
		return $params;
	}
	
	/**
	 * Return user region filter
	 * @return array
	 */
	private function _get_users_by_region($region_id){
		if(!$region_id) return array();
		$params["where"]["id_region"] = $region_id;
		return $params;
	}
	
	/**
	 * Return user city filter
	 * @return array
	 */
	private function _get_users_by_city($city_id){
		if(!$city_id) return array();
		$params["where"]["id_city"] = $city_id;
		return $params;
	}
	
	/**
	 * Return user type filter
	 * @param string $user_type
	 * @return array
	 */
	private function _get_users_by_type($user_type){
		$params = array();
		if(!$user_type || !in_array($user_type, $this->type_arr)) return array();
		$params["where"]["user_type"] = $user_type;
		return $params;
	}
	
	/**
	 * Return zip code filter
	 * @param string $zip_code
	 * @return array
	 */
	private function _get_users_by_zip($zip_code){
		if(empty($zip_code)) return array();
		$params["where"]["postal_code"] = $zip_code;
		return $params;
	}
	
	/**
	 * Return keywords filter
	 * @param string $keywords
	 * @return array
	 */
	private function _get_users_by_keyword($keyword){
		if(empty($keyword)) return array();
		$keyword = $this->DB->escape('%'.$keyword.'%');
		$params["where_sql"][] = "(fname LIKE ".$keyword." OR sname LIKE ".$keyword." OR company_name LIKE ".$keyword.")";
		return $params;
	}
	
	/**
	 * Return name filter
	 * @param string $keywords
	 * @return array
	 */
	private function _get_users_by_name($name){
		if(empty($name)) return array();
		$name_o = $this->DB->escape($name);
		$name_w = $this->DB->escape('%'.$name.'%');
		$params["where_sql"][] = "(company_name=".$name_o." OR fname LIKE ".$name_w." OR sname LIKE ".$name_w." OR CONCAT(fname, ' ', sname) LIKE ".$name_w.")";
		return $params;
	}
	
	/**
	 * Return not user filter
	 * @param string $keywords
	 * @return array
	 */
	private function _get_users_by_not_user($not_user){
		if(empty($not_user)) return array();
		$params["where"]["id !="] =  $not_user;
		return $params;
	}
	
	/**
	 * Return users as array
	 * @param integer $page
	 * @param integer $items_on_page
	 * @param string $order_by
	 * @param array $params
	 * @param array $filter_object_ids
	 * @param boolean $formatted
	 * @return array
	 */
	public function get_users_list($page=null, $items_on_page=null, $order_by=null, $params=array(), $filter_object_ids=null, $formatted=true, $all=false){
		$this->DB->select(implode(", ", $all ? $this->fields_all : $this->fields_general));
		$this->DB->from(USERS_TABLE);

		if(isset($params["where"]) && is_array($params["where"]) && count($params["where"])){
			foreach($params["where"] as $field => $value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])){
			foreach($params["where_in"] as $field => $value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])){
			foreach ($params["where_sql"] as $value){
				$this->DB->where($value);
			}
		}

		if(isset($filter_object_ids) && is_array($filter_object_ids) && count($filter_object_ids)){
			$filter_object_ids = array_unique($filter_object_ids);
			$this->DB->where_in("id", $filter_object_ids);
		}

		if(is_array($order_by) && count($order_by) > 0){
			foreach($order_by as $field => $dir){
				if(in_array($field, $this->fields_all) || $field == "CONCAT(fname, ' ', sname)"){
					$this->DB->order_by($field . " " . $dir);
				}
			}
		}

		if(!is_null($page)){
			$page = intval($page) ? intval($page) : 1;
			$this->DB->limit($items_on_page, $items_on_page*($page-1));
		}
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			$results = ($formatted) ? $this->format_users($results) : $results;
			return $results;
		}
		return false;
	}

	/**
	 * Return users as associated array by id
	 * @param integer $page
	 * @param integer $items_on_page
	 * @param string $order_by
	 * @param array $params
	 * @param array $filter_object_ids
	 * @param boolean $formatted
	 * @return array
	 */
	public function get_users_list_by_key($page=null, $items_on_page=null, $order_by=null, $params=array(), $filter_object_ids=null, $formatted=true){
		$list = $this->get_users_list($page, $items_on_page, $order_by, $params, $filter_object_ids, $formatted);
		if(!empty($list)){
			foreach($list as $l){
				$data[$l["id"]] = $l;
			}
			return $data;
		}else{
			return array();
		}
	}

	/**
	 * Return number of users
	 * @param array $params
	 * @param array $filter_object_ids
	 * @return integer
	 */
	public function get_users_count($params=array(), $filter_object_ids=null){
		$this->DB->select("COUNT(*) AS cnt");
		$this->DB->from(USERS_TABLE);

		if(isset($params["where"]) && is_array($params["where"]) && count($params["where"])){
			foreach($params["where"] as $field => $value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])){
			foreach($params["where_in"] as $field => $value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])){
			foreach($params["where_sql"] as $value){
			    $this->DB->where($value);
			}
		}

		if(isset($filter_object_ids) && is_array($filter_object_ids) && count($filter_object_ids)){
			$this->DB->where_in("id", $filter_object_ids);
		}

		$result = $this->DB->get()->result();
		
		if(!empty($result)){
			return intval($result[0]->cnt);
		}else{
			return 0;
		}
	}
	
	/**
	 * Return list of filtered users
	 * @param array $filters
	 * @param integer $page
	 * @param integer $items_on_page
	 * @param string $order_by
	 * @param boolean $formatted
	 */
	public function get_users_list_by_filters($filters=array(), $page=null, $items_on_page=null, $order_by=null, $formatted=true, $all=false){
		$params = array();
		foreach($filters as $filter=>$value){
			$params = array_merge_recursive($params, $this->{"_get_users_by_".$filter}($value));
		}
		return $this->get_users_list($page, $items_on_page, $order_by, $params, array(), $formatted, $all);
	}
	
	/**
	 * Return number of filtered users
	 * @param array $filters
	 * @return array
	 */
	public function get_users_count_by_filters($filters=array()){
		$params = array();
		foreach($filters as $filter=>$value){
			$params = array_merge_recursive($params, $this->{"_get_users_by_".$filter}($value));
		}
		
		return $this->get_users_count($params);
	}
	
	/**
	 * Return pagination of filtered users
	 * @param array $filters
	 * @param integer $page
	 * @param integer $items_on_page
	 * @param string $order_by
	 */
	public function get_users_pagination($filters=array(), $page=null, $limits=null, $order_by=null, $formatted=true){
		$params = array();
		foreach($filters as $filter=>$value){
			$params = array_merge_recursive($params, $this->{"_get_users_by_".$filter}($value));
		}
		
		$this->DB->select(implode(", ", $this->fields_general));
		$this->DB->from(USERS_TABLE);

		if(isset($params["where"]) && is_array($params["where"]) && count($params["where"])){
			foreach($params["where"] as $field => $value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])){
			foreach($params["where_in"] as $field => $value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])){
			foreach ($params["where_sql"] as $value){
				$this->DB->where($value);
			}
		}

		if(isset($filter_object_ids) && is_array($filter_object_ids) && count($filter_object_ids)){
			$this->DB->where_in("id", $filter_object_ids);
		}

		if(is_array($order_by) && count($order_by) > 0){
			foreach($order_by as $field => $dir){
				//if(in_array($field, $this->fields_general)){
					$this->DB->order_by($field . " " . $dir);
				//}
			}
		}

		if(!is_null($page)){
			$page = intval($page)?intval($page):1;
			$this->DB->limit($limits+($page > 1 ? 4 : 2), $limits*($page-1)-($page > 1 ? 2 : 0));
		}
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			$user_types = array();
			$extended = array();
			foreach($results as $r){
				if( in_array($r["user_type"], $this->type_arr) ){
					$user_types[$r["user_type"]][] = $r["id"];
				}		
			}
			foreach($user_types as $user_type=>$ids){
				$rs = $this->DB->select(implode(", ", $this->fields_general))->from(USERS_TABLE)->where_in("id", $ids)->get()->result_array();
				foreach($rs as $r){			   
					$extended[$r["id"]] = $r;
				}				
			}
			foreach($results as $r){
				$r = isset($extended[$r["id"]]) ? array_merge($r, $extended[$r["id"]]) : $r;
				$data[] = $r;
			}
			if($formatted){
				$data = $this->format_users($data);
			}
			return $data;
		}
		return false;
	}
		
	/**
	 * Return active users as array
	 * @param integer $page
	 * @param integer $items_on_page
	 * @param string $order_by
	 * @param array $params
	 * @param array $filter_object_ids
	 * @param boolean $formatted
	 * @return array
	 */
	public function get_active_users_list($page=null, $items_on_page=null, $order_by=null, $params=array(), $filter_object_ids=null, $formatted=true){
		$params["where"]["status"] = 1;
		$params["where"]["confirm"] = 1;
		return $this->get_users_list($page, $items_on_page, $order_by, $params, $filter_object_ids, $formatted);
	}

	/**
	 * Return number of active users
	 * @param array $params
	 * @param array $filter_object_ids
	 * @return array
	 */
	public function get_active_users_count($params=array(), $filter_object_ids=null){
		$params["where"]["status"] = 1;
		$params["where"]["confirm"] = 1;
		return $this->get_users_count($params, $filter_object_ids);
	}
	
	/**
	 * Return blocked users as array
	 * @param integer $page
	 * @param integer $items_on_page
	 * @param string $order_by
	 * @param array $params
	 * @param array $filter_object_ids
	 * @param boolean $formatted
	 * @return array
	 */
	public function get_not_active_users_list($page=null, $items_on_page=null, $order_by=null, $params=array(), $filter_object_ids=null, $formatted=true){
		$params["where"]["status"] = 0;
		return $this->get_users_list($page, $items_on_page, $order_by, $params, $filter_object_ids, $formatted);
	}

	/**
	 * Return number of blocked users
	 * @param array $params
	 * @param array $filter_object_ids
	 * @return array
	 */
	public function get_not_active_users_count($params=array(), $filter_object_ids=null){
		$params["where"]["status"] = 0;
		return $this->get_users_count($params, $filter_object_ids);
	}
	
	/**
	 * Save user data
	 * @param integer $user_id
	 * @param array $attrs
	 * @param string $file_name
	 * @param boolean $use_icon_moderation
	 * @return integer
	 */
	public function save_user($user_id=null, $attrs=array(), $file_name='', $use_icon_moderation=true){
		if(is_null($user_id)){
			if(!isset($attrs["group_id"])){
				$this->CI->load->model("users/models/Groups_model");
				$attrs["group_id"] = $this->CI->Groups_model->get_default_group_id();
			}
			if(!isset($attrs["date_created"])) $attrs["date_created"] = date("Y-m-d H:i:s");
			if(!isset($attrs["date_modified"])) $attrs["date_modified"] = date("Y-m-d H:i:s");
			$this->DB->insert(USERS_TABLE, $attrs);
			$user_id = $this->DB->insert_id();
		}else{
			if(!isset($attrs["date_modified"])) $attrs["date_modified"] = date("Y-m-d H:i:s");
			$this->DB->where("id", $user_id);
			$this->DB->update(USERS_TABLE, $attrs);
		}
		
		if(!empty($file_name) && !empty($user_id) && !empty($_FILES[$file_name]) && is_uploaded_file($_FILES[$file_name]["tmp_name"])){
			$this->CI->load->model("Uploads_model");
			
			$user = $this->get_user_by_id($user_id);
			if($user['user_logo']){
				$this->delete_logo($user_id);
			}
			
			$img_return = $this->CI->Uploads_model->upload($this->upload_config_id, $user_id."/", $file_name);
			if(empty($img_return["errors"])){
				if($use_icon_moderation){
					$this->CI->load->model("Moderation_model");
					$mstatus = intval($this->CI->Moderation_model->get_moderation_type_status($this->moderation_type));
				}else{
					$mstatus = 2;
				}
				if($mstatus != 2){
					$this->CI->Moderation_model->add_moderation_item($this->moderation_type, $user_id);
					if($mstatus == 1){
						$img_data["user_logo"] = $img_return["file"];
						$img_data["user_logo_moderation"] = "";
					}else{
						$img_data["user_logo_moderation"] = $img_return["file"];
					}
				}else{
					$img_data["user_logo"] = $img_return["file"];
				}
				$this->save_user($user_id, $img_data);
			}
		}
		
		return $user_id;
	}
	
	public function save_agent_company($id_user, $id_company, $approve_wait=true){
		$data['agent'] = $this->get_user_by_id($id_user);

		if($id_company == $data['agent']['agent_company']){
			return false;
		}
	
		$data['company'] = $this->get_user_by_id($id_company);
		
		$save_data = array(
			'agent_company' => $id_company,
			'agent_date' => date('Y-m-d H:i:s'),
			'agent_status' => 0
		);
		$this->save_user($id_user, $save_data);
		
		$this->CI->load->model("Notifications_model");
		if(!empty($data['agent']['agent_company'])){
			$old_company = $this->get_user_by_id($data['agent']['agent_company']);
			$this->update_agent_company_count($data['agent']['agent_company']);
			$mail_data = array(
				'fname' => $old_company['fname'],
				'sname' => $old_company['sname'],
				'user_name' => $data['agent']['output_name'],
				'company_name' => $old_company['output_name'],
			);
			$this->CI->Notifications_model->send_notification($old_company["email"], (($data['agent']['agent_status'])?"agent_unjoin":"agent_cancel_request"), $mail_data);
		}
		if($id_company != 0 && $approve_wait){
			$mail_data = array(
				'fname' => $data['company']['fname'],
				'sname' => $data['company']['sname'],
				'user_name' => $data['agent']['output_name'],
				'company_name' => $data['company']['output_name'],
			);
			$this->CI->Notifications_model->send_notification($data['company']["email"], "agent_send_request", $mail_data);
		}
		
		return true;
	}
	
	public function save_agent_company_status($id_user, $id_company, $status='accept'){
		$data['agent'] = $this->get_user_by_id($id_user);
		$data['company'] = $this->get_user_by_id($id_company);

		$mail_data = array(
			'fname' => $data['agent']['fname'],
			'sname' => $data['agent']['sname'],
			'user_name' => $data['agent']['output_name'],
			'company_name' => $data['company']['output_name'],
		);
		$this->CI->load->model("Notifications_model");
		
		if($status == 'accept'){
			$save_data = array(
				'agent_company' => $id_company,
				'agent_date' => date('Y-m-d H:i:s'),
				'agent_status' => 1
			);		
			$this->save_user($id_user, $save_data);
			$this->update_agent_company_count($id_company);
			
			$this->CI->load->model("Linker_model");
			$this->CI->Linker_model->add_link("users_contacts", $agent_id, $id_company);
			$this->CI->Linker_model->add_link("users_contacts", $id_company, $agent_id);
			
			$this->CI->Notifications_model->send_notification($data['agent']["email"], "agent_request_accepted", $mail_data);
		}elseif($status == 'decline'){
			$save_data = array(
				'agent_company' => 0,
				'agent_date' => date('Y-m-d H:i:s'),
				'agent_status' => 0
			);		
			$this->save_user($id_user, $save_data);	
			$this->CI->Notifications_model->send_notification($data['agent']["email"], "agent_request_declined", $mail_data);
		}elseif($status == 'delete'){
			$save_data = array(
				'agent_company' => 0,
				'agent_date' => date('Y-m-d H:i:s'),
				'agent_status' => 0
			);			
			$this->save_user($id_user, $save_data);
			$this->update_agent_company_count($id_company);
			$this->CI->Notifications_model->send_notification($data['agent']["email"], "agent_deleted", $mail_data);
		}
	}
	
	public function update_agent_company_count($id_company){
		$params["where"]["agent_company"] = $id_company;
		$params["where"]["agent_status"] = 1;
		$save_data['agent_count'] = $this->get_users_count($params);
		$this->save_user($id_company, $save_data);
	}
	
	/**
	 * Remove logo image
	 * @param integer $user_id user identifier
	 */
	public function delete_logo($user_id){
		$this->CI->load->model("Uploads_model");
		
		$user = $this->get_user_by_id($user_id);
		if($user["user_logo_moderation"]){
			$this->CI->Uploads_model->delete_upload($this->upload_config_id, $user_id . "/", $user['user_logo_moderation']);
			$save_data['user_logo_moderation'] = '';
			$this->CI->load->model('Moderation_model');
			$this->CI->Moderation_model->delete_moderation_item_by_obj($this->moderation_type, $user_id);
		}elseif($user["user_logo"]){
			$this->CI->Uploads_model->delete_upload($this->upload_config_id, $user_id . "/", $user["user_logo"]);
			$save_data["user_logo"] = "";
		}
		$this->save_user($user_id, $save_data);
	}

	/**
	 * Activate user
	 * @param integer $user_id
	 * @param integer $status
	 */
	public function activate_user($user_id, $status=1){
		$attrs["status"] = intval($status);
		$attrs["date_modified"] = date("Y-m-d H:i:s");
		$this->DB->where("id", $user_id);
		$this->DB->update(USERS_TABLE, $attrs);
	}

	private function _create_unique_gid($id, $data){
		$scheme = $this->user_gid_scheme[$data['user_type']]['str'];

		$this->CI->load->library('Translit');
		$current_lang = $this->CI->pg_language->get_lang_by_id($this->CI->pg_language->current_lang_id);
		$current_lang_code = $current_lang['code'];

		///[fname]
		if($data["fname"]){
			$name_replace = strtolower(strip_tags($data["fname"]));
			$name_replace = $this->CI->translit->convert($current_lang_code, $name_replace);
			$name_replace = preg_replace("/[\n\s\t]+/i", '-', $name_replace);
			$name_replace = preg_replace("/[^a-z0-9\-_]+/i", '', $name_replace);
			$name_replace = preg_replace("/[\-]{2,}/i", '-', $name_replace);
			$name_replace = ucfirst($name_replace);
		}else{
			$name_replace = "";
		}
		$scheme = str_replace("[fname]", $name_replace, $scheme);

		///[sname]
		if($data["sname"]){
			$name_replace = strtolower(strip_tags($data["sname"]));
			$name_replace = $this->CI->translit->convert($current_lang_code, $name_replace);
			$name_replace = preg_replace("/[\n\s\t]+/i", '-', $name_replace);
			$name_replace = preg_replace("/[^a-z0-9\-_]+/i", '', $name_replace);
			$name_replace = preg_replace("/[\-]{2,}/i", '-', $name_replace);
			$name_replace = ucfirst($name_replace);
		}else{
			$name_replace = "";
		}
		$scheme = str_replace("[sname]", $name_replace, $scheme);

		///[company_name]
		if($data["company_name"]){
			$name_replace = strtolower(strip_tags($data["company_name"]));
			$name_replace = $this->CI->translit->convert($current_lang_code, $name_replace);
			$name_replace = preg_replace("/[\n\s\t]+/i", '-', $name_replace);
			$name_replace = preg_replace("/[^a-z0-9\-_]+/i", '', $name_replace);
			$name_replace = preg_replace("/[\-]{2,}/i", '-', $name_replace);
			$name_replace = ucfirst($name_replace);
		}else{
			$name_replace = "";
		}
		$scheme = str_replace("[company_name]", $name_replace, $scheme);

		$param["where"]["unique_name"] = $scheme;
		if(!empty($id)){
			$param["where"]["id !="] = $id;
		}
		$gid_counts = $this->get_users_count($param);
		if($gid_counts > 0){
			if(strlen($scheme) > 96){
				$scheme = substr($scheme, 0, 96);
			}
			$scheme = $scheme . "-". rand(100, 999);
		}
		return $scheme;
	}
	/**
	 * Validate site mode settings
	 * @param array $data
	 * @return array
	 */
	public function validate_settings($data){
		$return = array("errors"=> array(), "data" => array());
		
		$fields = array();
		
		foreach($this->type_arr as $user_type){
			if(isset($data[$user_type.'_login_enabled'])){
				$return["data"][$user_type.'_login_enabled'] = intval($data[$user_type.'_login_enabled']) ? 1 : 0;
				if(!$return["data"][$user_type.'_login_enabled']){
				    $data[$user_type.'_register_enabled'] = 0;
				    $data[$user_type.'_register_mail_confirm'] = 0;
				}
			}
			if(isset($data[$user_type.'_register_enabled'])){
				$return["data"][$user_type.'_register_enabled'] = intval($data[$user_type.'_register_enabled']) ? 1 : 0;
				if($return["data"][$user_type.'_register_enabled'] == 1 && !isset($return["data"][$user_type.'_login_enabled'])){
				    $return["data"][$user_type.'_login_enabled'] = 1;
				}
			}
			if(isset($data[$user_type.'_register_mail_confirm'])){
				$return["data"][$user_type.'_register_mail_confirm'] = intval($data[$user_type.'_register_mail_confirm']) ? 1 : 0;
				if($return["data"][$user_type.'_register_mail_confirm'] == 1 && !isset($return["data"][$user_type.'_login_enabled'])){
				    $return["data"][$user_type.'_login_enabled'] = 1;
				}
				if($return["data"][$user_type.'_register_mail_confirm'] == 1 && !isset($return["data"][$user_type.'_register_enabled'])){
				    $return["data"][$user_type.'_register_enabled'] = 1;
				}
			}
			if(isset($data[$user_type.'_search_enabled'])){
			    $return["data"][$user_type.'_search_enabled'] = intval($data[$user_type.'_search_enabled']) ? 1 : 0;
			}
		}
		
		return $return;
	}
	
	/**
	 * Validate user data
	 * @param array $data
	 * @param boolean $required
	 * @return array
	 */
	public function validate_user($id, $data, $file_name=''){
		$return = array("errors"=>array(), "data"=>array());
		
		if($id){
			$this->set_format_settings('use_format', false);
			$user = $this->get_user_by_id($id);
			$this->set_format_settings('use_format', true);
		}else{
			$user = array();
		}

		$this->CI->config->load("reg_exps", TRUE);

		/// user type
		if(isset($data['user_type'])){
			$return["data"]["user_type"] = trim(strip_tags($data["user_type"])); # result return as private, agent, company
			if(empty($return["data"]["user_type"]) || !in_array($return["data"]["user_type"], $this->type_arr)){
				$return["errors"][] = l("error_user_type_incorrect", "users");
			}else{
				$this->CI->load->model("moderation/models/Moderation_badwords_model");
				$bw_count = $this->CI->Moderation_badwords_model->check_badwords($this->moderation_type_text, $return['data']['user_type']);
				if($bw_count){
					$return['errors'][] = str_replace('[field]', l('field_user_type', 'users'), l("error_badwords_field", "users"));
				}else{
					$user['user_type'] = $return['data']['user_type'];
				}
			}
		}elseif(!$id){
			$return["errors"][] = l("error_user_type_incorrect", "users");
		}
		
		if(isset($data["confirm"])){
			$return["data"]["confirm"] = intval($data["confirm"]);
		}
		
		if(isset($data['confirm_code'])){
			$return["data"]["confirm_code"] = trim(strip_tags($data["confirm_code"]));
		}
		
		if(isset($data["status"])){
			$return["data"]["status"] = intval($data["status"]);
		}

		if(isset($data["lang_id"])){
			$return["data"]["lang_id"] = intval($data["lang_id"]);
		}
		
		if(isset($data["id_currency"])){
			$return["data"]["id_currency"] = intval($data["id_currency"]);
		}
		
		if(isset($data["group_id"])){
			$return["data"]["group_id"] = intval($data["group_id"]);
		}

		/// email
		if(isset($data['email'])){
			$return["data"]["email"] = trim(strip_tags($data["email"]));

			$email_expr = $this->CI->config->item("email", "reg_exps");
			if(empty($return["data"]["email"]) || !preg_match($email_expr, $return["data"]["email"])){
				$return["errors"][] = l("error_email_incorrect", "users");
			}else{
				$this->CI->load->model("moderation/models/Moderation_badwords_model");
				$bw_count = $this->CI->Moderation_badwords_model->check_badwords($this->moderation_type_text, $return['data']['email']);
				if($bw_count) $return['errors'][] = str_replace('[field]', l('field_email', 'users'), l("error_badwords_field", "users"));
			}

			$eparams["where"]["email"] = $return["data"]["email"];
			if($id) $eparams["where"]["id <>"] = $id;
			$count = $this->get_users_count($eparams);
			if($count > 0) $return["errors"][] = l("error_email_already_exists", "users");
		}elseif(!$id){
			$return["errors"][] = l("error_empty_email", "users");
		}
		/*#MOD# Inject email to contact_email
 		if(isset($data['email'])){
			$return["data"]["contact_email"] = trim(strip_tags($data['email']));
			$email_expr = $this->CI->config->item("email", "reg_exps");
			if(!empty($return["data"]["contact_email"]) && !preg_match($email_expr, $return["data"]["contact_email"])){
				$return["errors"][] = l("error_contact_email_incorrect", "users");
			}else{
				$this->CI->load->model("moderation/models/Moderation_badwords_model");
				$bw_count = $this->CI->Moderation_badwords_model->check_badwords($this->moderation_type_text, $return['data']['contact_email']);
				if($bw_count) $return['errors'][] = str_replace('[field]', l('field_contact_email', 'users'), l("error_badwords_field", "users"));
			}
		}*/
		
		
		/// fname
		if(isset($data['fname'])){
			$return["data"]["fname"] = trim(strip_tags($data["fname"]));
			if(empty($return["data"]["fname"])){
				$return["errors"][] = l("error_empty_fname", "users");
			}else{
				$this->CI->load->model("moderation/models/Moderation_badwords_model");
				$bw_count = $this->CI->Moderation_badwords_model->check_badwords($this->moderation_type_text, $return['data']['fname']);
				if($bw_count) $return['errors'][] = str_replace('[field]', l('field_fname', 'users'), l("error_badwords_field", "users"));
			}
		}elseif(!$id){
			$return["errors"][] = l("error_empty_fname", "users");
		}

		/// sname
		if(isset($data['sname'])){
			$return["data"]["sname"] = trim(strip_tags($data["sname"]));
			/*if(empty($return["data"]["sname"])){
				$return["errors"][] = l("error_empty_sname", "users"); #MOD# Disable required last name 	
			}else{*/
				$this->CI->load->model("moderation/models/Moderation_badwords_model");
				$bw_count = $this->CI->Moderation_badwords_model->check_badwords($this->moderation_type_text, $return['data']['sname']);
				if($bw_count) $return['errors'][] = str_replace('[field]', l('field_sname', 'users'), l("error_badwords_field", "users"));
			//}
		}elseif(!$id){
			$return["errors"][] = l("error_empty_sname", "users");
		}

		/// phone
		if(isset($data['phone'])){
			$return["data"]["phone"] = trim(strip_tags($data["phone"]));
			
			if(empty($return["data"]["phone"])){
				$return["errors"][] = l("error_empty_phone", "users");
			}else{
				$this->CI->load->helper("start");
				$phone_format = get_phone_format('regexp');
				if($phone_format !== false && !preg_match($phone_format, $return["data"]["phone"])){
					$return["errors"][] = l("error_phone_incorrect", "users");
				}else{
					$this->CI->load->model("moderation/models/Moderation_badwords_model");
					$bw_count = $this->CI->Moderation_badwords_model->check_badwords($this->moderation_type_text, $return['data']['phone']);
					if($bw_count) $return['errors'][] = str_replace('[field]', l('field_phone', 'users'), l("error_badwords_field", "users"));
				}
			}
		}elseif(!$id){
			$return["errors"][] = l("error_empty_phone", "users");
		}
		/*#MOD# Inject phone to contact_phone
		if(isset($data['phone'])){
			$return["data"]["contact_phone"] = trim(strip_tags($data['phone']));
			if(!empty($return["data"]["contact_phone"])){
				$this->CI->load->helper("start");
				$phone_format = get_phone_format('regexp');
				if($phone_format !== false && !preg_match($phone_format, $return["data"]["contact_phone"])){
					$return["errors"][] = l("error_contact_phone_incorrect", "users");
				}else{
					$this->CI->load->model("moderation/models/Moderation_badwords_model");
				$bw_count = $this->CI->Moderation_badwords_model->check_badwords($this->moderation_type_text, $return['data']['contact_phone']);
				if($bw_count) $return['errors'][] = str_replace('[field]', l('field_contact_phone', 'users'), l("error_badwords_field", "users"));
				}
			}
		}*/
		/// password
		if(isset($data['password'])){
			$return["data"]["password"] = trim($data["password"]);
			$data["password"] = trim(strip_tags($data["password"]));
			
			if($return["data"]["password"] != $data["repassword"]){
				$return["errors"][] = l("error_pass_repass_not_equal", "users");
			}else{
				$password_expr = $this->CI->config->item("password", "reg_exps");
				if(!preg_match($password_expr, $return["data"]["password"])){
					$return["errors"][] = l("error_password_incorrect", "users");
				}
				$return["data"]["password"] = $this->encode_password($return["data"]["password"]);
			}
		}elseif(!$id){
			$return["errors"][] = l("error_password_empty", "users");
		}		
		
		if(isset($data["password_old"]) && $id){
			$data["password_old"] = $this->encode_password(trim($data["password_old"]));
			$userdata = $this->get_user_by_id($id);
			if($data["password_old"] != $userdata['password']){
				$return["errors"][] = l("error_password_old_invalid", "users");
			}
		}
		
		//// logo
		if(isset($data["user_logo"])){
			$return["data"]["user_logo"] = strip_tags($data["user_logo"]);
		}

		if(isset($data["user_logo_moderation"])){
			$return["data"]["user_logo_moderation"] = strip_tags($data["user_logo_moderation"]);
		}
		
		/*if(isset($return["data"]["user_open_id"])){
			$return["data"]["user_open_id"] = trim($return["data"]["user_open_id"]);
		}

		if(isset($return["data"]["group_id"])){
			$return["data"]["group_id"] = intval($return["data"]["group_id"]);
		}*/

		if(isset($data["contact_info"])){
			$return["data"]["contact_info"] = trim(strip_tags($data["contact_info"]));
			if(!empty($return["data"]["contact_info"])){
				$this->CI->load->model("moderation/models/Moderation_badwords_model");
				$bw_count = $this->CI->Moderation_badwords_model->check_badwords($this->moderation_type_text, $return['data']['contact_info']);
				if($bw_count) $return['errors'][] = str_replace('[field]', l('field_contact_info', 'users'), l("error_badwords_field", "users"));
			}
		}

		if(isset($data["contact_email"])){
			$return["data"]["contact_email"] = trim(strip_tags($data["contact_email"]));
			$email_expr = $this->CI->config->item("email", "reg_exps");
			if(!empty($return["data"]["contact_email"]) && !preg_match($email_expr, $return["data"]["contact_email"])){
				$return["errors"][] = l("error_contact_email_incorrect", "users");
			}else{
				$this->CI->load->model("moderation/models/Moderation_badwords_model");
				$bw_count = $this->CI->Moderation_badwords_model->check_badwords($this->moderation_type_text, $return['data']['contact_email']);
				if($bw_count) $return['errors'][] = str_replace('[field]', l('field_contact_email', 'users'), l("error_badwords_field", "users"));
			}
		}

		if(isset($data["contact_phone"])){
			$return["data"]["contact_phone"] = trim(strip_tags($data["contact_phone"]));
			if(!empty($return["data"]["contact_phone"])){
				$this->CI->load->helper("start");
				$phone_format = get_phone_format('regexp');
				if($phone_format !== false && !preg_match($phone_format, $return["data"]["contact_phone"])){
					$return["errors"][] = l("error_contact_phone_incorrect", "users");
				}else{
					$this->CI->load->model("moderation/models/Moderation_badwords_model");
				$bw_count = $this->CI->Moderation_badwords_model->check_badwords($this->moderation_type_text, $return['data']['contact_phone']);
				if($bw_count) $return['errors'][] = str_replace('[field]', l('field_contact_phone', 'users'), l("error_badwords_field", "users"));
				}
			}
		}
		
		if(isset($data["account"])){
			$return["data"]["account"] = intval($data["account"]);
		}
		
		if(isset($data["twitter"])){
			$return["data"]["twitter"] = trim(strip_tags($data["twitter"]));
			if(!empty($return["data"]["twitter"])){
				$url_expr = $this->CI->config->item("url", "reg_exps");
				if(!preg_match($url_expr, $return["data"]["twitter"])){
					$return["errors"][] = l("error_twitter_incorrect", "users");
				}else{
					$this->CI->load->model("moderation/models/Moderation_badwords_model");
					$bw_count = $this->CI->Moderation_badwords_model->check_badwords($this->moderation_type_text, $return['data']['twitter']);
					if($bw_count) $return['errors'][] = str_replace('[field]', l('field_twitter', 'users'), l("error_badwords_field", "users"));
				}
			}	
		}

		if(isset($data["facebook"])){
			$return["data"]["facebook"] = trim(strip_tags($data["facebook"]));
			if(!empty($return["data"]["facebook"])){
				$url_expr = $this->CI->config->item("url", "reg_exps");
				if(!preg_match($url_expr, $return["data"]["facebook"])){
					$return["errors"][] = l("error_facebook_incorrect", "users");
				}else{
					$this->CI->load->model("moderation/models/Moderation_badwords_model");
					$bw_count = $this->CI->Moderation_badwords_model->check_badwords($this->moderation_type_text, $return['data']['facebook']);
					if($bw_count) $return['errors'][] = str_replace('[field]', l('field_facebook', 'users'), l("error_badwords_field", "users"));
				}
			}	
		}

		if(isset($data["vkontakte"])){
			$return["data"]["vkontakte"] = trim(strip_tags($data["vkontakte"]));
			if(!empty($return["data"]["vkontakte"])){
				$url_expr = $this->CI->config->item("url", "reg_exps");
				if(!preg_match($url_expr, $return["data"]["vkontakte"])){
					$return["errors"][] = l("error_vkontakte_incorrect", "users");
				}else{
					$this->CI->load->model("moderation/models/Moderation_badwords_model");
					$bw_count = $this->CI->Moderation_badwords_model->check_badwords($this->moderation_type_text, $return['data']['vkontakte']);
					if($bw_count) $return['errors'][] = str_replace('[field]', l('field_vkontakte', 'users'), l("error_badwords_field", "users"));
				}
			}	
		}
		
		if(isset($data['featured_end_date'])){
			$value = strtotime($data['featured_end_date']);
			if($value > 0){
				$return['data']['featured_end_date'] = date("Y-m-d", $value);
			}else{
				$return['data']['featured_end_date'] = '0000-00-00 00:00:00';
			}
		}
		
		if(isset($data['show_logo_end_date'])){
			$value = strtotime($data['show_logo_end_date']);
			if($value > 0){
				$return['data']['show_logo_end_date'] = date("Y-m-d", $value);
			}else{
				$return['data']['show_logo_end_date'] = '0000-00-00 00:00:00';
			}
		}

		switch($user['user_type']){
			case 'private':
				/// standart contact fields
			break;
			case 'company':
				/// company name (required)
				if(isset($data['company_name'])){
					$return["data"]["company_name"] = trim(strip_tags($data["company_name"]));
					if(empty($return["data"]["company_name"])){
						$return["errors"][] = l("error_empty_company_name", "users");
					}else{
						$this->CI->load->model("moderation/models/Moderation_badwords_model");
						$bw_count = $this->CI->Moderation_badwords_model->check_badwords($this->moderation_type_text, $return['data']['company_name']);
						if($bw_count) $return['errors'][] = str_replace('[field]', l('field_company_name', 'users'), l("error_badwords_field", "users"));
					}
				}elseif(!$id){
					$return["errors"][] = l("error_empty_company_name", "users");
				}		
			
				/// contacts	
				if(isset($data["web_url"])){
					$return["data"]["web_url"] = trim(strip_tags($data["web_url"]));
					if(!empty($return["data"]["web_url"])){
						$url_expr = $this->CI->config->item("url", "reg_exps");
						if(!preg_match($url_expr, $return["data"]["web_url"])){
							$return["errors"][] = l("error_web_url_incorrect", "users");
						}else{
							$this->CI->load->model("moderation/models/Moderation_badwords_model");
							$bw_count = $this->CI->Moderation_badwords_model->check_badwords($this->moderation_type_text, $return['data']['web_url']);
							if($bw_count) $return['errors'][] = str_replace('[field]', l('field_web_url', 'users'), l("error_badwords_field", "users"));
						}
					}
				}
	
				if(isset($data["working_days"])){		
					if(is_array($data['working_days']) && !empty($data['working_days'])){	
						$return['data']['working_days'] = implode(',', array_intersect($data['working_days'], array(1,2,3,4,5,6,7)));
					}else{
						$return['data']['working_days'] = '';
					}
				}
		
				if(isset($data["working_hours_begin"])){
					$return['data']['working_hours_begin'] = intval($data['working_hours_begin']);
				}
			
				if(isset($data["working_hours_end"])){
					$return['data']['working_hours_end'] = intval($data['working_hours_end']);
				}

				if(isset($data["lunch_time_begin"])){
					$return['data']['lunch_time_begin'] = intval($data['lunch_time_begin']);
				}

				if(isset($data["lunch_time_end"])){
					$return['data']['lunch_time_end'] = intval($data['lunch_time_end']);
				}

				if(isset($data["id_country"])){
					$return["data"]["id_country"] = strval($data["id_country"]);
				}
	
				if(isset($data["id_region"])){
					$return["data"]["id_region"] = intval($data["id_region"]);
				}
	
				if(isset($data["id_city"])){
					$return["data"]["id_city"] = intval($data["id_city"]);
				}
			
				if(isset($data["address"])){
					$return["data"]["address"] = trim(strip_tags($data["address"]));
					if(!empty($return["data"]["address"])){
						$this->CI->load->model("moderation/models/Moderation_badwords_model");
						$bw_count = $this->CI->Moderation_badwords_model->check_badwords($this->moderation_type_text, $return['data']['address']);
						if($bw_count) $return['errors'][] = str_replace('[field]', l('field_address', 'users'), l("error_badwords_field", "users"));
					}
				}
	
				if(isset($data["postal_code"])){
					$return["data"]["postal_code"] = trim(strip_tags($data["postal_code"]));
					if(!empty($return["data"]["postal_code"])){
						$this->CI->load->model("moderation/models/Moderation_badwords_model");
						$bw_count = $this->CI->Moderation_badwords_model->check_badwords($this->moderation_type_text, $return['data']['postal_code']);
						if($bw_count) $return['errors'][] = str_replace('[field]', l('field_postal_code', 'users'), l("error_badwords_field", "users"));
					}
				}
	
				if(isset($data["lat"])){
					$return["data"]["lat"] = (float)$data["lat"];
				}
	
				if(isset($data["lon"])){
					$return["data"]["lon"] = (float)$data["lon"];
				}
				
				if(isset($data["agent_count"])){
					$return["data"]["agent_count"] = intval($data["agent_count"]);
				}
			break;
			case 'agent':
				if(isset($data["agent_company"])){
					$return["data"]["agent_company"] = intval($data["agent_company"]);
					if(empty($return["data"]["agent_company"])){
						$return["errors"][] = l("error_empty_agent_company", "users");
					}
				}
		
				if(isset($data["agent_status"])){
					$return["data"]["agent_status"] = $data["agent_status"] ? 1 : 0;
				}
			break;
		}
		
		if(isset($data['date_created'])){
			$value = strtotime($data['date_created']);
			if($value > 0){
				$return['data']['date_created'] = date('Y-m-d', $value);
			}else{
				$return['data']['date_created'] = '0000-00-00 00:00:00';
			}
		}
		
		if(isset($data['date_modified'])){
			$value = strtotime($data['date_modified']);
			if($value > 0){
				$return['data']['date_modified'] = date('Y-m-d', $value);
			}else{
				$return['data']['date_modified'] = '0000-00-00 00:00:00';
			}
		}
		
		//// create unique_name
		$allow_create_unique_name = false;
		if($return['data']['user_type']){
			$allow_create_unique_name = true;
			if(isset($this->user_gid_scheme[$return['data']['user_type']]['fields'])){
				foreach($this->user_gid_scheme[$return['data']['user_type']]['fields'] as $scheme_field){
					if(!isset($return["data"][$scheme_field])) $allow_create_unique_name = false;
				}
			}else{
				$allow_create_unique_name = false;
			}
		}
		if($allow_create_unique_name){
			$return["data"]["unique_name"] = $this->_create_unique_gid($id, $return["data"]);
		}elseif(!$id){
			$return["errors"][] = l("error_empty_unique_name", "users");
		}
		
		if(isset($data["unique_name"])){
			$return["data"]["unique_name"] = trim(strip_tags($data["unique_name"]));
			if(empty($return["data"]["unique_name"])){
				$return["errors"][] = l("error_empty_unique_name", "users");
			}
		}
		
		//// validate logo
		if($file_name && isset($_FILES[$file_name]) && is_array($_FILES[$file_name]) && is_uploaded_file($_FILES[$file_name]["tmp_name"])){
			$this->CI->load->model("Uploads_model");
			$validate_image = $this->CI->Uploads_model->validate_upload($this->upload_config_id, $file_name);
			if(!empty($validate_image["error"])){
				$return["errors"] = array_merge($return["errors"], (array)$validate_image["error"]);
			}
		}

		return $return;
	}

	/**
	 * Remove user
	 * Listings won't be deleted
	 * @param int $user_id
	 */
	public function delete_user($user_id){
		$data = $this->get_user_by_id($user_id);
		$this->DB->where("id", $user_id);
		$this->DB->delete(USERS_TABLE);

		//update agents
		if($data['user_type'] == 'agent' && $data['agent_status']){
			$this->update_agent_company_count($data['agent_company']);
		}

		/// delete uploads
		$this->CI->load->model("Uploads_model");
		if($data["user_logo_moderation"]){
			$this->CI->Uploads_model->delete_upload($this->upload_config_id, $user_id."/", $data["user_logo_moderation"]);
		}
		if($data["user_logo"]){
			$this->CI->Uploads_model->delete_upload($this->upload_config_id, $user_id."/", $data["user_logo"]);
		}

		/// delete moderation
		$this->CI->load->model("Moderation_model");
		$this->CI->Moderation_model->delete_moderation_item_by_obj($this->moderation_type, $user_id);

		/// delete links
		$this->delete_user_contacts($user_id);
	}
	
	/**
	 * Validate user logo
	 * @param string $file_name file name
	 */
	public function validate_logo($file_name){
		$return = array('errors' => array(), 'data' => array());
		if(isset($_FILES[$file_name]) && is_array($_FILES[$file_name]) && is_uploaded_file($_FILES[$file_name]["tmp_name"])){
			$this->load->model("Uploads_model");
			$validate_image = $this->CI->Uploads_model->validate_upload($this->upload_config_id, $file_name);
			$return["errors"] = $validate_image["error"];
		}
		return $return;
	}

	/**
	 * Format users data
	 * @param array $data
	 * @return array
	 */
	public function format_users($data){
		if(!$this->format_settings['use_format']) return $data;
		
		$user_for_location = array();

		$reference = $this->CI->pg_language->ds->get_reference('start', 'weekday-names');
		$weekdays = $reference['option'];
		$reference = $this->CI->pg_language->ds->get_reference('start', 'dayhour-names');
		$dayhours = $reference['option'];
		
		$group_ids = array();
		$company_ids = array();
		$language_ids = array();
		$currency_ids = array();
		
		if($this->format_settings['get_currency']){
			if(!$this->CI->pg_module->is_module_installed('payments')){
				$this->format_settings['get_currency'] = false;
			}
		}
		
		$this->CI->load->model('Uploads_model');
		
		foreach($data as $key=>$user){
			//// postfix for images path
			if(!empty($user['id'])){
				$user['postfix'] = $user["id"];
			}
			
			$user['user_type_str'] = l($user['user_type'], 'users');

			if($user['user_type'] == 'company'){
				$user['output_name'] = $user['company_name'];
			}else{
				$user['output_name'] = trim($user['fname'].' '.$user['sname']);
			}
			$user['name'] = $user['output_name'];

			$user['media']['default_logo'] = $this->CI->Uploads_model->format_default_upload($this->upload_config_id);
			if(!empty($user['user_logo'])){
				$user['media']['user_logo'] = $this->CI->Uploads_model->format_upload($this->upload_config_id, $user["postfix"], $user["user_logo"]);
			}else{
				$user['media']['user_logo'] = $user['media']['default_logo'];
			}

			if(!empty($user['user_logo_moderation'])){
				$user['media']['user_logo_moderation'] = $this->CI->Uploads_model->format_upload($this->upload_config_id, $user["postfix"], $user["user_logo_moderation"]);
			}else{
				$user['media']['user_logo_moderation'] = $this->CI->Uploads_model->format_default_upload($this->upload_config_id);
			}
			
			if($user['status']){
				$user['status_output'] = l('active_user', 'users');
			}else{
				$user['status_output'] = l('inactive_user', 'users');
			}

			switch($user['user_type']){
				case 'private':
					$user['is_map'] = 0; 
				break;
				case 'company':
					if(!empty($user["working_days"]) && !empty($user['working_days'])){
						$user['working_days'] = explode(',', $user['working_days']);
						$user['working_days_str'] = implode(', ', array_intersect_key($weekdays, array_flip($user['working_days'])));
					}
					
					if(!empty($user['working_hours_begin'])){
						$user['working_hours_begin_text'] = $dayhours[$user['working_hours_begin']];
					}else{
						$user['working_hours_begin_text'] = '';
					}
					if(!empty($user['working_hours_end'])){
						$user['working_hours_end_text'] = $dayhours[$user['working_hours_end']];
					}else{
						$user['working_hours_end_text'] = '';
					}
					if(!empty($user['working_hours_begin']) && !empty($user['working_hours_end'])){
						$user['working_hours_str'] = $dayhours[$user['working_hours_begin']].' - '.$dayhours[$user['working_hours_end']];
					}
					
					if(!empty($user['lunch_time_begin'])){
						$user['lunch_time_begin_text'] = $dayhours[$user['lunch_time_begin']];
					}else{
						$user['lunch_time_begin_text'] = '';
					}
					if(!empty($user['lunch_time_end'])){
						$user['lunch_time_end_text'] = $dayhours[$user['lunch_time_end']];
					}else{
						$user['lunch_time_end_text'] = '';
					}
					if(!empty($user['lunch_time_begin']) && !empty($user['lunch_time_end'])){
						$user['lunch_time_str'] = $dayhours[$user['lunch_time_begin']].' - '.$dayhours[$user['lunch_time_end']];
					}
					
					$user['is_map'] = !((float)$user['lat']==0 && (float)$user['lon']==0);
					
					$user_for_location[$user['id']] = array($user['id_country'], $user['id_region'], $user['id_city'], $user['address'], $user['postal_code']);
				break;
				case 'agent':
					if($user['agent_company']){
						$company_ids[] = $user['agent_company'];
						if(!$user['agent_status']) $user['agent_status_str'] = l('agent_status_not_approved', 'users');
					}
					$user['is_map'] = 0;
				break;
			}

			$user['is_featured'] = (strtotime($user['featured_end_date']) > time())?true:false;
			$user['is_show_logo'] = (strtotime($user['show_logo_end_date']) > time())?true:false;

			if(isset($user['review_sorter'])) $user['review_sorter'] = round($user['review_sorter'], 1);

			if($this->format_settings['get_language']){
				if($user['lang_id']) $language_ids[] = $user['lang_id'];
			}
			
			if($this->format_settings['get_currency']){
				if($user['id_currency']) $currency_ids[] = $user['id_currency'];
			}
			
			if($this->format_settings['get_group']){
				$group_ids[] = $user['group_id'];
			}

			if($this->format_settings['get_safe']){
				foreach($this->_safe_fields as $field_name){
					unset($user[$field_name]);
				}
			}
	
			$data[$key] = $user;
		}

		if(!empty($user_for_location)){			
			$this->CI->load->helper('countries');
		
			$user_locations = address_output_format($user_for_location);			
			$user_locations_data = get_location_data($user_for_location);
			$lang = $this->CI->pg_language->get_lang_by_id($this->CI->pg_language->current_lang_id);	
		
			foreach($data as $key=>$user){
				$data[$key]['country'] = (isset($user_locations_data["country"][$user["id_country"]])) ? $user_locations_data["country"][$user["id_country"]]["name"] : "";
				$data[$key]['region'] = (isset($user_locations_data["region"][$user["id_region"]])) ? $user_locations_data["region"][$user["id_region"]]["name"] : "";
				$data[$key]['region_code'] = (isset($user_locations_data["region"][$user["id_region"]])) ? $user_locations_data["region"][$user["id_region"]]["code"] : "";
				$data[$key]['city'] = (isset($user_locations_data["city"][$user["id_city"]])) ? $user_locations_data["city"][$user["id_city"]]["name"] : "";
				$data[$key]['location'] = $user_locations[$user["id"]];
			}
		}
		
		if($this->format_settings['get_contact']){
			if($this->CI->session->userdata('auth_type') == 'user'){
				$user_id = $this->CI->session->userdata('user_id');
				$user_type = $this->CI->session->userdata('user_type');
			}else{
				$user_id = 0;
				$user_type = null;
			}
		
			if($this->CI->pg_module->is_module_installed('users_services')){
				$this->CI->load->model('Users_services_model');
				$is_contact_services_active = $this->CI->Users_services_model->is_active_contact_services($user_type);
			}else{
				$is_contact_services_active = true;
			}
			
			$this->CI->load->helper('users');
			$is_login_enabled = is_login_enabled();
			
			foreach($data as $key=>$user){
				$user['no_access_contact'] = !$is_login_enabled && $is_contact_services_active;
				if($user['no_access_contact']){
					foreach($this->_contact_fields as $field_name){
						unset($user[$field_name]);
					}
					$user['is_contact'] = false;
				}elseif($is_contact_services_active){
					if($user_id != $user['id']){
						$user['is_contact'] = $this->CI->Users_services_model->is_available_contact($user_id, $user['id']);
						if(!$user['is_contact']){
							foreach($this->_contact_fields as $field_name){
								if(!empty($user[$field_name])) $user[$field_name] = l('please_buy', 'users');
							}
						}
						$user['is_company'] = $this->CI->Users_services_model->is_available_contact($user_id, $user['agent_company']);
					}else{
						$user['is_contact'] = true;
					}
				}else{
					$user['is_contact'] = true;
				}
				$data[$key] = $user;
			}
		}
		
		if($this->format_settings['get_group'] && !empty($group_ids)){
			$groups_by_id = array();
			$this->CI->load->model('users/models/Groups_model');
			$groups = $this->CI->Groups_model->get_groups_list(null, null, null, array('where_in'=>array('id'=>$group_ids)));
			foreach($groups as $group){
				$groups_by_id[$group['id']] = $group;
			}
			foreach($data as $key=>$user){
				$user['group_output'] = isset($groups_by_id[$user['group_id']]) ? $groups_by_id[$user['group_id']]['group_name'] : '';
				$data[$key] = $user;
			}
		}
		
		if($this->format_settings['get_company'] && !empty($company_ids)){
			$companies = $this->get_users_list_by_key(null, null, null, array(), $company_ids);
			foreach($data as $key=>$user){
				if($user['user_type'] != 'agent' || !$user['agent_company']) continue;
				$user['company'] = isset($companies[$user['agent_company']]) ? $companies[$user['agent_company']] : $this->format_default_user($user['agent_company']);
				$data[$key] = $user;
			}
		}
		
		if($this->format_settings['get_language'] && !empty($language_ids)){
			$languages = $this->CI->pg_language->return_langs();
			foreach($data as $key=>$user){
				if(isset($languages[$user['lang_id']])){
					$user['language'] = $languages[$user['lang_id']];
					$user['language_output'] = $user['language']['name'];
				}else{
					$user['language_output'] = '';
				}
				$data[$key] = $user;
			}
		}
		
		if($this->format_settings['get_currency'] && !empty($currency_ids)){
			$this->CI->load->model('payments/models/Payment_currency_model');
			$currencies = $this->CI->Payment_currency_model->return_currencies();
			foreach($data as $key=>$user){
				if(isset($currencies[$user['id_currency']])){
					$user['currency'] = $currencies[$user['id_currency']];
					$user['currency_output'] = $user['currency']['name'];
				}else{
					$user['currency_output'] = '';
				}
				$data[$key] = $user;
			}
		}
		
		return $data;
	}

	/**
	 * Format default user
	 * @param integer $id
	 * @return array
	 */
	public function format_default_user($id){
		$this->CI->load->model("Uploads_model");
		$data["postfix"] = $id;
		$data["output_name"] = "User is deleted";
		$data["media"]["user_logo"] = $this->CI->Uploads_model->format_default_upload($this->upload_config_id);
		$data["media"]["default_logo"] = $this->CI->Uploads_model->format_default_upload($this->upload_config_id);
		return $data;
	}

	/**
	 * Format user data
	 * @param array $data
	 * @return array
	 */
	public function format_user($data){
		$return = $this->format_users(array(0=>$data));
		return $return[0];
	}

	/**
	 * Return settings for seo
	 * @param string $method
	 * @param integer $lang_id 
	 * @return array
	 */
	public function get_seo_settings($method="", $lang_id=""){
		if(!empty($method)){
			return $this->_get_seo_settings($method, $lang_id);
		}else{
			$actions = array("account", "login_form", "profile", "view", "restore", 'index', 'search');
			foreach($this->type_arr as $user_type){
				$actions[] = "reg_".$user_type;
			}
			$return = array();
			foreach($actions as $action){
				$return[$action] = $this->_get_seo_settings($action, $lang_id);
			}
			return $return;
		}
	}

	/**
	 * Return setting for seo (internal)
	 * @return array
	 */
	private function _get_seo_settings($method, $lang_id=""){
		switch($method){
			case "account":
				return array(
					"title" => l("seo_tags_account_title", "users", $lang_id, "seo"),
					"keyword" => l("seo_tags_account_keyword", "users", $lang_id, "seo"),
					"description" => l("seo_tags_account_description", "users", $lang_id, "seo"),
					"templates" => array("unique_name", "name", "fname", "sname", "user_type", "user_type_str", "contact_email", "contact_phone"),
					"header" => l("seo_tags_account_header", "users", $lang_id, "seo"),
					"url_vars" => array(),
				);
			break;
			case "login_form":
				return array(
					"title" => l("seo_tags_login_form_title", "users", $lang_id, "seo"),
					"keyword" => l("seo_tags_login_form_keyword", "users", $lang_id, "seo"),
					"description" => l("seo_tags_login_form_description", "users", $lang_id, "seo"),
					"templates" => array(),
					"header" => l("seo_tags_login_form_header", "users", $lang_id, "seo"),
					"url_vars" => array()
				);
			break;
			case "restore":
				return array(
					"title" => l("seo_tags_restore_title", "users", $lang_id, "seo"),
					"keyword" => l("seo_tagsv_keyword", "users", $lang_id, "seo"),
					"description" => l("seo_tags_restore_description", "users", $lang_id, "seo"),
					"templates" => array(),
					"header" => l("seo_tags_restore_header", "users", $lang_id, "seo"),
					"url_vars" => array()
				);
			break;
			case "profile":
				return array(
					"title" => l("seo_tags_profile_title", "users", $lang_id, "seo"),
					"keyword" => l("seo_tags_profile_keyword", "users", $lang_id, "seo"),
					"description" => l("seo_tags_profile_description", "users", $lang_id, "seo"),
					"templates" => array("unique_name", "name", "fname", "sname", "user_type", "user_type_str", "contact_email", "contact_phone"),
					"header" => l("seo_tags_profile_header", "users", $lang_id, "seo"),
					"url_vars" => array()
				);
			break;
			case "index":
				return array(
					"title" => l("seo_tags_index_title", "users", $lang_id, "seo"),
					"keyword" => l("seo_tags_index_keyword", "users", $lang_id, "seo"),
					"description" => l("seo_tags_index_description", "users", $lang_id, "seo"),
					"templates" => array('user_type', 'user_type_str'),
					"header" => l("seo_tags_index_header", "users", $lang_id, "seo"),
					"url_vars" => array(
						"user_type" => array("user_type" => "literal"),
						/*"order" => array("order" => "literal"),
						"order_direction" => array("order_direction" => "literal"),
						"page" => array("page" => "literal"),*/
					),
				);
			break;
			case "search":
				return array(
					"title" => l("seo_tags_search_title", "users", $lang_id, "seo"),
					"keyword" => l("seo_tags_search_keyword", "users", $lang_id, "seo"),
					"description" => l("seo_tags_search_description", "users", $lang_id, "seo"),
					"templates" => array('user_type', 'user_type_str'),
					"header" => l("seo_tags_search_header", "users", $lang_id, "seo"),
					"url_vars" => array(
						"user_type" => array("user_type" => "literal"),
						"keyword" => array("keyword" => "literal"),
						/*"order" => array("order" => "literal"),
						"order_direction" => array("order_direction" => "literal"),
						"page" => array("page" => "literal"),*/
					),
				);
			break;
			case "view":
				return array(
					"title" => l("seo_tags_view_title", "users", $lang_id, "seo"),
					"keyword" => l("seo_tags_view_keyword", "users", $lang_id, "seo"),
					"description" => l("seo_tags_view_description", "users", $lang_id, "seo"),
					"templates" => array("name", "fname", "sname", "user_type", "user_type_str", "unique_name", "contact_email", "contact_phone"),
					"header" => l("seo_tags_view_header", "users", $lang_id, "seo"),
					"url_vars" => array(
						"id" => array("id" => "numeric"),
						"section" => array("section" => "literal"),
						"pdf" => array("pdf" => "literal"),
					),
					"optional" => array(
						"user_type" => array("user_type" => "literal"),
						"name" => array("name" => "literal"),
					),
				);
			break;
			default:
				foreach($this->type_arr as $user_type){
					if($method == "reg_".$user_type){
						return array(
							"title" => l('seo_tags_register_'.$user_type.'_title', "users", $lang_id, "seo"),
							"keyword" => l('seo_tags_register_'.$user_type.'_keyword', "users", $lang_id, "seo"),
							"description" => l('seo_tags_register_'.$user_type.'_description', "users", $lang_id, "seo"),
							"templates" => array(),
							"header" => l('seo_tags_register_'.$user_type.'_header', "users", $lang_id, "seo"),
							"url_vars" => array()
						);
						break;
					}
				}
			break;
		}
	}

	/**
	 * Get alias field for seo
	 * @param string $var_name_from
	 * @param string $var_name_to
	 * @param mixed $value
	 * @return mixed
	 */
	public function request_seo_rewrite($var_name_from, $var_name_to, $value){
		$user_data = array();

		if($var_name_from == $var_name_to){
			switch($var_name_to){
				case "name":
				case "type":
					return '';
				break;
				default:
					return $value;
				break;
			}
		}

		if($var_name_from == "login"){
			$user_data = $this->get_user_by_login($value);
		}
		
		if($var_name_to == "id"){
			return $user_data["id"];
		}
	}

	/**
	 * Return urls of pages for seo sitemaps
	 * @return array
	 */
	public function get_sitemap_xml_urls(){
		$this->CI->load->helper("seo");
		$return = array(
			array(
				"url" => rewrite_link("users", "account"),
				"priority" => 0.1
			),
			array(
				"url" => rewrite_link("users", "login_form"),
				"priority" => 0.3
			),
			array(
				"url" => rewrite_link("users", "profile"),
				"priority" => 0.1
			),
			array(
				"url" => rewrite_link("users", "register"),
				"priority" => 0.3
			),
			array(
				"url" => rewrite_link('users', 'index'),
				"priority" => 0.1
			),
		);
		$users = $this->get_active_users_list(null, null, null, array(), null, false);
		if(!empty($users)){
			$this->CI->load->library('Translit');
	
			$current_lang = $this->CI->pg_language->get_lang_by_id($this->CI->pg_language->current_lang_id);
			$current_lang_code = $current_lang['code'];
			
			foreach($users as $seo_user){
				$return[] = array(
					"url" => rewrite_link("users", "view", $seo_user),
					"priority" => 0.5
				);
			}
		}
		return $return;
	}

	
	/**
	 * Return urls of pages for seo sitemaps
	 * @return array
	 */
	public function get_sitemap_urls(){
		$this->CI->load->helper("seo");
		$this->CI->load->helper("users");
		$auth = $this->CI->session->userdata("auth_type");
		$block = array();
		
		$items = array();
		
		$login_enabled = is_login_enabled();
		if($login_enabled){
			$items[] =	array(
				"name" => l("header_login", "users"),
				"link" => rewrite_link("users", "login_form"),
				"clickable" => ($auth == "user") ? false : true
			);
					
			foreach($this->type_arr as $user_type){
				$register_enabled = $this->CI->pg_module->get_module_config("users", $user_type."_register_enabled");
				if(!$register_enabled) continue;
				$items[] = array(
					"name" => ld_option('user_type', 'users', $user_type).' '.l("header_register_as", "users"),
					"link" => rewrite_link("users", "reg_".$user_type),
					"clickable" => ($auth == "user") ? false : true
				);
			}
					
			$items[] = array(
				"name" => l("link_edit_account", "users"),
				"link" => rewrite_link("users", "account"),
				"clickable" => ($auth == "user") ? true : false
			);
					
			$items[] = array(
				"name" => l("link_edit_profile", "users"),
				"link" => rewrite_link("users", "profile"),
				"clickable" => ($auth == "user") ? true : false
			);
					
			$items[] = array(
				"name" => l("link_logout", "users"),
				"link" => site_url() . "users/logout",
				"clickable" => ($auth == "user") ? true : false
			);
		}	
		
		$block[] = array(
			"name" => l("header_profile", "users"),
			"link" => rewrite_link("users", "profile"),
			"clickable" => ($auth == "user") ? true : false,
			"items" => $items,
		);
		
		$items = array();
		
		$professionals = ld('professionals', 'users');
		foreach($professionals['option'] as $user_type=>$name){
			$search_enabled = $this->CI->pg_module->get_module_config("users", $user_type."_search_enabled");
			if(!$search_enabled) continue;
			$items[] = array(
				"name" => $name,
				"link" => site_url() . 'users/index/'.$user_type,
				"clickable" => true,
			);
		}
		
		if(!empty($items)){
			$block[] = array(
				"name" => l("header_search_users", "users"),
				"link" => site_url()."users/index",
				"clickable" => true,
				"items" => $items,
			);
		}
		
		return $block;
	}

	/**
	 * Availables banners places (callback method)
	 * @return array
	 */
	public function _banner_available_pages(){
		$return[] = array("link" => "users/profile", "name" => l("header_profile", "users"));
		$return[] = array("link" => "users/login_form", "name" => l("header_login", "users"));
		$return[] = array("link" => "users/reg_private", "name" => l("header_register_private", "users"));
		$return[] = array("link" => "users/reg_company", "name" => l("header_register_company", "users"));
		$return[] = array("link" => "users/reg_agent", "name" => l("header_register_agent", "users"));
		$return[] = array("link" => "users/account", "name" => l("header_edit_account", "users"));
		$return[] = array("link" => "users/index", "name" => l("header_search_users", "users"));
		$return[] = array("link" => "users/private_list", "name" => l("header_search_privates", "users"));
		$return[] = array("link" => "users/company_list", "name" => l("header_search_companies", "users"));
		$return[] = array("link" => "users/agent_list", "name" => l("header_search_agents", "users"));
		$return[] = array("link" => "users/view", "name" => l("header_view_profile", "users"));
		return $return;
	}

	/**
	 * Dinamic block register (callback method)
	 * @param array $params
	 * @param string $view
	 * @return string
	 */
	public function _dynamic_block_last_registered($params, $view=""){
		$count = $params["count"];
		if($params["with_logo"] == "yes"){
			$attrs["where"]["user_logo !="] = "";
		}else{
			$attrs = array();
		}
		$users = $this->get_active_users_list(1, $count, array("date_created" => "DESC"), $attrs);
		$this->CI->template_lite->assign("users", $users);
		return $this->CI->template_lite->fetch("helper_last_registered", "user", "users");
	}

	/**
	 * Dinamic block callback method
	 * @param array $params
	 * @param string $view
	 * @return string
	 */
	/*public function _dynamic_block_get_featured_users($params, $view=""){
		$count = $params["count"] ? intval($params["count"]) : 8;

		$sep_pos = strrpos($view, '_');
		
		$size = substr($view, $sep_pos+1);
		$this->CI->template_lite->assign('photo_size', $size);
		
		$view = substr($view, 0, $sep_pos);
		
		$attrs = array();
		//$attrs["where"]["featured_end_date > "] = date("Y-m-d H:i:s");
		$users = $this->get_active_users_list(1, $count, array("RAND()" => ""), $attrs);
		
		$this->CI->template_lite->assign('type', 'featured');

		$this->CI->template_lite->assign("users", $users);
		$this->CI->template_lite->assign("header_featured_users", l('header_featured_users' , 'users'));
		return $this->CI->template_lite->fetch('helper_users_block_'.$view, "user", "users");
	}*/
	
	/**
	 * Dinamic block featured companies callback method
	 * @param array $params
	 * @param string $view
	 * @return string
	 */
	public function _dynamic_block_get_featured_companies($params, $view=""){
		$count = $params["count"] ? intval($params["count"]) : 8;

		$sep_pos = strrpos($view, '_');
		
		$size = substr($view, $sep_pos+1);
		$this->CI->template_lite->assign('photo_size', $size);
		
		$view = substr($view, 0, $sep_pos);
		
		$attrs = array();
		$attrs["where"]["user_type"] = 'company';
		$attrs["where"]["featured_end_date > "] = date("Y-m-d H:i:s");
		$users = $this->get_active_users_list(1, $count, array("RAND()" => ""), $attrs);
		
		$this->CI->template_lite->assign('type', 'featured');

		$this->CI->template_lite->assign("header_featured_users", l('header_featured_companies' , 'users'));
		$this->CI->template_lite->assign("users", $users);
		return $this->CI->template_lite->fetch('helper_users_block_'.$view, "user", "users");
	}
	
	/**
	 * Dinamic block featured agents callback method
	 * @param array $params
	 * @param string $view
	 * @return string
	 */
	public function _dynamic_block_get_featured_agents($params, $view=""){
		$count = $params["count"] ? intval($params["count"]) : 8;

		$sep_pos = strrpos($view, '_');
		
		$size = substr($view, $sep_pos+1);
		$this->CI->template_lite->assign('photo_size', $size);
		
		$view = substr($view, 0, $sep_pos);
		
		$attrs = array();
		$attrs["where"]["user_type"] = 'agent';
		$attrs["where"]["featured_end_date > "] = date("Y-m-d H:i:s");
		$users = $this->get_active_users_list(1, $count, array("RAND()" => ""), $attrs);
		
		$this->CI->template_lite->assign('type', 'featured');

		$this->CI->template_lite->assign("users", $users);
		$this->CI->template_lite->assign("header_featured_users", l('header_featured_agents' , 'users'));
		return $this->CI->template_lite->fetch('helper_users_block_'.$view, "user", "users");
	}

	/*
	 * User account functions
	 */
	
	/**
	 * Get user account
	 * @param integer $user_id
	 * @return float
	 */
	public function get_user_account($user_id){
		$data = $this->get_user_by_id($user_id);
		return isset($data["account"]) ? $data["account"] : 0;
	}

	/**
	 * Update user account
	 * @param array $payment_data
	 * @param integer $payment_status
	 */
	public function update_user_account($payment_data, $payment_status){
		if($payment_status == 1){
			$user_id = $payment_data["id_user"];
			$user_data = $this->get_user_by_id($user_id);
			$data["account"] = $user_data["account"] + $payment_data["amount"];
			$this->save_user($user_id, $data);
			$this->account_add_spend_entry($user_id, $payment_data["amount"], "add", l("account_funds_add_message", "users"));
			$this->CI->load->model("Notifications_model");
			$this->CI->Notifications_model->send_notification($user_data["email"], "users_update_account", $user_data);
		}
	}

	/**
	 * Write off from account
	 * @param integer $id_user
	 * @param float $amount
	 * @param string message
	 */
	public function write_off_user_account($id_user, $amount, $message){
		$user_data = $this->get_user_by_id($id_user);
		$data["account"] = $user_data["account"] - $amount;
		if($data["account"] < 0){
			return l("error_money_not_sufficient", "users");
		}else{
			$this->save_user($id_user, $data);
			$this->account_add_spend_entry($id_user, $amount, "spend", $message);
			return true;
		}
	}

	/**
	 * Add spend transaction
	 * @param integer $id_user
	 * @param float $price
	 * @param string $price_type
	 * @param string $message
	 */
	public function account_add_spend_entry($id_user, $price, $price_type="add", $message=""){
		if(empty($message)){
			$message = ($price_type = "add") ? l("account_funds_add_message", "users") : l("account_funds_spend_message", "users");
		}

		$data = array(
			"id_user" => $id_user,
			"date_add" => date("Y-m-d H:i:s"),
			"price_type" => $price_type,
			"price" => $price,
			"message" => $message,
		);
		$this->DB->insert(USER_ACCOUNT_STAT_TABLE, $data);
	}

	/*
	 * Moderation functions
	 */
	
	/**
	 * Get users for moderation
	 * @param array $object_ids
	 * @return array
	 */
	public function _moder_get_list($object_ids){
		$params["where_in"]["id"] = $object_ids;

		$users = $this->get_users_list(null, null, null, $params);

		if(!empty($users)){
			foreach($users as $user){
				$return[$user["id"]] = $user;
			}
			return $return;
		}else{
			return array();
		}
	}

	/**
	 * Set moderation status to object
	 * @param integer $object_id
	 * @param integer $status
	 */
	public function _moder_set_status($object_id, $status){
		$user = $this->get_user_by_id($object_id);
		$backup_user = array();
		switch($status){
			case 0:
				if($user["user_logo_moderation"]){
					$backup_user["user_logo_moderation"] = "";
				}else{
					$backup_user["user_logo"] = "";
				}
				break;
			case 1:
				if($user["user_logo_moderation"]){
					$backup_user["user_logo"] = $user["user_logo_moderation"];
					$backup_user["user_logo_moderation"] = "";
				}
				break;
		}
		$this->save_user($object_id, $backup_user);
	}

	/**
	 * Get user type by id
	 * @param integer $user_id
	 * @return string
	 */
	public function get_user_type_by_id($user_id){	
		$this->set_format_settings('use_format', false);
		$data = $this->get_user_by_id($user_id);
		$this->set_format_settings('use_format', true);
		if(isset($data["user_type"])){
			return $data["user_type"];
		}else{
			return null;
		}
	}

	/*
	 * Contacts functions
	 */

	/**
	 * Add contact link
	 * @param integer $id_user
	 * @param integer $id_contact
	 */
	public function add_contact($id_user, $id_contact){
		$this->CI->load->model("Linker_model");
		$this->CI->Linker_model->add_link("users_contacts", $id_user, $id_contact);
		$this->CI->Linker_model->add_link("users_contacts", $id_contact, $id_user);
		return;
	}

	/**
	 * Remove contact link
	 * @param integer $id_user
	 * @param integer $id_contact
	 */
	public function delete_contact($id_user, $id_contact){
		$this->CI->load->model("Linker_model");
		$params["where_in"]["id_link_2"] = $params["where_in"]["id_link_1"] = array($id_user, $id_contact);
		$this->CI->Linker_model->delete_links("users_contacts", $params);
		return;
	}

	/**
	 * Remove all contacts
	 * @param integer $id_user
	 */
	public function delete_user_contacts($id_user){
		$this->CI->load->model("Linker_model");
		$params["where"]["id_link_2"] = $params["where"]["id_link_1"] = $id_user;
		$this->CI->Linker_model->delete_links("users_contacts", $params);
	}
	
	/**
	 * Return display sections
	 * @param array $data
	 */
	public function get_display_sections($data){
		//if($data["user_type"] == "agent" && $data['agent_status']) $this->display_sections["company"] = 1;
		if(!$data['is_map'] || !$this->CI->pg_module->is_module_installed("geomap")) $this->display_sections["map"] = 0;
		if(!$data["review_count"]) $this->display_sections["reviews"] = 0;
		return $this->display_sections;
	}
	
	/**
	 * Return user agents as array
	 * @param integer $company_id
	 * @param integer $agent_status
	 * @return array
	 */
	public function get_agents_list($company_id, $agent_status=1, $page=1, $items_on_page=null, $order_by=array(), $formatted=true, $name){
		$params["where"]["agent_company"] = $company_id;
		$params["where"]["agent_status"] = $agent_status;
		if($name != null) $params["where_sql"][] = "(fname LIKE '%".$name."%' OR sname LIKE '%".$name."%')";
		return $this->get_users_list($page, $items_on_page, $order_by, $params, array(), $formatted);		
	}
	
	/**
	 * Return number of user agents
	 * @param integer $company_id
	 * @param integer $agent_status
	 * @return integer
	 */
	public function get_agents_count($company_id, $agent_status=1 , $name = null){
		$params["where"]["agent_company"] = $company_id;
		$params["where"]["agent_status"] = $agent_status;
		if($name != null) $params["where_sql"][] = "(fname LIKE '%".$name."%' OR sname LIKE '%".$name."%')";
		return $this->get_users_count($params);
	}
	
	/**
	 * Approve agent
	 * @param integer $company_id
	 * @param integer $agent_id
	 */
	public function approve_agent($company_id, $agent_id){
		$company = $this->get_user_by_id($company_id);		
		$agent = $this->get_user_by_id($agent_id);
		
		$set["agent_status"] = 1;
		$this->save_user($agent_id, $set);
		
		$data['fname'] = $agent['fname'];
		$data['sname'] = $agent['sname'];
		$data["company_name"] = $company["output_name"];
		$data["user_name"] = $agent["output_name"];
		
		$this->update_agent_company_count($company_id);
		
		$this->CI->load->model("Linker_model");
		$this->CI->Linker_model->add_link("users_contacts", $agent_id, $company_id);
		$this->CI->Linker_model->add_link("users_contacts", $company_id, $agent_id);
		
		$this->CI->load->model("Notifications_model");
		$this->CI->Notifications_model->send_notification($agent["email"], "agent_request_accepted", $data);
	}
	
	/**
	 * Decline agent
	 * @param integer $company_id
	 * @param integer $agent_id
	 */
	public function decline_agent($company_id, $agent_id){
		$company = $this->get_user_by_id($company_id);		
		$agent = $this->get_user_by_id($agent_id);
		
		$set["agent_company"] = 0;
		$this->save_user($agent_id, $set);
		
		$data['fname'] = $agent['fname'];
		$data['sname'] = $agent['sname'];
		$data["company_name"] = $company["output_name"];
		$data["user_name"] = $agent["output_name"];
		
		$this->CI->load->model("Notifications_model");
		$this->CI->Notifications_model->send_notification($agent["email"], "agent_request_declined", $data);
	}
	
	/**
	 * Remove agent from company
	 * @param integer $company_id
	 * @param integer $agent_id
	 */
	public function delete_agent($company_id, $agent_id, $by_agent){
		$company = $this->get_user_by_id($company_id);
		$agent = $this->get_user_by_id($agent_id);
		
		$set["agent_company"] = 0;
		$set["agent_status"] = 0;
		$this->save_user($agent_id, $set);
		
		$mail_data["company_name"] = $company["output_name"];
		$mail_data["user_name"] = $agent["output_name"];
		
		$this->CI->load->model("Notifications_model");
		
		if($by_agent){
			if($agent['agent_status']){
				$company_template = 'agent_unjoin';
			}else{
				$company_template = 'agent_cancel_request';
			}
			$mail_data["fname"] = $company["fname"];
			$mail_data["sname"] = $company["sname"];
			$this->CI->Notifications_model->send_notification($company["email"], $company_template, $mail_data);
		}else{
			if($agent['agent_status']){
				$company_template = 'agent_unjoin';
				$agent_template = 'agent_deleted';
				
				$this->update_agent_company_count($company_id);
			}else{
				$company_template = 'agent_cancel_request';
				$agent_template = 'agent_request_declined';
			}
			
			$mail_data["fname"] = $company["fname"];
			$mail_data["sname"] = $company["sname"];
			$this->CI->Notifications_model->send_notification($company["email"], $company_template, $mail_data);
		
			//$mail_data["fname"] = $agent["fname"];
			//$mail_data["sname"] = $agent["sname"];
			//$this->CI->Notifications_model->send_notification($agent["email"], $agent_template, $mail_data);
		}
	}
	
	/**
	 * Check user is end date
	 * @param string $field
	 * @param integer $user_id
	 */
	public function is_end_date($field, $user_id){
		$this->DB->select($field)
				->from(USERS_TABLE)
				->where("id", $user_id)
				->where($field." >", date("Y-m-d H:i:s"))
				->where("UNIX_TIMESTAMP(".$field.") >", "86400");
		$result = $this->DB->get()->result_array();
		if (empty($result)) return false;
		return $result[0][$field];
	}
	
	/**
	 * Clear end date value for source field
	 * @param string $field
	 * @param string $service_name
	 * @return integer
	 */
	public function clear_end_date($field, $service_name){
		$this->DB->select(implode(", ", $this->fields_general))
				 ->from(USERS_TABLE)
				 ->where($field.' <', date('Y-m-d H:i:s'))
				 ->where('UNIX_TIMESTAMP('.$field.') >', '86400');
		$results = $this->DB->get()->result_array();
		$clean = 0;
		if(!empty($results) && is_array($results)){
			$clean = count($results);
			if($clean > 0){	
				$data[$field] = "0000-00-00 00:00:00";
				$this->DB->where($field." <", date("Y-m-d H:i:s"))
					->where("UNIX_TIMESTAMP(".$field.") >", "86400")
					->update(USERS_TABLE, $data);
					
				// send notification
				$this->CI->load->model("Notifications_model");
				$results = $this->format_users($results);
				foreach($results as $user_data){
					$mail_data = array(
						'user'=>$user_data['output_name'],
						'name'=>l($service_name, 'users_services', $user_data['lang_id']),
					);
					$this->CI->Notifications_model->send_notification($user_data['email'], "user_service_expired", $mail_data);
				}
			}
		}
		return $clean;
	}
	
	/**
	 * Install reviews fields
	 * @param array $fields
	 */
	public function install_reviews_fields($fields=array()){		
		if(empty($fields)) return;
		$this->CI->load->dbforge();
		$table_fields = $this->CI->db->get(USERS_TABLE)->list_fields();
		foreach((array)$fields as $field_name=>$field_data){
			if(!in_array($field_name, $table_fields)){
				$this->CI->dbforge->add_column(USERS_TABLE, array($field_name=>$field_data));
			}
		}
	}
	
	/**
	 * Uninstall reviews fields
	 * @param array $fields
	 */
	public function deinstall_reviews_fields($fields=array()){
		if(empty($fields)) return;
		$this->CI->load->dbforge();
		$table_fields = $this->CI->db->get(USERS_TABLE)->list_fields();
		foreach($fields as $field_name){
			if(in_array($field_name, $table_fields)){
				$this->CI->dbforge->drop_column(USERS_TABLE, $field_name);
			}
		}
	}
	
	/**
	 * Callback for reviews module
	 * @param integer $object_id
	 * @param integer $sorter
	 * @return string
	 */
	public function callback_reviews($action, $data){
		switch($action){
			case 'update':
				$user_data['review_type'] = $data['type_gid'];
				$user_data['review_sorter'] = $data['review_sorter'];
				$user_data['review_count'] = $data['review_count'];
				$user_data['review_data'] = serialize($data['review_data']);
				$this->save_user($data['id_object'], $user_data);
			break;
			case 'get_object':
				if(empty($data)) return array();	
				$users = $this->get_users_list_by_key(null, null, null, null, (array)$data);
				return $users;
			break;
		}
	}
	
	/**
	 * Callback for spam module
	 * @param string $action action name
	 * @param integer $user_ids user identifiers
	 * @return string
	 */
	public function spam_callback($action, $data){
		switch($action){
			case "ban":
				$this->save_user((int)$data, array("banned"=>1));
				return "banned";
				
				//$this->delete_user((int)$data);
				//return "removed";
			break;
			case "unban":
				$this->save_user((int)$data, array("banned"=>0));
				return "unbanned";
			break;
			case "delete":
				$this->delete_user((int)$data);
				return "removed";
			break;
			case 'get_content':
				if(empty($data)) return array();	
				$users = $this->get_users_list(null, null, null, null, (array)$data);
				$return = array();
				foreach($users as $user){
					$return[$user['id']] = $user['output_name'].', '.l($user['user_type'], 'users');
				}
				return $return;
			break;
			case 'get_link':
				if(empty($data)) return array();	
				$users = $this->get_users_list(null, null, null, null, (array)$data);
				$return = array();
				foreach($users as $user){
					$return[$user['id']] = site_url().'admin/users/edit/'.$user['id'];
				}
				return $return;
			break;
			case 'get_object':
				if(empty($data)) return array();	
				$users = $this->get_users_list_by_key(null, null, null, null, (array)$data);
				return $users;
			break;
		}
	}
	
	/**
	 * Check view user profile by new user
	 * @param integer $user_id user identifier
	 */
	public function check_views_count($user_id) {
		$views_array = $this->CI->session->userdata("views_users_array");
		if(isset($views_array) && !empty($views_array)){
			if(!in_array($user_id, $views_array)){
				$views_array[] = $user_id;
				$this->CI->session->set_userdata("views_users_array", $views_array);
				$this->update_views_field($user_id);
			}
		}else{
			$this->CI->session->set_userdata("views_users_array", array($user_id));
			$this->update_views_field($user_id);
		}
	}

	/**
	 * Update views count
	 * @param integer $user_id user identifier
	 */
	public function update_views_field($user_id){
		if($this->CI->session->userdata("auth_type") == "user"){
			$visitor_id = intval($this->CI->session->userdata("user_id"));
			if($user_id != $visitor_id){
				$this->CI->load->model('users/models/Users_visitor_model');
				$visitor = $this->CI->Users_visitor_model->get_visitor_by_visitor($user_id, $visitor_id);
				$save_data = array('id_user'=>$user_id, 'id_visitor'=>$visitor_id);
				$this->CI->Users_visitor_model->save_visitor($visitor['id'], $save_data);
			}
		}
			
		$this->DB->set("views", "views + 1", FALSE);
		$this->DB->where("id", $user_id);
		$this->DB->update(USERS_TABLE);
	}
	
	/**
	 * Install listings fields
	 * @param array $fields
	 */
	public function install_listings_fields($fields=array()){		
		if(empty($fields)) return;
		$this->CI->load->dbforge();
		$table_fields = $this->CI->db->get(USERS_TABLE)->list_fields();
		foreach((array)$fields as $field_name=>$field_data){
			if(!in_array($field_name, $table_fields)){
				$this->CI->dbforge->add_column(USERS_TABLE, array($field_name=>$field_data));
			}
		}
	}
	
	/**
	 * Uninstall listings fields
	 * @param array $fields
	 */
	public function deinstall_listings_fields($fields=array()){
		if(empty($fields)) return;
		$this->CI->load->dbforge();
		$table_fields = $this->CI->db->get(USERS_TABLE)->list_fields();
		foreach($fields as $field_name){
			if(in_array($field_name, $table_fields)){
				$this->CI->dbforge->drop_column(USERS_TABLE, $field_name);
			}
		}
	}
	
	/**
	 * Return pdf version
	 * @param string $content user content
	 * @param array $data user data
	 */
	public function get_pdf_file($content, $data){
		$content = preg_replace("/<script(.*?)<\/script>/is", "", $content);
		$content = str_replace("&nbsp;", " ", $content);

		$this->CI->load->library("dompdf");

		$this->CI->dompdf->set_paper("a4", "portrait");
		$this->CI->dompdf->load_html($content);
		$this->CI->dompdf->render();
		$file_name = $data["unique_name"].".pdf";
		$this->CI->dompdf->stream($file_name);
	}
	
	/**
	 * Return profile completeness
	 */
	public function get_profile_complete($user){
		$complete_count = 0;
		$params_count = 5;
		
		if($user['fname'] != '') $complete_count++;
		if($user['sname'] != '') $complete_count++;
		if($user['email'] != '') $complete_count++;
		if($user['phone'] != '') $complete_count++;
		if($user['logo_path'] != '') $complete_count++;
		
		switch($user['user_type']){
			case 'company':
				if(!empty($user['company_name'])) $complete_count++;
				if(!empty($user['company_url'])) $complete_count++;
				if(!empty($user['id_country'])) $complete_count++;
				if(!empty($user['id_region'])) $complete_count++;
				if(!empty($user['city'])) $complete_count++;
				if(!empty($user['address'])) $complete_count++;
				if(!empty($user['postal_code'])) $complete_count++;
				if(!empty($user['working_days'])) $complete_count++;
				if(!empty($user['working_hours_begin']) && !empty($user['working_hours_end'])) $complete_count++;
				$params_count += 9;
			break;
			case 'agent':
				if(!empty($user['agent_company'])) $complete_count++;
				$params_count += 1;
			break;
		}
          
		return ceil($complete_count * 100 / $params_count);
	}
	
	/**
	 * Update listings statistics
	 * @param integer $user_id user identifier
	 * @param string $type listing type
	 */
	public function update_listings_stat($user_id, $type, $inc=true){
		$name = 'listings_for_'.$type.'_count';
		if(!in_array($name, $this->fields_listings)) return;
		$this->DB->set($name, $name." ".($inc ? "+" : "-")." 1", FALSE);
		$this->DB->where("id", $user_id);
		$this->DB->update(USERS_TABLE);
	}
	
	/**
	 * Return seo link for search
	 */
	public function get_search_link_data(){
		$current_settings = isset($_SESSION['users_list']) ? $_SESSION['users_list'] : array();

		$user_types = $this->type_arr;
		if(isset($current_settings['data']['type']) && 
		   in_array($current_settings['data']['type'], $user_types)){
			$user_type = $current_settings['data']['type'];
		}else{
			$user_type = current($user_types);
		}
	
		$keyword = '';
		if(isset($current_settings['data']['keyword'])){
			$keyword = $current_settings['data']['keyword'];
		}
		
		$order = 'date_created';
		$order_direction = 'DESC';
		$page = 1;
		
		if(!empty($current_settings)){
			$order = $current_settings['order'];
			$order_direction = $current_settings['order_direction'];
			$page = $current_settings['page'];
		}
		
		$action_data = array(
			'user_type' => $user_type,
			'keyword' => $keyword,
			'order' => $order,
			'order_direction' => $order_direction,
			'page' => $page,
		);
		
		return $action_data;
	}
	
	/**
	 * Change format settings
	 * @param string $name parameter name
	 * @param mixed $value parameter value
	 */
	public function set_format_settings($name, $value=false){
		if(!is_array($name)) $name = array($name=>$value);
		foreach($name as $key => $item)	$this->format_settings[$key] = $item;
	}			
	
	/**
	 * Save user logo by local file
	 * @param integer $user_id user identifier
	 * @param string $file_name file name
	 * @param boolean $use_icon_moderation use moderation
	 * @return array
	 */
	public function save_local_user_logo($user_id, $file_name, $use_icon_moderation=true){
		$this->CI->load->model('Uploads_model');
		$img_return = $this->CI->Uploads_model->upload_exist($this->upload_config_id, $user_id."/", $file_name);
		
		if(empty($img_return["errors"])){
			if($use_icon_moderation){
				$this->CI->load->model("Moderation_model");
				$mstatus = intval($this->CI->Moderation_model->get_moderation_type_status($this->moderation_type));
			}else{
				$mstatus = 2;
			}
			if($mstatus != 2){
				$this->CI->Moderation_model->add_moderation_item($this->moderation_type, $user_id);
				if($mstatus == 1){
					$img_data["user_logo"] = $img_return["file"];
					$img_data["user_logo_moderation"] = "";
				}else{
					$img_data["user_logo_moderation"] = $img_return["file"];
				}
			}else{
				$img_data["user_logo"] = $img_return["file"];
			}
			$this->save_user($user_id, $img_data);
		}
		return $img_return;
	}
}
