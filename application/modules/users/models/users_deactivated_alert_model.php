<?php
/**
* Deactivated alert Model
* 
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/

define("DEACTIVATED_ALERTS_TABLE", DB_PREFIX."user_deactivated_alerts");

class Users_deactivated_alert_model extends Model{

	/**
	 * link to CodeIgniter object
	 * @var object
	 */
	private $CI;

	/**
	 * link to DataBase object
	 * @var object
	 */
	private $DB;

	/**
	 * Table fields
	 * @var array
	 */
	var $fields = array(
		"id",
		"fname",
		"sname",
		"name",
		"email",
		"phone",
		"id_reason",
		"message",
		"status",
		"date_add",
	);
	
	/**
	 * Format settings
	 * @var array
	 */
	private $format_settings = array(
		"use_format"  => true,
		"get_reason"  => false,
	);
	
	/**
	 * Constructor
	 *
	 * return Deactivated alert object
	 */
	public function __construct()
	{
		parent::Model();
		$this->CI = & get_instance();
		$this->DB = &$this->CI->db;
	}

	/**
	 * Get deactivated alert by ID
	 * @param integer $id deactivated alert ID
	 * @param 
	 */ 
	public function get_alert_by_id($id, $formatted=false){
		$id = intval($id);
		
		$this->DB->select(implode(", ", $this->fields));
		$this->DB->from(DEACTIVATED_ALERTS_TABLE);
		$this->DB->where("id", $id);
		
		//_compile_select;
		$result = $this->DB->get()->result();
		if(!empty($result)){
			$rt = get_object_vars($result[0]);
			if($formatted) $rt = $this->format_alert(array($rt));
			return $rt[0];
		}else
			return false;
	}
	
	/**
	 * Save deactivated alert
	 * @param array $data
	 * @return boolean
	 */
	public function save_alert($id, $data){
		if(!$id){
			$data["date_add"] = date("Y-m-d H:i:s");
			$this->DB->insert(DEACTIVATED_ALERTS_TABLE, $data);
			$id = $this->DB->insert_id();
		}else{
			$this->DB->where("id", $id);
			$this->DB->update(DEACTIVATED_ALERTS_TABLE, $data);
		}
		return $id;
	}
	
	/**
	 * Remove deactivated alert by ID
	 * @param integer $id alert ID
	 */ 
	public function delete_alert($id){
		$alert = $this->get_alert_by_id($id, true);
		if(!$alert) return false;
				
		$this->DB->where("id", $id);
		$this->DB->delete(DEACTIVATED_ALERTS_TABLE);
	}	
	
	/**
	 * Mark deactivated alert as read
	 * @param integer $id alert ID
	 */ 
	public function mark_as_read($id){
		$data["status"] = 1;
		$this->save_alert($id, $data);
	}
	
	/**
	 * Return deactivated alerts as array
	 * @param integer $page
	 * @param string $limits 
	 * @param array $order_by
	 * @param array $params
	 * @return array
	 */
	private function _get_alerts_list($page=null, $limits=null, $order_by=null, $params=array()){
		$this->DB->select(implode(", ", $this->fields));
		$this->DB->from(DEACTIVATED_ALERTS_TABLE);
		
		if(isset($params["where"]) && is_array($params["where"]) && count($params["where"])){
			foreach($params["where"] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}
		
		if(isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])){
			foreach($params["where_in"] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])){
			foreach($params["where_sql"] as $value){
				$this->DB->where($value);
			}
		}

		if (is_array($order_by) && count($order_by) > 0){
			foreach ($order_by as $field => $dir){
				if (in_array($field, $this->fields)){
					$this->DB->order_by($field." ".$dir);
				}
			}
		} else if ($order_by){
			$this->DB->order_by($order_by);
		}

		if(!is_null($page)){
			$page = intval($page)?intval($page):1;
			$this->DB->limit($limits, $limits*($page-1));
		}

		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			foreach($results as $r){
				$data[] = $r;
			}
			return $this->format_alert($data);
		}
		return array();
	}
	
	/**
	 * Return deactivated alerts as array
	 * @param array $params
	 * @return integer
	 */
	private function _get_alerts_count($params=null){
		$this->DB->select("COUNT(*) AS cnt");
		$this->DB->from(DEACTIVATED_ALERTS_TABLE);

		if(isset($params["where"]) && is_array($params["where"]) && count($params["where"])){
			foreach($params["where"] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])){
			foreach($params["where_in"] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])){
			foreach($params["where_sql"] as $value){
				$this->DB->where($value);
			}
		}

		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			return intval($results[0]["cnt"]);
		}
		return 0;
	}
	
	/**
	 * Return deactivated alerts as array
	 * @param integer $page
	 * @param string $limits 
	 * @param array $order_by
	 * @return array
	 */
	public function get_alerts_list($page=null, $limits=null, $order_by=null){
		return $this->_get_alerts_list();
	}
	
	/**
	 * Return number of deactivated alerts
	 * @param string $type_gid
	 * @return integer
	 */
	public function get_alerts_count(){
		return $this->_get_alerts_count();
	}
	
	/**
	 * Validate deactivate alert
	 * @param integer $id alert identifier
	 * @param array $data
	 * @return array
	 */
	public function validate_alert($id, $data){
		$return = array("errors"=> array(), "data" => array());
		
		if(isset($data["id"])){
			$return["data"]["id"] = intval($data['id']);
		}

		if(isset($data["fname"])){
			$return["data"]["fname"] = trim(strip_tags($data["fname"]));
		}
		
		if(isset($data["sname"])){
			$return["data"]["sname"] = trim(strip_tags($data["sname"]));
		}

		if(isset($data["name"])){
			$return["data"]["name"] = trim(strip_tags($data["name"]));
		}
		
		if(isset($data["email"])){
			$return["data"]["email"] = trim(strip_tags($data["email"]));
		}
		
		if(isset($data["phone"])){
			$return["data"]["phone"] = trim(strip_tags($data["phone"]));
		}
		
		if(isset($data["id_reason"])){
			$return["data"]["id_reason"] = intval($data["id_reason"]);
			if(empty($return["data"]["id_reason"])){
				$return["errors"][] = l("error_empty_deactivated_reason", "users");
			}
		}elseif(!$id){
			$return["errors"][] = l("error_empty_deactivated_reason", "users");
		}
		
		if(isset($data["message"])){
			$return["data"]["message"] = trim(strip_tags($data["message"]));
		}
		
		if(isset($data["status"])){
			$return["data"]["status"] = intval($data['status']);
		}
		
		return $return;
	}
	
	/**
	 * Validate deactivate settings
	 * @param array $data
	 * @return array
	 */
	public function validate_settings($data){
		$return = array("errors"=> array(), "data" => array());
		
		if(isset($data["deactivated_email"])){
			$return["data"]["deactivated_email"] = trim(strip_tags($data["deactivated_email"]));
			if(!empty($return["data"]["deactivated_email"])){
				$this->CI->config->load("reg_exps", TRUE);
				$email_expr = $this->CI->config->item("email", "reg_exps");
				$return["data"]["_deactivated_email"] = strip_tags($return["data"]["deactivated_email"]);
				if(empty($return["data"]["deactivated_email"]) || !preg_match($email_expr, $return["data"]["deactivated_email"])){
					$return["errors"][] = l("error_email_incorrect", "users");
				}
			}
		}

		if(isset($data["deactivated_send_mail"])){
			$return["data"]["deactivated_send_mail"]  = $data["deactivated_send_mail"] ? 1 : 0;
		}
		
		if(isset($data["deactivated_send_alert"])){
			$return["data"]["deactivated_send_alert"]  = $data["deactivated_send_alert"] ? 1 : 0;
			if($return["data"]["deactivated_send_alert"]){
				if(isset($return["data"]["deactivated_email"])){
					if(empty($return["data"]["deactivated_email"])){
						$return["errors"][] = l("error_empty_deactivated_email", "users");
					}
				}else{
					$deactivated_email = $this->pg_module->get_module_config("users", "deactivated_email");
					if(empty($deactivated_email)){
						$return["errors"][] = l("error_empty_deactivated_email", "users");
					}
				}
			}
		}

		return $return;
	}
	
	/**
	 * Set format settings
	 * @param array $data
	 * @return array
	 */
	public function set_format_settings($data){
		foreach($data as $key=>$value){
			if(isset($this->format_settings[$key]))
				$this->format_settings[$key] = $value;
		}
	}	
		
	/**
	 * Format deactivated alert
	 * @param array $data
	 * @return array
	 */
	public function format_alert($data){
		if(!$this->format_settings["use_format"]){
			return $data;
		}
		
		$reasons_search = array();
		
		foreach($data as $key=>$alert){
			$data[$key] = $alert;			
			//get reason
			if($this->format_settings["get_reason"] && $alert["id_reason"]){
				$reasons_search[] = $alert["id_reason"];
			}
			$data[$key]["message"] = nl2br($alert["message"]);			
		}
		
		if($this->format_settings["get_reason"] && !empty($reasons_search)){
			$this->CI->load->model("users/models/Users_deactivated_reason_model");
			
			$lang_id = $this->pg_language->current_lang_id;
			$reference = $this->pg_language->ds->get_reference($this->CI->Users_deactivated_reason_model->module_gid, 
				$this->CI->Users_deactivated_reason_model->content[0], $lang_id);
			foreach($data as $key=>$alert){
				$data[$key]["reason"] = isset($reference["option"][$alert["id_reason"]]) ? 
					$reference["option"][$alert["id_reason"]] : "";
			}
		}
		
		return $data;
	}
}
