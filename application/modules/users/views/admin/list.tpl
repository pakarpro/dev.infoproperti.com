{include file="header.tpl"}
{helper func_name=get_admin_level1_menu helper_name=menu func_param='admin_users_types_menu'}

<div class="actions">
	<ul>
		<li><div class="l"><a href="{$site_url}admin/users/add/{$user_type}">{l i='link_add_user' gid='users'}</a></div></li>
		{helper func_name=button_add_funds module=users_payments}
	</ul>
	&nbsp;
</div>
<!-- MOD FOR USER SEARCH -->
<div align="right" style="margin-bottom:5px;">
    <form action="" method="post" enctype="multipart/form-data">
    <table>
        <tr>
        	<td><input type="submit" value="search" name="btn_save"/></td>
            <td><input type="text" name="search-user" value="{$search}"/></td>
        </tr>
	{if $search ne ''}
        <tr>
        	<td></td>
            <td align="right"><input type="submit" name="cancel_search" value="Cancel search"/></td>
        </tr>
    {/if}                
    </table>
    </form>
</div>
<!-- END OF MOD -->
<div class="menu-level3">
	<ul>
		{foreach item=item from=$filters}
		{assign var='l' value='filter_'`$item`'_users'}
		<li class="{if $filter eq $item}active{/if}{if !$filter_data[$item]} hide{/if}"><a href="{$site_url}admin/users/index/{$user_type}/{$item}">{l i=$l gid='users'} ({$filter_data[$item]})</a></li>
		{/foreach}
	</ul>
	&nbsp;
</div>
<table cellspacing="0" cellpadding="0" class="data" width="100%">
<tr>
	<th class="first"><input type="checkbox" id="grouping_all"></th>
	<th class="w150"><a href="{$sort_links.name'}"{if $order eq 'name'} class="{$order_direction|lower}"{/if}>{l i='field_name' gid='users'}</a></th>
	<th class="w150"><a href="{$sort_links.email}"{if $order eq 'email'} class="{$order_direction|lower}"{/if}>{l i='field_email' gid='users'}</a></th>
	<th><a href="{$sort_links.account}"{if $order eq 'account'} class="{$order_direction|lower}"{/if}>{l i='field_account' gid='users'}</a></th>
	{depends module=reviews}
	<th class="w30"><a href="{$sort_links.reviews}"{if $order eq 'reviews'} class="{$order_direction|lower}"{/if}>{l i='field_reviews_count' gid='users'}</a></th>
	{/depends}
	<th class="w100"><a href="{$sort_links.date_created}"{if $order eq 'date_created'} class="{$order_direction|lower}"{/if}>{l i='field_date_created' gid='users'}</a></th>
	<th class="w150">&nbsp;</th>
</tr>
{foreach item=item from=$users}
{counter print=false assign=counter}
<tr{if $counter is div by 2} class="zebra"{/if}>
	<td class="first w20 center"><input type="checkbox" class="grouping" value="{$item.id}"></td>
	<td>{$item.output_name|truncate:100}</td>
	<td>{$item.email|truncate:50}</td>
	<td class="center">{block name=currency_format_output module=start value=$item.account}</td>
	{depends module=reviews}
	<td class="center"><a href="{$site_url}admin/reviews/index/users_object/{$item.id}">{$item.review_count}</a></td>
	{/depends}
	<td class="center">{$item.date_created|date_format:$page_data.date_format}</td>
	<td class="icons">
		{if $item.status}
		<a href="{$site_url}admin/users/activate/{$item.id}/0"><img src="{$site_root}{$img_folder}icon-full.png" width="16" height="16" border="0" alt="{l i='link_deactivate_user' gid='users' type='button'}" title="{l i='link_deactivate_user' gid='users' type='button'}"></a>
		{else}
		<a href="{$site_url}admin/users/activate/{$item.id}/1"><img src="{$site_root}{$img_folder}icon-empty.png" width="16" height="16" border="0" alt="{l i='link_activate_user' gid='users' type='button'}" title="{l i='link_activate_user' gid='users' type='button'}"></a>
		{/if}
		<a href="{$site_url}admin/users/edit/{$item.id}"><img src="{$site_root}{$img_folder}icon-edit.png" width="16" height="16" border="0" alt="{l i='link_edit_user' gid='users' type='button'}" title="{l i='link_edit_user' gid='users' type='button'}"></a>
		<a href="{$site_url}admin/users/delete/{$item.id}" onclick="javascript: if(!confirm('{l i='note_delete_user' gid='users' type='js'}')) return false;"><img src="{$site_root}{$img_folder}icon-delete.png" width="16" height="16" border="0" alt="{l i='link_delete_user' gid='users' type='button'}" title="{l i='link_delete_user' gid='users' type='button'}"></a>
        <!-- mod for news cms -->
		{if $item.news_privilege}
		<a href="{$site_url}admin/users/news_privilege/{$item.id}/0"><img src="{$site_root}{$img_folder}icon-export.png" width="16" height="16" border="0" alt="{l i='revoke_news_privileges' gid='users'}" title="{l i='revoke_news_privileges' gid='users'}"></a>
		{else}
		<a href="{$site_url}admin/users/news_privilege/{$item.id}/1"><img src="{$site_root}{$img_folder}icon-export-empty.png" width="16" height="16" border="0" alt="{l i='grant_news_privileges' gid='users'}" title="{l i='grant_news_privileges' gid='users'}"></a>
		{/if}
        
        <!-- end of news cms mod -->        
	</td>
</tr>
{foreachelse}
{assign var='colspan' value=7}
{depends module=reviews}{assign var='colspan' value=$colspan+2}{/depends}
<tr><td colspan="{$colspan}" class="center">{l i='no_users' gid='users'}</td></tr>
{/foreach}
</table>
{include file="pagination.tpl"}

<script type="text/javascript">{literal}
var reload_link = "{/literal}{$site_url}admin/users/index/{literal}";
var filter = '{/literal}{$filter}{literal}';
var order = '{/literal}{$order}{literal}';
var loading_content;
var order_direction = '{/literal}{$order_direction}{literal}';
$(function(){
	$('#grouping_all').bind('click', function(){
		var checked = $(this).is(':checked');
		if(checked){
			$('input[type=checkbox].grouping').attr('checked', 'checked');
		}else{
			$('input[type=checkbox].grouping').removeAttr('checked');
		}
	});
});

function reload_this_page(value){
	var link = reload_link + value + '/' + filter + '/' + order + '/' + order_direction;
	location.href=link;
}
{/literal}</script>

{include file="footer.tpl"}
