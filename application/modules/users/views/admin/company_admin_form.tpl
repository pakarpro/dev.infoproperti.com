<form action="" method="post" enctype="multipart/form-data">
<div class="edit-form n150">
	<div class="row header">{if $data.id}{l i='admin_header_users_change' gid='users'}{else}{l i='admin_header_users_add' gid='users'}{/if}</div>
	<div class="row">
		<div class="h">{l i='field_user_type' gid='users'}: </div>
		<div class="v"><b>{$data.user_type_str}</b></div>
	</div>
	<div class="row">
		<div class="h">{l i='field_email' gid='users'}:&nbsp;* </div>
		<div class="v"><input type="text" name="data[email]" value="{$data.email|escape}"></div>
	</div>
	{if $data.id}
	<div class="row">
		<div class="h">{l i='field_change_password' gid='users'}:{if !$data.id}&nbsp;*{/if} </div>
		<div class="v"><input type="checkbox" value="1" name="update_password" id="pass_change_field"></div>
	</div>
	{/if}
	<div class="row">
		<div class="h">{l i='field_password' gid='users'}:{if !$data.id}&nbsp;*{/if} </div>
		<div class="v"><input type="password" name="data[password]" id="pass_field" {if $data.id}disabled{/if}></div>
	</div>
	<div class="row">
		<div class="h">{l i='field_repassword' gid='users'}: </div>
		<div class="v"><input type="password" name="data[repassword]" id="repass_field" {if $data.id}disabled{/if}></div>
	</div>
	<div class="row">
		<div class="h">{l i='field_fname' gid='users'}:&nbsp;* </div>
		<div class="v"><input type="text" name="data[fname]" value="{$data.fname|escape}"></div>
	</div>
	<div class="row">
		<div class="h">{l i='field_sname' gid='users'}:&nbsp;* </div>
		<div class="v"><input type="text" name="data[sname]" value="{$data.sname|escape}"></div>
	</div>
	<div class="row">
		<div class="h">{l i='field_phone' gid='users'}:&nbsp;* </div>
		<div class="v"><input type="text" name="data[phone]" value="{$data.phone|escape}" class="phone-field"></div>
	</div>
	<div class="row">
		<div class="h">{l i='field_company' gid='users'}:&nbsp;* </div>
		<div class="v"><input type="text" name="data[company_name]" value="{$data.company_name|escape}"></div>
	</div>
	<div class="row">
		<div class="h">{l i='field_icon' gid='users'}: </div>
		<div class="v">
			<input type="file" name="user_icon">
			{if $data.user_logo || $data.user_logo_moderation}
			<br><input type="checkbox" name="user_icon_delete" value="1" id="uichb"><label for="uichb">{l i='field_icon_delete' gid='users'}</label><br>
			{if $data.user_logo_moderation}<img src="{$data.media.user_logo_moderation.thumbs.middle}">{else}<img src="{$data.media.user_logo.thumbs.middle}">{/if}
			{/if}
		</div>
	</div>

	<div class="row">
		<div class="h">{l i='field_contact_email' gid='users'}: </div>
		<div class="v"><input type="text" name="data[contact_email]" value="{$data.contact_email|escape}"></div>
	</div>
	<div class="row">
		<div class="h">{l i='field_contact_phone' gid='users'}: </div>
		<div class="v"><input type="text" name="data[contact_phone]" value="{$data.contact_phone|escape}" class="phone-field"></div>
	</div>
	<div class="row">
		<div class="h">{l i='field_contact_info' gid='users'}: </div>
		<div class="v"><textarea rows="5" cols="80" name="data[contact_info]">{$data.contact_info|escape}</textarea></div>
	</div>
	<div class="row">
		<div class="h">{l i='field_web_url' gid='users'}: </div>
		<div class="v"><input type="text" name="data[web_url]" value="{$data.web_url|escape}"></div>
	</div>
	<div class="row">
		<div class="h">{l i='field_twitter' gid='users'}: </div>
		<div class="v"><input type="text" name="data[twitter]" value="{$data.twitter|escape}"></div>
	</div>
	<div class="row">
		<div class="h">{l i='field_facebook' gid='users'}: </div>
		<div class="v"><input type="text" name="data[facebook]" value="{$data.facebook|escape}"></div>
	</div>
	<div class="row">
		<div class="h">{l i='field_vkontakte' gid='users'}: </div>
		<div class="v"><input type="text" name="data[vkontakte]" value="{$data.vkontakte|escape}"></div>
	</div>

	<div class="row">
		<div class="h">{l i='field_working_days' gid='users'}: </div>
		<div class="v">			
			{foreach item=item key=key from=$weekdays.option}
			<input type="checkbox" name="data[working_days][]" value="{$key|escape}" id="working_days_{$key|escape}" {if $data.working_days && $key|in_array:$data.working_days}checked="checked"{/if} />
			<label for="working_days_{$key|escape}">{$item}</label>
			{/foreach}
		</div>
	</div>	
	<div class="row">
		<div class="h">{l i='field_working_hours' gid='users'}: </div>
		<div class="v">
			{l i='text_from' gid='users'}:
			<select name="data[working_hours_begin]">
				<option value="" {if !$data.workings_hours_begin}selected{/if}>{$dayhours.header}</option>
				{foreach item=item key=key from=$dayhours.option}
				<option value="{$key|escape}" {if $data.working_hours_begin eq $key}selected{/if}>{$item}</option>
				{/foreach}
			</select>
			{l i='text_till' gid='users'}:
			<select name="data[working_hours_end]">
				<option value="" {if !$data.workings_hours_end}selected{/if}>{$dayhours.header}</option>
				{foreach item=item key=key from=$dayhours.option}
				<option value="{$key|escape}" {if $data.working_hours_end eq $key}selected{/if}>{$item}</option>
				{/foreach}
			</select>
		</div>
	</div>
	<div class="row">
		<div class="h">{l i='field_lunch_time' gid='users'}: </div>
		<div class="v">
			{l i='text_from' gid='users'}:
			<select name="data[lunch_time_begin]">
				<option value="" {if !$data.lunch_time_begin}selected{/if}>{$dayhours.header}</option>
				{foreach item=item key=key from=$dayhours.option}
				<option value="{$key|escape}" {if $data.lunch_time_begin eq $key}selected{/if}>{$item}</option>
				{/foreach}
			</select>
			{l i='text_till' gid='users'}:
			<select name="data[lunch_time_end]">
				<option value="" {if !$data.lunch_time_end}selected{/if}>{$dayhours.header}</option>
				{foreach item=item key=key from=$dayhours.option}
				<option value="{$key|escape}" {if $data.lunch_time_end eq $key}selected{/if}>{$item}</option>
				{/foreach}
			</select>
		</div>
	</div>
	<div class="row">
		<div class="h">{l i='field_region' gid='users'}: </div>
		<div class="v">{country_select select_type='city' id_country=$data.id_country id_region=$data.id_region id_city=$data.id_city}</div>
	</div>
	<div class="row">
		<div class="h">{l i='field_address' gid='users'}: </div>
		<div class="v"><input type="text" name="data[address]" value="{$data.address|escape}"></div>
	</div>
	<div class="row">
		<div class="h">{l i='field_postal_code' gid='users'}: </div>
		<div class="v"><input type="text" name="data[postal_code]" value="{$data.postal_code|escape}"></div>
	</div>
	{if $data.id}
	<div class="row">
		<div class="h">{l i='field_contact_map' gid='users'}:</div>
		<div class="v">{block name=show_default_map module=geomap object_id=$data.id gid='user_profile' markers=$markers settings=$map_settings width=578 height=400 map_id=user_map}</div>
	</div>
	{/if}
	{if $use_services}
	<div class="row">
		<div class="h">{l i='field_services' gid='users'}: <br><br><br>
			<div class="select_actions">
				<a href="javascript:void(0)" id="services_select_all">{l i='select_all' gid='start'}</a>
				<a href="javascript:void(0)" id="services_unselect_all">{l i='unselect_all' gid='start'}</a>
			</div> 
		</div>
		<div class="v">
			{if $use_featured}
			<input type="hidden" name="services[featured]" value="0">
			<input type="checkbox" name="services[featured]" value="1" id="service_featured" class="width6" {if $data.is_featured}checked{/if}>
			<label for="service_featured" class="width6">{l i='field_service_featured_company' gid='users'}</label>
			{/if}
			{if $use_show_logo}
			<input type="hidden" name="services[show_logo]" value="0">
			<input type="checkbox" name="services[show_logo]" value="1" id="service_show_logo" class="width6" {if $data.is_show_logo}checked{/if}>
			<label for="service_show_logo" class="width6">{l i='field_service_show_logo' gid='users'}</label>
			{/if}
		</div>
		<script>{literal}
			$(function(){
				$('#services_select_all').bind('click', function(){
					$('input[name^=services]').attr('checked', 'checked');
				});
				$('#services_unselect_all').bind('click', function(){
					$('input[name^=services]').removeAttr('checked');
				});
			});
		{/literal}</script>
	</div>
	{/if}
	<div class="row">
		<div class="h">{l i='field_confirm' gid='users'}: </div>
		<div class="v"><input type="checkbox" name="data[confirm]" value="1" {if $data.confirm}checked{/if}></div>
	</div>
</div>
<div class="btn"><div class="l"><input type="submit" name="btn_save" value="{l i='btn_save' gid='start' type='button'}"></div></div>
<a class="cancel" href="{$back_url}">{l i='btn_cancel' gid='start'}</a>
<input type="hidden" name="map[view_type]" value="{$user_map_settings.view_type}" id="map_type">
<input type="hidden" name="map[zoom]" value="{$user_map_settings.zoom}" id="map_zoom">
<input type="hidden" name="data[lat]" value="{$data.lat|escape}" id="lat">
<input type="hidden" name="data[lon]" value="{$data.lon|escape}" id="lon">
</form>
{block name=geomap_load_geocoder module='geomap'}
<script>{literal}
	function update_coordinates(country, region, city, address, postal_code){
		if(typeof(geocoder) != 'undefined'){
			var location = geocoder.getLocationFromAddress(country, region, city, address, postal_code);
			geocoder.geocodeLocation(location, function(latitude, longitude){
				$('#lat').val(latitude);
				$('#lon').val(longitude);
				user_map.moveMarkers(latitude, longitude);
			});	
		}
	}
	$(function(){
		var location_change_wait = 0;
		var country_old = '{/literal}{$data.id_country}{literal}';
		var region_old = '{/literal}{$data.id_region}{literal}';
		var city_old = '{/literal}{$data.id_city}{literal}';
		var address_old = '{/literal}{'\''|str_replace:'\\\'':$data.address}{literal}';
		var postal_code_old = '{/literal}{"'"|str_replace:"\'":$data.postal_code}{literal}';
		
		$('input[name=id_city]').bind('change', function(){
			var city = $(this).val();
			if(city == 0) return;
			location_change_wait++;
			check_address_updated();
		});
		
		$('input[name=data\\[address\\]], input[name=data\\[postal_code\\]]').bind('keypress', function(){
			location_change_wait++;
			setTimeout(check_address_updated, 1000);
		});
		
		function check_address_updated(){
			location_change_wait--;
			if(location_change_wait) return;
			var country = $('input[name=id_country]').val();
			var region = $('input[name=id_region]').val();
			var city = $('input[name=id_city]').val();
			var address = $('input[name=data\\[address\\]]').val();
			var postal_code = $('input[name=data\\[postal_code\\]]').val();
			if(country == country_old && region == region_old && 
				city == city_old && address == address_old && postal_code == postal_code_old) return;
			country_old = country;
			region_old = region;
			city_old = city;
			address_old = address;
			postal_code_old = postal_code;
			var country_name = $('input[name=id_country]').attr('data-name');
			var region_name = $('input[name=id_region]').attr('data-name');
			var city_name = $('input[name=id_city]').attr('data-name');
			update_coordinates(country_name, region_name, city_name, address, postal_code);
		}
	});
{/literal}</script>
{if $phone_format}
{js file='jquery.maskedinput.min.js'}
<script>{literal}
$(function(){
	$('.phone-field').mask('{/literal}{$phone_format}{literal}');
});
{/literal}</script>
{/if}
<script>{literal}
	if(typeof(get_user_type_data) == 'undefined'){
		function get_user_type_data(type){
			$('#map_type').val(type);
		}
	}
	if(typeof(get_user_zoom_data) == 'undefined'){
		function get_user_zoom_data(zoom){
			$('#map_zoom').val(zoom);
		}
	}
	if(typeof(get_user_drag_data) == 'undefined'){
		function get_user_drag_data(point_gid, lat, lon){
			$('#lat').val(lat);
			$('#lon').val(lon);
		}
	}

	$(function(){
		$("div.row:odd").addClass("zebra");
		$("#pass_change_field").click(function(){
			if(this.checked){
				$("#pass_field").removeAttr("disabled");
				$("#repass_field").removeAttr("disabled");
			}else{
				$("#pass_field").attr('disabled', 'disabled'); $("#repass_field").attr('disabled', 'disabled');
			}
		});
	});
{/literal}</script>
