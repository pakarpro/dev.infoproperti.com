{include file="header.tpl"}
{helper func_name=get_admin_level1_menu helper_name=menu func_param='admin_users_settings_menu'}

<div class="actions">
	<ul>
		<li id="delete_all"><div class="l"><a href="{$site_url}admin/users/deactivated_alerts_delete">{l i='btn_delete' gid='start'}</a></div></li>
	</ul>
	&nbsp;
</div>

<form id="deactivated_alerts_form" action="" method="post">
<table cellspacing="0" cellpadding="0" class="data" width="100%">
	<tr>
		<th class="first w20"><input type="checkbox" id="grouping_all"></th>
		<th class="w100"><a href="{$sort_links.name}"{if $order eq 'name'} class="{$order_direction|lower}"{/if}>{l i='field_deactivated_alert_name' gid='users'}</a></th>
		<th class="w100"><a href="{$sort_links.email}"{if $order eq 'email'} class="{$order_direction|lower}"{/if}>{l i='field_deactivated_alert_email' gid='users'}</a></th>
		<th class="w100"><a href="{$sort_links.id_reason}"{if $order eq 'id_reason'} class="{$order_direction|lower}"{/if}>{l i='field_deactivated_alert_reason' gid='users'}</a></th>
		<th class="w50"><a href="{$sort_links.date_add}"{if $order eq 'date_add'} class="{$order_direction|lower}"{/if}>{l i='field_deactivated_alert_date_add' gid='users'}</a></th>
		<th class="w70">&nbsp;</th>
	</tr>
	{foreach item=item from=$alerts}
		{counter print=false assign=counter}
		<tr{if $counter is div by 2} class="zebra"{/if}>				
			<td class="center"><input type="checkbox" name="ids[]" class="grouping" value="{$item.id}" /></td>
			<td>{if !$item.status}<b>{/if}{$item.name|truncate:100}{if !$item.status}</b>{/if}</td>
			<td>{if !$item.status}<b>{/if}{$item.email|truncate:50}{if !$item.status}</b>{/if}</td>
			<td>{if !$item.status}<b>{/if}{$item.reason|truncate:50}{if !$item.status}</b>{/if}</td>
			<td class="center">{if !$item.status}<b>{/if}{$item.date_add|date_format:$page_data.date_format}{if !$item.status}</b>{/if}</td>
			<td class="icons">	
				<a href="{$site_url}admin/users/deactivated_alerts_show/{$item.id}"><img src="{$site_root}{$img_folder}icon-view.png" width="16" height="16" border="0" alt="{l i='link_alerts_show' gid='spam' type='button'}" title="{l i='link_deactivate_show' gid='users' type='button'}"></a>
				<a href="{$site_url}admin/users/deactivated_alerts_delete/{$item.id}" onclick="javascript: if(!confirm('{l i='note_deactivated_alert_delete' gid='users' type='js'}')) return false;"><img src="{$site_root}{$img_folder}icon-delete.png" width="16" height="16" border="0" alt="{l i='link_deactivated_alerts_delete' gid='users'}" title="{l i='link_deactivated_alerts_delete' gid='users'}"></a>
			</td>
		</tr>
	{foreachelse}
		<tr><td colspan="6" class="center">{l i='no_deactivated_alerts' gid='users'}</td></tr>
	{/foreach}
</table>
</form>
{include file="pagination.tpl"}

<script>{literal}
var reload_link = "{/literal}{$site_url}admin/users/deactivated_alerts/{literal}";
var order = '{/literal}{$order}{literal}';
var loading_content;
var order_direction = '{/literal}{$order_direction}{literal}';
$(function(){
	$('#grouping_all').bind('click', function(){
		var checked = $(this).is(':checked');
		if(checked){
			$('input[type=checkbox].grouping').attr('checked', 'checked');
		}else{
			$('input[type=checkbox].grouping').removeAttr('checked');
		}
	});
	$('#delete_all').bind('click', function(){
		if(!$('input[type=checkbox].grouping').is(':checked')) return false;
		if(this.id == 'delete_all' && !confirm('{/literal}{l i='note_deactivated_alerts_delete_all' gid='users' type='js'}{literal}')) return false;
		$('#deactivated_alerts_form').attr('action', $(this).find('a').attr('href')).submit();		
		return false;
	});

});
function reload_this_page(value){
	var link = reload_link + value + '/' + order + '/' + order_direction;
	location.href=link;
}
{/literal}</script>

{include file="footer.tpl"}
