<form action="{$data.action}" method="post" enctype="multipart/form-data" name="save_form">
<div class="edit-form n150">
<div class="row header">{if $data.id}{l i='admin_header_users_change' gid='users'}{else}{l i='admin_header_users_add' gid='users'}{/if}</div>
	<div class="row">
		<div class="h">{l i='field_user_type' gid='users'}: </div>
		<div class="v"><b>{$data.user_type_str}</b></div>
	</div>
	
	<div class="row">
		<div class="h">{l i='field_email' gid='users'}:&nbsp;* </div>
		<div class="v"><input type="text" name="data[email]" value="{$data.email|escape}"></div>
	</div>
	{if $data.id}
	<div class="row">
		<div class="h">{l i='field_change_password' gid='users'}: </div>
		<div class="v"><input type="checkbox" value="1" name="update_password" id="pass_change_field"></div>
	</div>
	{/if}
	<div class="row">
		<div class="h">{l i='field_password' gid='users'}:{if !$data.id}&nbsp;*{/if} </div>
		<div class="v"><input type="password" name="data[password]" id="pass_field" {if $data.id}disabled{/if}></div>
	</div>
	<div class="row">
		<div class="h">{l i='field_repassword' gid='users'}:{if !$data.id}&nbsp;*{/if} </div>
		<div class="v"><input type="password" name="data[repassword]" id="repass_field" {if $data.id}disabled{/if}></div>
	</div>
	<div class="row">
		<div class="h">{l i='field_fname' gid='users'}:&nbsp;* </div>
		<div class="v"><input type="text" name="data[fname]" value="{$data.fname|escape}"></div>
	</div>
	<div class="row">
		<div class="h">{l i='field_sname' gid='users'}:&nbsp;* </div>
		<div class="v"><input type="text" name="data[sname]" value="{$data.sname|escape}"></div>
	</div>
	<div class="row">
		<div class="h">{l i='field_phone' gid='users'}:&nbsp;* </div>
		<div class="v"><input type="text" name="data[phone]" value="{$data.phone|escape}" class="phone-field"></div>
	</div>
	<div class="row">
		<div class="h">{l i='field_icon' gid='users'}: </div>
		<div class="v">
			<input type="file" name="user_icon">
			{if $data.user_logo || $data.user_logo_moderation}
			<br><input type="checkbox" name="user_icon_delete" value="1" id="uichb"><label for="uichb">{l i='field_icon_delete' gid='users'}</label><br>
			{if $data.user_logo_moderation}<img src="{$data.media.user_logo_moderation.thumbs.middle}">{else}<img src="{$data.media.user_logo.thumbs.middle}">{/if}
			{/if}
		</div>
	</div>
	<div class="row">
		<div class="h">{l i='field_contact_email' gid='users'}: </div>
		<div class="v"><input type="text" name="data[contact_email]" value="{$data.contact_email|escape}"></div>
	</div>
	<div class="row">
		<div class="h">{l i='field_contact_phone' gid='users'}: </div>
		<div class="v"><input type="text" name="data[contact_phone]" value="{$data.contact_phone|escape}" class="phone-field"></div>
	</div>
	<div class="row">
		<div class="h">{l i='field_contact_info' gid='users'}: </div>
		<div class="v"><textarea rows="5" cols="80" name="data[contact_info]">{$data.contact_info|escape}</textarea></div>
	</div>
	<div class="row">
		<div class="h">{l i='field_twitter' gid='users'}: </div>
		<div class="v"><input type="text" name="data[twitter]" value="{$data.twitter|escape}"></div>
	</div>
	<div class="row">
		<div class="h">{l i='field_facebook' gid='users'}: </div>
		<div class="v"><input type="text" name="data[facebook]" value="{$data.facebook|escape}"></div>
	</div>
	<div class="row">
		<div class="h">{l i='field_vkontakte' gid='users'}: </div>
		<div class="v"><input type="text" name="data[vkontakte]" value="{$data.vkontakte|escape}"></div>
	</div>
	<div class="row">
		<div class="h">{l i='field_confirm' gid='users'}: </div>
		<div class="v"><input type="checkbox" name="data[confirm]" value="1" {if $data.confirm}checked{/if}></div>
	</div>
</div>
<div class="btn"><div class="l"><input type="submit" name="btn_save" value="{l i='btn_save' gid='start' type='button'}"></div></div>
<a class="cancel" href="{$back_url}">{l i='btn_cancel' gid='start'}</a>
</form>
<div class="clr"></div>
{if $phone_format}
{js file='jquery.maskedinput.min.js'}
<script>{literal}
$(function(){
	$('.phone-field').mask('{/literal}{$phone_format}{literal}');
});
{/literal}</script>
{/if}

{js file='jquery-ui.custom.min.js'}
<link href='{$site_root}{$js_folder}jquery-ui/jquery-ui.custom.css' rel='stylesheet' type='text/css' media='screen' />
<script type='text/javascript'>{literal}
    $(function(){
		now = new Date();
		yr =  (new Date(now.getYear() - 80, 0, 1).getFullYear()) + ':' + (new Date(now.getYear() - 18, 0, 1).getFullYear());
           $( "#datepicker" ).datepicker({
			dateFormat :'yy-mm-dd',
			changeYear: true,
			changeMonth: true,
			yearRange: yr
		});
    });
	$(function(){
		$("div.row:odd").addClass("zebra");
		$("#pass_change_field").click(function(){
			if(this.checked){
				$("#pass_field").removeAttr("disabled");
				$("#repass_field").removeAttr("disabled");
			}else{
				$("#pass_field").attr('disabled', 'disabled'); $("#repass_field").attr('disabled', 'disabled');
			}
		});
	});
{/literal}</script>
