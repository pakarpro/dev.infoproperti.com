{if $users}
{if $users_page_data.header}<h2>{$users_page_data.header}</h2>{/if}
{js file='jcarousellite.min.js'}
{js file='init_carousel_controls.js'}
<script type="text/javascript">{literal}
$(function(){
	$('.carousel_block').jCarouselLite({
		visible: {/literal}{$users_page_data.visible}{literal},
		btnNext: '.directionright',
		btnPrev: '.directionleft',
		circular:false,
		afterEnd: function(a) {
			var index = $(a[0]).index();
			carousel_controls.update_controls(index);
		}
	});

	carousel_controls = new init_carousel_controls({
		carousel_images_count: {/literal}{$users_page_data.visible}{literal},
		carousel_total_images:{/literal}{$users_page_data.count}{literal},
		btnNext: '#directionright',
		btnPrev: '#directionleft'
	});
});
{/literal}</script>
<div class="carousel">
	<div id="directionleft" class="directionleft inactive">
		<div class="with-icon i-larr w" id="l_hover"></div>
	</div>
	<div class="carousel_block item_{$users_page_data.visible}_info">
	<ul>
	{foreach from=$users item=item}
		<li><div class="ptoto_prof"><a href="{seolink module='users' method='view' data=$item}"><img src="{$item.media.user_logo.thumbs.middle}"></a></div></li>
		{/foreach}
	</ul>
	</div>
	<div id="directionright" class="directionright inactive">
		<div class="with-icon i-rarr w" id="r_hover"></div>
	</div>
	<div class="clr"></div>
</div>
{/if}
