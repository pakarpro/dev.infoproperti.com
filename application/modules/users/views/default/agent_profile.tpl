<p class="header-comment">
{l i="text_register_as" gid='users'}: <b>{$data.user_type_str}</b><br>
{l i="field_date_created" gid='users'}: <b>{$data.date_created|date_format:$page_data.date_format}</b>
</p>

<h2 class="line top bottom linked">
	{l i='table_header_personal' gid='users'}
	<a class="btn-link fright" href="{$site_url}users/profile/personal/"><ins class="with-icon i-edit"></ins></a>
</h2>
<div class="view-section">
	<div class="r">
		<div class="f">{l i='field_fname' gid='users'}:&nbsp;*</div>
		<div class="v">{$data.fname}</div>
	</div>
	<div class="r">
		<div class="f">{l i='field_sname' gid='users'}:&nbsp;*</div>
		<div class="v">{$data.sname}</div>
	</div>
	<div class="r">
		<div class="f">{l i='field_phone' gid='users'}:&nbsp;*</div>
		<div class="v">{if $data.phone}{$data.phone}{else}{l i='no_information' gid='users'}{/if}</div>
	</div>
	<div class="r">
		<div class="f">{l i='field_icon' gid='users'}:</div>
		<div class="v">
		{if $data.user_logo_moderation}
		<img src="{$data.media.user_logo_moderation.thumbs.middle}" alt="{$data.output_name|truncate:30|escape}">
		{else}
		<img src="{$data.media.user_logo.thumbs.middle}" alt="{$data.output_name|escape}">
		{/if}
		</div>
	</div>
</div>

<h2 class="line top bottom linked">
	{l i='table_header_company' gid='users'}
	<a class="btn-link fright" href="{$site_url}users/profile/company/"><ins class="with-icon i-edit"></ins></a>
</h2>
<div class="view-section">
	{if $data.agent_company}
	<div class="r">
		<div class="f">{l i='field_company' gid='users'}:</div>
		<div class="v">{$data.company.output_name}</div>
	</div>
		{if !$data.agent_status}
		<div class="r">
			<div class="f">{l i='field_company_request_status' gid='users'}:</div>
			<div class="v">{l i='awaiting_aproval_since' gid='users'} {$data.agent_date|date_format:$page_data.date_format}</div>
		</div>
		{else}
		{if $data.company.contact_email}
		<div class="r">
			<div class="f">{l i='field_contact_email' gid='users'}:</div>
			<div class="v">{$data.company.contact_email}</div>
		</div>
		{/if}
		{if $data.company.contact_phone}
		<div class="r">
			<div class="f">{l i='field_contact_phone' gid='users'}:</div>
			<div class="v">{$data.company.contact_phone}</div>
		</div>
		{/if}
		{if $data.company.contact_info}
		<div class="r">
			<div class="f">{l i='field_contact_info' gid='users'}:</div>
			<div class="v">{$data.company.contact_info|nl2br}</div>
		</div>
		{/if}
		{if $data.company.web_url}
		<div class="r">
			<div class="f">{l i='field_web_url' gid='users'}:</div>
			<div class="v">{$data.company.web_url}</div>
		</div>
		{/if}
		{if $data.company.facebook}
		<div class="r">
			<div class="f">{l i='field_facebook' gid='users'}:</div>
			<div class="v">{$data.company.facebook}</div>
		</div>
		{/if}
		{if $data.company.twitter}
		<div class="r">
			<div class="f">{l i='field_twitter' gid='users'}:</div>
			<div class="v">{$data.company.twitter}</div>
		</div>
		{/if}
		{if $data.company.vkontakte}
		<?php /* disable for now
		<div class="r">
			<div class="f">{l i='field_vkontakte' gid='users'}:</div>
			<div class="v">{$data.company.vkontakte}</div>
		</div>
		*/?>
		{/if}
		{/if}
	{else}
	<div class="r">
		<div class="f">{l i='field_company' gid='users'}:</div>
		<div class="v">{l i='no_information' gid='users'}</div>
	</div>
	{/if}

</div>

<h2 class="line top bottom linked">
	{l i='table_header_contact' gid='users'}
	<a class="btn-link fright" href="{$site_url}users/profile/contact/"><ins class="with-icon i-edit"></ins></a>
</h2>
<div class="view-section">
	<div class="r">
		<div class="f">{l i='field_contact_email' gid='users'}:</div>
		<div class="v">{if $data.contact_email}{$data.contact_email}{else}{l i='no_information' gid='users'}{/if}</div>
	</div>
	<div class="r">
		<div class="f">{l i='field_contact_phone' gid='users'}:</div>
		<div class="v">{if $data.contact_phone}{$data.contact_phone}{else}{l i='no_information' gid='users'}{/if}</div>
	</div>
	<div class="r">
		<div class="f">{l i='field_contact_info' gid='users'}:</div>
		<div class="v">{if $data.contact_info}{$data.contact_info|nl2br}{else}{l i='no_information' gid='users'}{/if}</div>
	</div>
	<div class="r">
		<div class="f">{l i='field_facebook' gid='users'}:</div>
		<div class="v">{if $data.facebook}{$data.facebook}{else}{l i='no_information' gid='users'}{/if}</div>
	</div>
	<div class="r">
		<div class="f">{l i='field_twitter' gid='users'}:</div>
		<div class="v">{if $data.twitter}{$data.twitter}{else}{l i='no_information' gid='users'}{/if}</div>
	</div>
	<?php /* disable for now
	<div class="r">
		<div class="f">{l i='field_vkontakte' gid='users'}:</div>
		<div class="v">{if $data.vkontakte}{$data.vkontakte}{else}{l i='no_information' gid='users'}{/if}</div>
	</div>
	*/ ?>
</div>

{helper func_name=get_user_subscriptions_list module=subscriptions}
