{l i='no_information' gid='users' assign='no_info_str'}
{l i='please_buy' gid='users' assign='buy_str'}
{l i='not_access' gid='users' assign='not_access_str'}

{if $section_gid == 'overview'}
	
{/if}
{if $section_gid == 'company'}
	
{/if}
{if $section_gid == 'reviews'}
	{block name=get_reviews_block module=reviews object_id=$user.id type_gid='users_object'}
{/if}
{if $section_gid == 'contacts'}
	{block name=view_user_block module=users user_id=$user.id user=$user}
{/if}
{if $section_gid == 'map'}
	{if !$ajax}
    {literal}
	<script>
	if(typeof(get_user_geocode_data) != 'function'){
	    function get_user_geocode_data(point_gid, lat, lon){
		$.ajax({
		    url: '{/literal}{$site_url}{literal}users/ajax_set_geocode_coordinates/'+point_gid,
		    data: 'lat='+lat+'&lon='+lon,
		    type: 'post'
		});
	    }
	}
	</script>
	{/literal}
	{block name=show_default_map module=geomap gid='user_profile' id_user=$user_id object_id=$user.id markers=$markers settings=$map_settings width='630' height='500' only_load_scripts=1}	
	{/if}
   
	{if $user.no_access_contact}
		<div class="r gray_italic">{$not_access_str}</div>
	{elseif $user.is_contact}
		{block name=show_default_map module=geomap gid='user_profile' id_user=$user_id object_id=$user.id markers=$markers settings=$map_settings width='630' height='500' only_load_content=1}
	{else}
		<div class="r gray_italic">{$buy_str}</div>
	{/if}
	{if !$user.is_contact && !$user.no_access_contact}
	<div class="buy-box"><input type="button" value="{l i='btn_activate_contact' gid='services'}" name="map_btn" /></div>
	{/if}
{/if}
{if $section_gid == 'map_info'}
    <div id="map_container_info">
	{if $user.no_access_contact}
		<div class="r gray_italic">{$not_access_str}</div>
	{elseif $user.is_contact}
		{block name=show_default_map module=geomap gid='user_info' id_user=$user_id object_id=$user.id markers=$markers settings=$map_settings width='300' height='300' only_load_content=1}
	{else}
	   <div class="r gray_italic">{$buy_str}</div>
	{/if}
	{if !$user.is_contact && !$user.no_access_contact}
	<div class="buy-box"><input type="button" value="{l i='btn_activate_contact' gid='services'}" name="map_info_btn" /></div>
	{/if}
    </div>
{/if}
{if $section_gid == 'listings'}
{/if}
