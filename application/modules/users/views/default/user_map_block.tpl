	{if $view_mode eq 'map'}
		{assign var='current_user_id' value=$user.id}
			<div class="user-block">
				<div class="item no-hover no-border user">
					<h3><a href="{seolink module='users' method='view' data=$user}" title="{$user.output_name|escape}">{$user.output_name|truncate:50}</a>, {l i=$user.user_type gid='users'}</h3>
					<div class="image">
						<a href="{seolink module='users' method='view' data=$user}"><img src="{$user.media.user_logo.thumbs.middle}" title="{$user.output_name|escape}"></a>
					</div>
					<div class="body">
						{if $user_type eq "agent" && $user.company}<h3>{$user.company.output_name|truncate:50}</h3>{/if}
						{if $is_contact || $users_for_contact[$current_user_id]}<h3>{$user.phone}</h3>{/if}
						<div class="t-1"></div>
						<div class="t-2">
							<span class="status_text">{if $user.is_featured}{l i='status_featured' gid='users'}{/if}</span>
						</div>		
						<div class="t-3">
							{if $user.user_type eq 'company' && $user.agent_count}<span>{l i='field_agent_count' gid='users'}:</span> {$user.agent_count}<br>{/if}
							{if $user.listings_for_sale_count}<span>{l i='field_listings_for_sale' gid='users'}:</span> {$user.listings_for_sale_count}<br>{/if}
							{if $user.listings_for_rent_count}<span>{l i='field_listings_for_rent' gid='users'}:</span> {$user.listings_for_buy_count}<br>{/if}
						</div>
						<div class="t-4">
							{depends module=reviews}
							<span>{l i='field_rate' gid='users'}:</span> {$user.review_sorter}<br>
							<span>{l i='field_reviews_count' gid='users'}:</span> {$user.review_count}<br>
							{/depends}
						</div>
					</div>
					<div class="clr"></div>
				</div>
			</div>
	{else}
		<a href="{seolink module='users' method='view' data=$user}" title="{$user.output_name|escape}">{$user.output_name|truncate:20}</a>, {l i=$user.user_type gid='users'}
	{/if}
