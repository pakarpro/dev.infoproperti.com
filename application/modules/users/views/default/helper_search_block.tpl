{js module=users file='users-search.js'}
<div class="users_form edit_block">
	<h2>{l i='header_search_users' gid='users'}</h2>
	<form id="search_users_form" name="search_user_form" action="" method="POST">
	{ld i='professionals' gid='users' assign='professinals'}
	{if $user_types}
	<div class="r">
		<div class="f">{l i='field_user_type' gid='users'}:</div>
		<div class="v">
			<select name="user_type" id="user_type">
				{foreach item=item from=$user_types}
			    <option value="{$item|escape}" {if $item eq $user_type}selected{/if}>{ld_option i='professionals' gid='users' option=$item}</option>
				{/foreach}
			</select>
			<script>{literal}
				$(function(){
					$('#user_type').bind('change', function(){
						if($(this).val() == 'company'){
							$('#regionbox').show();
							$('#postalcodebox').show();
						}else{
							$('#regionbox').hide();
							$('#postalcodebox').hide();
						}
					});
				});
			{/literal}</script>
		</div>
	</div>	
	{/if}
	<div class="r {if $user_type ne 'company'}hide{/if}" id="regionbox">
		<div class="f">{l i='field_region' gid='users'}:</div>
		<div class="v">{country_input select_type='city' id_country=$data.country id_region=$data.region id_city=$data.city}</div>
	</div>
	<div class="r {if $user_type ne 'company'}hide{/if}" id="postalcodebox">
		<div class="f">{l i='field_postal_code' gid='users'}:</div>
		<div class="v"><input type="text" name="zip" value="{$data.zip|escape}" /></div>
	</div>
	<div class="r">
		<div class="f">{l i='field_keyword' gid='users'}:</div>
		<div class="v"><input type="text" name="keyword" value="{$data.keyword|escape}" /></div>
	</div>
	<div class="r">
		<input type="submit" value="{l i='btn_search' gid='start' type='button'}" />
	</div>
	</form>
</div>
<script type='text/javascript'>{literal}
	var uSearch;
	$(function(){
		uSearch = new userSearch({
			siteUrl: '{/literal}{$site_url}{literal}',
		});
	});
{/literal}</script>
