{include file="header.tpl" load_type='ui'}
	<div class="content-block">

		<h1>{seotag tag='header_text'}</h1>
		<p class="header-comment">{l i='text_register' gid='users'}</p>
		<div class="edit_block">
			{include file=$user_type"_register.tpl" module="users" theme="default"}
		</div>
		
		{block name=show_social_networks_like module=social_networking}
		{block name=show_social_networks_share module=social_networking}
		{block name=show_social_networks_comments module=social_networking}
	</div>
{include file="footer.tpl"}
