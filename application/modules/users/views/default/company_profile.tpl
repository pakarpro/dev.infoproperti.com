<p class="header-comment">
{l i="text_register_as" gid='users'}: <b>{$data.user_type_str}</b><br>
{l i="field_date_created" gid='users'}: <b>{$data.date_created|date_format:$page_data.date_format}</b>
</p>

<h2 class="line top bottom linked">
	{l i='table_header_personal' gid='users'}
	<a class="btn-link fright" href="{$site_url}users/profile/personal/"><ins class="with-icon i-edit"></ins></a>
</h2>
<div class="view-section">
	<div class="r">
		<div class="f">{l i='field_fname' gid='users'}:</div>
		<div class="v">{$data.fname}</div>
	</div>
	<div class="r">
		<div class="f">{l i='field_sname' gid='users'}:</div>
		<div class="v">{$data.sname}</div>
	</div>
	<div class="r">
		<div class="f">{l i='field_phone' gid='users'}:</div>
		<div class="v">{$data.phone}</div>
	</div>
	<div class="r">
		<div class="f">{l i='field_company' gid='users'}:</div>
		<div class="v">{$data.company_name}</div>
	</div>
	<div class="r">
		<div class="f">{l i='field_icon' gid='users'}:</div>
		<div class="v">
		{if $data.user_logo_moderation}
		<img src="{$data.media.user_logo_moderation.thumbs.middle}" alt="{$data.output_name|truncate:30|escape}" />{else}
		<img src="{$data.media.user_logo.thumbs.middle}" alt="{$data.output_name|escape}" />
		{/if}
		</div>
	</div>
</div>


<h2 class="line top bottom linked">
	{l i='table_header_contact' gid='users'}
	<a class="btn-link fright" href="{$site_url}users/profile/contact/"><ins class="with-icon i-edit"></ins></a>
</h2>
<div class="view-section">
	<div class="r">
		<div class="f">{l i='field_contact_email' gid='users'}:</div>
		<div class="v">{if $data.contact_email}{$data.contact_email}{else}{l i='no_information' gid='users'}{/if}</div>
	</div>
	<div class="r">
		<div class="f">{l i='field_contact_phone' gid='users'}:</div>
		<div class="v">{if $data.contact_phone}{$data.contact_phone}{else}{l i='no_information' gid='users'}{/if}</div>
	</div>
	<div class="r">
		<div class="f">{l i='field_contact_info' gid='users'}:</div>
		<div class="v">{if $data.contact_info}{$data.contact_info|nl2br}{else}{l i='no_information' gid='users'}{/if}</div>
	</div>
	<div class="r">
		<div class="f">{l i='field_web_url' gid='users'}:</div>
		<div class="v">{if $data.web_url}{$data.web_url}{else}{l i='no_information' gid='users'}{/if}</div>
	</div>
	
	<div class="r">
		<div class="f">{l i='field_facebook' gid='users'}:</div>
		<div class="v">{if $data.facebook}{$data.facebook}{else}{l i='no_information' gid='users'}{/if}</div>
	</div>
	<div class="r">
		<div class="f">{l i='field_twitter' gid='users'}:</div>
		<div class="v">{if $data.twitter}{$data.twitter}{else}{l i='no_information' gid='users'}{/if}</div>
	</div>
	<?php /* disable for now
	<div class="r">
		<div class="f">{l i='field_vkontakte' gid='users'}:</div>
		<div class="v">{if $data.vkontakte}{$data.vkontakte}{else}{l i='no_information' gid='users'}{/if}</div>
	</div>
	*/ ?>
	<div class="r">
		<div class="f">{l i='field_working_days' gid='users'}:</div>
		<div class="v">{if $data.working_days_str}{$data.working_days_str}{else}{l i='no_information' gid='users'}{/if}
		</div>
	</div>
	<div class="r">
		<div class="f">{l i='field_working_hours' gid='users'}:</div>
		<div class="v">{if $data.working_hours_str}{$data.working_hours_str}{else}{l i='no_information' gid='users'}{/if}
		</div>
	</div>
	<div class="r">
		<div class="f">{l i='field_lunch_time' gid='users'}:</div>
		<div class="v">
			{if $data.lunch_time_str}{$data.lunch_time_str}{else}{l i='no_information' gid='users'}{/if}</div>
	</div>
	
</div>
<h2 class="line top bottom linked">
	{l i='table_header_location' gid='users'}
	<a class="btn-link fright" href="{$site_url}users/profile/location/"><ins class="with-icon i-edit"></ins></a>
</h2>
<div class="view-section">
	<div class="r">
		<div class="f">{l i='field_location' gid='users'}:</div>
		<div class="v">{if $data.location}{$data.location}{else}{l i='no_information' gid='users'}{/if}</div>
	</div>
	<div class="r">
		<div class="f">{l i='field_postal_code' gid='users'}:</div>
		<div class="v">{if $data.postal_code}{$data.postal_code}{else}{l i='no_information' gid='users'}{/if}</div>
	</div>
	{if $data.id_country && $data.id_region && $data.id_city}
	<div class="r">
		<div class="f">{l i='field_contact_map' gid='users'}:</div>
		<div class="v">{block name=show_default_map module=geomap object_id=$data.id gid='user_profile' markers=$markers settings=$map_settings width='559' height='400'}</div>
	</div>
	{/if}	
</div>
{helper func_name=get_user_subscriptions_list module=subscriptions}
