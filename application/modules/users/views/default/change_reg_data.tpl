{include file="header.tpl"}
{include file="left_panel.tpl" module="start"}
<div class="rc">

	<div class="content-block">

		<h1>{l i='text_change_regdata' gid='users'}</h1>
		
		<h2 class="line top bottom">{l i='text_change_email' gid='users'}</h2>
		<div class="view-section">
			<div class="edit_block">
			<form action="" method="post">
				<div class="r">
					<div class="f">{l i='field_current_email' gid='users'}:</div>
					<div class="v"><b>{$data.email}</b></div>
				</div>
				<div class="r">
					<div class="f">{l i='field_new_email' gid='users'}:&nbsp;* </div>
					<div class="v"><input type="text" name="email" value="" autocomplite="off"></div>
				</div>
				<div class="r">
					<div class="f">&nbsp;</div>
					<div class="v"><input type="submit" class="btn" value="{l i='btn_send' gid='start'  type='button'}" name="btn_email_save"></div>
				</div>
			</form>
			</div>
			<div class="edit_block">{helper func_name=show_social_networking_link module=users_connections}</div>
		</div>
		
		{if $phone_format}
			{js file='jquery.maskedinput.min.js'}
			<script>{literal}
				$(function(){
					$('#phone').mask('{/literal}{$phone_format}{literal}');
				});
			{/literal}</script>
		{/if}
		<h2 class="line top bottom">{l i='text_change_phone' gid='users'}</h2>
		<div class="view-section">
			<div class="edit_block">
			<form action="" method="post">
				<div class="r">
					<div class="f">{l i='field_current_phone' gid='users'}:</div>
					<div class="v"><b>{$data.phone}</b></div>
				</div>
				<div class="r">
					<div class="f">{l i='field_new_phone' gid='users'}:&nbsp;* </div>
					<div class="v"><input type="text" name="phone" value="" autocomplite="off" id="phone"></div>
				</div>
				<div class="r">
					<div class="f">&nbsp;</div>
					<div class="v"><input type="submit" class="btn" value="{l i='btn_send' gid='start'  type='button'}" name="btn_phone_save"></div>
				</div>
			</form>
			</div>
			<div class="edit_block">{helper func_name=show_social_networking_link module=users_connections}</div>
		</div>

		<h2 class="line top bottom">{l i='text_change_password' gid='users'}</h2>
		<div class="view-section">
			<div class="edit_block">
			<form action="" method="post">
				<div class="r">
					<div class="f">{l i='field_password_old' gid='users'}:&nbsp;* </div>
					<div class="v"><input type="password" name="password_old" value=""></div>
				</div>
				<div class="r">
					<div class="f">{l i='field_password' gid='users'}:&nbsp;* </div>
					<div class="v"><input type="password" name="password" value=""></div>
				</div>
				<div class="r">
					<div class="f">{l i='field_repassword' gid='users'}:&nbsp;* </div>
					<div class="v"><input type="password" name="repassword" value=""></div>
				</div>
				<div class="r">
					<div class="f">&nbsp;</div>
					<div class="v"><input type="submit" class="btn" value="{l i='btn_send' gid='start'  type='button'}" name="btn_passw_save"></div>
				</div>
			</form>
			</div>		
		</div>	
			
	</div>
</div>
<div class="clr"></div>
{include file="footer.tpl"}

