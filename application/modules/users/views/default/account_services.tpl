{include file="header.tpl"}

{include file="left_panel.tpl" module="start"}

<div class="rc" id="account">
	<h1>{l i='header_account_services' gid='users'}</h1>
	{helper func_name=user_featured_services module=users_services}
	{helper func_name=user_show_logo_services module=users_services}
	<br>
</div>
<div class="clr"></div>
{include file="footer.tpl"}
