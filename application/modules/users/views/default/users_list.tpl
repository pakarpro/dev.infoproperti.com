{include file="header.tpl"}

{capture assign='right_block'}
	{if $use_map_in_search}{block name=show_default_map module=geomap id_user=$data.id gid='user_search' markers=$markers settings=$map_settings width='300' height='300' map_id='users_map_container'}{/if}
	{helper func_name=show_banner_place module=banners func_param='right-banner'}
{/capture}

{capture assign='content_block'}
<div class="rc">
	
	{capture assign='main_block'}
	<div class="content-block">
		{if $use_map}<div id="map_mode_link">{helper func_name=show_map_view module=geomap func_param=$site_url'users/set_view_mode/map'}</div>{/if}
		
		<h1>{l i='header_users_result' gid='users'} - <span id="total_rows">{$page_data.total_rows}</span> {l i='header_users_found' gid='users'}</h1>
		
		<div class="tabs tab-size-15 hide noPrint">
			<ul id="search_users_sections">
				{foreach item=tgid from=$user_types}
				<li id="m_{$tgid}" sgid="{$tgid}" class="{if $current_user_type eq $tgid}active{/if}"><a href="{$menu_action_link|replace:'[user_type]':$tgid}">{l i=$tgid gid='users'}</a></li>
				{/foreach}
			</ul>
		</div>
		
		<div id="users_block">{$block}</div>
		{js module=users file='users-list.js'}
		<script>{literal}
			$(function(){
				new usersList({
					siteUrl: '{/literal}{$site_url}{literal}',
					listAjaxUrl: '{/literal}users/ajax_users{literal}',
					userType: '{/literal}{$current_user_type}{literal}',
					sectionId: 'search_users_sections',
					order: '{/literal}{$order}{literal}',
					orderDirection: '{/literal}{$order_direction}{literal}',
					page: {/literal}{$page}{literal},
					tIds: ['pages_block_1', 'pages_block_2', 'sorter_block']
				});
			});
		{/literal}</script>
	</div>
	{/capture}
	
	{if $right_block|trim}
	<div class="rc-1">
		{$main_block}
	</div>
	<div class="rc-2">
		{$right_block}
	</div>
	{else}
		{$main_block}
	{/if}
</div>

<div class="lc">
	<div class="inside account_menu">
		{block name=users_search_block module=users}
		{if $use_poll_in_search}{block name=show_poll_place_block module=polls one_poll_place=0}{/if}
		{helper func_name=show_banner_place module=banners func_param='big-left-banner'}
		{helper func_name=show_banner_place module=banners func_param='left-banner'}
	</div>
</div>

<div class="clr"></div>
{/capture}

{if $right_block|trim}
<div class="rc_wrapper">
	<div class="panel">
		<div class="inside">
			{$content_block}
		</div>
	</div>
</div>
{else}
	{$content_block}
{/if}

{include file="footer.tpl"}
