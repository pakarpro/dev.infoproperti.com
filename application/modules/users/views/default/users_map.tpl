{include file="header.tpl"}

<div class="rc">
	<div class="content-block">
		<div id="list_mode_link"><a href="{$site_url}users/set_view_mode/list" class="btn-link fright" title="{l i='link_view_list' gid='users' type='button'}"><ins class="with-icon i-list"></ins></a></div>
		
		<h1>{l i='header_users_result' gid='users'} - <span id="total_rows">{$page_data.total_rows}</span> {l i='header_users_found' gid='users'}</h1>

		<div class="tabs tab-size-15 hide noPrint">
			<ul id="search_users_sections">
				{foreach item=tgid from=$user_types}
				<li id="m_{$tgid}" sgid="{$tgid}" class="{if $current_user_type eq $tgid}active{/if}"><a href="{$menu_action_link|replace:'[user_type]':$tgid}">{l i=$tgid gid='users'}</a></li>
				{/foreach}
			</ul>
		</div>

		{if $users}
		<div class="sorter line" id="sorter_block">
			{sorter links=$sort_data.links order=$sort_data.order direction=$sort_data.direction url=$sort_data.url}
			<div class="fright" id="pages_block_1">{pagination data=$page_data type='cute'}</div>
		</div>
		{/if}
	
		<div id="users_map">
		{if $users}
		{block name=show_default_map module=geomap id_user=$data.id gid='user_search' settings=$map_settings width='755' height='400' map_id='users_map_full_container'}
		{$block}
		{else}
		<div class="item empty">{l i='empty_users' gid='users'}</div>
		{/if}
		</div>

		{if $users}<div id="pages_block_2">{pagination data=$page_data type='full'}</div>{/if}
		
		{js module=users file='users-map.js'}
		<script>{literal}
		var usersMap;
		$(function(){
			usersMap = new usersMap({
				siteUrl: '{/literal}{$site_url}{literal}',
				mapAjaxUrl: '{/literal}users/ajax_users{literal}',
				userType: '{/literal}{$current_user_type}{literal}',
				sectionId: 'search_users_sections',
				order: '{/literal}{$order}{literal}',
				orderDirection: '{/literal}{$order_direction}{literal}',
				page: {/literal}{$page}{literal},
				tIds: ['pages_block_1', 'pages_block_2', 'sorter_block']
			});
		});
		{/literal}</script>
	</div>
</div>

<div class="lc">
	<div class="inside account_menu">
		{block name=users_search_block module=users}
		{if $use_poll_in_search}{block name=show_poll_place_block module=polls one_poll_place=0}{/if}
		{helper func_name=show_banner_place module=banners func_param='big-left-banner'}
		{helper func_name=show_banner_place module=banners func_param='left-banner'}
	</div>
</div>

<div class="clr"></div>

{include file="footer.tpl"}
