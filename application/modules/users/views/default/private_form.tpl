{if $action eq 'personal'}
<h1>{l i='table_header_personal' gid='users'}</h1>

<div class="edit_block">
<form action="" method="post" enctype="multipart/form-data">
<div class="r">
		<div class="f">{l i='field_fname' gid='users'}:&nbsp;<span class="ast">*</span> </div>
		<div class="v"><input type="text" name="data[fname]" value="{$data.fname|escape}"></div>
	</div>
	<div class="r">
		<div class="f">{l i='field_sname' gid='users'}:<!--&nbsp;*--> </div>
		<div class="v"><input type="text" name="data[sname]" value="{$data.sname|escape}"></div>
	</div>
	<div class="r">
		<div class="f">{l i='field_phone' gid='users'}:&nbsp;<span class="ast">*</span> </div>
		<div class="v"><input type="text" name="data[phone]" value="{$data.phone|escape}" class="phone-field"></div>
	</div>
	<div class="r">
		<div class="f">{l i='field_icon' gid='users'}: </div>
		<div class="v">
			<input type="file" name="user_icon">
			{if $data.user_logo || $data.user_logo_moderation}
			<br><input type="checkbox" name="user_icon_delete" value="1" id="uichb"><label for="uichb">{l i='field_icon_delete' gid='users'}</label><br>
			{if $data.user_logo_moderation}<img src="{$data.media.user_logo_moderation.thumbs.middle}">{else}<img src="{$data.media.user_logo.thumbs.middle}">{/if}

			{/if}
		</div>
	</div>
	<div class="r">
		<div class="f">&nbsp;</div>
		<div class="v">
			<input type="submit" value="{l i='btn_save' gid='start' type='button'}" name="btn_register" class="btn" />
			<a href="{seolink module='users' method='profile'}" class="btn-link fleft"><ins class="with-icon i-larr"></ins>{l i='back_to_my_profile' gid='users'}</a>
		</div>
	</div>
	</form>
</div>
{/if}

{if $action eq 'contact'}
<h1>{l i='table_header_contact' gid='users'}</h1>

<div class="edit_block">
<form action="" method="post" enctype="multipart/form-data">
	<div class="r">
		<div class="f">{l i='field_contact_email' gid='users'}: </div>
		<div class="v"><input type="text" name="data[contact_email]" value="{$data.contact_email}"></div>
	</div>
	<div class="r">
		<div class="f">{l i='field_contact_phone' gid='users'}: </div>
		<div class="v"><input type="text" name="data[contact_phone]" value="{$data.contact_phone}" class="phone-field"></div>
	</div>
	<div class="r">
		<div class="f">{l i='field_contact_info' gid='users'}: </div>
		<div class="v"><textarea rows="5" cols="80" name="data[contact_info]">{$data.contact_info|escape}</textarea></div>
	</div>
	<div class="r">
		<div class="f">{l i='field_facebook' gid='users'}: </div>
		<div class="v"><input type="text" name="data[facebook]" value="{$data.facebook|escape}"></div>
	</div>
	<div class="r">
		<div class="f">{l i='field_twitter' gid='users'}: </div>
		<div class="v"><input type="text" name="data[twitter]" value="{$data.twitter|escape}"></div>
	</div>
    <!-- 
	<div class="r">
		<div class="f">{l i='field_vkontakte' gid='users'}: </div>
		<div class="v"><input type="text" name="data[vkontakte]" value="{$data.vkontakte|escape}"></div>
	</div>
    -->
	<div class="r">
		<div class="f">&nbsp;</div>
		<div class="v">
			<input type="submit" value="{l i='btn_save' gid='start' type='button'}" name="btn_register" class="btn" />
			<a href="{seolink module='users' method='profile'}" class="btn-link fleft"><ins class="with-icon i-larr"></ins>{l i='back_to_my_profile' gid='users'}</a>
		</div>
	</div>
	</form>
</div>
{/if}

{if $action eq 'subscriptions'}
<h1>{l i='table_header_subscriptions' gid='users'}</h1>

<div class="edit_block">
<form action="" method="post" enctype="multipart/form-data">
	{helper func_name=get_user_subscriptions_form module=subscriptions func_param=profile}
	<div class="r">
		<div class="f">&nbsp;</div>
		<div class="v">
			<input type="submit" value="{l i='btn_save' gid='start' type='button'}" name="btn_register" class="btn" />
			<a href="{seolink module='users' method='profile'}" class="btn-link fleft"><ins class="with-icon i-larr"></ins>{l i='back_to_my_profile' gid='users'}</a>
		</div>
	</div>
	</form>
</div>
{/if}
<script type='text/javascript'>{literal}
   $(document).ready(function(){
		now = new Date();
		yr =  (new Date(now.getYear() - 80, 0, 1).getFullYear()) + ':' + (new Date(now.getYear() - 18, 0, 1).getFullYear());
		$( "#datepicker" ).datepicker({
			dateFormat :'yy-mm-dd',
			changeYear: true,
			changeMonth: true,
			yearRange: yr
		});
   });
{/literal}</script>
	
{if $phone_format}
	{js file='jquery.maskedinput.min.js'}
	<script>{literal}
	$(function(){
		$('.phone-field').mask('{/literal}{$phone_format}{literal}');
	});
	{/literal}</script>
{/if}

