{include file="header.tpl" load_type='ui'}
{include file="left_panel.tpl" module="start"}
<div class="rc">
	<div class="content-block">
		<h1>{l i='text_profile_visitors' gid='users'} ({$users_count})</h1>
		<div id="users_block">{$block}</div>
		{js module=users file='users-list.js'}
		<script>{literal}
		$(function(){
			new usersList({
				siteUrl: '{/literal}{$site_url}{literal}',
				listAjaxUrl: 'users/ajax_visitors',
				order: '{/literal}{$order}{literal}',
				orderDirection: '{/literal}{$order_direction}{literal}',
				page: {/literal}{$page}{literal},
				tIds: ['pages_block_1', 'pages_block_2', 'sorter_block']
			});
		});
		{/literal}</script>
	</div>
</div>
<div class="clr"></div>
{include file="footer.tpl"}
