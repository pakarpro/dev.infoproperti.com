{if $user_id}
<li class="user_top_menu_block">
	<a href="{$site_url}users/profile"><img src="{$user_logo|escape}" title="{$user_name|escape}" width="28" height="28"></a> <a href="{$site_url}users/profile">{$user_name|truncate:25|escape}<div class="arrow"></div></a>
	{menu gid=$user_type+'_top_menu' template='user_top_menu'}
</li>
<script>{literal}
	$(function(){
		$('.user_top_menu_block').each(function(i, item){
			var element = $(item);
			var submenu = element.find('ul');
			if(submenu.length == 0) return;
			element.children('a').bind('touchstart', function(){
				element.toggleClass('hover');
				return false;
			});
			element.children('a').bind('touchenter', function(){
				element.removeClass('hover');
				element.trigger('hover');
				return false;
			}).bind('touchleave', function(){
				element.removeClass('hover').trigger('blur');
				return false;
			});
		});
	});
{/literal}</script>
{else}
<li><a href="#" onclick="javascript: ; return false;" id="ajax_login_link">{l i='header_login' gid='users'}</a></li>
<!-- <li><a href="{$site_url}users/login_form" onclick="javascript: ; return false;" id="ajax_login_link"><button id="loginsignup-btn">{l i='header_login' gid='users'}</button></a></li>-->
<script>
var user_ajax_login;
var login_ajax_url = '{$site_url}users/ajax_login_form';
{literal}
$(function(){
	$('#ajax_login_linkx').click(function(){
		ajax_login_form(login_ajax_url);
		return false;
	});
	user_ajax_login = new loadingContent({
		loadBlockWidth: '520px',
		linkerObjID: 'ajax_login_link',
		loadBlockLeftType: 'right',
		loadBlockTopType: 'bottom'
	});
});
function ajax_login_form(url){
	$.ajax({
		url: url,
		cache: false,
		success: function(data){
			user_ajax_login.show_load_block(data);
		}
	});
}
{/literal}
</script>
{/if}
