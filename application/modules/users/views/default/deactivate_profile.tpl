{include file="header.tpl"}
{include file="left_panel.tpl" module="start"}
<div class="rc">
	<div class="content-block">
		<h1>{l i='text_deactivate_profile' gid='users'}</h1>
		{l i='text_deactivate_profile_tooltip' gid='users'}<br><br>
		<div class="content-value">
			<div class="edit_block">
			<form action="" method="post">
				<div class="r">
					<div class="f">{l i='field_deactivated_reason' gid='users'}:&nbsp;* </div>
					<div class="v">
						<select name="reason_id">
							{ld i='deactivated_reason_object' gid='users' assign='reasons'}
							{foreach item=item key=key from=$reasons.option}
							<option value="{$key|escape}" {if $data.id_reason eq $key}selected="selected"{/if}>{$item}</option>
							{/foreach}
						</select>
					</div>
				</div>
				<div class="r">
					<div class="f">{l i='field_deactivated_message' gid='users'}: </div>
					<div class="v"><textarea rows="5" cols="8" name="message">{$data.message|escape}</textarea></div>
				</div>
				<div class="r">
					<div class="f">&nbsp;</div>
					<div class="v"><input type="submit" class="btn" value="{l i='btn_send' gid='start'  type='button'}" name="btn_save"></div>
				</div>
			</form>
			</div>
		</div>
	</div>
</div>
<div class="clr"></div>
{include file="footer.tpl"}
