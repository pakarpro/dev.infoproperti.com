<div id="user_select_{$select_data.rand}" class="user-select">
	<span id="user_text_{$select_data.rand}">
	{foreach item=item from=$select_data.selected}
	{$item.output_name|truncate:100} {if $select_data.max_select ne 1}<br>{/if}<input type="hidden" name="{$select_data.var_name}{if $select_data.max_select ne 1}[]{/if}" value="{$item.id}">
	{/foreach}
	</span>
	<a href="#" id="user_link_{$select_data.rand}">{l i='link_manage_users' gid='users'}</a>{if $select_data.max_select > 1} <i>({l i='max_user_select' gid='users'}: {$select_data.max_select})</i>{/if}<br>
	<div class="clr"></div>
</div>
{js module=users file='users-select.js'}
<script type='text/javascript'>{literal}
$(function(){
	new usersSelect({
		siteUrl: '{/literal}{$site_url}{literal}',	
		selected_items: [{/literal}{$select_data.selected_str}{literal}],
		max: '{/literal}{$select_data.max_select}{literal}',
		var_name: '{/literal}{$select_data.var_name}{literal}',
		template: '{/literal}{$select_data.template}{literal}',
		params: {{/literal}{foreach item=item key=key from=$select_data.params}{$key}:'{$item}',{/foreach}{literal}},
		rand: '{/literal}{$select_data.rand}{literal}',
		
	});
});
{/literal}</script>
