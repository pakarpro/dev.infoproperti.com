{include file="header.tpl"}
{include file="left_panel.tpl" module="start"}
<div class="rc">
	<div class="content-block">
		<h1>{l i='text_change_password' gid='users'}</h1>
		
		<div class="content-value">
	
			<div class="edit_block">
			<form action="" method="post">
				<div class="r">
					<div class="f">{l i='field_password_old' gid='users'}:&nbsp;* </div>
					<div class="v"><input type="password" name="password_old" value=""></div>
				</div>
				<div class="r">
					<div class="f">{l i='field_password' gid='users'}:&nbsp;* </div>
					<div class="v"><input type="password" name="password" value=""></div>
				</div>
				<div class="r">
					<div class="f">{l i='field_repassword' gid='users'}:&nbsp;* </div>
					<div class="v"><input type="password" name="repassword" value=""></div>
				</div>
				<div class="r">
					<div class="f">&nbsp;</div>
					<div class="v"><input type="submit" class="btn" value="{l i='btn_send' gid='start'  type='button'}" name="btn_save"></div>
				</div>
			</form>
		
			</div>
		</div>
	</div>
</div>
<div class="clr"></div>
{include file="footer.tpl"}
