{include file="header.tpl"}
{helper func_name='users_pagination_block' module='users' func_param=$user}	
{block name=show_social_networks_like module=social_networking func_param=true}

<!-- right panel starts -->
{capture assign='right_block'}
	{depends module=contact}
	    {capture assign='header'}
		<div class="user_contact"><h2>{l i='header_user_contact' gid='users'}</h2></div>
		{/capture}
		{block name=show_contact_form module=contact user_id=$user.id header=$header}
	{/depends}
	
	<!-- ads banner -->
	{helper func_name=show_banner_place module=banners func_param='right-banner'}
	<!-- ads banner ends-->
{/capture}
<!-- right panel ends -->

{capture assign='main_block'}
	<div class="content-block">
	{assign var=user_type value=$user.user_type}
	{include file="view_block.tpl" module="users" theme="default"}
	{js module=users file='users-menu.js'}
	{js module=users_services file='available_view.js'}
	{js file='change_link_action.js'}
	<script>{literal}
		var rMenu;
		$(function(){
			rMenu = new usersMenu({
				siteUrl: '{/literal}{$site_url}{literal}',
				idUser: '{/literal}{$user.id}{literal}',
				{/literal}{if $user_type eq 'agent' && $user.agent_status}idCompany: '{$user.agent_company}',{/if}{literal}
				{/literal}{depends module=users_services}available_view: new available_view(),{/depends}{literal}
			});
		});
	{/literal}</script>
	{block name=user_listings_block module=listings user=$user max_count='5'}	
</div>	
{/capture}
	
{if $right_block|trim}
<div class="rc_wrapper">
<div class="panel">
<div class="inside">
	<div class="lc-1" id="view_user">
		{$main_block}
	</div>
	<div class="rc-2 right-panel">
		{$right_block}
	</div>
	<div class="clr"></div>
</div>
</div>
</div>
{else}
	<div id="view_user">
		{$main_block}
	</div>
{/if}

{include file="footer.tpl"}
