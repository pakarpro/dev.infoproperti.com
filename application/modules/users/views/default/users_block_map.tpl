{block name=update_default_map module=geomap markers=$markers map_id='users_map_full_container'}

{capture assign='sorter_block'}{strip}
	{sorter links=$sort_data.links order=$sort_data.order direction=$sort_data.direction url=$sort_data.url}
	<div class="fright" id="pages_block_1">{pagination data=$page_data type='cute'}</div>
{/strip}{/capture}
{capture assign='paginator_bottom_block'}{pagination data=$page_data type='full'}{/capture}

<script>{literal}
	$(function(){
		$('#total_rows').html('{/literal}{$page_data.total_rows}{literal}');
		$('#sorter_block').html(decodeURIComponent('{/literal}{$sorter_block|rawurlencode}{literal}'));
		$('#pages_block_2').html('{/literal}{strip}{$paginator_bottom_block|addslashes}{/strip}{literal}');
	});
{/literal}</script>
