{if $users}
<div class="container-home-featured-companies">
	<div class="slider-container featured-companies">
		<h2 class="head-title">
			{if !$header_featured_users || $header_featured_users eq ''}
			{l i='header_featured_users' gid='users'}
			{else}
			{$header_featured_users}
			{/if}
		</h2>
		<!-- <div class="{$type}_users_block"> -->
		<ul class="feat-comp clearfix">
			{foreach item=item key=key from=$users}
			<li>
				<!--<div class="user {$photo_size}">-->
					{if $item.media.user_logo.thumbs[$photo_size]}
					<a class="feat-thumb" href="{seolink module='users' method='view' data=$item}"><img src="{$item.media.user_logo.thumbs[$photo_size]}" alt="{$item.output_name|truncate:30|escape}" title="{$item.output_name|escape}"></a>
					{/if}
					<a href="{seolink module='users' method='view' data=$item}">{$item.output_name|truncate:30}</a>
					<span class="business-type">{l i=$item.user_type gid='users'}</span>
				<!--</div>-->
			</li>
			{/foreach}
		</ul>
		<!-- </div> -->
	</div>
</div>

{/if}
