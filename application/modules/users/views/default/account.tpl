{include file="header.tpl"}

{include file="left_panel.tpl" module="start"}

<div class="rc" id="account">
	<h1>{seotag tag='header_text'}</h1>
	{helper func_name=update_account_block module=users_payments}
	<br>
</div>
<div class="clr"></div>
{include file="footer.tpl"}
