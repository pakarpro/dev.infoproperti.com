	<!-- user_block_list -->
	{if $users}
	<div class="sorter line" id="sorter_block">
		{sorter links=$sort_data.links order=$sort_data.order direction=$sort_data.direction url=$sort_data.url}
		<div class="fright" id="pages_block_1">{pagination data=$page_data type='cute'}</div>
	</div>
	{/if}

	<div>
		{foreach item=item from=$users}
		{assign var='current_user_id' value=$item.id}
		<div class="user-block clearfix">
			<div id="item-block-{$item.id}" class="clearfix item user {if $item.is_highlight}highlight{/if}">

			<h3><a href="{seolink module='users' method='view' data=$item}">{$item.output_name|truncate:50}</a>, {l i=$item.user_type gid='users'}</h3>
			<div class="image">
				<a href="{seolink module='users' method='view' data=$item}"><img src="{$item.media.user_logo.thumbs.middle}" title="{$item.output_name|escape}"></a>
			</div>
			<div class="body">
				{l i='no_information' gid='start' assign='no_info_str'}
				{if $user_type eq "agent" && $item.company}<h3>{$item.company.output_name}</h3>{/if}
				{if $is_contact || $users_for_contact[$current_user_id]}<h3>{$item.phone}</h3>{/if}	
				<div class="t-1">	
				
				</div>
				<div class="t-2">
					<span class="status_text">{if $item.is_featured}{l i='status_featured' gid='users'}{/if}</span>
				</div>		
				<div class="t-3">
				{if $item.user_type eq "company" && $item.agent_count}<span>{l i='field_agent_count' gid='users'}:</span> {$item.agent_count}<br>{/if}
				{if $item.listings_for_sale_count}<span>{l i='field_listings_for_sale' gid='users'}:</span> {$item.listings_for_sale_count}<br>{/if}
				{if $item.listings_for_rent_count}<span>{l i='field_listings_for_rent' gid='users'}:</span> {$item.listings_for_rent_count}<br>{/if}
				</div>
				<div class="t-4">
					{depends module=reviews}
					{block name=get_rate_block module=reviews rating_data_main=$item.review_sorter type_gid='users_object' template='mini' read_only='true'}<br>
					<span>{l i='field_rate' gid='users'}:</span> {$item.review_sorter}<br>
					<span>{l i='field_reviews_count' gid='users'}:</span> {$item.review_count}<br>
					{/depends}
				</div>
			</div>
			<div class="clr"></div>
			<div class="a-link">
				<a href="{seolink module='users' method='view' data=$item}">{l i='link_details' gid='users'}</a>
			</div>
		</div>
	</div>
	{foreachelse}
	<div class="item empty">{l i='no_users' gid='users'}</div>
	{/foreach}
	</div>
	{if $users}<div id="pages_block_2">{pagination data=$page_data type='full'}</div>{/if}

	{if $update_map}
	{block name=update_default_map module=geomap markers=$markers map_id='users_map_container'}
	{/if}
	
	<script>{literal}
	$(function(){
		$('#total_rows').html('{/literal}{$page_data.total_rows}{literal}');
	});
	{/literal}</script>
