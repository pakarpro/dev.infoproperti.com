{if $phone_format}
{js file='jquery.maskedinput.min.js'}
<script>{literal}
$(function(){
	$('#phone').mask('{/literal}{$phone_format}{literal}');
});
{/literal}</script>
{/if}
<form action="" method="post" enctype="multipart/form-data">
	<div class="r">
		<div class="f">{l i='field_fname' gid='users'}:&nbsp;<span class="ast">*</span></div>
		<div class="v"><input type="text" name="data[fname]" value="{$data.fname|escape}"></div>
	</div>
	<div class="r">
		<div class="f">{l i='field_sname' gid='users'}:</div>
		<div class="v"><input type="text" name="data[sname]" value="{$data.sname|escape}"></div>
	</div>
	<div class="r">
		<div class="f">{l i='field_email' gid='users'}:&nbsp;<span class="ast">*</span></div>
		<div class="v"><input type="text" name="data[email]" value="{$data.email|escape}"></div>
	</div>
	<div class="r">
		<div class="f">{l i='field_phone' gid='users'}:&nbsp;<span class="ast">*</span></div>
		<div class="v"><input type="text" name="data[phone]" value="{$data.phone|escape}" id="phone"></div>
	</div>
	<div class="r">
		<div class="f">{l i='field_password' gid='users'}:&nbsp;<span class="ast">*</span></div>
		<div class="v"><input type="password" name="data[password]"></div>
	</div>
	<div class="r">
		<div class="f">{l i='field_repassword' gid='users'}:&nbsp;<span class="ast">*</span></div>
		<div class="v"><input type="password" name="data[repassword]"></div>
	</div>
	<div class="r">
		<div class="f"><input type="checkbox" name="license_agreement" value="1" maxlength="{$data.show_contact_info}" id="license_agreement" /> <label for="license_agreement">{l i='field_license_agreement' gid='users'}</label></div>
		<div class="v"></div>
	</div>	
	<div class="r">
		<div class="f">{l i='field_captcha' gid='users'}:&nbsp;<span class="ast">*</span></div>
		<div class="v captcha">{$data.captcha_image} <input type="text" name="captcha_confirmation" value="" maxlength="{$data.captcha_word_length}" /></div>
	</div>
	<div class="r">
		<div class="f">&nbsp;</div>
		<div class="v"><input type="submit" value="{l i='btn_register' gid='start' type='button'}" name="btn_register"></div>
	</div>
	</form>
