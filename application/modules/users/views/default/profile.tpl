{include file="header.tpl" load_type='ui'}
{include file="left_panel.tpl" module="start"}
<div class="rc">
	<div class="content-block">
		{if $action eq 'view'}
			<h1>{seotag tag='header_text'}</h1>
			<div id="profile">
				{include file=$user_type+"_profile.tpl" module="users" theme="default"}
			</div>
		{else}
			{include file=$user_type+"_form.tpl" module="users" theme="default"}
		{/if}
	</div>
</div>
<div class="clr"></div>
{include file="footer.tpl"}
