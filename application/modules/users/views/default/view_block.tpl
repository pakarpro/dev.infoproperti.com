		{capture assign="review_callback"}{literal}
			function(data){
				window.location.href = '{/literal}{seolink module='users' method='view' data=$user section='reviews'}#m_reviews{literal}';
			}
		{/literal}{/capture}
		<!-- content for user profile-->
		<h1>{seotag tag='header_text'}</h1>
		<div class="actions noPrint">
			<div class="act-icon contact">{block name='button_contact' module='mailbox' user_id=$user.id user_type=$user_type}</div>
			<div class="act-icon send-review">{block name='send_review_block' module='reviews' object_id=$user.id type_gid='users_object' responder_id=$user.id success=$review_callback is_owner=$is_user_owner}</div>
			<div class="act-icon view-all-listing">{block name='user_listings_button' module='listings' user=$user}</div>
			<div class="act-icon pdf-view"><a href="{seolink method='view' module='users' data=$user pdf='yes'}" id="pdf_btn" class="btn-link link-r-margin" title="{l i='link_pdf' gid='listings' type='button'}"><ins class="with-icon i-pdf"></ins></a></div>
			<div class="act-icon print-view"><a href="javascript:void(0);" id="print_btn" class="btn-link link-r-margin" title="{l i='link_print' gid='listings' type='button'}" onclick="javascript: window.print(); return false;"><ins class="with-icon i-print"></ins></a></div>
			<div class="act-icon report-spam">{block name='mark_as_spam_block' module='spam' object_id=$user.id type_gid='users_object' is_owner=$is_user_owner}</div>
		</div>
		<div class="clr"></div>
		<div class="view_user">
			<div class="image">
				<a href="{seolink module='users' method='view' data=$user}"><img src="{$user.media.user_logo.thumbs.big}" title="{$user.output_name|escape}"></a>
			</div>
			<div class="body">
				{l i='no_information' gid='start' assign='no_info_str'}
				<div class="t-1 user-info-left">
					<!-- user info left -->
					{depends module=reviews}
					{block name=get_rate_block module=reviews rating_data_main=$user.review_sorter type_gid='users_object' template='normal' read_only='true'}<br>
					<span>{l i='field_rate' gid='users'}:</span> {$user.review_sorter}<br>
					<span>{l i='field_reviews_count' gid='users'}:</span> {$user.review_count}<br><br>
					{/depends}
					<div id="content_m_contacts" class="view-section{if $section_gid ne 'contacts'} hide{/if} print_block">{$user_content.contacts}</div>
					<span class="status_text">{if $user.is_featured}{l i='status_featured' gid='users'}{/if}</span>
					{if $user.user_type eq "company" && $user.agent_count}<span>{l i='field_agent_count' gid='users'}:</span> {$user.agent_count}<br>{/if}
					
					
				</div>
				<div class="t-2 user-info-right">
					<!-- user info right -->
					<span>{l i='field_register' gid='users'}:</span><br> {$user.date_created|date_format:$page_data.date_format}<br><br>
					<span>{l i='field_views' gid='users'}:</span> {$user.views}<br>
					{if $user.listings_for_sale_count}<span>{l i='field_listings_for_sale' gid='users'}:</span> {$user.listings_for_sale_count}<br>{/if}
					{if $user.listings_for_buy_count}<span>{l i='field_listings_for_buy' gid='users'}:</span> {$user.listings_for_buy_count}<br>{/if}
					{if $user.listings_for_rent_count}<span>{l i='field_listings_for_rent' gid='users'}:</span> {$user.listings_for_rent_count}<br>{/if}
					{if $user.listings_for_lease_count}<span>{l i='field_listings_for_lease' gid='users'}:</span> {$user.listings_for_lease_count}<br>{/if}
                    <!-- #modification# -->
				</div>
				
				<div class="r user-info-about-us">
					<div class="f" >{l i='field_description' gid='users'}: </div>
					<div class="v">
						{if $user.no_access_contact}<font class="gray_italic">{$not_access_str}</font>
						{elseif !$user.contact_info}<font class="gray_italic">{$no_info_str}</font>
						{elseif !$user.is_contact}<font class="gray_italic">{$buy_str}</font>
						{elseif $template eq 'small'}{$user.contact_info|truncate:55}
						{else}{$user.contact_info}
						{/if}
					</div>
				</div>
				
			</div>
			<div class="clr"></div>
		</div>
		<div class="edit_block">
			<?php /* disable for now {include file="view_menu.tpl" module="users" theme="default"} */ ?>
			<div id="user_block">
				{if $user.user_type_str eq "agent"}<div id="content_m_company" class="view-section{if $section_gid ne 'company'} hide{/if} noPrint">{$user_content.company}</div>{/if}
				<div id="content_m_reviews" class="view-section{if $section_gid ne 'reviews'} hide{/if} noPrint">{$user_content.reviews}</div>
				{if $user_type_str ne "private"}<div id="content_m_map" class="view-section{if $section_gid ne 'map'} hide{/if} noPrint">{$user_content.map}</div>{/if}
				<!-- #modification# <div id="content_m_contacts" class="view-section{if $section_gid ne 'contacts'} hide{/if} print_block">{$user_content.contacts}</div> -->
				<div id="content_m_listings" class="view-section{if $section_gid ne 'listings'} hide{/if} noPrint">{$user_content.listings}</div>
			</div>
		</div>