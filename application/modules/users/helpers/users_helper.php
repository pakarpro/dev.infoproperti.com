<?php

if(!defined("BASEPATH"))
	exit("No direct script access allowed");

if(!function_exists("login_form")){
	/**
	 * Return login form block
	 */
	function login_form(){
		$CI = & get_instance();
		if($CI->session->userdata("auth_type") == "user"){
			$CI->load->model("Users_model");
			$user_data = $CI->Users_model->get_user_by_id($CI->session->userdata("user_id"));
			$user_data = $CI->Users_model->format_user($user_data);
			$CI->template_lite->assign("user_data", $user_data);

			/*if($this->pg_module->is_module_installed("mailbox")){
				$CI->load->model("Mailbox_model");
				$new_messages_count = $CI->Mailbox_model->get_new_messages_count($CI->session->userdata("user_id"));
				$CI->template_lite->assign('new_messages_count', $new_messages_count);
			}*/
		}
		return $CI->template_lite->fetch("helper_login_form", "user", "users");
	}
}

if(!function_exists("users_lang_select")){
	/**
	 * Return lanuages selector
	 */
	function users_lang_select(){
		$CI = & get_instance();

		$lang_id = $CI->session->userdata("lang_id");
		if(!$lang_id) $lang_id = $CI->pg_language->get_default_lang_id();

		$count_active = 0;
		foreach($CI->pg_language->languages as $language){
			if($language["status"]){
				$count_active++;
			}
		}

		$CI->template_lite->assign("count_active", $count_active);
		$CI->template_lite->assign("current_lang", $lang_id);
		$CI->template_lite->assign("languages", $CI->pg_language->languages);
		return $CI->template_lite->fetch("helper_lang_select", "user", "users");
	}
}

if(!function_exists("auth_links")){
	/**
	 * Return sign in links
	 */
	function auth_links(){
		$CI = & get_instance();
		$CI->load->model("Users_model");

		if(!is_login_enabled()) return "";
		
		if($CI->session->userdata("auth_type") == "user"){
			$user_id = $CI->session->userdata("user_id");
			$CI->template_lite->assign('user_id', $user_id);
			
			$user_type = $CI->session->userdata("user_type");
			$CI->template_lite->assign('user_type', $user_type);
			
			$user_name = $CI->session->userdata("name");
			$CI->template_lite->assign('user_name', $user_name);
		
			$user_logo = $CI->session->userdata("logo");
			$CI->template_lite->assign('user_logo', $user_logo);
		}
		
		return $CI->template_lite->fetch("helper_auth_links", "user", "users");
	}
}

if(!function_exists("last_registered")){
	/**
	 * Return last registrated users
	 * @param integer $count
	 */
	function last_registered($count=9){
		$CI = & get_instance();
		$CI->load->model("Users_model");
		$attrs["where"]["user_logo !="] = "";
		$users = $CI->Users_model->get_active_users_list(1, $count, array("date_created" => "DESC"), $attrs);
		$CI->template_lite->assign("users", $users);
		return $CI->template_lite->fetch("helper_last_registered", "user", "users");
	}
}

if(!function_exists("featured_users_block")){
	/**
	 * Return featured users
	 * @param array $params
	 */
	function featured_users_block($params){
		$CI = & get_instance();
		$CI->load->model("Users_model");

		$user_types = $CI->Users_model->get_user_types();

		$header = $params["header"];

		$criteria["where"]["user_type"] = (isset($params["user_type"]) && in_array($params["user_type"], $user_types)) ? $params["user_type"] : current($user_types);
		$criteria["where"]["featured_end_date > "] = date("Y-m-d H:i:s");

		$items_on_page = intval($params["count"]);
		if(!($items_on_page)) $items_on_page = $CI->pg_module->get_module_config("users", "items_per_page");

		$carousel_items = intval($params["visible"]);
		if(!$carousel_items) $carousel_items = 5;

		$order = "date_modified";
		$order_direction = "DESC";
		$order_array[$order] = $order_direction;

		$items_count = $CI->Users_model->get_active_users_count($criteria);
		$users = array();
		if($items_count > 0){
			$users = $CI->Users_model->get_active_users_list(1, $items_on_page, $order_array, $criteria);
			$CI->template_lite->assign("users", $users);
		}

		$page_data = array(
			"header" => $header,
			"rand" => rand(100000, 999999),
			"count" => count($users),
			"visible" => $carousel_items,
		);
		$CI->template_lite->assign("users_page_data", $page_data);
		return $CI->template_lite->fetch("users_carousel_block", "user", "users");
	}
}

if(!function_exists("user_select")){
	/**
	 * Return user selector
	 * @param array $selected
	 * @param integer $max_select
	 * @param string $var_name
	 * @param integer $user_type
	 */
	function user_select($selected=array(), $max_select=0, $var_name="id_user", $template="default", $params=array()){
		
		$CI = & get_instance();
		$CI->load->model("Users_model");

		if($max_select == 1 && !empty($selected) && !is_array($selected)){
			$selected = array($selected);
		}

		if(!empty($selected)){
			$data["selected"] = $CI->Users_model->get_users_list(null, null, null, array(), $selected, true);
			$data["selected_str"] = implode(",", $selected);
		}else{
			$data["selected_str"] = "";
		}

		$data["max_select"] = $max_select;		
		$data["var_name"] = $var_name;
		$data["template"] = $template;
		$data["params"] = $params;
		$data["rand"] = rand(100000, 999999);

		$CI->template_lite->assign("select_data", $data);
		return $CI->template_lite->fetch("helper_user_select_".$template, "user", "users");
	}
}

if(!function_exists("view_user_block")){
	/**
	 * Return user profile
	 * @param integer $user_id
	 */
	function view_user_block($params){
		$CI = & get_instance();
		$CI->load->model("Users_model");
		if(!isset($params['user'])){
			$CI->Users_model->set_format_settings('get_contact', true);
			$params['user'] = $CI->Users_model->get_user_by_id($params['user_id']);
			$CI->Users_model->set_format_settings('get_contact', false);
		}
		$CI->template_lite->assign("user_data", $params['user']);
		if(isset($params['template'])) $CI->template_lite->assign("template", $params['template']);
		return $CI->template_lite->fetch("helper_view_user_block", "user", "users");
	}
}

if(!function_exists("admin_home_users_block")){
	/**
	 * Return homepage statistics
	 */
	function admin_home_users_block(){
		$CI = & get_instance();

		$auth_type = $CI->session->userdata("auth_type");
		if($auth_type != "admin") return "";

		$user_type = $CI->session->userdata("user_type");

		$show = true;

		$stat_users = array(
			"index_method" => true,
			"moderation_method" => true
		);

		if($user_type == "moderator"){
			$show = false;
			$CI->load->model("Ausers_model");
			$methods_users = $CI->Ausers_model->get_module_methods("users");
			$methods_moderation = $CI->Ausers_model->get_module_methods("moderation");
			if((is_array($methods_users) && !in_array("index", $methods_users)) || (is_array($methods_moderation) && !in_array("index", $methods_moderation))){
				$show = true;
			}else{
				$permission_data = $CI->session->userdata("permission_data");
				if(isset($permission_data["users"]["index"]) && $permission_data["users"]["index"] == 1){
					$show = true;
					$stat_users["index_method"] = (bool)$permission_data["users"]["index"];
				}
			}
		}

		if(!$show){
			return "";
		}
		
		$CI->load->model("Users_model");
		$user_types = $CI->Users_model->get_user_types();
		foreach($user_types as $user_type){
			$stat_users[$user_type]["all"] = $CI->Users_model->get_users_count_by_filters(array('type'=>$user_type));
			$stat_users[$user_type]["active"] = $CI->Users_model->get_users_count_by_filters(array('type'=>$user_type, 'active'=>1));
			$stat_users[$user_type]["blocked"] = $CI->Users_model->get_users_count_by_filters(array('type'=>$user_type, 'not_active'=>1));
			$stat_users[$user_type]["unconfirm"]= $CI->Users_model->get_users_count_by_filters(array('type'=>$user_type, 'not_confirm'=>1));
		}

		$CI->load->model("Moderation_model");
		$stat_users["icons"] = $CI->Moderation_model->get_moderation_list_count("user_logo");

		$CI->template_lite->assign("stat_users", $stat_users);
		return $CI->template_lite->fetch("helper_admin_home_block", "admin", "users");
	}
}

if(!function_exists("users_search_block")){
	/**
	 * Return user search form
	 * @param array $params
	 */
	function users_search_block($params){
		$CI = & get_instance();
		$CI->load->model("Users_model");

		$CI->uri->_fetch_uri_string();
		$uri = $CI->uri->ruri_string();
		$uri = trim(substr($uri, 1), "/");
		if(empty($uri) || count(explode("/", $uri))<2){
			$class  = $CI->router->fetch_class(true);
			$method = $CI->router->fetch_method();
			$uri = $class."/".$method;
		}
		
		$cmp = false;
		if(!strstr($uri, "users/index") && !strstr($uri, "users/search")){
			return '';
		}
		
		$user_types = $CI->Users_model->get_user_types();
		foreach($user_types as $user_type){
		    $search_enabled = $CI->pg_module->get_module_config('users', $user_type.'_search_enabled');
		    if(!$search_enabled){
				unset($user_types[$key]);
			}
		}
		
		if(empty($user_types)) return '';
		
		$CI->template_lite->assign('user_types', $user_types);
		
		if(!empty($_SESSION["users_list"]["data"])){
			$CI->template_lite->assign("data", $_SESSION["users_list"]["data"]);
		}
		
		return $CI->template_lite->fetch("helper_search_block", "user", "users");
	}
}

if(!function_exists("make_agent")){
	/**
	 * Return approcve/decline agent block
	 */
	function make_agent($user_id){
		$CI = & get_instance();
		if($CI->session->userdata("auth_type") != "user") return;
			
		$CI->load->model("Users_model");
		$user_data = $CI->Users_model->get_user_by_id($CI->session->userdata("user_id"));
		$user_data = $CI->Users_model->format_user($user_data);
		$CI->template_lite->assign("user_data", $user_data);

		return $CI->template_lite->fetch("helper_make_agent", "user", "users");
	}
}

if (!function_exists('site_currency_select')){
	/**
	 * Returns currency selector
	 *
	 * @return string
	 */
	function site_currency_select(){
		$CI = & get_instance();
		if($CI->pg_module->is_module_installed("payments")){
			$CI->load->model('payments/models/Payment_currency_model');
			$currencies = $CI->Payment_currency_model->get_currency_list();
			$CI->template_lite->assign("currencies", $currencies);
			return $CI->template_lite->fetch("helper_currency_select", "user", "users");
		}
	}
}

if (!function_exists('user_info')){
	/**
	 * Returns user info
	 *
	 * @return string
	 */
	function user_info($params){
		if(!isset($params['user']) || !$params['user']) return '';
		
		$CI = & get_instance();

		if(!is_array($params['user'])){
			$CI->load->model('Users_model');
			$params['user'] = $CI->Users_model->get_user_by_id($params['user']);
		}	
		
		if(!$params['user']) return '';
		
		$CI->template_lite->assign('user', $params['user']);
		
		if(isset($params['all_listings']))
			$CI->template_lite->assign('show_all_listings', $params['all_listings']);
		
		if($CI->session->userdata("auth_type") != 'user'){
			$CI->template_lite->assign('show_contact_form', 1);
		}
		
		if($params['user']['user_type'] == 'company'){
			$map_markers = array(
				array( 
					'gid' => $params['user']['id'],
					"country" => $params['user']["country"], 
					"region" => $params['user']["region"], 
					"city" => $params['user']["city"], 
					"address" => $params['user']["address"], 
					"postal_code" => $params['user']["postal_code"], 
					"lat" => (float)$params['user']["lat"], 
					"lon" => (float)$params['user']["lon"], 
					"html" => ($params['user']['location'] ? $params['user']['location'] : $params['user']["output_name"].', '.l($params['users']['user_type'], 'users')),
					"dragging" => false,
				),
			);
		
			$current_language = $CI->pg_language->get_lang_by_id($CI->pg_language->current_lang_id);
			$view_settings = array(
				"class" => "", 
				"geocode_listener" => "get_user_geocode_data", 
				"lang" => $current_language["code"],
			);
			$CI->template_lite->assign("map_settings", $view_settings);
			$CI->template_lite->assign("map_markers", $map_markers);
		}
		
		$is_owner = 0;
		if($CI->session->userdata("auth_type") == "user"){
			$auth_id = $CI->session->userdata("user_id");
			if($auth_id == $params['user']['id']) $is_owner = 1;
		}
		$CI->template_lite->assign('is_user_owner', $is_owner);
		
		return $CI->template_lite->fetch("helper_user_info", "user", "users");
	}
}

if (!function_exists('show_profile_info')){
	/**
	 * Returns profile info
	 *
	 * @return string
	 */
	function show_profile_info(){
		$CI = & get_instance();
		
		$CI->uri->_fetch_uri_string();
		$uri = $CI->uri->uri_string;
		$uri = trim(substr($uri, 1), "/");
		if(empty($uri) || count(explode("/", $uri))<2){
			$class  = $CI->router->fetch_class(true);
			$method = $CI->router->fetch_method();
			$uri = $class."/".$method;
		}
		if(!strstr($uri, 'start/homepage')) return '';
			
		$user_id = $CI->session->userdata("user_id");
		$user = $CI->Users_model->get_user_by_id($user_id);
		$CI->template_lite->assign('user', $user);
	
		$complete = $CI->Users_model->get_profile_complete($user);
		$CI->template_lite->assign('complete', $complete);
	
		return $CI->template_lite->fetch("helper_profile_info", "user", "users");
	}
}

if (!function_exists('is_login_enabled')){
	/**
	 * Check login enabled
	 *
	 * @return string
	 */
	function is_login_enabled(){
		$CI = & get_instance();
		$CI->load->model('Users_model');
		$login_enabled = false;
		$user_types = $CI->Users_model->get_user_types();
		foreach($user_types as $user_type){
			$user_login_enabled = $CI->pg_module->get_module_config("users", $user_type."_login_enabled");
			$login_enabled = $login_enabled || $user_login_enabled;
		}
		return $login_enabled;
	}
}

if(!function_exists("users_visitors_block")){
	/**
	 * Return profile visitors
	 */
	function users_visitors_block(){
		$CI = & get_instance();

		$user_id = intval($CI->session->userdata("user_id"));

		$CI->load->model("users/models/Users_visitor_model");
		
		$visitors_count_week = $CI->Users_visitor_model->get_visitors_count(array('user'=>$user_id, 'week'=>1));
		$CI->template_lite->assign("visitors_count_week", $visitors_count_week);
		
		$visitors_count_month = $CI->Users_visitor_model->get_visitors_count(array('user'=>$user_id, 'month'=>1));
		$CI->template_lite->assign("visitors_count_month", $visitors_count_month);
		
		return $CI->template_lite->fetch("helper_profile_visitors", "user", "users");
	}
}

if(!function_exists("users_visits_block")){
	/**
	 * Return profile visits
	 */
	function users_visits_block(){
		$CI = & get_instance();

		$user_id = intval($CI->session->userdata("user_id"));

		$CI->load->model("users/models/Users_visitor_model");
		
		$visits_count_week = $CI->Users_visitor_model->get_visitors_count(array('visitor'=>$user_id, 'week'=>1));
		$CI->template_lite->assign("visits_count_week", $visits_count_week);
		
		$visits_count_month = $CI->Users_visitor_model->get_visitors_count(array('visitor'=>$user_id, 'month'=>1));
		$CI->template_lite->assign("visits_count_month", $visits_count_month);
		
		return $CI->template_lite->fetch("helper_profile_visits", "user", "users");
	}
}

if(!function_exists("users_pagination_block")){
	/**
	 * Return pagination block
	 */
	function users_pagination_block($user){
		$CI = & get_instance();
		$CI->load->model('Users_model');
		
		$prev_user = $user; 
		$next_user = $user;
		$current_page = 1;
		$total_pages = 1;
		
		$current_settings = isset($_SESSION["users_pagination"])?$_SESSION["users_pagination"]:array();
		if(!isset($current_settings["order"])) $current_settings["order"] = "date_created";
		if(!isset($current_settings["order_direction"])) $current_settings["order_direction"] = "DESC";
		if(!isset($current_settings["page"])) $current_settings["page"] = 1;
		if(!isset($current_settings["data"])) $current_settings["data"] = array();
		$items_on_page = $CI->pg_module->get_module_config("start", "index_items_per_page") + 2;
		if($current_settings['data']['type'] != $user['user_type']) $current_settings['data'] = array('type' => $user['user_type']);		
		$page = ceil(($current_settings["page"]*$items_on_page - 3)/$items_on_page);

		$users_count = $CI->Users_model->get_users_count_by_filters($current_settings['data']);		
		if($users_count > 1){
			$order_array = array();
			if($current_settings['order'] == "name"){
				if($user_type == "company"){
					$order_array = array("company_name" => $current_settings['order_direction']);
				}else{
					$order_array = array("CONCAT(fname, ' ', sname)" => $current_settings['order_direction']);
				}
			}else{
				$order_array = array($current_settings['order'] => $current_settings['order_direction']);
			}
			$users = $CI->Users_model->get_users_pagination($current_settings['data'], $page, $items_on_page, $order_array);
			$pages_count = ceil($users_count/$items_on_page);
			$count = count($users);
			if($current_settings["page"] > 1 && $current_settings["page"] == $pages_count) $count -= 2;
			foreach($users as $i=>$data){
				if($data['id'] == $user['id']){		
					$page = $current_settings["page"];
					$current_page = ($current_settings["page"]-1)*$items_on_page + $i + ($current_settings["page"] == 1 ? 1 : -1);
					$total_pages = $users_count;			
				
					if($i > 1){
						$prev_user = $users[$i-1];
					}elseif($current_settings["page"] > 1){
						$prev_user = $users[$i-1];
						$page--;		
					}elseif($i > 0){
						$prev_user = $users[$i-1];
					}
					
					if($i < $count-($pages_count > 1 ? 2 : 1)){
						$next_user = $users[$i+1];
					}elseif($current_settings["page"] < $pages_count){				
						$next_user = $users[$i+1];
						$page++;
					}
					
					if($page != $current_settings["page"]){
						$current_settings["page"] = $page;
						$_SESSION["users_pagination"] = $current_settings;
					}
					break;
				}
			}
		}
		
		$CI->template_lite->assign('prev_user', $prev_user); 
		$CI->template_lite->assign('next_user', $next_user);
		$CI->template_lite->assign('current_page', $current_page);
		$CI->template_lite->assign('total_pages', $total_pages);
		
		return $CI->template_lite->fetch("helper_users_pagination", "user", "users");
	}
}
