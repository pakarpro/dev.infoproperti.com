DROP TABLE IF EXISTS `[prefix]groups`;
CREATE TABLE IF NOT EXISTS `[prefix]groups` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `gid` varchar(50) NOT NULL,
  `is_default` tinyint(3) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `gid` (`gid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `[prefix]users`;
CREATE TABLE IF NOT EXISTS `[prefix]users` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `email` varchar(50) NOT NULL,
  `email_restore` varchar(50) NOT NULL,
  `email_restore_code` varchar(50) NOT NULL,
  `unique_name` varchar(20) NOT NULL,
  `user_type` enum('private','company', 'agent') NOT NULL DEFAULT 'private',
  `password` varchar(50) NOT NULL,
  `status` tinyint(3) NOT NULL,
  `confirm` tinyint(3) NOT NULL,
  `confirm_code` varchar(50) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `sname` varchar(100) NOT NULL,
  `lang_id` int(3) NOT NULL,
  `id_currency` int(3) NOT NULL,
  `user_open_id` varchar(100) NOT NULL,
  `user_logo` varchar(100) NOT NULL,
  `user_logo_moderation` varchar(100) NOT NULL,
  `group_id` int(3) NOT NULL,
  `account` decimal(11,2) NOT NULL,
  `id_country` char(2) NOT NULL,
  `id_region` int(3) NOT NULL,
  `id_city` int(3) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `phone_restore` varchar(20) NOT NULL,
  `phone_restore_code` varchar(50) NOT NULL,
  `contact_phone` varchar(50) NOT NULL,
  `contact_email` varchar(50) NOT NULL,
  `contact_info` text NULL,
  `address` varchar(255) NOT NULL,
  `postal_code` varchar(12) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `web_url` varchar(255) NOT NULL,
  `featured_end_date` datetime NOT NULL,
  `show_logo_end_date` datetime NOT NULL,
  `working_days` varchar(100) NOT NULL, 
  `working_hours_begin` tinyint(3) NOT NULL, 
  `working_hours_end` tinyint(3) NOT NULL, 
  `lunch_time_begin` tinyint(3) NOT NULL,
  `lunch_time_end` tinyint(3) NOT NULL,
  `lat` decimal(11,7) NOT NULL,
  `lon` decimal(11,7) NOT NULL,
  `agent_company` int(11) NOT NULL,
  `agent_date` datetime NOT NULL,
  `agent_status` tinyint(1) NOT NULL,
  `agent_count` smallint(5) NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `vkontakte` varchar(255) NOT NULL,
  `views` int(3) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `unique_name` (`unique_name`),
  KEY `email` (`email`,`password`),
  KEY `id_country` (`id_country`,`id_region`,`id_city`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `[prefix]user_connections`;
CREATE TABLE IF NOT EXISTS `[prefix]user_connections` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `service_id` int(3) NOT NULL,
  `access_token` TEXT NOT NULL,
  `access_token_secret` TEXT NOT NULL,
  `data` TEXT NOT NULL,
  `date_end` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `[prefix]user_account_list`;
CREATE TABLE IF NOT EXISTS `[prefix]user_account_list` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `id_user` int(3) NOT NULL,
  `date_add` datetime NOT NULL,
  `message` varchar(255) NOT NULL,
  `price` float NOT NULL,
  `price_type` enum('add','spend') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user_2` (`id_user`,`date_add`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `[prefix]user_services`;
CREATE TABLE IF NOT EXISTS `[prefix]user_services` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `id_user` int(3) NOT NULL,
  `service_name` varchar(255) NOT NULL,
  `service_data` text NOT NULL,
  `post_count` int(3) NOT NULL,
  `post_period` int(3) NOT NULL,
  `contact_count` int(3) NOT NULL,
  `contact_service_end_date` datetime NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `contact_status` tinyint(1) NOT NULL,
  `post_status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user_3` (`id_user`,`date_created`,`date_modified`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `[prefix]user_deactivated_alerts`;
CREATE TABLE IF NOT EXISTS `[prefix]user_deactivated_alerts` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `sname` varchar(255) NOT NULL,
  `email` varchar(128) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `id_reason` tinyint(4) NOT NULL,
  `message` text NULL,
  `status` tinyint(1),
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_reason` (`id_reason`,`date_add`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS  `[prefix]user_profile_visitors`;
CREATE TABLE IF NOT EXISTS `[prefix]user_profile_visitors` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `id_user` int(3) NOT NULL,
  `id_visitor` int(3) NOT NULL,
  `last_visit_date` datetime NOT NULL,
  `visits_count` int(3) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`),
  KEY `id_visiter` (`id_visitor`),
  UNIQUE `user_visiter` (`id_user`, `id_visitor`, `last_visit_date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
