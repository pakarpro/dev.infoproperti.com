function userSearch(optionArr){
	this.properties = {
		siteUrl: '',
		viewAjaxUrl: 'users/ajax_search',
		saveSearchUrl: '',
		sFromId: 'search_users_form',
		sectionId: 'search_users_sections',
		listBlockId: 'users_block',
		mapBlockId: 'users_map',
		errorObj: new Errors(),
	};
	
	var _self = this;

	this.Init = function(options){
		_self.properties = $.extend(_self.properties, options);
		$('#'+_self.properties.sFromId).bind('submit', function(){
			_self.search();
			return false;
		});
	}
	
	this.search = function(){
		var user_type = $('#'+_self.properties.sFromId+' select:first').val();
		$('#'+_self.properties.sectionId+' li').removeClass('active');
		$('#'+_self.properties.sectionId+' li[sgid='+user_type+']').addClass('active');
		var send_data = $('#'+_self.properties.sFromId).serialize();
		$.ajax({
			url: _self.properties.siteUrl + _self.properties.viewAjaxUrl,
			type: 'POST',
			cache: false,
			data: send_data,
			success: function(data){
				$('#'+_self.properties.sectionId+' [sgid='+user_type+']').trigger('click');
			}
		});
	}
	
	this.save_search = function(){
		$.ajax({
			url: _self.properties.siteUrl + _self.properties.saveSearchUrl, 
			type: 'GET',
			cache: false,
			dataType: 'json',
			success: function(data){
				if(data.success){
					_self.properties.errorObj.show_error_block(data.success, 'success');
				}else{
					_self.properties.errorObj.show_error_block(data.error, 'error');
				}
				return false;
			}
		});
	}	
	
	_self.Init(optionArr);
}
