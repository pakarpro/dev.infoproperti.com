function usersMenu(optionArr){
	this.properties = {
		siteUrl: '',
		viewAjaxUrl: 'users/ajax_get_section/',
		sectionId: 'user_sections',
		listBlockId: 'user_block',
		tryDisplayWithoutAjax: true,
		containerPrefix: 'content_',
		tabPrefix: 'm',
		errorObj: new Errors(),
		available_view: null,
		CurrentSection: 'm_contacts',
		idUser: 0,
		idCompany: 0,
		template: '',
	}
	
	this.loaded = {};

	var _self = this;
	
	this.Init = function(options){ 
		_self.properties = $.extend(_self.properties, options);
		
		_self.loaded[_self.properties.tabPrefix + '_contacts'] = true;
		
		_self.init_links();
		
		if(_self.properties.available_view){
			_self.properties.available_view.set_properties({
				siteUrl: _self.properties.siteUrl,
				success_request: function(message){
					switch(_self.properties.CurrentSection){
						case _self.properties.tabPrefix+'_contacts':
							_self.loaded[_self.properties.tabPrefix+'_map'] = false;
							_self.loaded[_self.properties.tabPrefix+'_map_info'] = false;
						break;
						case _self.properties.tabPrefix+'_map':
						case _self.properties.tabPrefix+'_map_info':
							_self.loaded[_self.properties.tabPrefix+'_contacts'] = false;
						break;
					}
					
					_self.show_tab(_self.properties.CurrentSection, true);

					if(message){
						_self.properties.errorObj.show_error_block(message, 'success');
					}	
				},
				fail_request: function(message){
					_self.properties.errorObj.show_error_block(message, 'error');
				}
			});
		}
	}
	
	this.init_links = function(){
		var contacts_btn = '#'+_self.properties.containerPrefix+_self.properties.tabPrefix+'_contacts input[name=contacts_btn]';
		var map_btn = '#'+_self.properties.containerPrefix+_self.properties.tabPrefix+'_map input[name=map_btn]';
		var map_info_btn = '#'+_self.properties.containerPrefix+_self.properties.tabPrefix+'_map_info input[name=map_info_btn]';
		$(contacts_btn+','+map_btn+','+map_info_btn).live('click', function(){
			if(_self.properties.available_view){
				_self.properties.available_view.check_available(_self.properties.idUser);
			}else{
				_self.show_tab(_self.properties.CurrentSection, true);
			}
		});
	    
		$('#'+_self.properties.sectionId+' li').live('click', function(){
			var id = $(this).attr('id');
			_self.properties.CurrentSection = id;
			switch(id){
				case _self.properties.tabPrefix+'_company':
					if(_self.properties.available_view){
						_self.properties.available_view.check_available(_self.properties.idCompany);
					}else{
						_self.show_tab(_self.properties.CurrentSection, true);
					}
				break;
				case _self.properties.tabPrefix+'_map':	
				case _self.properties.tabPrefix+'_map_info':	
				case _self.properties.tabPrefix+'_contacts':
					if(_self.loaded[id]){
						_self.show_tab(id, false);
					}else{
						_self.show_tab(id, true);
					}
				break;
				default:
					_self.show_tab(id, false);
				break;
			}
			return false;
		});
	}
	
	this.show_tab = function(id, load){
		$('#user_sections li').removeClass('active');
		$('#'+id).addClass('active');
			
		$('#'+_self.properties.listBlockId + ' .view-section').hide();
		
		_self.show_block(id, load);
	}
	
	this.show_block = function(id, load){
		var section_gid = $('#'+id).attr('sgid');
		if(load){
			var url = _self.properties.siteUrl + _self.properties.viewAjaxUrl + 
					  _self.properties.idUser + '/' + section_gid +
					  (_self.properties.template ? '/' + _self.properties.template : '');
			$.ajax({
				url: url, 
				type: 'GET',
				cache: false,
				success: function(data){
					$('#'+_self.properties.containerPrefix+_self.properties.tabPrefix+'_'+section_gid).show().html(data);
					_self.loaded[id] = true;
				}
			});
		}else{
		    $('#'+_self.properties.containerPrefix+_self.properties.tabPrefix+'_'+section_gid).show();
		}
	}
	
	_self.Init(optionArr);
}
