<?php

$install_lang["block_featured_agents_block"] = "Рекомендуемые агенты";
$install_lang["block_featured_companies_block"] = "Рекомендуемые компании";
$install_lang["param_featured_agents_block_count"] = "Количество";
$install_lang["param_featured_companies_block_count"] = "Количество";
$install_lang["view_featured_agents_block_gallery_big"] = "Галерея 200x200";
$install_lang["view_featured_agents_block_gallery_middle"] = "Галерея 100x100";
$install_lang["view_featured_companies_block_gallery_big"] = "Галерея 200x200";
$install_lang["view_featured_companies_block_gallery_middle"] = "Галерея 100x100";

