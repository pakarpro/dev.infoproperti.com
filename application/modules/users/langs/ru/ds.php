<?php

$install_lang["user_type"]["header"] = "Типы пользователей";
$install_lang["user_type"]["option"]["private"] = "Частное лицо";
$install_lang["user_type"]["option"]["company"] = "Агентство";
$install_lang["user_type"]["option"]["agent"] = "Агент";

$install_lang["professionals"]["header"] = "Профессионалы";
$install_lang["professionals"]["option"]["company"] = "Агентство";
$install_lang["professionals"]["option"]["agent"] = "Агент";
$install_lang["professionals"]["option"]["private"] = "Частное лицо";

$install_lang["deactivated_reason_object"]["header"] = "Причина деактивации профайла";
$install_lang["deactivated_reason_object"]["option"]["1"] = "В целях защиты частной жизни";
$install_lang["deactivated_reason_object"]["option"]["2"] = "Отсутствует необходимость";
$install_lang["deactivated_reason_object"]["option"]["3"] = "Не понимаю, как пользоваться сайтом";
$install_lang["deactivated_reason_object"]["option"]["4"] = "Это временно, я еще вернусь";
$install_lang["deactivated_reason_object"]["option"]["5"] = "Другое";


