<?php

$install_lang["block_featured_agents_block"] = "Featured agents";
$install_lang["block_featured_companies_block"] = "Featured companies";
$install_lang["param_featured_agents_block_count"] = "Count";
$install_lang["param_featured_companies_block_count"] = "Count";
$install_lang["view_featured_agents_block_gallery_big"] = "Gallery 200x200";
$install_lang["view_featured_agents_block_gallery_middle"] = "Gallery 100x100";
$install_lang["view_featured_companies_block_gallery_big"] = "Gallery 200x200";
$install_lang["view_featured_companies_block_gallery_middle"] = "Gallery 100x100";

