<?php

$install_lang["user_type"]["header"] = "User type";
$install_lang["user_type"]["option"]["private"] = "Private person";
$install_lang["user_type"]["option"]["company"] = "Agency";
$install_lang["user_type"]["option"]["agent"] = "Agent";

$install_lang["professionals"]["header"] = "Professionals";
$install_lang["professionals"]["option"]["company"] = "Agency";
$install_lang["professionals"]["option"]["agent"] = "Agent";
$install_lang["professionals"]["option"]["private"] = "Private person";

$install_lang["deactivated_reason_object"]["header"] = "Deactivated reason";
$install_lang["deactivated_reason_object"]["option"]["1"] = "I have privacy concerns";
$install_lang["deactivated_reason_object"]["option"]["2"] = "I don’t find it useful";
$install_lang["deactivated_reason_object"]["option"]["3"] = "I don’t understand how to use it";
$install_lang["deactivated_reason_object"]["option"]["4"] = "It’s temporarily, I’ll be back";
$install_lang["deactivated_reason_object"]["option"]["5"] = "Other";


