<?php

$install_lang["admin_menu_main_items_users_menu_item"] = "Users";
$install_lang["admin_menu_main_items_users_menu_item_tooltip"] = "";
$install_lang["admin_menu_settings_items_feedbacks-items_users_deact_alers_menu_item"] = "Profile deactivation";
$install_lang["admin_menu_settings_items_feedbacks-items_users_deact_alers_menu_item_tooltip"] = "Set up reasons and view profile deactivation alerts";
$install_lang["admin_menu_settings_items_system-items_users_deact_sett_menu_item"] = "Profile deactivation";
$install_lang["admin_menu_settings_items_system-items_users_deact_sett_menu_item_tooltip"] = "Set up reasons and view profile deactivation alerts";
$install_lang["admin_users_menu_groups_list_item"] = "User groups";
$install_lang["admin_users_menu_groups_list_item_tooltip"] = "";
$install_lang["admin_users_menu_users_list_item"] = "Users";
$install_lang["admin_users_menu_users_list_item_tooltip"] = "";
$install_lang["admin_users_settings_menu_users_deactivated_alerts_item"] = "Deactivations";
$install_lang["admin_users_settings_menu_users_deactivated_alerts_item_tooltip"] = "";
$install_lang["admin_users_settings_menu_users_deactivated_reasons_item"] = "Reasons for de-activation";
$install_lang["admin_users_settings_menu_users_deactivated_reasons_item_tooltip"] = "";
$install_lang["admin_users_settings_menu_users_deactivated_settings_item"] = "Settings";
$install_lang["admin_users_settings_menu_users_deactivated_settings_item_tooltip"] = "";
$install_lang["admin_users_types_menu_users_types_agent_item"] = "Agents";
$install_lang["admin_users_types_menu_users_types_agent_item_tooltip"] = "";
$install_lang["admin_users_types_menu_users_types_company_item"] = "Agencies";
$install_lang["admin_users_types_menu_users_types_company_item_tooltip"] = "";
$install_lang["admin_users_types_menu_users_types_private_item"] = "Private persons";
$install_lang["admin_users_types_menu_users_types_private_item_tooltip"] = "";
$install_lang["admin_users_types_menu_users_types_settings_item"] = "Settings";
$install_lang["admin_users_types_menu_users_types_settings_item_tooltip"] = "";
$install_lang["agent_account_menu_agent_my_account_item"] = "My account";
$install_lang["agent_account_menu_agent_my_account_item_tooltip"] = "";
$install_lang["agent_account_menu_agent_payments_item_agent_my_account_balance_item"] = "Account balance";
$install_lang["agent_account_menu_agent_payments_item_agent_my_account_balance_item_tooltip"] = "";
$install_lang["agent_account_menu_agent_my_profile_item"] = "My profile";
$install_lang["agent_account_menu_agent_my_profile_item_agent_my_profile_change_regdata_item"] = "Registration data";
$install_lang["agent_account_menu_agent_my_profile_item_agent_my_profile_change_regdata_item_tooltip"] = "";
$install_lang["agent_account_menu_agent_my_profile_item_agent_my_profile_deactivate_item"] = "De-activate profile";
$install_lang["agent_account_menu_agent_my_profile_item_agent_my_profile_deactivate_item_tooltip"] = "";
$install_lang["agent_account_menu_agent_my_profile_item_agent_my_profile_edit_item"] = "Edit profile";
$install_lang["agent_account_menu_agent_my_profile_item_agent_my_profile_edit_item_tooltip"] = "";
$install_lang["agent_account_menu_agent_my_profile_item_agent_my_profile_services_item"] = "Paid services";
$install_lang["agent_account_menu_agent_my_profile_item_agent_my_profile_services_item_tooltip"] = "";
$install_lang["agent_account_menu_agent_my_profile_item_tooltip"] = "";
$install_lang["agent_account_menu_agent_overview_item"] = "Overview";
$install_lang["agent_account_menu_agent_overview_item_tooltip"] = "";
$install_lang["agent_main_menu_agent-main-menu-pro-item"] = "Professionals";
$install_lang["agent_main_menu_agent-main-menu-pro-item_agent-main-menu-pro-agent-item"] = "Find an agent";
$install_lang["agent_main_menu_agent-main-menu-pro-item_agent-main-menu-pro-agent-item_tooltip"] = "";
$install_lang["agent_main_menu_agent-main-menu-pro-item_agent-main-menu-pro-company-item"] = "Find an agency";
$install_lang["agent_main_menu_agent-main-menu-pro-item_agent-main-menu-pro-company-item_tooltip"] = "";
$install_lang["agent_main_menu_agent-main-menu-pro-item_agent-main-menu-pro-private-item"] = "Find a private person";
$install_lang["agent_main_menu_agent-main-menu-pro-item_agent-main-menu-pro-private-item_tooltip"] = "";
$install_lang["agent_main_menu_agent-main-menu-pro-item_tooltip"] = "";
$install_lang["agent_top_menu_agent-main-logout-item"] = "Log out";
$install_lang["agent_top_menu_agent-main-logout-item_tooltip"] = "";
$install_lang["agent_top_menu_agent-main-my-account-item"] = "Account options";
$install_lang["agent_top_menu_agent-main-my-account-item_tooltip"] = "";
$install_lang["agent_top_menu_agent-main-my-profile-item"] = "My profile";
$install_lang["agent_top_menu_agent-main-my-profile-item_tooltip"] = "";
$install_lang["company_account_menu_company_my_account_item"] = "My account";
$install_lang["company_account_menu_company_my_account_item_tooltip"] = "";
$install_lang["company_account_menu_company_payments_item_company_my_account_balance_item"] = "Account balance";
$install_lang["company_account_menu_company_payments_item_company_my_account_balance_item_tooltip"] = "";
$install_lang["company_account_menu_company_my_agents_item"] = "My agents";
$install_lang["company_account_menu_company_my_agents_item_company_my_agents_list_item"] = "List of agents";
$install_lang["company_account_menu_company_my_agents_item_company_my_agents_list_item_tooltip"] = "";
$install_lang["company_account_menu_company_my_agents_item_company_my_agents_requests_item"] = "Agent inquiries";
$install_lang["company_account_menu_company_my_agents_item_company_my_agents_requests_item_tooltip"] = "";
$install_lang["company_account_menu_company_my_agents_item_tooltip"] = "";
$install_lang["company_account_menu_company_my_profile_item"] = "My profile";
$install_lang["company_account_menu_company_my_profile_item_company_my_profile_change_regdata_item"] = "Registration data";
$install_lang["company_account_menu_company_my_profile_item_company_my_profile_change_regdata_item_tooltip"] = "";
$install_lang["company_account_menu_company_my_profile_item_company_my_profile_deactivate_item"] = "De-activate profile";
$install_lang["company_account_menu_company_my_profile_item_company_my_profile_deactivate_item_tooltip"] = "";
$install_lang["company_account_menu_company_my_profile_item_company_my_profile_edit_item"] = "Edit profile";
$install_lang["company_account_menu_company_my_profile_item_company_my_profile_edit_item_tooltip"] = "";
$install_lang["company_account_menu_company_my_profile_item_company_my_profile_services_item"] = "Paid services";
$install_lang["company_account_menu_company_my_profile_item_company_my_profile_services_item_tooltip"] = "";
$install_lang["company_account_menu_company_my_profile_item_tooltip"] = "";
$install_lang["company_account_menu_company_overview_item"] = "Overview";
$install_lang["company_account_menu_company_overview_item_tooltip"] = "";
$install_lang["company_main_menu_company-main-menu-pro-item"] = "Professionals";
$install_lang["company_main_menu_company-main-menu-pro-item_company-main-menu-pro-agent-item"] = "Find an agent";
$install_lang["company_main_menu_company-main-menu-pro-item_company-main-menu-pro-agent-item_tooltip"] = "";
$install_lang["company_main_menu_company-main-menu-pro-item_company-main-menu-pro-company-item"] = "Find an agency";
$install_lang["company_main_menu_company-main-menu-pro-item_company-main-menu-pro-company-item_tooltip"] = "";
$install_lang["company_main_menu_company-main-menu-pro-item_company-main-menu-pro-private-item"] = "Find a private person";
$install_lang["company_main_menu_company-main-menu-pro-item_company-main-menu-pro-private-item_tooltip"] = "";
$install_lang["company_main_menu_company-main-menu-pro-item_tooltip"] = "";
$install_lang["company_top_menu_company-main-logout-item"] = "Log out";
$install_lang["company_top_menu_company-main-logout-item_tooltip"] = "";
$install_lang["company_top_menu_company-main-my-account-item"] = "Account options";
$install_lang["company_top_menu_company-main-my-account-item_tooltip"] = "";
$install_lang["company_top_menu_company-main-my-agents-item"] = "My agents";
$install_lang["company_top_menu_company-main-my-agents-item_tooltip"] = "";
$install_lang["company_top_menu_company-main-my-profile-item"] = "My profile";
$install_lang["company_top_menu_company-main-my-profile-item_tooltip"] = "";
$install_lang["guest_main_menu_main-menu-pro-item"] = "Professionals";
$install_lang["guest_main_menu_main-menu-pro-item_main-menu-pro-agent-item"] = "Find an agent";
$install_lang["guest_main_menu_main-menu-pro-item_main-menu-pro-agent-item_tooltip"] = "";
$install_lang["guest_main_menu_main-menu-pro-item_main-menu-pro-company-item"] = "Find an agency";
$install_lang["guest_main_menu_main-menu-pro-item_main-menu-pro-company-item_tooltip"] = "";
$install_lang["guest_main_menu_main-menu-pro-item_main-menu-pro-private-item"] = "Find a private person";
$install_lang["guest_main_menu_main-menu-pro-item_main-menu-pro-private-item_tooltip"] = "";
$install_lang["guest_main_menu_main-menu-pro-item_tooltip"] = "";
$install_lang["private_account_menu_private_my_account_item"] = "My account";
$install_lang["private_account_menu_private_my_account_item_tooltip"] = "";
$install_lang["private_account_menu_private_payments_item_private_my_account_balance_item"] = "Account balance";
$install_lang["private_account_menu_private_payments_item_private_my_account_balance_item_tooltip"] = "";
$install_lang["private_account_menu_private_my_profile_item"] = "My profile";
$install_lang["private_account_menu_private_my_profile_item_private_my_profile_change_regdata_item"] = "Registration data";
$install_lang["private_account_menu_private_my_profile_item_private_my_profile_change_regdata_item_tooltip"] = "";
$install_lang["private_account_menu_private_my_profile_item_private_my_profile_deactivate_item"] = "De-activate profile";
$install_lang["private_account_menu_private_my_profile_item_private_my_profile_deactivate_item_tooltip"] = "";
$install_lang["private_account_menu_private_my_profile_item_private_my_profile_edit_item"] = "Edit profile";
$install_lang["private_account_menu_private_my_profile_item_private_my_profile_edit_item_tooltip"] = "";
$install_lang["private_account_menu_private_my_profile_item_private_my_profile_services_item"] = "Paid services";
$install_lang["private_account_menu_private_my_profile_item_private_my_profile_services_item_tooltip"] = "";
$install_lang["private_account_menu_private_my_profile_item_tooltip"] = "";
$install_lang["private_account_menu_private_overview_item"] = "Overview";
$install_lang["private_account_menu_private_overview_item_tooltip"] = "";
$install_lang["private_main_menu_private-main-menu-pro-item"] = "Professionals";
$install_lang["private_main_menu_private-main-menu-pro-item_private-main-menu-pro-agent-item"] = "Find an agent";
$install_lang["private_main_menu_private-main-menu-pro-item_private-main-menu-pro-agent-item_tooltip"] = "";
$install_lang["private_main_menu_private-main-menu-pro-item_private-main-menu-pro-company-item"] = "Find an agency";
$install_lang["private_main_menu_private-main-menu-pro-item_private-main-menu-pro-company-item_tooltip"] = "";
$install_lang["private_main_menu_private-main-menu-pro-item_private-main-menu-pro-private-item"] = "Find a private person";
$install_lang["private_main_menu_private-main-menu-pro-item_private-main-menu-pro-private-item_tooltip"] = "";
$install_lang["private_main_menu_private-main-menu-pro-item_tooltip"] = "";
$install_lang["private_top_menu_private-main-logout-item"] = "Log out";
$install_lang["private_top_menu_private-main-logout-item_tooltip"] = "";
$install_lang["private_top_menu_private-main-my-account-item"] = "Account options";
$install_lang["private_top_menu_private-main-my-account-item_tooltip"] = "";
$install_lang["private_top_menu_private-main-my-profile-item"] = "My profile";
$install_lang["private_top_menu_private-main-my-profile-item_tooltip"] = "";
$install_lang["agent_account_menu_agent_payments_item"] = "Payments";
$install_lang["agent_account_menu_agent_payments_item_tooltip"] = "";
$install_lang["company_account_menu_company_payments_item"] = "Payments";
$install_lang["company_account_menu_company_payments_item_tooltip"] = "";
$install_lang["private_account_menu_private_payments_item"] = "Payments";
$install_lang["private_account_menu_private_payments_item_tooltip"] = "";
