<div class="region-box">
	<input type="button" class="with-icon-small i-search no-hover search-btn" id="country_open_{$country_helper_data.rand}" name="submit" />
	<span><input type="text" name="region_name" id="country_text_{$country_helper_data.rand}" autocomplete="off" value="{$country_helper_data.location_text}" placeholder="{l i='field_search_country' gid='listings'}"></span>
	<input type="hidden" name="{$country_helper_data.var_country_name}" id="country_hidden_{$country_helper_data.rand}" value="{$country_helper_data.country.code}">
	<input type="hidden" name="{$country_helper_data.var_region_name}" id="region_hidden_{$country_helper_data.rand}" value="{$country_helper_data.region.id}">
	<input type="hidden" name="{$country_helper_data.var_city_name}" id="city_hidden_{$country_helper_data.rand}" value="{$country_helper_data.city.id}">
</div>

{js module=countries file='country-input.js'}
<script type='text/javascript'>
{if $country_helper_data.var_js_name}var {$country_helper_data.var_js_name};{/if}
{literal}
$(function(){
{/literal}var region_{$country_helper_data.rand} = {literal}new countryInput({
		siteUrl: '{/literal}{$site_url}{literal}',
		rand: '{/literal}{$country_helper_data.rand}{literal}',
		id_country: '{/literal}{$country_helper_data.country.code}{literal}',
		id_region: '{/literal}{$country_helper_data.region.id}{literal}',
		id_city: '{/literal}{$country_helper_data.city.id}{literal}',
		select_type: '{/literal}{$country_helper_data.select_type}{literal}'
	});
});
{/literal}</script>