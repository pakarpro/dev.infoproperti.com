<!-- select location -->
<div id="country_select_{$country_helper_data.rand}" class="controller-select button-select-location">
	<span id="country_text_{$country_helper_data.rand}" class="location-output">
	{if $country_helper_data.country}{$country_helper_data.country.name}{/if}{if $country_helper_data.region}, {$country_helper_data.region.name}{/if}{if $country_helper_data.city}, {$country_helper_data.city.name}{/if}
	</span>
	<a href="#" id="country_open_{$country_helper_data.rand}" class="">{l i='link_select_region' gid='countries'}</a>
	<input type="hidden" name="{$country_helper_data.var_country_name}" id="country_hidden_{$country_helper_data.rand}" value="{$country_helper_data.country.code}" data-name="{$country_helper_data.country.name|escape}">
	<input type="hidden" name="{$country_helper_data.var_region_name}" id="region_hidden_{$country_helper_data.rand}" value="{$country_helper_data.region.id}" data-name="{$country_helper_data.region.name|escape}">
	<input type="hidden" name="{$country_helper_data.var_city_name}" id="city_hidden_{$country_helper_data.rand}" value="{$country_helper_data.city.id}" data-name="{$country_helper_data.city.name|escape}">
</div>

{js module=countries file='country-select.js'}
<script type='text/javascript'>
{if $country_helper_data.var_js_name}var {$country_helper_data.var_js_name};{/if}
{literal}
$(function(){
	{/literal}{if $country_helper_data.var_js_name}{$country_helper_data.var_js_name} = {/if}{literal}new countrySelect({
		siteUrl: '{/literal}{$site_url}{literal}',
		rand: '{/literal}{$country_helper_data.rand}{literal}',
		id_country: '{/literal}{$country_helper_data.country.code}{literal}',
		id_region: '{/literal}{$country_helper_data.region.id}{literal}',
		id_city: '{/literal}{$country_helper_data.city.id}{literal}',
		select_type: '{/literal}{$country_helper_data.select_type}{literal}'
	});
});
{/literal}</script>
