{if !$geomap_js_loaded}
<script src="http://maps.google.com/maps/api/js?v=3.8&libraries=places{if $map_reg_key}&key={$map_reg_key}{/if}&sensor=true"></script>
{js module=geomap file='googlemapsv3.js'}
{/if}
<script>{literal}
var geocoder;
$(function(){
	geocoder = new GoogleMapsv3_Geocoder({});
});
{/literal}</script>
