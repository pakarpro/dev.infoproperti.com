<script type="text/javascript">{strip}{literal}
	var options = {};
	ymaps.ready(function(){
		{/literal}
		{$map_id}.clear();
		{foreach item=item from=$markers}
			options = {literal}{{/literal}
				{if $item.gid}gid: '{$item.gid}',{/if}
				{if $item.dragging}draggable: true, 
					{if $view_settings.drag_listener}drag_listener: {$view_settings.drag_listener},{/if}
				{/if}
				{if $item.info}info: '{strip}{'\''|str_replace:'\\\'':$item.info}{/strip}',{/if}
			{literal}}{/literal};
			{$map_id}.addMarker({$item.lat}, {$item.lon}, options);
		{/foreach}
		{literal}
	});
{/literal}{/strip}</script>
