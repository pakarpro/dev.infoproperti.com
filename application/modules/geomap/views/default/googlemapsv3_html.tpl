{if !$geomap_js_loaded && !$only_load_content}
<link href="{$site_root}application/modules/geomap/views/default/css/googlev3.css" rel="stylesheet" type="text/css">
<script src="http://maps.google.com/maps/api/js?v=3.8&libraries=places{if $map_reg_key}&key={$map_reg_key}{/if}&sensor=true"></script>
{js module=geomap file='googlemapsv3.js'}
{/if}
{if !$only_load_scripts}
{if !$map_id}{assign var='map_id' value='map_'+$rand}{/if}
<div id="{$map_id}" class="{$view_settings.class} map_container">&nbsp;{l i='loading' gid='geomap'}</div>
{if $settings.use_panorama && !$view_settings.panorama_container}
<div id="pano_container_{$rand}" class="pano_container"></div>
{/if}
{if $settings.use_router}
<div id="routes_{$rand}" class="routes"></div>
<div id="route_links_{$rand}" class="routes-btn">
	<a href="javascript:void(0);" id="add_route_btn_{$rand}">{l i='route_add' gid='geomap'}</a>
	<a href="javascript:void(0);" id="remove_route_btn_{$rand}" class="hide">{l i='route_delete' gid='geomap'}</a>
</div>
{/if}
<script>{literal}
var {/literal}{$map_id}{literal};
$(function(){
	{/literal}{$map_id}{literal} = new GoogleMapsv3({
		{/literal}
		map_container: '{$map_id}',
		default_zoom: {$settings.zoom},
		{if $settings.view_type}default_map_type: {$settings.view_type},{/if}
		width: {$view_settings.width},
		height: {$view_settings.height},
		lat: {$settings.lat},
		lon: {$settings.lon},
		{if !$view_settings.disable_smart_zoom && $settings.use_smart_zoom}use_smart_zoom: true,{/if}
		{if $settings.use_type_selector}use_type_selector: true,{/if}
		{if $settings.use_panorama && !$view_settings.panorama_disable}
		use_panorama: true,
		panorama_container: '{if $view_settings.panorama_container}{$view_settings.panorama_container}{else}pano_container_{$rand}{/if}',
		{/if}
		{if $settings.use_searchbox}use_searchbox: true,{/if}
		{if $settings.use_search_radius}use_search_radius: true,{/if}
		{if $settings.use_show_details}use_show_details: true,{/if}
		{if $view_settings.zoom_listener}zoom_listener: {$view_settings.zoom_listener},{/if}
		{if $view_settings.type_listener}type_listener: {$view_settings.type_listener},{/if}
		{if $view_settings.geocode_listener}geocode_listener: {$view_settings.geocode_listener},{/if}
		{if $settings.media.icon}icon: '{$settings.media.icon.thumbs.small}',{/if}
		{if $settings.use_router}
		use_router: true,
		routes_container: 'routes_{$rand}',
		{/if}
		{if $settings.use_amenities && $settings.amenities|count}
		use_amenities: true,
		amenities: ['{"','"|implode:$settings.amenities}'],
		amenities_names: {$amenities_names_str},
		{/if}
		{literal}
	});
	
	{/literal}{if $settings.use_router}{literal}
	$('#add_route_btn_{/literal}{$rand}{literal}').bind('click', function(){
		$('#add_route_btn_{/literal}{$rand}{literal}').hide()
		$('#remove_route_btn_{/literal}{$rand}{literal}').show();
		{/literal}{$map_id}{literal}.createRoute();
	});
	
	$('#remove_route_btn_{/literal}{$rand}{literal}').bind('click', function(){
		$('#remove_route_btn_{/literal}{$rand}{literal}').hide()
		$('#add_route_btn_{/literal}{$rand}{literal}').show();
		{/literal}{$map_id}{literal}.deleteRoute();
	});
	{/literal}{/if}{literal}
	
	{/literal}{foreach item=item from=$markers}{literal}
		{/literal}{$map_id}{literal}.addMarker({/literal}{$item.lat}{literal}, {/literal}{$item.lon}{literal}, {
			{/literal}
			{if $item.gid}gid: '{$item.gid}',{/if}
			{if $item.dragging}draggable: true, 
			{if $view_settings.drag_listener}drag_listener: {$view_settings.drag_listener},{/if}
			{/if}
			{if $item.info}info: '{strip}{'\''|str_replace:'\\\'':$item.info}{/strip}',{/if}
			{literal}
		});
	{/literal}{/foreach}{literal}
});
{/literal}</script>
{/if}
