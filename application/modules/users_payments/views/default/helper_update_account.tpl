<div class="content-block">

	<div class="edit_block">
		<div class="r">
			<div class="f">{l i='on_account_header' gid='users_payments'}: <b>{block name=currency_format_output module=start value=$data.account cur_gid=$base_currency.gid}</b></div>
		</div>
		<h2 id="account">{l i='header_add_funds' gid='users_payments'}</h1>
		<form action="{$site_url}users_payments/save_payment/" method="post" >
		<div class="r">
			<div class="f">{l i='field_amount' gid='users_payments'}:&nbsp;* </div>
			<div class="v"><input type="text" name="amount" class="short"> {block name=currency_output module=start cur_gid=$base_currency.gid}</div>
		</div>
		{if $billing_systems}
		<div class="r">
			<div class="f">{l i='field_billing' gid='users_payments'}:&nbsp;* </div>
			<div class="v"><select name="system_gid" id="system_gid" class="middle"><option value="">...</option>{foreach item=item from=$billing_systems}<option value="{$item.gid}">{$item.name}</option>{/foreach}</select></div>
		</div>
		{foreach item=item from=$billing_systems}
		<div class="r hide" id="details_{$item.gid}">
			<div class="f">{l i='field_info_data' gid='payments'}:</div>
			<div class="v">{$item.info_data}</div>
		</div>
		{/foreach}
		<div class="r">
			<div class="b"><input type="submit" class='btn' value="{l i='btn_send' gid='start' type='button'}" name="btn_payment_save"></div>
		</div>
		<script>{literal}
			$(function(){
				$('#system_gid').bind('change', function(){
					{/literal}{foreach item=item from=$billing_systems}{literal}
					$('#details_{/literal}{$item.gid}{literal}').hide();
					{/literal}{if $item.info_data}{literal}
					if($(this).val() == '{/literal}{$item.gid}{literal}') 
						$('#details_{/literal}{$item.gid}{literal}').show();
					{/literal}{/if}{literal}
					{/literal}{/foreach}{literal}
				});
			});
		{/literal}</script>
		{else}
		<div class="r">
			<i>{l i='error_empty_billing_system_list' gid='users_payments'}</i>
		</div>
		{/if}
		</form>
	</div>
</div>
