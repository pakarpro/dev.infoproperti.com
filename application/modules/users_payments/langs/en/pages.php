<?php

$install_lang["account_funds_add_message"] = "Add funds";
$install_lang["account_funds_spend_message"] = "Write off funds";
$install_lang["added_by_admin"] = "Added by admin";
$install_lang["error_empty_amount"] = "Empty amount";
$install_lang["error_empty_billing_system_list"] = "Service is temporarily unavailable";
$install_lang["error_empty_system_gid"] = "Choose payment system";
$install_lang["error_empty_users_list"] = "Empty user list";
$install_lang["error_invalid_amount"] = "Invalid amount";
$install_lang["error_money_not_sufficient"] = "Insufficient funds on the account";
$install_lang["error_no_users_to_add_funds"] = "Users are not selected";
$install_lang["field_amount"] = "Enter payment amount";
$install_lang["field_billing"] = "Choose payment method";
$install_lang["header_add_funds"] = "Add funds to account";
$install_lang["link_add_funds"] = "Add funds +";
$install_lang["on_account_header"] = "Account balance";
$install_lang["success_add_funds"] = "Account is successfully updated";

