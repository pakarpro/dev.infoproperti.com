<?php

$install_lang["admin_fields_menu_fields_list_item"] = "Fields";
$install_lang["admin_fields_menu_fields_list_item_tooltip"] = "";
$install_lang["admin_fields_menu_forms_list_item"] = "Forms";
$install_lang["admin_fields_menu_forms_list_item_tooltip"] = "";
$install_lang["admin_fields_menu_sections_list_item"] = "Sections";
$install_lang["admin_fields_menu_sections_list_item_tooltip"] = "";
$install_lang["admin_menu_settings_items_system-items_field_editor_menu_item"] = "Field editor";
$install_lang["admin_menu_settings_items_system-items_field_editor_menu_item_tooltip"] = "Manage search forms, fields & blocks of information in a listing";

