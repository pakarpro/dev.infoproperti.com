<?php

$install_lang["field_type"]["header"] = "Field type";
$install_lang["field_type"]["option"]["checkbox"] = "Checkbox";
$install_lang["field_type"]["option"]["select"] = "Drop-down";
$install_lang["field_type"]["option"]["text"] = "Text field";
$install_lang["field_type"]["option"]["textarea"] = "Text area";
$install_lang["field_type"]["option"]["multiselect"] = "Multi-select";


