<?php

$install_lang["field_type"]["header"] = "Тип поля";
$install_lang["field_type"]["option"]["checkbox"] = "Чекбокс";
$install_lang["field_type"]["option"]["select"] = "Раскрывающийся список";
$install_lang["field_type"]["option"]["text"] = "Текстовое поле";
$install_lang["field_type"]["option"]["textarea"] = "Текстовая область";
$install_lang["field_type"]["option"]["multiselect"] = "Список с множественным выбором";


