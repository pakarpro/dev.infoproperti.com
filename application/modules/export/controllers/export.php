<?php
/**
* Export user side controller
* 
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/
class Export extends Controller{
	
	/**
	 * Constructor
	 *
	 * @return Export
	 */
	public function __construct(){
		parent::Controller();
		$this->load->model("Menu_model");
	}
	
	/**
	 * Render simple generation action
	 * @param integer $selection_id selection identifier
	 */
	public function index($selection_id){
		if($this->session->userdata("auth_type") != "user"){show_404();	return;}
		
		$this->load->model("export/models/export_module_model", "Export_module_model");
		$this->load->model("Export_model");

		$data = $this->Export_model->get_selection_by_id($selection_id, true);
		if($data["output_type"] != "browser" || !$data["published"]){show_404();return;}		

		$user_id = $this->session->userdata("user_id");
		$this->Export_model->generate_selection($selection_id, $data['admin_form_data'], $user_id);
	}
	
	/**
	 * Render advanced generation action
	 * @param integer $selection_id selection identifier
	 */
	public function advanced($selection_id){
		if($this->session->userdata("auth_type") != "user"){show_404();	return;}
		
		$this->load->model("export/models/export_module_model", "Export_module_model");
		$this->load->model("Export_model");

		$data = $this->Export_model->get_selection_by_id($selection_id, true);
		if($data["output_type"] != "browser" || !$data["published"] || !$data["editable"]){show_404();return;}		

		if($this->input->post("btn_save")){
			$form_data = $this->Export_module_model->process_search_form($data["id_object"], false);
			$user_id = $this->session->userdata("user_id");
			$this->Export_model->generate_selection($selection_id, $form_data, $user_id);
		}		
		$this->Menu_model->breadcrumbs_set_active(l("header_my_export", "export")." ".$data["driver"]["output_name"]);
		$search_form = $this->Export_module_model->get_search_form($data["id_object"], $data['admin_form_data'], false);
		$this->template_lite->assign("export_form", $search_form);		
		$this->template_lite->assign("data", $data);		
		$this->template_lite->view("advanced");		
	}
}
