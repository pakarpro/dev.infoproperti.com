<?php

$module["module"] = "export";
$module["install_name"] = "Export";
$module["install_descr"] = "Content export";
$module["version"] = "1.01";
$module["files"] = array(
	array("file", "read", "application/modules/export/controllers/admin_export.php"),
	array("file", "read", "application/modules/export/controllers/api_export.php"),
	array("file", "read", "application/modules/export/controllers/export.php"),
	array("file", "read", "application/modules/export/install/module.php"),
	array("file", "read", "application/modules/export/install/permissions.php"),
	array("file", "read", "application/modules/export/install/structure_deinstall.sql"),
	array("file", "read", "application/modules/export/install/structure_install.sql"),
	array("file", "read", "application/modules/export/models/drivers/export_csv_model.php"),
	array("file", "read", "application/modules/export/models/drivers/export_oodle_model.php"),
	array("file", "read", "application/modules/export/models/drivers/export_synda_agents_model.php"),
	array("file", "read", "application/modules/export/models/drivers/export_synda_listings_model.php"),
	array("file", "read", "application/modules/export/models/drivers/export_tff_model.php"),
	array("file", "read", "application/modules/export/models/drivers/export_vast_model.php"),
	array("file", "read", "application/modules/export/models/drivers/export_xml_model.php"),
	array("file", "read", "application/modules/export/models/drivers/export_yrl_model.php"),
	array("file", "read", "application/modules/export/models/drivers/export_zif_model.php"),
	array("file", "read", "application/modules/export/models/export_driver_model.php"),
	array("file", "read", "application/modules/export/models/export_install_model.php"),
	array("file", "read", "application/modules/export/models/export_model.php"),
	array("file", "read", "application/modules/export/models/export_module_model.php"),
	array("file", "read", "application/modules/export/views/admin/driver_csv_edit.tpl"),
	array("file", "read", "application/modules/export/views/admin/driver_edit.tpl"),
	array("file", "read", "application/modules/export/views/admin/driver_field_edit.tpl"),
	array("file", "read", "application/modules/export/views/admin/driver_fields.tpl"),
	array("file", "read", "application/modules/export/views/admin/drivers_list.tpl"),
	array("file", "read", "application/modules/export/views/admin/edit.tpl"),
	array("file", "read", "application/modules/export/views/admin/list.tpl"),
	array("file", "read", "application/modules/export/views/default/advanced.tpl"),
	array("dir", "read", "application/modules/export/langs"),
);

$module["dependencies"] = array(
	"start" => array("version"=>"1.01"),
	"menu" => array("version"=>"1.01"),
);

$module["linked_modules"] = array(
	"install" => array(
		"menu"			=> "install_menu",
		"ausers"		=> "install_ausers",
		"cronjob"		=> "install_cronjob",
	),
	"deinstall" => array(
		"menu"			=> "deinstall_menu",
		"ausers"		=> "deinstall_ausers",
		"cronjob"		=> "deinstall_cronjob",
	)
);

