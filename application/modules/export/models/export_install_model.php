<?php
/**
* Export install model
* 
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/
class Export_install_model extends Model{
	
	/**
	 * Link to CodeIgniter object
	 * @var object
	 */
	private $CI;

	/**
	 * Menu configuration
	 * @var array
	 */
	private $menu = array(
		"admin_menu" => array(
			"action" => "none",
			"items" => array(
				"exp-import-items" => array(
					"action" => "none",
					"items" => array(
						"export_menu_item" => array("action" => "create", "link" => "admin/export/index", "status" => 1, "sorter" => 2),
					),
				),
			),			
		),
		
		"admin_export_menu" => array(
			"action" => "create",
			"name" => "Admin mode - Export listings",
			"items" => array(
				"export_selections_item" => array("action" => "create", "link" => "admin/export/index", "status" => 1),
				"export_drivers_item" => array("action" => "create", "link" => "admin/export/drivers", "status" => 1),
			),
		),
	);
	
	/**
	 * Ausers configuration
	 * @var array
	 */
	private $ausers = array(
		array("module" => "export", "method" => "index", "is_default" => 1),
		array("module" => "export", "method" => "drivers", "is_default" => 0),
	);
	
	/**
	 * Driver configuration
	 * @var array
	 */
	private $drivers = array(
		array(
			"gid" => "csv", 
			"link" => "http://mastpoint.curzonnassau.com/csv-1203/", 
			"settings" => array("delimiter"=>";", "enclosure" => "\""),
			"elements" => array(
				array("name"=>"id", "type"=>"int", "required"=>0),
				array("name"=>"id_user", "type"=>"int", "required"=>0),
				array("name"=>"user_name", "type"=>"text", "required"=>0),
				array("name"=>"id_type", "type"=>"int", "required"=>0),
				array("name"=>"type", "type"=>"text", "required"=>0),
				array("name"=>"id_category", "type"=>"int", "required"=>0),
				array("name"=>"category_name", "type"=>"text", "required"=>0),
				array("name"=>"id_property_type", "type"=>"int", "required"=>0),
				array("name"=>"property_type", "type"=>"text", "required"=>0),
				array("name"=>"status", "type"=>"text", "required"=>0),
				array("name"=>"country_code", "type"=>"text", "required"=>0),
				array("name"=>"id_region", "type"=>"int", "required"=>0),
				array("name"=>"id_city", "type"=>"int", "required"=>0),
				array("name"=>"country", "type"=>"text", "required"=>0),
				array("name"=>"region", "type"=>"text", "required"=>0),
				array("name"=>"region_code", "type"=>"text", "required"=>0),
				array("name"=>"city", "type"=>"text", "required"=>0),
				array("name"=>"address", "type"=>"text", "required"=>0),
				array("name"=>"zip", "type"=>"text", "required"=>0),
				array("name"=>"sold", "type"=>"int", "required"=>0),
				array("name"=>"price", "type"=>"int", "required"=>0),
				array("name"=>"price_max", "type"=>"int", "required"=>0),
				array("name"=>"gid_currency", "type"=>"text", "required"=>0),
				array("name"=>"square", "type"=>"int", "required"=>0),
				array("name"=>"square_max", "type"=>"int", "required"=>0),
				array("name"=>"square_unit", "type"=>"text", "required"=>0),
				array("name"=>"date_created", "type"=>"text", "required"=>0),
				array("name"=>"date_modified", "type"=>"text", "required"=>0),
				array("name"=>"date_publish", "type"=>"text", "required"=>0),
				array("name"=>"date_expire", "type"=>"text", "required"=>0),
				array("name"=>"date_open", "type"=>"text", "required"=>0),
				array("name"=>"uploads", "type"=>"file", "required"=>0),
			),
			"editable" => 1,
			"status" => 1,
		),
		array(
			"gid" => "xml", 
			"link" => "http://www.w3.org/XML/", 
			"settings" => array(),
			"elements" => array(
				array("name"=>"id", "type"=>"int", "required"=>0),
				array("name"=>"id_user", "type"=>"int", "required"=>0),
				array("name"=>"user_name", "type"=>"text", "required"=>0),
				array("name"=>"id_type", "type"=>"int", "required"=>0),
				array("name"=>"type", "type"=>"text", "required"=>0),
				array("name"=>"id_category", "type"=>"int", "required"=>0),
				array("name"=>"category_name", "type"=>"text", "required"=>0),
				array("name"=>"id_property_type", "type"=>"int", "required"=>0),
				array("name"=>"property_type", "type"=>"text", "required"=>0),
				array("name"=>"status", "type"=>"text", "required"=>0),
				array("name"=>"country_code", "type"=>"text", "required"=>0),
				array("name"=>"id_region", "type"=>"int", "required"=>0),
				array("name"=>"id_city", "type"=>"int", "required"=>0),
				array("name"=>"country", "type"=>"text", "required"=>0),
				array("name"=>"region", "type"=>"text", "required"=>0),
				array("name"=>"region_code", "type"=>"text", "required"=>0),
				array("name"=>"city", "type"=>"text", "required"=>0),
				array("name"=>"address", "type"=>"text", "required"=>0),
				array("name"=>"zip", "type"=>"text", "required"=>0),
				array("name"=>"sold", "type"=>"int", "required"=>0),
				array("name"=>"price", "type"=>"int", "required"=>0),
				array("name"=>"price_max", "type"=>"int", "required"=>0),
				array("name"=>"gid_currency", "type"=>"text", "required"=>0),
				array("name"=>"square", "type"=>"int", "required"=>0),
				array("name"=>"square_max", "type"=>"int", "required"=>0),
				array("name"=>"square_unit", "type"=>"text", "required"=>0),
				array("name"=>"date_created", "type"=>"text", "required"=>0),
				array("name"=>"date_modified", "type"=>"text", "required"=>0),
				array("name"=>"date_publish", "type"=>"text", "required"=>0),
				array("name"=>"date_expire", "type"=>"text", "required"=>0),
				array("name"=>"date_open", "type"=>"text", "required"=>0),
				array("name"=>"uploads", "type"=>"file", "required"=>0),
			),
			"editable" => 1,
			"status" => 1,
		),
		array(
			"gid" => "yrl", 
			"link" => "http://help.yandex.ru/webmaster/?id=1113400", 
			"settings" => array(),
			"elements" => array(
				array("name"=>"realty-type", "type"=>"text", "required"=>0),
				array("name"=>"internal-id", "type"=>"int", "required"=>1),
				array("name"=>"type", "type"=>"text", "required"=>1),
				array("name"=>"property-type", "type"=>"text", "required"=>1),
				array("name"=>"category", "type"=>"text", "required"=>1),
				array("name"=>"url", "type"=>"text", "required"=>1),
				array("name"=>"creation-date", "type"=>"text", "required"=>1),
				array("name"=>"last-update-date", "type"=>"text", "required"=>0),
				array("name"=>"expire-date", "type"=>"text", "required"=>0),
				array("name"=>"payed-adv", "type"=>"int", "required"=>0),
				array("name"=>"manually-added", "type"=>"int", "required"=>0),
				
				array("name"=>"location.country", "type"=>"text", "required"=>1),
				array("name"=>"location.region", "type"=>"text", "required"=>0),
				array("name"=>"location.district", "type"=>"text", "required"=>0),
				array("name"=>"location.locality-name", "type"=>"text", "required"=>0),
				array("name"=>"location.sub-locality-name", "type"=>"text", "required"=>0),
				array("name"=>"location.non-admin-sub-locality", "type"=>"text", "required"=>0),
				array("name"=>"location.address", "type"=>"text", "required"=>0),
				array("name"=>"location.direction", "type"=>"text", "required"=>0),
				array("name"=>"location.distance", "type"=>"text", "required"=>0),
				array("name"=>"location.latitude", "type"=>"text", "required"=>0),
				array("name"=>"location.longitude", "type"=>"text", "required"=>0),
				array("name"=>"location.metro", "type"=>"text", "required"=>0),
				array("name"=>"location.time-on-transport", "type"=>"text", "required"=>0),
				array("name"=>"location.time-on-foot", "type"=>"text", "required"=>0),
				array("name"=>"location.railway-station", "type"=>"text", "required"=>0),
				
				array("name"=>"sales-agent.name", "type"=>"text", "required"=>0),
				array("name"=>"sales-agent.phone", "type"=>"text", "required"=>1),
				array("name"=>"sales-agent.category", "type"=>"text", "required"=>0),
				array("name"=>"sales-agent.organization", "type"=>"text", "required"=>0),
				array("name"=>"sales-agent.agency-id", "type"=>"int", "required"=>0),
				array("name"=>"sales-agent.url", "type"=>"text", "required"=>0),
				array("name"=>"sales-agent.email", "type"=>"text", "required"=>0),
				array("name"=>"sales-agent.partner", "type"=>"text", "required"=>0),
				
				array("name"=>"price.value", "type"=>"int", "required"=>1),
				array("name"=>"price.currency", "type"=>"text", "required"=>1),
				array("name"=>"price.period", "type"=>"text", "required"=>0),
				array("name"=>"price.unit", "type"=>"text", "required"=>0),
				
				array("name"=>"not-for-agents", "type"=>"int", "required"=>0),
				array("name"=>"haggle", "type"=>"int", "required"=>0),
				array("name"=>"mortgage", "type"=>"int", "required"=>0),
				array("name"=>"prepayment", "type"=>"text", "required"=>0),
				array("name"=>"rent-pledge", "type"=>"int", "required"=>0),
				array("name"=>"agent-fee", "type"=>"text", "required"=>0),
				array("name"=>"with-pets", "type"=>"int", "required"=>0),
				array("name"=>"with-children", "type"=>"int", "required"=>0),
				array("name"=>"image", "type"=>"text", "required"=>0),
				array("name"=>"renovation", "type"=>"text", "required"=>0),
				array("name"=>"description", "type"=>"text", "required"=>0),
				array("name"=>"area.value", "type"=>"int", "required"=>0),
				array("name"=>"area.unit", "type"=>"text", "required"=>0),
				array("name"=>"living-space.value", "type"=>"text", "required"=>0),
				array("name"=>"living-space.unit", "type"=>"text", "required"=>0),
				array("name"=>"kitchen-space.value", "type"=>"text", "required"=>0),
				array("name"=>"kitchen-space.unit", "type"=>"text", "required"=>0),
				array("name"=>"lot-area.value", "type"=>"text", "required"=>0),
				array("name"=>"lot-area.unit", "type"=>"text", "required"=>0),
				array("name"=>"new-flat", "type"=>"int", "required"=>0),
				array("name"=>"rooms", "type"=>"int", "required"=>1),
				array("name"=>"rooms-offered", "type"=>"text", "required"=>1),
				array("name"=>"open-plan", "type"=>"int", "required"=>0),
				array("name"=>"rooms-type", "type"=>"text", "required"=>0),
				array("name"=>"phone", "type"=>"int", "required"=>0),
				array("name"=>"internet", "type"=>"int", "required"=>0),
				array("name"=>"room-furniture", "type"=>"int", "required"=>0),
				array("name"=>"kitchen-furniture", "type"=>"int", "required"=>0),
				array("name"=>"television", "type"=>"int", "required"=>0),
				array("name"=>"washing-machine", "type"=>"int", "required"=>0),
				array("name"=>"refrigerator", "type"=>"int", "required"=>0),
				array("name"=>"balcony", "type"=>"text", "required"=>0),
				array("name"=>"bathroom-unit", "type"=>"text", "required"=>0),
				array("name"=>"floor-covering", "type"=>"text", "required"=>0),
				array("name"=>"window-view", "type"=>"text", "required"=>0),
				array("name"=>"floor", "type"=>"text", "required"=>0),
				array("name"=>"floors-total", "type"=>"text", "required"=>0),
				array("name"=>"building-name", "type"=>"text", "required"=>0),
				array("name"=>"building-type", "type"=>"text", "required"=>0),
				array("name"=>"building-series", "type"=>"text", "required"=>0),
				array("name"=>"building-state", "type"=>"text", "required"=>0),
				array("name"=>"built-year", "type"=>"text", "required"=>0),
				array("name"=>"ready-quarter", "type"=>"text", "required"=>0),
				array("name"=>"lift", "type"=>"int", "required"=>0),
				array("name"=>"rubbish-chute", "type"=>"int", "required"=>0),
				array("name"=>"is-elite", "type"=>"int", "required"=>0),
				array("name"=>"parking", "type"=>"int", "required"=>0),
				array("name"=>"alarm", "type"=>"int", "required"=>0),
				array("name"=>"ceiling-height", "type"=>"text", "required"=>0),
				array("name"=>"pmg", "type"=>"int", "required"=>0),
				array("name"=>"toilet", "type"=>"text", "required"=>0),
				array("name"=>"shower", "type"=>"text", "required"=>0),
				array("name"=>"kitchen", "type"=>"int", "required"=>0),
				array("name"=>"pool", "type"=>"int", "required"=>0),
				array("name"=>"billiard", "type"=>"int", "required"=>0),
				array("name"=>"sauna", "type"=>"int", "required"=>0),
				array("name"=>"heating-supply", "type"=>"int", "required"=>0),
				array("name"=>"water-supply", "type"=>"int", "required"=>0),
				array("name"=>"sewerage-supply", "type"=>"int", "required"=>0),
				array("name"=>"electricity-supply", "type"=>"int", "required"=>0),
				array("name"=>"gas-supply", "type"=>"int", "required"=>0),
			),
			"editable" => 0,
			"status" => 1,
		),
		array(
			"gid" => "zif", 
			"link" => "http://www.zillow.com/feeds/Feeds.htm", 
			"settings" => array(),
			"elements" => array(
				array("name"=>"Location.StreetAddress", "type"=>"text", "required"=>1),
				array("name"=>"Location.UnitNumber", "type"=>"int", "required"=>0),
				array("name"=>"Location.City", "type"=>"text", "required"=>1),
				array("name"=>"Location.State", "type"=>"text", "required"=>1),
				array("name"=>"Location.Zip", "type"=>"text", "required"=>1),				
				array("name"=>"Location.ParcelId", "type"=>"text", "required"=>0),
				array("name"=>"Location.ZPID", "type"=>"int", "required"=>0),
				array("name"=>"Location.Lat", "type"=>"text", "required"=>0),
				array("name"=>"Location.Long", "type"=>"text", "required"=>0),
				array("name"=>"Location.County", "type"=>"text", "required"=>0),
				array("name"=>"Location.StreetIntersection", "type"=>"text", "required"=>0),
				array("name"=>"Location.DisplayAddress", "type"=>"text", "required"=>0),				
				array("name"=>"ListingType", "type"=>"text", "required"=>1),
				array("name"=>"ListingSold", "type"=>"int", "required"=>1),				
				array("name"=>"ListingDetails.Status", "type"=>"int", "required"=>1),				
				array("name"=>"ListingDetails.Price", "type"=>"int", "required"=>1),
				array("name"=>"ListingDetails.ListingUrl", "type"=>"text", "required"=>0),
				array("name"=>"ListingDetails.MlsId", "type"=>"text", "required"=>0),
				array("name"=>"ListingDetails.MlsName", "type"=>"text", "required"=>0),
				array("name"=>"ListingDetails.DateListed", "type"=>"text", "required"=>0),
				array("name"=>"ListingDetails.DateSold", "type"=>"text", "required"=>0),
				array("name"=>"ListingDetails.VirtualTourUrl", "type"=>"text", "required"=>0),
				array("name"=>"ListingDetails.ListingEmail", "type"=>"text", "required"=>0),
				array("name"=>"ListingDetails.AlwaysEmailAgent", "type"=>"text", "required"=>0),
				array("name"=>"RentalDetails.Availability", "type"=>"text", "required"=>0),
				array("name"=>"RentalDetails.LeaseTerm", "type"=>"text", "required"=>0),
				array("name"=>"RentalDetails.DepositFees", "type"=>"text", "required"=>0),
				array("name"=>"RentalDetails.UtilitiesIncluded.Water", "type"=>"int", "required"=>0),
				array("name"=>"RentalDetails.UtilitiesIncluded.Sewage", "type"=>"int", "required"=>0),
				array("name"=>"RentalDetails.UtilitiesIncluded.Garbage", "type"=>"int", "required"=>0),
				array("name"=>"RentalDetails.UtilitiesIncluded.Electricity", "type"=>"int", "required"=>0),
				array("name"=>"RentalDetails.UtilitiesIncluded.Gas", "type"=>"int", "required"=>0),
				array("name"=>"RentalDetails.UtilitiesIncluded.Internet", "type"=>"int", "required"=>0),
				array("name"=>"RentalDetails.UtilitiesIncluded.Cable", "type"=>"int", "required"=>0),
				array("name"=>"RentalDetails.UtilitiesIncluded.SatTV", "type"=>"int", "required"=>0),
				array("name"=>"RentalDetails.PetsAllowed.NoPets", "type"=>"int", "required"=>0),
				array("name"=>"RentalDetails.PetsAllowed.Cats", "type"=>"int", "required"=>0),
				array("name"=>"RentalDetails.PetsAllowed.SmallDogs", "type"=>"int", "required"=>0),
				array("name"=>"RentalDetails.PetsAllowed.LargeDog", "type"=>"int", "required"=>0),				
				array("name"=>"BasicDetails.PropertyType", "type"=>"text", "required"=>1),
				array("name"=>"BasicDetails.Title", "type"=>"text", "required"=>0),
				array("name"=>"BasicDetails.Description", "type"=>"text", "required"=>0),
				array("name"=>"BasicDetails.Bedrooms", "type"=>"int", "required"=>0),
				array("name"=>"BasicDetails.Bathrooms", "type"=>"int", "required"=>0),
				array("name"=>"BasicDetails.FullBathrooms", "type"=>"int", "required"=>0),
				array("name"=>"BasicDetails.HalfBathrooms", "type"=>"int", "required"=>0),
				array("name"=>"BasicDetails.LivingArea", "type"=>"int", "required"=>0),
				array("name"=>"BasicDetails.LotSize", "type"=>"int", "required"=>0),
				array("name"=>"BasicDetails.YearBuilt", "type"=>"int", "required"=>0),				
				array("name"=>"Pictures", "type"=>"text", "required"=>0),
				//array("name"=>"Pictures.Picture.PictureUrl", "type"=>"text", "required"=>0),
				//array("name"=>"Pictures.Picture.PictureCaption", "type"=>"text", "required"=>0),				
				array("name"=>"Agent.FirstName", "type"=>"text", "required"=>0),
				array("name"=>"Agent.LastName", "type"=>"text", "required"=>0),
				array("name"=>"Agent.EmailAddress", "type"=>"text", "required"=>1),
				array("name"=>"Agent.PictureUrl", "type"=>"text", "required"=>0),
				array("name"=>"Agent.OfficeLineNumber", "type"=>"text", "required"=>0),
				array("name"=>"Agent.MobilePhoneLineNumber", "type"=>"text", "required"=>0),
				array("name"=>"Agent.FaxLineNumber", "type"=>"text", "required"=>0),
				array("name"=>"Office.BrokerageName", "type"=>"text", "required"=>0),
				array("name"=>"Office.BrokerPhone", "type"=>"text", "required"=>1),
				array("name"=>"Office.BrokerEmail", "type"=>"text", "required"=>0),
				array("name"=>"Office.BrokerWebsite", "type"=>"text", "required"=>0),
				array("name"=>"Office.StreetAddress", "type"=>"text", "required"=>0),
				array("name"=>"Office.UnitNumber", "type"=>"int", "required"=>0),
				array("name"=>"Office.City", "type"=>"text", "required"=>0),
				array("name"=>"Office.State", "type"=>"text", "required"=>0),
				array("name"=>"Office.Zip", "type"=>"text", "required"=>0),
				array("name"=>"Office.Name", "type"=>"text", "required"=>0),
				array("name"=>"Office.FranchiseName", "type"=>"text", "required"=>0),
				array("name"=>"OpenHouses.OpenHouse.Date", "type"=>"text", "required"=>0),
				array("name"=>"OpenHouses.OpenHouse.StartTime", "type"=>"text", "required"=>0),
				array("name"=>"OpenHouses.OpenHouse.EndTime", "type"=>"text", "required"=>0),
				array("name"=>"Schools.District", "type"=>"text", "required"=>0),
				array("name"=>"Schools.Elementary", "type"=>"text", "required"=>0),
				array("name"=>"Schools.Middle", "type"=>"text", "required"=>0),
				array("name"=>"Schools.High", "type"=>"text", "required"=>0),
				array("name"=>"Neighborhood.Name", "type"=>"text", "required"=>0),
				array("name"=>"Neighborhood.Description", "type"=>"text", "required"=>0),
				array("name"=>"RichDetails.AdditionalFeatures", "type"=>"text", "required"=>0),
				array("name"=>"RichDetails.Appliances", "type"=>"text", "required"=>0),
				array("name"=>"RichDetails.ArchitectureStyle", "type"=>"text", "required"=>0),
				array("name"=>"RichDetails.Attic", "type"=>"int", "required"=>0),
				array("name"=>"RichDetails.BarbecueArea", "type"=>"int", "required"=>0),
				array("name"=>"RichDetails.Basement", "type"=>"int", "required"=>0),
				array("name"=>"RichDetails.BuildingUnitCount", "type"=>"int", "required"=>0), 
				array("name"=>"RichDetails.CableReady", "type"=>"int", "required"=>0), 
				array("name"=>"RichDetails.CeilingFan", "type"=>"int", "required"=>0), 
				array("name"=>"RichDetails.CondoFloorNum", "type"=>"int", "required"=>0), 
				array("name"=>"RichDetails.CoolingSystem", "type"=>"text", "required"=>0), 
				array("name"=>"RichDetails.Deck", "type"=>"int", "required"=>0), 
				array("name"=>"RichDetails.DisabledAccess", "type"=>"int", "required"=>0), 
				array("name"=>"RichDetails.Dock", "type"=>"int", "required"=>0), 
				array("name"=>"RichDetails.Doorman", "type"=>"int", "required"=>0), 
				array("name"=>"RichDetails.DoublePaneWindows", "type"=>"int", "required"=>0), 
				array("name"=>"RichDetails.Elevator", "type"=>"int", "required"=>0), 
				array("name"=>"RichDetails.ExteriorTypes", "type"=>"text", "required"=>0), 
				array("name"=>"RichDetails.Fireplace", "type"=>"int", "required"=>0), 
				array("name"=>"RichDetails.FloorCoverings", "type"=>"text", "required"=>0), 
				array("name"=>"RichDetails.Garden", "type"=>"int", "required"=>0), 
				array("name"=>"RichDetails.GatedEntry", "type"=>"int", "required"=>0), 
				array("name"=>"RichDetails.Greenhouse", "type"=>"int", "required"=>0),
				array("name"=>"RichDetails.HeatingFuels", "type"=>"text", "required"=>0),
				array("name"=>"RichDetails.HeatingSystems", "type"=>"text", "required"=>0),
				array("name"=>"RichDetails.HottubSpa", "type"=>"int", "required"=>0),
				array("name"=>"RichDetails.Intercom", "type"=>"int", "required"=>0),
				array("name"=>"RichDetails.JettedBathTub", "type"=>"int", "required"=>0),
				array("name"=>"RichDetails.Lawn", "type"=>"int", "required"=>0),
				array("name"=>"RichDetails.LegalDescription", "type"=>"text", "required"=>0),
				array("name"=>"RichDetails.MotherInLaw", "type"=>"int", "required"=>0),
				array("name"=>"RichDetails.NumFloors", "type"=>"int", "required"=>0),
				array("name"=>"RichDetails.NumParkingSpaces", "type"=>"int", "required"=>0),
				array("name"=>"RichDetails.ParkingType", "type"=>"text", "required"=>0),
				array("name"=>"RichDetails.Patio", "type"=>"int", "required"=>0),
				array("name"=>"RichDetails.Pond", "type"=>"int", "required"=>0),
				array("name"=>"RichDetails.Pool", "type"=>"int", "required"=>0),
				array("name"=>"RichDetails.Porch", "type"=>"int", "required"=>0),
				array("name"=>"RichDetails.RoofTypes", "type"=>"text", "required"=>0),
				array("name"=>"RichDetails.RoomCount", "type"=>"int", "required"=>0),
				array("name"=>"RichDetails.Rooms", "type"=>"text", "required"=>0),
				array("name"=>"RichDetails.RvParking", "type"=>"int", "required"=>0),
				array("name"=>"RichDetails.Sauna", "type"=>"int", "required"=>0),
				array("name"=>"RichDetails.SecuritySystem", "type"=>"int", "required"=>0),
				array("name"=>"RichDetails.Skylight", "type"=>"int", "required"=>0),
				array("name"=>"RichDetails.SportsCourt", "type"=>"int", "required"=>0),
				array("name"=>"RichDetails.SprinklerSystem", "type"=>"int", "required"=>0),
				array("name"=>"RichDetails.VaultedCeiling", "type"=>"int", "required"=>0),				
				array("name"=>"RichDetails.ViewTypes", "type"=>"text", "required"=>0),
				array("name"=>"RichDetails.Waterfront", "type"=>"int", "required"=>0),
				array("name"=>"RichDetails.Wetbar", "type"=>"int", "required"=>0),
				array("name"=>"RichDetails.WhatOwnerLoves", "type"=>"text", "required"=>0),
				array("name"=>"RichDetails.Wired", "type"=>"int", "required"=>0),
				array("name"=>"RichDetails.YearUpdated", "type"=>"int", "required"=>0),
				array("name"=>"RichDetails.FitnessCenter", "type"=>"int", "required"=>0),
				array("name"=>"RichDetails.BasketballCourt", "type"=>"int", "required"=>0),
				array("name"=>"RichDetails.TennisCourt", "type"=>"int", "required"=>0),
				array("name"=>"RichDetails.NearTransportation", "type"=>"int", "required"=>0),
				array("name"=>"RichDetails.ControlledAccess", "type"=>"int", "required"=>0),
				array("name"=>"RichDetails.Over55ActiveCommunity", "type"=>"int", "required"=>0),
				array("name"=>"RichDetails.AssistedLivingCommunity", "type"=>"int", "required"=>0),
				array("name"=>"RichDetails.Storage", "type"=>"int", "required"=>0),
				array("name"=>"RichDetails.FencedYard", "type"=>"int", "required"=>0),
				array("name"=>"RichDetails.PropertyName", "type"=>"text", "required"=>0),
				array("name"=>"RichDetails.Furnished", "type"=>"int", "required"=>0),
				array("name"=>"RichDetails.HighspeedInternet", "type"=>"int", "required"=>0),
				array("name"=>"RichDetails.OnsiteLaundry", "type"=>"int", "required"=>0),
				array("name"=>"RichDetails.CableSatTV", "type"=>"int", "required"=>0),
				//array("name"=>"RichDetails.TaxYear", "type"=>"int", "required"=>0),
				array("name"=>"RichDetails.TaxAmount", "type"=>"int", "required"=>0),				
			),
			"editable" => 0,
			"status" => 1,
		),
		array(
			"gid" => "tff", 
			"link" => "http://developer.trulia.com/docs", 
			"settings" => array(),
			"elements" => array(			
				array("name"=>"location.building-name", "type"=>"text", "required"=>0),
				array("name"=>"location.city-name", "type"=>"text", "required"=>0),	
				array("name"=>"location.county", "type"=>"text", "required"=>0),
				array("name"=>"location.directions", "type"=>"text", "required"=>0),
				array("name"=>"location.display-address", "type"=>"text", "required"=>1),
				array("name"=>"location.elevation", "type"=>"text", "required"=>0),				
				array("name"=>"location.geocode-type", "type"=>"text", "required"=>0),
				array("name"=>"location.latitude", "type"=>"text", "required"=>0),
				array("name"=>"location.longitude", "type"=>"text", "required"=>0),
				array("name"=>"location.neighborhood-name", "type"=>"text", "required"=>0),
				array("name"=>"location.neighborhood-description", "type"=>"text", "required"=>0),
				array("name"=>"location.parcel-id", "type"=>"text", "required"=>0),
				array("name"=>"location.state-code", "type"=>"text", "required"=>0),
				array("name"=>"location.street-address", "type"=>"text", "required"=>0),
				array("name"=>"location.street-intersection", "type"=>"text", "required"=>0),				
				array("name"=>"location.subdivision", "type"=>"text", "required"=>0),
				array("name"=>"location.unit-number", "type"=>"int", "required"=>0),
				array("name"=>"location.zipcode", "type"=>"text", "required"=>0),
				
				array("name"=>"details.date-available", "type"=>"text", "required"=>0),
				array("name"=>"details.date-listed", "type"=>"text", "required"=>0),
				array("name"=>"details.date-sold", "type"=>"text", "required"=>0),
				array("name"=>"details.description", "type"=>"text", "required"=>0),
				array("name"=>"details.listing-title", "type"=>"text", "required"=>0),
				array("name"=>"details.living-area-square-feet", "type"=>"int", "required"=>0),
				array("name"=>"details.lot-size", "type"=>"text", "required"=>0),
				array("name"=>"details.mlsId", "type"=>"text", "required"=>0),
				array("name"=>"details.mlsName", "type"=>"text", "required"=>0),
				array("name"=>"details.num-bedrooms", "type"=>"int", "required"=>0),
				array("name"=>"details.num-full-bathrooms", "type"=>"int", "required"=>0),
				array("name"=>"details.num-half-bathrooms", "type"=>"int", "required"=>0),
				array("name"=>"details.num-bathrooms", "type"=>"int", "required"=>0),
				array("name"=>"details.price", "type"=>"int", "required"=>0),				
				array("name"=>"details.property-type", "type"=>"text", "required"=>1),
				array("name"=>"details.provider-listingid", "type"=>"int", "required"=>1),
				array("name"=>"details.sale-price", "type"=>"int", "required"=>0),
				array("name"=>"details.year-built", "type"=>"int", "required"=>0),
				
				array("name"=>"landing-page.lp-url", "type"=>"text", "required"=>1),
				array("name"=>"listing-type", "type"=>"text", "required"=>0),
				array("name"=>"status", "type"=>"text", "required"=>1),	
				array("name"=>"foreclosure-status", "type"=>"text", "required"=>0),
				array("name"=>"site.site-name", "type"=>"text", "required"=>1),
				
				array("name"=>"rental-terms.application-fee", "type"=>"text", "required"=>0),
				array("name"=>"rental-terms.application-fee-description", "type"=>"text", "required"=>0),
				array("name"=>"rental-terms.price-term", "type"=>"text", "required"=>0),
				array("name"=>"rental-terms.rental-type", "type"=>"text", "required"=>0),
				array("name"=>"rental-terms.lease-type", "type"=>"text", "required"=>0),
				array("name"=>"rental-terms.lease-max-length-months", "type"=>"int", "required"=>0),
				array("name"=>"rental-terms.lease-min-length-months", "type"=>"int", "required"=>0),
				array("name"=>"rental-terms.lease-details", "type"=>"text", "required"=>0),				
				array("name"=>"rental-terms.lease-periods", "type"=>"int", "required"=>0),
				array("name"=>"rental-terms.security-deposit", "type"=>"text", "required"=>0),
				array("name"=>"rental-terms.security-deposit-description", "type"=>"text", "required"=>0),
				array("name"=>"rental-terms.credit-cards", "type"=>"text", "required"=>0),
				array("name"=>"rental-terms.credit-cards-accepted", "type"=>"int", "required"=>0),
				array("name"=>"rental-terms.pets.large-dogs-allowed", "type"=>"int", "required"=>0),
				array("name"=>"rental-terms.pets.small-dogs-allowed", "type"=>"int", "required"=>0),	
				array("name"=>"rental-terms.pets.cats-allowed", "type"=>"int", "required"=>0),
				array("name"=>"rental-terms.pets.pet-other-allowed", "type"=>"int", "required"=>0),
				array("name"=>"rental-terms.pets.max-pets", "type"=>"int", "required"=>0),
				array("name"=>"rental-terms.pets.pet-deposit", "type"=>"text", "required"=>0),
				array("name"=>"rental-terms.pets.pet-comments", "type"=>"text", "required"=>0),				
				array("name"=>"rental-terms.pets.pet-fee", "type"=>"int", "required"=>0),				
				array("name"=>"rental-terms.pets.pet-rent", "type"=>"int", "required"=>0),
				array("name"=>"rental-terms.pets.pet-weight", "type"=>"int", "required"=>0),
				array("name"=>"rental-terms.utilities-included.landlord-pays-aircon", "type"=>"int", "required"=>0),
				array("name"=>"rental-terms.utilities-included.landlord-pays-broadbandinternet", "type"=>"int", "required"=>0),
				array("name"=>"rental-terms.utilities-included.landlord-pays-cable", "type"=>"int", "required"=>0),
				array("name"=>"rental-terms.utilities-included.landlord-pays-electric", "type"=>"int", "required"=>0),
				array("name"=>"rental-terms.utilities-included.landlord-pays-gas", "type"=>"int", "required"=>0),
				array("name"=>"rental-terms.utilities-included.landlord-pays-heat", "type"=>"int", "required"=>0),
				array("name"=>"rental-terms.utilities-included.landlord-pays-hotwater", "type"=>"int", "required"=>0),
				array("name"=>"rental-terms.utilities-included.landlord-pays-satellite", "type"=>"int", "required"=>0),
				array("name"=>"rental-terms.utilities-included.landlord-pays-sewer", "type"=>"int", "required"=>0),
				array("name"=>"rental-terms.utilities-included.landlord-pays-telephone", "type"=>"int", "required"=>0),
				array("name"=>"rental-terms.utilities-included.landlord-pays-trash", "type"=>"int", "required"=>0),
				array("name"=>"rental-terms.utilities-included.landlord-pays-water", "type"=>"int", "required"=>0),
				array("name"=>"rental-terms.utilities-included.landlord-utilities-portion-included", "type"=>"text", "required"=>0),
				array("name"=>"rental-terms.utilities-included.utilities-comment", "type"=>"text", "required"=>0),
				array("name"=>"rental-terms.property-manager-on-site", "type"=>"int", "required"=>0),
				array("name"=>"rental-terms.rent-control", "type"=>"int", "required"=>0),
				array("name"=>"rental-terms.subletting-allowed", "type"=>"int", "required"=>0),
			
				array("name"=>"pictures", "type"=>"text", "required"=>0),
				//array("name"=>"pictures-caption", "type"=>"text", "required"=>0),
				//array("name"=>"pictures-description", "type"=>"text", "required"=>0),
				//array("name"=>"pictures-seq-number", "type"=>"int", "required"=>0),
				//array("name"=>"pictures-url", "type"=>"text", "required"=>0),
			
				array("name"=>"virtual-tours", "type"=>"text", "required"=>0),
				//array("name"=>"virtual-tours-caption", "type"=>"text", "required"=>0),
				//array("name"=>"virtual-tours-description", "type"=>"text", "required"=>0),
				//array("name"=>"virtual-tours-seq-number", "type"=>"int", "required"=>0),
				//array("name"=>"virtual-tours-url", "type"=>"text", "required"=>0),
			
				array("name"=>"videos", "type"=>"text", "required"=>0),
				//array("name"=>"videos-caption", "type"=>"text", "required"=>0),
				//array("name"=>"videos-description", "type"=>"text", "required"=>0),
				//array("name"=>"videos-seq-number", "type"=>"text", "required"=>0),
				//array("name"=>"videos-url", "type"=>"text", "required"=>0),
				
				array("name"=>"floorplan-layouts", "type"=>"text", "required"=>0),
				//array("name"=>"floorplan-layouts-caption", "type"=>"text", "required"=>0),
				//array("name"=>"floorplan-layouts-description", "type"=>"text", "required"=>0),
				//array("name"=>"floorplan-layouts-seq-number", "type"=>"text", "required"=>0),
				//array("name"=>"floorplan-layouts-url", "type"=>"text", "required"=>0),
				
				array("name"=>"agent.agent-alernate-email", "type"=>"text", "required"=>0),
				array("name"=>"agent.agent-email", "type"=>"text", "required"=>0),
				array("name"=>"agent.agent-id", "type"=>"text", "required"=>0),
				array("name"=>"agent.agent-name", "type"=>"text", "required"=>0),
				array("name"=>"agent.agent-phone", "type"=>"text", "required"=>0),
				array("name"=>"agent.agent-picture-url", "type"=>"text", "required"=>0),
				
				array("name"=>"brokerage.brokerage-broker-name", "type"=>"text", "required"=>0),				
				array("name"=>"brokerage.brokerage-email", "type"=>"text", "required"=>0),
				array("name"=>"brokerage.brokerage-id", "type"=>"text", "required"=>0),
				array("name"=>"brokerage.brokerage-logo-url", "type"=>"text", "required"=>0),
				array("name"=>"brokerage.brokerage-mls-code", "type"=>"text", "required"=>0),
				array("name"=>"brokerage.brokerage-name", "type"=>"text", "required"=>0),
				array("name"=>"brokerage.brokerage-phone", "type"=>"text", "required"=>0),
				array("name"=>"brokerage.brokerage-website", "type"=>"text", "required"=>0),
				array("name"=>"brokerage.brokerage-address.brokerage-city-name", "type"=>"text", "required"=>0),
				array("name"=>"brokerage.brokerage-address.brokerage-state-code", "type"=>"text", "required"=>0),
				array("name"=>"brokerage.brokerage-address.brokerage-street-address", "type"=>"text", "required"=>0),				
				array("name"=>"brokerage.brokerage-address.brokerage-zipcode", "type"=>"text", "required"=>0),
			
				array("name"=>"office.office-broker-id", "type"=>"text", "required"=>0),				
				array("name"=>"office.office-email", "type"=>"text", "required"=>0),
				array("name"=>"office.office-id", "type"=>"int", "required"=>0),
				array("name"=>"office.office-mls-code", "type"=>"text", "required"=>0),
				array("name"=>"office.office-name", "type"=>"text", "required"=>0),
				array("name"=>"office.office-phone", "type"=>"text", "required"=>0),
				array("name"=>"office.office-websit", "type"=>"text", "required"=>0),
				
				array("name"=>"franchise.franchise-email", "type"=>"text", "required"=>0),
				array("name"=>"franchise.franchise-logo-url", "type"=>"text", "required"=>0),
				array("name"=>"franchise.franchise-name", "type"=>"text", "required"=>0),
				array("name"=>"franchise.franchise-phone", "type"=>"text", "required"=>0),
				array("name"=>"franchise.franchise-website", "type"=>"text", "required"=>0),
								
				array("name"=>"builder.builder-email", "type"=>"text", "required"=>0),
				array("name"=>"builder.builder-id", "type"=>"int", "required"=>0),
				array("name"=>"builder.builder-lead-email", "type"=>"text", "required"=>0),
				array("name"=>"builder.builder-logo-url", "type"=>"text", "required"=>0),
				array("name"=>"builder.builder-name", "type"=>"text", "required"=>0),
				array("name"=>"builder.builder-phone", "type"=>"text", "required"=>0),
				array("name"=>"builder.builder-state-code", "type"=>"text", "required"=>0),				
				array("name"=>"builder.builder-website", "type"=>"text", "required"=>0),
				array("name"=>"builder.builder-address.builder-city-name", "type"=>"text", "required"=>0),
				array("name"=>"builder.builder-address.builder-street-address", "type"=>"text", "required"=>0),
				array("name"=>"builder.builder-address.builder-zipcode", "type"=>"text", "required"=>0),
				
				array("name"=>"property-manager.property-management-company-name", "type"=>"text", "required"=>0),
				array("name"=>"property-manager.property-manager-email", "type"=>"text", "required"=>0),
				array("name"=>"property-manager.property-manager-lead-email", "type"=>"text", "required"=>0),
				array("name"=>"property-manager.property-manager-logo-url", "type"=>"text", "required"=>0),
				array("name"=>"property-manager.property-manager-name", "type"=>"text", "required"=>0),
				array("name"=>"property-manager.property-manager-phone", "type"=>"text", "required"=>0),
				array("name"=>"property-manager.property-manager-website", "type"=>"text", "required"=>0),
				array("name"=>"property-manager.property-manager-office-hours.day-of-the-week", "type"=>"text", "required"=>0),
				array("name"=>"property-manager.property-manager-office-hours.office-start-time", "type"=>"text", "required"=>0),
				array("name"=>"property-manager.property-manager-office-hours.office-end-time", "type"=>"text", "required"=>0),
				array("name"=>"property-manager.property-manager-office-hours.comment", "type"=>"text", "required"=>0),
				
				array("name"=>"community.building-amenities", "type"=>"text", "required"=>0),
				array("name"=>"community.community-amenities", "type"=>"text", "required"=>0),
				array("name"=>"community.community-building", "type"=>"text", "required"=>0),				
				array("name"=>"community.community-description", "type"=>"text", "required"=>0),
				array("name"=>"community.community-email", "type"=>"text", "required"=>0),
				array("name"=>"community.community-fax", "type"=>"text", "required"=>0),
				array("name"=>"community.community-id", "type"=>"text", "required"=>0),
				array("name"=>"community.community-name", "type"=>"text", "required"=>0),
				array("name"=>"community.community-phone", "type"=>"text", "required"=>0),								
				array("name"=>"community.community-website", "type"=>"text", "required"=>0),
				array("name"=>"community.community-address.community-city-name", "type"=>"text", "required"=>0),
				array("name"=>"community.community-address.community-zipcode", "type"=>"text", "required"=>0),
				array("name"=>"community.community-address.community-state-code", "type"=>"text", "required"=>0),
				array("name"=>"community.community-address.community-street-address", "type"=>"text", "required"=>0),	
				
				array("name"=>"plan.plan-base-price", "type"=>"int", "required"=>0),
				array("name"=>"plan.plan-base-price", "type"=>"text", "required"=>0),
				array("name"=>"plan.plan-id", "type"=>"text", "required"=>0),
				array("name"=>"plan.plan-name", "type"=>"text", "required"=>0),
				array("name"=>"plan.plan-type", "type"=>"text", "required"=>0),				
				
				array("name"=>"spec.is-spec-home", "type"=>"int", "required"=>0),
				array("name"=>"spec.spec-id", "type"=>"int", "required"=>0),
				
				array("name"=>"open-home.period1-date", "type"=>"text", "required"=>0),
				array("name"=>"open-home.period1-details", "type"=>"text", "required"=>0),
				array("name"=>"open-home.period1-end-tim", "type"=>"text", "required"=>0),
				array("name"=>"open-home.period1-start-time", "type"=>"text", "required"=>0),
				array("name"=>"open-home.period2-date", "type"=>"text", "required"=>0),
				array("name"=>"open-home.period2-details", "type"=>"text", "required"=>0),
				array("name"=>"open-home.period2-end-tim", "type"=>"text", "required"=>0),
				array("name"=>"open-home.period2-start-time", "type"=>"text", "required"=>0),
				array("name"=>"open-home.period3-date", "type"=>"text", "required"=>0),
				array("name"=>"open-home.period3-details", "type"=>"text", "required"=>0),
				array("name"=>"open-home.period3-end-tim", "type"=>"text", "required"=>0),
				array("name"=>"open-home.period3-start-time", "type"=>"text", "required"=>0),
				array("name"=>"open-home.period4-date", "type"=>"text", "required"=>0),
				array("name"=>"open-home.period4-details", "type"=>"text", "required"=>0),
				array("name"=>"open-home.period4-end-tim", "type"=>"text", "required"=>0),
				array("name"=>"open-home.period4-start-time", "type"=>"text", "required"=>0),
				
				array("name"=>"taxes", "type"=>"text", "required"=>0),
				//array("name"=>"taxes.tax-amount", "type"=>"int", "required"=>0),
				//array("name"=>"taxes.tax-description", "type"=>"text", "required"=>0),
				//array("name"=>"taxes.tax-type", "type"=>"text", "required"=>0),
				//array("name"=>"taxes.tax-year", "type"=>"text", "required"=>0),
				
				array("name"=>"hoa-fees.hoa-fee", "type"=>"text", "required"=>0),
				array("name"=>"hoa-fees.hoa-period", "type"=>"text", "required"=>0),
				array("name"=>"hoa-fees.hoa-description", "type"=>"text", "required"=>0),
				
				array("name"=>"additional-fees", "type"=>"text", "required"=>0),
				//array("name"=>"additional-fees.fee.fee-amount", "type"=>"text", "required"=>0),
				//array("name"=>"additional-fees.fee.fee-description", "type"=>"text", "required"=>0),
				//array("name"=>"additional-fees.fee.fee-period", "type"=>"text", "required"=>0),
				//array("name"=>"additional-fees.fee.fee-type", "type"=>"text", "required"=>0),
				
				array("name"=>"schools.school-district.elementary", "type"=>"text", "required"=>0),
				array("name"=>"schools.school-district.middle", "type"=>"text", "required"=>0),
				array("name"=>"schools.school-district.juniorhigh", "type"=>"text", "required"=>0),
				array("name"=>"schools.school-district.high", "type"=>"text", "required"=>0),
				array("name"=>"schools.school-district.district-name", "type"=>"text", "required"=>0),
				array("name"=>"schools.school-district.district-website", "type"=>"text", "required"=>0),
				array("name"=>"schools.school-district.phone-number", "type"=>"text", "required"=>0),
				
				array("name"=>"detailed-characteristics.appliances.additional-appliance.additional-appliance-name", "type"=>"text", "required"=>0),
				array("name"=>"detailed-characteristics.appliances.additional-appliance.additional-appliance-description", "type"=>"text", "required"=>0),
				array("name"=>"detailed-characteristics.appliances.appliances-comments", "type"=>"text", "required"=>0),
				array("name"=>"detailed-characteristics.appliances.has-dishwasher", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.appliances.has-disposal", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.appliances.has-dryer", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.appliances.has-microwave", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.appliances.has-refrigerator", "type"=>"int", "required"=>0),				
				array("name"=>"detailed-characteristics.appliances.has-washer", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.appliances.range-type", "type"=>"text", "required"=>0),
				array("name"=>"detailed-characteristics.architecture-style", "type"=>"text", "required"=>0),
				array("name"=>"detailed-characteristics.assigned-parking-space-cost", "type"=>"text", "required"=>0),
				array("name"=>"detailed-characteristics.building-has-concierge", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.building-has-doorman", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.building-has-elevator", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.building-has-fitness-center", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.building-has-on-site-maintenance", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.cooling-systems.has-air-conditioning", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.cooling-systems.has-ceiling-fan", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.cooling-systems.other-cooling", "type"=>"text", "required"=>0),
				array("name"=>"detailed-characteristics.exterior-type", "type"=>"text", "required"=>0),
				array("name"=>"detailed-characteristics.floor-coverings", "type"=>"text", "required"=>0),
				array("name"=>"detailed-characteristics.foundation-type", "type"=>"text", "required"=>0),
				array("name"=>"detailed-characteristics.furnished", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.garage-type", "type"=>"text", "required"=>0),
				array("name"=>"detailed-characteristics.has-assigned-parking-space", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.has-attic", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.has-balcony", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.has-barbeque-area", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.has-basement", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.has-cable-satellite", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.has-courtyard", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.has-deck", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.has-disabled-access", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.has-dock", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.has-doublepane-windows", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.has-garage", "type"=>"int", "required"=>0),				
				array("name"=>"detailed-characteristics.has-garden", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.has-gated-entry", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.has-greenhouse", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.has-handrails", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.has-hot-tub-spa", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.has-intercom", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.has-jetted-bath-tub", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.has-lawn", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.has-mother-in-law", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.has-patio", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.has-pond", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.has-pool", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.has-porch", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.has-private-balcony", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.has-private-patio", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.has-rv-parking", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.has-sauna", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.has-security-system", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.has-skylight", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.has-sportscourt", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.has-sprinkler-system", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.has-terrace", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.has-vaulted-ceiling", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.has-view", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.has-washer-dryer-hookup", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.has-wet-bar", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.has-window-coverings", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.heating-systems.fireplace-type", "type"=>"text", "required"=>0),
				array("name"=>"detailed-characteristics.heating-systems.has-fireplace", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.heating-systems.heating-fuel", "type"=>"text", "required"=>0),
				array("name"=>"detailed-characteristics.heating-systems.heating-system", "type"=>"text", "required"=>0),
				array("name"=>"detailed-characteristics.is-waterfront", "type"=>"int", "required"=>0),				
				array("name"=>"detailed-characteristics.num-floors-in-unit", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.other-amenities", "type"=>"text", "required"=>0),
				array("name"=>"detailed-characteristics.parking-comment", "type"=>"text", "required"=>0),
				array("name"=>"detailed-characteristics.parking-space-fee", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.parking-types", "type"=>"text", "required"=>0),	
				array("name"=>"detailed-characteristics.roof-type", "type"=>"text", "required"=>0),
				array("name"=>"detailed-characteristics.room-count", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.rooms", "type"=>"text", "required"=>0),
				//array("name"=>"detailed-characteristics.rooms.room-type", "type"=>"text", "required"=>0),
				//array("name"=>"detailed-characteristics.rooms.room-size", "type"=>"int", "required"=>0),
				//array("name"=>"detailed-characteristics.rooms.room-description", "type"=>"text", "required"=>0),
				array("name"=>"detailed-characteristics.total-floors-in-building", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.total-unit-parking-spaces", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.total-units-in-building", "type"=>"int", "required"=>0),
				array("name"=>"detailed-characteristics.view-type", "type"=>"text", "required"=>0),
				array("name"=>"detailed-characteristics.year-updated", "type"=>"int", "required"=>0),				
				
				array("name"=>"advertise-with-us.channel", "type"=>"text", "required"=>0),
				array("name"=>"advertise-with-us.featured", "type"=>"int", "required"=>0),
				array("name"=>"advertise-with-us.branded", "type"=>"int", "required"=>0),
				array("name"=>"advertise-with-us.branded-logo-url", "type"=>"text", "required"=>0), 
			),
			"editable" => 0,
			"status" => 1,
		),
		array(
			"gid" => "oodle", 
			"link" => "http://www.oodle.com/info/feed", 
			"settings" => array(),
			"elements" => array(
				array("name"=>"category", "type"=>"text", "required"=>1),
				array("name"=>"description", "type"=>"text", "required"=>1),
				array("name"=>"id", "type"=>"int", "required"=>1),
				array("name"=>"title", "type"=>"text", "required"=>1),
				array("name"=>"url", "type"=>"text", "required"=>1),
				array("name"=>"address", "type"=>"text", "required"=>0),
				array("name"=>"city", "type"=>"text", "required"=>0),
				array("name"=>"country", "type"=>"text", "required"=>0),
				array("name"=>"latitude", "type"=>"text", "required"=>0),
				array("name"=>"longitude", "type"=>"text", "required"=>0),
				array("name"=>"neighborhood", "type"=>"text", "required"=>0),
				array("name"=>"state", "type"=>"text", "required"=>0),
				array("name"=>"zip_code", "type"=>"text", "required"=>0),
				array("name"=>"agent", "type"=>"text", "required"=>0),
				array("name"=>"agent_email", "type"=>"text", "required"=>0),
				array("name"=>"agent_phone", "type"=>"text", "required"=>0),
				array("name"=>"agent_url", "type"=>"text", "required"=>0),
				array("name"=>"amenities", "type"=>"text", "required"=>0),
				array("name"=>"balconies", "type"=>"int", "required"=>0),
				array("name"=>"bathrooms", "type"=>"int", "required"=>0),
				array("name"=>"bedrooms", "type"=>"int", "required"=>0),
				array("name"=>"broker", "type"=>"text", "required"=>0),
				array("name"=>"broker_email", "type"=>"text", "required"=>0),
				array("name"=>"broker_phone", "type"=>"text", "required"=>0),
				array("name"=>"broker_url", "type"=>"text", "required"=>0),
				array("name"=>"condition", "type"=>"text", "required"=>0),
				array("name"=>"create_time", "type"=>"text", "required"=>0),
				array("name"=>"currency", "type"=>"text", "required"=>0),
				array("name"=>"event_date", "type"=>"text", "required"=>0),
				array("name"=>"expire_time", "type"=>"text", "required"=>0),
				array("name"=>"facing", "type"=>"text", "required"=>0),
				array("name"=>"featured", "type"=>"int", "required"=>0),
				array("name"=>"fee", "type"=>"int", "required"=>0),
				array("name"=>"furnished", "type"=>"text", "required"=>0),
				array("name"=>"image_url", "type"=>"text", "required"=>0),
				array("name"=>"ip_address", "type"=>"text", "required"=>0),
				array("name"=>"lead_email", "type"=>"text", "required"=>0),
				array("name"=>"lot_size", "type"=>"int", "required"=>0),
				array("name"=>"lot_size_units", "type"=>"text", "required"=>0),
				array("name"=>"mls_id", "type"=>"text", "required"=>0),
				array("name"=>"price", "type"=>"int", "required"=>0),
				array("name"=>"registration", "type"=>"int", "required"=>0),
				array("name"=>"secondary_source", "type"=>"text", "required"=>0),
				array("name"=>"seller_email", "type"=>"text", "required"=>0),
				array("name"=>"seller_name", "type"=>"text", "required"=>0),
				array("name"=>"seller_phone", "type"=>"text", "required"=>0),
				array("name"=>"seller_type", "type"=>"text", "required"=>0),
				array("name"=>"seller_url", "type"=>"text", "required"=>0),
				array("name"=>"square_feet", "type"=>"int", "required"=>0),
				array("name"=>"vastu_compliant", "type"=>"int", "required"=>0),
				array("name"=>"year", "type"=>"int", "required"=>0),
			),
			"editable" => 0,
			"status" => 1,
		),
		array(
			"gid" => "vast", 
			"link" => "http://www.vast.com/info/submitcontent", 
			"settings" => array(),
			"elements" => array(
				array("name"=>"record_id", "type"=>"int", "required"=>0),
				array("name"=>"title", "type"=>"text", "required"=>0),
				array("name"=>"url", "type"=>"text", "required"=>0),
				array("name"=>"category", "type"=>"text", "required"=>0),
				array("name"=>"subcategory", "type"=>"text", "required"=>0),
				array("name"=>"image", "type"=>"text", "required"=>0),
				array("name"=>"address", "type"=>"text", "required"=>0),
				array("name"=>"city", "type"=>"text", "required"=>0),
				array("name"=>"state", "type"=>"text", "required"=>0),
				array("name"=>"zip", "type"=>"text", "required"=>0),
				array("name"=>"country", "type"=>"text", "required"=>0),
				array("name"=>"bedrooms", "type"=>"text", "required"=>0),
				array("name"=>"bathrooms", "type"=>"text", "required"=>0),
				array("name"=>"square_footage", "type"=>"text", "required"=>0),
				array("name"=>"stories", "type"=>"text", "required"=>0),
				array("name"=>"lot_size", "type"=>"text", "required"=>0),
				array("name"=>"parking_spots", "type"=>"text", "required"=>0),
				array("name"=>"year_built", "type"=>"text", "required"=>0),
				array("name"=>"currency", "type"=>"text", "required"=>0),
				array("name"=>"price", "type"=>"text", "required"=>0),
				array("name"=>"amenities", "type"=>"text", "required"=>0),
				array("name"=>"description", "type"=>"text", "required"=>0),
				array("name"=>"listing_time", "type"=>"text", "required"=>0),
				array("name"=>"expire_time", "type"=>"text", "required"=>0),
			),
			"editable" => 0,
			"status" => 1,
		),
		array(
			"gid" => "synda_listings", 
			"link" => "http://www.syndafeed.com", 
			"settings" => array(),
			"elements" => array(
				array("name"=>"PublisherId", "type"=>"int", "required"=>1),
				array("name"=>"ListingId", "type"=>"int", "required"=>1),
				array("name"=>"PropertyId", "type"=>"int", "required"=>0),
				array("name"=>"GMTLastModified", "type"=>"text", "required"=>1),
				array("name"=>"ListingStatus", "type"=>"int", "required"=>1),
				array("name"=>"PropertyType", "type"=>"text", "required"=>1),
				array("name"=>"AgentId", "type"=>"int", "required"=>1),
				array("name"=>"DistressSale", "type"=>"int", "required"=>0),
				array("name"=>"RentalPeriod", "type"=>"text", "required"=>1),
				array("name"=>"CapRate", "type"=>"text", "required"=>0),
				array("name"=>"Currency", "type"=>"text", "required"=>1),
				array("name"=>"Price", "type"=>"int", "required"=>1),
				array("name"=>"PropertyTax", "type"=>"int", "required"=>0),
				array("name"=>"HomeownerDues", "type"=>"int", "required"=>0),
				array("name"=>"StreetAddress", "type"=>"text", "required"=>0),
				array("name"=>"StreetAddress.hide", "type"=>"int", "required"=>0),
				array("name"=>"District", "type"=>"text", "required"=>0),
				array("name"=>"City", "type"=>"text", "required"=>1),
				array("name"=>"StateProvince", "type"=>"text", "required"=>1),
				array("name"=>"Country", "type"=>"text", "required"=>1),
				array("name"=>"PostalCode", "type"=>"text", "required"=>0),
				array("name"=>"StructureArea", "type"=>"text", "required"=>0),
				array("name"=>"LotLengthWidth", "type"=>"text", "required"=>0),
				array("name"=>"IrregularLot", "type"=>"text", "required"=>0),
				array("name"=>"LandArea", "type"=>"text", "required"=>0),
				array("name"=>"BedRooms", "type"=>"text", "required"=>0),
				array("name"=>"BathRooms", "type"=>"text", "required"=>0),
				array("name"=>"ReceptionRooms", "type"=>"int", "required"=>0),
				array("name"=>"TotalRooms", "type"=>"text", "required"=>0),
				array("name"=>"YearBuilt", "type"=>"text", "required"=>0),
				array("name"=>"AVMDeny", "type"=>"int", "required"=>0),
				array("name"=>"Latitude", "type"=>"text", "required"=>0),
				array("name"=>"Longitude", "type"=>"text", "required"=>0),
				array("name"=>"PhotoURL", "type"=>"text", "required"=>0),
				array("name"=>"VirtualTourURL", "type"=>"text", "required"=>0),
				array("name"=>"Previews", "type"=>"text", "required"=>0),
				//array("name"=>"Previews.Preview.Begins", "type"=>"text", "required"=>0),
				//array("name"=>"Previews.Preview.Ends", "type"=>"text", "required"=>0),
				//array("name"=>"Previews.Preview.Notes", "type"=>"text", "required"=>0),
				array("name"=>"GMTOffset", "type"=>"text", "required"=>0),
				//array("name"=>"DescriptionLang", "type"=>"text", "required"=>0),
				//array("name"=>"DescriptionLang.value", "type"=>"text", "required"=>1),
				array("name"=>"DescriptionLang.Caption", "type"=>"text", "required"=>1),
				array("name"=>"DescriptionLang.Description", "type"=>"text", "required"=>1),
				array("name"=>"DescriptionLang.DetailsURL", "type"=>"text", "required"=>1),
			),
			"editable" => 0,
			"status" => 1,
		),
		array(
			"gid" => "synda_agents", 
			"link" => "http://www.syndafeed.com", 
			"settings" => array(),
			"elements" => array(
				array("name"=>"PublisherId", "type"=>"text", "required"=>1),
				array("name"=>"AgentId", "type"=>"text", "required"=>1),
				array("name"=>"PersonalTitle", "type"=>"text", "required"=>0),
				array("name"=>"FirstName", "type"=>"text", "required"=>1),
				array("name"=>"LastName", "type"=>"text", "required"=>1),
				array("name"=>"LicenseType.value", "type"=>"text", "required"=>1),
				array("name"=>"LicenseType.Jurisdiction", "type"=>"text", "required"=>1),
				array("name"=>"LicenseType.Jurisdiction", "type"=>"text", "required"=>1),
				array("name"=>"LicenseType.LicenseNumber", "type"=>"text", "required"=>1),
				array("name"=>"Email", "type"=>"text", "required"=>1),
				array("name"=>"Password", "type"=>"text", "required"=>1),
				array("name"=>"Password.type", "type"=>"text", "required"=>1),
				array("name"=>"Telephone", "type"=>"text", "required"=>0),
				array("name"=>"Fax", "type"=>"text", "required"=>0),
				array("name"=>"PhotoURL", "type"=>"text", "required"=>0),
				array("name"=>"BrokerName", "type"=>"text", "required"=>0),
				array("name"=>"BrokerPhone", "type"=>"text", "required"=>0),
				array("name"=>"BrokerURL", "type"=>"text", "required"=>0),
				array("name"=>"BrokerMlsName", "type"=>"text", "required"=>0),
				array("name"=>"BrokerMlsCode", "type"=>"text", "required"=>0),
			),
			"editable" => 0,
			"status" => 1,
		),
	);
	
	/**
	 * Seo configuration
	 * @var array
	 */
	private $seo = array(
		"module_gid" => "export", 
		"model_name" => "Export_model", 
		"get_settings_method" => "get_seo_settings",
		"get_rewrite_vars_method" => "request_seo_rewrite",
		"get_sitemap_urls_method" => "get_sitemap_xml_urls",
	);
	
	/**
	 * Constructor
	 *
	 * @return Install object
	 */
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
	}

	/**
	 * Install links to menu module
	 */
	public function install_menu(){
		
		$this->CI->load->helper("menu");

		foreach($this->menu as $gid => $menu_data){
			$this->menu[$gid]["id"] = linked_install_set_menu($gid, $menu_data["action"], $menu_data["name"]);
			linked_install_process_menu_items($this->menu, "create", $gid, 0, $this->menu[$gid]["items"]);
		}
	}
	
	/**
	 * Update languages
	 * @param array $langs_ids
	 */
	public function install_menu_lang_update($langs_ids=null){
		if(empty($langs_ids)) return false;
		$langs_file = $this->CI->Install_model->language_file_read("export", "menu", $langs_ids);

		if(!$langs_file){log_message("info", "Empty menu langs data"); return false;}

		$this->CI->load->helper("menu");

		foreach($this->menu as $gid => $menu_data){
			linked_install_process_menu_items($this->menu, "update", $gid, 0, $this->menu[$gid]["items"], $gid, $langs_file);
		}
		return true;
	}

	/**
	 * Export languages
	 * @param array $langs_ids
	 */
	public function install_menu_lang_export($langs_ids){
		if(empty($langs_ids)) return false;
		$this->CI->load->helper("menu");

		$return = array();
		foreach($this->menu as $gid => $menu_data){
			$temp = linked_install_process_menu_items($this->menu, "export", $gid, 0, $this->menu[$gid]["items"], $gid, $langs_ids);
			$return = array_merge($return, $temp);
		}
		return array("menu" => $return);
	}
	
	/**
	 * Uninstall menu
	 */
	public function deinstall_menu(){
		
		$this->CI->load->helper("menu");
		foreach($this->menu as $gid => $menu_data){
			if($menu_data["action"] == "create"){
				linked_install_set_menu($gid, "delete");
			}else{
				linked_install_delete_menu_items($gid, $this->menu[$gid]["items"]);
			}
		}
	}
	
	/**
	 * Install links to ausers module
	 */
	public function install_ausers(){
		// install ausers permissions
		$this->CI->load->model("Ausers_model");
		
		foreach((array)$this->ausers as $method_data){
			//$validate_data = $this->CI->Ausers_model->validate_method($method_data, true);
			$validate_data = array("errors" => array(), "data" => $method_data);
			if(!empty($validate_data["errors"])) continue;
			$this->CI->Ausers_model->save_method(null, $validate_data["data"]);
		}
	}
	
	/**
	 * Import moderators languages
	 * @param array $langs_ids
	 */
	public function install_ausers_lang_update($langs_ids=null){
		$langs_file = $this->CI->Install_model->language_file_read("export", "ausers", $langs_ids);
		if(!$langs_file){log_message("info", "Empty ausers langs data");return false;}

		// install ausers permissions
		$this->CI->load->model("Ausers_model");
		$params["where"]["module"] = "export";
		$methods = $this->CI->Ausers_model->get_methods_lang_export($params);

		foreach($methods as $method){
			if(!empty($langs_file[$method["method"]])){
				$this->CI->Ausers_model->save_method($method["id"], array(), $langs_file[$method["method"]]);
			}
		}
	}
	
	/**
	 * Export moderators languages
	 * @param array $langs_ids
	 */
	public function install_ausers_lang_export($langs_ids){
		$this->CI->load->model("Ausers_model");
		$params["where"]["module"] = "export";
		$methods = $this->CI->Ausers_model->get_methods_lang_export($params, $langs_ids);
		foreach($methods as $method){
			$return[$method["method"]] = $method["langs"];
		}
		return array("ausers" => $return);
	}
	
	/**
	 * Uninstall links to ausers module
	 */
	public function deinstall_ausers(){
		$this->CI->load->model("Ausers_model");
		$params = array();
		$params["where"]["module"] = "export";
		$this->CI->Ausers_model->delete_methods($params);
	}	

	/**
	 * Install links to cronjob module
	 */
	public function install_cronjob(){
		$this->CI->load->model("Cronjob_model");
		$cron_data = array(
			"name" => "Export scheduler",
			"module" => "export",
			"model" => "Export_model",
			"method" => "cron_export",
			"cron_tab" => "0 0 1 * *",
			"status" => "1"
		);
		$this->CI->Cronjob_model->save_cron(null, $cron_data);
	}
	
	/**
	 * Uninstall links to cronjob module
	 */
	public function deinstall_cronjob(){
		$this->CI->load->model("Cronjob_model");
		$cron_data = array();
		$cron_data["where"]["module"] = "export";
		$this->CI->Cronjob_model->delete_cron_by_param($cron_data);
	}

	
	/**
	 * Install module actions
	 */
	public function _arbitrary_installing(){
		///// drivers
		$this->CI->load->model("export/models/Export_driver_model");		
		foreach((array)$this->drivers as $driver_data){
			$validate_data = $this->CI->Export_driver_model->validate_driver(null, $driver_data);
			if(!empty($validate_data["errors"])) continue;
			$this->CI->Export_driver_model->save_driver(null, $validate_data["data"]);
		}		
		///// seo
		$this->CI->pg_seo->set_seo_module("export", (array)$this->seo);
	}
	
	/**
	 * Import module languages
	 */
	public function _arbitrary_lang_install(){
		$langs_file = $this->CI->Install_model->language_file_read("export", "arbitrary");
		if(!$langs_file){log_message("info", "Empty arbitrary langs data");return false;}
	}

	/**
	 * Export module languages
	 * @param array $langs_ids
	 */
	public function _arbitrary_lang_export($langs_ids=null){
		return array();
	}

	/**
	 * Unistall module actions
	 */
	public function _arbitrary_deinstalling(){
		///// drivers
		$this->CI->load->model("export/models/Export_driver_model");
		
		foreach((array)$this->drivers as $driver_data){
			$this->CI->Export_driver_model->remove_driver($driver_data["gid"]);
		}
		
		///// seo
		$this->CI->pg_seo->delete_seo_module("export");
	}
}
