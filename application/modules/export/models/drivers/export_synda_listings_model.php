<?php 
/**
 * SyndaFeed listings export driver model
 * 
 * @package PG_RealEstate
 * @subpackage application
 * @category	modules
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/

if(!defined("BASEPATH")) exit("No direct script access allowed");

class Export_synda_listings_model extends Model{
	
	/**
	 * Link to CodeIgniter object
	 * @var object
	 */
	private $CI;
	
	/**
	 * Not available symbols
	 */
	protected $search = array('&', "'", '"', ">", "<");
	
	/**
	 * Symbols for replace
	 */
	protected $replace = array('&amp;', '&apos;', '&quot;', "&gt;", "&lt;");
	
	/**
	 * Property types
	 * @var array
	 */
	protected $property_types = array('sfam', 'apt', 'land', 'duplex', 'multi', 'town', 'condo', 'comm', 'manu');
	
	/**
	 * Listing status codes
	 * @var array
	 */
	protected $listing_status_codes = array('a', 's');
	
	/**
	 * Rental period codes
	 * @var array
	 */
	protected $rental_period_codes = array('a', 's', 'w', 'm');
	
	/**
	 * Distress sale codes
	 * @var array
	 */
	protected $distress_sale_codes = array('v', 's', 'f', 'b');
	
	/**
	 * Constructor
	 * @return Export_synda_listings_model
	 */
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
	}

	/**
	 * Return file name
	 */
	public function get_filename(){
		return "listings.xml";
	}

	/**
	 * Generate output file
	 * @param string $filename export file name
	 * @param array $data export data
	 * @param array $settings driver settings
	 */
	public function generate($filename, $data, $settings){
		$f = fopen($filename, "wb");
		
		fputs($f, '<?xml version="1.0" encoding="UTF-8">'."\n");
		fputs($f, '<Listings>'."\n");
		foreach($data as $i=>$row){
			if(!$i) continue;
			
			fputs($f, '	<Listing>'."\n");
			fputs($f, '			<PublisherId>'.$row['PublisherId'].'</PublisherId>'."\n");
			fputs($f, '			<ListingId>'.$row['ListingId'].'</ListingId>'."\n");
				
			if(isset($row['PropertyId']) && !empty($row['PropertyId']))
				fputs($f, '			<PropertyId>'.$row['PropertyId'].'</PropertyId>'."\n");
				
			fputs($f, '			<GMTLastModified>'.date('c', strtotime($row['GMTLastModified'])).'</GMTLastModified>'."\n");				
			fputs($f, '			<ListingStatus>active</ListingStatus>'."\n");					
			fputs($f, '			<PropertyType>'.$row['PropertyType'].'</PropertyType>'."\n");
			fputs($f, '			<AgentId>'.$row['AgentId'].'</AgentId>'."\n");
			
			if(isset($row['DistressSale']) && !empty($row['DistressSale']))
				fputs($f, '			<DistressSale>'.$row['DistressSale'].'</DistressSale>'."\n");
				
			fputs($f, '			<RentalPeriod>'.$row['RentalPeriod'].'</RentalPeriod>'."\n");
			
			if(isset($row['CapRate']) && !empty($row['CapRate']))
				fputs($f, '			<CapRate>'.$row['CapRate'].'</CapRate>'."\n");
					
			fputs($f, '			<Currency>'.$row['Currency'].'</Currency>'."\n");
			fputs($f, '			<Price>'.$row['Price'].'</Price>'."\n");
				
			if(isset($row['PropertyTax']) && !empty($row['PropertyTax']))
				fputs($f, '			<PropertyTax>'.$row['PropertyTax'].'</PropertyTax>'."\n");
				
			if(isset($row['HomeownerDues']) && !empty($row['HomeownerDues']))
				fputs($f, '			<HomeownerDues>'.$row['HomeownerDues'].'</HomeownerDues>'."\n");
			
			if(isset($row['StreetAddress']) && !empty($row['StreetAddress']))
				fputs($f, '			<StreetAddress hide="'.(isset($row['StreetAddress']) ? 'true' : 'false').'"><![CDATA['.$row['StreetAddress'].']]></StreetAddress>'."\n");
			
			if(isset($row['District']) && !empty($row['District']))
				fputs($f, '			<District><![CDATA['.$row['District'].']]></District>'."\n");
					
			fputs($f, '			<City><![CDATA['.$row['City'].']]></City>'."\n");
			fputs($f, '			<StateProvince><![CDATA['.$row['StateProvince'].']]></StateProvince>'."\n");				
			fputs($f, '			<Country>'.$row['Country'].'</Country>'."\n");
				
			if(isset($row['PostalCode']) && !empty($row['PostalCode']))
				fputs($f, '			<PostalCode>'.$row['PostalCode'].'</PostalCode>'."\n");
					
			if(isset($row['StructureArea']) && !empty($row['StructureArea']))
				fputs($f, '			<StructureArea>'.$row['StructureArea'].'</StructureArea>'."\n");
				
			if(isset($row['LotLengthWidth']) && !empty($row['LotLengthWidth']))
				fputs($f, '			<LotLengthWidth>'.$row['LotLengthWidth'].'</LotLengthWidth>'."\n");
				
			if(isset($row['IrregularLot']) && !empty($row['IrregularLot']))
				fputs($f, '			<IrregularLot>'.$row['IrregularLot'].'</IrregularLot>'."\n");
				
			if(isset($row['LandArea']) && !empty($row['LandArea']))
				fputs($f, '			<LandArea>'.$row['LandArea'].'</LandArea>'."\n");
				
			if(isset($row['BedRooms']) && !empty($row['BedRooms']))
				fputs($f, '			<BedRooms>'.$row['BedRooms'].'</BedRooms>'."\n");
				
			if(isset($row['BathRooms']) && !empty($row['BathRooms']))
				fputs($f, '			<BathRooms>'.$row['BathRooms'].'</BathRooms>'."\n");
				
			if(isset($row['ReceptionRooms']) && !empty($row['ReceptionRooms']))
				fputs($f, '			<ReceptionRooms>'.$row['ReceptionRooms'].'</ReceptionRooms>'."\n");
				
			if(isset($row['TotalRooms']) && !empty($row['TotalRooms']))
				fputs($f, '			<TotalRooms>'.$row['TotalRooms'].'</TotalRooms>'."\n");
				
			if(isset($row['YearBuilt']) && !empty($row['YearBuilt']))
				fputs($f, '			<YearBuilt>'.$row['YearBuilt'].'</YearBuilt>'."\n");
				
			if(isset($row['AVMDeny']) && $row['AVMDeny'])
				fputs($f, '			<AVMDeny>1</AVMDeny>'."\n");
				
			if(isset($row['Latitude']) && !empty($row['Latitude']))
				fputs($f, '			<Latitude>'.floatval($row['Latitude']).'</Latitude>'."\n");
					
			if(isset($row['Longitude']) && !empty($row['Longitude']))
				fputs($f, '			<Longitude>'.floatval($row['Longitude']).'</Longitude>'."\n");
				
			if(isset($row['PhotoURL']) && !empty($row['PhotoURL']))				
				fputs($f, '			<PhotoURL>'.$row['PhotoURL'].'</PhotoURL>'."\n");
				
			if(isset($row['VirtualTourURL']) && !empty($row['VirtualTourURL']))				
				fputs($f, '			<VirtualTourURL>'.$row['VirtualTourURL'].'</VirtualTourURL>'."\n");
				
			if(isset($row['Previews']) && !empty($row['Previews'])){
				fputs($f, '			<Previews>'."\n");
					
				foreach((array)$row['Previews'] as $preview){
					fputs($f, '				<Preview>'."\n");
					fputs($f, '					<Begins>'.date('Y-m-d H:m:s', strtotime($preview['Begins'])).'</Begins>'."\n");
					fputs($f, '					<Ends>'.date('Y-m-d H:m:s', strtotime($preview['Ends'])).'</Ends>'."\n");
					fputs($f, '					<Notes><![CDATA['.$preview['Notes'].']]></Notes>'."\n");
					fputs($f, '				</Preview>'."\n");
				}
				
				fputs($f, '			</Previews>'."\n");
			}
				
			if(isset($row['GMTOffset']) && !empty($row['GMTOffset']))
				fputs($f, '			<GMTOffset>'.$row['GMTOffset'].'</GMTOffset>'."\n");

			fputs($f, '			<DescriptionLang value="'.$this->CI->pg_language->get_lang_code_by_id($this->CI->pg_language->current_lang_id).'">'."\n");
			fputs($f, '				<Caption><![CDATA['.$row['DescriptionLang.Caption'].']]></Caption>'."\n");
			fputs($f, '				<Description><![CDATA['.$row['DescriptionLang.Description'].']]></Description>'."\n");
			fputs($f, '				<DetailsURL>'.$row['DescriptionLang.DetailsURL'].'</DetailsURL>'."\n");
			fputs($f, '			</DescriptionLang>'."\n");
			fputs($f, '	</Listing>'."\n");
			
		}
		fputs($f, '</Listings>'."\n");
		
		fclose($f);
	}
	
	/**
	 * Validate data
	 * @param array $data data for validation
	 */
	public function validate_data($data){
		$return = array("errors" => array(), "data" => array());		
		$return["data"] = $data;		
		if(!isset($data["PublisherId"]) || empty($data["PublisherId"])) $return["errors"][] = "PublisherId";
		if(!isset($data["ListingId"]) || empty($data["ListingId"])) $return["errors"][] = "ListingId";
		if(!isset($data["GMTLastModified"]) || strtotime($data["GMTLastModified"])<=0) $return["errors"][] = "GMTLastModified";
		if(!isset($data["ListingStatus"]) || empty($data["ListingStatus"])) $return["errors"][] = "ListingStatus";
		if(!isset($data["PropertyType"]) || empty($data["PropertyType"])) $return["errors"][] = "PropertyType";
		if(!isset($data["AgentId"]) || empty($data["AgentId"])) $return["errors"][] = "AgentId";
		//if(!isset($data["RentalPeriod"])) $return["errors"][] = "RentalPeriod";
		if(!isset($data["Currency"]) || empty($data["Currency"])) $return["errors"][] = "Currency";
		if(!isset($data["Price"]) || empty($data["Price"])) $return["errors"][] = "Price";
		if(!isset($data["City"]) || empty($data["City"])) $return["errors"][] = "City";
		if(!isset($data["StateProvince"]) || empty($data["StateProvince"])) $return["errors"][] = "StateProvice";
		if(!isset($data["Country"]) || empty($data["Country"])) $return["errors"][] = "Country";
		if(!isset($data["DescriptionLang.Caption"]) || empty($data["DescriptionLang.Caption"])) $return["errors"][] = "DescriptionLang.Caption";
		if(!isset($data["DescriptionLang.Description"]) || empty($data["DescriptionLang.Description"])) $return["errors"][] = "DescriptionLang.Description";
		if(!isset($data["DescriptionLang.DetailsURL"]) || empty($data["DescriptionLang.DetailsURL"])) $return["errors"][] = "DescriptionLang.DefaultURL";
		
		if(isset($return['data']['PhotoURL']) && !empty($return['data']['PhotoURL'])) $return['data']['PhotoURL'] = array_shift(explode('|', $return['data']['PhotoURL']));
		if(isset($return['data']['VirtualTourURL']) && !empty($return['data']['VirtualTourURL'])) $return['data']['PhotoURL'] = array_shift(explode('|', $return['data']['VirtualTourURL']));
		
		return $return;
	}
}
