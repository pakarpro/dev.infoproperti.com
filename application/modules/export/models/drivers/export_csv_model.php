<?php 
/**
 * CSV export driver model
 * 
* @package PG_RealEstate
 * @subpackage application
 * @category	modules
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/

if(!defined("BASEPATH")) exit("No direct script access allowed");

class Export_csv_model extends Model{
	
	/**
	 * Link to CodeIgniter object
	 * @var object
	 */
	private $CI;

	/**
	 * Content type
	 * @var object
	 */
	public $content_file = "text_csv";
	
	/**
	 * Constructor
	 * @return Export_csv_model
	 */
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
	}
	
	/**
	 * Return file name
	 */
	public function get_filename(){
		return "export.csv";
	}
	
	/**
	 * Generate output file
	 * @param string $filename export file name
	 * @param array $data export data
	 * @param array $settings driver settings
	 */
	public function generate($filename, $data, $settings){
		$f = fopen($filename, "wb");
		foreach($data as $row){
			fputcsv($f, $row, $settings["delimiter"] ? $settings["delimiter"] : ";", $settings["enclosure"] ? $settings["enclosure"] : "\"");
		}
		fclose($f);
	}
	
	/**
	 * Validate data
	 * @param array $data data for validation
	 */
	public function validate_data($data){
		$return = array("errors" => array(), "data" => array());
		$return["data"] = $data;
		return $return;
	}
}
