<?php 
/**
 * Yandex realty driver model
 * 
* @package PG_RealEstate
 * @subpackage application
 * @category	modules
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/

if(!defined("BASEPATH")) exit("No direct script access allowed");

class Export_yrl_model extends Model{
	
	/**
	 * Link to CodeIgniter object
	 * @var object
	 */
	private $CI;
	
	/**
	 * Not available symbols
	 */
	protected $search = array('&', "'", '"', ">", "<");
	
	/**
	 * Symbols for replace
	 */
	protected $replace = array('&amp;', '&apos;', '&quot;', "&gt;", "&lt;");

	/**
	 * Realty type
	 * @var array
	 */
	private $realty_type = array("городская", "загородная");
	
	/**
	 * Yrl realty type
	 * @var array
	 */
	private $type = array("продажа", "аренда");
	
	/**
	 * Property type
	 * @var array
	 */
	private $property_type = array('жилая', 'коммерческая', 'земельный участок');
	
	/**
	 * Realty category
	 * @var array
	 */
	private $category = array(
		"комната", "квартира", "дом", "участок", "flat", "room", 
		"house", "cottage", "townhouse", "таунхаус", "часть дома", 
		"house with lot", "дом с участком", "дача", "lot", "земельный участок",
	);
	
	/**
	 * Currency
	 * @var array
	 */
	private $currency = array("RUR", "RUB", "EUR", "USD", "UAH", "BYR", "KZT");
	
	/**
	 * Rental period
	 * @var array
	 */
	private $period = array("день", "месяц", "day", "month");
	
	/**
	 * Square units
	 * @var array
	 */
	private $unit = array("кв. м", "гектар", "cотка", "sq.m", "hectare");
	
	/**
	 * Building state
	 * @var array
	 */
	private $building_state = array("unfinished", "built", "hand-over");
	
	/**
	 * Ready quarter
	 * @var array
	 */
	private $ready_quarter = array(1, 2, 3, 4);
	
	/**
	 * Constructor
	 * @return Export_yrl_model
	 */
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
	}
	
	/**
	 * Return file name
	 */
	public function get_filename(){
		return "export.xml";
	}
	
	/**
	 * Generate output file
	 * @param string $filename export file name
	 * @param array $data export data
	 * @param array $settings driver settings
	 */
	public function generate($filename, $data, $settings){
		$f = fopen($filename, "wb");
		fputs($f, '<?xml version="1.0" encoding="utf-8"?>'."\n");
		fputs($f, '<realty-feed xmlns="http://webmaster.yandex.ru/schemas/feed/realty/2010-06">'."\n");
		fputs($f, '	<generation-date>'.date('c').'</generation-date>'."\n");
		foreach($data as $i=>$row){
			if(!$i) continue;
			fputs($f, '	<offer internal-id="'.$row['internal-id'].'">'."\n");
			fputs($f, '		<type>'.$row['type'].'</type>'."\n");
			fputs($f, '		<property-type>'.$row['property-type'].'</property-type>'."\n");
			fputs($f, '		<category>'.$row['category'].'</category>'."\n");
			fputs($f, '		<url>'.$row['url'].'</url>'."\n");
			fputs($f, '		<creation-date>'.$row['creation-date'].'</creation-date>'."\n");
			
			if(isset($row['last-update-date']) && !empty($row['last-update-date']))
				fputs($f, '		<last-update-date>'.$row['last-update-date'].'</last-update-date>'."\n");
			
			if(isset($row['expire-date']) && !empty($row['expire-date']))
				fputs($f, '		<expire-date>'.$row['expire-date'].'</expire-date>'."\n");
			
			if(isset($row['payed-adv']))
				fputs($f, '		<payed-adv>'.$row['payed-adv'].'</payed-adv>'."\n");
			
			if(isset($row['manually-added']))
				fputs($f, '		<manually-added>'.$row['manually-added'].'</manually-added>'."\n");
			
			fputs($f, '		<location>'."\n");
			fputs($f, '			<country>'.$row['location.country'].'</country>'."\n");
			
			if(isset($row['location.region']) && !empty($row['location.region']))
				fputs($f, '			<region>'.$row['location.region'].'</region>'."\n");
			
			if(isset($row['location.district']) && !empty($row['location.district']))
				fputs($f, '			<district>'.$row['location.district'].'</district>'."\n");
			
			if(isset($row['location.locality-name']) && !empty($row['location.locality-name']))			
				fputs($f, '			<locality-name>'.$row['location.locality-name'].'</locality-name>'."\n");
				
			if(isset($row['location.sub-locality-name']) && !empty($row['location.sub-locality-name']))
				fputs($f, '			<sub-locality-name>'.$row['location.sub-locality-name'].'</sub-locality-name>'."\n");
				
			if(isset($row['location.non-admin-sub-locality']) && !empty($row['location.non-admin-sub-locality']))
				fputs($f, '			<non-admin-sub-locality>'.$row['location.non-admin-sub-locality'].'</non-admin-sub-locality>'."\n");
		
			if(isset($row['location.address']) && !empty($row['location.address']))	
				fputs($f, '			<address>'.$row['location.address'].'</address>'."\n");
				
			if(isset($row['location.direction']) && !empty($row['location.direction']))	
				fputs($f, '			<direction>'.$row['location.direction'].'</direction>'."\n");
			
			if(isset($row['location.distance']) && !empty($row['location.distance']))	
				fputs($f, '			<distance>'.$row['location.distance'].'</distance>'."\n");
			
			if(isset($row['location.latitude']) && !empty($row['location.latitude']))	
				fputs($f, '			<latitude>'.$row['location.latitude'].'</latitude>'."\n");
				
			if(isset($row['location.longitude']) && !empty($row['location.longitude']))		
				fputs($f, '			<longitude>'.$row['location.longitude'].'</longitude>'."\n");
			
			if( (isset($row['location.metro']) && !empty($row['location.metro'])) ||
				(isset($row['location.time-on-foot']) && !empty($row['location.time-on-foot'])) ||
				(isset($row['location.time-on-transport']) && !empty($row['location.time-on-transport'])) ){
				fputs($f, '			<metro>'."\n"); 
				
				if(isset($row['location.metro']) && !empty($row['location.metro']))
					fputs($f, '				<name>'.$row['location.metro'].'</name>'."\n");
					
				if(isset($row['location.time-on-foot']) && !empty($row['location.time-on-foot']))
					fputs($f, '				<time-on-foot>'.$row['location.time-on-foot'].'</time-on-foot>'."\n");
				
				if(isset($row['location.time-on-transport']) && !empty($row['location.time-on-transport']))
					fputs($f, '				<time-on-transport>'.$row['location.time-on-transport'].'</time-on-transport>'."\n");
				
				fputs($f, '			</metro>'."\n");
			}
			
			if(isset($row['location.railway-station']) && !empty($row['location.railway-station']))
				fputs($f, '			<railway-station>'.$row['location.railway-station'].'</railway-station>'."\n");
			
			fputs($f, '		</location>'."\n");
			fputs($f, '		<sales-agent>'."\n");
			
			if(isset($row['sales-agent.name']) && !empty($row['sales-agent.name']))
				fputs($f, '			<name>'.$row['sales-agent.name'].'</name>'."\n");
				
			fputs($f, '			<phone>'.$row['sales-agent.phone'].'</phone>'."\n");
				
			if(isset($row['sales-agent.category']) && !empty($row['sales-agent.category']))
				fputs($f, '			<category>'.$row['sales-agent.category'].'</category>'."\n");
				
			if(isset($row['sales-agent.organization']) && !empty($row['sales-agent.organization']))	
				fputs($f, '			<organization>'.$row['sales-agent.organization'].'</organization>'."\n");
				
			if(isset($row['sales-agent.agency-id']) && !empty($row['sales-agent.agency-id']))
				fputs($f, '			<agency-id>'.$row['sales-agent.agency-id'].'</agency-id>'."\n");
			
			if(isset($row['sales-agent.url']) && !empty($row['sales-agent.url']))			
				fputs($f, '			<url>'.$row['sales-agent.url'].'</url>'."\n");
			
			if(isset($row['sales-agent.email']) && !empty($row['sales-agent.email']))
				fputs($f, '			<email>'.$row['sales-agent.email'].'</email>'."\n");
			
			if(isset($row['sales-agent.partner']) && !empty($row['sales-agent.partner']))
				fputs($f, '			<partner>'.$row['sales-agent.partner'].'</partner>'."\n");
				
			fputs($f, '		</sales-agent>'."\n");
			
			fputs($f, '		<price>'."\n");
			fputs($f, '			<value>'.$row['price.value'].'</value>'."\n");
			fputs($f, '			<currency>'.$row['price.currency'].'</currency>'."\n");
			
			if(isset($row['price.period']) && !empty($row['price.period']))
				fputs($f, '			<period>'.$row['price.period'].'</period>'."\n");
			
			if(isset($row['price.unit']) && !empty($row['price.unit']))
				fputs($f, '			<unit>'.$row['price.unit'].'</unit>'."\n");
			
			fputs($f, '		</price>'."\n");
			
			if(isset($row['not-for-agents']))
				fputs($f, '		<not-for-agents>'.$row['not-for-agents'].'</not-for-agents>'."\n");
			
			if(isset($row['haggle']))
				fputs($f, '		<haggle>'.$row['haggle'].'</haggle>'."\n");
				
			if(isset($row['mortgage']))	
				fputs($f, '		<mortgage>'.$row['mortgage'].'</mortgage>'."\n");
			
			if(isset($row['prepayment']))	
				fputs($f, '		<prepayment>'.$row['prepayment'].'</prepayment>'."\n");
				
			if(isset($row['rent-pledge']))
				fputs($f, '		<rent-pledge>'.$row['rent-pledge'].'</rent-pledge>'."\n");
				
			if(isset($row['rent-pledge']))
				fputs($f, '		<agent-fee>'.$row['agent-fee'].'</agent-fee>'."\n");
				
			if(isset($row['with-pets']))	
				fputs($f, '		<with-pets>'.$row['with-pets'].'</with-pets>'."\n");
				
			if(isset($row['with-children']))	
				fputs($f, '		<with-children>'.$row['with-children'].'</with-children>'."\n");
			
			if(isset($row['image']) && !empty($row['image'])){
				foreach($row['image'] as $image){
					fputs($f, '		<image>'.$image.'</image>'."\n");
				}
			}
			
			if(isset($row['renovation']) && !empty($row['renovation']))
				fputs($f, '		<renovation>'.$row['renovation'].'</renovation>'."\n");
				
			if(isset($row['description']) && !empty($row['description']))	
				fputs($f, '		<description>'.$row['description'].'</description>'."\n");
				
			if(isset($row['area.value']) && !empty($row['area.value']) && isset($row['area.unit']) && !empty($row['area.unit'])){
				fputs($f, '		<area>'."\n");
				fputs($f, '			<value>'.$row['area.value'].'</value>'."\n");
				fputs($f, '			<unit>'.$row['area.unit'].'</unit>'."\n");
				fputs($f, '		</area>'."\n");
			}
			
			if(isset($row['living.value']) && !empty($row['living.value']) && isset($row['living.unit']) && !empty($row['living.unit'])){
				fputs($f, '		<living-space>'."\n");
				fputs($f, '			<value>'.$row['living.value'].'</value>'."\n");
				fputs($f, '			<unit>'.$row['living.unit'].'</unit>'."\n");
				fputs($f, '		</living-space>'."\n");
			}
			
			if(isset($row['kitchen.value']) && !empty($row['kitchen.value']) && isset($row['kitchen.unit']) && !empty($row['kitchen.unit'])){
				fputs($f, '		<kitchen-space>'."\n");
				fputs($f, '			<value>'.$row['kitchen.value'].'</value>'."\n");
				fputs($f, '			<unit>'.$row['kitchen.unit'].'</unit>'."\n");
				fputs($f, '		</kitchen-space>'."\n");
			}

			if(isset($row['lot-area.value']) && !empty($row['lot-area.value']) && isset($row['lot-area.unit']) && !empty($row['lot-area.unit'])){
				fputs($f, '		<lot-area>'."\n");
				fputs($f, '			<value>'.$row['lot-area.value'].'</value>'."\n");
				fputs($f, '			<unit>'.$row['lot-area.unit'].'</unit>'."\n");
				fputs($f, '		</lot-area>'."\n");
			}
			
			if(isset($row['lot-type']) && !empty($row['lot-type']))
				fputs($f, '		<lot-type>'.$row['lot-type'].'</lot-type>'."\n");
			
			switch($row['property-type']){
				case 'жилая':
					if(isset($row['new-flat']) && !empty($row['new-flat']))
						fputs($f, '		<new-flat>'.$row['new-flat'].'</new-flat>'."\n");
				
					fputs($f, '		<rooms>'.$row['rooms'].'</rooms>'."\n");
					fputs($f, '		<rooms-offered>'.$row['rooms-offered'].'</rooms-offered>'."\n");
				
					if(isset($row['open-plan']))
						fputs($f, '		<open-plan>'.$row['open-plan'].'</open-plan>'."\n");
			
					if(isset($row['rooms-type']) && !empty($row['rooms-type']))
						fputs($f, '		<rooms-type>'.$row['rooms-type'].'</rooms-type>'."\n");
				
					if(isset($row['phone']))
						fputs($f, '		<phone>'.$row['phone'].'</phone>'."\n");
					
					if(isset($row['internet']))	
						fputs($f, '		<internet>'.$row['internet'].'</internet>'."\n");
					
					if(isset($row['room-furniture']))	
						fputs($f, '		<room-furniture>'.$row['room-furniture'].'</room-furniture>'."\n");
					
					if(isset($row['kitchen-furniture']))
						fputs($f, '		<kitchen-furniture>'.$row['kitchen-furniture'].'</kitchen-furniture>'."\n");
				
					if(isset($row['television']))
						fputs($f, '		<television>'.$row['television'].'</television>'."\n");
					
					if(isset($row['washing-machine']))
						fputs($f, '		<washing-machine>'.$row['washing-machine'].'</washing-machine>'."\n");
				
					if(isset($row['refrigerator']))
						fputs($f, '		<refrigerator>'.$row['refrigerator'].'</refrigerator>'."\n");
				
					if(isset($row['balcony']) && !empty($row['balcony']))
						fputs($f, '		<balcony>'.$row['balcony'].'</balcony>'."\n");
					
					if(isset($row['bathroom-unit']) && !empty($row['bathroom-unit']))
						fputs($f, '		<bathroom-unit>'.$row['bathroom-unit'].'</bathroom-unit>'."\n");
				
					if(isset($row['floor-covering']) && !empty($row['floor-covering']))
						fputs($f, '		<floor-covering>'.$row['floor-covering'].'</floor-covering>'."\n");
				
					if(isset($row['window-view']) && !empty($row['window-view']))
						fputs($f, '		<window-view>'.$row['window-view'].'</window-view>'."\n");
				
					if(isset($row['floor']) && !empty($row['floor']))
						fputs($f, '		<floor>'.$row['floor'].'</floor>'."\n");
					
					if(isset($row['floors-total']) && !empty($row['floors-total']))
						fputs($f, '		<floors-total>'.$row['floors-total'].'</floors-total>'."\n");
					
					if(isset($row['building-name']) && !empty($row['building-name']))
						fputs($f, '		<building-name>'.$row['building-name'].'</building-name>'."\n");
				
					if(isset($row['building-type']) && !empty($row['building-type']))
						fputs($f, '		<building-type>'.$row['building-type'].'</building-type>'."\n");
				
					if(isset($row['building-series']) && !empty($row['building-series']))
						fputs($f, '		<building-series>'.$row['building-series'].'</building-series>'."\n");
				
					if(isset($row['building-state']) && !empty($row['building-state']))
						fputs($f, '		<building-state>'.$row['building-state'].'</building-state>'."\n");
					
					if(isset($row['built-year']) && !empty($row['built-year']) && strlen($row['built-year'])==4)	
						fputs($f, '		<built-year>'.$row['built-year'].'</built-year>'."\n");
				
					if(isset($row['ready-quarter']) && in_array($row['ready-quarter'], $this->ready_quarter))	
						fputs($f, '		<ready-quarter>'.$row['ready-quarter'].'</ready-quarter>'."\n");
					
					if(isset($row['lift']))	
						fputs($f, '		<lift>'.$row['lift'].'</lift>'."\n");
				
					if(isset($row['rubbish-chute']))	
						fputs($f, '		<rubbish-chute>'.$row['rubbish-chute'].'</rubbish-chute>'."\n");
					
					if(isset($row['is-elite']))		
						fputs($f, '		<is-elite>'.$row['is-elite'].'</is-elite>'."\n");
					
					if(isset($row['parking']))
						fputs($f, '		<parking>'.$row['parking'].'</parking>'."\n");
				
					if(isset($row['alarm']))
						fputs($f, '		<alarm>'.$row['alarm'].'</alarm>'."\n");
				
					if(isset($row['ceiling-height']) && !empty($row['ceiling-height']))
						fputs($f, '		<ceiling-height>'.$row['ceiling-height'].'</ceiling-height>'."\n");
				break;
			}
			
			switch($row['realty-type']){
				case 'загородная':
					if(isset($row['pmg']))
						fputs($f, '		<pmg>'.$row['pmg'].'</pmg>'."\n");
					
					if(isset($row['toilet']) && !empty($row['toilet']))
						fputs($f, '		<toilet>'.$row['toilet'].'</toilet>'."\n");
				
					if(isset($row['shower']) && !empty($row['shower']))
						fputs($f, '		<shower>'.$row['shower'].'</shower>'."\n");
					
					if(isset($row['kitchen']))
						fputs($f, '		<kitchen>'.$row['kitchen'].'</kitchen>'."\n");
		
					if(isset($row['pool']))
						fputs($f, '		<pool>'.$row['pool'].'</pool>'."\n");
				
					if(isset($row['billiard']))
						fputs($f, '		<billiard>'.$row['billiard'].'</billiard>'."\n");
        
					if(isset($row['sauna']))
						fputs($f, '		<sauna>'.$row['sauna'].'</sauna>'."\n");
				
					if(isset($row['water-supply']))
						fputs($f, '		<water-supply>'.$row['water-supply'].'</water-supply>'."\n");
        
					if(isset($row['electricity-supply']))
						fputs($f, '		<electricity-supply>'.$row['electricity-supply'].'</electricity-supply>'."\n");
        
					if(isset($row['sewerage-supply']))
						fputs($f, '		<sewerage-supply>'.$row['sewerage-supply'].'</sewerage-supply>'."\n");
        
					if(isset($row['gas-supply']))
						fputs($f, '		<gas-supply>'.$row['gas-supply'].'</gas-supply>'."\n");
				break;
			}
			fputs($f, '	</offer>'."\n");
		}
		fputs($f, '</realty-feed>'."\n");
		fclose($f);
	}
	
	/**
	 * Validate data
	 * @param array $data data for validation
	 */
	public function validate_data($data){
		$return = array('errors'=>array(), 'data'=>array());
	
		if(isset($data['realty-type'])){
			$return['data']['realty-type'] = str_replace($this->search, $this->replace, $data['realty-type']);
			if(!in_array($return['data']['realty-type'], $this->realty_type)) $return['data']['realty-type'] = current($this->realty_type);
		}else{
			$return['data']['realty-type'] = current($this->realty_type);
		}
		
		if(isset($data['internal-id'])){
			$return['data']['internal-id'] = intval($data['internal-id']);
			if(empty($return['data']['internal-id'])) $return['errors'][] = 'internal-id';
		}else{
			$return['errors'][] = 'internal-id';
		}
		
		if(isset($data['type'])){
			$return['data']['type'] = str_replace($this->search, $this->replace, $data['type']);
			switch($return['data']['type']){
				case 'sale':
					$return['data']['type'] = 'продажа';
				break;
				case 'buy':
					$return['data']['type'] = 'покупка';
				break;
				case 'rent':
					$return['data']['type'] = 'аренда';
				break;
			}
			if(!in_array($return['data']['type'], $this->type)) $return['errors'][] = 'type';
		}else{
			$return['errors'][] = 'type';
		}
		
		if(isset($data['property-type'])){
			$return['data']['property-type'] = str_replace($this->search, $this->replace, $data['property-type']);
			switch($return['data']['property-type']){
				case 'residential':
					$return['data']['property-type'] = 'жилая';
				break;
				case 'commercial':
					$return['data']['property-type'] = 'коммерческая';
				break;
				case  'lot_and_land':
					$return['data']['property-type'] ='земельный участок';
				break;
			}
			if(empty($return['data']['property-type'])) $return['errors'][] = 'property-type';
		}else{
			$return['errors'][] = 'property-type';
		}

		if(isset($data['category'])){
			$return['data']['category'] = str_replace($this->search, $this->replace, $data['category']);
			if(empty($return['data']["category"]) || !in_array($return['data']["category"], $this->category)) $return['errors'][] = 'category';
		}else{
			$return['errors'][] = 'category';
		}
		
		if(isset($data['url'])){
			$return['data']['url'] = str_replace($this->search, $this->replace, $data['url']);
			if(empty($return['data']['url'])) $return['errors'][] = 'url';
		}else{
			$return['errors'][] = 'url';
		}
		
		if(isset($data['creation-date'])){
			$value = strtotime($data['creation-date']);
			if($value > 0){
				$return['data']['creation-date'] = date('c', $value);
			}else{
				$return['errors'][] = 'creation-date';
			}
		}else{
			$return['errors'][] = 'creation-date';
		}
		
		if(isset($data['last-update-date'])){
			$value = strtotime($data['last-update-date']);
			if($value > 0) $return['data']['last-update-date'] = date('c', $value);
		}
		
		if(isset($data['expire-date'])){
			$value = strtotime($data['expire-date']);
			if($value > 0) $return['data']['expire-date'] = date('c', $value);
		}
		
		if(isset($data['payed-adv'])){
			$return['data']['payed-adv'] = $data['payed-adv'] ? 1 : 0;
		}
		
		if(isset($data['manually-added'])){
			$return['data']['manually-added'] = $data['manually-added'] ? 1 : 0;
		}
		
		if(isset($data['location.country'])){
			$return['data']['location.country'] = str_replace($this->search, $this->replace, $data['location.country']);
			if(empty($return['data']['location.country'])) $return["errors"][] = 'location.country';
		}else{
			$return['errors'][] = 'location.region';
		}
		
		if(isset($data['location.region'])){
			if($data['location.locality-name'] != 'Москва' || $data['location.locality-name'] != 'Санкт-Петербург'){
				$return['data']['location.region'] = str_replace($this->search, $this->replace, $data['location.region']);
				if(empty($return['data']['location.region'])){ 
					$return['errors'][] = 'location.region';
				}
			}
		}else{
			$return['errors'][] = 'location.region';
		}
	
		if(isset($data['location.district'])){
			$return['data']['location.district'] = str_replace($this->search, $this->replace, $data['location.district']);
		}
		
		if(isset($data['location.locality-name'])){
			$return['data']['location.locality-name'] = str_replace($this->search, $this->replace, $data['location.locality-name']);
			if(empty($return['data']['location.locality-name']) && $return['data']['realty-type'] == 'городская'){
				$return['errors'][] = 'location.locality-name';
			}
		}elseif($return['data']['realty-type'] == 'городская'){
			$return['errors'][] = 'location.locality-name';
		}
		
		if(isset($data['location.sub-locality-name'])){
			$return['data']['location.sub-locality-name'] = str_replace($this->search, $this->replace, $data['location.sub-locality-name']);
		}
		
		if(isset($data['location.non-admin-sub-locality'])){
			$return['data']['location.non-admin-sub-locality'] = str_replace($this->search, $this->replace, $data['location.non-admin-sub-locality']);
		}
		
		if(isset($data['location.address'])){
			$return['data']['location.address'] = str_replace($this->search, $this->replace, $data['location.address']);
			if(empty($return['data']['location.address']) && $return['data']['realty-type'] == 'городская'){
				$return['errors'][] = 'location.address';
			}
		}elseif($return['data']['realty-type'] == 'городская'){
			$return['errors'][] = 'location.address';
		}
				
		if(isset($data['location.direction'])){
			if($return['data']['location.locality-name'] == 'Москва'){
				$return['data']['location.direction'] = str_replace($this->search, $this->replace, $data['location.direction']);
			}
		}
		
		if(isset($data['location.distance'])){
			if($return['data']['location.locality-name'] == 'Москва'){
				$return['data']['location.distance'] = intval($this->search, $this->replace, $data['location.distance']);
			}
		}
			
		if(isset($data['location.latitude'])){	
			$return['data']['location.latitude'] = floatval($data['location.latitude']);
		}
				
		if(isset($data['location.longitude'])){
			$return['data']['location.longitude'] = floatval($data['location.longitude']);
		}
		
		if(isset($data['location.metro'])){
			$return['data']['location.metro'] = str_replace($this->search, $this->replace, $data['location.metro']);
		}
	
		if(isset($data['location.time-on-foot'])){
			$return['data']['location.time-on-foot'] = intval($data['location.time-on-foot']);
		}
		
		if(isset($data['location.time-on-transport'])){
			$return['data']['location.time-on-transport'] = intval($data['location.time-on-transport']);
		}
		
		if(isset($data['location.railway-station'])){
			$return['data']['location.railway-station'] = str_replace($this->search, $this->replace, $data['location.railway-station']);
		}
			
		if(isset($data['sales-agent.name'])){
			$return['data']['sales-agent.name'] = str_replace($this->search, $this->replace, $data['sales-agent.name']);
		}
				
		if(isset($data['sales-agent.phone'])){
			$return['data']['sales-agent.phone'] = str_replace($this->search, $this->replace, $data['sales-agent.phone']);
			if(empty($return['data']['sales-agent.phone'])) $return["errors"][] = "sales-agent.phone";
		}else{
			$return["errors"][] = "sales-agent.phone";
		}
				
		if(isset($data['sales-agent.category'])){
			$return['data']['sales-agent.category'] = str_replace($this->search, $this->replace, $data['sales-agent.category']);
		}
				
		if(isset($data['sales-agent.organization'])){	
			$return['data']['sales-agent.organization'] = str_replace($this->search, $this->replace, $data['sales-agent.organization']);
		}
			
		if(isset($data['sales-agent.agency-id'])){
			$return['data']['sales-agent.agency-id'] = intval($data['sales-agent.agency-id']);
		}
			
		if(isset($data['sales-agent.url'])){
			$return['data']['sales-agent.url'] = str_replace($this->search, $this->replace, $data['sales-agent.url']);
		}
			
		if(isset($data['sales-agent.email'])){
			$return['data']['sales-agent.email'] = str_replace($this->search, $this->replace, $data['sales-agent.email']);
		}	
		
		if(isset($data['sales-agent.partner'])){
			$return['data']['sales-agent.partner'] = str_replace($this->search, $this->replace, $data['sales-agent.partner']);
		}		
	
		if(isset($data['price.value'])){
			$return['data']['price.value'] = intval($data['price.value']);
			if(empty($return['data']["price.value"])) $return["errors"][] = "price.value";
		}else{
			$return["errors"][] = "price.value";
		}
		
		if(isset($data['price.currency'])){
			$return['data']['price.currency'] = str_replace($this->search, $this->replace, $data['price.currency']);
			if(!in_array($return['data']["price.currency"], $this->currency)) $return["errors"][] = "price.currency";
		}else{
			$return["errors"][] = "price.currency";
		}
		
		if(isset($data['price.period'])){
			$return['data']['price.period'] = str_replace($this->search, $this->replace, $data['price.period']);
		}
			
		if(isset($data['price.unit'])){
			$return['data']['price.unit'] = str_replace($this->search, $this->replace, $data['price.unit']);
		}
			
		if(isset($data['not-for-agents'])){
			$return['data']['not-for-agents'] = $data['not-for-agents'] ? 1 : 0;
		}
			
		if(isset($data['haggle'])){
			$return['data']['haggle'] = $data['haggle'] ? 1 : 0;
		}
		
		if(isset($data['mortgage'])){
			$return['data']['mortgage'] = $data['mortgage'] ? 1 : 0;
		}
			
		if(isset($data['prepayment'])){
			$return['data']['prepayment'] = $data['prepayment'] ? 1 : 0;
		}
				
		if(isset($data['rent-pledge'])){
			$return['data']['rent-pledge'] = $data['rent-pledge'] ? 1 : 0;
		}
				
		if(isset($data['agent-fee'])){
			$return['data']['agent-fee'] = intval($data['agent-fee']);
			if($return['data']['agent-fee']<0 || $return['data']['agent-fee']>100){
				unset($return['data']['agent-fee']);
			}
		}
		
		if(isset($data['with-pets'])){
			$return['data']['with-pets'] = $data['with-pets'] ? 1 : 0;
		}
		
		if(isset($data['with-children'])){
			$return['data']['with-children'] = $data['with-children'] ? 1 : 0;
		}
	
		if(isset($data['image'])){
			$return['data']['image'] = explode('|', $data['image']);
			foreach($return['data']['image'] as $key=>$image){
				$return['data']['image'][$key] = str_replace($this->search, $this->replace, $image);
			}
		}

		if(isset($data['renovation'])){
			$return['data']['renovation'] = str_replace($this->search, $this->replace, $data['renovation']);
		}
				
		if(isset($data['description'])){
			$return['data']['description'] = str_replace($this->search, $this->replace, $data['description']);
		}		
				
		if(isset($data['area.value'])){
			$return['data']['area.value'] = intval($data['area.value']);
		}
		
		if(isset($data['area.unit'])){
			$return['data']['area.unit'] = str_replace($this->search, $this->replace, $data['area.unit']);
		}
			
		if(isset($data['living.value'])){
			$return['data']['living.value'] = intval($data['living.value']);
		}
		
		if(isset($data['living.unit'])){
			$return['data']['living.unit'] = str_replace($this->search, $this->replace, $data['living.unit']);
		}
		
		if(isset($data['kitchen.value'])){
			$return['data']['kitchen.value'] = intval($data['kitchen.value']);
		}	
		
		if(isset($data['kitchen.unit'])){
			$return['data']['kitchen.unit'] = str_replace($this->search, $this->replace, $data['kitchen.unit']);
		}
		
		if(isset($data['lot-area.value'])){
			$return['data']['lot-area.value'] = intval($data['lot-area.value']);
		}
		
		if(isset($data['lot-area.unit'])){
			$return['data']['lot-area.unit'] = str_replace($this->search, $this->replace, $data['lot-area.unit']);
		}
		
		if(isset($data['lot-type'])){
			$return['data']['lot-type'] = str_replace($this->search, $this->replace, $data['lot-type']);
		}
			
		if(isset($data['new-flat'])){
			$return['data']['new-flat'] = $data['new-flat'] ? 1 : 0;
		}
				
		if(isset($data['rooms'])){
			$return['data']['rooms'] = intval($data['rooms']);
			if($return['data']['property-type'] == 'жилая' && empty($return['data']['rooms'])){
				$return["errors"][] = "rooms";
			}
		}elseif($return['data']['property-type'] == 'жилая'){
			$return["errors"][] = "rooms";
		}
		
		if(isset($data['rooms-offered'])){
			$return['data']['rooms-offered'] = str_replace($this->search, $this->replace, $data['rooms-offered']);
			if($return['data']['property-type'] == 'жилая' && empty($return['data']["rooms-offered"])){
				$return["errors"][] = "rooms-offered";
			}
		}elseif($return['data']['property-type'] == 'жилая'){
			$return["errors"][] = "rooms-offered";
		}
	
		if(isset($data['open-plan'])){
			$return['data']['open-plan'] = $data['open-plan'] ? 1 : 0;
		}
			
		if(isset($data['rooms-type'])){
			$return['data']['rooms-type'] = str_replace($this->search, $this->replace, $data['rooms-type']);
		}
				
		if(isset($data['phone'])){
			$return['data']['phone'] = $data['phone'] ? 1 : 0;
		}
					
		if(isset($data['internet'])){
			$return['data']['internet'] = $data['internet'] ? 1 : 0;
		}
					
		if(isset($data['room-furniture'])){
			$return['data']['room-furniture'] = $data['room-furniture'] ? 1 : 0;
		}
					
		if(isset($data['kitchen-furniture'])){
			$return['data']['kitchen-furniture'] = $data['kitchen-furniture'] ? 1 : 0;
		}
				
		if(isset($data['television'])){
			$return['data']['television'] = $data['television'] ? 1 : 0;
		}
					
		if(isset($data['washing-machine'])){
			$return['data']['washing-machine'] = $data['washing-machine'] ? 1 : 0;
		}
				
		if(isset($data['refrigerator'])){
			$return['data']['refrigerator'] = $data['refrigerator'] ? 1 : 0;
		}
				
		if(isset($data['balcony'])){
			$return['data']['balcony'] = str_replace($this->search, $this->replace, $data['balcony']);
		}
					
		if(isset($data['bathroom-unit'])){
			$return['data']['bathroom-unit'] = str_replace($this->search, $this->replace, $data['bathroom-unit']);
		}
				
		if(isset($data['floor-covering'])){
			$return['data']['floor-covering'] = str_replace($this->search, $this->replace, $data['floor-covering']);
		}
				
		if(isset($data['window-view'])){
			$return['data']['window-view'] = str_replace($this->search, $this->replace, $data['window-view']);
		}
				
		if(isset($data['floor'])){
			$return['data']['floor'] = intval($data['floor']);
		}
					
		if(isset($data['floors-total'])){
			$return['data']['floors-total'] = intval($data['floors-total']);
		}
					
		if(isset($data['building-name'])){
			$return['data']['building-name'] = str_replace($this->search, $this->replace, $data['building-name']);
		}
				
		if(isset($data['building-type'])){
			$return['data']['building-type'] = str_replace($this->search, $this->replace, $data['building-type']);
		}
				
		if(isset($data['building-series'])){
			$return['data']['building-series'] = str_replace($this->search, $this->replace, $data['building-series']);
		}
				
		if(isset($data['building-state'])){
			$return['data']['building-state'] = str_replace($this->search, $this->replace, $data['building-state']);
		}
	
		if(isset($data['built-year'])){
			if(strlen($data['built-year'])!=4) $data['built-year'] = 0;
			$return['data']['built-year'] = intval($data['built-year']);
		}
				
		if(isset($data['ready-quarter'])){
			$return['data']['ready-quarter'] = $data['ready-quarter'];
			if(!in_array($return['data']['ready-quarter'], $this->ready_quarter)){
				unset($return['data']['ready-quarter']);
			}
		}
					
		if(isset($data['lift'])){
			$return['data']['lift'] = $data['lift'] ? 1 : 0;
		}
				
		if(isset($data['rubbish-chute'])){	
			$return['data']['rubbish-chute'] = $data['rubbish-chute'] ? 1 : 0;
		}
					
		if(isset($data['is-elite'])){		
			$return['data']['is-elite'] = $data['is-elite'] ? 1 : 0;
		}
					
		if(isset($data['parking'])){
			$return['data']['parking'] = $data['parking'] ? 1 : 0;
		}
				
		if(isset($data['alarm'])){
			$return['data']['alarm'] = $data['alarm'] ? 1 : 0;
		}
				
		if(isset($data['ceiling-height'])){
			$return['data']['ceiling-height'] = floatval($data['ceiling-height']);
		}
			
		if(isset($data['pmg'])){
			$return['data']['pmg'] = $data['pmg'] ? 1 : 0;
		}
					
		if(isset($data['toilet'])){
			$return['data']['toilet'] = str_replace($this->search, $this->replace, $data['toilet']);
		}

		if(isset($data['shower'])){
			$return['data']['shower'] = str_replace($this->search, $this->replace, $data['shower']);
		}
					
		if(isset($data['kitchen'])){
			$return['data']['kitchen'] = $data['kitchen'] ? 1 : 0;
		}
		
		if(isset($data['pool'])){
			$return['data']['pool'] = $data['pool'] ? 1 : 0;
		}
				
		if(isset($data['billiard'])){
			$return['data']['billiard'] = $data['billiard'] ? 1 : 0;
		}
        
		if(isset($data['sauna'])){
			$return['data']['sauna'] = $data['sauna'] ? 1 : 0;
		}
				
		if(isset($data['water-supply'])){
			$return['data']['water-supply'] = $data['water-supply'] ? 1 : 0;
		}
        
		if(isset($data['electricity-supply'])){
			$return['data']['electricity-supply'] = $data['electricity-supply'] ? 1 : 0;
		}
        
		if(isset($data['sewerage-supply'])){
			$return['data']['sewerage-supply'] = $data['sewerage-supply'] ? 1 : 0;
		}
        
		if(isset($data['gas-supply'])){
			$return['data']['gas-supply'] = $data['gas-supply'] ? 1 : 0;
		}

		return $return;
	}
}
