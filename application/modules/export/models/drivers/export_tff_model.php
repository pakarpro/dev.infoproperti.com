<?php 
/**
 * Trulia export driver model
 * 
* @package PG_RealEstate
 * @subpackage application
 * @category	modules
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/

if(!defined("BASEPATH")) exit("No direct script access allowed");

class Export_tff_model extends Model{
	
	/**
	 * Link to CodeIgniter object
	 * @var object
	 */
	private $CI;
	
	/**
	 * Not available symbols
	 * @var array
	 */
	protected $search = array('&', "'", '"', ">", "<");
	
	/**
	 * Symbols for replace
	 * @var array
	 */
	protected $replace = array('&amp;', '&apos;', '&quot;', "&gt;", "&lt;");
	
	/**
	 * Geocode types
	 * @var array
	 */
	protected $geocode_types = array('exact', 'offset', 'approximate');
	
	/**
	 * Property types
	 * @var array
	 */
	protected $property_types = array('Apartment/Condo/Townhouse', 'Condo', 'Townhouse', 'Coop',
									  'Apartment', 'Loft', 'TIC', 'Mobile/Manufactured', 'Farm/Ranch',
									  'Multi-Family', 'Income/Investment', 'Houseboat', 'Lot/Land',
									  'Single-Family Home');
									  
	/**
	 * Listing types
	 * @var array
	 */
	protected $listing_types = array('resale', 'foreclosure', 'new home', 'rental');
	
	/**
	 * Statuses
	 * @var array
	 */
	protected $statuses = array('for rent', 'for sale', 'pending', 'active contingent', 'sold', 'withdrawn');
	
	/**
	 * Foreclosure statuses
	 * @var array
	 */
	protected $foreclosure_statuses = array('notice of default (pre-foreclosure)', 'lis pendens (pre-foreclosure)',
											'notice of trustee sale (auction)', 'notice of foreclosure sale (auction)',
											'reo - bank owned');
	
	/**
	 * Price terms
	 * @var array
	 */
	protected $price_terms = array('night', 'week', 'month', 'year');
	
	/**
	 * Rental types
	 * @var array
	 */
	protected $rental_types = array('standard', 'corporate', 'senior', 'military', 'campus',
									'market rate apt', 'condominium', 'cooperative', 'assisted living',
									'subsidized', 'nursing home', 'student', 'vacation', 'other');
	
	/**
	 * Credit cards
	 * @var array
	 */
	protected $credit_cards = array('visa', 'mastercard', 'discover', 'american express', 'other');
	
	/**
	 * Lease types
	 * @var array
	 */
	protected $lease_types = array('sublet', 'month-to-month', 'annual', 'bi-annual');
	
	/**
	 * Days of the week
	 * @var array
	 */
	protected $days_of_the_week = array('sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat');
	
	/**
	 * Hoa-periods
	 * @var array
	 */
	protected $hoa_periods = array('monthly', 'annual');	
	
	/**
	 * Appliances
	 * @var array
	 */
	protected $range_types = array('gas', 'electric', 'other');	
	
	/**
	 * Fireplace types
	 * @var array
	 */
	protected $fireplace_types = array('gas', 'wood', 'electric', 'decorative');	
	
	/**
	 * Heating systems
	 * @var array
	 */
	protected $heating_systems = array('gas', 'electric', 'radiant', 'other');	
	
	/**
	 * Heating fuels
	 * @var array
	 */
	protected $heating_fuels = array('coal', 'oil', 'gas', 'electric', 'propane', 'butane', 'solar', 'woodpellet', 'other', 'none');	
	
	/**
	 * Garage types
	 * @var array
	 */
	protected $garage_types = array('attached', 'detached');	
	
	/**
	 * Parking types
	 * @var array
	 */
	protected $parking_types = array('surface lot', 'garage lot', 'covered lot', 'street', 'carport', 'none', 'other');	
	
	/**
	 * Parking space fees
	 * @var array
	 */
	protected $parking_space_fees = array('free', 'paid', 'both');	
	
	/**
	 * Constructor
	 * @return Export_tff_model
	 */
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
	}
	
	/**
	 * Return file name
	 */
	public function get_filename(){
		return "export.xml";
	}
	
	/**
	 * Generate output file
	 * @param string $filename export file name
	 * @param array $data export data
	 * @param array $settings driver settings
	 */
	public function generate($filename, $data, $settings){
		$f = fopen($filename, "wb");
		fputs($f, '<?xml version="1.0" encoding="utf-8">'."\n");
		fputs($f, '<properties>'."\n");
		
		foreach($data as $i=>$row){
			if(!$i) continue;
			fputs($f, '	<property>'."\n");		
			fputs($f, '		<location>'."\n");
			
			if(isset($row['location.unit-number']) && !empty($row['location.unit-number']))
				fputs($f, '			<unit-number>'.$row['location.unit-number'].'</unit-number>'."\n");
			
			if(isset($row['location.street-address']) && !empty($row['location.street-address']))
				fputs($f, '			<street-address>'.str_replace($this->search, $this->replace, $row['location.street-address']).'</street-address>'."\n");
			
			if(isset($row['location.city-name']) && !empty($row['location.city-name']))
				fputs($f, '			<city-name>'.str_replace($this->search, $this->replace, $row['location.city-name']).'</city-name>'."\n");
			
			if(isset($row['location.zipcode']) && !empty($row['location.zipcode']))
				fputs($f, '			<zipcode>'.str_replace($this->search, $this->replace, $row['zipcode']).'</zipcode>'."\n");
				
			if(isset($row['location.county']) && !empty($row['location.county']))
				fputs($f, '			<county>'.str_replace($this->search, $this->replace, $row['location.county']).'</county>'."\n");
				
			if(isset($row['location.state-code']) && !empty($row['location.state-code']))	
				fputs($f, '			<state-code>'.str_replace($this->search, $this->replace, $row['location.state-code']).'</state-code>'."\n");
				
			if(isset($row['location.street-intersection']) && !empty($row['location.street-intersection']))		
				fputs($f, '			<street-intersection>'.str_replace($this->search, $this->replace, $row['location.street-intersection']).'</street-intersection>'."\n");
			
			if(isset($row['location.parcel-id']) && !empty($row['location.parcel-id']))		
				fputs($f, '			<parcel-id>'.str_replace($this->search, $this->replace, $row['location.parcel-id']).'</parcel-id>'."\n");
				
			if(isset($row['location.building-name']) && !empty($row['location.building-name']))		
				fputs($f, '			<building-name>'.str_replace($this->search, $this->replace, $row['location.building-name']).'</building-name>'."\n");
			
			if(isset($row['location.subdivision']) && !empty($row['location.subdivision']))		
				fputs($f, '			<subdivision>'.str_replace($this->search, $this->replace, $row['location.subdivision']).'</subdivision>'."\n");
			
			if(isset($row['location.neighborhood-name']) && !empty($row['location.neighborhood-name']))		
				fputs($f, '			<neighborhood-name>'.str_replace($this->search, $this->replace, $row['location.neighborhood-name']).'</neighborhood-name>'."\n");
			
			if(isset($row['location.neighborhood-description']) && !empty($row['location.neighborhood-description']))		
				fputs($f, '			<neighborhood-description>'.str_replace($this->search, $this->replace, $row['location.neighborhood-description']).'</neighborhood-description>'."\n");
			
			if(isset($row['location.elevation']) && !empty($row['location.elevation']))		
				fputs($f, '			<elevation>'.str_replace($this->search, $this->replace, $row['location.elevation']).'</elevation>'."\n");
			
			if(isset($row['location.longitude']) && !floatval($row['location.longitude']))
				fputs($f, '			<longitude>'.floatval($row['location.longitude']).'</longitude>'."\n");
			
			if(isset($row['location.latitude']) && !floatval($row['location.latitude']))
				fputs($f, '			<latitude>'.floatval($row['location.latitude']).'</latitude>'."\n");
			
			if(isset($row['location.geocode-type']) && in_array($row['location.geocode-type'], $this->geocode_types))
				fputs($f, '			<geocode-type>'.$row['location.geocode-type'].'</geocode-type>'."\n");
				
			fputs($f, '			<display-address>'.($row['location.display-address'] ? 'yes' : 'no').'</display-address>'."\n");
			
			if(isset($row['location.directions']) && !empty($row['location.directions']))		
				fputs($f, '			<directions>'.str_replace($this->search, $this->replace, $row['location.directions']).'</directions>'."\n");
			
			fputs($f, '		</location>'."\n");
			fputs($f, '		<details>'."\n");
			
			if(isset($row['details.listing-title']) && !empty($row['details.listing-title']))
				fputs($f, '			<listing-title>'.str_replace($this->search, $this->replace, $row['details.listing-title']).'</listing-title>'."\n");
				
			fputs($f, '			<price>'.$row['details.price'].'</price>'."\n");
			
			if(isset($row['details.year-built']) && !empty($row['details.year-built']) && strlen($row['details.year-built']) == 4)
				fputs($f, '			<year-built>'.$row['details.year-built'].'</year-built>'."\n");
			
			if(isset($row['details.num-bedrooms']) && !empty($row['details.num-bedrooms']))
				fputs($f, '			<num-bedrooms>'.$row['details.num-bedrooms'].'</num-bedrooms>'."\n");
				
			if(isset($row['details.num-full-bathrooms']) && !empty($row['details.num-full-bathrooms']))
				fputs($f, '			<num-full-bathrooms>'.$row['details.num-full-bathrooms'].'</num-full-bathrooms>'."\n");
			
			if(isset($row['details.num-half-bathrooms']) && !empty($row['details.num-half-bathrooms']))
				fputs($f, '			<num-half-bathrooms>'.$row['details.num-half-bathrooms'].'</num-half-bathrooms>'."\n");
			
			if(isset($row['details.num-bathrooms']) && !empty($row['details.num-bathrooms']))
				fputs($f, '			<num-bathrooms>'.$row['details.num-bathrooms'].'</num-bathrooms>'."\n");
			
			if(isset($row['details.lot-size']) && !empty($row['details.lot-size']))
				fputs($f, '			<lot-size>'.$row['details.lot-size'].'</lot-size>'."\n");
			
			if(isset($row['details.living-area-square-feet']) && !empty($row['details.living-area-square-feet']))
				fputs($f, '			<living-area-square-feet>'.$row['details.living-area-square-feet'].'</living-area-square-feet>'."\n");
				
			if(isset($row['details.date-listed']) && !strtotime($row['details.date-listed']))
				fputs($f, '			<date-listed>'.date('Y-m-d', strtotime($row['details.date-listed'])).'</date-listed>'."\n");
			
			if(isset($row['details.date-available']) && !strtotime($row['details.date-available']))
				fputs($f, '			<date-available>'.date('Y-m-d', strtotime($row['details.date-available'])).'</date-available>'."\n");
			
			if(isset($row['details.date-sold']) && !strtotime($row['details.date-sold']))	
				fputs($f, '			<date-sold>'.$row['details.date-sold'].'</date-sold>'."\n");
			
			if(isset($row['details.sale-price']) && !empty($row['details.sale-price']))
				fputs($f, '			<sale-price>'.$row['details.sale-price'].'</sale-price>'."\n");
			
			if(isset($row['details.property-type']) && in_array($row['details.property-type'], $this->property_types))
				fputs($f, '			<property-type>'.$row['details.property-type'].'</property-type>'."\n");
			
			if(isset($row['details.description']) && !empty($row['details.description']))
				fputs($f, '			<description>'.str_replace($this->search, $this->replace, $row['details.description']).'</description>'."\n");
				
			if(isset($row['details.mlsId']) && !empty($row['details.mlsId']))	
				fputs($f, '			<mlsId>'.str_replace($this->search, $this->replace, $row['details.mlsId']).'</mlsId>'."\n");
				
			if(isset($row['details.mlsName']) && !empty($row['details.mlsName']))	
				fputs($f, '			<mlsName>'.str_replace($this->search, $this->replace, $row['details.mlsName']).'</mlsName>'."\n");
				
			fputs($f, '			<provider-listingid>'.str_replace($this->search, $this->replace, $row['details.provider-listingid']).'</provider-listingid>'."\n");
			fputs($f, '		</details>'."\n");			
			
			if(isset($row['landing-page.lp-url']) && !empty($row['landing-page.lp-url'])){
				fputs($f, '		<landing-page>'."\n");
				fputs($f, '			<lp-url>'.str_replace($this->search, $this->replace, $row['landing-page.lp-url']).'</lp-url>'."\n");
				fputs($f, '		</landing-page>'."\n");
			}
			
			if(isset($row['listing-type']) && in_array($row['listing-type'], $this->listing_types))
				fputs($f, '		<listing-type>'.$row['listing-type'].'</listing-type>'."\n");
			
			if(isset($row['status']) && in_array($row['status'], $this->statuses))
				fputs($f, '		<status>'.$row['status'].'</status>'."\n");
			
			if(isset($row['foreclosure-status']) && in_array($row['foreclosure-status'], $this->foreclosure_statuses))
				fputs($f, '		<foreclosure-status>'.$row['foreclosure-status'].'</foreclosure-status>'."\n");
				
			fputs($f, '		<site>'."\n");
			fputs($f, '			<site-url>'.str_replace($this->search, $this->replace, preg_replace('/http(s)?:\/\//', '', SITE_SERVER)).'</site-url>'."\n");
			
			if(isset($row['site.site-name']) && !empty($row['site.site-name']))
				fputs($f, '			<site-name>'.str_replace($this->search, $this->replace, $row['site.site-name']).'</site-name>'."\n");
				
			fputs($f, '		</site>'."\n");			
			fputs($f, '		<rental-terms>'."\n");
			
			if(isset($row['rental-terms.price-term']) && in_array($row['rental-terms.price-term'], $this->price_terms))
				fputs($f, '			<price-term>'.$row['rental-terms.price-term'].'</price-term>'."\n");
			
			if(isset($row['rental-terms.rental-type']) && in_array($row['rental-terms.rental-type'], $this->rental_types))
				fputs($f, '			<rental-type>'.$row['rental-terms.rental-type'].'</rental-type>'."\n");
				
			if(isset($row['rental-terms.lease-type']) && in_array($row['rental-terms.lease-type'], $this->lease_types))
				fputs($f, '			<lease-type>'.$row['rental-terms.lease-type'].'</lease-type>'."\n");
			
			if(isset($row['rental-terms.lease-min-length-months']) && !empty($row['rental-terms.lease-min-length-months']))
				fputs($f, '			<lease-min-length-months>'.$row['rental-terms.lease-min-length-months'].'</lease-min-length-months>'."\n");
			
			if(isset($row['rental-terms.lease-max-length-months']) && !empty($row['rental-terms.lease-max-length-months']))
				fputs($f, '			<lease-max-length-months>'.$row['rental-terms.lease-max-length-months'].'</lease-max-length-months>'."\n");
			
			if(isset($row['rental-terms.lease-periods']) && !empty($row['rental-terms.lease-periods'])){
				fputs($f, '			<lease-periods>'."\n");
				
				foreach((array)$row['rental-terms.lease-periods'] as $lease_period)
					fputs($f, '				<lease-period>'.str_replace($this->search, $this->replace, $lease_period).'</lease-period>'."\n");
					
				fputs($f, '			</lease-periods>'."\n");
			}
			
			if(isset($row['rental-terms.lease-details']) && !empty($row['rental-terms.lease-details']))
				fputs($f, '			<lease-details>'.str_replace($this->search, $this->replace, $row['rental-terms.lease-details']).'</lease-details>'."\n");
			
			if(isset($row['rental-terms.security-deposit']) && !empty($row['rental-terms.security-deposit']))
				fputs($f, '			<security-deposit>'.str_replace($this->search, $this->replace, $row['rental-terms.security-deposit']).'</security-deposit>'."\n");
			
			if(isset($row['rental-terms.security-deposit-description']) && !empty($row['rental-terms.security-deposit-description']))
				fputs($f, '			<security-deposit-description>'.str_replace($this->search, $this->replace, $row['rental-terms.security-deposit-description']).'</security-deposit-description>'."\n");
			
			if(isset($row['rental-terms.application-fee']) && !empty($row['rental-terms.application-fee']))
				fputs($f, '			<application-fee>'.$row['rental-terms.application-fee'].'</application-fee>'."\n");
			
			if(isset($row['rental-terms.application-fee-description']) && !empty($row['rental-terms.application-fee-description']))
				fputs($f, '			<application-fee-description>'.str_replace($this->search, $this->replace, $row['application-fee-description']).'</application-fee-description>'."\n");
				
			if(isset($row['rental-terms.credit-cards-accepted']))	
				fputs($f, '			<credit-cards-accepted>'.($row['rental-terms.credit-cards-accepted'] ? 'yes' : 'no').'</credit-cards-accepted>'."\n");

			$credit_cards = array_intersect((array)$row['rental-terms.credit-cards'], $this->credit_cards);
			if(isset($row['rental-terms.credit-cards']) && !empty($credit_cards)){
				fputs($f, '			<credit-cards>'."\n");
				
				foreach($credit_cards as $credit_card)
					fputs($f, '				<credit-card>'.$credit_card.'</credit-card>'."\n");
				
				fputs($f, '			</credit-cards>'."\n");
			}
			
			fputs($f, '			<pets>'."\n");
			
			if(isset($row['rental-terms.pets.small-dogs-allowed']))	
				fputs($f, '				<small-dogs-allowed>'.($row['rental-terms.pets.small-dogs-allowed'] ? 'yes' : 'no').'</small-dogs-allowed>'."\n");
			
			if(isset($row['rental-terms.pets.large-dogs-allowed']))
				fputs($f, '				<large-dogs-allowed>'.($row['rental-terms.pets.large-dogs-allowed'] ? 'yes' : 'no').'</large-dogs-allowed>'."\n");
			
			if(isset($row['rental-terms.pets.cats-allowed']))
				fputs($f, '				<cats-allowed>'.($row['rental-terms.pets.cats-allowed'] ? 'yes' : 'no').'</cats-allowed>'."\n");
		
			if(isset($row['rental-terms.pets.pet-other-allowed']))
				fputs($f, '				<pet-other-allowed>'.($row['rental-terms.pets.pet-other-allowed'] ? 'yes' : 'no').'</pet-other-allowed>'."\n");
			
			if(isset($row['rental-terms.pets.max-pets']) && !empty($row['rental-terms.pets.max-pets']))
				fputs($f, '				<max-pets>'.$row['rental-terms.pets.max-pets'].'</max-pets>'."\n");
				
			if(isset($row['rental-terms.pets.pet-deposit']) && !empty($row['rental-terms.pets.pet-deposit']))
				fputs($f, '				<pet-deposit>'.str_replace($this->search, $this->replace, $row['rental-terms.pets.pet-deposit']).'</pet-deposit>'."\n");
			
			if(isset($row['rental-terms.pets.pet-fee']) && !empty($row['rental-terms.pets.pet-fee']))
				fputs($f, '				<pet-fee>'.$row['rental-terms.pets.pet-fee'].'</pet-fee>'."\n");
			
			if(isset($row['rental-terms.pets.pet-rent']) && !empty($row['rental-terms.pets.pet-rent']))
				fputs($f, '				<pet-rent>'.str_replace($this->search, $this->replace, $row['rental-terms.pets.pet-rent']).'</pet-rent>'."\n");
			
			if(isset($row['rental-terms.pets.pet-weight']) && !empty($row['rental-terms.pets.pet-weight']))
				fputs($f, '				<pet-weight>'.$row['rental-terms.pets.pet-weight'].'</pet-weight>'."\n");
			
			if(isset($row['rental-terms.pets.pet-comments']) && !empty($row['rental-terms.pets.pet-comments'])){
				fputs($f, '				<pet-comments>'."\n");
				
				foreach($row['rental-terms.pets.pet-comments'] as $pet_comment)
					fputs($f, '					<pet-comment>'.str_replace($this->search, $this->replace, $pet_comment).'</pet-comment>'."\n");
					
				fputs($f, '				</pet-comments>'."\n");
			}
			
			fputs($f, '			</pets>'."\n");
			
			fputs($f, '			<utilities-included>'."\n");
			
			if(isset($row['rental-terms.utilities-included.landlord-pays-aircon']))
				fputs($f, '				<landlord-pays-aircon>'.($row['rental-terms.utilities-included.landlord-pays-aircon'] ? 'yes' : 'no').'</landlord-pays-aircon>'."\n");

			if(isset($row['rental-terms.utilities-included.landlord-pays-broadbandinternet']))
				fputs($f, '				<landlord-pays-broadbandinternet>'.($row['rental-terms.utilities-included.landlord-pays-broadbandinternet'] ? 'yes' : 'no').'</landlord-pays-broadbandinternet>'."\n");
			
			if(isset($row['rental-terms.utilities-included.landlord-pays-cable']))
				fputs($f, '				<landlord-pays-cable>'.($row['rental-terms.utilities-included.landlord-pays-cable'] ? 'yes' : 'no').'</landlord-pays-cable>'."\n");
				
			if(isset($row['rental-terms.utilities-included.landlord-pays-electric']))	
				fputs($f, '				<landlord-pays-electric>'.($row['rental-terms.utilities-included.landlord-pays-electric'] ? 'yes' : 'no').'</landlord-pays-electric>'."\n");
			
			if(isset($row['rental-terms.utilities-included.landlord-pays-gas']))	
				fputs($f, '				<landlord-pays-gas>'.($row['rental-terms.utilities-included.landlord-pays-gas'] ? 'yes' : 'no').'</landlord-pays-gas>'."\n");

			if(isset($row['rental-terms.utilities-included.landlord-pays-heat']))	
				fputs($f, '				<landlord-pays-heat>'.($row['rental-terms.utilities-included.landlord-pays-heat'] ? 'yes' : 'no').'</landlord-pays-heat>'."\n");
				
			if(isset($row['rental-terms.utilities-included.landlord-pays-hotwater']))
				fputs($f, '				<landlord-pays-hotwater>'.($row['rental-terms.utilities-included.landlord-pays-hotwater'] ? 'yes' : 'no').'</landlord-pays-hotwater>'."\n");
			
			if(isset($row['rental-terms.utilities-included.landlord-pays-satellite']))
				fputs($f, '				<landlord-pays-satellite>'.($row['rental-terms.utilities-included.landlord-pays-satellite'] ? 'yes' : 'no').'</landlord-pays-satellite>'."\n");
			
			if(isset($row['rental-terms.utilities-included.landlord-pays-sewer']))
				fputs($f, '				<landlord-pays-sewer>'.($row['rental-terms.utilities-included.landlord-pays-sewer'] ? 'yes' : 'no').'</landlord-pays-sewer>'."\n");
			
			if(isset($row['rental-terms.utilities-included.landlord-pays-telephone']))
				fputs($f, '				<landlord-pays-telephone>'.($row['rental-terms.utilities-included.landlord-pays-telephone'] ? 'yes' : 'no').'</landlord-pays-telephone>'."\n");
			
			if(isset($row['rental-terms.utilities-included.landlord-pays-trash']))
				fputs($f, '				<landlord-pays-trash>'.($row['rental-terms.utilities-included.landlord-pays-trash'] ? 'yes' : 'no').'</landlord-pays-trash>'."\n");
				
			if(isset($row['rental-terms.utilities-included.landlord-pays-water']))
				fputs($f, '				<landlord-pays-water>'.($row['rental-terms.utilities-included.landlord-pays-water'] ? 'yes' : 'no').'</landlord-pays-water>'."\n");
				
			if(isset($row['rental-terms.utilities-included.landlord-utilities-portion-included']) && !empty($row['rental-terms.utilities-included.landlord-utilities-portion-included']))
				fputs($f, '				<landlord-utilities-portion-included>'.str_replace($this->search, $this->replace, $row['rental-terms.utilities-included.landlord-utilities-portion-included']).'</landlord-utilities-portion-included>'."\n");

			if(isset($row['rental-terms.utilities-included.utilities-comments']) && !empty($row['rental-terms.utilities-included.utilities-comments'])){
				fputs($f, '				<utilities-comments>'."\n");
				
				foreach((array)$row['rental-terms.utilities-included.utilities-comments'] as $utilities_comment)
					fputs($f, '					<utilities-comment>'.str_replace($this->search, $this->replace, $utilities_comment).'</utilities-comment>'."\n");
				
				fputs($f, '				</utilities-comments>'."\n");
			}
			
			fputs($f, '			</utilities-included>'."\n");
			
			if(isset($row['rental-terms.property-manager-on-site']))
				fputs($f, '			<property-manager-on-site>'.($row['rental-terms.property-manager-on-site'] ? 'yes' : 'no').'</property-manager-on-site>'."\n");
			
			if(isset($row['rental-terms.rent-control']))
				fputs($f, '			<rent-control>'.($row['rental-terms.rent-control'] ? 'yes' : 'no').'</rent-control>'."\n");
			
			if(isset($row['rental-terms.subletting-allowed']))
				fputs($f, '			<subletting-allowed>'.($row['rental-terms.subletting-allowed'] ? 'yes' : 'no').'</subletting-allowed>'."\n");
				
			fputs($f, '		</rental-terms>'."\n");
			
			if(isset($row['pictures']) && !empty($row['pictures'])){
				fputs($f, '		<pictures>'."\n");
				
				foreach($row['pictures'] as $picture_url){
					fputs($f, '			<picture>'."\n");
					fputs($f, '				<picture-url>'.str_replace($this->search, $this->replace, $picture_url).'</picture-url>'."\n");
					//fputs($f, '				<picture-caption>'.$picture['picture-caption'].'</picture-caption>'."\n");
					//fputs($f, '				<picture-description>'.$picture['picture-description'].'</picture-description>'."\n");
					//fputs($f, '				<picture-seq-number>'.$picture['picture-seq-number'].'</picture-seq-number>'."\n");
					fputs($f, '			</picture>'."\n");
				}
				
				fputs($f, '		</pictures>'."\n");
			}
			
			if(isset($row['virtual-tours']) && !empty($row['virtual-tours'])){
				fputs($f, '		<virtual-tours>'."\n");
				
				foreach($row['virtual-tours'] as $virtual_tour_url){
					fputs($f, '			<virtual-tour>'."\n");
					fputs($f, '				<virtual-tour-url>'.str_replace($this->search, $this->replace, $virtual_tour_url).'</virtual-tour-url>'."\n");
					//fputs($f, '				<virtual-tour-caption>'.$virtual_tour['virtual-tour-caption'].'</virtual-tour-caption>'."\n");
					//fputs($f, '				<virtual-tour-description>'.$virtual_tour['virtual-tour-description'].'</virtual-tour-description>'."\n");
					//fputs($f, '				<virtual-tour-seq-number>'.$virtual_tour['virtual-tour-seq-number'].'</virtual-tour-seq-number>'."\n");
					fputs($f, '			</virtual-tour>'."\n");
				}
				
				fputs($f, '		</virtual-tours>'."\n");
			}
			
			if(isset($row['videos']) && !empty($row['videos'])){
				fputs($f, '		<videos>'."\n");
				
				foreach($row['videos'] as $video_url){
					fputs($f, '			<video>'."\n");
					fputs($f, '				<video-url>'.str_replace($this->search, $this->replace, $video_url).'</video-url>'."\n");
					//fputs($f, '				<video-caption>'.$video['video-caption'].'</video-caption>'."\n");
					//fputs($f, '				<video-description>'.$video['video-description'].'</video-description>'."\n");
					//fputs($f, '				<video-seq-number>'.$video['video-seq-number'].'</video-seq-number>'."\n");
					fputs($f, '			</video>'."\n");
				}
				
				fputs($f, '		</videos>'."\n");
			}	
				
			if(isset($row['floorplan-layouts']) && !empty($row['floorplan-layouts'])){
				fputs($f, '		<floorplan-layouts>'."\n");
				
				foreach($row['floorplan-layouts'] as $floorplan_layout_url){
					fputs($f, '			<floorplan-layout>'."\n");
					fputs($f, '				<floorplan-layout-url>'.str_replace($this->search, $this->replace, $floorplan_layout_url).'</floorplan-layout-url>'."\n");
					//fputs($f, '				<floorplan-layout-caption>'.$row['floorplan-layout-caption'].'</floorplan-layout-caption>'."\n");
					//fputs($f, '				<floorplan-layout-description>'.$row['floorplan-layout-description'].'</floorplan-layout-description>'."\n");
					//fputs($f, '				<floorplan-layout-seq-number>'.$row['floorplan-layout-seq-number'].'</floorplan-layout-seq-number>'."\n");
					//fputs($f, '			</floorplan-layout>'."\n");
				}
				
				fputs($f, '		</floorplan-layouts>'."\n");
			}
			
			fputs($f, '		<agent>'."\n");
			
			if(isset($row['agent.agent-name']) && !empty($row['agent.agent-name']))
				fputs($f, '			<agent-name>'.str_replace($this->search, $this->replace, $row['agent.agent-name']).'</agent-name>'."\n");
			
			if(isset($row['agent.agent-phone']) && !empty($row['agent.agent-phone']))
				fputs($f, '			<agent-phone>'.str_replace($this->search, $this->replace, $row['agent.agent-phone']).'</agent-phone>'."\n");
			
			if(isset($row['agent.agent-email']) && !empty($row['agent.agent-email']))
				fputs($f, '			<agent-email>'.str_replace($this->search, $this->replace, $row['agent.agent-email']).'</agent-email>'."\n");
				
			if(isset($row['agent.agent-alternate-email']) && !empty($row['agent.agent-alternate-email']))	
				fputs($f, '			<agent-alternate-email>'.str_replace($this->search, $this->replace, $row['agent.agent-alternate-email']).'</agent-alternate-email>'."\n");
			
			if(isset($row['agent.agent-picture-url']) && !empty($row['agent.agent-picture-url']))	
				fputs($f, '			<agent-picture-url>'.str_replace($this->search, $this->replace, $row['agent.agent-picture-url']).'</agent-picture-url>'."\n");
			
			if(isset($row['agent.agent-id']) && !empty($row['agent.agent-id']))	
				fputs($f, '			<agent-id>'.$row['agent.agent-id'].'</agent-id>'."\n");
				
			fputs($f, '		</agent>'."\n");
			fputs($f, '		<brokerage>'."\n");
			
			if(isset($row['brokerage.brokerage-name']) && !empty($row['brokerage.brokerage-name']))	
				fputs($f, '			<brokerage-name>'.str_replace($this->search, $this->replace, $row['brokerage.brokerage-name']).'</brokerage-name>'."\n");
			
			if(isset($row['brokerage.brokerage-broker-name']) && !empty($row['brokerage.brokerage-broker-name']))	
				fputs($f, '			<brokerage-broker-name>'.str_replace($this->search, $this->replace, $row['brokerage.brokerage-broker-name']).'</brokerage-broker-name>'."\n");
			
			if(isset($row['brokerage.brokerage-id']) && !empty($row['brokerage.brokerage-id']))	
				fputs($f, '			<brokerage-id>'.$row['brokerage.brokerage-id'].'</brokerage-id>'."\n");
			
			if(isset($row['brokerage.brokerage-mls-code']) && !empty($row['brokerage.brokerage-mls-code']))	
				fputs($f, '			<brokerage-mls-code>'.str_replace($this->search, $this->replace, $row['brokerage.brokerage-mls-code']).'</brokerage-mls-code>'."\n");
			
			if(isset($row['brokerage.brokerage-phone']) && !empty($row['brokerage.brokerage-phone']))
				fputs($f, '			<brokerage-phone>'.str_replace($this->search, $this->replace, $row['brokerage.brokerage-phone']).'</brokerage-phone>'."\n");
			
			if(isset($row['brokerage.brokerage-email']) && !empty($row['brokerage.brokerage-email']))
				fputs($f, '			<brokerage-email>'.str_replace($this->search, $this->replace, $row['brokerage.brokerage-email']).'</brokerage-email>'."\n");
			
			if(isset($row['brokerage.brokerage-website']) && !empty($row['brokerage.brokerage-website']))
				fputs($f, '			<brokerage-website>'.str_replace($this->search, $this->replace, $row['brokerage.brokerage-website']).'</brokerage-website>'."\n");
			
			if(isset($row['brokerage.brokerage-logo-url']) && !empty($row['brokerage.brokerage-logo-url']))
				fputs($f, '			<brokerage-logo-url>'.str_replace($this->search, $this->replace, $row['brokerage.brokerage-logo-url']).'</brokerage-logo-url>'."\n");
				
			fputs($f, '			<brokerage-address>'."\n");
			
			if(isset($row['brokerage.brokerage-address.brokerage-street-address']) && !empty($row['brokerage.brokerage-address.brokerage-street-address']))
				fputs($f, '				<brokerage-street-address>'.str_replace($this->search, $this->replace, $row['brokerage.brokerage-address.brokerage-street-address']).'</brokerage-street-address>'."\n");
			
			if(isset($row['brokerage.brokerage-address.brokerage-city-name']) && !empty($row['brokerage.brokerage-address.brokerage-city-name']))
				fputs($f, '				<brokerage-city-name>'.str_replace($this->search, $this->replace, $row['brokerage.brokerage-address.brokerage-city-name']).'</brokerage-city-name>'."\n");
			
			if(isset($row['brokerage.brokerage-address.brokerage-zipcode']) && !empty($row['brokerage.brokerage-address.brokerage-zipcode']))
				fputs($f, '				<brokerage-zipcode>'.str_replace($this->search, $this->replace, $row['brokerage.brokerage-address.brokerage-zipcode']).'</brokerage-zipcode>'."\n");
				
			if(isset($row['brokerage.brokerage-address.brokerage-state-code']) && !empty($row['brokerage.brokerage-address.brokerage-state-code']))	
				fputs($f, '				<brokerage-state-code>'.str_replace($this->search, $this->replace, $row['brokerage.brokerage-address.brokerage-state-code']).'</brokerage-state-code>'."\n");
			
			fputs($f, '			</brokerage-address>'."\n");
			fputs($f, '		</brokerage>'."\n");
			fputs($f, '		<office>'."\n");
			
			if(isset($row['office.office-name']) && !empty($row['office.office-name']))
				fputs($f, '			<office-name>'.str_replace($this->search, $this->replace, $row['office.office-name']).'</office-name>'."\n");
				
			if(isset($row['office.office-id']) && !empty($row['office.office-id']))
				fputs($f, '			<office-id>'.str_replace($this->search, $this->replace, $row['office.office-id']).'</office-id>'."\n");
				
			if(isset($row['office.office-mls-code']) && !empty($row['office.office-mls-code']))	
				fputs($f, '			<office-mls-code>'.str_replace($this->search, $this->replace, $row['office.office-mls-code']).'</office-mls-code>'."\n");
			
			if(isset($row['office.office-broker-id']) && !empty($row['office.office-broker-id']))	
				fputs($f, '			<office-broker-id>'.str_replace($this->search, $this->replace, $row['office.office-broker-id']).'</office-broker-id>'."\n");
			
			if(isset($row['office.office-phone']) && !empty($row['office.office-phone']))	
				fputs($f, '			<office-phone>'.str_replace($this->search, $this->replace, $row['office.office-phone']).'</office-phone>'."\n");
			
			if(isset($row['office.office-email']) && !empty($row['office.office-email']))	
				fputs($f, '			<office-email>'.str_replace($this->search, $this->replace, $row['office.office-email']).'</office-email>'."\n");
			
			if(isset($row['office.office-website']) && !empty($row['office.office-website']))	
				fputs($f, '			<office-website>'.str_replace($this->search, $this->replace, $row['office.office-website']).'</office-website>'."\n");
				
			fputs($f, '		</office>'."\n");
			fputs($f, '		<franchise>'."\n");
			
			if(isset($row['franchise.franchise-name']) && !empty($row['franchise.franchise-name']))	
				fputs($f, '			<franchise-name>'.str_replace($this->search, $this->replace, $row['franchise.franchise-name']).'</franchise-name>'."\n");
			
			if(isset($row['franchise.franchise-phone']) && !empty($row['franchise.franchise-phone']))	
				fputs($f, '			<franchise-phone>'.str_replace($this->search, $this->replace, $row['franchise.franchise-phone']).'</franchise-phone>'."\n");
			
			if(isset($row['franchise.franchise-email']) && !empty($row['franchise.franchise-email']))	
				fputs($f, '			<franchise-email>'.str_replace($this->search, $this->replace, $row['franchise.franchise-email']).'</franchise-email>'."\n");
			
			if(isset($row['franchise.franchise-website']) && !empty($row['franchise.franchise-website']))	
				fputs($f, '			<franchise-website>'.str_replace($this->search, $this->replace, $row['franchise.franchise-website']).'</franchise-website>'."\n");
			
			if(isset($row['franchise.franchise-logo-url']) && !empty($row['franchise.franchise-logo-url']))	
				fputs($f, '			<franchise-logo-url>'.str_replace($this->search, $this->replace, $row['franchise.franchise-logo-url']).'</franchise-logo-url>'."\n");
				
			fputs($f, '		</franchise>'."\n");
			fputs($f, '		<builder>'."\n");
			
			if(isset($row['builder.builder-id']) && !empty($row['builder.builder-id']))	
				fputs($f, '			<builder-id>'.str_replace($this->search, $this->replace, $row['builder.builder-id']).'</builder-id>'."\n");
			
			if(isset($row['builder.builder-name']) && !empty($row['builder.builder-name']))	
				fputs($f, '			<builder-name>'.str_replace($this->search, $this->replace, $row['builder.builder-name']).'</builder-name>'."\n");
			
			if(isset($row['builder.builder-phone']) && !empty($row['builder.builder-phone']))	
				fputs($f, '			<builder-phone>'.str_replace($this->search, $this->replace, $row['builder.builder-phone']).'</builder-phone>'."\n");
			
			if(isset($row['builder.builder-email']) && !empty($row['builder.builder-email']))	
				fputs($f, '			<builder-email>'.str_replace($this->search, $this->replace, $row['builder.builder-email']).'</builder-email>'."\n");
				
			if(isset($row['builder.builder-lead-email']) && !empty($row['builder.builder-lead-email']))		
				fputs($f, '			<builder-lead-email>'.str_replace($this->search, $this->replace, $row['builder.builder-lead-email']).'</builder-lead-email>'."\n");
			
			if(isset($row['builder.builder-website']) && !empty($row['builder.builder-website']))	
				fputs($f, '			<builder-website>'.str_replace($this->search, $this->replace, $row['builder.builder-website']).'</builder-website>'."\n");
				
			if(isset($row['builder.builder-logo-url']) && !empty($row['builder.builder-logo-url']))		
				fputs($f, '			<builder-logo-url>'.str_replace($this->search, $this->replace, $row['builder.builder-logo-url']).'</builder-logo-url>'."\n");
				
			fputs($f, '			<builder-address>'."\n");
			
			if(isset($row['builder.builder-address.builder-street-address']) && !empty($row['builder.builder-address.builder-street-address']))	
				fputs($f, '				<builder-street-address>'.str_replace($this->search, $this->replace, $row['builder.builder-address.builder-street-address']).'</builder-street-address>'."\n");
			
			if(isset($row['builder.builder-address.builder-city-name']) && !empty($row['builder.builder-address.builder-city-name']))	
				fputs($f, '				<builder-city-name>'.str_replace($this->search, $this->replace, $row['builder.builder-address.builder-city-name']).'</builder-city-name>'."\n");
			
			if(isset($row['builder.builder-address.builder-zipcode']) && !empty($row['builder.builder-address.builder-zipcode']))	
				fputs($f, '				<builder-zipcode>'.str_replace($this->search, $this->replace, $row['builder.builder-address.builder-zipcode']).'</builder-zipcode>'."\n");
				
			if(isset($row['builder.builder-address.builder-state-code']) && !empty($row['builder.builder-address.builder-state-code']))	
				fputs($f, '				<builder-state-code>'.str_replace($this->search, $this->replace, $row['builder.builder-address.builder-state-code']).'</builder-state-code>'."\n");
			
			fputs($f, '			</builder-address>'."\n");
			fputs($f, '		</builder>'."\n");
			fputs($f, '		<property-manager>'."\n");
			
			if(isset($row['property-manager.property-manager-name']) && !empty($row['property-manager.property-manager-name']))		
				fputs($f, '			<property-manager-name>'.str_replace($this->search, $this->replace, $row['property-manager.property-manager-name']).'</property-manager-name>'."\n");
			
			if(isset($row['property-manager.property-management-company-name']) && !empty($row['property-manager.property-management-company-name']))		
				fputs($f, '			<property-management-company-name>'.str_replace($this->search, $this->replace, $row['property-manager.property-management-company-name']).'</property-management-company-name>'."\n");
			
			if(isset($row['property-manager.property-manager-office-hours']) && !empty($row['property-manager.property-manager-office-hours'])){
				fputs($f, '			<property-manager-office-hours>'."\n");
				
				foreach($row['property-manager.property-manager-office-hours'] as $property_manager_office_hours){
					if(isset($property_manager_office_hours['day-of-the-week']) && in_array($property_manager_office_hours['day-of-the-week'], $this->days_of_the_week)){
						fputs($f, '			<office-day>'."\n");
						fputs($f, '				<day-of-the-week>'.$property_manager_office_hours['day-of-the-week'].'</day-of-the-week>'."\n");
						fputs($f, '				<office-start-time>'.str_replace($this->search, $this->replace, $property_manager_office_hours['office-start-time']).'</office-start-time>'."\n");
						fputs($f, '				<office-end-time>'.str_replace($this->search, $this->replace, $property_manager_office_hours['office-end-time']).'</office-end-time>'."\n");
						fputs($f, '				<comment>'.str_replace($this->search, $this->replace, $property_manager_office_hours['comment']).'</comment>'."\n");
						fputs($f, '			</office-day>'."\n");
					}
				}
				
				fputs($f, '			</property-manager-office-hours>'."\n");
			}

			if(isset($row['property-manager.property-manager-phone']) && !empty($row['property-manager.property-manager-phone']))		
				fputs($f, '			<property-manager-phone>'.str_replace($this->search, $this->replace, $row['property-manager.property-manager-phone']).'</property-manager-phone>'."\n");
			
			if(isset($row['property-manager.property-manager-email']) && !empty($row['property-manager.property-manager-email']))		
				fputs($f, '			<property-manager-email>'.str_replace($this->search, $this->replace, $row['property-manager.property-manager-email']).'</property-manager-email>'."\n");
			
			if(isset($row['property-manager.property-manager-lead-email']) && !empty($row['property-manager.property-manager-lead-email']))		
				fputs($f, '			<property-manager-lead-email>'.str_replace($this->search, $this->replace, $row['property-manager.property-manager-lead-email']).'</property-manager-lead-email>'."\n");
			
			if(isset($row['property-manager.property-manager-website']) && !empty($row['property-manager.property-manager-website']))		
				fputs($f, '			<property-manager-website>'.str_replace($this->search, $this->replace, $row['property-manager.property-manager-website']).'</property-manager-website>'."\n");
			
			if(isset($row['property-manager.property-manager-logo-url']) && !empty($row['property-manager.property-manager-logo-url']))		
				fputs($f, '			<property-manager-logo-url>'.str_replace($this->search, $this->replace, $row['property-manager.property-manager-logo-url']).'</property-manager-logo-url>'."\n");
				
			fputs($f, '		</property-manager>'."\n");
			fputs($f, '		<community>'."\n");
			
			if(isset($row['community.community-building']) && !empty($row['community.community-building']))		
				fputs($f, '			<community-building>'.str_replace($this->search, $this->replace, $row['community.community-building']).'</community-building>'."\n");
				
			if(isset($row['community.community-id']) && !empty($row['community.community-id']))			
				fputs($f, '			<community-id>'.str_replace($this->search, $this->replace, $row['community.community-id']).'</community-id>'."\n");
				
			if(isset($row['community.community-name']) && !empty($row['community.community-name']))				
				fputs($f, '			<community-name>'.str_replace($this->search, $this->replace, $row['community.community-name']).'</community-name>'."\n");
			
			if(isset($row['community.community-phone']) && !empty($row['community.community-phone']))				
				fputs($f, '			<community-phone>'.str_replace($this->search, $this->replace, $row['community.community-phone']).'</community-phone>'."\n");
			
			if(isset($row['community.community-email']) && !empty($row['community.community-email']))				
				fputs($f, '			<community-email>'.str_replace($this->search, $this->replace, $row['community.community-email']).'</community-email>'."\n");
			
			if(isset($row['community.community-fax']) && !empty($row['community.community-fax']))				
				fputs($f, '			<community-fax>'.str_replace($this->search, $this->replace, $row['community.community-fax']).'</community-fax>'."\n");
				
			if(isset($row['community.community-description']) && !empty($row['community.community-description']))					
				fputs($f, '			<community-description>'.str_replace($this->search, $this->replace, $row['community.community-description']).'</community-description>'."\n");
			
			if(isset($row['community.community-website']) && !empty($row['community.community-website']))					
				fputs($f, '			<community-website>'.str_replace($this->search, $this->replace, $row['community.community-website']).'</community-website>'."\n");
				
			fputs($f, '			<community-address>'."\n");
			
			if(isset($row['community.community-address.community-street-address']) && !empty($row['community.community-address.community-street-address']))					
				fputs($f, '				<community-street-address>'.str_replace($this->search, $this->replace, $row['community.community-address.community-street-address']).'</community-street-address>'."\n");

			if(isset($row['community.community-address.community-city-name']) && !empty($row['community.community-address.community-city-name']))					
				fputs($f, '				<community-city-name>'.str_replace($this->search, $this->replace, $row['community.community-address.community-city-name']).'</community-city-name>'."\n");
				
			if(isset($row['community.community-address.community-zipcode']) && !empty($row['community.community-address.community-zipcode']))
				fputs($f, '				<community-zipcode>'.str_replace($this->search, $this->replace, $row['community.community-address.community-zipcode']).'</community-zipcode>'."\n");
			
			if(isset($row['community.community-address.community-state-code']) && !empty($row['community.community-address.community-state-code']))
				fputs($f, '				<community-state-code>'.str_replace($this->search, $this->replace, $row['community.community-address.community-state-code']).'</community-state-code>'."\n");
				
			fputs($f, '			</community-address>'."\n");
			
			if(isset($row['community.building-amenities']) && !empty($row['community.building-amenities'])){
				fputs($f, '			<building-amenities>'."\n");
				
				foreach($row['community.building-amenities'] as $building_amenity)
					fputs($f, '				<building-amenity>'.str_replace($this->search, $this->replace, $building_amenity).'</building-amenity>'."\n");
					
				fputs($f, '			</building-amenities>'."\n");
			}
			
			if(isset($row['community.community-amenities']) && !empty($row['community.community-amenities'])){
				fputs($f, '			<community-amenities>'."\n");
				
				foreach($row['community.community-amenities'] as $community_amenity)
					fputs($f, '				<community-amenity>'.str_replace($this->search, $this->replace, $community_amenity).'</community-amenity>'."\n");

				fputs($f, '			</community-amenities>'."\n");
			}

			fputs($f, '		</community>'."\n");

			fputs($f, '		<plan>'."\n");
			
			if(isset($row['plan.plan-id']) && !empty($row['plan.plan-id']))
				fputs($f, '			<plan-id>'.$row['plan.plan-id'].'</plan-id>'."\n");
				
			if(isset($row['plan.plan-name']) && !empty($row['plan.plan-name']))
				fputs($f, '			<plan-name>'.str_replace($this->search, $this->replace, $row['plan.plan-name']).'</plan-name>'."\n");
			
			if(isset($row['plan.plan-type']) && !empty($row['plan.plan-type']))
				fputs($f, '			<plan-type>'.str_replace($this->search, $this->replace, $row['plan.plan-type']).'</plan-type>'."\n");
			
			if(isset($row['plan.plan-base-price']) && !empty($row['plan.plan-base-price']))
				fputs($f, '			<plan-base-price>'.($row['plan.plan-base-price'] ? 'yes' : 'no').'</plan-base-price>'."\n");
				
			fputs($f, '		</plan>'."\n");
			fputs($f, '		<spec>'."\n");
			
			if(isset($row['spec.is-spec-home']) && !empty($row['spec.is-spec-home']))
				fputs($f, '			<is-spec-home>'.($row['spec.is-spec-home'] ? 'yes' : 'no').'</is-spec-home>'."\n");
			
			if(isset($row['spec.spec-id']) && !empty($row['spec.spec-id']))
				fputs($f, '			<spec-id>'.$row['spec.spec-id'].'</spec-id>'."\n");
				
			fputs($f, '		</spec>'."\n");
			fputs($f, '		<open-home>'."\n");
			
			if(isset($row['open-home.period1-date']) && !empty($row['open-home.period1-date']))
				fputs($f, '			<period1-date>'.str_replace($this->search, $this->replace, $row['open-home.period1-date']).'</period1-date>'."\n");
			
			if(isset($row['open-home.period1-start-time']) && !empty($row['open-home.period1-start-time']))
				fputs($f, '			<period1-start-time>'.str_replace($this->search, $this->replace, $row['open-home.period1-start-time']).'</period1-start-time>'."\n");
				
			if(isset($row['open-home.period1-end-time']) && !empty($row['open-home.period1-end-time']))	
				fputs($f, '			<period1-end-time>'.str_replace($this->search, $this->replace, $row['open-home.period1-end-time']).'</period1-end-time>'."\n");
			
			if(isset($row['open-home.period1-details']) && !empty($row['open-home.period1-details']))	
				fputs($f, '			<period1-details>'.str_replace($this->search, $this->replace, $row['open-home.period1-details']).'</period1-details>'."\n");
			
			if(isset($row['open-home.period2-date']) && !empty($row['open-home.period2-date']))
				fputs($f, '			<period2-date>'.str_replace($this->search, $this->replace, $row['open-home.period2-date']).'</period2-date>'."\n");
				
			if(isset($row['open-home.period2-start-time']) && !empty($row['open-home.period2-start-time']))
				fputs($f, '			<period2-start-time>'.str_replace($this->search, $this->replace, $row['open-home.period2-start-time']).'</period2-start-time>'."\n");
			
			if(isset($row['open-home.period2-end-time']) && !empty($row['open-home.period2-end-time']))	
				fputs($f, '			<period2-end-time>'.str_replace($this->search, $this->replace, $row['open-home.period2-end-time']).'</period2-end-time>'."\n");
				
			if(isset($row['open-home.period2-details']) && !empty($row['open-home.period2-details']))		
				fputs($f, '			<period2-details>'.str_replace($this->search, $this->replace, $row['open-home.period2-details']).'</period2-details>'."\n");
				
			if(isset($row['open-home.period3-date']) && !empty($row['open-home.period3-date']))	
				fputs($f, '			<period3-date>'.str_replace($this->search, $this->replace, $row['open-home.period3-date']).'</period3-date>'."\n");
			
			if(isset($row['open-home.period3-start-time']) && !empty($row['open-home.period3-start-time']))
				fputs($f, '			<period3-start-time>'.str_replace($this->search, $this->replace, $row['open-home.period3-start-time']).'</period3-start-time>'."\n");
			
			if(isset($row['open-home.period3-end-time']) && !empty($row['open-home.period3-end-time']))	
				fputs($f, '			<period3-end-time>'.str_replace($this->search, $this->replace, $row['open-home.period3-end-time']).'</period3-end-time>'."\n");
			
			if(isset($row['open-home.period3-details']) && !empty($row['open-home.period3-details']))		
				fputs($f, '			<period3-details>'.str_replace($this->search, $this->replace, $row['open-home.period3-details']).'</period3-details>'."\n");
				
			if(isset($row['open-home.period4-date']) && !empty($row['open-home.period4-date']))		
				fputs($f, '			<period4-date>'.str_replace($this->search, $this->replace, $row['open-home.period4-date']).'</period4-date>'."\n");
			
			if(isset($row['open-home.period4-start-time']) && !empty($row['open-home.period4-start-time']))
				fputs($f, '			<period4-start-time>'.str_replace($this->search, $this->replace, $row['open-home.period4-start-time']).'</period4-start-time>'."\n");
			
			if(isset($row['open-home.period4-end-time']) && !empty($row['open-home.period4-end-time']))	
				fputs($f, '			<period4-end-time>'.str_replace($this->search, $this->replace, $row['open-home.period4-end-time']).'</period4-end-time>'."\n");
				
			if(isset($row['open-home.period4-details']) && !empty($row['open-home.period4-details']))			
				fputs($f, '			<period4-details>'.str_replace($this->search, $this->replace, $row['open-home.period4-details']).'</period4-details>'."\n");
				
			fputs($f, '		</open-home>'."\n");
			
			if(isset($row['taxes']) && !empty($row['taxes'])){
				fputs($f, '		<taxes>'."\n");
				
				foreach($row['taxes'] as $tax){
					fputs($f, '			<tax>'."\n");
					fputs($f, '				<tax-type>'.str_replace($this->search, $this->replace, $tax['tax-type']).'</tax-type>'."\n");
					fputs($f, '				<tax-year>'.$tax['tax-year'].'</tax-year>'."\n");
					fputs($f, '				<tax-amount>'.$tax['tax-amount'].'</tax-amount>'."\n");
					fputs($f, '				<tax-description>'.str_replace($this->search, $this->replace, $tax['tax-description']).'</tax-description>'."\n");
					fputs($f, '			</tax>'."\n");
				}
				
				fputs($f, '		</taxes>'."\n");
			}
			
			fputs($f, '		<hoa-fees>'."\n");
			
			if(isset($row['hoa-fees.hoa-fee']) && !empty($row['hoa-fees.hoa-fee']))
				fputs($f, '			<hoa-fee>'.$row['hoa-fees.hoa-fee'].'</hoa-fee>'."\n");
			
			if(isset($row['hoa-fees.hoa-period']) && in_array($row['hoa-fees.hoa-period'], $this->hoa_periods))
				fputs($f, '			<hoa-period>'.$row['hoa-fees.hoa-period'].'</hoa-period>'."\n");
			
			if(isset($row['hoa-fees.hoa-description']) && !empty($row['hoa-fees.hoa-description']))
				fputs($f, '			<hoa-description>'.str_replace($this->search, $this->replace, $row['hoa-fees.hoa-description']).'</hoa-description>'."\n");
				
			fputs($f, '		</hoa-fees>'."\n");
	
			if(isset($row['additional-fees']) && !empty($row['additional-fees'])){
				fputs($f, '		<additional-fees>'."\n");
	
				foreach($row['additional-fees'] as $fee){
					fputs($f, '			<fee>'."\n");
					fputs($f, '				<fee-type>'.str_replace($this->search, $this->replace, $fee['fee-type']).'</fee-type>'."\n");
					fputs($f, '				<fee-amount>'.$fee['fee-amount'].'</fee-amount>'."\n");
					fputs($f, '				<fee-period>'.$fee['fee-period'].'</fee-period>'."\n");
					fputs($f, '				<fee-description>'.str_replace($this->search, $this->replace, $fee['fee-description']).'</fee-description>'."\n");
					fputs($f, '			</fee>'."\n");
				}
				
				fputs($f, '		</additional-fees>'."\n");
			}
			
			fputs($f, '		<schools>'."\n");
			fputs($f, '			<school-district>'."\n");
			
			if(isset($row['schools.school-district.elementary']) && !empty($row['schools.school-district.elementary']))
				fputs($f, '				<elementary>'.str_replace($this->search, $this->replace, $row['schools.school-district.elementary']).'</elementary>'."\n");
			
			if(isset($row['schools.school-district.middle']) && !empty($row['schools.school-district.middle']))
				fputs($f, '				<middle>'.str_replace($this->search, $this->replace, $row['schools.school-district.middle']).'</middle>'."\n");
			
			if(isset($row['schools.school-district.juniorhigh']) && !empty($row['schools.school-district.juniorhigh']))
				fputs($f, '				<juniorhigh>'.str_replace($this->search, $this->replace, $row['schools.school-district.juniorhigh']).'</juniorhigh>'."\n");
				
			if(isset($row['schools.school-district.high']) && !empty($row['schools.school-district.high']))	
				fputs($f, '				<high>'.str_replace($this->search, $this->replace, $row['schools.school-district.high']).'</high>'."\n");
			
			if(isset($row['schools.school-district.district-name']) && !empty($row['schools.school-district.district-name']))	
				fputs($f, '				<district-name>'.str_replace($this->search, $this->replace, $row['schools.school-district.district-name']).'</district-name>'."\n");
			
			if(isset($row['schools.school-district.district-website']) && !empty($row['schools.school-district.district-website']))				
				fputs($f, '				<district-website>'.str_replace($this->search, $this->replace, $row['schools.school-district.district-website']).'</district-website>'."\n");
			
			if(isset($row['schools.school-district.district-phone-number']) && !empty($row['schools.school-district.district-phone-number']))	
				fputs($f, '				<district-phone-number>'.str_replace($this->search, $this->replace, $row['schools.school-district.district-phone-number']).'</district-phone-number>'."\n");
				
			fputs($f, '			</school-district>'."\n");
			fputs($f, '		</schools>'."\n");

			fputs($f, '		<detailed-characteristics>'."\n");
			fputs($f, '			<appliances>'."\n");
			
			if(isset($row['detailed-characteristics.appliances.has-washer']))
				fputs($f, '				<has-washer>'.($row['detailed-characteristics.appliances.has-washer'] ? 'yes' : 'no').'</has-washer>'."\n");
			
			if(isset($row['detailed-characteristics.appliances.has-dryer']))
				fputs($f, '				<has-dryer>'.($row['detailed-characteristics.appliances.has-dryer'] ? 'yes' : 'no').'</has-dryer>'."\n");
			
			if(isset($row['detailed-characteristics.appliances.has-dishwasher']))
				fputs($f, '				<has-dishwasher>'.($row['detailed-characteristics.appliances.has-dishwasher'] ? 'yes' : 'no').'</has-dishwasher>'."\n");
			
			if(isset($row['detailed-characteristics.appliances.has-refrigerator']))
				fputs($f, '				<has-refrigerator>'.($row['detailed-characteristics.appliances.has-refrigerator'] ? 'yes' : 'no').'</has-refrigerator>'."\n");
				
			if(isset($row['detailed-characteristics.appliances.has-disposal']))	
				fputs($f, '				<has-disposal>'.($row['detailed-characteristics.appliances.has-disposal'] ? 'yes' : 'no').'</has-disposal>'."\n");
			
			if(isset($row['detailed-characteristics.appliances.has-microwave']))	
				fputs($f, '				<has-microwave>'.($row['detailed-characteristics.appliances.has-microwave'] ? 'yes' : 'no').'</has-microwave>'."\n");
				
			if(isset($row['detailed-characteristics.appliances.range-type']) && in_array($row['detailed-characteristics.appliances.range-type'], $this->range_types))		
				fputs($f, '				<range-type>'.$row['detailed-characteristics.appliances.range-type'].'</range-type>'."\n");
			
			if(isset($row['detailed-characteristics.appliances.additional-appliances']) && !empty($row['detailed-characteristics.appliances.additional-appliances'])){
				foreach($row['additional-appliance'] as $additional_appliance){
					fputs($f, '				<additional-appliance>'."\n");
					fputs($f, '					<additional-appliance-name>'.str_replace($this->search, $this->replace, $row['additional-appliance-name']).'</additional-appliance-name>'."\n");
					fputs($f, '					<additional-appliance-description>'.str_replace($this->search, $this->replace, $row['additional-appliance-description']).'</additional-appliance-description>'."\n");
					fputs($f, '				</additional-appliance>'."\n");
				}
			}
			
			if(isset($row['detailed-characteristics.appliances.appliances-comments']) && !empty($row['detailed-characteristics.appliances.appliances-comments']))		
				fputs($f, '				<appliances-comments>'.str_replace($this->search, $this->replace, $row['detailed-characteristics.appliances.appliances-comments']).'</appliances-comments>'."\n");
				
			fputs($f, '			</appliances>'."\n");		
			fputs($f, '			<cooling-systems>'."\n");
			
			if(isset($row['detailed-characteristics.cooling-systems.has-air-conditioning']))
				fputs($f, '				<has-air-conditioning>'.($row['detailed-characteristics.cooling-systems.has-air-conditioning'] ? 'yes' : 'no').'</has-air-conditioning>'."\n");
			
			if(isset($row['detailed-characteristics.cooling-systems.has-ceiling-fan']))
				fputs($f, '				<has-ceiling-fan>'.($row['detailed-characteristics.cooling-systems.has-ceiling-fan'] ? 'yes' : 'no').'</has-ceiling-fan>'."\n");
			
			if(isset($row['detailed-characteristics.cooling-systems.other-cooling']) && !empty($row['detailed-characteristics.cooling-systems.other-cooling']))
				fputs($f, '				<other-cooling>'.str_replace($this->search, $this->replace, $row['detailed-characteristics.cooling-systems.other-cooling']).'</other-cooling>'."\n");
			
			fputs($f, '			</cooling-systems>'."\n");
			fputs($f, '			<heating-systems>'."\n");
			
			if(isset($row['detailed-characteristics.heating-systems.has-fireplace']))
				fputs($f, '				<has-fireplace>'.($row['detailed-characteristics.heating-systems.has-fireplace'] ? 'yes' : 'no').'</has-fireplace>'."\n");
			
			if(isset($row['detailed-characteristics.heating-systems.fireplace-type']) && in_array($row['detailed-characteristics.heating-systems.fireplace-type'], $this->fireplace_types))
				fputs($f, '				<fireplace-type>'.$row['detailed-characteristics.heating-systems.fireplace-type'].'</fireplace-type>'."\n");
				
			if(isset($row['detailed-characteristics.heating-systems.heating-system']) && in_array($row['detailed-characteristics.heating-systems.heating-system'], $this->heating_systems))
				fputs($f, '				<heating-system>'.$row['detailed-characteristics.heating-systems.heating-system'].'</heating-system>'."\n");
			
			if(isset($row['detailed-characteristics.heating-systems.heating-fuel']) && in_array($row['detailed-characteristics.heating-systems.heating-fuel'], $this->heating_fuels))
				fputs($f, '				<heating-fuel>'.$row['detailed-characteristics.heating-systems.heating-fuel'].'</heating-fuel>'."\n");
			
			fputs($f, '			</heating-systems>'."\n");
			
			if(isset($row['detailed-characteristics.floor-coverings']) && !empty($row['detailed-characteristics.floor-coverings']))
				fputs($f, '			<floor-coverings>'.str_replace($this->search, $this->replace, $row['detailed-characteristics.floor-coverings']).'</floor-coverings>'."\n");
				
			if(isset($row['detailed-characteristics.total-unit-parking-spaces']) && !empty($row['detailed-characteristics.total-unit-parking-spaces']))
				fputs($f, '			<total-unit-parking-spaces>'.$row['detailed-characteristics.total-unit-parking-spaces'].'</total-unit-parking-spaces>'."\n");
				
			if(isset($row['detailed-characteristics.has-garage']))	
				fputs($f, '			<has-garage>'.($row['detailed-characteristics.has-garage'] ? 'yes' : 'no').'</has-garage>'."\n");
			
			if(isset($row['detailed-characteristics.garage-type']) && in_array($row['detailed-characteristics.garage-type'], $this->garage_types))	
				fputs($f, '			<garage-type>'.$row['detailed-characteristics.garage-type'].'</garage-type>'."\n");
			
			if(isset($row['detailed-characteristics.parking-types']) && in_array($row['detailed-characteristics.parking-types'], $this->parking_types)){
				fputs($f, '			<parking-types>'."\n");		
				fputs($f, '				<parking-type>'.$row['detailed-characteristics.parking-types'].'</parking-type>'."\n");
				fputs($f, '			</parking-types>'."\n");
			}
			
			if(isset($row['detailed-characteristics.has-assigned-parking-space']))	
				fputs($f, '			<has-assigned-parking-space>'.($row['detailed-characteristics.has-assigned-parking-space'] ? 'yes' : 'no').'</has-assigned-parking-space>'."\n");

			if(isset($row['detailed-characteristics.parking-space-fee']) && in_array($row['detailed-characteristics.parking-space-fee'], $this->parking_space_fees))	
				fputs($f, '			<parking-space-fee>'.$row['detailed-characteristics.parking-space-fee'].'</parking-space-fee>'."\n");
			
			if(isset($row['detailed-characteristics.assigned-parking-space-cost']) && !empty($row['detailed-characteristics.assigned-parking-space-cost']))	
				fputs($f, '			<assigned-parking-space-cost>'.$row['detailed-characteristics.assigned-parking-space-cost'].'</assigned-parking-space-cost>'."\n");
				
			if(isset($row['detailed-characteristics.parking-comment']) && !empty($row['detailed-characteristics.parking-comment']))		
				fputs($f, '			<parking-comment>'.str_replace($this->search, $this->replace, $row['detailed-characteristics.parking-comment']).'</parking-comment>'."\n");
				
			if(isset($row['detailed-characteristics.foundation-type']) && !empty($row['detailed-characteristics.foundation-type']))			
				fputs($f, '			<foundation-type>'.str_replace($this->search, $this->replace, $row['detailed-characteristics.foundation-type']).'</foundation-type>'."\n");
				
			if(isset($row['detailed-characteristics.roof-type']) && !empty($row['detailed-characteristics.roof-type']))			
				fputs($f, '			<roof-type>'.str_replace($this->search, $this->replace, $row['detailed-characteristics.roof-type']).'</roof-type>'."\n");
				
			if(isset($row['detailed-characteristics.architecture-style']) && !empty($row['detailed-characteristics.architecture-style']))				
				fputs($f, '			<architecture-style>'.str_replace($this->search, $this->replace, $row['detailed-characteristics.architecture-style']).'</architecture-style>'."\n");
			
			if(isset($row['detailed-characteristics.exterior-type']) && !empty($row['detailed-characteristics.exterior-type']))				
				fputs($f, '			<exterior-type>'.str_replace($this->search, $this->replace, $row['detailed-characteristics.exterior-type']).'</exterior-type>'."\n");
				
			if(isset($row['detailed-characteristics.room-count']) && !empty($row['detailed-characteristics.room-count']))					
				fputs($f, '			<room-count>'.$row['detailed-characteristics.room-count'].'</room-count>'."\n");
			
			if(isset($row['detailed-characteristics.rooms']) && !empty($row['detailed-characteristics.rooms'])){
				fputs($f, '			<rooms>'."\n");
				
				foreach($row['detailed-characteristics.rooms'] as $room){
					fputs($f, '				<room>'."\n");
					fputs($f, '					<room-type>'.str_replace($this->search, $this->replace, $room['room-type']).'</room-type>'."\n");
					fputs($f, '					<room-size>'.$room['room-size'].'</room-size>'."\n");
					fputs($f, '					<room-description>'.str_replace($this->search, $this->replace, $room['room-description']).'</room-description>'."\n");
					fputs($f, '				</room>'."\n");
				}
				
				fputs($f, '			</rooms>'."\n");
			}

			if(isset($row['detailed-characteristics.year-updated']) && !empty($row['detailed-characteristics.year-updated']) && strlen($row['detailed-characteristics.year-updated'])==4)
				fputs($f, '			<year-updated>'.$row['detailed-characteristics.year-updated'].'</year-updated>'."\n");
			
			if(isset($row['detailed-characteristics.total-units-in-building']) && !empty($row['detailed-characteristics.total-units-in-building']))
				fputs($f, '			<total-units-in-building>'.$row['detailed-characteristics.total-units-in-building'].'</total-units-in-building>'."\n");
			
			if(isset($row['detailed-characteristics.total-floors-in-building']) && !empty($row['detailed-characteristics.total-floors-in-building']))
				fputs($f, '			<total-floors-in-building>'.$row['detailed-characteristics.total-floors-in-building'].'</total-floors-in-building>'."\n");
			
			if(isset($row['detailed-characteristics.num-floors-in-unit']) && !empty($row['detailed-characteristics.num-floors-in-unit']))
				fputs($f, '			<num-floors-in-unit>'.$row['detailed-characteristics.num-floors-in-unit'].'</num-floors-in-unit>'."\n");
				
			if(isset($row['detailed-characteristics.has-attic']))
				fputs($f, '			<has-attic>'.($row['detailed-characteristics.has-attic'] ? 'yes' : 'no').'</has-attic>'."\n");
				
			if(isset($row['detailed-characteristics.has-balcony']))	
				fputs($f, '			<has-balcony>'.($row['detailed-characteristics.has-balcony'] ? 'yes' : 'no').'</has-balcony>'."\n");
				
			if(isset($row['detailed-characteristics.has-barbeque-area']))		
				fputs($f, '			<has-barbeque-area>'.($row['detailed-characteristics.has-barbeque-area'] ? 'yes' : 'no').'</has-barbeque-area>'."\n");
				
			if(isset($row['detailed-characteristics.has-basement']))
				fputs($f, '			<has-basement>'.($row['detailed-characteristics.has-basement'] ? 'yes' : 'no').'</has-basement>'."\n");
			
			if(isset($row['detailed-characteristics.has-cable-satellite']))
				fputs($f, '			<has-cable-satellite>'.($row['detailed-characteristics.has-cable-satellite'] ? 'yes' : 'no').'</has-cable-satellite>'."\n");
				
			if(isset($row['detailed-characteristics.has-courtyard']))
				fputs($f, '			<has-courtyard>'.($row['detailed-characteristics.has-courtyard'] ? 'yes' : 'no').'</has-courtyard>'."\n");
			
			if(isset($row['detailed-characteristics.has-deck']))
				fputs($f, '			<has-deck>'.($row['detailed-characteristics.has-deck'] ? 'yes' : 'no').'</has-deck>'."\n");
			
			if(isset($row['detailed-characteristics.has-disabled-access']))
				fputs($f, '			<has-disabled-access>'.($row['detailed-characteristics.has-disabled-access'] ? 'yes' : 'no').'</has-disabled-access>'."\n");

			if(isset($row['detailed-characteristics.has-dock']))
				fputs($f, '			<has-dock>'.($row['detailed-characteristics.has-dock'] ? 'yes' : 'no').'</has-dock>'."\n");
				
			if(isset($row['detailed-characteristics.has-doublepane-windows']))	
				fputs($f, '			<has-doublepane-windows>'.($row['detailed-characteristics.has-doublepane-windows'] ? 'yes' : 'no').'</has-doublepane-windows>'."\n");
			
			if(isset($row['detailed-characteristics.has-garden']))	
				fputs($f, '			<has-garden>'.($row['detailed-characteristics.has-garden'] ? 'yes' : 'no').'</has-garden>'."\n");
				
			if(isset($row['detailed-characteristics.has-gated-entry']))		
				fputs($f, '			<has-gated-entry>'.($row['detailed-characteristics.has-gated-entry'] ? 'yes' : 'no').'</has-gated-entry>'."\n");
			
			if(isset($row['detailed-characteristics.has-greenhouse']))		
				fputs($f, '			<has-greenhouse>'.($row['detailed-characteristics.has-greenhouse'] ? 'yes'  : 'no').'</has-greenhouse>'."\n");
				
			if(isset($row['detailed-characteristics.has-handrails']))			
				fputs($f, '			<has-handrails>'.($row['detailed-characteristics.has-handrails'] ? 'yes' : 'no').'</has-handrails>'."\n");
				
			if(isset($row['detailed-characteristics.has-hot-tub-spa']))			
				fputs($f, '			<has-hot-tub-spa>'.($row['detailed-characteristics.has-hot-tub-spa'] ? 'yes' : 'no').'</has-hot-tub-spa>'."\n");
			
			if(isset($row['detailed-characteristics.has-intercom']))			
				fputs($f, '			<has-intercom>'.($row['detailed-characteristics.has-intercom'] ? 'yes' : 'no').'</has-intercom>'."\n");
				
			if(isset($row['detailed-characteristics.has-jetted-bath-tub']))				
				fputs($f, '			<has-jetted-bath-tub>'.($row['detailed-characteristics.has-jetted-bath-tub'] ? 'yes' : 'no').'</has-jetted-bath-tub>'."\n");
			
			if(isset($row['detailed-characteristics.has-lawn']))
				fputs($f, '			<has-lawn>'.($row['has-lawn'] ? 'yes' : 'no').'</has-lawn>'."\n");
			
			if(isset($row['detailed-characteristics.has-mother-in-law']))
				fputs($f, '			<has-mother-in-law>'.($row['detailed-characteristics.has-mother-in-law'] ? 'yes' : 'no').'</has-mother-in-law>'."\n");
				
			if(isset($row['detailed-characteristics.has-patio']))	
				fputs($f, '			<has-patio>'.($row['detailed-characteristics.has-patio'] ? 'yes' : 'no').'</has-patio>'."\n");
			
			if(isset($row['detailed-characteristics.has-pond']))	
				fputs($f, '			<has-pond>'.($row['detailed-characteristics.has-pond'] ? 'yes' : 'no').'</has-pond>'."\n");
			
			if(isset($row['detailed-characteristics.has-pool']))	
				fputs($f, '			<has-pool>'.($row['detailed-characteristics.has-pool'] ? 'yes' : 'no').'</has-pool>'."\n");
			
			if(isset($row['detailed-characteristics.has-porch']))	
				fputs($f, '			<has-porch>'.($row['detailed-characteristics.has-porch'] ? 'yes' : 'no').'</has-porch>'."\n");
			
			if(isset($row['detailed-characteristics.has-private-balcony']))	
				fputs($f, '			<has-private-balcony>'.($row['detailed-characteristics.has-private-balcony'] ? 'yes'  : 'no').'</has-private-balcony>'."\n");
			
			if(isset($row['detailed-characteristics.has-private-patio']))	
				fputs($f, '			<has-private-patio>'.($row['detailed-characteristics.has-private-patio'] ? 'yes' : 'no').'</has-private-patio>'."\n");
			
			if(isset($row['detailed-characteristics.has-rv-parking']))	
				fputs($f, '			<has-rv-parking>'.($row['detailed-characteristics.has-rv-parking'] ? 'yes' : 'no').'</has-rv-parking>'."\n");
			
			if(isset($row['detailed-characteristics.has-sauna']))	
				fputs($f, '			<has-sauna>'.($row['detailed-characteristics.has-sauna'] ? 'yes' : 'no').'</has-sauna>'."\n");
			
			if(isset($row['detailed-characteristics.has-security-system']))	
				fputs($f, '			<has-security-system>'.($row['detailed-characteristics.has-security-system'] ? 'yes' : 'no').'</has-security-system>'."\n");
			
			if(isset($row['detailed-characteristics.has-skylight']))	
				fputs($f, '			<has-skylight>'.($row['detailed-characteristics.has-skylight'] ? 'yes' : 'no').'</has-skylight>'."\n");
			
			if(isset($row['detailed-characteristics.has-sportscourt']))	
				fputs($f, '			<has-sportscourt>'.($row['detailed-characteristics.has-sportscourt'] ? 'yes' : 'no').'</has-sportscourt>'."\n");
			
			if(isset($row['detailed-characteristics.has-sprinkler-system']))	
				fputs($f, '			<has-sprinkler-system>'.($row['detailed-characteristics.has-sprinkler-system'] ? 'yes' : 'no').'</has-sprinkler-system>'."\n");
			
			if(isset($row['detailed-characteristics.has-terrace']))	
				fputs($f, '			<has-terrace>'.($row['detailed-characteristics.has-terrace'] ? 'yes' : 'no').'</has-terrace>'."\n");
			
			if(isset($row['detailed-characteristics.has-vaulted-ceiling']))	
				fputs($f, '			<has-vaulted-ceiling>'.($row['detailed-characteristics.has-vaulted-ceiling'] ? 'yes' : 'no').'</has-vaulted-ceiling>'."\n");
			
			if(isset($row['detailed-characteristics.has-view']))	
				fputs($f, '			<has-view>'.($row['detailed-characteristics.has-view'] ? 'yes' : 'no').'</has-view>'."\n");
			
			if(isset($row['detailed-characteristics.has-washer-dryer-hookup']))	
				fputs($f, '			<has-washer-dryer-hookup>'.($row['detailed-characteristics.has-washer-dryer-hookup'] ? 'yes' : 'no').'</has-washer-dryer-hookup>'."\n");
			
			if(isset($row['detailed-characteristics.has-wet-bar']))	
				fputs($f, '			<has-wet-bar>'.($row['detailed-characteristics.has-wet-bar'] ? 'yes' : 'no').'</has-wet-bar>'."\n");
			
			if(isset($row['detailed-characteristics.has-window-coverings']))	
				fputs($f, '			<has-window-coverings>'.($row['detailed-characteristics.has-window-coverings'] ? 'yes' : 'no').'</has-window-coverings>'."\n");

			if(isset($row['detailed-characteristics.building-has-concierge']))		
				fputs($f, '			<building-has-concierge>'.($row['detailed-characteristics.building-has-concierge'] ? 'yes' : 'no').'</building-has-concierge>'."\n");
			
			if(isset($row['detailed-characteristics.building-has-doorman']))		
				fputs($f, '			<building-has-doorman>'.($row['detailed-characteristics.building-has-doorman'] ? 'yes' : 'no').'</building-has-doorman>'."\n");
			
			if(isset($row['detailed-characteristics.building-has-elevator']))		
				fputs($f, '			<building-has-elevator>'.($row['detailed-characteristics.building-has-elevator'] ? 'yes' : 'no').'</building-has-elevator>'."\n");
				
			if(isset($row['detailed-characteristics.building-has-fitness-center']))		
				fputs($f, '			<building-has-fitness-center>'.($row['detailed-characteristics.building-has-fitness-center'] ? 'yes' : 'no').'</building-has-fitness-center>'."\n");
				
			if(isset($row['detailed-characteristics.building-has-on-site-maintenance']))		
				fputs($f, '			<building-has-on-site-maintenance>'.($row['detailed-characteristics.building-has-on-site-maintenance'] ? 'yes' : 'no').'</building-has-on-site-maintenance>'."\n");
			
			if(isset($row['detailed-characteristics.is-waterfront']))		
				fputs($f, '			<is-waterfront>'.($row['detailed-characteristics.is-waterfront'] ? 'yes' : 'no').'</is-waterfront>'."\n");
			
			if(isset($row['detailed-characteristics.other-amenities']) && !empty($row['detailed-characteristics.other-amenities'])){
				fputs($f, '			<other-amenities>'."\n");
				fputs($f, '				<other-amenity>'.str_replace($this->search, $this->replace, $row['detailed-characteristics.other-amenities']).'</other-amenity>'."\n");
				fputs($f, '			</other-amenities>'."\n");
			}
			
			if(isset($row['detailed-characteristics.furnished']))		
				fputs($f, '			<furnished>'.($row['detailed-characteristics.furnished'] ? 'yes' : 'no').'</furnished>'."\n");
				
			if(isset($row['detailed-characteristics.view-type']) && !empty($row['detailed-characteristics.view-type']))			
				fputs($f, '			<view-type>'.str_replace($this->search, $this->replace, $row['detailed-characteristics.view-type']).'</view-type>'."\n");
				
			fputs($f, '		</detailed-characteristics>'."\n");
			fputs($f, '		<advertise-with-us>'."\n");
			
			if(isset($row['advertise-with-us.channel']) && !empty($row['advertise-with-us.channel']))			
				fputs($f, '			<channel>'.str_replace($this->search, $this->replace, $row['advertise-with-us.channel']).'</channel>'."\n");
			
			if(isset($row['advertise-with-us.featured']) && !empty($row['advertise-with-us.featured']))			
				fputs($f, '			<featured>'.($row['advertise-with-us.featured'] ? 'yes' : 'no').'</featured>'."\n");
			
			if(isset($row['advertise-with-us.branded']) && !empty($row['advertise-with-us.branded']))			
				fputs($f, '			<branded>'.($row['advertise-with-us.branded'] ? 'yes' : 'no').'</branded>'."\n");
				
			if(isset($row['advertise-with-us.branded-logo-url']) && !empty($row['advertise-with-us.branded-logo-url']))				
				fputs($f, '			<branded-logo-url>'.str_replace($this->search, $this->replace, $row['advertise-with-us.branded-logo-url']).'</branded-logo-url>'."\n");
				
			fputs($f, '		</advertise-with-us>'."\n");
			fputs($f, '	</property>'."\n");
		}
		
		fputs($f, '</properties>'."\n");		
		fclose($f);
	}
	
	/**
	 * Validate data
	 * @param array $data data for validation
	 */
	public function validate_data($data){
		$return = array("errors" => array(), "data" => array());		
		$return["data"] = $data;	
		if(!isset($data['details.provider-listingid']) || empty($data['details.provider-listingid'])) $return["errors"][] = "location.provider-listingid";
		if(!isset($data['location.display-address']) || empty($data['location.display-address'])) $return["errors"][] = "location.display-address";
		
		if(isset($return['data']['pictures']) && !empty($return['data']['pictures'])) $return['data']['pictures'] = explode('|', $return['data']['pictures']);
		if(isset($return['data']['virtual-tours']) && !empty($return['data']['virtual-tours'])) $return['data']['virtual-tours'] = explode('|', $return['data']['virtual-tours']);
		if(isset($return['data']['videos']) && !empty($return['data']['videos'])) $return['data']['videos'] = explode('|', $return['data']['videos']);
		
		return $return;
	}
}
