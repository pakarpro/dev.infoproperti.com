<?php 
/**
 * XML export driver model
 * 
* @package PG_RealEstate
 * @subpackage application
 * @category	modules
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/

if(!defined("BASEPATH")) exit("No direct script access allowed");

class Export_xml_model extends Model{
	
	/**
	 * Link to CodeIgniter object
	 * @var object
	 */
	private $CI;
	
	/**
	 * Not available symbols
	 */
	protected $search = array('&', "'", '"', ">", "<");
	
	/**
	 * Symbols for replace
	 */
	protected $replace = array('&amp;', '&apos;', '&quot;', "&gt;", "&lt;");
	
	/**
	 * Constructor
	 * @return Export_xml_model
	 */
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
	}

	/**
	 * Return file name
	 */
	public function get_filename(){
		return "export.xml";
	}

	/**
	 * Generate output file
	 * @param string $filename export file name
	 * @param array $data export data
	 * @param array $settings driver settings
	 */
	public function generate($filename, $data, $settings){
		$f = fopen($filename, "wb");
		fputs($f, '<?xml version="1.0" encoding="utf-8"?>'."\n");
		fputs($f, '<items>'."\n");
		foreach($data as $i => $row){
			if(!$i) continue;
			fputs($f, '	<item>'."\n");
			foreach($row as $key=>$value){
				$value = str_replace($this->search, $this->replace, $value);
				fputs($f, '			<'.$key.'>'.$value.'</'.$key.'>'."\n");
			}
			fputs($f, '	</item>'."\n");
		}
		fputs($f, '</items>'."\n");
		fclose($f);
	}
	
	/**
	 * Validate data
	 * @param array $data data for validation
	 */
	public function validate_data($data){
		$return = array("errors" => array(), "data" => array());		
		$return["data"] = $data;	
		return $return;
	}
}
