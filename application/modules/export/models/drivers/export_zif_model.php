<?php 
/**
 * Zillow export driver model
 * 
* @package PG_RealEstate
 * @subpackage application
 * @category	modules
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/

if(!defined("BASEPATH")) exit("No direct script access allowed");

class Export_zif_model extends Model{
	
	/**
	 * Link to CodeIgniter object
	 * @var object
	 */
	private $CI;
	
	/**
	 * Not available symbols
	 */
	protected $search = array('&', "'", '"', ">", "<");
	
	/**
	 * Symbols for replace
	 */
	protected $replace = array('&amp;', '&apos;', '&quot;', "&gt;", "&lt;");
	
	/**
	 * Lease terms
	 * @var array
	 */
	private $lease_term = array('ContactForDetails', 'Monthly', 'SixMonths', 'OneYear', 'RentToOwn');
	
	/**
	 * Property types
	 * @var array
	 */
	private $property_types = array('SingleFamily', 'Condo', 'Townhouse', 'Coop', 'MultiFamily', 
									'Manufactured', 'VacantLand', 'Other', 'Apartment');
	
	/**
	 * Appliances
	 * @var array
	 */
	private $appliances = array('Dishwasher', 'Dryer', 'Freezer', 'GarbageDisposal', 'Microwave', 
								'RangeOven', 'Refrigerator', 'TrashCompactor', 'Washer');

	/**
	 * Architecture styles
	 * @var array
	 */
	private $architecture_styles = array('Bungalow', 'CapeCod', 'Colonial', 'Contemporary', 'Craftsman',
										 'French', 'Georgian', 'Loft', 'Modern', 'Queen AnneVictorian',
										 'RanchRambler', 'SantaFePuebloStyle', 'Spanish', 'Split-level', 
										 'Tudor', 'Other');
				
	/**
	 * Cooling systems
	 * @var array
	 */
	private $cooling_systems = array('None', 'Central', 'Evaporative', 'Geothermal', 'Wall', 'Solar', 'Other');
	
	/**
	 * Exterior types
	 * @var array
	 */
	private $exterior_types = array('Brick', 'CementConcrete', 'Composition', 'Metal', 'Shingle', 'Stone',
									'Stucco', 'Vinyl', 'Wood', 'WoodProducts', 'Other');

	/**
	 * Floor coverings
	 * @var array
	 */
	private $floor_coverings = array('Carpet', 'Concrete', 'Hardwood', 'Laminate', 'LinoleumVinyl', 'Slate',
									 'Softwood', 'Tile', 'Other');
	
	/**
	 * Heating fuels
	 * @var array
	 */
	private $heating_fuels = array('None', 'Coal', 'Electric', 'Gas', 'Oil', 'PropaneButane', 'Solar', 'WoodPellet', 'Other');
	
	/**
	 * Heating systems
	 * @var array
	 */
	private $heating_systems = array('Baseboard', 'ForcedAir', 'HeatPump', 'Radiant', 'Stove', 'Wall', 'Other');
	
	/**
	 * Parking types
	 * @var array
	 */
	private $parking_types = array('Carport', 'GarageAttached', 'GarageDetached', 'OffStreet', 'OnStreet', 'None');
	
	/**
	 * Roof types
	 * @var array
	 */
	private $roof_types = array('Asphalt', 'BuiltUp', 'Composition', 'Metal', 'ShakeShingle', 'Slate', 'Tile', 'Other');
	
	/**
	 * Rooms
	 * @var array
	 */
	private $rooms = array('BreakfastNook', 'DiningRoom', 'FamilyRoom', 'LaundryRoom', 'Library', 'MasterBath',
						   'MudRoom', 'Office', 'Pantry', 'RecreationRoom', 'Workshop', 'SolariumAtrium', 'SunRoom',
						   'WalkInCloset');
	
	/**
	 * View types
	 * @var array
	 */
	private $view_types = array('None', 'City', 'Mountain', 'Park', 'Territorial', 'Water');
		
	/**
	 * Constructor
	 * @return Export_zif_model
	 */
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
	}
	
	/**
	 * Return file name
	 */
	public function get_filename(){
		return "export.xml";
	}
	
	/**
	 * Generate output file
	 * @param string $filename export file name
	 * @param array $data export data
	 * @param array $settings driver settings
	 */
	public function generate($filename, $data, $settings){
		$this->CI->config->load("reg_exps", true);
		$email_expr = $this->CI->config->item("email", "reg_exps");
	
		$f = fopen($filename, "wb");
		fputs($f, '<?xml version="1.0" encoding="utf-8">'."\n");
		fputs($f, '<Listings>'."\n");
		foreach($data as $i=>$row){
			if(!$i) continue;
			fputs($f, '	<Listing>'."\n");
			fputs($f, '		<Location>'."\n");
			fputs($f, '			<StreetAddress>'.str_replace($this->search, $this->replace, $row['Location.StreetAddress']).'</StreetAddress>'."\n");
			
			if(isset($row['Location.UnitNumber']) && !empty($row['Location.UnitNumber']))
				fputs($f, '			<UnitNumber>'.$row['Location.UnitNumber'].'</UnitNumber>'."\n");
						
			fputs($f, '			<City>'.str_replace($this->search, $this->replace, $row['Location.City']).'</City>'."\n");
			fputs($f, '			<State>'.str_replace($this->search, $this->replace, $row['Location.State']).'</State>'."\n");
			fputs($f, '			<Zip>'.str_replace($this->search, $this->replace, $row['Location.Zip']).'</Zip>'."\n");
			
			if(isset($row['Location.ParcelId']) && !empty($row['Location.ParcelId']))
				fputs($f, '			<ParcelId>'.str_replace($this->search, $this->replace, $row['Location.ParcelId']).'</ParcelId>'."\n");
				
			if(isset($row['Location.ZPID']) && !empty($row['Location.ZPID']))
				fputs($f, '			<ZPID>'.str_replace($this->search, $this->replace, $row['Location.ZPID']).'</ZPID>'."\n");
			
			if(isset($row['Location.Lat']) && !floatval($row['Location.Lat']))
				fputs($f, '			<Lat>'.floatval($row['Location.Lat']).'</Lat>'."\n");
			
			if(isset($row['Location.Long']) && !floatval($row['Location.Long']))
				fputs($f, '			<Long>'.floatval($row['Location.Long']).'</Long>'."\n");
			
			if(isset($row['Location.County']) && !empty($row['Location.County']))
				fputs($f, '			<County>'.str_replace($this->search, $this->replace, $row['Location.County']).'</County>'."\n");
			
			if(isset($row['Location.StreetIntersection']) && !empty($row['Location.StreetIntersection']))
				fputs($f, '			<StreetIntersection>'.str_replace($this->search, $this->replace, $row['Location.StreetIntersection']).'</StreetIntersection>'."\n");
			
			if(isset($row['Location.DisplayAddress']))
				fputs($f, '			<DisplayAddress>'.($row['Location.DisplayAddress'] ? 'Yes' : 'No').'</DisplayAddress>'."\n");
			
			fputs($f, '		</Location>'."\n");
			fputs($f, '		<ListingDetails>'."\n");
			
			$row['ListingDetails.Status'] = $row['ListingDetails.Status'] ? 'Active' : 'Pending';
			if($row['ListingSold']) $row['Status'] = 'Sold';
			if($row['ListingType'] == 'rent' || $row['ListingType'] == 'lease') $row['ListingDetails.Status'] = 'For Rent';
			fputs($f, '			<Status>'.$row['ListingDetails.Status'].'</Status>'."\n");
			fputs($f, '			<Price>'.$row['ListingDetails.Price'].'</Price>'."\n");
			
			if(isset($row['ListingDetails.ListingUrl']) && !empty($row['ListingDetails.ListingUrl']))
				fputs($f, '			<ListingUrl>'.str_replace($this->search, $this->replace, $row['ListingDetails.ListingUrl']).'</ListingUrl>'."\n");
		
			if(isset($row['ListingDetails.MlsId']) && !empty($row['ListingDetails.MlsId']))
				fputs($f, '			<MlsId>'.$row['ListingDetails.MlsId'].'</MlsId>'."\n");
			
			if(isset($row['ListingDetails.MlsName']) && !empty($row['ListingDetails.MlsName']))
				fputs($f, '			<MlsName>'.str_replace($this->search, $this->replace, $row['ListingDetails.MlsName']).'</MlsName>'."\n");
			
			if(isset($row['ListingDetails.DateListed']) && !strtotime($row['ListingDetails.DateListed']))
				fputs($f, '			<DateListed>'.date('Y-m-d', strtotime($row['ListingDetails.DateListed'])).'</DateListed>'."\n");
			
			if(isset($row['ListingDetails.DateSold']) && !strtotime($row['ListingDetails.DateSold']))
				fputs($f, '			<DateSold>'.date('Y-m-d', strtotime($row['ListingDetails.DateSold'])).'</DateSold>'."\n");
			
			if(isset($row['ListingDetails.VirtualTourUrl']) && !empty($row['ListingDetails.VirtualTourUrl']))
				fputs($f, '			<VirtualTourUrl>'.str_replace($this->search, $this->replace, $row['ListingDetails.VirtualTourUrl']).'</VirtualTourUrl>'."\n");
			
			if(isset($row['ListingDetails.ListingEmail']) && !empty($row['ListingDetails.ListingEmail']) && preg_match($email_expr, $row["ListingDetails.ListingEmail"]))
				fputs($f, '			<ListingEmail>'.str_replace($this->search, $this->replace, $row['ListingDetails.ListingEmail']).'</ListingEmail>'."\n");
			
			if(isset($row['ListingDetails.AlwaysEmailAgent']))
				fputs($f, '			<AlwaysEmailAgent>'.($row['ListingDetails.AlwaysEmailAgent'] ? 1 : 0).'</AlwaysEmailAgent>'."\n");			
			
			fputs($f, '		</ListingDetails>'."\n");
			fputs($f, '		<RentalDetails>'."\n");
			
			if(isset($row['RentalDetails.Availability']) && !empty($row['RentalDetails.Availability'])){
				switch($row['RentalDetails.Availability']){
					case 'ContactForDetails':
					case 'Now':
						fputs($f, '			<Availability>'.$row['RentalDetails.Availability'].'</Availability>'."\n");
					break;
					default:
						if(strtotime($row['RentalDetails.Availability'])){
							fputs($f, '			<Availability>'.date('Y-m-d', strtotime($row['RentalDetails.Availability'])).'</Availability>'."\n");
						}				
					break;
				}
			}
			
			if(isset($row['RentalDetails.LeaseTerm']) && in_array($row['RentalDetails.LeaseTerm'], $this->lease_term))
				fputs($f, '			<LeaseTerm>'.str_replace($this->search, $this->replace, $row['RentalDetails.LeaseTerm']).'</LeaseTerm>'."\n");
			
			if(isset($row['RentalDetails.DepositFees']) && !empty($row['RentalDetails.DepositFees']))
				fputs($f, '			<DepositFees>'.str_replace($this->search, $this->replace, $row['RentalDetails.DepositFees']).'</DepositFees>'."\n");
			
			if(isset($row['RentalDetails.UtilitiesIncluded.Water']) || 
			   isset($row['RentalDetails.UtilitiesIncluded.Sewage']) || 
			   isset($row['RentalDetails.UtilitiesIncluded.Electricity']) || 
			   isset($row['RentalDetails.UtilitiesIncluded.Gas']) ||
			   isset($row['RentalDetails.UtilitiesIncluded.Internet']) || 
			   isset($row['RentalDetails.UtilitiesIncluded.Cable']) || 
			   isset($row['RentalDetails.UtilitiesIncluded.SatTV'])){
				fputs($f, '			<UtilitiesIncluded>'."\n");
				
				if(isset($row['Water']))
					fputs($f, '				<Water>'.($row['RentalDetails.UtilitiesIncluded.Water'] ? 'Yes' : 'No').'</Water>'."\n");
				
				if(isset($row['Sewage']))
					fputs($f, '				<Sewage>'.($row['RentalDetails.UtilitiesIncluded.Sewage'] ? 'Yes' : 'No').'</Sewage>'."\n");
					
				if(isset($row['Electricity']))
					fputs($f, '				<Electricity>'.($row['RentalDetails.UtilitiesIncluded.Electricity'] ? 'Yes' : 'No').'</Electricity>'."\n");	
					
				if(isset($row['Gas']))
					fputs($f, '				<Gas>'.($row['RentalDetails.UtilitiesIncluded.Gas'] ? 'Yes' : 'No').'</Gas>'."\n");
					
				if(isset($row['Internet']))
					fputs($f, '				<Internet>'.($row['RentalDetails.UtilitiesIncluded.Internet'] ? 'Yes' : 'No').'</Internet>'."\n");
					
				if(isset($row['Cable']))
					fputs($f, '				<Cable>'.($row['RentalDetails.UtilitiesIncluded.Cable'] ? 'Yes' : 'No').'</Cable>'."\n");
					
				if(isset($row['SatTV']))
					fputs($f, '				<SatTV>'.($row['RentalDetails.UtilitiesIncluded.SatTV'] ? 'Yes' : 'No').'</SatTV>'."\n");
					
				fputs($f, '			</UtilitiesIncluded>'."\n");
			}
			if(isset($row['RentalDetails.PetsAllowed.NoPets']) || 
			   isset($row['RentalDetails.PetsAllowed.Cats']) || 
			   isset($row['RentalDetails.PetsAllowed.SmallDogs']) || 
			   isset($row['RentalDetails.PetsAllowed.LargeDogs'])){
				fputs($f, '			<PetsAllowed>'."\n");
				
				if(isset($row['NoPets']))
					fputs($f, '				<NoPets>'.($row['RentalDetails.PetsAllowed.NoPets'] ? 'Yes' : 'No').'</NoPets>'."\n");
				
				if(isset($row['Cats']))
					fputs($f, '				<Cats>'.($row['RentalDetails.PetsAllowed.Cats'] ? 'Yes' : 'No').'</Cats>'."\n");
				
				if(isset($row['SmallDogs']))
					fputs($f, '				<SmallDogs>'.($row['RentalDetails.PetsAllowed.SmallDogs'] ? 'Yes' : 'No').'</SmallDogs>'."\n");
					
				if(isset($row['LargeDogs']))
					fputs($f, '				<LargeDogs>'.($row['RentalDetails.PetsAllowed.LargeDogs'] ? 'Yes' : 'No').'</LargeDogs>'."\n");	
				
				fputs($f, '			</PetsAllowed>'."\n");
			}
						
			fputs($f, '		</RentalDetails>'."\n");
			fputs($f, '		<BasicDetails>'."\n");
			fputs($f, '			<PropertyType>'.$row['BasicDetails.PropertyType'].'</PropertyType>'."\n");
			
			if(isset($row['BasicDetails.Title']) && !empty($row['BasicDetails.Title']))
				fputs($f, '				<Title>'.str_replace($this->search, $this->replace, $row['BasicDetails.Title']).'</Title>'."\n");
			
			if(isset($row['BasicDetails.Description']) && !empty($row['BasicDetails.Description']))
				fputs($f, '				<Description>'.str_replace($this->search, $this->replace, $row['BasicDetails.Description']).'</Description>'."\n");

			if(isset($row['BasicDetails.Bedrooms']) && !empty($row['BasicDetails.Bedrooms']))
				fputs($f, '				<Bedrooms>'.$row['BasicDetails.Bedrooms'].'</Bedrooms>'."\n");
			
			if(isset($row['BasicDetails.Bathrooms']) && !empty($row['BasicDetails.Bathrooms']))
				fputs($f, '				<Bathrooms>'.$row['BasicDetails.Bathrooms'].'</Bathrooms>'."\n");
			
			if(isset($row['BasicDetails.FullBathrooms']) && !empty($row['BasicDetails.FullBathrooms']))
				fputs($f, '				<FullBathrooms>'.$row['BasicDetails.FullBathrooms'].'</FullBathrooms>'."\n");
			
			if(isset($row['BasicDetails.HalfBathrooms']) && !empty($row['BasicDetails.HalfBathrooms']))
				fputs($f, '				<HalfBathrooms>'.$row['BasicDetails.HalfBathrooms'].'</HalfBathrooms>'."\n");
			
			if(isset($row['BasicDetails.LivingArea']) && !empty($row['BasicDetails.LivingArea']))
				fputs($f, '				<LivingArea>'.$row['BasicDetails.LivingArea'].'</LivingArea>'."\n");
			
			if(isset($row['BasicDetails.LotSize']) && !empty($row['BasicDetails.LotSize']))
				fputs($f, '				<LotSize>'.$row['BasicDetails.LotSize'].'</LotSize>'."\n");

			if(isset($row['BasicDetails.YearBuilt']) && !empty($row['BasicDetails.YearBuilt']) && strlen($row['BasicDetails.YearBuilt']) == 4)
				fputs($f, '				<YearBuilt>'.$row['BasicDetails.YearBuilt'].'</YearBuilt>'."\n");
			
			fputs($f, '		</BasicDetails>'."\n");
			
			if(isset($row['Pictures']) && !empty($row['Pictures'])){
				fputs($f, '		<Pictures>'."\n");
				foreach($row['Pictures'] as $picture){
					fputs($f, '			<Picture>'."\n");
					
						fputs($f, '			<PictureUrl>'.str_replace($this->search, $this->replace, $picture).'</PictureUrl>'."\n");
						//fputs($f, '			<Caption>'.str_replace($this->search, $this->replace, $picture).'</Caption>'."\n");
						
					fputs($f, '			</Picture>'."\n");
				}			
				fputs($f, '		</Pictures>'."\n");
			}
			
			fputs($f, '		<Agent>'."\n");
				
			if(isset($row['Agent.FirstName']) && !empty($row['Agent.FirstName']))
				fputs($f, '			<FirstName>'.str_replace($this->search, $this->replace, $row['Agent.FirstName']).'</FirstName>'."\n");
				
			if(isset($row['Agent.LastName']) && !empty($row['Agent.LastName']))
				fputs($f, '			<LastName>'.str_replace($this->search, $this->replace, $row['Agent.LastName']).'</LastName>'."\n");
				
			fputs($f, '			<EmailAddress>'.str_replace($this->search, $this->replace, $row['Agent.EmailAddress']).'</EmailAddress>'."\n");
				
			if(isset($row['Agent.PictureUrl']) && !empty($row['Agent.PictureUrl']))
				fputs($f, '			<PictureUrl>'.str_replace($this->search, $this->replace, $row['Agent.PictureUrl']).'</PictureUrl>'."\n");
					
			if(isset($row['Agent.OfficeLineNumber']) && !empty($row['Agent.OfficeLineNumber']) && preg_match('/^\d{3}-\d{3}-\d{3}(\sx\d{5})?/', $row['Agent.FaxLineNumber']))
				fputs($f, '			<OfficeLineNumber>'.$row['Agent.OfficeLineNumber'].'</OfficeLineNumber>'."\n");
				
			if(isset($row['Agent.MobilePhoneLineNumber']) && !empty($row['Agent.MobilePhoneLineNumber']) && preg_match('/^\d{3}-\d{3}-\d{3}/', $row['Agent.MobilePhoneLineNumber']))
				fputs($f, '			<MobilePhoneLineNumber>'.$row['Agent.MobilePhoneLineNumber'].'</MobilePhoneLineNumber>'."\n");
				
			if(isset($row['Agent.FaxLineNumber']) && !empty($row['Agent.FaxLineNumber']) && preg_match('/^\d{3}-\d{3}-\d{3}/', $row['Agent.FaxLineNumber']))
				fputs($f, '			<FaxLineNumber>'.$row['Agent.FaxLineNumber'].'</FaxLineNumber>'."\n");
					
			fputs($f, '		</Agent>'."\n");
			
			fputs($f, '		<Office>'."\n");
				
			if(isset($row['Office.BrokerageName']) && !empty($row['Office.BrokerageName']))
				fputs($f, '			<BrokerageName>'.str_replace($this->search, $this->replace, $row['Office.BrokerageName']).'</BrokerageName>'."\n");
				
			fputs($f, '			<BrokerPhone>'.$row['Office.BrokerPhone'].'</BrokerPhone>'."\n");

			if(isset($row['Office.BrokerEmail']) && !empty($row['Office.BrokerEmail']))
				fputs($f, '			<BrokerEmail>'.str_replace($this->search, $this->replace, $row['Office.BrokerEmail']).'</BrokerEmail>'."\n");
				
			if(isset($row['Office.BrokerWebsite']) && !empty($row['Office.BrokerWebsite']))
				fputs($f, '			<BrokerWebsite>'.str_replace($this->search, $this->replace, $row['Office.BrokerWebsite']).'</BrokerWebsite>'."\n");
			
			if(isset($row['Office.StreetAddress']) && !empty($row['Office.StreetAddress']))
				fputs($f, '			<StreetAddress>'.str_replace($this->search, $this->replace, $row['Office.StreetAddress']).'</StreetAddress>'."\n");
				
			if(isset($row['Office.UnitNumber']) && !empty($row['Office.UnitNumber']))	
				fputs($f, '			<UnitNumber>'.str_replace($this->search, $this->replace, $row['Office.UnitNumber']).'</UnitNumber>'."\n");
					
			if(isset($row['Office.City']) && !empty($row['Office.City']))		
				fputs($f, '			<City>'.str_replace($this->search, $this->replace, $row['Office.City']).'</City>'."\n");
				
			if(isset($row['Office.State']) && !empty($row['Office.State']))		
				fputs($f, '			<State>'.str_replace($this->search, $this->replace, $row['Office.State']).'</State>'."\n");
				
			if(isset($row['Office.Zip']) && !empty($row['Office.Zip']))
				fputs($f, '			<Zip>'.str_replace($this->search, $this->replace, $row['Office.Zip']).'</Zip>'."\n");
					
			if(isset($row['Office.Name']) && !empty($row['Office.Name']))	
				fputs($f, '			<OfficeName>'.str_replace($this->search, $this->replace, $row['Office.Name']).'</OfficeName>'."\n");
					
			if(isset($row['Office.FranchiseName']) && !empty($row['Office.FranchiseName']))		
				fputs($f, '			<FranchiseName>'.str_replace($this->search, $this->replace, $row['Office.FranchiseName']).'</FranchiseName>'."\n");
				
			fputs($f, '		</Office>'."\n");
			
			if(isset($row['OpenHouses.OpenHouse.Date']) && !strtotime($row['OpenHouses.OpenHouse.Date'])){
				fputs($f, '		<OpenHouses>'."\n");
				fputs($f, '			<OpenHouse>'."\n");
				fputs($f, '				<Date>'.date('Y-m-d', strtotime($row['OpenHouses.OpenHouse.Date'])).'</Date>'."\n");
				
				if(isset($row['OpenHouses.OpenHouse.StartTime']) && !strtotime($row['OpenHouses.OpenHouse.StartTime']))
					fputs($f, '				<StartTime>'.date('H:i', strtotime($row['OpenHouses.OpenHouse.StartTime'])).'</StartTime>'."\n");
				
				if(isset($row['OpenHouses.OpenHouse.EndTime']) && !strtotime($row['OpenHouses.OpenHouse.EndTime']))
					fputs($f, '				<EndTime>'.date('H:i', strtotime($row['OpenHouses.OpenHouse.EndTime'])).'</EndTime>'."\n");
					
				fputs($f, '			</OpenHouse>'."\n");
				fputs($f, '		</OpenHouses>'."\n");
			}
			
			if(isset($row['Schools.District']) && !empty($row['Schools.District']) ||
			   isset($row['Schools.Elementary']) && !empty($row['Schools.Elementary']) ||
			   isset($row['Schools.Middle']) && !empty($row['Schools.Middle']) ||
			   isset($row['Schools.High']) && !empty($row['Schools.High'])){
				fputs($f, '		<Schools>'."\n");
				
				if(isset($row['Schools.District']) && !empty($row['Schools.District']))
					fputs($f, '			<District>'.str_replace($this->search, $this->replace, $row['Schools.District']).'</District>'."\n");
				
				if(isset($row['Schools.Elementary']) && !empty($row['Schools.Elementary']))
					fputs($f, '			<Elementary>'.str_replace($this->search, $this->replace, $row['Schools.Elementary']).'</Elementary>'."\n");
					
				if(isset($row['Schools.Middle']) && !empty($row['Schools.Middle']))
					fputs($f, '			<Middle>'.str_replace($this->search, $this->replace, $row['Schools.Middle']).'</Middle>'."\n");
					
				if(isset($row['Schools.High']) && !empty($row['Schools.High']))	
					fputs($f, '			<High>'.str_replace($this->search, $this->replace, $row['Schools.High']).'</High>'."\n");
					
				fputs($f, '		</Schools>'."\n");
			}
			
			if(isset($row['Neighborhood.Name']) && !empty($row['Neighborhood.Name']) ||
			   isset($row['Neighborhood.Description']) && !empty($row['Neighborhood.Description'])){
				fputs($f, '		<Neighborhood>'."\n");
				
				if(isset($row['Neighborhood.Name']) && !empty($row['Neighborhood.Name']))
					fputs($f, '			<Name>'.str_replace($this->search, $this->replace, $row['Neighborhood.Name']).'</Name>'."\n");
				
				if(isset($row['Neighborhood.Description']) && !empty($row['Neighborhood.Description']))
					fputs($f, '			<Description>'.str_replace($this->search, $this->replace, $row['Neighborhood.Description']).'</Description>'."\n");
				
				fputs($f, '		</Neighborhood>'."\n");
			}
			
			$appliances = array_intersect((array)$row['RichDetails.Appliances'], $this->appliances);
			$cooling_systems = array_intersect((array)$row['RichDetails.CoolingSystems'], $this->cooling_systems);
			$exterior_types = array_intersect((array)$row['RichDetails.ExteriorTypes'], $this->exterior_types);
			$floor_coverings = array_intersect((array)$row['FloorCoverings'], $this->floor_coverings);
			$heating_fuels = array_intersect((array)$row['RichDetails.HeatingFuels'], $this->heating_fuels);
			$heating_systems = array_intersect((array)$row['RichDetails.HeatingSystems'], $this->heating_systems);
			$parking_types = array_intersect((array)$row['RichDetails.ParkingTypes'], $this->parking_types);
			$rooms = array_intersect((array)$row['RichDetails.Rooms'], $this->rooms);
			$view_types = array_intersect((array)$row['RichDetails.ViewTypes'], $this->view_types);
			
			if(isset($row['RichDetails.AdditionalFeatures']) && !empty($row['RichDetails.AdditionalFeatures']) ||
			   isset($row['RichDetails.Appliances']) && !empty($appliances) ||
			   isset($row['RichDetails.ArchitectureStyle']) && in_array($row['RichDetails.ArchitectureStyle'], $this->architecture_styles) ||
			   isset($row['RichDetails.Attic']) ||
			   isset($row['RichDetails.BarbecueArea']) ||
			   isset($row['RichDetails.Basement']) ||
			   isset($row['RichDetails.BuildingUnitCount']) && !empty($row['RichDetails.BuildingUnitCount']) &&
					in_array($row['BasicDetails.PropertyType'], array('Condo', 'Coop', 'MultiFamily')) || 
			   isset($row['RichDetails.CableReady']) ||
			   isset($row['RichDetails.CeilingFan']) ||
			   isset($row['RichDetails.CondoFloorNum']) && !empty($row['RichDetails.CondoFloorNum']) && 
					in_array($row['BasicDetails.PropertyType'], array('Condo', 'Coop')) || 
			   isset($row['RichDetails.CoolingSystems']) && !empty($cooling_systems) ||
			   isset($row['RichDetails.Deck']) ||
			   isset($row['RichDetails.DisabledAccess']) ||
			   isset($row['RichDetails.Dock']) ||
			   isset($row['RichDetails.Doorman']) || 
			   isset($row['RichDetails.DoublePaneWindows']) ||
			   isset($row['RichDetails.Elevator']) ||
			   isset($row['RichDetails.ExteriorTypes']) && !empty($exterior_types) ||
			   isset($row['RichDetails.Fireplace']) ||
			   isset($row['RichDetails.FloorCoverings']) && !empty($floor_coverings) ||
			   isset($row['RichDetails.Garden']) ||
			   isset($row['RichDetails.GatedEntry']) ||
			   isset($row['RichDetails.Greenhouse']) ||
			   isset($row['RichDetails.HeatingFuels']) && !empty($heating_fuels) ||
			   isset($row['RichDetails.HeatingSystems']) && !empty($heating_systems) ||
			   isset($row['RichDetails.HottubSpa']) ||
			   isset($row['RichDetails.Intercom']) ||
			   isset($row['RichDetails.JettedBathTub']) ||
			   isset($row['RichDetails.Lawn']) ||
			   isset($row['RichDetails.LegalDescription']) && !empty($row['RichDetails.LegalDescription']) ||
			   isset($row['RichDetails.MotherInLaw']) ||
			   isset($row['RichDetails.NumFloors']) && !empty($row['RichDetails.NumFloors']) || 
			   isset($row['RichDetails.NumParkingSpaces']) && !empty($row['RichDetails.NumParkingSpaces']) || 
			   isset($row['RichDetails.ParkingTypes']) && !empty($parking_types) ||
			   isset($row['RichDetails.Patio']) ||
			   isset($row['RichDetails.Pond']) ||
			   isset($row['RichDetails.Pool']) ||
			   isset($row['RichDetails.Porch']) ||
			   isset($row['RichDetails.RoomCount']) && !empty($row['RichDetails.RoomCount']) ||
			   isset($row['RichDetails.Rooms']) && !empty($rooms) ||
			   isset($row['RichDetails.RvParking']) ||
			   isset($row['RichDetails.Sauna']) ||
			   isset($row['RichDetails.SecuritySystem']) ||
			   isset($row['RichDetails.Skylight']) ||
			   isset($row['RichDetails.SportsCourt']) ||
			   isset($row['RichDetails.SprinklerSystem']) ||
			   isset($row['RichDetails.VaultedCeiling']) ||
			   isset($row['RichDetails.ViewTypes']) && !empty($view_types) ||
			   isset($row['RichDetails.Waterfront']) ||
			   isset($row['RichDetails.Wetbar']) ||
			   isset($row['RichDetails.WhatOwnerLoves']) && !empty($row['RichDetails.WhatOwnerLoves']) ||
			   isset($row['RichDetails.Wired']) ||
			   isset($row['RichDetails.YearUpdated']) && !empty($row['RichDetails.YearUpdated']) ||
			   isset($row['RichDetails.FitnessCenter']) ||
			   isset($row['RichDetails.BasketballCourt']) ||
			   isset($row['RichDetails.TennisCourt']) ||
			   isset($row['RichDetails.NearTransportation']) ||
			   isset($row['RichDetails.ControlledAccess']) ||
			   isset($row['RichDetails.Over55ActiveCommunity']) ||
			   isset($row['RichDetails.AssistedLivingCommunity']) ||
			   isset($row['RichDetails.Storage']) ||
			   isset($row['RichDetails.FencedYard']) ||
			   isset($row['RichDetails.PropertyName']) && !empty($row['RichDetails.PropertyName']) ||
			   isset($row['RichDetails.Furnished']) ||
			   isset($row['RichDetails.HighspeedInternet']) ||
			   isset($row['RichDetails.OnsiteLaundry']) ||
			   isset($row['RichDetails.CableSatTV'])
			){
				fputs($f, '		<RichDetails>'."\n");
				
				if(isset($row['RichDetails.AdditionalFeatures']) && !empty($row['RichDetails.AdditionalFeatures']))
					fputs($f, '			<AdditionalFeatures>'.str_replace($this->search, $this->replace, implode(','.$row['RichDetails.AdditionalFeatures'])).'<AdditionalFeatures>'."\n");
				
				if(isset($row['RichDetails.Appliances']) && !empty($appliances)){
					fputs($f, '			<Appliances>'."\n");
					
					foreach($appliances as $appliance)
						fputs($f, '				<Appliance>'.$appliance.'</Appliance>'."\n");			
					
					fputs($f, '			</Appliances>'."\n");
				}
				
				if(isset($row['RichDetails.ArchitectureStyle']) && in_array($row['RichDetails.ArchitectureStyle'], $this->architecture_styles))
					fputs($f, '			<ArchitectureStyle>'.$row['RichDetails.ArchitectureStyle'].'</ArchitectureStyle>'."\n");
				
				if(isset($row['RichDetails.Attic']))
					fputs($f, '			<Attic>'.($row['RichDetails.Attic'] ? 'Yes' : 'No').'</Attic>'."\n");
				
				if(isset($row['RichDetails.BarbecueArea']))
					fputs($f, '			<BarbecueArea>'.($row['RichDetails.BarbecueArea'] ? 'Yes' : 'No').'</BarbecueArea>'."\n");
				
				if(isset($row['RichDetails.Basement']))
					fputs($f, '			<Basement>'.($row['RichDetails.Basement'] ? 'Yes' : 'No').'</Basement>'."\n");
					
				if(isset($row['RichDetails.BuildingUnitCount']) && !empty($row['RichDetails.BuildingUnitCount']) &&
				   in_array($row['BasicDetails.PropertyType'], array('Condo', 'Townhouse', 'Coop', 'MultiFamily')))
					fputs($f, '			<BuildingUnitCount>'.$row['RichDetails.BuildingUnitCount'].'</BuildingUnitCount>'."\n");
					
				if(isset($row['RichDetails.CableReady']))
					fputs($f, '			<CableReady>'.($row['RichDetails.CableReady'] ? 'Yes' : 'No').'</CableReady>'."\n");
			
				if(isset($row['RichDetails.CeilingFan']))
					fputs($f, '			<CeilingFan>'.($row['RichDetails.CeilingFan'] ? 'Yes' : 'No').'</CeilingFan>'."\n");
					
				if(isset($row['RichDetails.CondoFloorNum']) && !empty($row['RichDetails.CondoFloorNum']) && 
					in_array($row['BasicDetails.PropertyType'], array('Condo', 'Coop')))
					fputs($f, '			<CondoFloorNum>'.$row['RichDetails.CondoFloorNum'].'</CondoFloorNum>'."\n");
				
				if(isset($row['RichDetails.CoolingSystems']) && !empty($cooling_systems)){
					fputs($f, '			<CoolingSystems>'."\n");
					
					foreach($cooling_systems as $cooling_system)
						fputs($f, '				<CoolingSystem>'.$cooling_system.'</CoolingSystem>'."\n");
						
					fputs($f, '			</CoolingSystems>'."\n");
				}
				
				if(isset($row['RichDetails.Deck']))
					fputs($f, '			<Deck>'.($row['RichDetails.Deck'] ? 'Yes' : 'No').'</Deck>'."\n");
					
				if(isset($row['RichDetails.DisabledAccess']))
					fputs($f, '			<DisabledAccess>'.($row['RichDetails.DisabledAccess'] ? 'Yes' : 'No').'</DisabledAccess>'."\n");
					
				if(isset($row['RichDetails.Dock']))
					fputs($f, '			<Dock>'.($row['RichDetails.Dock'] ? 'Yes' : 'No').'</Dock>'."\n");
				
				if(isset($row['RichDetails.Doorman']))
					fputs($f, '			<Doorman>'.($row['RichDetails.Doorman'] ? 'Yes' : 'No').'</Doorman>'."\n");
				
				if(isset($row['RichDetails.DoublePaneWindows']))
					fputs($f, '			<DoublePaneWindows>'.($row['RichDetails.DoublePaneWindows'] ? 'Yes' : 'No').'</DoublePaneWindows>'."\n");
				
				if(isset($row['RichDetails.Elevator']))
					fputs($f, '			<Elevator>'.($row['RichDetails.Elevator'] ? 'Yes' : 'No').'</Elevator>'."\n");
					
				if(isset($row['RichDetails.ExteriorTypes']) && !empty($exterior_types)){
					fputs($f, '			<ExteriorTypes>'."\n");
					
					foreach($exterior_types as $exterior_type)
						fputs($f, '				<ExteriorType>'.$exterior_type.'</ExteriorType>'."\n");
						
					fputs($f, '			</ExteriorTypes>'."\n");
				}
				
				if(isset($row['RichDetails.Fireplace']))
					fputs($f, '			<Fireplace>'.$row['RichDetails.Fireplace'].'</Fireplace>'."\n");
					
				if(isset($row['RichDetails.FloorCoverings']) && !empty($floor_coverings)){	
					fputs($f, '			<FloorCoverings>'."\n");
					
					foreach($floor_coverings as $floor_covering)
						fputs($f, '				<FloorCovering>'.$floor_covering.'</FloorCovering>'."\n");
			
					fputs($f, '			</FloorCoverings>'."\n");
				}
				
				if(isset($row['RichDetails.Garden']))
					fputs($f, '			<Garden>'.($row['RichDetails.Garden'] ? 'Yes' : 'No').'</Garden>'."\n");
			
				if(isset($row['RichDetails.GatedEntry']))
					fputs($f, '			<GatedEntry>'.($row['RichDetails.GatedEntry'] ? 'Yes' : 'No').'</GatedEntry>'."\n");
				
				if(isset($row['RichDetails.Greenhouse']))
					fputs($f, '			<Greenhouse>'.($row['RichDetails.Greenhouse'] ? 'Yes' : 'No').'</Greenhouse>'."\n");
					
				if(isset($row['RichDetails.HeatingFuels']) && !empty($heating_fuels)){	
					fputs($f, '			<HeatingFuels>'."\n");
			
					foreach($heating_fuels as $heating_fuel)
						fputs($f, '				<HeatingFuel>'.$heating_fuel.'</HeatingFuel>'."\n");
					
					fputs($f, '			</HeatingFuels>'."\n");
				}
				
				if(isset($row['RichDetails.HeatingSystems']) && !empty($heating_systems)){	
					fputs($f, '			<HeatingSystems>'."\n");
					
					foreach($heating_systems as $heating_system)
						fputs($f, '				<HeatingSystem>'.$heating_system.'</HeatingSystem>'."\n");
					
					fputs($f, '			</HeatingSystems>'."\n");
				}
				
				if(isset($row['RichDetails.HottubSpa']))
					fputs($f, '			<HottubSpa>'.($row['RichDetails.HottubSpa'] ? 'Yes' : 'No').'</HottubSpa>'."\n");
				
				if(isset($row['RichDetails.Intercom']))
					fputs($f, '			<Intercom>'.($row['RichDetails.Intercom'] ? 'Yes' : 'No').'</Intercom>'."\n");
			
				if(isset($row['RichDetails.JettedBathTub']))
					fputs($f, '			<JettedBathTub>'.($row['RichDetails.JettedBathTub'] ? 'Yes' : 'No').'</JettedBathTub>'."\n");
				
				if(isset($row['RichDetails.Lawn']))
					fputs($f, '			<Lawn>'.($row['RichDetails.Lawn'] ? 'Yes' : 'No').'</Lawn>'."\n");
				
				if(isset($row['RichDetails.LegalDescription']) && !empty($row['RichDetails.LegalDescription']))
					fputs($f, '			<LegalDescription>'.str_replace($this->search, $this->replace, $row['RichDetails.LegalDescription']).'</LegalDescription>'."\n");
					
				if(isset($row['RichDetails.MotherInLaw']))	
					fputs($f, '			<MotherInLaw>'.($row['RichDetails.MotherInLaw'] ? 'Yes' : 'No').'</MotherInLaw>'."\n");
				
				if(isset($row['RichDetails.NumFloors']) && !empty($row['RichDetails.NumFloors']))
					fputs($f, '			<NumFloors>'.$row['RichDetails.NumFloors'].'</NumFloors>'."\n");
				
				if(isset($row['RichDetails.NumParkingSpaces']) && !empty($row['RichDetails.NumParkingSpaces']))
					fputs($f, '			<NumParkingSpaces>'.$row['RichDetails.NumParkingSpaces'].'</NumParkingSpaces>'."\n");
					
				if(isset($row['RichDetails.ParkingTypes']) && !empty($parking_types)){
					fputs($f, '			<ParkingTypes>'."\n");
					
					foreach($parking_types as $parking_type)
						fputs($f, '				<ParkingType>'.$parking_type.'</ParkingType>'."\n");
			
					fputs($f, '			</ParkingTypes>'."\n");
				}
				
				if(isset($row['RichDetails.Patio']))	
					fputs($f, '			<Patio>'.($row['RichDetails.Patio'] ? 'Yes' : 'No').'</Patio>'."\n");
				
				if(isset($row['RichDetails.Pond']))	
					fputs($f, '			<Pond>'.($row['RichDetails.Pond'] ? 'Yes' : 'No').'</Pond>'."\n");
				
				if(isset($row['RichDetails.Pool']))
					fputs($f, '			<Pool>'.($row['RichDetails.Pool'] ? 'Yes' : 'No').'</Pool>'."\n");
				
				if(isset($row['RichDetails.Porch']))
					fputs($f, '			<Porch>'.($row['RichDetails.Porch'] ? 'Yes' : 'No').'</Porch>'."\n");
				
				if(isset($row['RichDetails.RoofTypes']) && !empty($roof_types)){
					fputs($f, '			<RoofTypes>'."\n");
					
					foreach($roof_types as $roof_type)
						fputs($f, '				<RoofType>'.$roof_type.'</RoofType>'."\n");
						
					fputs($f, '			</RoofTypes>'."\n");
				}
				
				if(isset($row['RichDetails.RoomCount']) && !empty($row['RichDetails.RoomCount']))
					fputs($f, '			<RoomCount>'.$row['RichDetails.RoomCount'].'</RoomCount>'."\n");
				
				if(isset($row['RichDetails.Rooms']) && !empty($rooms)){
					fputs($f, '			<Rooms>'."\n");
			
					foreach($rooms as $room)
						fputs($f, '				<Room>'.$room.'</Room>'."\n");
			
					fputs($f, '			</Rooms>'."\n");
				}
				
				if(isset($row['RichDetails.RvParking']))
					fputs($f, '			<RvParking>'.($row['RichDetails.RvParking'] ? 'Yes' : 'No').'</RvParking>'."\n");
					
				if(isset($row['RichDetails.Sauna']))	
					fputs($f, '			<Sauna>'.($row['RichDetails.Sauna'] ? 'Yes' : 'No').'</Sauna>'."\n");
				
				if(isset($row['RichDetails.SecuritySystem']))	
					fputs($f, '			<SecuritySystem>'.($row['RichDetails.SecuritySystem'] ? 'Yes' : 'No').'</SecuritySystem>'."\n");
				
				if(isset($row['RichDetails.Skylight']))	
					fputs($f, '			<Skylight>'.($row['RichDetails.Skylight'] ? 'Yes' : 'No').'</Skylight>'."\n");
				
				if(isset($row['RichDetails.SportsCourt']))	
					fputs($f, '			<SportsCourt>'.($row['RichDetails.SportsCourt'] ? 'Yes' : 'No').'</SportsCourt>'."\n");
				
				if(isset($row['RichDetails.SprinklerSystem']))	
					fputs($f, '			<SprinklerSystem>'.($row['RichDetails.SprinklerSystem'] ? 'Yes' : 'No').'</SprinklerSystem>'."\n");
					
				if(isset($row['RichDetails.VaultedCeiling']))	
					fputs($f, '			<VaultedCeiling>'.($row['RichDetails.VaultedCeiling'] ? 'Yes' : 'No').'</VaultedCeiling>'."\n");
			
				if(isset($row['RichDetails.ViewTypes']) && !empty($view_types)){	
					fputs($f, '			<ViewTypes>'."\n");
					
					foreach($view_types as $view_type)
						fputs($f, '				<ViewType>'.$view_type.'</ViewType>');
				
					fputs($f, '			</ViewTypes>'."\n");
				}
				
				if(isset($row['RichDetails.Waterfront']))	
					fputs($f, '			<Waterfront>'.($row['RichDetails.Waterfront'] ? 'Yes' : 'No').'</Waterfront>'."\n");
				
				if(isset($row['RichDetails.Wetbar']))	
					fputs($f, '			<Wetbar>'.($row['RichDetails.Wetbar'] ? 'Yes' : 'No').'</Wetbar>'."\n");
			
				if(isset($row['RichDetails.WhatOwnerLoves']) && !empty($row['RichDetails.WhatOwnerLoves']))	
					fputs($f, '			<WhatOwnerLoves>'.str_replace($this->search, $this->replace, $row['RichDetails.WhatOwnerLoves']).'</WhatOwnerLoves>'."\n");
					
				if(isset($row['RichDetails.Wired']))
					fputs($f, '			<Wired>'.($row['RichDetails.Wired'] ? 'Yes' : 'No').'</Wired>'."\n");
			
				if(isset($row['RichDetails.YearUpdated']) && !empty($row['RichDetails.YearUpdated']) && strlen($row['RichDetails.YearUpdated'])>4)	
					fputs($f, '			<YearUpdated>'.$row['RichDetails.YearUpdated'].'</YearUpdated>'."\n");
					
				if(isset($row['RichDetails.FitnessCenter']))
					fputs($f, '			<FitnessCenter>'.($row['RichDetails.FitnessCenter'] ? 'Yes' : 'No').'</FitnessCenter>'."\n");
				
				if(isset($row['RichDetails.BasketballCourt']))
					fputs($f, '			<BasketballCourt>'.($row['RichDetails.BasketballCourt'] ? 'Yes' : 'No').'</BasketballCourt>'."\n");
				
				if(isset($row['RichDetails.TennisCourt']))
					fputs($f, '			<TennisCourt>'.($row['RichDetails.TennisCourt'] ? 'Yes' : 'No').'</TennisCourt>'."\n");
				
				if(isset($row['RichDetails.NearTransportation']))
					fputs($f, '			<NearTransportation>'.($row['RichDetails.NearTransportation'] ? 'Yes' : 'No').'</NearTransportation>'."\n");
					
				if(isset($row['RichDetails.ControlledAccess']))
					fputs($f, '			<ControlledAccess>'.($row['RichDetails.ControlledAccess'] ? 'Yes' : 'No').'</ControlledAccess>'."\n");
			
				if(isset($row['RichDetails.Over55ActiveCommunity']))
					fputs($f, '			<Over55ActiveCommunity>'.($row['RichDetails.Over55ActiveCommunity'] ? 'Yes' : 'No').'</Over55ActiveCommunity>'."\n");
				
				if(isset($row['RichDetails.AssistedLivingCommunity']))
					fputs($f, '			<AssistedLivingCommunity>'.($row['RichDetails.AssistedLivingCommunity'] ? 'Yes' : 'No').'</AssistedLivingCommunity>'."\n");
				
				if(isset($row['RichDetails.Storage']))
					fputs($f, '			<Storage>'.($row['RichDetails.Storage'] ? 'Yes' : 'No').'</Storage>'."\n");
					
				if(isset($row['RichDetails.FencedYard']))
					fputs($f, '			<FencedYard>'.($row['RichDetails.FencedYard'] ? 'Yes' : 'No').'</FencedYard>'."\n");

				if(isset($row['RichDetails.PropertyName']) && !empty($row['RichDetails.PropertyName']))	
					fputs($f, '			<PropertyName>'.str_replace($this->search, $this->replace, $row['RichDetails.PropertyName']).'</PropertyName>'."\n");
					
				if(isset($row['RichDetails.Furnished']))
					fputs($f, '			<Furnished>'.($row['RichDetails.Furnished'] ? 'Yes' : 'No').'</Furnished>'."\n");
					
				if(isset($row['RichDetails.HighspeedInternet']))
					fputs($f, '			<HighspeedInternet>'.($row['RichDetails.HighspeedInternet'] ? 'Yes' : 'No').'</HighspeedInternet>'."\n");
					
				if(isset($row['RichDetails.OnsiteLaundry']))
					fputs($f, '			<OnsiteLaundry>'.($row['RichDetails.OnsiteLaundry'] ? 'Yes' : 'No').'</OnsiteLaundry>'."\n");
				
				if(isset($row['RichDetails.CableSatTV']))
					fputs($f, '			<CableSatTV>'.($row['RichDetails.CableSatTV'] ? 'Yes' : 'No').'</CableSatTV>'."\n");
					
				if(isset($row['RichDetails.TaxAmount']) && !empty($row['RichDetails.TaxAmount'])){
					fputs($f, '			<Taxes>'."\n");
					fputs($f, '				<Tax>'."\n");
					
					foreach((array)$row['RichDetails.TaxAmount'] as $tax_amount){
						//fputs($f, '					<Year>'.$row['TaxYear'].'</Year>'."\n");
						fputs($f, '					<Amount>'.floatval($tax_amount).'</Amount>'."\n");
					}
					
					fputs($f, '				</Tax>'."\n");
					fputs($f, '			</Taxes>'."\n");
				}
				
				fputs($f, '		</RichDetails>'."\n");			
			}
			
			fputs($f, '	</Listing>'."\n");
		}		
		fputs($f, '</Listings>'."\n");		
		fclose($f);
	}
	
	/**
	 * Validate data
	 * @param array $data data for validation
	 */
	public function validate_data($data){
		$return = array("errors" => array(), "data" => array());
		
		$this->CI->config->load("reg_exps", TRUE);
		
		$email_expr = $this->CI->config->item("email", "reg_exps");
		
		$return["data"] = $data;
		if(!isset($data["Location.StreetAddress"]) || empty($data["Location.StreetAddress"])) $return["errors"][] = "Location.StreetAddress";
		if(!isset($data["Location.City"]) || empty($data["Location.City"])) $return["errors"][] = "Location.City";
		if(!isset($data["Location.State"]) || empty($data["Location.State"])) $return["errors"][] = "Location.State";
		if(!isset($data["Location.Zip"]) || empty($data["Location.Zip"])) $return["errors"][] = "Location.Zip";
		if(!isset($data["ListingDetails.Status"]))
			if(!isset($data["ListingSold"]) || empty($data["ListingSold"])) 
				if($data["ListingType"] != "rent" && $data["ListingType"] != "lease")
					$return["errors"][] = "ListingDetails.Status";
		if(!isset($data["ListingDetails.Price"]) || empty($data["ListingDetails.Price"])) $return["errors"][] = "ListingDetails.Price";
		if(!isset($data["BasicDetails.PropertyType"]) || !in_array($data["BasicDetails.PropertyType"], $this->property_types)) $return["errors"][] = "BasicDetails.PropertyType";
		if(!isset($data["Agent.EmailAddress"]) || empty($data["Agent.EmailAddress"]) || !preg_match($email_expr, $data["Agent.EmailAddress"])) $return["errors"][] = "Agent.EmailAddress";
		if(!isset($data["Office.BrokerPhone"]) || empty($data["Office.BrokerPhone"])) $return["errors"][] = "Office.BrokerPhone";

		if(isset($return['data']['Pictures']) && !empty($return['data']['Pictures'])) $return['data']['Pictures'] = explode('|', $return['data']['Pictures']);
		if(isset($return['data']['VirtualTourUrl']) && !empty($return['data']['VirtualTourUrl'])) $return['data']['VirtualTourUrl'] = array_shift(explode('|', $return['data']['VirtualTourUrl']));
		
		return $return;
	}
}
