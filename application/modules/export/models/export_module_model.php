<?php  
/**
 * Export module model
 * 
 * @package PG_RealEstate
 * @subpackage application
 * @category	modules
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/

if(!defined("BASEPATH")) exit("No direct script access allowed");

define("EXPORT_MODULES_TABLE", DB_PREFIX."export_modules");

class Export_module_model extends Model{
	
	/**
	 * Link to CodeIgniter model
	 * @var object
	 */
	private $CI;
	
	/**
	 * Link to database object
	 * @var object
	 */
	private $DB;

	/**
	 * Export data
	 * @var array
	 */
	private $attrs = array(
		"id", 
		"module", 
		"model",
		"callback_get_fields", 
		"callback_get_admin_form", 
		"callback_get_user_form",
		"callback_process_form", 
		"callback_export_data",
		"sorter",
		"date_created", 
		"date_modified", 
	);

	/**
	 * Cache module data
	 * @var array
	 */
	private $module_cache = array();

	/**
	 * Constructor
	 * @return Export_module_model
	 */
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
		$this->DB = &$this->CI->db;
	}

	/**
	 * Return module by id
	 * @param string $id module id
	 * @param boolean $formatted format results
	 * @return array
	 */
	public function get_module_by_id($id, $formatted=true){
		if(empty($this->module_cache[$id])){
			$this->DB->select(implode(", ", $this->attrs))->from(EXPORT_MODULES_TABLE)->where("id", $id);
			$results = $this->DB->get()->result_array();
			if(!empty($results) && is_array($results)){
				if($formatted){
					$this->module_cache[$id] = $this->format_module($results[0]);
				}else{
					return $results[0];
				}
			}
		}
		return $this->module_cache[$id];
	}

	/**
	 * Return list of modules as array
	 * @param boolean $formatted format results
	 * @return array
	 */
	public function get_modules($formatted=true){
		$data = array();
		$this->DB->select(implode(", ", $this->attrs))->from(EXPORT_MODULES_TABLE)->order_by('sorter');
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			foreach($results as $result){
				if($formatted){
					$data[] = $this->format_module($result);
				}else{
					$data[] = $result;
				}
			}
		}
		return $data;
	}
	
	/**
	 * Save module data
	 * @param array $data
	 * @return boolean
	 */
	public function save_module($id, $data){
		if(!$id){
			$data["date_created"] = date("Y-m-d H:i:s");
			$data["date_modified"] = date("Y-m-d H:i:s");
			$this->DB->insert(EXPORT_MODULES_TABLE, $data);		
			$id = $this->DB->insert_id();
		}else{
			$this->DB->where("id", $id);
			$this->DB->update(EXPORT_MODULES_TABLE, $data);
		}
		return $id;
	}
	
	/**
	 * Remove export module by id
	 * @param string $module_gid module guid
	 */
	public function remove_module($module_gid){
		$this->DB->where("module", $module_gid);
		$this->DB->delete(EXPORT_MODULES_TABLE);
	}
	
	/**
	 * Validate module data
	 * @param array $data data for validation
	 * @return array
	 */
	public function validate_module($id, $data){
		$return = array("errors"=> array(), "data" => array());

		if(isset($data['id'])){
			$return['data']['id'] = intval($data['id']);
			if(empty($return['data']['id'])) unset($return['data']['id']);
		}
		
		if(isset($data['module'])){
			$return['data']['module'] = trim(strip_tags($data["module"]));
			if(empty($return['data']['module'])) $return["errors"][] = l("error_empty_module", "export");
		}elseif(!$id){
			$return["errors"][] = l("error_empty_module", "export");
		}
		
		if(isset($data['model'])){
			$return['data']['model'] = trim(strip_tags($data["model"]));
			if(empty($return['data']['model'])) $return["errors"][] = l("error_empty_model", "export");
		}elseif(!$id){
			$return["errors"][] = l("error_empty_model", "export");
		}
		
		if(isset($data['callback_get_fields'])){
			$return['data']['callback_get_fields'] = trim(strip_tags($data["callback_get_fields"]));
			if(empty($return['data']['callback_get_fields'])) $return["errors"][] = l("error_empty_callback_get_fields", "export");
		}elseif(!$id){
			$return["errors"][] = l("error_empty_callback_get_fields", "export");
		} 
		
		if(isset($data['callback_get_admin_form'])){
			$return['data']['callback_get_admin_form'] = trim(strip_tags($data["callback_get_admin_form"]));
			if(empty($return['data']['callback_get_admin_form'])) $return["errors"][] = l("error_empty_callback_get_admin_form", "export");
		}elseif(!$id){
			$return["errors"][] = l("error_empty_callback_get_admin_form", "export");
		} 
		
		if(isset($data['callback_get_user_form'])){
			$return['data']['callback_get_user_form'] = trim(strip_tags($data["callback_get_user_form"]));
			if(empty($return['data']['callback_get_user_form'])) $return["errors"][] = l("error_empty_callback_get_user_form", "export");
		}elseif(!$id){
			$return["errors"][] = l("error_empty_callback_get_user_form", "export");
		}
		
		if(isset($data['callback_process_form'])){
			$return['data']['callback_process_form'] = trim(strip_tags($data["callback_process_form"]));
			if(empty($return['data']['callback_process_form'])) $return["errors"][] = l("error_empty_callback_process_form", "export");
		}elseif(!$id){
			$return["errors"][] = l("error_empty_callback_process_form", "export");
		}
		
		if(isset($data['callback_export_data'])){
			$return['data']['callback_export_data'] = trim(strip_tags($data["callback_export_data"]));
			if(empty($return['data']['callback_export_data'])) $return["errors"][] = l("error_empty_callback_export_data", "export");
		}elseif(!$id){
			$return["errors"][] = l("error_empty_callback_export_data", "export");
		}
		
		if(isset($data['sorter'])){
			$return['data']['sorter'] = intval($data['sorter']);
		}
		
		if(isset($data['date_created'])){
			$value = strtotime($data['date_created']);
			if($value > 0) $return['data']['date_created'] = date("Y-m-d", $value);
		}
		
		if(isset($data['date_modified'])){
			$value = strtotime($data['date_modified']);
			if($value > 0) $return['data']['date_modified'] = date("Y-m-d", $value);
		}
		
		return $return;
	}
	
	/**
	 * Format module data
	 * @param array
	 * @return array
	 */
	public function format_module($data){
		$data["output_name"] = l("module_".$data["module"], "export");
		return $data;
	}
	
	/**
	 * Return default module
	 * @return array
	 */
	public function format_default_module($module_id){
		return null;
	}
	
	/**
	 * Return custom fields which available for export
	 * @param integer $module_id module identifier
	 * @return array
	 */
	public function get_module_fields($module_id){
		$module = $this->get_module_by_id($module_id);
		if(empty($module["module"]) || empty($module["model"]) ||
		   empty($module["callback_get_fields"])) return array();
		$this->CI->load->model($module["module"]."/models/".$module["model"], $module["model"]);
		$fields = $this->CI->{$module["model"]}->{$module["callback_get_fields"]}();
		return $fields;
	}
	
	/**
	 * Return admin search form
	 * @param integer $module_id module identifier
	 * @param array $data selected values
	 * @param boolean $admin_mode admin mode
	 * @return array
	 */
	public function get_search_form($module_id, $data, $admin_mode=true){
		$module = $this->get_module_by_id($module_id);
		if(empty($module["module"]) || empty($module["model"]) || empty($module["callback_get_admin_form"])) return '';
		$this->CI->load->model($module["module"]."/models/".$module["model"], $module["model"]);
		if($admin_mode){
			return $this->CI->{$module["model"]}->{$module["callback_get_admin_form"]}($data);
		}else{
			return $this->CI->{$module["model"]}->{$module["callback_get_user_form"]}($data);
		}
	}
	
	/**
	 * Process admin search form
	 * @param integer $module_id module identifier
	 * @param boolean $admin_mode admin mode
	 * @return array
	 */
	public function process_search_form($module_id, $admin_mode=true){
		$module = $this->get_module_by_id($module_id);
		if(empty($module["module"]) || empty($module["model"]) ||
		   empty($module["callback_get_admin_form"])) return array();
		$this->CI->load->model($module["module"]."/models/".$module["model"], $module["model"]);
		return $this->CI->{$module["model"]}->{$module["callback_process_form"]}($admin_mode);
	}
	
	/**
	 * Return export data from module
	 * @param integer $module_id module identifier
	 * @param array $data selected values
	 * @return array
	 */
	public function export_data($module_id, $data=array(), $relations=array(), $user_id=0){
		$module = $this->get_module_by_id($module_id);
		if(empty($module["module"]) || empty($module["model"]) ||
		   empty($module["callback_export_data"])) return array();
		$this->CI->load->model($module["module"]."/models/".$module["model"], $module["model"]);
		return $this->CI->{$module["model"]}->{$module["callback_export_data"]}($data, $relations, $user_id);
	}
	
	/**
	 * Update languages
	 * @param array $data
	 * @param array $langs_file
	 * @param array $langs_ids
	 */
	public function update_lang($data, $langs_file, $langs_ids){
		foreach($data as $value){
			$this->CI->pg_language->pages->set_string_langs(
				"export", 
				$value, 
				$langs_file[$value], 
				$langs_ids);
		}
	}
	
	/**
	 * Export languages
	 * @param array $data
	 * @param array $langs_ids
	 */
	public function export_lang($data, $langs_ids){
		$langs = array();
		return array_merge($langs, $this->CI->pg_language->export_langs("export", (array)$data, $langs_ids));
	}
}
