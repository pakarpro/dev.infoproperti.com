<?php
/**
* Export model
* 
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/

define("EXPORT_TABLE", DB_PREFIX."export");

class Export_model extends Model{

	/**
	 * link to CodeIgniter object
	 * @var object
	 */
	private $CI;

	/**
	 * link to DataBase object
	 * @var object
	 */
	private $DB;

	/**
	 * Table fields
	 * @var array
	 */
	var $_fields = array(
		"id",
		"gid_driver",
		"id_object",
		"name",
		"relations",
		"admin_form_data",
		"output_type",
		"file_format",
		"append_date",
		"scheduler",
		"published",
		"editable",
		"status",
		"date_created",
	);
	
	/**
	 * Format settings
	 * @var array
	 */
	private $format_settings = array(
		"use_format"  => true,
		"get_driver"  => true,
		"get_module"  => true,
	);
	
	/**
	 * Cache array for used drivers
	 * @var array
	 */
	private $driver_cache = array();
	
	/**
	 * Cache array for used modules
	 * @var array
	 */
	private $module_cache = array();
	
	/**
	 * Temp export path
	 * @var string
	 */
	private $save_path = "export/";
	
	/**
	 * Output types
	 * @var array
	 */
	public $output_types = array("file", "browser");

	/**
	 * Constructor
	 *
	 * return Export_model
	 * required Export_driver_model
	 */
	function __construct(){
		parent::Model();
		$this->CI = & get_instance();
		$this->DB = &$this->CI->db;
		$this->CI->load->model("export/models/Export_driver_model", "Export_driver_model");
		$this->CI->load->model("export/models/Export_module_model", "Export_module_model");
	}

	/**
	 * Get export driver by GUID
	 * @param string $driver_gid driver guid
	 * @return array
	 */
	public function get_driver_by_gid($driver_gid){
		if(!isset($this->driver_cache[$driver_gid])){
			$driver_data = $this->CI->Export_driver_model->get_driver_by_gid($driver_gid);
			if(!is_array($driver_data) || !count($driver_data)) return false;
			$this->driver_cache[$driver_data["id"]] = $driver_data;
			$this->driver_cache[$driver_gid] = $driver_data;
		}

		if(is_array($this->driver_cahce[$driver_gid]) && count($this->driver_cache[$type_gid])){
			return $this->driver_cahce[$driver_gid];
		}else{
			return false;
		}
	}


	/**
	 * Get export selection by ID
	 * @param integer $id export selection identifier
	 * @param 
	 */ 
	public function get_selection_by_id($id, $formatted=false){
		$id = intval($id);
		
		$this->DB->select(implode(", ", $this->_fields))->from(EXPORT_TABLE)->where("id", $id);
		
		//_compile_select;
		$result = $this->DB->get()->result_array();
		if(!empty($result)){
			$rt = array($result[0]);
			if($formatted) $rt = $this->format_selection($rt);
			return $rt[0];
		}else
			return false;
	}
	
	/**
	 * Save selection
	 * @param array $data
	 * @return boolean
	 */
	public function save_selection($id, $data){
		if(!$id){
			$data["date_created"] = date("Y-m-d H:i:s");
			$this->DB->insert(EXPORT_TABLE, $data);
			$id = $this->DB->insert_id();
		}else{
			$this->DB->where("id", $id);
			$this->DB->update(EXPORT_TABLE, $data);
		}
		return $id;
	}
	
	/**
	 * Remove selection by ID
	 * @param integer $id selection identifier
	 */ 
	public function delete_selection($id){
		$this->DB->where("id", $id);
		$this->DB->delete(EXPORT_TABLE);
	}	
	
	/**
	 * Activate selection
	 * @param integer $selection_id selection identifier
	 * @return boolean
	 */
	public function activate_selection($selection_id){
		$selection = $this->get_selection_by_id($selection_id, true);
		if(!is_array($selection["relations"]) || empty($selection["relations"])) return false;
		/*$elements = array();
		foreach((array)$selection["driver"]["elements"] as $element){
			$elements[] = $element["name"];
		}
		$relations = array();
		foreach((array)$selection["relations"] as $relation){
			$relations[] = $relation["name"];
		}
		$required = array_diff($elements, $relations);
		if(!empty($required)) return false;*/
		$data["status"] = "1";
		$this->save_selection($selection_id, $data);
		return true;
	}
	
	/**
	 * Deactivate selection
	 * @param integer $selection_id selection identifier
	 * @return boolean
	 */
	public function deactivate_selection($selection_id){
		$data["status"] = "0";
		$this->save_selection($selection_id, $data);
		return true;
	}
	
	/**
	 * Return modules filter
	 * @param integer $module_id module identifier
	 * @return array
	 */
	private function _get_selections_by_module($module_id){
		if(!$object) return array();
		$params["where"]["id_module"] = $module_id;
		return $params;
	}
	
	/**
	 * Return drivers filter
	 * @param string $driver_gid driver guid
	 * @return array
	 */
	private function _get_selections_by_driver($driver_gid){
		if(!$object) return array();
		$params["where"]["gid_driver"] = $driver_gid;
		return $params;
	}
	
	/**
	 * Return active selections filter
	 * @param boolean $use use filter
	 * @return array
	 */
	private function _get_selections_by_active($use=true){
		if(!$use) return array();
		$params["where"]["status"] = 1;
		return $params;
	}
	
	/**
	 * Return not active listings filter
	 * @param boolean $use use filter
	 * @return array
	 */
	private function _get_selections_by_not_active($use=true){
		if(!$use) return array();
		$params["where"]["status"] = 0;
		return $params;
	}
	
	/**
	 * Return export selections as array
	 * @param integer $page
	 * @param string $limits 
	 * @param array $order_by
	 * @param array $params
	 * @return array
	 */
	private function _get_selections_list($page=null, $limits=null, $order_by=null, $params=array()){
		$this->DB->select(implode(", ", $this->_fields));
		$this->DB->from(EXPORT_TABLE);
		
		if(isset($params["where"]) && is_array($params["where"]) && count($params["where"])){
			foreach($params["where"] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}
		
		if(isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])){
			foreach($params["where_in"] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])){
			foreach($params["where_sql"] as $value){
				$this->DB->where($value);
			}
		}

		if (is_array($order_by) && count($order_by) > 0){
			foreach ($order_by as $field => $dir){
				if (in_array($field, $this->_fields)){
					$this->DB->order_by($field." ".$dir);
				}
			}
		} else if ($order_by){
			$this->DB->order_by($order_by);
		}

		if(!is_null($page)){
			$page = intval($page)?intval($page):1;
			$this->DB->limit($limits, $limits*($page-1));
		}

		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			foreach($results as $r){
				$data[] = $r;
			}
			return $this->format_selection($data);
		}
		return array();
	}
	
	/**
	 * Return selections as array
	 * @param array $params
	 * @return integer
	 */
	private function _get_selections_count($params=null){
		$this->DB->select("COUNT(*) AS cnt");
		$this->DB->from(EXPORT_TABLE);

		if(isset($params["where"]) && is_array($params["where"]) && count($params["where"])){
			foreach($params["where"] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])){
			foreach($params["where_in"] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])){
			foreach($params["where_sql"] as $value){
				$this->DB->where($value);
			}
		}

		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			return intval($results[0]["cnt"]);
		}
		return 0;
	}
	
	/**
	 * Return selections as array
	 * @param array $filters
	 * @param integer $page
	 * @param string $limits 
	 * @param array $order_by
	 * @return array
	 */
	public function get_selections_list($filters=array(), $page=null, $limits=null, $order_by=null){
		$params = array();
		foreach($filters as $filter=>$value){
			$params = array_merge_recursive($params, $this->{"_get_selections_by_".$filter}($value));
		}
		return $this->_get_selections_list($page, $limits, $order_by);
	}
	
	/**
	 * Return number of selections
	 * @param string $type_gid
	 * @return integer
	 */
	public function get_selections_count($filters=array()){
		$params = array();
		foreach($filters as $filter=>$value){
			$params = array_merge_recursive($params, $this->{"_get_selections_by_".$filter}($value));
		}
		return $this->_get_selections_count($params);
	}
	
	/**
	 * Validate selection data
	 * @param array $data
	 * @param boolean $required
	 * @return array
	 */
	public function validate_selection($id, $data){
		$return = array("errors"=> array(), "data" => array());
		
		if(isset($data["name"])){
			$return["data"]["name"] = trim(strip_tags($data['name']));
			if(empty($return["data"]["name"])) $return["errors"][] = l("error_empty_name", "export");
		}elseif(!$id){
			$return["errors"][] = l("error_empty_name", "export");
		}

		if(isset($data["gid_driver"])){
			$return["data"]["gid_driver"] = trim(strip_tags($data['gid_driver']));
			if( empty($return["data"]["gid_driver"]) || !($driver = $this->Export_driver_model->get_driver_by_gid($return["data"]["gid_driver"])) ){
				$return["errors"][] = l("error_empty_driver", "export");
			}
		}elseif(!$id){
			$return["errors"][] = l("error_empty_driver", "export");
		}

		if(isset($data["id_object"])){
			$return["data"]["id_object"] = intval($data['id_object']);
			if( empty($return["data"]["id_object"]) || !($module = $this->Export_module_model->get_module_by_id($return["data"]["id_object"])) ){
				$return["errors"][] = l("error_empty_object", "export");
			}
		}elseif(!$id){
			$return["errors"][] = l("error_empty_object", "export");
		}

		if(isset($data["relations"])){
			$return["data"]["relations"] = serialize($data['relations']);
		}elseif(!$id){
			//$return["errors"][] = l("error_empty_relations", "export");
		}

		if(isset($data["output_type"])){
			$return["data"]["output_type"] = trim(strip_tags($data['output_type']));
			if( empty($return["data"]["output_type"]) ){
				$return["errors"][] = l("error_empty_output_type", "export");
			}
		}elseif(!$id){
			$return["data"]["output_type"] = 'file';
			//$return["errors"][] = l("error_empty_output_type", "export");
		}
	
		if(isset($data["admin_form_data"])){
			if(!is_array($data["admin_form_data"])) $data["admin_form_data"] = array();
			$return["data"]["admin_form_data"] = serialize($data["admin_form_data"]);
		}
		
		if(isset($data["file_format"])){
			$return["data"]["file_format"] = trim(strip_tags($data['file_format']));
		}

		if(isset($data["scheduler"])){
			$return["data"]["scheduler"] = $data["scheduler"];
			if($return["data"]["output_type"] == "browser"){
				unset($return["data"]["scheduler"]);
			}elseif(empty($return["data"]["scheduler"])){
				$return["errors"][] = l("error_empty_scheduler", "export");
			}else{
				switch($return["data"]["scheduler"]["type"]){
					case "cron":
						$return["data"]["scheduler"] = array(
							"type" => "cron",
							"date" => strval($return["data"]["scheduler"]["date"]),
							"hours" => intval($return["data"]["scheduler"]["hours"]),
							"minutes" => intval($return["data"]["scheduler"]["minutes"]),
							"period" => strval($return["data"]["scheduler"]["period"]),
						);
						$return["data"]["scheduler"]["date_for_cron"] = strtotime($return["data"]["scheduler"]["date"]." ".$return["data"]["scheduler"]["hours"].":".$return["data"]["scheduler"]["minutes"]);	
						$return["data"]["scheduler"] = serialize($return["data"]["scheduler"]);
					break;
					case "intime":
						$return["data"]["scheduler"] = array(
							"type" => "intime",
							"date" => strval($return["data"]["scheduler"]["date"]),
							"hours" => intval($return["data"]["scheduler"]["hours"]),
							"minutes" => intval($return["data"]["scheduler"]["minutes"]),
						);
						$return["data"]["scheduler"]["date_for_cron"] = strtotime($return["data"]["scheduler"]["date"]." ".$return["data"]["scheduler"]["hours"].":".$return["data"]["scheduler"]["minutes"]);	
						$return["data"]["scheduler"] = serialize($return["data"]["scheduler"]);
					break;
					case "manual":
						$return["data"]["scheduler"] = serialize(array("type"=>"manual"));
					break;
					default:
						unset($return["data"]["scheduler"]);
					break;
				}
			}
		}
	
		if(isset($data["published"])){
			$return["data"]["published"] = $data["published"] ? 1 : 0;
		}
		
		if(isset($data["editable"])){
			$return["data"]["editable"] = $data["editable"] ? 1 : 0;
		}
		
		if(isset($data["append_date"])){
			$return["data"]["append_date"] = $data['append_date'] ? 1 : 0;
		}

		return $return;
	}
	
	/**
	 * Set format settings
	 * @param array $data
	 * @return array
	 */
	public function set_format_settings($data){
		foreach($data as $key=>$value){
			if(isset($this->format_settings[$key]))
				$this->format_settings[$key] = $value;
		}
	}	
		
	/**
	 * Format selection
	 * @param array $data
	 * @return array
	 */
	public function format_selection($data){
		if(!$this->format_settings["use_format"]){
			return $data;
		}
		
		$drivers_data = $this->CI->Export_driver_model->get_drivers();
		foreach($drivers_data as $driver_data){
			$this->driver_cache[$driver_data["gid"]] = $driver_data;
		}
		
		$modules_data = $this->CI->Export_module_model->get_modules();
		foreach($modules_data as $module_data){
			$this->module_cache[$module_data["id"]] = $module_data;
		}	
		
		$drivers_search = array();
		$modules_search = array();
		
		foreach($data as $key=>$selection){
			//get driver
			if($this->format_settings["get_driver"]){
				$drivers_search[] = $selection["gid_driver"];
			}
			//get module
			if($this->format_settings["get_module"]){
				$modules_search[] = $selection["id_object"];
			}
			
			$selection["relations"] = unserialize($selection["relations"]);
			$selection["admin_form_data"] = unserialize($selection["admin_form_data"]);
			if($selection["output_type"] == "file"){
				$selection["scheduler"] = unserialize($selection["scheduler"]);
				if(!isset($selection["scheduler"]["type"]))
					$selection["scheduler"] = array("type" => "manual");
			}else{
				unset($selection["scheduler"]);
			}
			
			$selection["output_name"] = $selection["name"];
			
			$data[$key] = $selection;
		}
	
		if($this->format_settings["get_driver"] && !empty($drivers_search)){
			foreach($data as $key=>$selection){
				$data[$key]["driver"] = (isset($this->driver_cache[$selection["gid_driver"]])) ? 
					$this->driver_cache[$selection["gid_driver"]] : $this->CI->Export_driver_model->format_default_driver($selection["gid_driver"]);
			}
		}
	
		if($this->format_settings["get_module"] && !empty($modules_search)){
			foreach($data as $key=>$selection){
				$data[$key]["module"] = (isset($this->module_cache[$selection["id_object"]])) ? 
					$this->module_cache[$selection["id_object"]] : $this->CI->Export_module_model->format_default_module($selection["id_object"]);
			}
		}
		
		return $data;
	}
	
	/**
	 * Create new export file
	 * @param string $selection_id selection identifier
	 */
	public function generate_selection($selection_id, $form_data=null, $user_id=0){
		@ini_set("max_execution_time", 0);
		
		$selection = $this->get_selection_by_id($selection_id, true);
		if(!$selection) return "";

		if(is_null($form_data)) $form_data = $selection['admin_form_data'];
		
		$model_name = "Export_".$selection["gid_driver"]."_model";
		$this->CI->load->model("export/models/drivers/".strtolower($model_name), $model_name);
		
		$archive = false;
		$attachements = array();
		
		$data = $this->CI->Export_module_model->export_data($selection["id_object"], $form_data, $selection["relations"], $user_id);

		foreach($data['data'] as $i=>$item){
			$row = array();
			foreach($selection["relations"] as $relation){
				$value = $item[$relation["link"]];
				switch($relation["type"]){
					case "text":
						if(isset($data['fk'][$i][$relation["link"]])){
							$value = $data['fk'][$i][$relation["link"]];
						}
						if(is_array($value)){
							foreach($value as $k=>$v){
								$value[$k] = strval(strip_tags($v));
							}
							$row[$relation["name"]] = implode('|', $value);	
						}else{
							$row[$relation["name"]] = strval(strip_tags($value));
						}
					break;
					case "int":
						$row[$relation["name"]] = intval($value);
					break;
					case "file":
						//add file to archive
						$archive = true;
						if(is_array($value)){
							foreach($value as $k=>$v){
								//if(!file_exists($v)) continue;
								$attachements[] = $v;
								$value[$k] = basename($v);								
							}
							$row[$relation["name"]] = implode("|", $value);
						}else{
							$attachements[] = $value;
							$row[$relation["name"]] = basename($value);
						}
					break;
					default:
						$row[$relation["name"]] = $value;
					break;					
				}
			}
	
			$result = $this->CI->{$model_name}->validate_data($row, $selection["driver"]["settings"]);
			if(!empty($result["errors"])){
				if(isset($data['fk'][$i])) unset($data['fk'][$i]);
				unset($data['data'][$i]);
			}else{
				$data['data'][$i] = $result["data"];
			}
		}

		$data = $data['data'];

		$columns = array();
		foreach($selection["relations"] as $relation){
			$columns[] = $relation["name"];
		}
		
		array_unshift($data, $columns);
		
		$path = TEMPPATH.$this->save_path.'/'.$selection['id'].'/';
		if(!is_dir($path)){
			@mkdir($path, 0777, TRUE);
		}
		$filename = tempnam($path, $selection["gid_driver"]);
		$this->CI->{$model_name}->generate($filename, $data, $selection["driver"]["settings"]);

		$content_file = $this->CI->{$model_name}->content_type;
		$output_filename = $this->CI->{$model_name}->get_filename();

		if($archive){
			$this->CI->load->library("zip");						
						
			$this->CI->zip->add_data($output_filename, file_get_contents($filename));
			unlink($filename);
			
			foreach($attachements as $attachement){
				$content = @file_get_contents($attachement);
				if(!$content) continue;
				$this->CI->zip->add_data(basename($attachement), $content);
			}
			
			$filename = tempnam($path, $selection["gid_driver"]);
			$this->CI->zip->archive($filename);
			
			$content_file = "application/octet-stream";
			$output_filename = "export.zip";
			
		}

		switch($selection["output_type"]){
			case "browser":
				header("Content-Description: File Transfer");
				header("Content-Type: ".$content_file);
				header("Content-Disposition: attachment; filename=".$output_filename);
				header("Content-Transfer-Encoding: binary");
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				header("Pragma: public");
				header("Content-Length: ".filesize($filename));
				echo file_get_contents($filename);
				unlink($filename);	
				exit;
			break;
			case "file":
				if($selection["file_format"]){
					$new_filename = /*strftime(*/$selection["file_format"]/*)*/;
				}else{
					$new_filename = $output_filename;
				}
				if($selection['append_date']){
					$new_filename = date('Y-m-d_H_i').'_'.$new_filename;
				}
				$path = UPLOAD_DIR.$this->save_path.'/'.$selection['id'].'/';
				if(!is_dir($path)){
					@mkdir($path, 0777, TRUE);
				}
				$new_filename = $path.$new_filename;
				copy($filename, $new_filename);
				unlink($filename);	
			break;
		}
		
	}
	
	/**
	 * Return settings for seo
	 * @param string $method
	 * @param integer $lang_id 
	 * @return array
	 */
	public function get_seo_settings($method="", $lang_id=""){
		if(!empty($method)){
			return $this->_get_seo_settings($method, $lang_id);
		}else{
			$actions = array("index", "advanced");
			$return = array();
			foreach($actions as $action){
				$return[$action] = $this->_get_seo_settings($action, $lang_id);
			}
			return $return;
		}
	}

	/**
	 * Return setting for seo (internal)
	 * @return array
	 */
	private function _get_seo_settings($method, $lang_id=""){
		if($method == "index"){
			return array(
				"title" => l("seo_tags_index_title", "export", $lang_id, "seo"),
				"keyword" => l("seo_tags_index_keyword", "export", $lang_id, "seo"),
				"description" => l("seo_tags_index_description", "export", $lang_id, "seo"),
				"templates" => array("id"),
				"header" => l("seo_tags_export_header", "export", $lang_id, "seo"),
				"url_vars" => array(
					"id" => array("id" => "numeric"),
				),
			);
		}elseif($method == "advanced"){
			return array(
				"title" => l("seo_tags_advanced_title", "export", $lang_id, "seo"),
				"keyword" => l("seo_tags_advanced_keyword", "export", $lang_id, "seo"),
				"description" => l("seo_tags_advanced_description", "export", $lang_id, "seo"),
				"templates" => array("id"),
				"header" => l("seo_tags_export_header", "export", $lang_id, "seo"),
				"url_vars" => array(
					"id" => array("id" => "numeric"),
				),
			);
		}
	}

	/**
	 * Get alias field for seo
	 * @param string $var_name_from
	 * @param string $var_name_to
	 * @param mixed $value
	 * @return mixed
	 */
	public function request_seo_rewrite($var_name_from, $var_name_to, $value){
		$user_data = array();

		if($var_name_from == $var_name_to){
			return $value;
		}

		if($var_name_to == "id"){
			return $user_data["id"];
		}
	}

	/**
	 * Return urls of pages for seo sitemaps
	 * @return array
	 */
	public function get_sitemap_xml_urls(){
		$this->CI->load->helper("seo");
		$return = array(
			
		);
		
		return $return;
	}

	/**
	 * Export active selections
	 */
	public function cron_export(){
		$subscriptions_list = $this->get_selections_list(array("active"=>1));
		foreach($subscriptions_list as $key => $value){
			if($value["scheduler"]["type"] == "manual" ||
			   $value["scheduler"]["date_for_cron"] == 0 ||
			   $value["scheduler"]["date_for_cron"] > time()) continue;
		
			$this->generate_selection($value["id"], $value["admin_form_data"]);
			switch($value["scheduler"]["type"]){
				case "intime":
					$value["scheduler"]["date_for_cron"] = 0;
				break;
				case "cron":
					$d = $value["scheduler"]["date_for_cron"];
					switch($value["scheduler"]["period"]){
						case "day":
							$value["scheduler"]["date_for_cron"] = mktime(date("H", $d), date("i", $d), date("s", $d), date("n", $d), date("j", $d) + 1, date("Y", $d));
						break;
						case "week":
							$value["scheduler"]["date_for_cron"] = mktime(date("H", $d), date("i", $d), date("s", $d), date("n", $d), date("j", $d) + 7, date("Y", $d));
						break;
						case "month":
							$value["scheduler"]["date_for_cron"] = mktime(date("H", $d), date("i", $d), date("s", $d), date("n", $d) + 1, date("j", $d), date("Y", $d));
						break;
					}
				break;
			}
			
			$data = array("scheduler" => $value["scheduler"]);
			$validate_data = $this->validate_selection($value["id"], $data);
			if(!empty($validate_data['errors'])) continue;
			$this->save_selection($value["id"], $validate_data['data']);
		}
	}
}
