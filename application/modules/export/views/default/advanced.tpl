{include file="header.tpl" load_type="ui"}

{include file="left_panel.tpl" module="start"}

<div class="rc">
	<div class="content-block">
		<h1>{l i='header_my_export' gid='export'}</h1>
		
		<div class="edit_block">

		<form method="post" action="{seolink module='export' method='advanced' data=$data}" name="save_form" enctype="multipart/form-data">
			<div class="edit-form n150">
				{$export_form}
			</div>
			<div class="b"><input type="submit" name="btn_save" value="{l i='btn_save' gid='start' type='button'}"></div>
		</form>	
		
		</div>
	</div>
</div>
<div class="clr"></div>

{include file="footer.tpl"}
