{include file="header.tpl"}
<form method="post" action="{$data.action}" name="save_form">
	<div class="edit-form n150">
		<div class="row header">{if $add_flag}{l i='admin_header_driver_field_create' gid='export'}{else}{l i='admin_header_driver_field_edit' gid='export'}{/if}</div>
		<div class="row">
			<div class="h">{l i='field_setting_name' gid='export'}:&nbsp;* </div>
			<div class="v"><input type="text" name="data[name]" value="{$data.name|escape}"></div>
		</div>
		<div class="row">
			<div class="h">{l i='field_setting_type' gid='export'}:&nbsp;* </div>
			<div class="v">
				<select name="data[type]">
					{foreach item=item from=$types}
					<option value="{$item|escape}" {if $item eq $data.type}selected{/if}>{l i='field_type_'+$item gid='export'}</option>
					{/foreach}
				</select>
			</div>
		</div>
		{*<div class="row">
			<div class="h">{l i='field_setting_required' gid='export'}: </div>
			<div class="v">
				<input type="hidden" name="data[required]" value="0">
				<input type="checkbox" name="data[required]" value="1" {if $data.required}checked{/if}>
			</div>
		</div>*}
	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="{l i='btn_save' gid='start' type='button'}"></div></div>
	<a class="cancel" href="{$site_url}admin/export/driver_fields/{$driver_gid}">{l i='btn_cancel' gid='start'}</a>
</form>
<div class="clr"></div>

<script>{literal}
$(function(){
	$("div.row:odd").addClass("zebra");
});
{/literal}</script>

{include file="footer.tpl"}
