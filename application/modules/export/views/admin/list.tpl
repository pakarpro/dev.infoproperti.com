{include file="header.tpl"}
{helper func_name=get_admin_level1_menu helper_name=menu func_param='admin_export_menu'}
<div class="actions">
	<ul>
		<li id="add"><div class="l"><a href="{$site_url}admin/export/edit/">{l i='btn_add' gid='start'}</a></div></li>
	</ul>
	&nbsp;
</div>

<form id="export_form" action="" method="post">
<table cellspacing="0" cellpadding="0" class="data" width="100%">
	<tr>
		<th class="w100">{l i='field_selection_name' gid='export'}</th>
		<th class="w100">{l i='field_selection_driver' gid='export'}</th>
		<th class="w100">{l i='field_selection_module' gid='export'}</th>
		<th class="w50">{l i='field_selection_status' gid='export'}</th>
		{*<th class="w50"><a href="{$sort_links.date_created}"{if $order eq 'date_created'} class="{$order_direction|lower}"{/if}>{l i='field_selection_date_created' gid='export'}</a></th>*}
		<th class="w70">&nbsp;</th>
	</tr>
	{foreach item=item from=$selections}
		{counter print=false assign=counter}
		<tr{if $counter is div by 2} class="zebra"{/if}>				
			<td>{$item.name|truncate:100}</td>
			<td>{$item.driver.output_name|truncate:50}</td>
			<td>{$item.module.output_name|truncate:50}</td>
			<td class="center">
				{if $item.status}
				<a href="{$site_url}admin/export/activate/{$item.id}/0"><img src="{$site_root}{$img_folder}icon-full.png" width="16" height="16" border="0" alt="{l i='link_deactivate_selection' gid='export' type='button'}" title="{l i='link_deactivate_selection' gid='export' type='button'}">
				{else}
				<a href="{$site_url}admin/export/activate/{$item.id}/1"><img src="{$site_root}{$img_folder}icon-empty.png" width="16" height="16" border="0" alt="{l i='link_activate_selection' gid='export' type='button'}" title="{l i='link_activate_selection' gid='export' type='button'}"></a>
				{/if}
			</td>
			{*<td class="center">{$item.date_created|date_format:$page_data.date_format}</td>*}
			<td class="icons">
				<a href="{$site_url}admin/export/edit/{$item.id}"><img src="{$site_root}{$img_folder}icon-edit.png" width="16" height="16" border="0" alt="{l i='link_edit_selection' gid='export' type='button'}" title="{l i='link_edit_selection' gid='export' type='button'}"></a>
				<a href="{$site_url}admin/export/delete/{$item.id}" onclick="javascript: if(!confirm('{l i='note_delete_selection' gid='export' type='js'}')) return false;"><img src="{$site_root}{$img_folder}icon-delete.png" width="16" height="16" border="0" alt="{l i='link_delete_selection' gid='export' type='button'}" title="{l i='link_delete_selection' gid='export' type='button'}"></a>
				{if $item.status}
				<a href="{$site_url}admin/export/generate/{$item.id}"><img src="{$site_root}{$img_folder}icon-play.png" width="16" height="16" border="0" alt="{l i='link_generate_selection' gid='export' type='button'}" title="{l i='link_generate_selection' gid='export' type='button'}"></a>
				{/if}
			</td>
		</tr>
	{foreachelse}
		<tr><td colspan="6" class="center">{l i='no_selections' gid='export'}</td></tr>
	{/foreach}
</table>
</form>
{include file="pagination.tpl"}

<script>{literal}
	var reload_link = "{/literal}{$site_url}admin/export/index{literal}";
	var filter = '{/literal}{$filter}{literal}';
	var order = '{/literal}{$order}{literal}';
	var loading_content;
	var order_direction = '{/literal}{$order_direction}{literal}';
	$(function(){
		$('#grouping_all').bind('click', function(){
			var checked = $(this).is(':checked');
			if(checked){
				$('input.grouping').attr('checked', 'checked');
			}else{
				$('input.grouping').removeAttr('checked');
			}
		});
		
		$('#grouping_all').bind('click', function(){
			var checked = $(this).is(':checked');
			if(checked){
				$('input[type=checkbox].grouping').attr('checked', 'checked');
			}else{
				$('input[type=checkbox].grouping').removeAttr('checked');
			}
		});
	
		$('#delete_all').bind('click', function(){
			if(!$('input[type=checkbox].grouping').is(':checked')) return false; 
			if(this.id == 'delete_all' && !confirm('{/literal}{l i='note_selections_delete_all' gid='export' type='js'}{literal}')) return false;
			$('#export_form').attr('action', $(this).find('a').attr('href')).submit();		
			return false;
		});
	});
	function reload_this_page(value){
		var link = reload_link + filter + '/' + value + '/' + order + '/' + order_direction;
		location.href=link;
	}
{/literal}</script>

{include file="footer.tpl"}
