{include file="header.tpl" load_type='editable|ui'}
{js file='admin-multilevel-sorter.js'}
<div class="actions">
	<ul>
		<li><div class="l"><a href="{$site_url}admin/export/edit_driver_field/{$driver_data.gid}">{l i='btn_driver_field_create' gid='export'}</a></div></li>
	</ul>
	&nbsp;
</div>
<table cellspacing="0" cellpadding="0" class="data" width="100%">
<tr>
	<th class="first v150">{l i='field_custom_name' gid='export'}</th>
	<th class="w150">{l i='field_custom_type' gid='export'}</th>
	<th class="w70">&nbsp;</th>
</tr>
{foreach item=item key=key from=$driver_data.elements}
{counter print=false assign=counter}
<tr{if $counter is div by 2} class="zebra"{/if}>
	<td>{$item.name|truncate:100}</td>
	<td>{$item.type|truncate:30}</td>
	<td class="icons">
		<a href="{$site_url}admin/export/edit_driver_field/{$driver_data.gid}/{$key}"><img src="{$site_root}{$img_folder}icon-edit.png" width="16" height="16" border="0" alt="{l i='link_custom_field_edit' gid='export' type='button'}" title="{l i='link_custom_field_edit' gid='export' type='button'}"></a>
		<a href="{$site_url}admin/export/delete_driver_field/{$driver_data.gid}/{$key}" onclick="javascript: if(!confirm('{l i='note_custom_field_delete' gid='export' type='js'}')) return false;"><img src="{$site_root}{$img_folder}icon-delete.png" width="16" height="16" border="0" alt="{l i='link_custom_field_delete' gid='export' type='button'}" title="{l i='link_custom_field_delete' gid='export' type='button'}"></a>
	</td>
</tr>
{foreachelse}
<tr><td colspan="3" class="center">{l i='no_driver_fields' gid='export'}</td></tr>
{/foreach}
</table>
{include file="pagination.tpl"}

<script>{literal}
	var reload_link = "{/literal}{$site_url}admin/export/edit_driver/{$driver_data.gid}/{literal}";
	var filter = '{/literal}{$filter}{literal}';
	var order = '{/literal}{$order}{literal}';
	var loading_content;
	var order_direction = '{/literal}{$order_direction}{literal}';
	$(function(){
		$('#grouping_all').bind('click', function(){
			var checked = $(this).is(':checked');
			if(checked){
				$('input.grouping').attr('checked', 'checked');
			}else{
				$('input.grouping').removeAttr('checked');
			}
		});
		
		$('#grouping_all').bind('click', function(){
			var checked = $(this).is(':checked');
			if(checked){
				$('input[type=checkbox].grouping').attr('checked', 'checked');
			}else{
				$('input[type=checkbox].grouping').removeAttr('checked');
			}
		});
	
		$('#all').bind('click', function(){
			if(!$('input[type=checkbox].grouping').is(':checked')) return false; 
			if(this.id == 'delete_all' && !confirm('{/literal}{l i='note_custom_field_delete_all' gid='export' type='js'}{literal}')) return false;
			$('#alerts_form').attr('action', $(this).find('a').attr('href')).submit();		
			return false;
		});
	});
	function reload_this_page(value){
		var link = reload_link + filter + '/' + value + '/' + order + '/' + order_direction;
		location.href=link;
	}
{/literal}</script>
{include file="footer.tpl"}
