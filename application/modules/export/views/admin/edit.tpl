{include file="header.tpl" load_type='ui'}

<div class="menu-level3">
	<ul>
		{if $data.id}
		<li class="{if $section_gid eq 'general'}active{/if}"><a href="{$site_url}admin/export/edit/{$data.id}/general">{l i='filter_section_general' gid='export'}</a></li>
		<li class="{if $section_gid eq 'custom_fields'}active{/if}"><a href="{$site_url}admin/export/edit/{$data.id}/custom_fields">{l i='filter_section_custom_fields' gid='export'}</a></li>
		<li class="{if $section_gid eq 'search_form'}active{/if}"><a href="{$site_url}admin/export/edit/{$data.id}/search_form">{l i='filter_section_search_form' gid='export'}</a></li>
		{else}
		<li class="{if $section_gid eq 'general'}active{/if}">{l i='filter_section_general' gid='export'}</li>
		{/if}
	</ul>
	&nbsp;
</div>

{if $section_gid eq 'general'}
{js file='jquery-ui.custom.min.js'}
<LINK href='{$site_root}{$js_folder}jquery-ui/jquery-ui.custom.css' rel='stylesheet' type='text/css' media='screen'>
<form method="post" action="{$data.action}" name="save_form" enctype="multipart/form-data">
	<div class="edit-form n150">
		<div class="row header">{l i='admin_header_selections_general_edit' gid='export'}</div>
		<div class="row">
			<div class="h">{l i="field_selection_name" gid='export'}:&nbsp;* </div>
			<div class="v"><input type="text" name="data[name]" value="{$data.name|escape}" /></div>
		</div>
		<div class="row zebra">
			<div class="h">{l i="field_selection_driver" gid='export'}:&nbsp;* </div>
			<div class="v">
				{if $data.id}
					{$data.driver.output_name|truncate:100}
				{else}
				<select name="data[gid_driver]">
					{foreach item=item from=$drivers}
					<option value="{$item.gid}">{$item.output_name|truncate:100}</option>
					{foreachelse}
					<option value="">...</option>
					{/foreach}
				</select>
				{/if}
			</div>
		</div>
		<div class="row">
			<div class="h">{l i="field_selection_module" gid='export'}:&nbsp;* </div>
			<div class="v">
				{if $data.id}
					{$data.module.output_name|truncate:100}
				{else}
				<select name="data[id_object]">
					{foreach item=item from=$modules}
					<option value="{$item.id}">{$item.output_name|truncate:100}</option>
					{foreachelse}
					<option value="">-</option>
					{/foreach}
				</select>
				{/if}
			</div>
		</div>
		{if $data.id}
		<div class="row zebra">
			<div class="h">{l i="field_selection_output_type" gid='export'}:&nbsp;* </div>
			<div class="v">
				<select name="data[output_type]" id="output_type">
					{foreach item=item from=$output_types}
					<option value="{$item|escape}" {if $data.output_type eq $item}selected{/if}>{l i='output_'+$item gid='export'}</option>
					{/foreach}
				</select>
			</div>
		</div>
	
		<div id="settings_file" class="settings {if $data.output_type ne 'file'}hide{/if}">
		
		<div class="row">
			<div class="h">{l i="field_selection_file_format" gid='export'}: </div>
			<div class="v">
				<input type="text" name="data[file_format]" value="{$data.file_format|escape}">
			</div>
		</div>
		<div class="row zebra">
			<div class="h">{l i="field_selection_append_date" gid='export'}: </div>
			<div class="v">
				<input type="hidden" name="data[append_date]" value="0">
				<input type="checkbox" name="data[append_date]" value="1" {if $data.append_date}checked{/if}>
			</div>
		</div>
		<div class="row">
			<div class="h">{l i="field_selection_scheduler" gid='export'}:&nbsp;* </div>
			<div class="v">
				<p><input type="radio" name="data[scheduler][type]" value="manual" {if !$data.scheduler.type || $data.scheduler.type eq 'manual'}checked{/if}>{l i='start_manual' gid='export'}</p>
				{depends module=cronjob}
				<p>
					<input type="radio" name="data[scheduler][type]" value="intime" {if $data.scheduler.type eq 'intime'}checked{/if}>{l i='start_in_time' gid='export'}
					<input type='text' value='{if $data.scheduler.type eq 'intime'}{$data.scheduler.date}{/if}' name="scheduler[intime][date]" id="datepicker1" maxlength="10" class="short">
					<select name="scheduler[intime][hours]" class="short">
						{foreach item=item from=$hours}
						<option value="{$item|escape}" {if $data.scheduler.hours eq $item AND $data.scheduler.type eq 'intime'}selected{/if}>{$item}</option>
						{/foreach}
					</select>
					<select name="scheduler[intime][minutes]" class="short">
						{foreach item=item from=$minutes}
						<option value="{$item|escape}" {if $data.scheduler.minutes eq $item AND $data.scheduler.type eq 'intime'}selected{/if}>{$item}</option>
						{/foreach}
					</select>
				</p>
				<p>
					<input type="radio" name="data[scheduler][type]" value="cron" {if $data.scheduler.type eq 'cron'}checked{/if}>{l i='start_cron' gid='export'}
					
					<select name="scheduler[cron][period]" class="middle">
						<option value="day" {if $data.scheduler.period eq 'day'}selected{/if}>{l i='start_type_day' gid='export'}</option>
						<option value="week" {if $data.scheduler.period eq 'week'}selected{/if}>{l i='start_type_week' gid='export'}</option>
						<option value="month" {if $data.scheduler.period eq 'month'}selected{/if}>{l i='start_type_month' gid='export'}</option>
					</select>
					{l i='start_type_since' gid='export'}
					<input type='text' value='{if $data.scheduler.type eq 'cron'}{$data.scheduler.date}{/if}' name="scheduler[cron][date]" id="datepicker2" maxlength="10" class="short">
					<select name="scheduler[cron][hours]" class="short">
						{foreach item=item from=$hours}
						<option value="{$item|escape}" {if $data.scheduler.hours eq $item && $data.scheduler.type eq 'cron'}selected{/if}>{$item}</option>
						{/foreach}
					</select>
					<select name="scheduler[cron][minutes]" class="short">
						{foreach item=item from=$minutes}
						<option value="{$item|escape}" {if $data.scheduler.minutes eq $item AND $data.scheduler.type eq 'cron'}selected{/if}>{$item}</option>
						{/foreach}
					</select>
				</p>
				{/depends}
			</div>
		</div>
		
		{if !$data.append_date && $data.file_format}
		<div class="row zebra">
			<div class="h">{l i='link_export_data' gid='export'}</div>
			<div class="v">{$site_url}uploads/export/{$data.id}/{$data.file_format|strftime}</div>
		</div>
		{/if}
		
		</div>
		
		<div id="settings_browser" class="settings {if $data.output_type ne 'browser'}hide{/if}">
		
		<div class="row">
			<div class="h">{l i="field_selection_published" gid='export'}: </div>
			<div class="v">
				<input type="hidden" name="data[published]" value="0" />
				<input type="checkbox" name="data[published]" value="1" {if $data.published}checked="checked"{/if} id="publish_selection" />
				<span id="publish_link" {if !$data.published}class="hide"{/if}>{seolink module='export' method='index' data=$data}{*l i='link_export_data' gid='export'*}</span>
			</div>
		</div>
		<div class="row zebra">
			<div class="h">{l i="field_selection_editable" gid='export'}: </div>
			<div class="v">
				<input type="hidden" name="data[editable]" value="0" />
				<input type="checkbox" name="data[editable]" value="1" {if $data.editable}checked="checked"{/if} {if !$data.published}disabled{/if} id="advanced_selection" />
				<span id="advanced_link" {if !$data.editable}class="hide"{/if}>{seolink module='export' method='advanced' data=$data}</span>
				{*l i='link_export_form' gid='export'*}
			</div>
		</div>
		
		</div>
		{/if}
	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="{l i='btn_save' gid='start' type='button'}"></div></div>
	<a class="cancel" href="{$site_url}admin/export">{l i='btn_cancel' gid='start'}</a>
</form>
<script>{literal}
	$(function(){
		$('#datepicker1, #datepicker2').datepicker({dateFormat :'yy-mm-dd'});
		$('#output_type').bind('change', function(){
			$('.settings').hide();
			$('#settings_'+$(this).val()).show();
		});
		$('#publish_selection').bind('change', function(){
			if(this.checked){
				$('#advanced_selection').removeAttr('disabled');
				$('#publish_link').show();
			}else{
				$('#advanced_selection').attr('disabled', 'disabled').removeAttr('checked');
				$('#advanced_link').hide();
				$('#publish_link').hide();
			}
		});
		$('#advanced_selection').bind('change', function(){
			if(this.checked){
				$('#advanced_link').show();
			}else{
				$('#advanced_link').hide();
			}
		});
	});
{/literal}</script>
{elseif $section_gid eq 'custom_fields' && $data.id}

{if $data.driver.editable}
	{if $driver_fields|count}
	<form method="post" action="{$site_root}admin/export/create_relation/{$data.id}" name="save_form" enctype="multipart/form-data">
	<div class="filter-form">
		<div class="block-l">
			{l i='form_driver_fields_header' gid='export'}:<br>
			<select name="name" id="name_select" class="wide">{foreach item=item key=key from=$driver_fields}<option value="{$key|escape}">{$item.name}</option>{/foreach}</select>
		</div>
		<div class="block-r">
			{l i='form_module_fields_header' gid='export'}:<br>
			<select name="link" id="link_select" class="wide">{foreach item=item key=key from=$custom_fields}<option value="{$key|escape}">{$item.label}</option>{foreachelse}<option value="-1">-</option>{/foreach}</select>
		</div>
	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="{l i='btn_add' gid='start' type='button'}"></div></div>
	</form>

	<script>{literal}
		$(function(){
			$('#name_select').bind('change', function(){
				var linkSelect = $('#link_select');
				$.post('{/literal}{$site_url}{literal}/admin/export/ajax_get_module_fields/{/literal}{$data.id}{literal}', {index: $(this).val()}, function(data){
					linkSelect.empty();
					if($.makeArray(data).length){
						for(i in data){
							linkSelect.append('<option value="'+i+'">'+data[i].label+'</option>');
						}
					}else{
						linkSelect.append('<option value="">-</option>');
					}
				}, 'json');			
			});
		});
	{/literal}</script>		
	{/if}
	
			
	<table cellspacing="0" cellpadding="0" class="data" width="100%">
	<tr>
		<th class="first w150">{l i='field_custom_name' gid='export'}</th>
		<th class="w150">{l i='field_custom_type' gid='export'}</th>
		<th class="w300">{l i='field_custom_link' gid='export'}</th>
		<th class="w70">&nbsp;</th>
	</tr>
	{foreach item=item key=key from=$data.relations}
	{counter print=false assign=counter}
	<tr{if $counter is div by 2} class="zebra"{/if}>
		<td>{$item.name|truncate:100}</td>
		<td>{$item.type|truncate:30}</td>
		<td>{$item.label|truncate:150}</td>
		<td class="icons">
			{if $data.driver.editable}
			<a href="{$site_url}admin/export/delete_relation/{$data.id}/{$key}"><img src="{$site_root}{$img_folder}icon-delete.png" width="16" height="16" border="0" alt="{l i='link_delete_relation' gid='export' type='button'}" title="{l i='link_delete_relation' gid='export' type='button'}"></a>
			{/if}
		</td>
	</tr>
	{foreachelse}
	<tr><td colspan="4" class="center">{l i='no_custom_fields' gid='export'}</td></tr>
	{/foreach}
	</table>
	<br><a class="cancel" href="{$site_url}admin/export">{l i='btn_cancel' gid='start'}</a>
			
{else}
<form method="post" action="{$site_root}admin/export/save_relations/{$data.id}" name="save_form" enctype="multipart/form-data">
	<table cellspacing="0" cellpadding="0" class="data" width="100%">
	<tr>
		<th class="first w150">{l i='field_custom_name' gid='export'}</th>
		<th class="w150">{l i='field_custom_type' gid='export'}</th>
		<th class="w300">{l i='field_custom_link' gid='export'}</th>
	</tr>
	{foreach item=item key=key from=$driver_fields}
	{counter print=false assign=counter}
	<tr{if $counter is div by 2} class="zebra"{/if}>
		<td>{$item.name|truncate:100}{if $item.required}&nbsp;*{/if}</td>
		<td>{$item.type|truncate:30}</td>
		<td>
			<select name="link[{$key|escape}]">
			<option value="-1">...</option>
			{foreach item=item2 key=key2 from=$item.custom_fields}
			<option value="{$key2|escape}" {if $data.relations[$key] && $data.relations[$key].link eq $item2.name}selected{/if}>{$item2.label}</option>
			{/foreach}
			</select>
		</td>				
	</tr>
	{foreachelse}
	<tr><td colspan="3" class="center">{l i='no_custom_fields' gid='export'}</td></tr>
	{/foreach}
	</table>
				
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="{l i='btn_save' gid='start' type='button'}"></div></div>
	<a class="cancel" href="{$site_url}admin/export">{l i='btn_cancel' gid='start'}</a>
</form>
{/if}
			
{elseif $section_gid eq 'search_form' && $data.id}
<form method="post" action="{$site_root}admin/export/save_form_data/{$data.id}" name="save_form" enctype="multipart/form-data">
	<div class="edit-form n150" id="export_form_box">
		<div class="row header">{l i='admin_header_selections_form_edit' gid='export'}</div>
		{assign var='data' value=$data.admin_form_data}
		{$search_form}
	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="{l i='btn_save' gid='start' type='button'}"></div></div>
	<a class="cancel" href="{$site_url}admin/export">{l i='btn_cancel' gid='start'}</a>
</form>	
{/if}

<div class="clr"></div>
{include file="footer.tpl"}
