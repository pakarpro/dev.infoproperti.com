<?php
$module['module'] = 'themes';
$module['install_name'] = 'Color themes';
$module['install_descr'] = 'Color themes for user & admin modes';
$module['version'] = '2.02';
$module['files'] = array(
	array('file', 'read', "application/modules/themes/controllers/admin_themes.php"),
	array('file', 'read', "application/modules/themes/install/module.php"),
	array('file', 'read', "application/modules/themes/install/permissions.php"),
	array('file', 'read', "application/modules/themes/models/themes_install_model.php"),
	array('file', 'read', "application/modules/themes/models/themes_model.php"),
	array('file', 'read', "application/modules/themes/views/admin/css/style-ltr.css"),
	array('file', 'read', "application/modules/themes/views/admin/css/style-rtl.css"),
	array('file', 'read', "application/modules/themes/views/admin/edit_set.tpl"),
	array('file', 'read', "application/modules/themes/views/admin/form_rtl.tpl"),
	array('file', 'read', "application/modules/themes/views/admin/list_enable.tpl"),
	array('file', 'read', "application/modules/themes/views/admin/list_installed.tpl"),
	array('file', 'read', "application/modules/themes/views/admin/list_sets.tpl"),
	array('file', 'read', "application/modules/themes/views/admin/view.tpl"),
	array('dir', 'read', 'application/modules/themes/langs'),
);
$module['dependencies'] = array(
	'start' => array('version'=>'1.01'),
	'menu' => array('version'=>'1.01')
);
$module['linked_modules'] = array(
	'install' => array(
		'menu'		=> 'install_menu',
		'ausers'	=> 'install_ausers'
	),
	'deinstall' => array(
		'menu'		=> 'deinstall_menu',
		'ausers'	=> 'deinstall_ausers'
	)
);
