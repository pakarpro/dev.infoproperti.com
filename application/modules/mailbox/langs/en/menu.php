<?php

$install_lang["agent_account_menu_agent_mailbox_item"] = "My messages";
$install_lang["agent_account_menu_agent_mailbox_item_tooltip"] = "";
$install_lang["agent_top_menu_agent-main-my-messages-item"] = "My messages";
$install_lang["agent_top_menu_agent-main-my-messages-item_tooltip"] = "";
$install_lang["company_account_menu_company_mailbox_item"] = "My messages";
$install_lang["company_account_menu_company_mailbox_item_tooltip"] = "";
$install_lang["company_top_menu_company-main-my-messages-item"] = "My messages";
$install_lang["company_top_menu_company-main-my-messages-item_tooltip"] = "";
$install_lang["private_account_menu_private_mailbox_item"] = "My messages";
$install_lang["private_account_menu_private_mailbox_item_tooltip"] = "";
$install_lang["private_top_menu_private-main-my-messages-item"] = "My messages";
$install_lang["private_top_menu_private-main-my-messages-item_tooltip"] = "";

