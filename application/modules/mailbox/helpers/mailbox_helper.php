<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if(!function_exists('new_messages')) {

	function new_messages($attrs) {
		$CI = & get_instance();
		if ('user' != $CI->session->userdata("auth_type")) {
			return false;
		}
		$user_id = $CI->session->userdata("user_id");
		if(!$user_id) {
			log_message('Empty user id');
			return false;
		}
		if(empty($attrs['template'])) {
			$attrs['template'] = 'header';
		}
		$CI->load->model('Mailbox_model');
		$count = $CI->Mailbox_model->get_new_messages_count($user_id);

		$CI->template_lite->assign('messages_count', $count);
		return $CI->template_lite->fetch('helper_new_messages_' . $attrs['template'], 'user', 'mailbox');
	}

}

if ( ! function_exists('button_contact')){

	function button_contact($params) {
		$CI = & get_instance();
		
		if(!isset($params['user_id'])) return '';
		
		if($CI->session->userdata("auth_type") == "user"){
			$user_id = $CI->session->userdata('user_id');
			if($user_id == $params['user_id']) return '';
		}else{
			$CI->load->helpers('users');		
			if(!is_login_enabled()) return '';
			$CI->template_lite->assign("is_guest", 1);
		}

		if(!isset($params['user_type'])){
			$CI->load->model('Users_model');
			$CI->Users_model->set_format_settings('use_format', false);
			$user = $CI->Users_model->get_user_type_by_id($params['user_id']);
			$CI->Users_model->set_format_settings('use_format', true);
			if(!$user) return '';
			$params['user_type'] = $user_type;
		}
		
		if(!isset($params['template'])) $params['template'] = 'icon';
		$CI->template_lite->assign('template', $params['template']);
		
		$CI->template_lite->assign('user_type', $params['user_type']);
		$CI->template_lite->assign('user_id', $params['user_id']);
		return $CI->template_lite->fetch('helper_contact', 'user', 'mailbox');
	}

}

if(!function_exists('contacts_count')) {

	function contacts_count($attrs = array()) {
		$CI = & get_instance();
		if(empty($attrs['user_id']['user_id'])) {
			$attrs['user_id'] = $CI->session->userdata("user_id");
		}
		if(!$attrs['user_id']) {
			return false;
		}
		$CI->load->model('Mailbox_model');
		$where = array();
		$where['where']['id_com_user'] = $attrs['user_id'];
		$count = $CI->Mailbox_model->get_chats_count($where);
		$CI->template_lite->assign('contacts_count', $count);
		return $CI->template_lite->fetch('helper_contacts_count', 'user', 'mailbox');
	}

}
