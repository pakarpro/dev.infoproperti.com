<div class="fleft actions hide" id="grouping-btn">
		<a href="#" onclick="javascript: if(confirm('{l i='note_delete_chats' gid='mailbox' type='js'}')) {literal}{{/literal} mb.delete_chats(0); {literal}}{/literal} return false;" class="btn-link"><ins class="with-icon i-delete"></ins></a>
		<a href="#" onclick="javascript: mb.open_move_form(0); return false;" class="btn-link"><ins class="with-icon i-folder"></ins></a>
</div>
<div class="clr"></div>
<div>	
		<table class="list">
		<tr id="sorter_block">
			<th class="w30">{if $chats}<input type="checkbox" class="grouping_all">{else}&nbsp;{/if}</th>		
			<th>{l i='field_nickname' gid='mailbox'}</th>		
			<th class="w150"><a href="#" orderkey="new" class="link-sorter">{l i='field_new_messages' gid='mailbox'}{if $page_data.order eq 'new'}<ins class="fright i-sorter with-icon-small {$page_data.order_direction|lower}"></ins>{/if}</a></th>		
			<th class="w125"><a href="#" orderkey="total" class="link-sorter">{l i='field_all_messages' gid='mailbox'}{if $page_data.order eq 'total'}<ins class="fright i-sorter with-icon-small {$page_data.order_direction|lower}"></ins>{/if}</a></th>		
			<th class="w100">{l i='actions' gid='mailbox'}</th>	
		</tr>
		{foreach item=item from=$chats}
		<tr>
			<td><input type="checkbox" class="grouping chat_item" value="{$item.id}"></td>
			<td>{$item.user_data.output_name}</td>
			<td>{$item.new}</td>
			<td>{$item.total}</td>
			<td>
				<a href="#" onclick="javascript: if(confirm('{l i='note_delete_chat' gid='mailbox' type='js'}')) {literal}{{/literal} mb.delete_chats({$item.id}); {literal}}{/literal} return false;" class="btn-link fright"><ins class="with-icon i-delete"></ins></a>
				<a href="{$site_url}mailbox/chat/{$item.user_data.id}" class="btn-link fright"><ins class="with-icon i-eye"></ins></a>
			</td>
		</tr>
		{foreachelse}
			<tr>
				<td colspan="5" class="empty">{l i='no_chats' gid='mailbox'}</td>
			</tr>
		{/foreach}
		</table>	

</div>
<div id="pages_block_2">{pagination data=$page_data type='full'}</div>
