<div class="load_content_controller">

	<h1>{if $folder.id}{l i='header_edit_folder' gid='mailbox'}{else}{l i='header_add_folder' gid='mailbox'}{/if}</h1>

	<div class="inside edit_block">
	
		<div class="r">
			<div class="f">{l i='header_folder' gid='mailbox'}: </div>
			<div class="v">
				<input type="text" name="folder_name" id="save_folder_name" class="middle" value="{$folder.name}" />
			</div>
		</div>
		<div class="r">
			<div class="v"><input id="save_folder_button" type="button" name="move_btn" value="{l i='btn_apply' gid='start' type='button'}" class='btn' /></div>
		</div>

	</div>
	
</div>
