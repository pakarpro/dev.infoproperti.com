{switch from=$template}
	{case value='button'}
		<a id="contact_link" href="javascript:void(0);" class="btn-link" title="{l i='link_contact_'+$user_type gid='mailbox' type='button'}"><ins class="with-icon i-mail"></ins></a><span class="btn-text link-r-margin">{l i='btn_contact' gid='mailbox'}</span>
	{case value='icon'}
		<a id="contact_link" href="javascript:void(0);" class="btn-link link-r-margin" title="{l i='link_contact_'+$user_type gid='mailbox' type='button'}"><ins class="with-icon i-mail"></ins></a>
{/switch}
{js module=mailbox file='message_link.js'}
{js module=users_services file='available_view.js'}
<script type='text/javascript'>
	{literal}
	$(function(){
		message_link({
			siteUrl: '{/literal}{$site_url}{literal}',
			idContact: '{/literal}{$user_id}{literal}',
			{/literal}{if $is_guest}display_login: true,{/if}{literal}
			{/literal}{depends module=users_services}available_view: new available_view(),{/depends}{literal}
		});
	});
	{/literal}
</script>
