<div class="load_content_controller">
	<h1>{l i='link_move_selected' gid='mailbox'}</h1>
	<div class="inside edit_block">
	
		<div class="r">
			<div class="f">{l i='header_chat_folders' gid='mailbox'}: </div>
			<div class="v">
				<select name="move_folder" id="move_folder_select" class="middle">
				{foreach item=item key=key from=$folders}
				<option gid="{$item.folder_type}" value="{$item.id}">{$item.name} ({$item.chats})</option>
				{/foreach}
				</select>
			</div>
		</div>
		
		<div class="r">
			<div class="v"><input id="move_folder_button" type="button" name="move_btn" value="{l i='btn_apply' gid='start' type='button'}" class='btn' /></div>
		</div>

	</div>
</div>