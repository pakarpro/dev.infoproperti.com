function message_link(optionArr){
	this.properties = {
		siteUrl: '',
		errorObj: new Errors(),
		available_view: null,
		display_login: false,
		idContact: 0
	}

	var _self = this;

	this.Init = function(options){
		_self.properties = $.extend(_self.properties, options);
		_self.init_links();
		
		if(_self.properties.available_view){
			_self.properties.available_view.set_properties({
				siteUrl: _self.properties.siteUrl,
				success_request: function(message) {
					document.location.href = _self.properties.siteUrl + 'mailbox/chat/' + _self.properties.idContact;
				},
				fail_request: function(message) {
					_self.properties.errorObj.show_error_block(message, 'error');
				}
			});
		}
	}

	this.init_links = function(){
		$('#contact_link').bind('click', function(){
			if(_self.properties.available_view){
				_self.properties.available_view.check_available(_self.properties.idContact);
			}else if(_self.properties.display_login == 1){
				$('html, body').animate({
					scrollTop: $("#ajax_login_link").offset().top
				}, 2000);
				$("#ajax_login_link").click();
			}else{
				document.location.href = _self.properties.siteUrl + 'mailbox/chat/' + _self.properties.idContact;
			}
			return false;
		});
	}

	_self.Init(optionArr);
}
