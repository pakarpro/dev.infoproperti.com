function Mailbox(optionArr){
	this.properties = {
		siteUrl: '',
		
		viewType: 'chats', /// chats, chat, folders enabled
		
		folderId: 0,
		chatId: 0,
		
		messagesShown: 0,

		pageData: {
			orderDirection: 'ASC',
			orderKey: 'new'
		},
		
		folderBlockId: 'folders',
		windowBlockId: 'chat_window',
		mListId: 'message_list',
		mBoxId: 'message_area',
		mSubmitId: 'msg_submit',

		urlOpenFolder: 'mailbox/ajax_view_chats/',
		urlFolderBlock: 'mailbox/ajax_view_folders/',
		urlListFolderBlock: 'mailbox/ajax_view_list_folders/',
		
		urlFormFolder: 'mailbox/ajax_form_folder/',
		urlAddFolder: 'mailbox/ajax_save_folder/',
		urlDeleteFolder: 'mailbox/ajax_delete_folder/',

		urlMoveChats: 'mailbox/ajax_move_chats',
		urlDeleteChats: 'mailbox/ajax_delete_chats',

		urlSendMessage: 'mailbox/ajax_send_message',
		urlGetMessages: 'mailbox/ajax_get_messages/',
		urlDeleteMessage: 'mailbox/ajax_delete_message/',

		urlOpenEditFolder: 'mailbox/ajax_open_edit_folder/',
		urlOpenMoveFolder: 'mailbox/ajax_open_move_folder/',

		keyCache1: 0,
		keyCache2: 0,

		reloadTimeout: 10000,
		timeoutId: false,
		errorObj: new Errors,
		
		initialNextBtn: false,

		contentObj: new loadingContent({
			loadBlockWidth: '400px',
			closeBtnClass: 'load_content_controller_close',
			closeBtnPadding: 15
		})
	}

	var _self = this;

	this.Init = function(options){
		_self.properties = $.extend(_self.properties, options);
		
		if(_self.properties.viewType == 'chats'){
			_self.init_chats();
		}else if(_self.properties.viewType == 'chat'){
			_self.init_chat();
		}else if(_self.properties.viewType == 'folders'){
			_self.init_folders();
		}

	}

	this.init_chats = function(){
		$('#'+_self.properties.windowBlockId+' .link-sorter' ).live('click', function(){
			var orderKey = $(this).attr('orderkey');
			if(( _self.properties.pageData.orderKey != orderKey ) || ( _self.properties.pageData.orderKey == orderKey && _self.properties.pageData.orderDirection == 'DESC' )){
				var orderDirection = 'ASC';
			}else{
				var orderDirection = 'DESC';
			}
			_self.properties.pageData.orderKey == orderKey;
			_self.properties.pageData.orderDirection == orderDirection;
			_self.reload_chats(_self.properties.folderId, orderKey, orderDirection);
			return false;	
		});
		
		$('#pages_block_2 a').live('click', function(){
			_self.reload_chats_by_url($(this).attr('href'));
			return false;	
		});
		
		$('#'+_self.properties.folderBlockId+' > select').live('change', function(){
			_self.reload_chats($(this).val(), _self.properties.pageData.orderKey, _self.properties.pageData.orderDirection);
		});

		$('#'+_self.properties.windowBlockId).find('.grouping_all').live('change', function(){
			var checked = $(this).is(':checked');
			if(checked){
				$('#'+_self.properties.windowBlockId).find('.grouping').attr('checked', 'checked').trigger('change');
			}else{
				$('#'+_self.properties.windowBlockId).find('.grouping').removeAttr('checked').trigger('change');
			}
		});
		
		$('#'+_self.properties.windowBlockId).find('.grouping').live('change', function(){
			var elsCntChecked = $('#'+_self.properties.windowBlockId).find('.grouping:checked').length;
			var elsCnt = $('#'+_self.properties.windowBlockId).find('.grouping').length;
			if(elsCntChecked > 0){
				$('#grouping-btn').show();
				if(elsCntChecked == elsCnt) $('#'+_self.properties.windowBlockId).find('.grouping_all').attr('checked', 'checked');
			}else{
				$('#grouping-btn').hide();
				$('#'+_self.properties.windowBlockId).find('.grouping_all').removeAttr('checked');
			}
		});
		
	}
	
	
	this.init_chat = function(){
		$('#'+_self.properties.mSubmitId).unbind().bind('click', function(){
			_self.send_message();
		});
		$('#'+_self.properties.mBoxId).unbind().bind('keydown', function(e){
			_self.properties.keyCache1 = _self.properties.keyCache2;
			_self.properties.keyCache2 = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
			if (_self.properties.keyCache1 == 17 && _self.properties.keyCache2 == 13) _self.send_message();
		});

		_self.update_messages_shown();

		clearTimeout(_self.properties.timeoutId);
		_self.properties.timeoutId = setTimeout(_self.get_new_messages, _self.properties.reloadTimeout);
		
		if(_self.properties.initialNextBtn){
			$('#next-page').bind('click', function(){
				_self.get_messages(0, _self.properties.messagesShown);
				return false;
			}).show();
		}
	}
	
	this.init_folders = function(){
		
	}

	this.reload_chats = function(idFolder, order, dir){
		_self.properties.folderId = idFolder;
		var url = _self.properties.siteUrl + _self.properties.urlOpenFolder + idFolder;
		if(order){
			url = url + '/' + order;
		}
		if(dir){
			url = url + '/' + dir;
		}
		_self.reload_chats_by_url(url);
	}
	
	this.reload_chats_by_url = function(url){
		$('#'+_self.properties.windowBlockId).load(url);
	}

	this.delete_chats = function(chat_id){
		var chats = new Array();
		if(!chat_id){
			$('#'+_self.properties.windowBlockId+' input.grouping:checked').each(function(i){
				var val = $(this).val(); chats[val] = val;
			});
		}else{
			chats[chat_id] = chat_id;
		}
		$.ajax({
			url: _self.properties.siteUrl + _self.properties.urlDeleteChats,
			type: 'POST',
			data: ({ chats: chats }),
			dataType: 'json',
			cache: false,
			success: function(data){
				if(typeof(data.error) != 'undefined' && data.error != ''){
					_self.properties.errorObj.show_error_block(data.error, 'error');
				}else{
					_self.properties.errorObj.show_error_block(data.success, 'success');
					_self.reload_chats(_self.properties.folderId, _self.properties.pageData.orderKey, _self.properties.pageData.orderDirection);
					_self.reload_folders();
				}
			}
		});
	}

	this.move_chats = function(id_folder, chat_id){
		var chats = new Array();
		
		if(!chat_id){
			$('#'+_self.properties.windowBlockId+' input.grouping:checked').each(function(i){
				var val = $(this).val(); chats[val] = val;
			});
		}else{
			chats[chat_id] = chat_id;
		}

		$.ajax({
			url: _self.properties.siteUrl + _self.properties.urlMoveChats,
			type: 'POST',
			data: ({
				chats: chats,
				id_folder: id_folder
			}),
			dataType: 'json',
			cache: false,
			success: function(data){
				if(typeof(data.error) != 'undefined' && data.error != ''){
					_self.properties.errorObj.show_error_block(data.error, 'error');
				}else{
					_self.properties.errorObj.show_error_block(data.success, 'success');
					_self.reload_chats(_self.properties.folderId, _self.properties.pageData.orderKey, _self.properties.pageData.orderDirection);
					_self.reload_folders();
					_self.properties.contentObj.hide_load_block();
				}
			}
		});
	}

	this.open_move_form = function(id_chat){
		$.ajax({
			url: _self.properties.siteUrl+_self.properties.urlOpenMoveFolder,
			cache: false,
			success: function(data){
				_self.properties.contentObj.show_load_block(data);
				$('#move_folder_button').unbind().bind('click', function(){
					var idFolder = $('#move_folder_select > option:selected').val();
					_self.move_chats(idFolder, id_chat);
				});
			}
		});
	}
	
	this.reload_folders = function(){
		$('#'+_self.properties.folderBlockId).load(_self.properties.siteUrl + _self.properties.urlFolderBlock + _self.properties.folderId);
	}
	
	this.reload_list_folders = function(){
		$('#'+_self.properties.windowBlockId).load(_self.properties.siteUrl + _self.properties.urlListFolderBlock);
	}

	this.save_folder = function(id_folder, name){
		$.ajax({
			url: _self.properties.siteUrl + _self.properties.urlAddFolder,
			type: 'POST',
			data: ({
				name: name,
				id: id_folder
			}),
			dataType: 'json',
			cache: false,
			success: function(data){
				if(typeof(data.error) != 'undefined' && data.error != ''){
					_self.properties.errorObj.show_error_block(data.error, 'error');
				}else{
					_self.properties.errorObj.show_error_block(data.success, 'success');
					_self.reload_list_folders();
					_self.properties.contentObj.hide_load_block();
				}
			}
		});
	}

	this.edit_folder = function(idFolder){
		if(!idFolder) idFolder = 0;
		$.ajax({
			url: _self.properties.siteUrl+_self.properties.urlFormFolder + idFolder,
			cache: false,
			success: function(data){
				_self.properties.contentObj.show_load_block(data);
				$('#save_folder_button').unbind().bind('click', function(){
					_self.save_folder(idFolder, $('#save_folder_name').val());
				});
			}
		});
	}
	
	this.delete_folder = function(id_folder){
		$.ajax({
			url: _self.properties.siteUrl + _self.properties.urlDeleteFolder + id_folder,
			dataType: 'json',
			cache: false,
			success: function(data){
				if(typeof(data.error) != 'undefined' && data.error != ''){
					_self.properties.errorObj.show_error_block(data.error, 'error');
				}else{
					_self.properties.errorObj.show_error_block(data.success, 'success');
					_self.reload_list_folders();
				}
			}
		});
	}	

	this.send_message = function(){
		if( !_self.properties.chatId || !$('#'+_self.properties.mListId).length){
			clearTimeout(_self.properties.timeoutId);
			return;
		}

		$.ajax({
			url: _self.properties.siteUrl + _self.properties.urlSendMessage,
			type: 'POST',
			data: ({
				message: $('#'+_self.properties.mBoxId).val(),
				id_chat: _self.properties.chatId
				}),
			dataType: 'json',
			cache: false,
			success: function(data){
				if(typeof(data.error) != 'undefined' && data.error != ''){
					_self.properties.errorObj.show_error_block(data.error, 'error');
				}else{
					_self.get_new_messages();
					$('#'+_self.properties.mBoxId).val('');
				}
			}
		});
	}

	this.get_new_messages = function(){
		_self.get_messages(1, 0);
	}

	this.get_prev_messages = function(){
		_self.get_messages(0, _self.properties.messagesShown);
	}
	
	this.get_messages = function(isNew, offset){
		//// if chat not set or message list not exists - stop updater
		if( !_self.properties.chatId || !$('#'+_self.properties.mListId).length){
			clearTimeout(_self.properties.timeoutId);
			return;
		}
		$.ajax({
			url: _self.properties.siteUrl + _self.properties.urlGetMessages + _self.properties.chatId + '/'+ isNew +'/'+offset,
			dataType: 'json',
			cache: false,
			success: function(data){
				if(typeof(data.error) != 'undefined' && data.error != ''){
					_self.properties.errorObj.show_error_block(data.error, 'error');
				}else{
					if(typeof(data.html) != 'undefined' && data.html != ''){
						if(isNew){
							$('#'+_self.properties.mListId).prepend(data.html);
						}else{
							$('#'+_self.properties.mListId).append(data.html);
							if(data.next_btn){
								$('#next-page').show();
							}else{
								$('#next-page').hide();
							}
						}
					}
					_self.update_messages_shown();
					clearTimeout(_self.properties.timeoutId);
					_self.properties.timeoutId = setTimeout(_self.get_new_messages, _self.properties.reloadTimeout);
				}
			}
		});
	}


	this.delete_message = function(id_message){
		$.ajax({
			url: _self.properties.siteUrl + _self.properties.urlDeleteMessage + id_message,
			dataType: 'json',
			cache: false,
			success: function(data){
				if(typeof(data.error) != 'undefined' && data.error != ''){
					_self.properties.errorObj.show_error_block(data.error, 'error');
				}else{
					_self.properties.errorObj.show_error_block(data.success, 'success');
					$('#message_'+id_message).remove();
				}
				_self.update_messages_shown();
			}
		});
	}	
	
	this.update_messages_shown = function(){
		_self.properties.messagesShown = $('#'+_self.properties.mListId+' li').length;
	}
	
	_self.Init(optionArr);
}
