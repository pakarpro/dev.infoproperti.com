<?php
$module['module'] = 'mailbox';
$module['install_name'] = 'Mailbox';
$module['install_descr'] = 'Mailbox tool for site users';
$module['version'] = '2.02';
$module['files'] = array(
	array('file', 'read', "application/modules/mailbox/controllers/api_mailbox.php"),	
	array('file', 'read', "application/modules/mailbox/controllers/mailbox.php"),
	array('file', 'read', "application/modules/mailbox/helpers/mailbox_helper.php"),
	array('file', 'read', "application/modules/mailbox/install/module.php"),
	array('file', 'read', "application/modules/mailbox/install/permissions.php"),
	array('file', 'read', "application/modules/mailbox/install/settings.php"),
	array('file', 'read', "application/modules/mailbox/install/structure_deinstall.sql"),
	array('file', 'read', "application/modules/mailbox/install/structure_install.sql"),
	array('file', 'read', "application/modules/mailbox/js/mailbox.js"),
	array('file', 'read', "application/modules/mailbox/js/message_link.js"),
	array('file', 'read', "application/modules/mailbox/models/mailbox_install_model.php"),
	array('file', 'read', "application/modules/mailbox/models/mailbox_model.php"),
	array('file', 'read', "application/modules/mailbox/views/default/ajax_edit_folder.tpl"),
	array('file', 'read', "application/modules/mailbox/views/default/ajax_move_folder.tpl"),
	array('file', 'read', "application/modules/mailbox/views/default/block_chats.tpl"),
	array('file', 'read', "application/modules/mailbox/views/default/block_folders.tpl"),
	array('file', 'read', "application/modules/mailbox/views/default/block_list_folders.tpl"),
	array('file', 'read', "application/modules/mailbox/views/default/block_messages_item.tpl"),
	array('file', 'read', "application/modules/mailbox/views/default/chat.tpl"),
	array('file', 'read', "application/modules/mailbox/views/default/chats.tpl"),
	array('file', 'read', "application/modules/mailbox/views/default/folders.tpl"),
	array('file', 'read', "application/modules/mailbox/views/default/helper_contact.tpl"),
	array('file', 'read', "application/modules/mailbox/views/default/helper_contacts_count.tpl"),
	array('file', 'read', "application/modules/mailbox/views/default/helper_new_messages_header.tpl"),
	array('file', 'read', "application/modules/mailbox/views/default/helper_new_messages_homepage.tpl"),
	array('dir', 'read', 'application/modules/mailbox/langs'),
);

$module['dependencies'] = array(
	'start' => array('version'=>'1.01'),
	'menu' => array('version'=>'1.01'),
	'moderation' => array('version'=>'1.01'),
	'users' => array('version'=>'2.01')
);

$module['linked_modules'] = array(
	'install' => array(
		'menu'			=> 'install_menu',
		'moderation'	=> 'install_moderation',
		'site_map'		=> 'install_site_map'
	),
	'deinstall' => array(
		'menu'			=> 'deinstall_menu',
		'moderation'	=> 'deinstall_moderation',
		'site_map'		=> 'deinstall_site_map'
	)
);
