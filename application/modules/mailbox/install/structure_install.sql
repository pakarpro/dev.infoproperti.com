DROP TABLE IF EXISTS `[prefix]com_chats`;
CREATE TABLE IF NOT EXISTS `[prefix]com_chats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_com_user` int(11) NOT NULL,
  `id_from_user` int(11) NOT NULL,
  `new` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `id_folder` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_com_user` (`id_com_user`,`id_from_user`,`id_folder`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `[prefix]com_folders`;
CREATE TABLE IF NOT EXISTS `[prefix]com_folders` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `folder_type` varchar(20) NOT NULL,
  `id_user` int(3) NOT NULL,
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

INSERT INTO `[prefix]com_folders` VALUES(1, '', 'default', 0, '2011-04-27 13:23:38');
INSERT INTO `[prefix]com_folders` VALUES(2, '', 'trash', 0, '2011-04-27 13:23:59');
INSERT INTO `[prefix]com_folders` VALUES(3, '', 'favorites', 0, '2011-04-27 13:24:12');

DROP TABLE IF EXISTS `[prefix]com_messages`;
CREATE TABLE IF NOT EXISTS `[prefix]com_messages` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `id_chat` int(3) NOT NULL,
  `message` text NOT NULL,
  `message_type` varchar(30) NOT NULL,
  `is_new` tinyint(3) NOT NULL,
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_chat` (`id_chat`,`is_new`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;