<?php
/**
* Mailbox user side controller
*
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/


Class Mailbox extends Controller {

	/**
	 * link to CodeIgniter object
	 * @var object
	 */
	private $owner_id = 0;

	/**
	 * Constructor
	 */
	function __construct() {
		parent::Controller();
		$this->load->model("Mailbox_model");

		if ($this->session->userdata("auth_type") != "user") {
			show_404();
		}
		
		$user_type = $this->session->userdata('user_type');
		
		$this->owner_id = $this->session->userdata('user_id');
		$this->load->model('Menu_model');
		$this->Menu_model->breadcrumbs_set_parent('mailbox_item');
		$this->Menu_model->set_menu_active_item($user_type.'_account_menu', 'mailbox_item');	
	}

	public function index($id = null, $view = "full", $order_key = 'new', $order_direction = 'ASC', $page = 1) {
		$this->chats($id_folder, $order_key, $order_direction, $page);
	}

	public function chats($id, $order_key = 'new', $order_direction = 'ASC', $page = 1){
		$id = intval($id);
		$data = array(
			'order_key' => $order_key,
			'order_direction' => $order_direction,
			'page' => $page,
			'active_folder' => !empty($id) ? $id : $this->Mailbox_model->default_folder
		);
		
		$user_type = $this->session->userdata("user_type");
		$this->Menu_model->breadcrumbs_set_parent($user_type.'_mailbox_item');
		
		$this->template_lite->assign('folders_block', $this->_folders_block($data['active_folder']));
		$this->template_lite->assign('chats_block', $this->_chats_block($data['active_folder'], $order_key, $order_direction, $page));
		$this->template_lite->assign('data', $data);
		$this->template_lite->view('chats');
	}

	//// aliases
	public function chat($id_from_user) {
		if($this->pg_module->is_module_installed('users_services')) {
			$this->load->model('Users_services_model');
			$return = $this->Users_services_model->available_contact_action($this->owner_id, $id_from_user);
			if(!$return["available"]){
				show_404(); return;
			}
		}

		$chat_data = $this->Mailbox_model->is_chat_exists($this->owner_id, $id_from_user);
		if ($chat_data === false) {
			$id_chat = $this->Mailbox_model->create_chat($this->owner_id, $id_from_user);
			$id_folder = $this->Mailbox_model->default_folder;
			$chat_data = $this->Mailbox_model->get_chat_by_id($id_chat);
		} else {
			$id_chat = $chat_data["id"];
			$id_folder = $chat_data["id_folder"];
		}

		$data = array(
			'id_folder' => $id_folder,
			'id_chat' => $id_chat,
			'active_chat' => $id_chat,
			'active_folder' => $id_folder,
			"next_btn" => false
		);
		$this->template_lite->assign('chat', $chat_data);

		$params["where"]["id_chat"] = $id_chat;

		$data['messages_count'] = $messages_count = $this->Mailbox_model->get_messages_count($params);
		if($messages_count){
			$messages = $this->Mailbox_model->get_messages($params, 0, $this->pg_module->get_module_config('mailbox', 'items_per_page'));
			$this->template_lite->assign('messages', $messages);
			if (!empty($messages)) {
				foreach ($messages as $m) $ids[] = $m["id"];
				$this->Mailbox_model->set_read_messages($id_chat, $ids);

				$this->config->load('date_formats', TRUE);
				$this->template_lite->assign('date_format', $this->config->item('st_format_date_time_literal', 'date_formats'));
			}
			if(count($messages) < $messages_count) {
				$data["next_btn"] = true;
			}
		}

		$this->template_lite->assign('data', $data);
		$this->Menu_model->breadcrumbs_set_active(l('breadcrumb_chat_header', 'mailbox', '', 'text', array('name'=> $chat_data["user_data"]["output_name"])));
		$this->template_lite->view('chat');
	}

	public function folders() {
		$this->template_lite->assign('folders_block', $this->_folders_list_block());
		$this->template_lite->assign('data', $data);
		$this->Menu_model->breadcrumbs_set_active(l('breadcrumb_folders_header', 'mailbox'));
		$this->template_lite->view('folders');
	}

	///// private, block html
	private function _chats_block($id_folder = null, $order_key = 'new', $order_direction = 'ASC', $page = 1) {
		$folder = $this->Mailbox_model->get_folder_by_id($id_folder);
		$this->template_lite->assign('folder', $folder);

		$chat_params["where"]["id_folder"] = $id_folder;
		$chat_params["where"]["id_com_user"] = $this->owner_id;
		$chats_count = $this->Mailbox_model->get_chats_count($chat_params);

		if (!$page) $page = 1;
		$items_on_page = $this->pg_module->get_module_config('mailbox', 'items_per_page');
		$this->load->helper('sort_order');
		$page = get_exists_page_number($page, $chats_count, $items_on_page);

		$chats = $this->Mailbox_model->get_chats_list($chat_params, $page, $items_on_page, array($order_key => $order_direction));
		$this->template_lite->assign('chats', $chats);

		$this->load->helper("navigation");
		$url = site_url() . "mailbox/ajax_view_chats/" . $id_folder . "/" . $order_key . "/" . $order_direction . "/";
		$page_data = get_user_pages_data($url, $chats_count, $items_on_page, $page, 'briefPage');
		$page_data['order'] = $order_key;
		$page_data['order_direction'] = $order_direction;
		$this->template_lite->assign('page_data', $page_data);

		$this->template_lite->assign('sort_links', $sort_links);
		return $this->template_lite->fetch('block_chats');
	}

	///// private, block html
	private function _folders_block($id_active_folder) {
		$folders = $this->Mailbox_model->get_user_folders($this->owner_id);
		$this->template_lite->assign('folders', $folders);
		$this->template_lite->assign('id_active_folder', $id_active_folder);
		return $this->template_lite->fetch('block_folders');
	}

	private function _folders_list_block() {
		$folders = $this->Mailbox_model->get_user_folders($this->owner_id);
		$this->template_lite->assign('folders', $folders);
		return $this->template_lite->fetch('block_list_folders');
	}

	///// AJAX actions
	public function ajax_view_chats($id_folder, $order_key = 'new', $order_direction = 'ASC', $page = 1) {
		echo $this->_chats_block($id_folder, $order_key, $order_direction, $page);
	}

	public function ajax_view_folders($id_folder) {
		echo $this->_folders_block($id_folder);
	}

	public function ajax_view_list_folders() {
		echo $this->_folders_list_block();
	}

	public function ajax_save_folder() {
		$id = intval($this->input->post('id'));
		$id_user = $this->owner_id;
		$post_data["name"] = $this->input->post('name', true);

		$validate_data = $this->Mailbox_model->validate_folder($id, $id_user, $post_data);
		if (!empty($validate_data["errors"])) {
			$return["error"] = implode("<br>", $validate_data["errors"]);
			echo json_encode($return);
			return;
		} else {
			$return = $validate_data["data"];
			$return["folder_type"] = "user";
			$return["id_user"] = $id_user;
			$return["id"] = $id = $this->Mailbox_model->save_folder($id, $return);
			$return["success"] = l('success_folder_save', 'mailbox');
			echo json_encode($return);
			return;
		}
		return;
	}

	public function ajax_delete_folder($id_folder) {
		$this->Mailbox_model->delete_folder($id_folder);
		$return["success"] = l('success_folder_delete', 'mailbox');
		echo json_encode($return);
		return;
	}

	public function ajax_delete_chats() {
		$chats = $this->input->post('chats');
		if (!empty($chats)) {
			$chat_params["where_in"]["id"] = $chats;
			$chat_params["where"]["id_com_user"] = $this->owner_id;
			$chat_list = $this->Mailbox_model->get_chats_list($chat_params);

			if (!empty($chat_list)) {
				foreach ($chat_list as $chat) {
					$this->Mailbox_model->delete_chat($chat["id"]);
				}
			}
			$return["success"] = l('success_chats_delete', 'mailbox');
			echo json_encode($return);
			return;
		}

		$return["error"] = l('error_select_chats', 'mailbox');
		echo json_encode($return);
		return;
	}

	public function ajax_move_chats() {
		$id_folder = intval($this->input->post('id_folder'));
		$chats = $this->input->post('chats');
		if (!empty($chats)) {
			$chat_params["where_in"]["id"] = $chats;
			$chat_params["where"]["id_com_user"] = $this->owner_id;
			$chat_list = $this->Mailbox_model->get_chats_list($chat_params);

			if (!empty($chat_list)) {
				foreach ($chat_list as $chat) {
					$id_chat[] = $chat["id"];
				}
				$this->Mailbox_model->move_chat($id_chat, $id_folder);
			}
			$return["success"] = l('success_chats_moved', 'mailbox');
			echo json_encode($return);
			return;
		}

		$return["error"] = l('error_select_chats', 'mailbox');
		echo json_encode($return);
		return;
	}

	public function ajax_send_message() {
		$id_chat = intval($this->input->post('id_chat', true));
		$post_data["message"] = $this->input->post('message', true);
		if (!$id_chat) {
			echo "";
			return;
		}

		$validate_data = $this->Mailbox_model->validate_message($post_data);
		if (!empty($validate_data["errors"])) {
			$return["error"] = implode("<br>", $validate_data["errors"]);
			echo json_encode($return);
			return;
		} else {
			$this->Mailbox_model->create_message($id_chat, $validate_data["data"]["message"], "outbox", 1);

			/// get friend user chat and put message on it
			$chat = $this->Mailbox_model->get_chat_by_id($id_chat);
			$friend_chat = $this->Mailbox_model->is_chat_exists($chat["id_from_user"], $chat["id_com_user"]);
			if ($friend_chat === false) {
				$id_friend_chat = $this->Mailbox_model->create_chat($chat["id_from_user"], $chat["id_com_user"]);
			} else {
				$id_friend_chat = $friend_chat["id"];
			}
			$this->Mailbox_model->create_message($id_friend_chat, $validate_data["data"]["message"], "inbox", 1);

			$return["success"] = l('success_message_save', 'mailbox');
			echo json_encode($return);
			return;
		}
		return;
	}

	public function ajax_get_messages($id_chat, $new=true, $offset=0) {
		$chat = $this->Mailbox_model->get_chat_by_id($id_chat);
		if($this->pg_module->is_module_installed('users_services')) {
			$this->load->model('Users_services_model');
			$return = $this->Users_services_model->available_contact_action($chat['id_com_user'], $chat['id_from_user']);
			if(!$return["available"]){
				return '';
			}
		}

		$params["where"]["id_chat"] = $id_chat;
		if($new){
			$params["where"]["is_new"] = 1;
			$offset = $limit = 0;
		}else{
			$offset = intval($offset);
			$limit = $this->pg_module->get_module_config('mailbox', 'items_per_page');
			$return['messages_count'] = $messages_count = $this->Mailbox_model->get_messages_count($params);
		}

		$messages = $this->Mailbox_model->get_messages($params, $offset, $limit);

		$this->template_lite->assign('messages', $messages);

		$this->config->load('date_formats', TRUE);
		$this->template_lite->assign('date_format', $this->config->item('st_format_date_time_literal', 'date_formats'));

		$return["html"] = $this->template_lite->fetch('block_messages_item');
		$return["next_btn"] = false;

		if ($new && !empty($messages)) {
			foreach ($messages as $m) $ids[] = $m["id"];
			$this->Mailbox_model->set_read_messages($id_chat, $ids);
		}else{
			if($messages_count > $offset + count($messages) ){
				$return["next_btn"] = true;
			}
		}

		echo json_encode($return);
		return;
	}

	public function ajax_delete_message($id_message) {
		$message = $this->Mailbox_model->get_message_by_id($id_message);
		$id_chat = $message["id_chat"];
		$chat = $this->Mailbox_model->get_chat_by_id($id_chat);

		if ($chat["id_com_user"] != $this->owner_id) {
			$return["error"] = l('error_not_permissions_delete_message', 'mailbox');
			echo json_encode($return);
			return;
		} else {
			$this->Mailbox_model->delete_message($id_message);

			$return["success"] = l('message_successfully_deleted', 'mailbox');
			echo json_encode($return);
			return;
		}
	}

	public function ajax_form_folder($id_folder=0) {
		$id_folder = intval($id_folder);
		if($id_folder){
			$folder = $this->Mailbox_model->get_folder_by_id($id_folder);
			$this->template_lite->assign('folder', $folder);
		}

		$this->template_lite->view('ajax_edit_folder');
	}

	public function ajax_open_move_folder() {
		$folders = $this->Mailbox_model->get_user_folders($this->owner_id);
		$this->template_lite->assign('folders', $folders);

		$this->template_lite->view('ajax_move_folder');
	}

}
