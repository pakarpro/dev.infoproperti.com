<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
* Mailbox Model
*
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/




define('MAILBOX_FOLDERS_TABLE', DB_PREFIX.'com_folders');
define('MAILBOX_CHATS_TABLE', DB_PREFIX.'com_chats');
define('MAILBOX_MESSAGES_TABLE', DB_PREFIX.'com_messages');

class Mailbox_model extends Model
{
	private $CI;
	private $DB;
	private $fields_folders = array(
		'id',
		'name',
		'folder_type',
		'id_user',
		'date_add',
	);

	private $fields_chats = array(
		'id',
		'id_com_user',
		'id_from_user',
		'new',
		'total',
		'id_folder',
	);

	private $fields_messages = array(
		'id',
		'id_chat',
		'message',
		'message_type',
		'is_new',
		'date_add',
	);

	public $default_folder = 1;
	private $moderation_type = "mailbox";
	/**
	 * Constructor
	 *
	 * @return users object
	 */
	function __construct()
	{
		parent::Model();
		$this->CI = & get_instance();
		$this->DB = &$this->CI->db;
	}

	public function get_folder_by_id($id_folder){
		$this->DB->select(implode(", ", $this->fields_folders))->from(MAILBOX_FOLDERS_TABLE)->where("id", $id_folder);
		$result = $this->DB->get()->result_array();
		if(!empty($result)){
			$format = $this->format_folders(array($result[0]));
			return $format[0];
		}
		return array();
	}

	public function get_user_folders($id_user){
		$this->DB->select(implode(", ", $this->fields_folders))->from(MAILBOX_FOLDERS_TABLE);
		$this->DB->where("(id_user='0' AND folder_type IN ('default', 'trash', 'favorites')) OR (id_user='".$id_user."' AND folder_type='user')");
		$this->DB->order_by("id ASC");

		$result = $this->DB->get()->result_array();
		$folders = array();
		foreach($result as $folder){
			$folders[$folder['id']] = $folder;
		}
		if(!empty($result)){
			return $this->format_folders($folders, $id_user);
		}
		return array();
	}

	public function validate_folder($id, $id_user, $data){
		$return = array("errors"=> array(), "data" => array());

		if(isset($data["name"])){
			$return["data"]["name"] = trim(strip_tags($data["name"]));
			if(empty($return["data"]["name"])){
				$return["errors"][] = l('error_name_incorrect', 'mailbox');
			}
		}

		if($id){
			$folder_data = $this->get_folder_by_id($id);
			if($folder_data["id_user"] != $id_user){
				$return["errors"][] = l('error_user_is_not_owner', 'mailbox');
			}

			if($folder_data["folder_type"] != 'user'){
				$return["errors"][] = l('error_user_has_not_permissions', 'mailbox');
			}
		}
		return $return;
	}

	public function save_folder($id, $data){
		if (empty($id)){
			$data["date_add"] = date("Y-m-d H:i:s");
			$this->DB->insert(MAILBOX_FOLDERS_TABLE, $data);
			$id = $this->DB->insert_id();
		}else{
			$this->DB->where('id', $id);
			$this->DB->update(MAILBOX_FOLDERS_TABLE, $data);
		}
		return $id;
	}

	public function delete_folder($id){
		$this->DB->where('id', $id);
		$this->DB->delete(MAILBOX_FOLDERS_TABLE);

		$this->DB->where('id_folder', $id);
		$this->DB->delete(MAILBOX_CHATS_TABLE);
		return;
	}

	public function format_folders($data, $id_user=null){
		foreach($data as $folder){
			$folder_ids[] = $folder["id"];
		}

		$this->DB->select("id_folder, COUNT(*) AS chats")->from(MAILBOX_CHATS_TABLE);
		$this->DB->where_in("id_folder", $folder_ids);
		if(!empty($id_user)){
			$this->DB->where("id_com_user", $id_user);
		}
		$this->DB->group_by("id_folder");
		$result = $this->DB->get()->result_array();

		$chat_counts = array();
		if(!empty($result)){
			foreach($result as $r){
				$chat_counts[$r["id_folder"]] = $r["chats"];
			}
		}

		foreach($data as $k => $folder){
			if($folder["folder_type"] != "user"){
				$data[$k]["name"] = l('folder_name_'.$folder["folder_type"], 'mailbox');
			}
			$data[$k]["chats"] = isset($chat_counts[$folder["id"]])?$chat_counts[$folder["id"]]:0;
		}
		return $data;
	}

	/////// chat functions
	public function get_chat_by_id($id_chat){
		$this->DB->select(implode(", ", $this->fields_chats));
		$this->DB->from(MAILBOX_CHATS_TABLE)->where("id", $id_chat);
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			$data = array($results[0]);
			$data = $this->format_chats($data);
			return $data[0];
		}
		return array();
	}

	public function get_chat($id_com_user, $id_from_user){
		$this->DB->select(implode(", ", $this->fields_chats));
		$this->DB->from(MAILBOX_CHATS_TABLE)->where("id_com_user", $id_com_user)->where('id_from_user', $id_from_user);
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			$data = array($results[0]);
			$data = $this->format_chats($data);
			return $data[0];
		}
		return array();
	}

	public function get_chats_list($params=array(), $page=null, $limits=null, $order_by=null){
		$this->DB->select(implode(", ", $this->fields_chats));
		$this->DB->from(MAILBOX_CHATS_TABLE);

		if(isset($params["where"]) && is_array($params["where"]) && count($params["where"])){
			foreach($params["where"] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])){
			foreach($params["where_in"] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])){
			foreach($params["where_sql"] as $value){
				$this->DB->where($value);
			}
		}

		if (is_array($order_by) && count($order_by) > 0) {
			foreach ($order_by as $field => $dir) {
				if (in_array($field, $this->fields_chats)) {
					$this->DB->order_by($field . " " . $dir);
				}
			}
		} else if ($order_by) {
			$this->DB->order_by($order_by);
		}

		if(!is_null($page) ){
			$page = intval($page)?intval($page):1;
			$this->DB->limit($limits, $limits*($page-1));
		}

		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			foreach($results as $r){
				$data[] = $r;
			}
			return $this->format_chats($data);
		}
		return array();
	}

	public function get_chats_count($params){
		$this->DB->select("COUNT(*) AS cnt");
		$this->DB->from(MAILBOX_CHATS_TABLE);

		if(isset($params["where"]) && is_array($params["where"]) && count($params["where"])){
			foreach($params["where"] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])){
			foreach($params["where_in"] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])){
			foreach($params["where_sql"] as $value){
				$this->DB->where($value);
			}
		}

		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			return intval($results[0]["cnt"]);
		}
		return 0;
	}

	public function is_chat_exists($id_com_user, $id_from_user){
		$params["where"]['id_com_user'] = $id_com_user;
		$params["where"]['id_from_user'] = $id_from_user;
		$data = $this->get_chats_list($params);
		if(count($data) > 0){
			return $data[0];
		}else{
			return false;
		}
	}

	public function create_chat($id_com_user, $id_from_user){
		$data["id_com_user"] = $id_com_user;
		$data["id_from_user"] = $id_from_user;
		$data["id_folder"] = $this->default_folder;
		$this->DB->insert(MAILBOX_CHATS_TABLE, $data);
		$id = $this->DB->insert_id();
		return $id;
	}

	public function move_chat($id_chat, $id_folder){
		$data["id_folder"] = intval($id_folder);
		if(is_array($id_chat)){
			$this->DB->where_in('id', $id_chat);
		}else{
			$this->DB->where('id', $id_chat);
		}
		$this->DB->update(MAILBOX_CHATS_TABLE, $data);
		return;
	}

	public function delete_chat($id){
		$this->DB->where('id', $id);
		$this->DB->delete(MAILBOX_CHATS_TABLE);

		$this->DB->where('id_chat', $id);
		$this->DB->delete(MAILBOX_MESSAGES_TABLE);
		return;
	}

	public function format_chats($data){
		if(!empty($data)){
			$users_ids = array();
			foreach($data as $chat){
				if(!in_array($chat["id_com_user"], $users_ids)) $users_ids[] = $chat["id_com_user"];
				if(!in_array($chat["id_from_user"], $users_ids)) $users_ids[] = $chat["id_from_user"];
			}

			$this->CI->load->model('Users_model');
			$temp = $this->CI->Users_model->get_users_list(null, null, null, array(), $users_ids);
			foreach($temp as $d){
				$user_data[$d["id"]] = $d;
			}

			foreach($data as $k => $chat){
				if(!empty($user_data[$chat["id_from_user"]])){
					$data[$k]["user_data"] = $user_data[$chat["id_from_user"]];
				}
				if(!empty($user_data[$chat["id_com_user"]])){
					$data[$k]["owner_data"] = $user_data[$chat["id_com_user"]];
				}
			}
		}
		return $data;
	}

	public function update_chat_statistic($id){
		$this->DB->select('COUNT(*) AS total_count')->from(MAILBOX_MESSAGES_TABLE)->where('id_chat', $id);
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			$data["total"] = intval($results[0]["total_count"]);
		}else{
			$data["total"] = 0;
		}

		$this->DB->select('COUNT(*) AS new_count')->from(MAILBOX_MESSAGES_TABLE)->where('id_chat', $id)->where("is_new", 1);
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			$data["new"] = intval($results[0]["new_count"]);
		}else{
			$data["new"] = 0;
		}

		$this->DB->where('id', $id);
		$this->DB->update(MAILBOX_CHATS_TABLE, $data);
	}

	//// messages functions
	public function get_message_by_id($id){
		$this->DB->select(implode(", ", $this->fields_messages))->from(MAILBOX_MESSAGES_TABLE)->where("id", $id);
		$result = $this->DB->get()->result_array();
		if(!empty($result)){
			$format = $this->format_messages(array($result[0]));
			return $format[0];
		}
		return array();
	}

	public function create_message($id_chat, $message, $message_type, $is_new){
		$data["id_chat"] = $id_chat;
		$data["message"] = $message;
		$data["message_type"] = $message_type;
		$data["is_new"] = $is_new;
		$data["date_add"] = date("Y-m-d H:i:s");
		$this->DB->insert(MAILBOX_MESSAGES_TABLE, $data);
		$id = $this->DB->insert_id();

		$this->update_chat_statistic($id_chat);
		return $id;
	}

	public function validate_message($data){
		$return = array("errors"=> array(), "data" => array());

		if(isset($data["message"])){
			$return["data"]["message"] = trim(strip_tags($data["message"]));
			if(empty($return["data"]["message"])){
				$return["errors"][] = l('error_empty_message', 'mailbox');
			}
			$this->CI->load->model('moderation/models/Moderation_badwords_model');
			$bw_count = $this->CI->Moderation_badwords_model->check_badwords($this->moderation_type, $return["data"]["message"]);
			if($bw_count){
				$return["errors"][] = l('error_badwords_message', 'mailbox');
			}
		}

		return $return;
	}

	public function delete_message($id){
		$message_data = $this->get_message_by_id($id);
		$id_chat = $message_data["id_chat"];

		$this->DB->where('id', $id);
		$this->DB->delete(MAILBOX_MESSAGES_TABLE);

		$this->update_chat_statistic($id_chat);
	}

	public function get_messages($params=array(), $offset=null, $limits=null){
		$this->DB->select(implode(", ", $this->fields_messages))->from(MAILBOX_MESSAGES_TABLE);
		if(isset($params["where"]) && is_array($params["where"]) && count($params["where"])){
			foreach($params["where"] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])){
			foreach($params["where_in"] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])){
			foreach($params["where_sql"] as $value){
				$this->DB->where($value);
			}
		}

		if($limits){
			$this->DB->limit($limits, intval($offset));
		}
		
		$this->DB->order_by("date_add DESC");
				
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			foreach($results as $r){
				$data[] = $r;
			}
			return $this->format_messages($data);
		}
		return array();
	}

	public function get_new_messages_count($id_user){
		$this->DB->select('SUM(new) AS cnt')->from(MAILBOX_CHATS_TABLE)->where('id_com_user', $id_user)->where('new <>', 0);

		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			return intval($results[0]["cnt"]);
		}
		return 0;
	}

	public function get_messages_count($params = array()){
		$this->DB->select('COUNT(*) AS cnt')->from(MAILBOX_MESSAGES_TABLE);
		if(isset($params["where"]) && is_array($params["where"]) && count($params["where"])){
			foreach($params["where"] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])){
			foreach($params["where_in"] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])){
			foreach($params["where_sql"] as $value){
				$this->DB->where($value);
			}
		}

		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			return intval($results[0]["cnt"]);
		}
		return 0;
	}

	public function set_read_messages($id_chat, $ids){
		$data["is_new"] = 0;
		$this->DB->where_in('id', $ids);
		$this->DB->where('id_chat', $id_chat);
		$this->DB->update(MAILBOX_MESSAGES_TABLE, $data);
		$this->update_chat_statistic($id_chat);
	}

	public function format_messages($data){
		$chats_ids = array();
		foreach($data as $m){
			if(!in_array($m["id_chat"], $chats_ids)) $chats_ids[] = $m["id_chat"];
		}

		$params["where_in"]["id"] = $chats_ids;
		$temp = $this->get_chats_list($params);
		foreach($temp as $d){
			$chats[$d["id"]] = $d;
		}

		foreach($data as $k=>$v){
			$data[$k]["message"] = nl2br($v["message"]);
			if($v["message_type"] == 'inbox'){
				$data[$k]["user_data"] = $chats[$v["id_chat"]]["user_data"];
			}else{
				$data[$k]["user_data"] = $chats[$v["id_chat"]]["owner_data"];
			}
		}

		return $data;
	}

	////// seo
	function get_seo_settings($method='', $lang_id=''){
		if(!empty($method)){
			return $this->_get_seo_settings($method, $lang_id);
		}else{
			$actions = array('index');
			$return = array();
			foreach($actions as $action){
				$return[$action] = $this->_get_seo_settings($action, $lang_id);
			}
			return $return;
		}
	}

	function _get_seo_settings($method, $lang_id=''){
		if($method == "index"){
			return array(
				"title" => l('seo_tags_index_title', 'mailbox', $lang_id, 'seo'),
				"keyword" => l('seo_tags_index_keyword', 'mailbox', $lang_id, 'seo'),
				"description" => l('seo_tags_index_description', 'mailbox', $lang_id, 'seo'),
				"templates" => array(),
				"header" => l('seo_tags_index_header', 'mailbox', $lang_id, 'seo'),
				"url_vars" => array()
			);
		}
	}

	function request_seo_rewrite($var_name_from, $var_name_to, $value){
		return $value;
	}

	function get_sitemap_xml_urls(){
		$this->CI->load->helper('seo');
		$return = array(
			array(
				"url" => rewrite_link('mailbox', 'index'),
				"priority" => 0.1
			)
		);
		return $return;
	}

	function get_sitemap_urls(){
		$this->CI->load->helper('seo');
		$auth = $this->CI->session->userdata("auth_type");

		$block[] = array(
			"name" => l('header_mailbox', 'mailbox'),
			"link" => rewrite_link('mailbox', 'index'),
			"clickable" => ($auth=="user")?true:false,
		);
		return $block;
	}

}
