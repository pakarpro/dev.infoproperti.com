{if $count_active > 1}
<select onchange="javascript: load_language(this.value);">
{foreach item=item from=$languages}{if $item.status eq '1'}<option value="{$item.id}" {if $item.id eq $current_lang} selected{/if}>{$item.name}</option>{/if}{/foreach}
</select>
<script type="text/javascript">
var lang_url = '{$site_url}languages/change_lang/';
{literal}
function load_language(value){
	location.href = lang_url + value;
}
{/literal}
</script>
{/if}
