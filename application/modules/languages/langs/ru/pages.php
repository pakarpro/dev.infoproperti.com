<?php

$install_lang["admin_header_ds_add"] = "Добавить источник данных";
$install_lang["admin_header_ds_change"] = "Редактирование названия источника данных";
$install_lang["admin_header_ds_edit"] = "Редактирование источника данных";
$install_lang["admin_header_ds_item_add"] = "Добавить вариант";
$install_lang["admin_header_ds_item_change"] = "Изменить вариант";
$install_lang["admin_header_ds_item_edit"] = "Изменить вариант источника данных";
$install_lang["admin_header_ds_items"] = "Варианты источников данных";
$install_lang["admin_header_ds_list"] = "Источники данных";
$install_lang["admin_header_language_add"] = "Добавление данных";
$install_lang["admin_header_language_change"] = "Редактирование данных";
$install_lang["admin_header_language_edit"] = "Редактирование языка";
$install_lang["admin_header_languages_list"] = "Языки";
$install_lang["admin_header_page_add"] = "Добавление фразы";
$install_lang["admin_header_page_change"] = "Редактирование фразы";
$install_lang["admin_header_pages_list"] = "Страницы";
$install_lang["api_error_empty_lang_id"] = "Не указан id языка";
$install_lang["default_editable_text"] = "Редактировать";
$install_lang["error_code_exists"] = "Такой код языка уже существует";
$install_lang["error_code_incorrect"] = "Неверный код языка";
$install_lang["error_default_inactive_lang"] = "Чтобы установить язык в качестве стандартного по умолчанию, сперва нужно его активировать";
$install_lang["error_delete_default_lang"] = "Нельзя удалять стандартный язык";
$install_lang["error_gid_exists"] = "Фраза с таким системным именем уже существует";
$install_lang["error_gid_incorrect"] = "Неверное системное имя";
$install_lang["error_name_incorrect"] = "Неверное название языка";
$install_lang["error_rtl_incorrect"] = "Не установлен параметр направления текста RTL";
$install_lang["field_active"] = "Активность";
$install_lang["field_code"] = "Код";
$install_lang["field_default"] = "По умолчанию";
$install_lang["field_ds_name"] = "Источник данных";
$install_lang["field_gid"] = "Системное имя";
$install_lang["field_language_name"] = "Название";
$install_lang["field_name"] = "Название";
$install_lang["field_rtl"] = "Направление текста";
$install_lang["field_rtl_value_ltr"] = "Слева направо";
$install_lang["field_rtl_value_rtl"] = "Справа налево";
$install_lang["field_text"] = "Текст";
$install_lang["link_activate_language"] = "Активировать язык";
$install_lang["link_add_ds"] = "Добавить справочник";
$install_lang["link_add_ds_item"] = "Добавить вариант";
$install_lang["link_add_lang"] = "Добавить язык";
$install_lang["link_add_page"] = "Добавить фразу";
$install_lang["link_deactivate_language"] = "Отключить язык";
$install_lang["link_delete_ds"] = "Отключить источник данных";
$install_lang["link_delete_lang"] = "Удалить язык";
$install_lang["link_delete_page"] = "Удалить фразу";
$install_lang["link_delete_selected"] = "Удалить выбранное";
$install_lang["link_edit_ds"] = "Редактировать название источника данных";
$install_lang["link_edit_ds_items"] = "Редактировать варианты источника данных";
$install_lang["link_edit_lang"] = "Редактировать язык";
$install_lang["link_edit_page"] = "Редактировать фразу";
$install_lang["link_make_default_language"] = "Использовать язык по умолчанию";
$install_lang["link_resort_items"] = "Сохранить последовательность";
$install_lang["note_delete_ds"] = "Вы уверены, что хотите удалить этот источник данных?";
$install_lang["note_delete_ds_item"] = "Вы уверены, что хотите удалить этот вариант?";
$install_lang["note_delete_dses"] = "Вы уверены, что хотите удалить выбранные источники данных?";
$install_lang["note_delete_lang"] = "Вы уверены, что хотите удалить этот язык?";
$install_lang["note_delete_page"] = "Вы уверены, что хотите удалить эту фразу?";
$install_lang["note_delete_pages"] = "Вы уверены, что хотите удалить выбранные фразы?";
$install_lang["others_languages"] = "Другие языки";
$install_lang["success_added_ds"] = "Источник данных успешно создан";
$install_lang["success_added_ds_item"] = "Вариант добавлен";
$install_lang["success_added_lang"] = "Язык успешно добавлен";
$install_lang["success_added_page"] = "Фраза успешно добавлена";
$install_lang["success_defaulted_lang"] = "Язык используется по умолчанию";
$install_lang["success_deleted_lang"] = "Язык удален";
$install_lang["success_deleted_page"] = "Фраза удалена";
$install_lang["success_updated_ds"] = "Источник данных успешно обновлен";
$install_lang["success_updated_ds_item"] = "Фраза успешно изменена";
$install_lang["success_updated_lang"] = "Язык успешно обновлен";
$install_lang["success_updated_page"] = "Фраза успешно обновлена";
$install_lang["text_default_language"] = "Язык используется по умолчанию";
$install_lang["text_module_select"] = "Выбрать модуль";

