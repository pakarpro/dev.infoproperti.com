<?php
/**
* Dynamic blocks install model
*
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/

class Dynamic_blocks_install_model extends Model
{
	private $CI;
	private $menu = array(
		'admin_menu' => array(
			'action' => 'none',
			'items' => array(
				'settings_items' => array(
					'action' => 'none',
					'items' => array(
						'interface-items' => array(
							'action'=>'none',
							'items' => array(
								'dynblock_menu_item' => array('action' => 'create', 'link' => 'admin/dynamic_blocks', 'status' => 1, 'sorter' => 1)
							)
						)
					)
				)
			)
		),
		'admin_dynblocks_menu' => array(
			'action' => 'create',
			'name' => 'Admin mode - Interface - Dynamic blocks',
			'items' => array(
				'areas_list_item' => array('action' => 'create', 'link' => 'admin/dynamic_blocks', 'status' => 1),
				'blocks_list_item' => array('action' => 'create', 'link' => 'admin/dynamic_blocks/blocks', 'status' => 0)
			)
		)
	);
	
	private $ausers_methods = array(
		array('module' => 'dynamic_blocks', 'method' => 'index', 'is_default' => 1),
		array('module' => 'dynamic_blocks', 'method' => 'area_blocks', 'is_default' => 0),
	);

	/**
	 * Dynamic blocks
	 * @var array
	 */
	private $dynamic_blocks = array(
		array(
			"gid" => "html_code_block",
			"module" => "dynamic_blocks",
			"model" => "Dynamic_blocks_model",
			"method" => "_dynamic_block_get_html_code",
			"params" => array(
				"title"=>array("gid"=>"title", "type"=>"text", "default"=>""),
				"html"=>array("gid"=>"html", "type"=>"textarea", "default"=>""),
			),
			"views" => array(array("gid"=>"default")),
			"area" => array(
				"gid" => "index-page", 
				"params" => array(
					'title_1'=>'Join us in Facebook', 
					'title_2'=>'Найдите нас на Facebook', 
					'html'=>"<div id=\"fb-root\"></div>\n<script>(function(d, s, id) {\n  var js, fjs = d.getElementsByTagName(s)[0];\n  if (d.getElementById(id)) return;\n  js = d.createElement(s); js.id = id;\n  js.src = \"//connect.facebook.net/ru_RU/all.js#xfbml=1\";\n  fjs.parentNode.insertBefore(js, fjs);\n}(document, 'script', 'facebook-jssdk'));</script>\n<div class=\"fb-like-box\" data-href=\"https://facebook.com/pilotgroup.net\" data-width=\"470\" data-show-faces=\"true\" data-stream=\"false\" data-header=\"true\"></div>",
				),
				"view_str" => "default", 
				'width' => 50,
				"cache_time" => "600", 
				"sorter" => 4,
			),
		),
	);
	
	/**
	 * Constructor
	 *
	 * @return Install object
	 */
	function Dynamic_blocks_install_model()
	{
		parent::Model();
		$this->CI = & get_instance();
		//// load langs
		$this->CI->load->model('Install_model');
	}

	public function install_menu() {
		$this->CI->load->helper('menu');
		foreach($this->menu as $gid => $menu_data){
			$this->menu[$gid]['id'] = linked_install_set_menu($gid, $menu_data["action"], $menu_data["name"]);
			linked_install_process_menu_items($this->menu, 'create', $gid, 0, $this->menu[$gid]["items"]);
		}
	}

	public function install_menu_lang_update($langs_ids = null) {
		if(empty($langs_ids)) return false;
		$langs_file = $this->CI->Install_model->language_file_read('dynamic_blocks', 'menu', $langs_ids);

		if(!$langs_file) { log_message('info', 'Empty menu langs data'); return false; }

		$this->CI->load->helper('menu');

		foreach($this->menu as $gid => $menu_data){
			linked_install_process_menu_items($this->menu, 'update', $gid, 0, $this->menu[$gid]["items"], $gid, $langs_file);
		}
		return true;
	}

	public function install_menu_lang_export($langs_ids) {
		if(empty($langs_ids)) return false;
		$this->CI->load->helper('menu');

		$return = array();
		foreach($this->menu as $gid => $menu_data){
			$temp = linked_install_process_menu_items($this->menu, 'export', $gid, 0, $this->menu[$gid]["items"], $gid, $langs_ids);
			$return = array_merge($return, $temp);
		}
		return array( "menu" => $return );
	}

	public function deinstall_menu() {
		$this->CI->load->helper('menu');
		foreach($this->menu as $gid => $menu_data){
			if($menu_data['action'] == 'create'){
				linked_install_set_menu($gid, 'delete');
			}else{
				linked_install_delete_menu_items($gid, $this->menu[$gid]['items']);
			}
		}
	}

	/**
	 * Ausers module methods
	 */
	public function install_ausers() {
		// install ausers permissions
		$this->CI->load->model('Ausers_model');

		foreach($this->ausers_methods as $method){
			$this->CI->Ausers_model->save_method(null, $method);
		}
	}

	public function install_ausers_lang_update($langs_ids = null) {
		$langs_file = $this->CI->Install_model->language_file_read('dynamic_blocks', 'ausers', $langs_ids);

		// install ausers permissions
		$this->CI->load->model('Ausers_model');
		$params['where']['module'] = 'dynamic_blocks';
		$methods = $this->CI->Ausers_model->get_methods_lang_export($params);

		foreach($methods as $method){
			if(!empty($langs_file[$method['method']])){
				$this->CI->Ausers_model->save_method($method['id'], array(), $langs_file[$method['method']]);
			}
		}
	}

	public function install_ausers_lang_export($langs_ids) {
		$this->CI->load->model('Ausers_model');
		$params['where']['module'] = 'dynamic_blocks';
		$methods =  $this->CI->Ausers_model->get_methods_lang_export($params, $langs_ids);
		foreach($methods as $method){
			$return[$method['method']] = $method['langs'];
		}
		return array('ausers' => $return);
	}

	public function deinstall_ausers() {
		// delete moderation methods in ausers
		$this->CI->load->model('Ausers_model');
		$params['where']['module'] = 'dynamic_blocks';
		$this->CI->Ausers_model->delete_methods($params);
	}

	function _arbitrary_installing(){
		$this->CI->load->model("Dynamic_blocks_model");
	
		$area_ids = array();
		
		foreach((array)$this->dynamic_blocks as $block_data){
			
			$validate_data = $this->CI->Dynamic_blocks_model->validate_block(null, $block_data);
			if(!empty($validate_data["errors"])) continue;
			$id_block = $this->CI->Dynamic_blocks_model->save_block(null, $validate_data["data"]);
		
			if(!isset($block_data["area"])) continue;
		
			if(!isset($area_ids[$block_data["area"]["gid"]])){
				$area = $this->CI->Dynamic_blocks_model->get_area_by_gid($block_data["area"]["gid"]);
				$area_ids[$block_data["area"]["gid"]] = $area["id"];
			}

			// index area
			$block_data["area"]["id_area"] = $area_ids[$block_data["area"]["gid"]];
			$block_data["area"]["id_block"] = $id_block;
			$block_data["area"]["params"] = serialize($block_data["area"]["params"]);
	
			$validate_data = $this->CI->Dynamic_blocks_model->validate_area_block($block_data["area"], true);
			if(!empty($validate_data["errors"])) continue;
			$this->CI->Dynamic_blocks_model->save_area_block(null, $validate_data["data"]);
		}
	}
	
	/**
	 * Import module languages
	 */
	public function _arbitrary_lang_install($langs_ids=null){
		if(!is_array($langs_ids)) $langs_ids = (array)$langs_ids;	
		if(empty($langs_ids)) return false;
		
		$langs_file = $this->CI->Install_model->language_file_read("dynamic_blocks", "arbitrary", $langs_ids);
		if(!$langs_file){log_message("info", "Empty dynamic_blocks langs data");return false;}
	
		$this->CI->load->model("Dynamic_blocks_model");
		
		$data = array();
		
		foreach((array)$this->dynamic_blocks as $block_data){
			$block = $this->CI->Dynamic_blocks_model->get_block_by_gid($block_data["gid"]);
			if(!$block) continue;
			$data[] = $block;
		}
		
		$this->CI->Dynamic_blocks_model->update_langs($data, $langs_file, $langs_ids);
	}
	
	/**
	 * Export module languages
	 * @param array $langs_ids
	 */
	public function _arbitrary_lang_export($langs_ids=null){
		if(!is_array($langs_ids)) $langs_ids = (array)$langs_ids;
		
		$this->CI->load->model("Dynamic_blocks_model");
		
		$data = array();
		
		foreach((array)$this->dynamic_blocks as $block_data){
			$block = $this->CI->Dynamic_blocks_model->get_block_by_gid($block_data["gid"]);
			if(!$block) continue;
			$data[] = $block;
		}
		
		$langs = $this->CI->Dynamic_blocks_model->export_langs($data, $langs_ids);
		return array("dynamic_blocks" => $langs);
	}

	function _arbitrary_deinstalling(){
		$this->CI->load->model("Dynamic_blocks_model");
		foreach((array)$this->dynamic_blocks as $block_data){
			$this->CI->Dynamic_blocks_model->delete_block_by_gid($block_data["gid"]);
		}
	}

}
