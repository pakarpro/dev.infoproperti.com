{include file="header.tpl" load_type='ui'}
{*helper func_name=get_admin_level1_menu helper_name=menu func_param='admin_dynblocks_menu'*}
<div class="actions">
	<ul>
		<li><div class="l"><a href="#" onclick="javascript: mlSorter.update_sorting(); return false">{l i='link_save_block_sorting' gid='dynamic_blocks'}</a></div></li>
	</ul>
	&nbsp;
</div>

<div class="menu-level3">
	<ul>
		<li class="active"><a href="{$site_url}admin/dynamic_blocks/area_blocks/{$area.id}">{l i='filter_area_blocks' gid='dynamic_blocks'}</a></li>
		<li><a href="{$site_url}admin/dynamic_blocks/area_layout/{$area.id}">{l i='filter_area_layout' gid='dynamic_blocks'}</a></li>
	</ul>
	&nbsp;
</div>

<div class="filter-form">
	<form action="" method="post">
	<h3>{l i='add_area_block_header' gid='dynamic_blocks'}:</h3>
	<select name="id_block">
	<option value="0">...</option>
	{foreach item=item key=block_id from=$blocks}
	<option value="{$block_id}">{l i=$item.name_i gid=$item.lang_gid}</option>
	{/foreach}
	</select>
	<input type="submit" name="add_block" value="{l i='link_add_block' gid='dynamic_blocks' type='button'}">
	</form>
</div>

<div id="area_blocks">
<ul name="parent_0" class="sort connected" id="clsr0ul">
{foreach item=item from=$area_blocks}
<li id="item_{$item.id}">
	<div class="icons">
		<a href='#' onclick="if (confirm('{l i='note_delete_area_block' gid='dynamic_blocks' type='js'}')) mlSorter.deleteItem({$item.id});return false;"><img src="{$site_root}{$img_folder}icon-delete.png" width="16" height="16" alt="{l i='btn_delete' gid='start'}" title="{l i='btn_delete' gid='start'}"></a>
	</div>

	<form id="form_item_{$item.id}">
	<h3>{l i=$item.block_data.name_i gid=$item.block_data.lang_gid}</h3>
	<div class="edit-form n150">
	{foreach item=param from=$item.block_data.params_data}
		{assign var="param_gid" value=$param.gid}
		<div class="row" id="row_{$item.id}_{$param.gid}">
			<div class="h">{l i=$param.i gid=$item.block_data.lang_gid}: </div>
			<div class="v">
				{if $param.type eq 'textarea'}
				
				<textarea name="params[{$param_gid}]" rows="5" cols="80">{$item.params[$param_gid]|escape}</textarea>
				
				{elseif $param.type eq 'text'}
				
				{foreach item=lang_item key=lang_id from=$langs}
					{assign var=value_id value=$param_gid+'_'+$lang_id}
					<input type="{if $lang_id eq $current_lang_id}text{else}hidden{/if}" name="params[{$param_gid}_{$lang_id}]" value="{$item.params[$value_id]|strval|escape}" lang-editor="value" lang-editor-type="params-{$param_gid}" lang-editor-lid="{$lang_id}">
				{/foreach}
				<a href="#" lang-editor="button" lang-editor-type="params-{$param_gid}"><img src="{$site_root}{$img_folder}icon-translate.png" width="16" height="16"></a>

				{elseif $param.type eq 'int'}
				
				<input type="text" value="{$item.params[$param_gid]|intval|escape}" name="params[{$param_gid}]">
				
				{else}

				<input type="text" value="{$item.params[$param_gid]|escape}" name="params[{$param_gid}]">

				{/if}
			</div>
		</div>
	{/foreach}
		<div class="row">
			<div class="h">{l i='field_view' gid='dynamic_blocks'}: </div>
			<div class="v">
				<select name="view_str">
				{foreach item=view from=$item.block_data.views_data}
				{assign var="view_gid" value=$view.gid}
				<option value="{$view_gid}" {if $view_gid eq $item.view_str}selected{/if}>{l i=$view.i gid=$item.block_data.lang_gid}</option>
				{/foreach}
				</select>
			</div>
		</div>
		<div class="row">
			<div class="h">{l i='field_cache_time' gid='dynamic_blocks'}: </div>
			<div class="v"><input type="text" value="{$item.cache_time}" name="cache_time" class="short"> <i>{l i='field_cache_time_text' gid='dynamic_blocks'}</i></div>
		</div>
		<div class="row">
			<div class="h">&nbsp;</div>
			<div class="v"><input type="button" name="save_data" value="{l i='btn_save' gid='start' type='button'}" onclick="javascript: saveBlock('{$item.id}');"></div>
		</div>
		<div class="clr"></div>
	</div>
	</form>
</li>
{/foreach}
</ul>
</div>
{block name=lang_inline_editor module=start}
<script >
	var save_url = '{$site_url}admin/dynamic_blocks/ajax_save_area_block/';
{literal}
	var mlSorter;
	$(function(){
		mlSorter = new multilevelSorter({
			siteUrl: '{/literal}{$site_url}{literal}', 
			urlSaveSort: 'admin/dynamic_blocks/save_area_block_sorter/{/literal}{$area.id}{literal}',
			urlDeleteItem: 'admin/dynamic_blocks/ajax_delete_area_block/',
		});
	});

	function saveBlock(id){
		$.ajax({
			url: save_url + id, 
			type: 'POST',
			data: $('#form_item_'+id).serialize(), 
			cache: false,
			success: function(data){
				error_object.show_error_block('{/literal}{l i="success_update_area_block" gid="dynamic_blocks" type="js"}{literal}', 'success');
			}
		});
	}
{/literal}</script>{include file="footer.tpl"}
