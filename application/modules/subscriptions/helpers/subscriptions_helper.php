<?php

if ( ! function_exists('get_user_subscriptions_form'))
{
	function get_user_subscriptions_form($place){
		$CI = & get_instance();
		$CI->load->model('Subscriptions_model');
		$CI->load->model('subscriptions/models/Subscriptions_users_model');
		$params = array();
		$user_subscription = array();
		if(isset($_REQUEST['user_subscriptions_list'])){
			$params = array('where' => array('subscribe_type' => 'user'));
			$user_s = $_REQUEST['user_subscriptions_list'];
			foreach ($user_s as $key => $value) {
				$user_subscription[$value]  = 1;
			}
		}else{
			$user_id = $CI->session->userdata('user_id');
			
			$CI->load->model('Users_model');
			$subscriber_types = array_keys($CI->Users_model->get_user_types($user_id));
			$query = "subscribe_type = 'user' AND (";
			foreach($subscriber_types as $user_type) {
				$query .= "subscriber_type='$user_type' OR ";
			}
			$query = mb_substr($query, 0, -4) . ')';
			$params['where_sql'][] = $query;
			
			if ($user_id){
				$user_subscription = $CI->Subscriptions_users_model->get_subscriptions_by_id_user($user_id);
			}
		}
		$subscriptions_list = $CI->Subscriptions_model->get_subscriptions_list(null, null, null, $params);
		foreach ($subscriptions_list as $key => $subcription) {
			if (isset($user_subscription[$subcription['id']])){
				$subscriptions_list[$key]['subscribed'] = 1;
			}
		}
		$CI->template_lite->assign('subscriptions_list', $subscriptions_list);
		$html = $CI->template_lite->fetch('helper_form_' . $place, 'user', 'subscriptions');
		return $html;
	}
}
if ( ! function_exists('get_user_subscriptions_list'))
{
	function get_user_subscriptions_list(){
		$CI = & get_instance();
		$CI->load->model('Subscriptions_model');
		$CI->load->model('subscriptions/models/Subscriptions_users_model');

		$user_id = $CI->session->userdata('user_id');
		$subscriber_types = array_keys($CI->Users_model->get_user_types($user_id));
		$query = "subscribe_type = 'user' AND (";
		foreach($subscriber_types as $user_type) {
			$query .= " subscriber_type='$user_type' OR";
		}
		$query = mb_substr($query, 0, -3) . ')';
		$params = array();
		$params['where_sql'][] = $query;
		$subscriptions_list = $CI->Subscriptions_model->get_subscriptions_list(null, null, null, $params);
		if ($user_id){
			$user_subscription = $CI->Subscriptions_users_model->get_subscriptions_by_id_user($user_id);
			foreach ($subscriptions_list as $key => $subcription) {
				if (isset($user_subscription[$subcription['id']])){
					$subscriptions_list[$key]['subscribed'] = 1;
				}
			}
		}

		$CI->template_lite->assign('subscriptions_list', $subscriptions_list);
		$html = $CI->template_lite->fetch('user_subscription_list', 'user', 'subscriptions');
		return $html;
	}
}
