<?php
$module['module'] = 'subscriptions';
$module['install_name'] = 'User subscriptions';
$module['install_descr'] = 'User subscriptions & notifications';
$module['version'] = '1.02';

$module['files'] = array(
	array('file', 'read', "application/modules/subscriptions/controllers/admin_subscriptions.php"),
	array('file', 'read', "application/modules/subscriptions/helpers/subscriptions_helper.php"),
	array('file', 'read', "application/modules/subscriptions/install/module.php"),
	array('file', 'read', "application/modules/subscriptions/install/permissions.php"),
	array('file', 'read', "application/modules/subscriptions/install/settings.php"),
	array('file', 'read', "application/modules/subscriptions/install/structure_deinstall.sql"),
	array('file', 'read', "application/modules/subscriptions/install/structure_install.sql"),
	array('file', 'read', "application/modules/subscriptions/models/subscriptions_install_model.php"),
	array('file', 'read', "application/modules/subscriptions/models/subscriptions_model.php"),
	array('file', 'read', "application/modules/subscriptions/models/subscriptions_types_model.php"),
	array('file', 'read', "application/modules/subscriptions/models/subscriptions_users_model.php"),
	array('file', 'read', "application/modules/subscriptions/views/admin/edit_form.tpl"),
	array('file', 'read', "application/modules/subscriptions/views/admin/list.tpl"),
	array('file', 'read', "application/modules/subscriptions/views/admin/start_subscribe.tpl"),
	array('file', 'read', "application/modules/subscriptions/views/default/helper_form_profile.tpl"),
	array('file', 'read', "application/modules/subscriptions/views/default/helper_form_register.tpl"),
	array('file', 'read', "application/modules/subscriptions/views/default/user_subscription_list.tpl"),
	
	array('dir', 'read', 'application/modules/subscriptions/langs'),
);

$module['dependencies'] = array(
	'start'			=> array('version'=>'1.01'),
	'menu'			=> array('version'=>'1.01'),
	'notifications' => array('version'=>'1.01'),
	'users'			=> array('version'=>'1.01'),
	'cronjob'		=> array('version'=>'1.01')
);

$module['linked_modules'] = array(
	'install'	=> array(
		'menu'		=> 'install_menu',
		'cronjob'	=> 'install_cronjob'
	),
	'deinstall' => array(
		'menu'		=> 'deinstall_menu',
		'cronjob'	=> 'deinstall_cronjob'
	)
);
