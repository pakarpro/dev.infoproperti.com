<?php
$module['module'] = 'users_connections';
$module['install_name'] = 'User connections';
$module['install_descr'] = 'User authentication through social networking sites';
$module['version'] = '1.01';
$module['files'] = array(
	array('file', 'read', "application/modules/users_connections/controllers/users_connections.php"),
	array('file', 'read', "application/modules/users_connections/helpers/users_connections_helper.php"),	
	array('file', 'read', "application/modules/users_connections/install/module.php"),
	array('file', 'read', "application/modules/users_connections/install/permissions.php"),
	array('file', 'read', "application/modules/users_connections/install/settings.php"),
	array('file', 'read', "application/modules/users_connections/install/structure_deinstall.sql"),
	array('file', 'read', "application/modules/users_connections/install/structure_install.sql"),
	array('file', 'read', "application/modules/users_connections/models/users_connections_model.php"),
	array('file', 'read', "application/modules/users_connections/models/users_connections_install_model.php"),
	array('file', 'read', "application/modules/users_connections/views/default/oauth_link.tpl"),
	array('file', 'read', "application/modules/users_connections/views/default/oauth_login.tpl"),
	array('file', 'read', "application/modules/users_connections/views/default/oauth_usertype.tpl"),
	array('dir', 'read', 'application/modules/users_connections/langs'),
);

$module['dependencies'] = array(
	'start' => array('version'=>'1.01'),
	'menu' => array('version'=>'1.01'),
	'social_networking' => array('version'=>'1.02'),
	'users' => array('version'=>'1.02')
);

$module['linked_modules'] = array(
	'install' => array(
		'menu'	=> 'install_menu'
	),
	'deinstall' => array(
		'menu'	=> 'deinstall_menu'
	)
);
