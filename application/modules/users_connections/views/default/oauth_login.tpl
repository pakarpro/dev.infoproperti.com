{if count($services) > 0}
<div class="line top">
<span class="btn-text">{l i='you_can_auth' gid='users'}</span>
{foreach item=item from=$services}
	<a href="{$site_url}users_connections/oauth_login/{$item.id}" class="btn-link"><ins class="with-icon i-{$item.gid} link-margin"></ins></a>
{/foreach}
</div>
{/if}