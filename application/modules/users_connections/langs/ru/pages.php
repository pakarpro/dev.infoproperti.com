<?php

$install_lang["account_link_to"] = "Ваш профиль подключен к";
$install_lang["account_unlink_from"] = "Ваш профиль отключен от";
$install_lang["first_connect_via"] = "Это ваше первое подключение через";
$install_lang["please_set_email"] = "Пожалуйста, заполните email и пароль, чтобы иметь возможность входить на сайт с этими данными напрямую";
$install_lang["select_usertype"] = "Пожалуйста, выберите тип пользователя";
$install_lang["you_can_auth"] = "Или войти через";
$install_lang["you_can_link"] = "Вы можете привязать ваш профиль к следующим сервисам:";
$install_lang["you_can_unlink"] = "Вы можете отсоединить ваш профиль от следующих сервисов:";

