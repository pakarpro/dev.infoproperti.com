<?php

$install_lang["account_link_to"] = "You account is linked to";
$install_lang["account_unlink_from"] = "You account is unlinked from";
$install_lang["first_connect_via"] = "This is your first connection wiht the help of";
$install_lang["please_set_email"] = "Please fill in your email and password to sign in to the site directly with this information";
$install_lang["select_usertype"] = "Please choose your type of user";
$install_lang["you_can_auth"] = "Or sign in with";
$install_lang["you_can_link"] = "You can connect using";
$install_lang["you_can_unlink"] = "You can unlink your account from following services:";

