<?php
$module['module'] = 'sejutarumah';
$module['install_name'] = 'Sejuta Rumah';
$module['install_descr'] = 'Program Sejuta Rumah';
$module['version'] = '1.02';
$module['files'] = array(
	array('file', 'read', "application/modules/sejutarumah/controllers/sejutarumah.php"),
);
$module['dependencies'] = array(
	'start' => array('version'=>'1.01'),
	'menu' => array('version'=>'1.01'),
	'notifications' => array('version'=>'1.02')
);
$module['linked_modules'] = array(
	'install' => array(
		'menu'				=> 'install_menu',
		'banners'			=> 'install_banners',
		'notifications'		=> 'install_notifications',
		'site_map'			=> 'install_site_map',
		'social_networking' => 'install_social_networking'
	),

	'deinstall' => array(
		'menu'				=> 'deinstall_menu',
		'banners'			=> 'deinstall_banners',
		'notifications'		=> 'deinstall_notifications',
		'site_map'			=> 'deinstall_site_map',
		'social_networking' => 'deinstall_social_networking'
	)
);
