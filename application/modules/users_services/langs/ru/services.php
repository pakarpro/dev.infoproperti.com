<?php

$install_lang["agent_combined_service"] = "Пакеты услуг (для агентов)";
$install_lang["agent_combined_template"] = "Пакеты услуг (для агентов)";
$install_lang["agent_combined_template_contact_count"] = "Количество контактов";
$install_lang["agent_combined_template_period"] = "Период активности публикации (в днях)";
$install_lang["agent_combined_template_post_count"] = "Количество публикаций";
$install_lang["agent_combined_template_service_period"] = "Период активности услуги (в днях)";
$install_lang["agent_contact_service"] = "Связаться с пользователем (для агентов)";
$install_lang["agent_contact_template"] = "Связаться с пользователем (для агентов)";
$install_lang["agent_contact_template_contact_count"] = "Количество контактов";
$install_lang["agent_contact_template_service_period"] = "Период активности услуги (в днях)";
$install_lang["agent_featured_services"] = "Рекомендуемые агенты";
$install_lang["agent_featured_template"] = "Рекомендуемые агенты";
$install_lang["agent_featured_template_id_user"] = "Пользователь";
$install_lang["agent_featured_template_period"] = "Период услуги (в днях)";
$install_lang["agent_post_service"] = "Публикация объявлений (для агентов)";
$install_lang["agent_post_template"] = "Публикация объявлений (для агентов)";
$install_lang["agent_post_template_period"] = "Период активности публикации (в днях)";
$install_lang["agent_post_template_post_count"] = "Количество публикаций";
$install_lang["agent_show_logo_services"] = "Показывать фото агента в поиске";
$install_lang["agent_show_logo_template"] = "Показывать фото агента в поиске";
$install_lang["agent_show_logo_template_id_user"] = "Пользователь";
$install_lang["agent_show_logo_template_period"] = "Период услуги (в днях)";
$install_lang["company_combined_service"] = "Пакеты услуг (для агентств)";
$install_lang["company_combined_template"] = "Пакеты услуг (для агентств)";
$install_lang["company_combined_template_contact_count"] = "Количество контактов";
$install_lang["company_combined_template_period"] = "Период активности публикации (в днях)";
$install_lang["company_combined_template_post_count"] = "Количество публикаций";
$install_lang["company_combined_template_service_period"] = "Период активности услуги (в днях)";
$install_lang["company_contact_service"] = "Связаться с пользователем (для агентств)";
$install_lang["company_contact_template"] = "Связаться с пользователем (для агентств)";
$install_lang["company_contact_template_contact_count"] = "Количество контактов";
$install_lang["company_contact_template_service_period"] = "Период активности услуги (в днях)";
$install_lang["company_featured_services"] = "Рекомендуемые агентства";
$install_lang["company_featured_template"] = "Рекомендуемые агентства";
$install_lang["company_featured_template_id_user"] = "Пользователь";
$install_lang["company_featured_template_period"] = "Период услуги (в днях)";
$install_lang["company_post_service"] = "Публикация объявлений (для агентств)";
$install_lang["company_post_template"] = "Публикация объявлений (для агентств)";
$install_lang["company_post_template_period"] = "Период активности публикации (в днях)";
$install_lang["company_post_template_post_count"] = "Количество публикаций";
$install_lang["company_show_logo_services"] = "Показывать логотип агентства в поиске";
$install_lang["company_show_logo_template"] = "Показывать логотип агентства в поиске";
$install_lang["company_show_logo_template_id_user"] = "Пользователь";
$install_lang["company_show_logo_template_period"] = "Период услуги (в днях)";
$install_lang["private_combined_service"] = "Пакеты услуг (для частных лиц)";
$install_lang["private_combined_template"] = "Пакеты услуг (для частных лиц)";
$install_lang["private_combined_template_contact_count"] = "Количество контактов";
$install_lang["private_combined_template_period"] = "Период активности публикации (в днях)";
$install_lang["private_combined_template_post_count"] = "Количество публикаций";
$install_lang["private_combined_template_service_period"] = "Период активности услуги (в днях)";
$install_lang["private_contact_service"] = "Связаться с пользователем (для частных лиц)";
$install_lang["private_contact_template"] = "Связаться с пользователем (для частных лиц)";
$install_lang["private_contact_template_contact_count"] = "Количество контактов";
$install_lang["private_contact_template_service_period"] = "Период активности услуги (в днях)";
$install_lang["private_post_service"] = "Публикация объявлений (для частных лиц)";
$install_lang["private_post_template"] = "Публикация объявлений (для частных лиц)";
$install_lang["private_post_template_period"] = "Период активности публикации (в днях)";
$install_lang["private_post_template_post_count"] = "Количество публикаций";

