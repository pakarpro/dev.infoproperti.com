<?php

$install_lang["notification_user_package_enabled"] = "Платная услуга (пакет) включена";
$install_lang["tpl_user_package_enabled_subject"] = "[domain] | Платная услуга включена";
$install_lang["tpl_user_package_enabled_content"] = "Здравствуйте, [user],\n\nПлатная услуга [name] была включена.\n\nС уважением,\n[name_from]";

$install_lang["notification_user_service_enabled"] = "Платная услуга включена";
$install_lang["tpl_user_service_enabled_subject"] = "[domain] | Платная услуга включена";
$install_lang["tpl_user_service_enabled_content"] = "Здравствуйте, [user],\n\nПлатная услуга [name] была включена и теперь активна до [date].\n\nС уважением,\n[name_from]";

$install_lang["notification_user_service_expired"] = "Срок действия платной услуги истек";
$install_lang["tpl_user_service_expired_subject"] = "[domain] | Срок действия платной услуги истек";
$install_lang["tpl_user_service_expired_content"] = "Здравствуйте, [user],\n\nСрок действия платной услуги [name] истек. Хотите ли Вы продлить период ее активности?\n\nС уважением,\n[name_from]";
