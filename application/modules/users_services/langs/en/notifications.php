<?php

$install_lang["notification_user_package_enabled"] = "Paid service (package) enabled";
$install_lang["tpl_user_package_enabled_subject"] = "[domain] | Paid service enabled";
$install_lang["tpl_user_package_enabled_content"] = "Hello [user],\n\nPaid service [name] is enabled.\n\nBest regards,\n[name_from]";

$install_lang["notification_user_service_enabled"] = "Paid service enabled";
$install_lang["tpl_user_service_enabled_subject"] = "[domain] | Paid service enabled";
$install_lang["tpl_user_service_enabled_content"] = "Hello [user],\n\nPaid service [name] is enabled and active till [date].\n\nBest regards,\n[name_from]";

$install_lang["notification_user_service_expired"] = "Paid service expired";
$install_lang["tpl_user_service_expired_subject"] = "[domain] | Paid service expired";
$install_lang["tpl_user_service_expired_content"] = "Hello [user],\n\nPaid service [name] has expired. Would you like to extend its activity period?\n\nBest regards,\n[name_from]";
