<div class="content-block load_content">
	<h1>{l i='header_get_user_contacts' gid='users_services'}</h1>

	<div class="inside">
	{if $block_data.user_services}
		{l i='contact_service_spend_text' gid='users_services'}<br><br>
		<form method="POST" action="" id="ability_form">
		<input type="hidden" name='id_contact' value='{$block_data.id_contact}'>
		<select name="id_user_service">
		{foreach item=item from=$block_data.user_services}
		{assign var='service_name' value=$item.service_name}
		<option value="{$item.id}">{l i="service_string_name_$service_name" gid='users_services'} ({l i='service_contacts_left' gid='users_services'}: {$item.contact_count})</option>
		{/foreach}
		</select><br><br>
		<input type="button" name="btn_activate" id="ability_form_submit" value="{l i='btn_activate' gid='start' type='button'}">
		</form>
	{else}
		{l i='contact_service_buy_text' gid='users_services'}<br><br>
		<a href="{$site_url}users_services/index/" target="blank">{l i='contact_service_link_buy' gid='users_services'}</a>
	{/if}

	</div>
	<div class="clr"></div>
</div>
