<div class="content-block">
	<h2>{l i='featured_user_services' gid='users_services'}</h2>
	<p class="header-comment">
	{if $is_user_featured.is_featured}
		{l i='your_featured_period_till' gid='users_services'}: {$is_user_featured.featured_end_date|date_format:$page_data.date_format}<br />
		{l i='add_more_featured_period' gid='users_services'}<br />
	{else}
		{l i='create_featured_period' gid='users_services'}
	{/if}
	</p>
	<div class="r">
		<div class="b"><input type="button" class='btn' value="{l i='btn_apply' gid='start' type='button'}" onclick="document.location.href='{$site_url}users_services/apply_service/{$user_id}/agent_featured_services'"></div>
	</div>
</div>

