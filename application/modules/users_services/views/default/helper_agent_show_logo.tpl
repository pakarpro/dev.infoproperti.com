<div class="content-block">
	<h2>{l i='show_logo_services' gid='users_services'}</h2>
	<p class="header-comment">
	{if $is_show_logo.is_show}
		{l i='your_show_logo_period_till' gid='users_services'}: {$is_show_logo.show_end_date|date_format:$page_data.date_format}<br />
		{l i='add_more_show_logo_period' gid='users_services'}<br />
	{else}
		{l i='create_show_logo_period' gid='users_services'}
	{/if}
	</p>
	<div class="r">
		<div class="b"><input type="button" class='btn' value="{l i='btn_apply' gid='start' type='button'}" onclick="document.location.href='{$site_url}users_services/apply_service/{$user_id}/agent_show_logo_services'"></div>
	</div>
</div>
