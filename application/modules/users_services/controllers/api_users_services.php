<?php
/**
* Users services api controller
*
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Alexander Batukhtin <abatukhtin@pilotgroup.net>
**/
Class Api_Users_services extends Controller
{

	/**
	 * Constructor
	 */
	function __construct()
	{
		parent::Controller();
		$this->load->model('Users_services_model');
	}

	/**
	 * Services list
	 *
	 * @param type $filter contact|post|combined
	 */
	public function list_services() {

		$filter = (string)$this->input->post('filter', true);
		$user_type = $this->session->userdata('user_type');
		$data['usertype'] = $user_type;

		$available_filter = array('contact', 'post', 'combined');
		if(!in_array($filter, $available_filter)) {
			$filter = '';
		}

		$this->load->model('Services_model');
		/* Contact services */
		if (!$filter || $filter == 'contact'){
			$service_template = $user_type.'_contact_template';
			$contact_services = $this->Services_model->get_service_list(array('where' => array('template_gid' => $service_template, 'status' => 1)));
			$data['contact_services'] = $contact_services;
		}

		/* Post services */
		if (!$filter || $filter == 'post'){
			$service_template = $user_type.'_post_template';
			$post_services = $this->Services_model->get_service_list(array('where' => array('template_gid' => $service_template, 'status' => 1)));
			$data['post_services'] = $post_services;
		}

		/* Combined services */
		if (!$filter || $filter == 'combined'){
			$service_template = $user_type.'_combined_template';
			$combined_services = $this->Services_model->get_service_list(array('where' => array('template_gid' => $service_template, 'status' => 1)));
			$data['combined_services'] = $combined_services;
		}

		// seo
		/*$user_id = $this->session->userdata('user_id');
		$data = $this->Users_model->get_user_by_id($user_id);
		$this->pg_seo->set_seo_data($data);
		$this->session->set_userdata(array('service_redirect' => 'payments/statistic'));*/

		$this->set_api_content('data', $data);
	}

	/**
	 * My services
	 *
	 */
	public function my() {
		$user_id = $this->session->userdata('user_id');
		$user_type = $this->session->userdata('user_type');
		$data['usertype'] = $user_type;
		$order['id'] = 'DESC';

		$params = array();
		$params['where']['id_user'] = $user_id;
		$params['where']['service_name'] = 'post';
		$params['where_sql'][] = "(contact_status='1' OR post_status='1')";
		$current_services['post'] = $this->Users_services_model->get_services_list($order, $params);

		$params['where']['service_name'] = 'combined';
		$current_services['combined'] = $this->Users_services_model->get_services_list($order, $params);

		$params['where']['service_name'] = 'contact';
		$current_services['contact'] = $this->Users_services_model->get_services_list($order, $params);

		$data['current_services'] = $current_services;

		$this->config->load('date_formats', true);
		$data['date_format'] = $this->config->item('st_format_date_time_literal', 'date_formats');

		// seo
		/*$this->load->model('users/models/Users_model');
		$data = $this->Users_model->get_user_by_id($user_id);
		$this->pg_seo->set_seo_data($data);*/
		$this->set_api_content('data', $data);
	}

}
