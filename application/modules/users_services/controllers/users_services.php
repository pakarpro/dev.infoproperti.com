<?php
/**
* Users services user side controller
*
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Alexander Batukhtin <abatukhtin@pilotgroup.net>
* @version $Revision: 1 $ $Date: 2012-09-19 15:28:07 +0300 (Ср, 19 сент 2012) $ $Author: abatukhtin $
**/

Class Users_services extends Controller{

	/**
	 * Constructor
	 */
	function __construct(){
		parent::Controller();
		$this->load->model('Users_services_model');
	}

	public function index($filter = '') {

		$user_id = $this->session->userdata('user_id');

		$user_type = $this->session->userdata('user_type');
		$this->template_lite->assign('usertype', $user_type);

		$available_filter = array('contact', 'post', 'combined');
		$filter = (in_array($filter, $available_filter) ? $filter : '');

		$this->load->model('Services_model');

		/* Contact services */
		if (!$filter || $filter == 'contact'){
			$service_template = $user_type.'_contact_template';
			$contact_services = $this->Services_model->get_service_list(array('where' => array('template_gid' => $service_template, 'status' => 1)));
			$this->template_lite->assign('contact_services', $contact_services);
		}

		/* Post services */
		if (!$filter || $filter == 'post'){
			$service_template = $user_type.'_post_template';
			$post_services = $this->Services_model->get_service_list(array('where' => array('template_gid' => $service_template, 'status' => 1)));
			$this->template_lite->assign('post_services', $post_services);
			
			$service_template = $user_type.'_combined_template';
			$combined_services = $this->Services_model->get_service_list(array('where' => array('template_gid' => $service_template, 'status' => 1)));
			$this->template_lite->assign('combined_services', $combined_services);
		}

		/* Combined services */
		if (!$filter || $filter == 'combined'){
			$service_template = $user_type.'_combined_template';
			$combined_services = $this->Services_model->get_service_list(array('where' => array('template_gid' => $service_template, 'status' => 1)));
			$this->template_lite->assign('combined_services', $combined_services);
		}
		
		$this->load->model('payments/models/Payment_currency_model');
		$base_currency = $this->Payment_currency_model->get_currency_default(true);
		$this->template_lite->assign('base_currency', $base_currency);

		// breadcrumbs
		$this->load->model("Menu_model");
		$this->Menu_model->breadcrumbs_set_active(l("header_services", "users"));
		
		// seo
		$this->load->model('Users_model');
		$data = $this->Users_model->get_user_by_id($user_id);
		$this->pg_seo->set_seo_data($data);
		$this->session->set_userdata(array('service_redirect' => 'payments/statistic'));
		$this->template_lite->view('services');
	}

	/**
	 * Render paid services of user
	 */
	public function my_services(){
		$user_id = $this->session->userdata("user_id");

		$user_type = $this->session->userdata("user_type");
		$this->template_lite->assign("usertype", $user_type);

		$order["id"] = "DESC";

		$params = array();
		$params["where"]["id_user"] = $user_id;
		
		$params["where"]["service_name"] = "post";
		$params["where_sql"] = array("(post_status='1')");
		$current_services["post"] = $this->Users_services_model->get_services_list($order, $params);

		$params["where"]["service_name"] = "combined";
		$params["where_sql"] = array("(contact_status='1' OR post_status='1')");
		$current_services["combined"] = $this->Users_services_model->get_services_list($order, $params);

		$params["where"]["service_name"] = "contact";
		$params["where_sql"] = array("(contact_status='1')");
		$current_services["contact"] = $this->Users_services_model->get_services_list($order, $params);

		$this->template_lite->assign("current_services", $current_services);

		$this->config->load("date_formats", TRUE);
		$page_data["date_format"] = $this->config->item("st_format_date_time_literal", "date_formats");
		$this->template_lite->assign("page_data", $page_data);

		// breadcrumbs
		$this->load->model("Menu_model");
		$this->Menu_model->breadcrumbs_set_active(l("header_services", "users"));

		// seo
		$this->load->model("Users_model");
		$data = $this->Users_model->get_user_by_id($user_id);
		$this->pg_seo->set_seo_data($data);

		$this->template_lite->view("my_services");
	}

	public function apply_service($id, $service_gid) {
		$id = intval($id);
		if (!$id) {
			show_404();
		}

		if ($this->session->userdata('auth_type') != 'user') {
			show_404();
		}

		$this->session->set_userdata(array('service_redirect' => 'users/account/'));

		$params = array('id_user' => $id);
		$this->load->helper('payments');
		post_location_request(site_url() . "services/form/" . $service_gid, $params);
	}

	/**
	 * The method checks the availability of contact for the user.
	 *
	 * @param int $id_contact
	 */
	public function ajax_available_contact($id_contact){
		$return = array('available' => 0, 'content' => '', 'display_login' => 0);
		$id_user = $this->session->userdata('user_id');

		if ($this->session->userdata('auth_type') != 'user') {
			$return['display_login'] = 1;
		}else{
			$this->load->model('Users_services_model');
			$return = $this->Users_services_model->available_contact_action($id_user, $id_contact);
			if($return["content_buy_block"] == true){
				$return["content"] = $this->available_contact_block($id_user, $id_contact);
			}
		}
		echo json_encode($return);
	}

	private function available_contact_block($id_user, $id_contact){

		$data["id_contact"] = $id_contact;

		$params = array();
		$params["where"]['id_user'] = $id_user;
		$params["where"]['contact_status'] = '1';
		$params["where"]['contact_count >'] = 0;

		$data['user_services'] = $this->Users_services_model->get_services_list(null, $params);
		$this->config->load('date_formats', TRUE);
		$data["date_format"] = $this->config->item('st_format_date_literal', 'date_formats');
		$data["date_time_format"] = $this->config->item('st_format_date_time_literal', 'date_formats');

		$this->template_lite->assign('block_data', $data);
		return $this->template_lite->fetch('ajax_user_package_for_contact');
	}

	private function _activate_contact($id_contact_user, $id_user_service){
		$id_user = $this->session->userdata('user_id');

		$return = array('status' => 0, 'message' => '');

		$user_service = $this->Users_services_model->get_service($id_user_service);

		if(empty($user_service) || $user_service["id_user"] != $id_user || !$user_service["contact_count"] || !$user_service["contact_status"]){
			$return['status'] = 0;
			$return['message'] = l('error_service_activating', 'users_services');
		}else{
			$this->load->model('Users_model');
			$this->Users_model->add_contact($id_user, $id_contact_user);

			$user_service["contact_count"]--;
			$this->Users_services_model->save_service($id_user_service, $user_service);

			$return['status'] = 1;
			$return['message'] = l('success_service_activating', 'users_services');
		}

		return $return;
	}

	public function ajax_activate_contact($id_contact_user, $id_user_service) {
		echo json_encode($this->_activate_contact($id_contact_user, $id_user_service));
	}

}
