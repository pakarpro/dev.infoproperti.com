function available_view(optionArr){
	this.properties = {
		siteUrl: '/',
		checkAvailableAjaxUrl: 'users_services/ajax_available_contact/',
		buyAbilityAjaxUrl: 'users_services/ajax_activate_contact/',
		buyAbilityFormId: 'ability_form',
		buyAbilitySubmitId: 'ability_form_submit',

		success_request: function(message) {},
		fail_request: function(message) {},

		windowObj: new loadingContent({loadBlockWidth: '520px'})
	}

	var _self = this;

	this.Init = function(options){
		_self.properties = $.extend(_self.properties, options);
	}

	this.check_available = function(id_user){
		$.ajax({
			url: _self.properties.siteUrl + _self.properties.checkAvailableAjaxUrl + id_user,
			type: 'POST',
			dataType : "json",
			cache: false,
			success: function(data){
				if (data.display_login == 1){
					$('html, body').animate({
						scrollTop: $("#ajax_login_link").offset().top
					}, 2000);
					$("#ajax_login_link").click();
				}else{
					if (data.available == 1){
						_self.properties.success_request('');
					} else {
						_self.properties.windowObj.show_load_block(data.content);
						_self.init_ability_form();
					}
				}
			}
		});
	}

	this.init_ability_form = function (){
		$('#' + _self.properties.buyAbilitySubmitId).click(function(){
			var id_contact = $('#' + _self.properties.buyAbilityFormId + ' input[name=id_contact]').val();
			var id_user_service = $('#' + _self.properties.buyAbilityFormId + ' select[name=id_user_service]').val();

			if(!id_contact || !id_user_service) return;

			$.ajax({
				url: _self.properties.siteUrl + _self.properties.buyAbilityAjaxUrl + id_contact + '/' + id_user_service,
				type: 'GET',
				dataType : "json",
				cache: false,
				success: function(data){
					if (data.status == 1){
						_self.properties.success_request(data.message);
					} else {
						_self.properties.fail_request(data.message);
					}
					_self.properties.windowObj.hide_load_block();
				}
			});

		});
	}

	this.set_properties = function(properties){
		_self.properties = $.extend(_self.properties, properties);
	}

	_self.Init(optionArr);
}