<?php
$module['module'] = 'users_services';
$module['install_name'] = 'User services';
$module['install_descr'] = 'User services history';
$module['version'] = '1.01';
$module['files'] = array(
	array('file', 'read', "application/modules/users_services/controllers/api_users_services.php"),
	array('file', 'read', "application/modules/users_services/controllers/users_services.php"),
	array('file', 'read', "application/modules/users_services/helpers/users_services_helper.php"),
	array('file', 'read', "application/modules/users_services/install/module.php"),
	array('file', 'read', "application/modules/users_services/install/permissions.php"),
	array('file', 'read', "application/modules/users_services/install/settings.php"),
	array('file', 'read', "application/modules/users_services/install/structure_deinstall.sql"),
	array('file', 'read', "application/modules/users_services/install/structure_install.sql"),
	array('file', 'read', "application/modules/users_services/js/available_view.js"),
	array('file', 'read', "application/modules/users_services/models/users_services_model.php"),
	array('file', 'read', "application/modules/users_services/models/users_services_model.php"),
	array('file', 'read', "application/modules/users_services/views/default/ajax_user_package_for_contact.tpl"),
	array('file', 'read', "application/modules/users_services/views/default/helper_agent_featured.tpl"),
	array('file', 'read', "application/modules/users_services/views/default/helper_agent_show_logo.tpl"),
	array('file', 'read', "application/modules/users_services/views/default/helper_company_featured.tpl"),
	array('file', 'read', "application/modules/users_services/views/default/helper_company_show_logo.tpl"),
	array('file', 'read', "application/modules/users_services/views/default/my_services.tpl"),
	array('file', 'read', "application/modules/users_services/views/default/services.tpl"),	
	array('dir', 'read', 'application/modules/users_services/langs'),
);

$module['dependencies'] = array(
	'start' 		=> array('version'=>'1.01'),
	'menu' 			=> array('version'=>'1.01'),
	'services' 		=> array('version'=>'1.02'),
	'users' 		=> array('version'=>'1.02'),
	'notifications' => array('version'=>'1.03'),
);

$module['linked_modules'] = array(
	'install' => array(
		'menu'			=> 'install_menu',
		'services'		=> 'install_services',
		'cronjob'		=> 'install_cronjob',
		'notifications' => 'install_notifications',
	),
	'deinstall' => array(
		'menu'			=> 'deinstall_menu',
		'services'		=> 'deinstall_services',
		'cronjob'		=> 'deinstall_cronjob',
		'notifications' => 'deinstall_notifications',
	)
);
