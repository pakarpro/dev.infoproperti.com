CREATE TABLE IF NOT EXISTS `[prefix]user_services` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `id_user` int(3) NOT NULL,
  `service_name` varchar(255) NOT NULL,
  `service_data` text NOT NULL,
  `post_count` int(3) NOT NULL,
  `post_period` int(3) NOT NULL,
  `contact_count` int(3) NOT NULL,
  `contact_service_end_date` datetime NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `contact_status` tinyint(1) NOT NULL,
  `post_status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user_3` (`id_user`,`date_created`,`date_modified`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

