<?php
/**
* Users payments install model
*
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Alexander Batukhtin <abatukhtin@pilotgroup.net>
* @version $Revision: 1 $ $Date: 2012-09-12 15:24:47 +0300 (Ср, 12 сент 2012) $ $Author: abatukhtin $
**/

class Users_services_install_model extends Model{
	
	/**
	 * Link to code igniter object
	 * @var object
	 */
	var $CI;

	/**
	 * Menu configuration
	 * @var array
	 */
	private $menu = array(
		"private_account_menu" => array(
			"action" => "none",
			"items" => array(
				"private_payments_item" => array(	
					"action" => "none",
					"items" => array(
						"private_my_services_item" => array("action" => "create", "link" => "users_services/my_services", "status" => 1, "sorter" => 1),	
					),
				),
			),
		),
		"company_account_menu" => array(
			"action" => "none",
			"items" => array(
				"company_payments_item" => array(
					"action" => "none",
					"items" => array(
						"company_my_services_item" => array("action" => "create", "link" => "users_services/my_services", "status" => 1, "sorter" => 1),	
					),
				),
			),
		),
		"agent_account_menu" => array(
			"action" => "none",
			"items" => array(
				"agent_payments_item" => array(
					"action" => "none",
					"items" => array(
						"agent_my_services_item" => array("action" => "create", "link" => "users_services/my_services", "status" => 1, "sorter" => 1),	
					),
				),
			),
		),		
	);
	
	/**
	 * Services configuration
	 * @var array
	 */
	private $services = array(
		"templates" => array(
			//contact service
			array(
				"gid" => "private_contact_template",
				"callback_module" => "users_services",
				"callback_model" => "Users_services_model",
				"callback_method" => "service_contact_activate",
				"callback_validate_method" => "service_contact_validate",
				"price_type" => 1,
				"data_admin" => array("contact_count"=>"int", "service_period"=>"int"),
				"data_user" => array(),
				"moveable" => 1,
			),
			array(
				"gid" => "company_contact_template",
				"callback_module" => "users_services",
				"callback_model" => "Users_services_model",
				"callback_method" => "service_contact_activate",
				"callback_validate_method" => "service_contact_validate",
				"price_type" => 1,
				"data_admin" => array("contact_count"=>"int", "service_period"=>"int"),
				"data_user" => array(),
				"moveable" => 1,
			),
			array(
				"gid" => "agent_contact_template",
				"callback_module" => "users_services",
				"callback_model" => "Users_services_model",
				"callback_method" => "service_contact_activate",
				"callback_validate_method" => "service_contact_validate",
				"price_type" => 1,
				"data_admin" => array("contact_count"=>"int", "service_period"=>"int"),
				"data_user" => array(),
				"moveable" => 1,
			),
			//post service
			array(
				"gid" => "private_post_template",
				"callback_module" => "users_services",
				"callback_model" => "Users_services_model",
				"callback_method" => "service_post_activate",
				"callback_validate_method" => "service_post_validate",
				"price_type" => 1,
				"data_admin" => array("post_count"=>"int", "period"=>"int"),
				"data_user" => array(),
				"moveable" => 1,
			),
			array(
				"gid" => "company_post_template",
				"callback_module" => "users_services",
				"callback_model" => "Users_services_model",
				"callback_method" => "service_post_activate",
				"callback_validate_method" => "service_post_validate",
				"price_type" => 1,
				"data_admin" => array("post_count"=>"int", "period"=>"int"),
				"data_user" => array(),
				"moveable" => 1,
			),
			array(
				"gid" => "agent_post_template",
				"callback_module" => "users_services",
				"callback_model" => "Users_services_model",
				"callback_method" => "service_post_activate",
				"callback_validate_method" => "service_post_validate",
				"price_type" => 1,
				"data_admin" => array("post_count"=>"int", "period"=>"int"),
				"data_user" => array(),
				"moveable" => 1,
			),
			//combined service
			array(
				"gid" => "private_combined_template",
				"callback_module" => "users_services",
				"callback_model" => "Users_services_model",
				"callback_method" => "service_combined_activate",
				"callback_validate_method" => "service_combined_validate",
				"price_type" => 1,
				"data_admin" => array("contact_count"=>"int","service_period"=>"int","post_count"=>"int","period"=>"int"),
				"data_user" => array(),
				"moveable" => 1,
			),
			array(
				"gid" => "company_combined_template",
				"callback_module" => "users_services",
				"callback_model" => "Users_services_model",
				"callback_method" => "service_combined_activate",
				"callback_validate_method" => "service_combined_validate",
				"price_type" => 1,
				"data_admin" => array("contact_count"=>"int","service_period"=>"int","post_count"=>"int","period"=>"int"),
				"data_user" => array(),
				"moveable" => 1,
			),
			array(
				"gid" => "agent_combined_template",
				"callback_module" => "users_services",
				"callback_model" => "Users_services_model",
				"callback_method" => "service_combined_activate",
				"callback_validate_method" => "service_combined_validate",
				"price_type" => 1,
				"data_admin" => array("contact_count"=>"int","service_period"=>"int","post_count"=>"int","period"=>"int"),
				"data_user" => array(),
				"moveable" => 1,
			),
			//featured service
			array(
				"gid" => "company_featured_template",
				"callback_module" => "users_services",
				"callback_model" => "Users_services_model",
				"callback_method" => "service_featured_activate",
				"callback_validate_method" => "service_featured_validate",
				"price_type" => 1,
				"data_admin" => array("period" => "int"),
				"data_user" => array("id_user" => "hidden"),
				"moveable" => 0,
			),
			array(
				"gid" => "agent_featured_template",
				"callback_module" => "users_services",
				"callback_model" => "Users_services_model",
				"callback_method" => "service_featured_activate",
				"callback_validate_method" => "service_featured_validate",
				"price_type" => 1,
				"data_admin" => array("period" => "int"),
				"data_user" => array("id_user" => "hidden"),
				"moveable" => 0,
			),
			// show logo service
			array(
				"gid" => "company_show_logo_template",
				"callback_module" => "users_services",
				"callback_model" => "Users_services_model",
				"callback_method" => "service_show_logo_activate",
				"callback_validate_method" => "service_show_logo_validate",
				"price_type" => 1,
				"data_admin" => array("period" => "int"),
				"data_user" => array("id_user" => "hidden"),
				"moveable" => 0,
			),
			array(
				"gid" => "agent_show_logo_template",
				"callback_module" => "users_services",
				"callback_model" => "Users_services_model",
				"callback_method" => "service_show_logo_activate",
				"callback_validate_method" => "service_show_logo_validate",
				"price_type" => 1,
				"data_admin" => array("period" => "int"),
				"data_user" => array("id_user" => "hidden"),
				"moveable" => 0,
			),
		),
		"services" => array(
			// contact service
			array(
				"gid" => "private_contact_service",
				"template" => "private_contact_template",
				"pay_type" => 2,
				"status" => 0,
				"price" => 1,
				"data_admin" => array("contact_count" => 10, "service_period" => 1),
			),
			array(
				"gid" => "company_contact_service",
				"template" => "company_contact_template",
				"pay_type" => 2,
				"status" => 0,
				"price" => 1,
				"data_admin" => array("contact_count" => 10, "service_period" => 1),
			),
			array(
				"gid" => "agent_contact_service",
				"template" => "agent_contact_template",
				"pay_type" => 2,
				"status" => 0,
				"price" => 1,
				"data_admin" => array("contact_count" => 10, "service_period" => 1),
			),
			// post service
			array(
				"gid" => "private_post_service",
				"template" => "private_post_template",
				"pay_type" => 2,
				"status" => 1,
				"price" => 1,
				"data_admin" => array("post_count" => 10, "period" => 1),
			),
			array(
				"gid" => "company_post_service",
				"template" => "company_post_template",
				"pay_type" => 2,
				"status" => 1,
				"price" => 1,
				"data_admin" => array("post_count" => 10, "period" => 1),
			),
			array(
				"gid" => "agent_post_service",
				"template" => "agent_post_template",
				"pay_type" => 2,
				"status" => 1,
				"price" => 1,
				"data_admin" => array("post_count" => 10, "period" => 1),
			),
			// combined service
			array(
				"gid" => "private_combined_service",
				"template" => "private_combined_template",
				"pay_type" => 2,
				"status" => 0,
				"price" => 1,
				"data_admin" => array("contact_count" => 10, "service_period" => 1, "post_count" => 10, "period" => 1),
			),
			array(
				"gid" => "company_combined_service",
				"template" => "company_combined_template",
				"pay_type" => 2,
				"status" => 0,
				"price" => 1,
				"data_admin" => array("contact_count" => 10, "service_period" => 1, "post_count" => 10, "period" => 1),
			),
			array(
				"gid" => "agent_combined_service",
				"template" => "agent_combined_template",
				"pay_type" => 2,
				"status" => 0,
				"price" => 1,
				"data_admin" => array("contact_count" => 10, "service_period" => 1, "post_count" => 10, "period" => 1),
			),
			// featured service
			array(
				"gid" => "company_featured_services",
				"template" => "company_featured_template",
				"pay_type" => 2,
				"status" => 1,
				"price" => 10,
				"data_admin" => array("period" => "30"),
			),
			array(
				"gid" => "agent_featured_services",
				"template" => "agent_featured_template",
				"pay_type" => 2,
				"status" => 1,
				"price" => 10,
				"data_admin" => array("period" => "30"),
			),
			// show logo service
			array(
				"gid" => "company_show_logo_services",
				"template" => "company_show_logo_template",
				"pay_type" => 2,
				"status" => 1,
				"price" => 10,
				"data_admin" => array("period" => "30"),
			),
			array(
				"gid" => "agent_show_logo_services",
				"template" => "agent_show_logo_template",
				"pay_type" => 2,
				"status" => 1,
				"price" => 10,
				"data_admin" => array("period" => "30"),
			),
		),
	);
	
	/**
	 * Notifications configuration
	 */
	private $notifications = array(
		"templates" => array(
			array("gid"=>"user_package_enabled", "name"=>"Users paid service (package) enabled", "vars"=>array('user', 'name'), "content_type"=>"text"),
			array("gid"=>"user_service_enabled", "name"=>"Users paid service enabled", "vars"=>array('user', 'name', 'date'), "content_type"=>"text"),
			array("gid"=>"user_service_expired", "name"=>"Users paid service expired", "vars"=>array('user', 'name'), "content_type"=>"text"),
		),
		"notifications" => array(
			array("gid"=>"user_package_enabled", "template"=>"user_package_enabled", "send_type"=>"simple"),
			array("gid"=>"user_service_enabled", "template"=>"user_service_enabled", "send_type"=>"simple"),
			array("gid"=>"user_service_expired", "template"=>"user_service_expired", "send_type"=>"simple"),
		),
	);	
	
	/**
	 * Cron job configuration
	 */
	private $cronjob = array(
		array(
			"name"=>"Update users services", 
			"module"=>"users_services", 
			"model"=>"Users_services_model",
			"method"=>"update_services",
			"cron_tab"=>"*/10 * * * *",
			"status"=>"1",
		),
		array(
			"name"=>"Users featured section clean",
			"module"=>"users_services",
			"model"=>"Users_services_model",
			"method"=>"service_featured_cron",
			"cron_tab"=>"0 * * * *",
			"status"=>"1",
		),
		array(
			"name"=>"Users show logo section clean",
			"module"=>"users_services",
			"model"=>"Users_services_model",
			"method"=>"service_show_logo_cron",
			"cron_tab"=>"0 * * * *",
			"status"=>"1",
		),
	);
	
	/**
	 * Seo configuration
	 */
	private $seo_data = array(
		"module_gid" => "users_services",
		"model_name" => "Users_services_model",
		"get_settings_method" => "get_seo_settings",
		"get_rewrite_vars_method" => "request_seo_rewrite",
		"get_sitemap_urls_method" => "get_sitemap_xml_urls",
	);
	
	/**
	 * Constructor
	 *
	 * @required Install object
	 */
	function __construct(){
		parent::Model();
		$this->CI = & get_instance();
	}

	/**
	 * Install menu
	 */
	public function install_menu(){
		
		$this->CI->load->helper("menu");

		foreach($this->menu as $gid => $menu_data){
			$this->menu[$gid]["id"] = linked_install_set_menu($gid, $menu_data["action"], $menu_data["name"]);
			linked_install_process_menu_items($this->menu, "create", $gid, 0, $this->menu[$gid]["items"]);
		}
	}
	
	/**
	 * Update languages
	 * @param array $langs_ids
	 */
	public function install_menu_lang_update($langs_ids=null){
		if(empty($langs_ids)) return false;
		$langs_file = $this->CI->Install_model->language_file_read("users_services", "menu", $langs_ids);

		if(!$langs_file){log_message("info", "Empty menu langs data"); return false;}

		$this->CI->load->helper("menu");

		foreach($this->menu as $gid => $menu_data){
			linked_install_process_menu_items($this->menu, "update", $gid, 0, $this->menu[$gid]["items"], $gid, $langs_file);
		}
		return true;
	}

	/**
	 * Export languages
	 * @param array $langs_ids
	 */
	public function install_menu_lang_export($langs_ids){
		if(empty($langs_ids)) return false;
		$this->CI->load->helper("menu");

		$return = array();
		foreach($this->menu as $gid => $menu_data){
			$temp = linked_install_process_menu_items($this->menu, "export", $gid, 0, $this->menu[$gid]["items"], $gid, $langs_ids);
			$return = array_merge($return, $temp);
		}
		return array("menu" => $return);
	}
	
	/**
	 * Uninstall menu
	 */
	public function deinstall_menu(){
		
		$this->CI->load->helper("menu");
		foreach($this->menu as $gid => $menu_data){
			if($menu_data["action"] == "create"){
				linked_install_set_menu($gid, "delete");
			}else{
				linked_install_delete_menu_items($gid, $this->menu[$gid]["items"]);
			}
		}
	}

	/**
	 * Install links to service module
	 */
	public function install_services(){
		$this->CI->load->model("Services_model");
		
		foreach((array)$this->services["templates"] as $template_data){
			$data_admin = $template_data["data_admin"];
			$data_user = $template_data["data_user"];
			
			$validate_data = $this->CI->Services_model->validate_template(null, $template_data);
			if(!empty($validate_data["errors"])) continue;
			$this->CI->Services_model->save_template(null, $validate_data["data"]);
		}	
		
		foreach((array)$this->services["services"] as $service_data){
			$service_data["template_gid"] = $service_data["template"];
			
			$validate_data = $this->CI->Services_model->validate_service(null, $service_data);
			if(!empty($validate_data["errors"])) continue;
			$this->CI->Services_model->save_service(null, $validate_data["data"]);
		}
	}

	/**
	 * Import services languages
	 * @param array $langs_ids
	 */
	public function install_services_lang_update($langs_ids=null){
		
		$langs_file = $this->CI->Install_model->language_file_read("users_services", "services", $langs_ids);
		if(!$langs_file){log_message("info", "Empty services langs data");return false;}
		
		$services_data = array(
			"service" 	=> array(),
			"template" 	=> array(),
			"param"		=> array(),
		);
		
		$this->CI->load->model("Services_model");
		
		foreach((array)$this->services["templates"] as $template_data){
			$data_admin = isset($template_data["data_admin"]) ? array_keys($template_data["data_admin"]) : array();
			$data_user = isset($template_data["data_user"]) ? array_keys($template_data["data_user"]) : array();
			$services_data["template"][] = $template_data["gid"];
			$services_data["param"][$template_data["gid"]] = array_unique(array_merge($data_admin, $data_user));
		}
		
		foreach((array)$this->services["services"] as $service_data){
			$services_data["service"][] = $service_data["gid"];
		}

		$this->CI->Services_model->update_langs($services_data, $langs_file);
		return true;
	}

	/**
	 * Export services languages
	 * @param array $langs_ids
	 */
	public function install_services_lang_export($langs_ids=null){

		$services_data = array(
			"service" 	=> array(),
			"template" 	=> array(),
			"param"		=> array(),
		);

		$this->CI->load->model("Services_model");

		foreach((array)$this->services["templates"] as $template_data){
			$data_admin = isset($template_data["data_admin"]) ? array_keys($template_data["data_admin"]) : array();
			$data_user = isset($template_data["data_user"]) ? array_keys($template_data["data_user"]) : array();			
			$services_data["template"][] = $template_data["gid"];
			$services_data["param"][$template_data["gid"]] = array_merge($data_admin, $data_user);
		}
		
		foreach((array)$this->services["services"] as $service_data){
			$services_data["service"][] = $service_data["gid"];
		}
		
		return array("services" => $this->CI->Services_model->export_langs($services_data, $langs_ids));
	}

	
	/**
	 * Uninstall links to services module
	 */
	public function deinstall_services(){
		$this->CI->load->model("Services_model");
		
		foreach((array)$this->services["templates"] as $template_data){
			$this->CI->Services_model->delete_template_by_gid($template_data["gid"]);
		}
		
		foreach((array)$this->services["services"] as $service_data){
			$this->CI->Services_model->delete_service_by_gid($service_data["gid"]);
		}
	}
	
	/**
	 * Install links to notifications module
	 */
	public function install_notifications(){
		// add notification
		$this->CI->load->model("Notifications_model");
		$this->CI->load->model("notifications/models/Templates_model");

		$templates_ids = array();

		foreach((array)$this->notifications["templates"] as $template_data){
			if(is_array($template_data["vars"])) $template_data["vars"] = implode(",", $template_data["vars"]);
			
			$validate_data = $this->CI->Templates_model->validate_template(null, $template_data);
			if(!empty($validate_data["errors"])) continue;
			$templates_ids[$template_data['gid']] = $this->CI->Templates_model->save_template(null, $validate_data["data"]);
		}

		foreach((array)$this->notifications["notifications"] as $notification_data){
			if(!isset($templates_ids[$notification_data["template"]])){
				$template = $this->CI->Templates_model->get_template_by_gid($notification_data["template"]);
				$templates_ids[$notification_data["template"]] = $template["id"];
			}
			$notification_data["id_template_default"] = $templates_ids[$notification_data["template"]];
			$validate_data = $this->CI->Notifications_model->validate_notification(null, $notification_data);
			if(!empty($validate_data["errors"])) continue;
			$this->CI->Notifications_model->save_notification(null, $validate_data["data"]);
		}
	}
	
	/**
	 * Import notifications languages
	 * @param array $langs_ids
	 */
	public function install_notifications_lang_update($langs_ids=null){
		if(empty($langs_ids)) return false;
		if(!is_array($langs_ids)) $langs_ids = (array)$langs_ids;
		
		$this->CI->load->model("Notifications_model");
		
		$langs_file = $this->CI->Install_model->language_file_read("users_services", "notifications", $langs_ids);
		if(!$langs_file){log_message("info", "Empty notifications langs data");return false;}
	
		$this->CI->Notifications_model->update_langs($this->notifications, $langs_file, $langs_ids);
		return true;
	}
	
	/**
	 * Export notifications languages
	 * @param array $langs_ids
	 */
	public function install_notifications_lang_export($langs_ids=null){
		if(!is_array($langs_ids)) $langs_ids = (array)$langs_ids;
		$this->CI->load->model("Notifications_model");
		$langs = $this->CI->Notifications_model->export_langs((array)$this->notifications, $langs_ids);
		return array("notifications" => $langs);
	}
	
	/**
	 * Uninstall links to notifications module
	 */
	public function deinstall_notifications(){
		////// add notification
		$this->CI->load->model("Notifications_model");
		$this->CI->load->model("notifications/models/Templates_model");

		foreach((array)$this->notifications["notifications"] as $notification_data){
			$this->CI->Notifications_model->delete_notification_by_gid($notification_data["gid"]);
		}
		
		foreach((array)$this->notifications["templates"] as $template_data){
			$this->CI->Templates_model->delete_template_by_gid($template_data["gid"]);
		}
	}

	/**
	 * Install links to cronjob module
	 */
	public function install_cronjob(){
		$this->CI->load->model("Cronjob_model");
		
		foreach((array)$this->cronjob as $cron_data){
			$validate_data = $this->CI->Cronjob_model->validate_cron(null, $cron_data);
			if(!empty($validate_data["errors"])) continue;
			$this->CI->Cronjob_model->save_cron(null, $validate_data["data"]);
		}
	}

	/**
	 * Uninstall links to cronjob module
	 */
	public function deinstall_cronjob(){
		$this->CI->load->model("Cronjob_model");
		$cron_data = array();
		$cron_data["where"]["module"] = "users_services";
		$this->CI->Cronjob_model->delete_cron_by_param($cron_data);
	}
	
	/**
	 * Install
	 */
	function _arbitrary_installing(){
		// SEO
		$this->CI->pg_seo->set_seo_module("users_services", (array)$this->seo_data);
	}

	/**
	 * Import module languages
	 */
	public function _arbitrary_lang_install(){
		
	}

	/**
	 * Export module languages
	 * @param array $langs_ids
	 */
	public function _arbitrary_lang_export($langs_ids=null){
		return array();
	}

	/**
	 * Uninstall
	 */
	function _arbitrary_deinstalling(){
		$this->CI->pg_seo->delete_seo_module("users_services");
	}
}
