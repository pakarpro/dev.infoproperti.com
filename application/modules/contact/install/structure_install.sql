DROP TABLE IF EXISTS `[prefix]contacts`;
CREATE TABLE IF NOT EXISTS `[prefix]contacts` (
  `id` bigint(11) NOT NULL auto_increment,
  `user_id` int(3) NOT NULL,
  `sender` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `message` text NULL,
  `status` tinyint(1) NOT NULL,
  `allow_send` tinyint(1) NOT NULL,
  `send` tinyint(1) NOT NULL,
  `date_add` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `id_user` (`user_id`),
  KEY `date_add` (`date_add`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
