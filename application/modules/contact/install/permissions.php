<?php

$_permissions["admin_contact"]["index"] = "3";
$_permissions["admin_contact"]["all"] = "3";
$_permissions["admin_contact"]["unread"] = "3";
$_permissions["admin_contact"]["need_approve"] = "3";
$_permissions["admin_contact"]["show"] = "3";
$_permissions["admin_contact"]["read"] = "3";
$_permissions["admin_contact"]["send"] = "3";
$_permissions["admin_contact"]["send_all"] = "3";
$_permissions["admin_contact"]["delete"] = "3";
$_permissions["admin_contact"]["delete_all"] = "3";
$_permissions["admin_contact"]["settings"] = "3";
$_permissions["contact"]["ajax_send_message"] = "1";
$_permissions["contact"]["ajax_check_code"] = "1";
$_permissions["api_contact"]["api_send_message"] = "1";
