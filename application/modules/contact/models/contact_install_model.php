<?php
/**
* Contact install model
* 
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/
class Contact_install_model extends Model{
	
	/**
	 * Link to CoreIgniter object
	 * @var object
	 */
	private $CI;
	
	/**
	 * Menu configuration
	 * @var array
	 */
	private $menu = array(
		"admin_menu" => array(
			"action" => "none",
			"items" => array(
				'settings_items' => array(
					'action' => 'none',
					'items' => array(
						'feedbacks-items' => array(
							'action' => 'none',
							'items' => array(
								'contact_menu_item' => array('action' => 'create', 'link' => 'admin/contact/index', 'status' => 1, 'sorter' => 2)
							)
						)
					)
				)
			),
		),
		"admin_contact_menu" => array(
			"action" => "create",
			"name" => "Admin mode - Content - Contacts",
			"items" => array(
				"contact_list_item" => array("action" => "create", "link" => "admin/contact/index", "status" => 1),
				"contact_settings_item" => array("action" => "create", "link" => "admin/contact/settings", "status" => 1),
			),
		),
	);

	/**
	 * Notifications configuration
	 */
	private $notifications = array(
		"templates" => array(
			array("gid" => "user_guest_contact", "name" => "Contact from guest", "vars" => array("user", "sender", "email", "phone", "message"), "content_type" => "text"),
			array("gid" => "auser_guest_contact", "name" => "Contact from guest (for admin)", "vars" => array(), "content_type" => "text"),
		),
		"notifications" => array(
			array("gid" => "user_guest_contact", "template" => "user_guest_contact", "send_type" => "simple"),
			array("gid" => "auser_guest_contact", "template" => "auser_guest_contact", "send_type" => "simple"),
		),
	);

	/**
	 * Ausers configuration
	 * @var array
	 */
	private $ausers = array(
		array("module" => "contact", "method" => "index", "is_default" => 1),
		array("module" => "contact", "method" => "settings", "is_default" => 0),
	);
	
	/**
	 * Moderation types
	 * @var array
	 */
	private $moderation_types = array(
		array(
			"name" => "contact",
			"mtype" => "-1",
			"module" => "contact",
			"model" => "Contact_model",
			"check_badwords" => "1",
			"method_get_list" => "",
			"method_set_status" => "",
			"method_delete_object" => "",
			"allow_to_decline" => "0",
			"template_list_row" => "",
		),
 	);
	
	/**
	 * Constructor
	 *
	 * @return Install object
	 */
	function __construct(){
		parent::Model();
		$this->CI = & get_instance();
	}

	/**
	 * Check system requirements of module
	 */
	function _validate_requirements(){
		$result = array("data"=>array(), "result" => true);

		//check for Mbstring
		$good			= function_exists("mb_convert_encoding");
		$result["data"][] = array(
			"name" => "Mbstring extension (required for feeds parsing) is installed",
			"value" => $good?"Yes":"No",
			"result" => $good,
		);
		$result["result"] = $result["result"] && $good;

		//check for iconv
		$good			= function_exists("iconv");
		$result["data"][] = array(
			"name" => "Iconv extension (required for feeds parsing) is installed",
			"value" => $good?"Yes":"No",
			"result" => $good,
		);
		$result["result"] = $result["result"] && $good;
		return $result;
	}

	/**
	 * Install links to menu module
	 */
	public function install_menu(){
		
		$this->CI->load->helper("menu");

		foreach($this->menu as $gid => $menu_data){
			$this->menu[$gid]["id"] = linked_install_set_menu($gid, $menu_data["action"], $menu_data["name"]);
			linked_install_process_menu_items($this->menu, "create", $gid, 0, $this->menu[$gid]["items"]);
		}
	}
	
	/**
	 * Update languages
	 * @param array $langs_ids
	 */
	public function install_menu_lang_update($langs_ids=null){
		if(empty($langs_ids)) return false;
		$langs_file = $this->CI->Install_model->language_file_read("contact", "menu", $langs_ids);

		if(!$langs_file){log_message("info", "Empty menu langs data"); return false;}

		$this->CI->load->helper("menu");

		foreach($this->menu as $gid => $menu_data){
			linked_install_process_menu_items($this->menu, "update", $gid, 0, $this->menu[$gid]["items"], $gid, $langs_file);
		}
		return true;
	}

	/**
	 * Export languages
	 * @param array $langs_ids
	 */
	public function install_menu_lang_export($langs_ids){
		if(empty($langs_ids)) return false;
		$this->CI->load->helper("menu");

		$return = array();
		foreach($this->menu as $gid => $menu_data){
			$temp = linked_install_process_menu_items($this->menu, "export", $gid, 0, $this->menu[$gid]["items"], $gid, $langs_ids);
			$return = array_merge($return, $temp);
		}
		return array("menu" => $return);
	}
	
	/**
	 * Uninstall menu
	 */
	public function deinstall_menu(){
		$this->CI->load->helper("menu");
		foreach($this->menu as $gid => $menu_data){
			if($menu_data["action"] == "create"){
				linked_install_set_menu($gid, "delete");
			}else{
				linked_install_delete_menu_items($gid, $this->menu[$gid]["items"]);
			}
		}	
	}
	
	/**
	 * Install links to notifications module
	 */
	public function install_notifications(){
		// add notification
		$this->CI->load->model("Notifications_model");
		$this->CI->load->model("notifications/models/Templates_model");

		$templates_ids = array();

		foreach((array)$this->notifications["templates"] as $template_data){
			if(is_array($template_data["vars"]))
				$template_data["vars"] = implode(",", $template_data["vars"]);
			
			$validate_data = $this->CI->Templates_model->validate_template(null, $template_data);
			if(!empty($validate_data["errors"])) continue;
			$templates_ids[$template_gid] = $this->CI->Templates_model->save_template(null, $validate_data["data"]);
		}

		foreach((array)$this->notifications["notifications"] as $notification_data){
			if(!isset($templates_ids[$notification_data["template"]])){
				$template = $this->CI->Templates_model->get_template_by_gid($notification_data["template"]);
				$templates_ids[$notification_data["template"]] = $template["id"];
			}
			
			$notification_data["id_template_default"] = $templates_ids[$notification_data["template"]];
			
			$validate_data = $this->CI->Notifications_model->validate_notification(null, $notification_data);
			if(!empty($validate_data["errors"])) continue;
			$this->CI->Notifications_model->save_notification(null, $validate_data["data"]);
		}
	}
	
	/**
	 * Import notifications languages
	 * @param array $langs_ids
	 */
	public function install_notifications_lang_update($langs_ids=null){
		if(empty($langs_ids)) return false;
		$this->CI->load->model("Notifications_model");
		
		$langs_file = $this->CI->Install_model->language_file_read("contact", "notifications", $langs_ids);
		if(!$langs_file){log_message("info", "Empty notifications langs data");return false;}
		
		$this->CI->Notifications_model->update_langs((array)$this->notifications, $langs_file, $langs_ids);
		return true;
	}
	
	/**
	 * Export notifications languages
	 * @param array $langs_ids
	 */
	public function install_notifications_lang_export($langs_ids=null){
		$this->CI->load->model("Notifications_model");
		$langs = $this->CI->Notifications_model->export_langs((array)$this->notifications, $langs_ids);
		return array("notifications" => $langs);
	}
	
	/**
	 * Uninstall links to notifications module
	 */
	public function deinstall_notifications(){
		////// add notification
		$this->CI->load->model("Notifications_model");
		$this->CI->load->model("notifications/models/Templates_model");

		foreach((array)$this->notifications["notifications"] as $notification_data){
			$this->CI->Notifications_model->delete_notification_by_gid($notification_data["gid"]);
		}
		
		foreach((array)$this->notifications["templates"] as $template_data){
			$this->CI->Templates_model->delete_template_by_gid($template_data["gid"]);
		}
	}
	
	/**
	 * Install links to moderation module
	 */
	public function install_moderation(){
		$this->CI->load->model('moderation/models/Moderation_type_model');
		foreach($this->moderation_types as $mtype) {
			$mtype['date_add'] = date("Y-m-d H:i:s");
			$this->CI->Moderation_type_model->save_type(null, $mtype);
		}
	}
	
	/**
	 * Import moderation languages
	 * @param array $langs_ids
	 */
	public function install_moderation_lang_update($langs_ids=null){
		if(!is_array($langs_ids)) $langs_ids = (array)$langs_ids;
		$langs_file = $this->CI->Install_model->language_file_read('contact', 'moderation', $langs_ids);

		if(!$langs_file){log_message('info', 'Empty moderation langs data'); return false;}

		$this->CI->load->model('moderation/models/Moderation_type_model');
		$this->CI->Moderation_type_model->update_langs($this->moderation_types, $langs_file);
	}

	/**
	 * Export languages
	 * @param array $langs_ids
	 */
	public function install_moderation_lang_export($langs_ids=null){
		if(!is_array($langs_ids)) $langs_ids = (array)$langs_ids;
		$this->CI->load->model('moderation/models/Moderation_type_model');
		return array('moderation' => $this->CI->Moderation_type_model->export_langs($this->moderation_types, $langs_ids));
	}

	
	/**
	 * Uninstall links to moderation module
	 */
	public function deinstall_moderation(){
		$this->CI->load->model('moderation/models/Moderation_type_model');
		foreach($this->moderation_types as $mtype) {
			$type = $this->CI->Moderation_type_model->get_type_by_name($mtype["name"]);
			$this->CI->Moderation_type_model->delete_type($type['id']);
		}
	}
	
	/**
	 * Install links to ausers module
	 */
	public function install_ausers(){
		///// install ausers permissions
		$this->CI->load->model("Ausers_model");

		foreach((array)$this->ausers as $method_data){
			//$validate_data = $this->CI->Ausers_model->validate_method($method_data, true);
			$validate_data = array("errors"=>array(), "data"=>$method_data);
			if(!empty($validate_data["errors"])) continue;
			$this->CI->Ausers_model->save_method(null, $validate_data["data"]);
		}
	}
	
	/**
	 * Import moderators languages
	 * @param array $langs_ids
	 */
	public function install_ausers_lang_update($langs_ids=null){
		
		$langs_file = $this->CI->Install_model->language_file_read("contact", "ausers", $langs_ids);
		if(!$langs_file){log_message("info", "Empty ausers langs data");return false;}
		
		// install ausers permissions
		$this->CI->load->model("Ausers_model");
		$params["where"]["module"] = "contact";
		$methods = $this->CI->Ausers_model->get_methods_lang_export($params);

		foreach($methods as $method){
			if(!empty($langs_file[$method["method"]])){
				$this->CI->Ausers_model->save_method($method["id"], array(), $langs_file[$method["method"]]);
			}
		}
	}

	/**
	 * Export moderators languages
	 * @param array $langs_ids
	 */
	public function install_ausers_lang_export($langs_ids){
		$this->CI->load->model("Ausers_model");
		$params["where"]["module"] = "contact";
		$methods = $this->CI->Ausers_model->get_methods_lang_export($params, $langs_ids);
		foreach($methods as $method){
			$return[$method["method"]] = $method["langs"];
		}
		return array('ausers' => $return);
	}
	
	/**
	 * Uninstall links to ausers module
	 */
	public function deinstall_ausers(){
		$this->CI->load->model("Ausers_model");
		$params = array();
		$params["where"]["module"] = "contact";
		$this->CI->Ausers_model->delete_methods($params);
	}
	
	/**
	 * Install model
	 */
	function _arbitrary_installing(){
		//get administrator email
		if($this->pg_module->is_module_installed('ausers')){
			$this->CI->load->model("Ausers_model");
			$users = $this->CI->Ausers_model->get_users_list(null, 1, null, array('where'=>array('user_type'=>'admin')));
			if(!empty($users)){ 
				$this->CI->pg_module->set_module_config("contact", 'contact_send_mail', 1);
				$this->CI->pg_module->set_module_config("contact", 'contact_admin_email', $users[0]["email"]);
			}
		}
	}	
}
