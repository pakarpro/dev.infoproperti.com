<?php
/**
* Contact Model
* 
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/

define("CONTACTS_TABLE", DB_PREFIX."contacts");

class Contact_model extends Model{
	
	/**
	 * link to CodeIgniter object
	 * @var object
	 */
	private $CI;

	/**
	 * link to DataBase object
	 * @var object
	 */
	private $DB;

	/**
	 * Table fields
	 * @var array
	 */
	var $fields = array(
		"id",
		"user_id",
		"sender",
		"email",
		"phone",
		"message",
		"status",
		"allow_send",
		"send",
		"date_add",
	);
	
	/**
	 * Format settings
	 * @var array
	 */
	private $format_settings = array(
		"use_format" => true,
		"get_user" => true,
	);

	/**
	 * Moderation type
	 * @var string
	 */
	private $moderation_type = "contact";
	
	/**
	 * Constructor
	 *
	 * return Contact object
	 */
	function __construct()
	{
		parent::Model();
		$this->CI = & get_instance();
		$this->DB = &$this->CI->db;
	}

	/**
	 * Get contact by ID
	 * @param string $id
	 * @return array
	 */
	public function get_contact_by_id($id, $formatted=false){

		if(!$id) return false;

		$this->DB->select(implode(", ", $this->fields));
		$this->DB->from(CONTACTS_TABLE);
		$this->DB->where("id", $id);
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			$data = array($results[0]);
			if($formatted){
				$data = $this->format_contact($data);
			}
			return $data[0];
		}
		return array();
	}
	
	/**
	 * Save contact
	 * @param array $data
	 * @return boolean
	 */
	public function save_contact($id, $data){
		if(!$id){
			if (!isset($data["email"])){
				$data["email"] = "";
			}
			if (!isset($data["sender"])){
				$data["sender"] = "";
			}
			$data["date_add"] = date("Y-m-d H:i:s");
			$this->DB->insert(CONTACTS_TABLE, $data);
			$id = $this->DB->insert_id();
		}else{
			$this->DB->where("id", $id);
			$this->DB->update(CONTACTS_TABLE, $data);
		}
		return $id;
	}
	
	/**
	 * Remove contact
	 * @param integer $id
	 * @return boolean
	 */
	public function delete_contact($id){
		$this->DB->where("id", $id);
		$this->DB->delete(CONTACTS_TABLE);
		return true;
	}
		
	/**
	 * Return contacts as array
	 * @param integer $page
	 * @param string $limits 
	 * @param array $order_by
	 * @param array $params
	 * @param boolean $formatted
	 * @return array
	 */
	private function _get_contacts_list($page=null, $limits=null, $order_by=null, $params=array(), $formatted=true){
		$this->DB->select(implode(", ", $this->fields));
		$this->DB->from(CONTACTS_TABLE);
		
		if(isset($params["where"]) && is_array($params["where"]) && count($params["where"])){
			foreach($params["where"] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])){
			foreach($params["where_in"] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])){
			foreach($params["where_sql"] as $value){
				$this->DB->where($value);
			}
		}

		if(is_array($order_by) && count($order_by) > 0){
			foreach($order_by as $field => $dir){
				if(in_array($field, $this->fields)){
					$this->DB->order_by($field . " " . $dir);
				}
			}
		}elseif($order_by){
			$this->DB->order_by($order_by);
		}

		if(!is_null($page)){
			$page = intval($page)?intval($page):1;
			$this->DB->limit($limits, $limits*($page-1));
		}

		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			foreach($results as $r) $data[] = $r;
			if($formatted) $data = $this->format_contact($data);
			return $data;
		}
		return array();
	}
	
	/**
	 * Return number of contacts
	 * @param array $params
	 * @return integer
	 */
	private function _get_contacts_count($params=null){
		$this->DB->select("COUNT(*) AS cnt");
		$this->DB->from(CONTACTS_TABLE);

		if(isset($params["where"]) && is_array($params["where"]) && count($params["where"])){
			foreach($params["where"] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])){
			foreach($params["where_in"] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])){
			foreach($params["where_sql"] as $value){
				$this->DB->where($value);
			}
		}

		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			return intval($results[0]["cnt"]);
		}
		return 0;
	}
	
	/**
	 * Return all contacts as array
	 * @param integer $page
	 * @param string $limits 
	 * @param array $order_by
	 * @return array
	 */
	public function get_all_contacts_list($page=null, $limits=null, $order_by=null){
		return $this->_get_contacts_list($page, $limits, $order_by);
	}
	
	/**
	 * Return number of all contacts
	 * @return integer
	 */
	public function get_all_contacts_count(){
		return $this->_get_contacts_count();
	}
	
	/**
	 * Return unread contacts as array
	 * @param integer $page
	 * @param string $limits 
	 * @param array $order_by
	 * @return array
	 */
	public function get_unread_contacts_list($page=null, $limits=null, $order_by=null){
		$params = array("where"=>array("status"=>"0"));
		return $this->_get_contacts_list($page, $limits, $order_by, $params);
	}
	
	/**
	 * Return number of unread contacts
	 * @return integer
	 */
	public function get_unread_contacts_count(){
		$params = array("where"=>array("status"=>"0"));
		return $this->_get_contacts_count($params);
	}
	
	/**
	 * Return need approve contacts as array
	 * @param integer $page
	 * @param string $limits 
	 * @param array $order_by
	 * @return array
	 */
	public function get_need_approve_contacts_list($page=null, $limits=null, $order_by=null){
		$params = array("where"=>array("allow_send"=>"1", "send"=>"0"));
		return $this->_get_contacts_list($page, $limits, $order_by, $params);
	}
	
	/**
	 * Return number of need approve contacts
	 * @return integer
	 */
	public function get_need_approve_contacts_count(){
		$params = array("where"=>array("allow_send"=>"1", "send"=>"0"));
		return $this->_get_contacts_count($params);
	}
	
	/**
	 * Validate contact
	 * @param array $data
	 * @param boolean $required
	 * @return array
	 */
	public function validate_contact($id, $data){
		$return = array("errors"=>array(), "data"=>array());

		// id
		if(isset($data["id"])){
			$return["data"]["id"] = intval($data["id"]);
		}

		// user
		if(isset($data["user_id"])){
			$return["data"]["user_id"] = intval($data["user_id"]);
			if(empty($return["data"]["user_id"])) $return["errors"][] = l("error_empty_user", "contact");
		}elseif(!$id){
			$return["errors"][] = l("error_empty_user", "contact");
		}
	
		// sender
		if(isset($data["sender"])){
			$return["data"]["sender"] = trim(strip_tags($data["sender"]));
			if(empty($return["data"]["sender"])) $return["errors"][] = l("error_empty_sender", "contact");
		}elseif(!$id){
			$return["errors"][] = l("error_empty_sender", "contact");
		}
		
		// email
		if(isset($data["email"])){
			$return["data"]["email"] = trim(strip_tags($data["email"]));
			if(empty($return["data"]["email"])){
				$return["errors"][] = l("error_empty_email", "contact");
			}else{
				$this->CI->config->load("reg_exps", TRUE);
				$email_expr = $this->CI->config->item("email", "reg_exps");
				if(!preg_match($email_expr, $return["data"]["email"])){
					$return["errors"][] = l("error_email_incorrect", "contact");
				}
			}
		}elseif(!$id){
			$return["errors"][] = l("error_empty_email", "contact");
		}
		
		// phone
		if(isset($data["phone"])){
			$return["data"]["phone"] = trim(strip_tags($data["phone"]));
			if(empty($return["data"]["phone"])){
				$return["errors"][] = l("error_empty_phone", "contact");
			}else{
				$this->CI->load->helper("start");
				$phone_format = get_phone_format('regexp');
				if($phone_format !== false && !preg_match($phone_format, $return["data"]["phone"])){
					$return["errors"][] = l("error_phone_incorrect", "users");
				}
			}
		}elseif(!$id){
			$return["errors"][] = l("error_empty_phone", "contact");
		}
		
		// message
		if(isset($data["message"])){
			$return["data"]["message"] = trim(strip_tags($data["message"]));
			if(empty($return["data"]["message"])){
				$return["errors"][] = l("error_empty_message", "contact");
			}else{
				$this->CI->load->model("moderation/models/Moderation_badwords_model");
				$bw_count = $this->CI->Moderation_badwords_model->check_badwords($this->moderation_type, $return["data"]["message"]);
				if($bw_count){
					$return["errors"][] = l("error_badwords_message", "contact");
				}
			}
		}elseif(!$id){
			$return["errors"][] = l("error_empty_message", "contact");
		}
		
		// status
		if(isset($data["status"])){
			$return["data"]["status"] = $data["status"] ? 1 : 0;
		}
		
		// allow send
		if(isset($data["allow_send"])){
			$return["data"]["allow_send"] = $data["allow_send"] ? 1 : 0;
		}
		
		// send
		if(isset($data["send"])){
			$return["data"]["send"] = $data["send"] ? 1 : 0;
		}
		
		return $return;
	}
	
	/**
	 * Validate contact settings
	 * @param array $data
	 * @return array
	 */
	public function validate_contact_settings($data){
		$return = array("errors"=> array(), "data" => array());

		$return["data"]["contact_make_copies"] = $data["contact_make_copies"] ? 1: 0;
		$return["data"]["contact_need_approve"] = $data["contact_need_approve"] ? 1 : 0;
		$return["data"]["contact_send_mail"] = $data["contact_send_mail"] ? 1 : 0;
		
		
		/// email
		if(isset($data['contact_admin_email'])){
			$return["data"]["contact_admin_email"] = trim(strip_tags($data["contact_admin_email"]));

			if(!empty($return["data"]["contact_admin_email"])){
				$this->CI->config->load("reg_exps", TRUE);
				$email_expr = $this->CI->config->item("email", "reg_exps");
				if(!preg_match($email_expr, $return["data"]["contact_admin_email"])){
					$return["errors"][] = l("error_invalid_email", "contact");
				}
			}elseif($return["data"]["contact_send_mail"]){
				$return["errors"][] = l("error_empty_email", "contact");
			}
		}

		return $return;
	}
	
	/**
	 * Mark contact as read
	 * @param integer $id
	 * @return void
	 */
	public function mark_contact_as_read($id){
		$data["status"] = "1";
		$this->save_contact((int)$id, $data);
		return;
	}
	
	/**
	 * Mark contact as unread
	 * @param integer $id
	 * @return void
	 */
	public function mark_contact_as_unread($id){
		$data["status"] = "0";
		$this->save_contact((int)$id, $data);
		return;
	}
	
	/**
	 * Send contact
	 * @param integer $id
	 * @return boolean
	 */
	public function send_contact($id){
		if(!$id) return false;		
		
		$data = $this->get_contact_by_id($id);
		if(empty($data) || $data["send"]){
			return false;
		}

		//get user email
		$this->CI->load->model("Users_model");
		$user = $this->CI->Users_model->get_user_by_id($data["user_id"]);
		$email = $user["email"];
		
		$data['user'] = $user["output_name"];
		
		//send email
		$this->CI->load->model("Notifications_model");

		$this->CI->Notifications_model->send_notification($email, "user_guest_contact", $data);
		
		unset($data["user"]);
		
		//update		
		$data["send"] = "1";
		$this->save_contact($id, $data);
		return true;
	}
	
	/**
	 * Format contact
	 * @param array $data
	 * @return array
	 */
	public function format_contact($data){
		if(!$this->format_settings["use_format"]){
			return $data;
		}
		
		$users_search = array();
		
		foreach($data as $key=>$contact){
			$data[$key] = $contact;
			//	"get_user"
			if($this->format_settings["get_user"]){
				$users_search[] = $contact["user_id"];
			}
			$data[$key]["message"] = nl2br($contact["message"]);
		}
		
		if($this->format_settings["get_user"] && !empty($users_search)){
			$this->CI->load->model("Users_model");
			$users_data = $this->CI->Users_model->get_users_list_by_key(null, null, null, array(), $users_search);
			foreach($data as $key=>$contact){
				$data[$key]["user"] = (isset($users_data[$contact["user_id"]])) ? 
					$users_data[$contact["user_id"]] : $this->CI->Users_model->format_default_user($contact["user_id"]);
			}
		}
		
		return $data;
	}
}
