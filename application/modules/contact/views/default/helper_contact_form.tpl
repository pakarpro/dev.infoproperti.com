{if $header}{$header}{/if}
{if $phone_format}
{js file='jquery.maskedinput.min.js'}
<script>{literal}
$(function(){
	$('#phone').mask('{/literal}{$phone_format}{literal}');
});
{/literal}</script>
{/if}
{js file='jquery-validation/jquery.validate.js'}
{js file='jquery-validation/additional-methods.js'}
{js module=contact file='contact.js'}
<div class="contact_form edit_block">
	<form id="contact_form" name="contact_form" action="" method="POST">
		<div class="r">
			<div class="f">{l i='field_contact_sender' gid='contact'}&nbsp;*</div>
			<div class="v"><input type="text" AUTOCOMPLETE=OFF name="sender" value="{$sender|escape}"></div>
		</div>
		<div class="r">
			<div class="f">{l i='field_contact_phone' gid='contact'}&nbsp;*</div>
			<div class="v"><input type="text" AUTOCOMPLETE=OFF name="phone" value="{$phone|escape}" id="phone"></div>
		</div>
		<div class="r">
			<div class="f">{l i='field_contact_email' gid='contact'}&nbsp;*</div>
			<div class="v"><input type="text" AUTOCOMPLETE=OFF name="email" value="{$email|escape}"></div>
		</div>
		<div class="r">
			<div class="f">{l i='field_contact_message' gid='contact'}&nbsp;({l i='field_contact_optional' gid='contact' type=''})</div>
			<div class="v"><textarea name="message" rows="5" cols="23">{$message|escape}</textarea></div>
		</div>
		<div class="r">
			<div class="f">{l i='field_contact_captcha' gid='contact'}&nbsp;*</div>
			<div class="v captcha">
				{$data.captcha_image}
				<span><input type="text" AUTOCOMPLETE=OFF size="8" name="code" value="" maxlength="{$data.captcha_word_length}"></span>				
			</div>
		</div>
		<div class="r">
			<input type="submit" value="{l i='btn_send' gid='start' type='button'}" />
		</div>
		<input type="hidden" name="user_id" value="{$data.user_id}" /> 
	</form>
</div>
<script>{literal}
var ct;
$(function(){
	ct = new Contact({
		siteUrl: '{/literal}{$site_url}{literal}', 
		emptyUser: '{/literal}{l i='error_empty_user' gid='contact'}{literal}',
		emptySender: '{/literal}{l i='error_empty_sender' gid='contact'}{literal}',
		emptyPhone: '{/literal}{l i='error_empty_phone' gid='contact'}{literal}',
		emptyEmail: '{/literal}{l i='error_empty_email' gid='contact'}{literal}',
		emptyMessage: '{/literal}{l i='error_empty_message' gid='contact'}{literal}',
		emptyCode: '{/literal}{l i='error_empty_code' gid='contact'}{literal}',
		invalidEmail: '{/literal}{l i='error_invalid_email' gid='contact'}{literal}',
		invalidCode: '{/literal}{l i='error_invalid_captcha' gid='contact'}{literal}',
	});		
});
{/literal}</script>
