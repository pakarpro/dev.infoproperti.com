{include file="header.tpl"}
{helper func_name=get_admin_level1_menu helper_name=menu func_param='admin_contact_menu'}

<div class="actions">
	<ul>
		{if $filter_data.contact_need_approve}
		<li id="send_all"><div class="l"><a href="{$site_url}admin/contact/send/1">{l i='link_send' gid='contact'}</a></div></li>
		{/if}
		<li id="delete_all"><div class="l"><a href="{$site_url}admin/contact/delete">{l i='btn_delete' gid='start'}</a></div></li>
	</ul>
	&nbsp;
</div>

{if $filter_data.contact_make_copies}
<div class="menu-level3">
	<ul>
		<li class="{if $filter eq 'all'}active{/if}{if !$filter_data.all} hide{/if}"><a href="{$site_url}admin/contact/index/all">{l i='stat_header_contact_all' gid='contact'} ({$filter_data.all})</a></li>
		<li class="{if $filter eq 'unread'}active{/if}{if !$filter_data.unread} hide{/if}"><a href="{$site_url}admin/contact/index/unread">{l i='stat_header_contact_unread' gid='contact'} ({$filter_data.unread})</a></li>
		{if $filter_data.contact_need_approve}
		<li class="{if $filter eq 'need_approve'}active{/if}{if !$filter_data.need_approve} hide{/if}"><a href="{$site_url}admin/contact/index/need_approve">{l i='stat_header_contact_need_approve' gid='contact'} ({$filter_data.need_approve})</a></li>
		{/if}
	</ul>
	&nbsp;
</div>
{/if}
<form id="contact_form" action="" method="post">
<table cellspacing="0" cellpadding="0" class="data" width="100%">
	<tr>
		<th class="first w20"><a href="{$sort_links.status}"{if $order eq 'status'} class="{$order_direction|lower}"{/if}>&nbsp;</a></th>
		<th class="w20"><input type="checkbox" id="grouping_all"></th>
		<th class="w200"><a href="{$sort_links.message}"{if $order eq 'message'} class="{$order_direction|lower}"{/if}>{l i='field_contact_message' gid='contact'}</a></th>
		<th class="w150"><a href="{$sort_links.sender}"{if $order eq 'sender'} class="{$order_direction|lower}"{/if}>{l i='field_contact_sender' gid='contact'}</a></th>
		<th class="w100"><a href="{$sort_links.date_add}"{if $order eq 'date_add'} class="{$order_direction|lower}"{/if}>{l i='field_contact_date_add' gid='contact'}</a></th>
		<th class="w100">&nbsp;</th>
	</tr>
	{foreach item=item from=$contacts}
		{counter print=false assign=counter}
		<tr{if $counter is div by 2} class="zebra"{/if}>
			<td class="center">
				{if $item.status}
				<a href="{$site_url}admin/contact/read/{$item.id}/0"><img src="{$site_root}{$img_folder}icon-unmark.png" width="16" height="16" border="0" alt="{l i='link_unread_contact' gid='contact' type='button'}" title="{l i='link_unread_contact' gid='contact' type='button'}"></a>						
				{else}
				<a href="{$site_url}admin/contact/read/{$item.id}/1"><img src="{$site_root}{$img_folder}icon-mark.png" width="16" height="16" border="0" alt="{l i='link_read_contact' gid='contact' type='button'}" title="{l i='link_read_contact' gid='contact' type='button'}"></a>			
				{/if}
			</td>	
			<td class="center"><input type="checkbox" name="ids[]" class="grouping" value="{$item.id}"></td>
			<td class="left">{if !$item.status}<b>{/if}<a href="{$site_url}admin/contact/show/{$item.id}">{$item.message|truncate:75}</a>{if !$item.status}</b>{/if}</td>
			<td class="left">{if !$item.status}<b>{/if}{$item.sender}{if !$item.status}</b>{/if}</td>
			<td class="center">{if !$item.status}<b>{/if}{$item.date_add|date_format:$page_data.date_format}{if !$item.status}</b>{/if}</td>
			<td class="icons">				
				<a href="{$site_url}admin/contact/show/{$item.id}"><img src="{$site_root}{$img_folder}icon-view.png" width="16" height="16" border="0" alt="{l i='link_show_contact' gid='contact' type='button'}" title="{l i='link_show_contact' gid='contact' type='button'}"></a>
				<a href="{$site_url}admin/contact/delete/{$item.id}" onclick="javascript: if(!confirm('{l i='note_delete_contact' gid='contact' type='js'}')) return false;"><img src="{$site_root}{$img_folder}icon-delete.png" width="16" height="16" border="0" alt="{l i='link_delete_contact' gid='contact' type='button'}" title="{l i='link_delete_contact' gid='contact' type='button'}"></a>				
				{if $filter_data.contact_need_approve}
					{if $item.allow_send && !$item.send}
					<a href="{$site_url}admin/contact/send/1/{$item.id}"><img src="{$site_root}{$img_folder}icon-mail.png" width="16" height="16" border="0" alt="{l i='link_send_contact' gid='contact' type='button'}" title="{l i='link_send_contact' gid='contact' type='button'}"></a>	
					{else}
					&nbsp;
					{/if}
				{/if}
			</td>
		</tr>
	{foreachelse}
		<tr><td colspan="6" class="center">{l i='no_contacts' gid='contact'}</td></tr>
	{/foreach}
</table>
</form>
{include file="pagination.tpl"}

<script>{literal}
var reload_link = "{/literal}{$site_url}admin/contact/index/{literal}";
var filter = '{/literal}{$filter}{literal}';
var order = '{/literal}{$order}{literal}';
var loading_content;
var order_direction = '{/literal}{$order_direction}{literal}';
$(function(){
	$('#grouping_all').bind('click', function(){
		var checked = $(this).is(':checked');
		if(checked){
			$('input.grouping').attr('checked', 'checked');
		}else{
			$('input.grouping').removeAttr('checked');
		}
	});
	$('#grouping_all').bind('click', function(){
		var checked = $(this).is(':checked');
		if(checked){
			$('input[type=checkbox].grouping').attr('checked', 'checked');
		}else{
			$('input[type=checkbox].grouping').removeAttr('checked');
		}
	});
	$('#send_all,#delete_all').bind('click', function(){
		if(!$('input[type=checkbox].grouping').is(':checked')) return false; 
		if(this.id == 'delete_all' && !confirm('{/literal}{l i='note_delete_all_contacts' gid='contact' type='js'}{literal}')) return false;
		$('#contact_form').attr('action', $(this).find('a').attr('href')).submit();		
		return false;
	});

});
function reload_this_page(value){
	var link = reload_link + filter + '/' + value + '/' + order + '/' + order_direction;
	location.href=link;
}
{/literal}</script>

{include file="footer.tpl"}
