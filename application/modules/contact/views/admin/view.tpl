{include file="header.tpl"}
<div class="actions">
	<ul>
		{if	$data.allow_send && !$data.send}
		<li><div class="l"><a href="{$site_url}admin/contact/send/1/{$data.id}">{l i='link_send' gid='contact'}</a></div></li>
		{/if}
		<li><div class="l"><a href="{$site_url}admin/contact/delete/{$data.id}" onclick="javascript: if(!confirm('{l i='note_delete_contact' gid='contact' type='js'}')) return false;">{l i='btn_delete' gid='start'}</a></div></li>
	</ul>
	&nbsp;
</div>
<div class="edit-form n150">
	<div class="row header">{l i='admin_header_contact_show' gid='contact'}</div>
	<div class="row">
		<div class="h">{l i='field_contact_sender' gid='contact'}: </div>
		<div class="v">{$data.sender}</div>
	</div>
	<div class="row">
		<div class="h">{l i='field_contact_email' gid='contact'}: </div>
		<div class="v">{$data.email}</div>
	</div>
	<div class="row">
		<div class="h">{l i='field_contact_phone' gid='contact'}: </div>
		<div class="v">{$data.phone}</div>
	</div>
	<div class="row">
		<div class="h">{l i='field_contact_message' gid='contact'}: </div>
		<div class="v">{$data.message}</div>
	</div>
	<div class="row">
		<div class="h">{l i='field_contact_date_add' gid='contact'}: </div>
		<div class="v">{$data.date_add|date_format:$page_data.date_format}</div>
	</div>
	{if $data.allow_send || $data.send}
	<div class="row">
		<div class="h">{l i='field_contact_send_status' gid='contact'}: </div>
		<div class="v">{if $data.send}{l i='field_contact_send' gid='contact'}{else}{l i='field_contact_nosend' gid='contact'}{/if}</div>	
	</div>
	{/if}
</div>
<a class="cancel" href="{$site_url}admin/contact/index/{$filter}">{l i='btn_cancel' gid='start'}</a>
<div class="clr"></div>
<script>{literal}
$(function(){
	$("div.row:odd").addClass("zebra");
});
{/literal}</script>

{include file="footer.tpl"}
