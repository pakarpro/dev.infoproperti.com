<?php
/**
* Contact admin side controller
* 
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/
class Admin_Contact extends Controller{
	
	/**
	 * Available filters
	 * @var array
	 */
	private $filters = array("all", "unread", "need_approve");
	
	/**
	 * Constructor
	 *
	 * @return Admin_contact
	 */
	public function __construct(){
		parent::Controller();
		$this->load->model("Menu_model");
		$this->Menu_model->set_menu_active_item("admin_menu", "feedbacks-items");
	}
	
	/**
	 * Render contacts list
	 * @param string $filter
	 * @param string $order
	 * @param string $order_direction
	 * @param integer $page
	 */
	public function index($filter="all", $order="date_add", $order_direction="DESC", $page=1){
		$this->load->model("Contact_model");
		
		$this->Menu_model->set_menu_active_item("admin_contact_menu", "contact_list_item");
		
		if(!in_array($filter, $this->filters)) $filter = $this->filters[0];
		
		$current_settings = isset($_SESSION["contacts_list"]) ? $_SESSION["contacts_list"] : array();
		if(!isset($current_settings["order"])) $current_settings["order"] = "date_add";
		if(!isset($current_settings["order_direction"])) $current_settings["order_direction"] = "DESC";
		if(!isset($current_settings["page"])) $current_settings["page"] = 1;

		$current_settings["page"] = $page;		
		$current_settings["filter"] = $filter;

		if(!$order) $order = $current_settings["order"];
		$this->template_lite->assign("order", $order);
		$current_settings["order"] = $order;

		if(!$order_direction) $order_direction = $current_settings["order_direction"];
		$this->template_lite->assign("order_direction", $order_direction);
		$current_settings["order_direction"] = $order_direction;

		$contacts_count = $this->Contact_model->{"get_".$filter."_contacts_count"}();

		if(!$page) $page = $current_settings["page"];
		$items_on_page = $this->pg_module->get_module_config("start", "admin_items_per_page");
			
		$this->load->helper("sort_order");
		$page = get_exists_page_number($page, $contacts_count, $items_on_page);
		$current_settings["page"] = $page;

		$_SESSION["contacts_list"] = $current_settings;

		$sort_links = array(
			"date_add" => site_url()."admin/contact/index/".$filter."/date_add/".(($order != "date_add" xor $order_direction == "DESC") ? "ASC" : "DESC"),
			"sender" => site_url()."admin/contact/index/".$filter."/sender/".(($order != "sender" xor $order_direction == "DESC") ? "ASC" : "DESC"),
			"message" => site_url()."admin/contact/index/".$filter."/message/".(($order != "message" xor $order_direction == "DESC") ? "ASC" : "DESC"),
			"status" => site_url()."admin/contact/index/".$filter."/status/".(($order != "status" xor $order_direction == "DESC") ? "ASC" : "DESC"),
		);		
		$this->template_lite->assign("sort_links", $sort_links);

		if($contacts_count > 0){
			$contacts = $this->Contact_model->{"get_".$filter."_contacts_list"}($page, $items_on_page, array($order => $order_direction));
			$this->template_lite->assign("contacts", $contacts);
		}
		$this->load->helper("navigation");
		$this->config->load("date_formats", TRUE);
		$url = site_url()."admin/contact/index/".$filter."/".$order."/".$order_direction."/";
		$page_data = get_admin_pages_data($url, $contacts_count, $items_on_page, $page, "briefPage");
		$page_data["date_format"] = $this->config->item("st_format_date_time_literal", "date_formats");
		$this->template_lite->assign("page_data", $page_data);
		
		$filter_data["contact_make_copies"] = $this->pg_module->get_module_config("contact", "contact_make_copies");		
		if ($filter_data["contact_make_copies"]){
			$filter_data["contact_need_approve"] = $this->pg_module->get_module_config("contact", "contact_need_approve");			
			$filter_data["all"] = $this->Contact_model->get_all_contacts_count();
			$filter_data["unread"] = $this->Contact_model->get_unread_contacts_count();
			if ($filter_data["contact_need_approve"]){
				$filter_data["need_approve"] = $this->Contact_model->get_need_approve_contacts_count();
			}
		}
		$this->template_lite->assign("filter", $filter);
		$this->template_lite->assign("filter_data", $filter_data);
		$this->system_messages->set_data("header", l("admin_header_contacts_list", "contact"));		
		$this->template_lite->view("list");
	}
	
	/**
	 * Render need approve contacts list
	 * @param string $order
	 * @param string $order_direction
	 * @param integer $page
	 */
	public function need_approve($order="date_add", $order_direction="DESC", $page=1){
		$this->_get_contacts("need_approve", $order, $order_direction, $page);
	}
	
	/**
	 * Show contact
	 * @param integer $id
	 */
	public function show($id){
		if(!$id){show_404();return;}
		
		$this->Menu_model->set_menu_active_item("admin_contact_menu", "contact_list_item");
		
		$this->load->model("Contact_model");	
		
		$this->Contact_model->mark_contact_as_read($id);

		$current_settings = isset($_SESSION["contacts_list"]) ? $_SESSION["contacts_list"] : array("filter"=>"all");
		$this->template_lite->assign("filter", $current_settings["filter"]);

		$this->load->helper("navigation");
		$this->config->load("date_formats", true);
		$url = site_url()."admin/contact/index/".$current_settings['filter']."/".$order."/".$order_direction."/";
		$page_data = get_admin_pages_data($url, $contacts_count, $items_on_page, $page, "briefPage");
		$page_data["date_format"] = $this->config->item("st_format_date_time_literal", "date_formats");
		$this->template_lite->assign("page_data", $page_data);
		
		$data = $this->Contact_model->get_contact_by_id($id);		
		$this->template_lite->assign("data", $data);
		
		$this->system_messages->set_data("header", l("admin_header_contact_edit", "contact"));		
		$this->template_lite->view("view");
	}
	
	/**
	 * Change contact status
	 * @param integer $id
	 * @param integer $status
	 */
	public function read($id, $status){	
		if(!$id) return;		
		$this->load->model("Contact_model");
		
		if(intval($status)){
			$this->Contact_model->mark_contact_as_read($id);
		}else{
			$this->Contact_model->mark_contact_as_unread($id);
		}		
		redirect(site_url()."admin/contact/index");
	}
	
	/**
	 * Send or decline contacts
	 * @param integer $status
	 * @param integer $id
	 */
	public function send($status, $id=null){
		if(!$id) $id = $this->input->post("ids");
		if(!empty($id)){
			$this->load->model("Contact_model");
			foreach((array)$id as $i){
				if(intval($status)){
					$this->Contact_model->send_contact($i);
				}else{
					$this->Contact_model->decline_contact($i);
				}
			}
			$this->system_messages->add_message("success", l("success_contact_send", "contact"));
		}		
		$filter = isset($_SESSION["contacts_list"]['filter']) ? $_SESSION["contacts_list"]['filter'] : "all";
		redirect(site_url()."admin/contact/index/".$filter);
	}
	
	/**
	 * Remove contacts
	 * @param integer $id 
	 */
	public function delete($ids=null){
		if(!$ids) $ids = $this->input->post("ids");
		
		if(!empty($ids)){
		    $this->load->model("Contact_model");
		    foreach((array)$ids as $id) $this->Contact_model->delete_contact($id);
		    $this->system_messages->add_message("success", l("success_contact_delete", "contact"));
		}		
		$filter = isset($_SESSION["contacts_list"]['filter']) ? $_SESSION["contacts_list"]['filter'] : "all";
		redirect(site_url()."admin/contact/index/".$filter);
	}
	
	/**
	 * Render module settings
	 */
	public function settings(){
		$this->Menu_model->set_menu_active_item("admin_contact_menu", "contact_settings_item");
		
		if($this->input->post("btn_save")){
			$this->load->model("Contact_model");
			
			$post_data["contact_make_copies"] = $this->input->post("contact_make_copies");
			$post_data["contact_need_approve"] = $this->input->post("contact_need_approve");
			$post_data["contact_send_mail"] = $this->input->post("contact_send_mail");
			$post_data["contact_admin_email"] = $this->input->post("contact_admin_email");

			$validate_data = $this->Contact_model->validate_contact_settings($post_data);
			if(!empty($validate_data["errors"])){
				$this->system_messages->add_message("error", $validate_data["errors"]);
				$data = $post_data;
			}else{
				foreach($validate_data["data"] as $setting=>$value){
					$this->pg_module->set_module_config("contact", $setting, $value);
				}
				$this->system_messages->add_message("success", l("success_settings_saved", "contact"));
				$data = $validate_data["data"];
			}
		}else{
			$data["contact_make_copies"] = $this->pg_module->get_module_config("contact", "contact_make_copies");
			$data["contact_need_approve"] = $this->pg_module->get_module_config("contact", "contact_need_approve");
			$data["contact_send_mail"] = $this->pg_module->get_module_config("contact", "contact_send_mail");
			$data["contact_admin_email"] = $this->pg_module->get_module_config("contact", "contact_admin_email");
		}
		$this->template_lite->assign("data", $data);		
		$this->system_messages->set_data("header", l("admin_header_contacts_list", "contact"));				
		$this->template_lite->view("settings");
	}
}
