<?php
/**
* Contact api side controller
*
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Mikhail Chernov <mchernov@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: mchernov $
**/

Class Api_Contact extends Controller{
	
	/**
	 * Constructor
	 * @return Contact
	 */
	public function __construct(){
		parent::Controller();
		$this->load->model("Contact_model");
		/*if ($this->session->userdata("auth_type") == "user"){show_404();}*/		
	}
	
	/**
	 * Send message
	 */
	public function api_send_message(){
		$post_data["user_id"] = intval($this->input->post("user_id"));
		$post_data["sender"] = $this->input->post("sender", true);
		$post_data["phone"] = $this->input->post("phone", true);
		$post_data["email"] = $this->input->post("email", true);
		$post_data["message"] = $this->input->post("message", true);
		$post_data["code"] = $this->input->post("code", true);

		$make_copies = $this->pg_module->get_module_config("contact", "contact_make_copies");
		$need_approve = $this->pg_module->get_module_config("contact", "contact_need_approve");

		$validate_data = $this->Contact_model->validate_contact(null, $post_data);

		$is_captcha_valid = ($this->input->post("code", true) == $this->session->userdata("captcha_word")) ? 1 : 0;
		if(!$is_captcha_valid){
			$validate_data["errors"][] = l("error_invalid_captcha", "contact");
		}
				
		if(!empty($validate_data["errors"])){
			$this->set_api_content("errors", $validate_data["errors"]);
			return;
		}else{
			if($need_approve){
				$validate_data["data"]["allow_send"] = "1";
				
				$send_admin_mail = $this->pg_module->get_module_config("contact", "contact_send_mail");
				if($send_admin_mail){
					$email = $this->pg_module->get_module_config("contact", "contact_admin_email");
					if($email){
						$this->load->model("Notifications_model");	
						$this->Notifications_model->send_notification($email, "auser_guest_contact", array());
					}
				}
			}else{
				$validate_data["data"]["send"] = "1";
				
				//get user email
				$this->load->model("Users_model");
				$user = $this->Users_model->get_user_by_id($validate_data["data"]["user_id"]);
				$email = $user["email"];
		
				$validate_data["data"]["user"] = $user['output_name'];
		
				//send mail
				$this->load->model("Notifications_model");
				$this->Notifications_model->send_notification($email, "user_guest_contact", $validate_data["data"]);
			}
			
			if($make_copies){
				$this->Contact_model->save_contact(null, $validate_data["data"]);
			}
			$this->set_api_content("messages", l("success_contact_save", "contact"));
		}
		return;
	}
}
