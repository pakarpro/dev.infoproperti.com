<?php

$install_lang["notification_auser_guest_contact"] = "Contact from guest (for admin)";
$install_lang["notification_user_guest_contact"] = "Contact from user";
$install_lang["tpl_auser_guest_contact_content"] = "Hello admin,\n\nThere is a new guest contact on [domain]. Access administration panel > Feedbacks > Guest contacts to view it.\n\nBest regards,\n[name_from]";
$install_lang["tpl_auser_guest_contact_subject"] = "[domain] | Guest contact";
$install_lang["tpl_user_guest_contact_content"] = "Hello, [user]!\n\nUser [sender] ([email], [phone])\nsent a message to you:\n\n[message]\n\nBest regards,\n[name_from]";
$install_lang["tpl_user_guest_contact_subject"] = "User [sender] sent a message to you";
