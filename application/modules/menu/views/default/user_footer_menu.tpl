{capture assign='footer_menu'}
	{foreach key=key item=item from=$menu}
	<li {if $item.active}class="active"{/if}>
		{if $item.link}
		<a href="{$item.link}">{$item.value}</a>
		{else}
		{$item.value}
		{/if}
		{if $item.sub}
		{assign var='subitems' value='1'}
		<ul>
			{foreach item=subitem from=$item.sub}<li><a href="{$subitem.link}">{$subitem.value}</a></li>{/foreach}	
		</ul>
		{/if}
	</li>
	{/foreach}
{/capture}

<ul {if $subitems}class="with-groups"{/if}>
	{$footer_menu}
</ul>
