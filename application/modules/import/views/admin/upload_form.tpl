{include file="header.tpl"}

<form method="post" action="{$data.action}" name="save_form" enctype="multipart/form-data">
	<div class="edit-form n150">
		<div class="row header">{l i='admin_header_data_form_upload' gid='import'}</div>
		<div class="row">
			<div class="h">{l i="field_data_driver" gid='import'}:&nbsp;* </div>
			<div class="v">
				<select name="data[gid_driver]" id="driver_select">
					{foreach item=item from=$drivers}
					<option value="{$item.gid}" {if $item.gid eq $data.gid_driver}selected{/if}>{$item.output_name|truncate:100}</option>
					{foreachelse}
					<option value="">-</option>
					{/foreach}
				</select>				
			</div>
		</div>
		<div class="row">
			<div class="h">{l i="field_data_module" gid='import'}:&nbsp;* </div>
			<div class="v">
				<select name="data[id_object]">
					{foreach item=item from=$modules}
					<option value="{$item.id}" {if $item.id eq $data.id_object}selected{/if}>{$item.output_name|truncate:100}</option>
					{foreachelse}
					<option value="">-</option>
					{/foreach}
				</select>
			</div>
		</div>
		<div id="driver_box">{$settings}</div>
		<div class="row">
			<div class="h">{l i='field_data_file' gid='import'}:&nbsp;* </div>
			<div class="v"><input type="file" name="import_file"></div>
		</div>
	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="{l i='btn_save' gid='start' type='button'}"></div></div>
	<a class="cancel" href="{$site_url}admin/import">{l i='btn_cancel' gid='start'}</a>
</form>
<div class="clr"></div>
<script>{literal}
	$(function(){
		$("div.row:odd").addClass("zebra");
		$('#driver_select').bind('change', function(){
			$.post(
				'{/literal}{$site_url}admin/import/ajax_upload_form{literal}', 
				{driver_gid: $(this).val()},
				function(data){
					$('#driver_box').html(data);
					$("div.row:odd").addClass("zebra");
				}
			);
		});
		
	});
{/literal}</script>
{include file="footer.tpl"}
