{include file="header.tpl"}

{js file='jquery-ui.custom.min.js'}
<LINK href='{$site_root}{$js_folder}jquery-ui/jquery-ui.custom.css' rel='stylesheet' type='text/css' media='screen'>
<form method="post" action="{$data.action}" name="save_form" enctype="multipart/form-data">
	<table cellspacing="0" cellpadding="0" class="data" width="100%">
		<tr>
			<th class="first w200">{l i='field_data_name' gid='import'}</th>
			<th class="w200">{l i='field_data_link' gid='import'}</th>
		</tr>
		{foreach item=item key=key from=$data.relations}
		{counter print=false assign=counter}
		<tr{if $counter is div by 2} class="zebra"{/if}>
			<td>{l i='field' gid='import'} {$counter} {if $item.name}({$item.name|truncate:50}){/if}</td>
			<td>
				<select name="link[{$key}]">
					<option value="-1">...</option>
					{foreach item=item2 key=key2 from=$module_fields}
					<option value="{$key2}" {if $data.relations[$key] && $data.relations[$key].type eq $item2.type && $data.relations[$key].link eq $item2.name}selected{/if}>{$item2.label}</option>
					{/foreach}
				</select>
			</td>				
		</tr>
		{foreachelse}
		<tr><td colspan="3" class="center">{l i='no_custom_fields' gid='export'}</td></tr>
		{/foreach}
	</table>		
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="{l i='btn_save' gid='start' type='button'}"></div></div>
	<a class="cancel" href="{$site_url}admin/import">{l i='btn_cancel' gid='start'}</a>
</form>
<script>{literal}
   $(function(){
	   $( "#datepicker1, #datepicker2" ).datepicker({dateFormat :'yy-mm-dd'});
   });
{/literal}</script>
<div class="clr"></div>

{include file="footer.tpl"}
