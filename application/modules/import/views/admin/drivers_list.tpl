{include file="header.tpl"}
{helper func_name=get_admin_level1_menu helper_name=menu func_param='admin_import_menu'}
<div class="actions">
	&nbsp;
</div>

<table cellspacing="0" cellpadding="0" class="data" width="100%">
<tr>
	<th class="first">{l i='field_driver_name' gid='import'}</th>
	<th class="w50">{l i='field_driver_status' gid='import'}</th>
	<th class="w50">{l i='field_driver_link' gid='import'}</th>
</tr>
{foreach item=item from=$drivers}
{counter print=false assign=counter}
<tr{if $counter is div by 2} class="zebra"{/if}>
	<td class="first">{$item.output_name|truncate:100}</td>
	<td class="center">
		{if $item.status}
		<a href="{$site_url}admin/import/activate_driver/{$item.gid}/0"><img src="{$site_root}{$img_folder}icon-full.png" width="16" height="16" border="0" alt="{l i='link_deactivate_driver' gid='import' type='button'}" title="{l i='link_deactivate_driver' gid='import' type='button'}">
		{else}
		<a href="{$site_url}admin/import/activate_driver/{$item.gid}/1"><img src="{$site_root}{$img_folder}icon-empty.png" width="16" height="16" border="0" alt="{l i='link_activate_driver' gid='import' type='button'}" title="{l i='link_activate_driver' gid='import' type='button'}"></a>
		{/if}
	</td>
	<td class="center">
		<a href="{$item.link}" target="_blank">
			{if $item.need_regkey}
				{l i='driver_registration' gid='import'}
			{else}
				{l i='driver_info' gid='import'}
			{/if}
		</a>
	</td>
</tr>
{foreachelse}
<tr><td colspan="5" class="first center">{l i='no_drivers' gid='import'}</td></tr>
{/foreach}
</table>
{include file="pagination.tpl"}

{include file="footer.tpl"}
