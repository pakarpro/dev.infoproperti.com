{include file="header.tpl"}
{js module='import' file='import.js'}
<form method="post" action="{$data.action}" name="save_form" enctype="multipart/form-data">
<div class="filter-form">
	<div class="form">
		<b>{l i='header_set_relations' gid='import'}</b><br><br>
		{l i='text_set_relations' gid='import'}: {$relations} <a href="{$site_url}admin/import/relations/{$data.id}">{if $relations}{l i='link_update_relations' gid='import'}{else}{l i='link_create_relations' gid='import'}{/if}</a><br><br>	
		<br>
		{if $two_step}
		<b>{l i='header_data_file' gid='import'}</b><br><br>
		{l i='text_prepare_data_count' gid='import'}: <span id="processed">{$processed}</span> <a href="{$site_url}admin/import/parse/{$data.id}" id="parse_link">{l i='link_prepare_data' gid='import'}</a><br><br>	
		<br>
		{/if}
		<b>{l i='header_data_import' gid='import'}</b><br><br>
		{l i='text_make_data_count' gid='import'}: <span id="imported">{$imported}</span> <a href="{$site_url}admin/import/make/{$data.id}" id="make_link">{l i='link_make_data' gid='import'}</a><br><br>	
	</div>
</div>
</form>
<a class="cancel" href="{$site_url}admin/import">{l i='btn_cancel' gid='start'}</a>
<script>{literal}
	$(function(){
		new Import({
			siteUrl: '{/literal}{$site_url}{literal}', 
			importId: {/literal}{$data.id}{literal},
		});	
	});
{/literal}</script>
{include file="footer.tpl"}
