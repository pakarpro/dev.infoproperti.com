		{foreach item=item key=key from=$settings}
		<div class="row">
			<div class="h">{l i='field_'+$driver_gid+'_'+$item.name gid='import'}: </div>
			<div class="v">
				{if $item.type eq 'checkbox'}
				<input type="checkbox" name="data[settings][{$item.name}]]" value="1" {if $item.default}checked{/if}>
				{else}
				<input type="text" name="data[settings][{$item.name}]]" value="{$item.default|escape}">
				{/if}
			</div>
		</div>
		{/foreach}
