<?php
/**
* Import model
* 
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/

define("IMPORT_TABLE", DB_PREFIX."import");
define("IMPORT_DATA_TABLE", DB_PREFIX."import_data");

class Import_model extends Model{

	/**
	 * link to CodeIgniter object
	 * @var object
	 */
	private $CI;

	/**
	 * link to DataBase object
	 * @var object
	 */
	private $DB;

	/**
	 * Table fields
	 * @var array
	 */
	var $_fields = array(
		"id",
		"gid_driver",
		"id_object",
		"settings",
		"relations",
		"filename",
		"position",
		"total",
		"processed",
		"imported",	
		"failed",	
		"status",
		"date_created",
	);
	
	/**
	 * Format settings
	 * @var array
	 */
	private $format_settings = array(
		"use_format"  => true,
		"get_driver"  => true,
		"get_module"  => true,
	);
	
	/**
	 * Cache array for used drivers
	 * @var array
	 */
	private $driver_cache = array();
	
	/**
	 * Cache array for used modules
	 * @var array
	 */
	private $module_cache = array();
	
	/**
	 * Path to uploaded files
	 * @var string
	 */
	private $base_path = "import/";
	
	/**
	 * Import data limit per request
	 * @var integer
	 */
	protected $import_limit = 100;
	
	/**
	 * Constructor
	 *
	 * return Import_model
	 * required Import_driver_model
	 */
	function __construct(){
		parent::Model();
		$this->CI = & get_instance();
		$this->DB = &$this->CI->db;
		$this->CI->load->model("import/models/Import_driver_model");
		$this->CI->load->model("import/models/Import_module_model");
	}

	/**
	 * Get import driver by GUID
	 * @param string $driver_gid driver guid
	 * @return array
	 */
	public function get_driver_by_gid($driver_gid){
		if(!isset($this->driver_cache[$driver_gid])){
			$driver_data = $this->CI->Import_driver_model->get_driver_by_gid($driver_gid);
			if(!is_array($driver_data) || !count($driver_data)) return false;
			$this->driver_cache[$driver_data["id"]] = $driver_data;
			$this->driver_cache[$driver_gid] = $driver_data;
		}

		if(is_array($this->driver_cahce[$driver_gid]) && count($this->driver_cache[$type_gid])){
			return $this->driver_cahce[$driver_gid];
		}else{
			return false;
		}
	}


	/**
	 * Get import data by ID
	 * @param integer $id import data identifier
	 * @param 
	 */ 
	public function get_data_by_id($id, $formatted=false){
		$id = intval($id);
		
		$this->DB->select(implode(", ", $this->_fields));
		$this->DB->from(IMPORT_TABLE);
		$this->DB->where("id", $id);
		
		//_compile_select;
		$result = $this->DB->get()->result_array();
		if(!empty($result)){
			$rt = array($result[0]);
			if($formatted) $rt = $this->format_data($rt);
			return $rt[0];
		}else
			return false;
	}
	
	/**
	 * Save data
	 * @param array $data
	 * @return boolean
	 */
	public function save_data($id, $data){
		if(!$id){
			$data["date_created"] = date("Y-m-d H:i:s");
			$this->DB->insert(IMPORT_TABLE, $data);
			$id = $this->DB->insert_id();
		}else{		
			$this->DB->where("id", $id);
			$this->DB->update(IMPORT_TABLE, $data);
		}		
		return $id;
	}
	
	/**
	 * Remove data by ID
	 * @param integer $id data identifier
	 */ 
	public function delete_data($id){
		$this->DB->where("id_import", $id);
		$this->DB->delete(IMPORT_DATA_TABLE);
		
		$this->DB->where("id", $id);
		$this->DB->delete(IMPORT_TABLE);
	}
	
	/**
	 * Remove file
	 * @param integer $data_id data identifier
	 * @param boolean $deep remove all files
	 */ 
	public function delete_file($import_id, $deep=false){
		$import_data = $this->get_data_by_id($import_id, true);
		if(!$import_data) return false;
		if($deep){
			if(!file_exists(UPLOAD_DIR.$this->base_path.$import_id)) return;
			foreach(scandir(UPLOAD_DIR.$this->base_path.$import_id) as $filename){
				if($filename == "." || $filename == "..") continue;
				unlink(UPLOAD_DIR.$this->base_path.$import_id."/".$filename);
			}
			rmdir(UPLOAD_DIR.$this->base_path.$import_id);
			if(!$import_data["filename"]) return true;		
		}else{		
			if(!$import_data["filename"]) return false;		
			unlink(UPLOAD_DIR.$this->base_path.$import_id."/".$import_data["filename"]);		
			
		}
		$data["filename"] = "";
		$this->DB->where("id", $import_id);
		$this->DB->update(IMPORT_TABLE, $data);
		return true;
	}
		
	/**
	 * Return modules filter
	 * @param integer $module_id module identifier
	 * @return array
	 */
	private function _get_data_by_module($module_id){
		if(!$object) return array();
		$params["where"]["id_module"] = $module_id;
		return $params;
	}
	
	/**
	 * Return drivers filter
	 * @param string $driver_gid driver guid
	 * @return array
	 */
	private function _get_data_by_driver($driver_gid){
		if(!$object) return array();
		$params["where"]["gid_driver"] = $driver_gid;
		return $params;
	}
	
	/**
	 * Return import data as array
	 * @param integer $page
	 * @param string $limits 
	 * @param array $order_by
	 * @param array $params
	 * @return array
	 */
	private function _get_data_list($page=null, $limits=null, $order_by=null, $params=array()){
		$this->DB->select(implode(", ", $this->_fields));
		$this->DB->from(IMPORT_TABLE);
		
		if(isset($params["where"]) && is_array($params["where"]) && count($params["where"])){
			foreach($params["where"] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}
		
		if(isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])){
			foreach($params["where_in"] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])){
			foreach($params["where_sql"] as $value){
				$this->DB->where($value);
			}
		}

		if (is_array($order_by) && count($order_by) > 0){
			foreach ($order_by as $field => $dir){
				if (in_array($field, $this->_fields)){
					$this->DB->order_by($field." ".$dir);
				}
			}
		} else if ($order_by){
			$this->DB->order_by($order_by);
		}

		if(!is_null($page)){
			$page = intval($page)?intval($page):1;
			$this->DB->limit($limits, $limits*($page-1));
		}

		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			foreach($results as $r){
				$data[] = $r;
			}
			return $this->format_data($data);
		}
		return array();
	}
	
	/**
	 * Return data as array
	 * @param array $params
	 * @return integer
	 */
	private function _get_data_count($params=array()){
		$this->DB->select("COUNT(*) AS cnt");
		$this->DB->from(IMPORT_TABLE);

		if(isset($params["where"]) && is_array($params["where"]) && count($params["where"])){
			foreach($params["where"] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])){
			foreach($params["where_in"] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])){
			foreach($params["where_sql"] as $value){
				$this->DB->where($value);
			}
		}

		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			return intval($results[0]["cnt"]);
		}
		return 0;
	}
	
	/**
	 * Return data as array
	 * @param array $filters
	 * @param integer $page
	 * @param string $limits 
	 * @param array $order_by
	 * @return array
	 */
	public function get_data_list($filters=array(), $page=null, $limits=null, $order_by=null){
		$params = array();
		foreach($filters as $filter=>$value){
			$params = array_merge_recursive($params, $this->{"_get_data_by_".$filter}($value));
		}
		return $this->_get_data_list($page, $limits, $order_by);
	}
	
	/**
	 * Return number of data
	 * @param string $type_gid
	 * @return integer
	 */
	public function get_data_count($filters=array()){
		$params = array();
		foreach($filters as $filter=>$value){
			$params = array_merge_recursive($params, $this->{"_get_data_by_".$filter}($value));
		}
		return $this->_get_data_count($params);
	}
	
	/**
	 * Validate data data
	 * @param array $data
	 * @param boolean $required
	 * @return array
	 */
	public function validate_data($id, $data){
		$return = array("errors"=> array(), "data" => array());

		if(isset($data['id'])){
			$return['data']['id'] = intval($data['id']);
			if(empty($return['data']['id'])) unset($return['data']['id']);
		}
		
		if(isset($data['gid_driver'])){
			$return['data']['gid_driver'] = trim(strip_tags($data["gid_driver"]));
			if(empty($return['data']['gid_driver'])){
				$return["errors"][] = l("error_empty_driver", "import");
			}else{
				$driver = $this->Import_driver_model->get_driver_by_gid($return['data']['gid_driver']);
				if(!$driver) $return["errors"][] = l("error_invalid_driver", "import");
			}
		}elseif(!$id){
			$return["errors"][] = l("error_empty_driver", "import");
		}
		
		if(isset($data['id_object'])){
			$return['data']['id_object'] = intval($data['id_object']);
			if(empty($return['data']['id_object'])){
				$return["errors"][] = l("error_empty_object", "import");
			}else{
				$module = $this->Import_module_model->get_module_by_id($return['data']['id_object']);
				if(!$module) $return["errors"][] = l("error_invalid_module", "import");
			}
		}elseif(!$id){
			$return["errors"][] = l("error_empty_object", "import");
		}
		
		if(isset($data["settings"])){
			if(!is_array($data["settings"])) $data["settings"] = array();
			$return["data"]["settings"] = serialize($data["settings"]);
		}

		if(isset($data["relations"])){
			if(!is_array($data["relations"])) $data["relations"] = array();
			$return["data"]["relations"] = serialize($data["relations"]);
		}
		
		if(isset($data['filename'])){
			$return['data']['filename'] = trim(strip_tags($data["filename"]));
		}
		
		if(isset($data['position'])){
			$return['data']['position'] = intval($data["position"]);
		}
		
		if(isset($data['total'])){
			$return['data']['total'] = intval($data["total"]);
		}
		
		if(isset($data['processed'])){
			$return['data']['processed'] = intval($data["processed"]);
		}
		
		if(isset($data['imported'])){
			$return['data']['imported'] = intval($data["imported"]);
		}
		
		if(isset($data['failed'])){
			$return['data']['failed'] = intval($data["failed"]);
		}
		
		if(isset($data['status'])){
			$return['data']['status'] = intval($data["status"]);
		}	
		
		if(isset($data['date_created'])){
			$value = strtotime($data['date_created']);
			if($value > 0) $return['data']['date_created'] = date("Y-m-d", $value);
		}
	
		return $return;
	}
	
	/**
	 * Set format data
	 * @param array $data
	 * @return array
	 */
	public function set_format_data($data){
		foreach($data as $key=>$value){
			if(isset($this->format_data[$key]))
				$this->format_data[$key] = $value;
		}
	}	
		
	/**
	 * Format data
	 * @param array $data
	 * @return array
	 */
	public function format_data($data){
		if(!$this->format_settings["use_format"]){
			return $data;
		}
		
		$drivers_data = $this->CI->Import_driver_model->get_drivers();
		foreach($drivers_data as $driver_data){
			$this->driver_cache[$driver_data["gid"]] = $driver_data;
		}
		
		$modules_data = $this->CI->Import_module_model->get_modules();
		foreach($modules_data as $module_data){
			$this->module_cache[$module_data["id"]] = $module_data;
		}	
		
		$drivers_search = array();
		$modules_search = array();
		
		foreach($data as $key=>$item){
			$data[$key] = $item;			
			//get driver
			if($this->format_settings["get_driver"]){
				$drivers_search[] = $item["gid_driver"];
			}
			//get module
			if($this->format_settings["get_module"]){
				$modules_search[] = $item["id_object"];
			}
			
			$data[$key]["settings"] = (array)unserialize($item["settings"]);
			$data[$key]["relations"] = (array)unserialize($item["relations"]);
		}
		
		if($this->format_settings["get_driver"] && !empty($drivers_search)){
			foreach($data as $key=>$item){
				$data[$key]["driver"] = (isset($this->driver_cache[$item["gid_driver"]])) ? 
					$this->driver_cache[$item["gid_driver"]] : $this->CI->Import_driver_model->format_default_driver($item["gid_driver"]);
			}
		}
		
		if($this->format_settings["get_module"] && !empty($modules_search)){
			foreach($data as $key=>$item){
				$data[$key]["module"] = (isset($this->module_cache[$item["id_object"]])) ? 
					$this->module_cache[$item["id_object"]] : $this->CI->Import_module_model->format_default_module($item["id_object"]);
			}
		}
		
		return $data;
	}
	
	/**
	 * Validate file
	 * @param string $upload_gid upload file guid
	 * @param string $driver_gid driver guid
	 */
	public function validate_upload($upload_gid, $data){
	
		$return = array("errors" => array(), "data"=>$data);
	
		$this->CI->load->helper("upload");
		
		$model_name = "import_".$data["gid_driver"]."_model";
		$this->load->model("import/models/drivers/".$model_name, $model_name);
		
		//// upload src file
		$upload_config = array(
			"allowed_types" => $this->CI->{$model_name}->allowed_types,
			"overwrite" => true,
		);

		$result = validate_file($upload_gid, $upload_config);
		if($result["error"]) $return["errors"] = $result["error"];
		return $return;
	}
	
	/**
	 * validate file
	 * @param string $upload_gid upload file guid
	 * @param integer $data_id import data
	 */
	public function upload($upload_gid, $data_id){
		$return = array("errors"=>array(), "data"=>array());
		
		$import_data = $this->get_data_by_id($data_id, true);
		$model_name = "import_".$import_data["gid_driver"]."_model";
		$this->load->model("import/models/drivers/".$model_name, $model_name);
		
		$this->CI->load->helper("upload");
		
		//// upload src file
		$upload_config = array(
			"allowed_types" => $this->CI->{$model_name}->allowed_types,
			"overwrite" => true,
		);

		$result = upload_file($upload_gid, TEMPPATH.$this->base_path, $upload_config);	
		if(!empty($result["error"])){
			$return["errors"] = $result["error"];
		}else{
			@mkdir(UPLOAD_DIR.$this->base_path.$data_id);
			$pathinfo = pathinfo($result["data"]["full_path"]);
			if($pathinfo["extension"] == "zip"){
				$this->CI->load->library("unzip");				
				$this->CI->unzip->fileName = $result["data"]["full_path"];
				$this->CI->unzip->targetDir = UPLOAD_DIR.$this->base_path.$data_id;
				$this->CI->unzip->unzipAll();
				$this->CI->unzip->close();
				$result["data"]["file_name"] = substr($result["data"]["file_name"], 0, -4).".".$this->CI->{$model_name}->extension;				
				if(!file_exists(UPLOAD_DIR.$this->base_path.$data_id.'/'.$result["data"]["file_name"])){
					$found = false;
					foreach(scandir(UPLOAD_DIR.$this->base_path.$data_id) as $file){						
						if($file == '.' || $file == '..' || is_dir(UPLOAD_DIR.$this->base_path.$data_id.'/'.$file)) continue;
						$pathinfo = pathinfo(UPLOAD_DIR.$this->base_path.$data_id.'/'.$file);
						if($pathinfo['extension'] == $this->CI->{$model_name}->extension){
							@copy(UPLOAD_DIR.$this->base_path.$data_id.'/'.$file, UPLOAD_DIR.$this->base_path.$data_id.'/'.$result["data"]["file_name"]);
							@unlink(UPLOAD_DIR.$this->base_path.$data_id.'/'.$file);
							$found = true;
							break;
						}
					}
					if(!$found){
						$return["errors"][] = l('error_empty_data', 'import');
						return $return;
					}
				}
			}else{
				@copy($result["data"]["full_path"], UPLOAD_DIR.$this->base_path.$data_id."/".$result["data"]["file_name"]);
			}
			$return = $this->{$model_name}->analyze(UPLOAD_DIR.$this->base_path.$data_id."/".$result["data"]["file_name"], $import_data["settings"]);
			$return["data"]["filename"] = $result["data"]["file_name"];
		}
	
		return $return;
	}
	
	/**
	 * Parse imported data
	 * @param string $data_id data identifier
	 */
	public function parse_data($data_id){
		@ini_set("max_execution_time", 0);
		
		$return = array("errors"=>array(), "data"=>array());
		
		$import_data = $this->get_data_by_id($data_id, true);
		if(!$import_data){$return["errors"][] = l("error_empty_data", "import"); return $return;}

		$model_name = "Import_".$import_data["gid_driver"]."_model";
		$this->CI->load->model("import/models/drivers/".$model_name, $model_name);
	
		$filename = UPLOAD_DIR.$this->base_path.$data_id."/".$import_data["filename"];
		$results = $this->CI->{$model_name}->parse($filename, $import_data["settings"], $import_data["relations"], $import_data["position"], $import_data['processed'] || $import_data['imported'] || $import_data['failed']);

		//$this->decline_import_data($import_data["id"]);
		$this->save_import_data($import_data["id"], $results["data"]);
			
		$data["id"] = $import_data["id"];
		$data["position"] = $results["position"];
		$data["status"] = $results["status"];
		$data["processed"] += $results["processed"];
		$this->save_data($data_id, $data);
			
		$this->approve_import_data($import_data["id"]);
	
		if($results["status"] == 1){
			$this->delete_file($import_data["id"]);
		}
		
		$return["data"]["status"] = $results["status"];
		$return["data"]["processed"] = $results["processed"];
		
		return $return;
	}
	
	/**
	 * Import prepared data
	 */
	public function make_import_data($data_id, $user_id=0, $use_moderation=false){
		@ini_set("max_execution_time", 0);
				
		$import_data = $this->get_data_by_id($data_id, true);
		if(!$import_data){						
			return array("errors"=>array(l("error_empty_data", "import")), "data"=>array());
		}
		
		$return = array("errors"=>array(), "data"=>array());
		foreach((array)$import_data["relations"] as $index=>$relation){
			if(!$relation["link"]) unset($import_data["relations"][$index]);
		}
		if(empty($import_data["relations"])) $return["errors"][] = l("error_empty_relations", "import");
		
		//make unique relations
		$relations = array();
		foreach($import_data["relations"] as $index=>$relation){
			if(array_key_exists($relation['link'], $relations)){
				unset($import_data["relations"][$relations[$relation['link']]]);
			}
			$relations[$relation['link']] = $index;
		}
		
		$data  = $this->get_import_data($import_data["id"]);
		if(empty($data)) $return["errors"][] = l("error_empty_data", "import");

		if(empty($return["errors"])){
			foreach($data as $id=>$row){
				$object_data = array();
				foreach($import_data["relations"] as $index=>$relation){
					switch($relation["type"]){
						case "text":
							$object_data[$relation["link"]] = strval($row[$index]);
						break;
						case "int":
							$object_data[$relation["link"]] = intval($row[$index]);
						break;
						case "file":
							if(!$row[$index]) break;
							$attachements = explode("|", $row[$index]);
							foreach($attachements as $i=>$attachement){
								$attachements[$i] = UPLOAD_DIR.$this->base_path.$data_id."/".$attachement;
								if(!file_exists($attachements[$i])){
									unset($attachements[$i]);
								}
							}	
							$object_data[$relation["link"]] = $attachements;							
						break;
						default:
							$object_data[$relation["link"]] = $row[$index];
						break;					
					}
				}

				$result = $this->CI->Import_module_model->import_data($object_data, $import_data["id_object"], $import_data["relations"], $user_id, $use_moderation);
				if(!empty($result["errors"])){
					$this->fail_import_data($id, $result["errors"]);					
					$import_data["failed"]++;
				}else{
					$this->delete_import_data($id);
					$import_data["imported"]++;
				}
				if($import_data["processed"]) $import_data["processed"]--;
			}
		
			$validate_data = $this->validate_data($data_id, $import_data);
			$this->save_data($data_id, $validate_data["data"]);
		
			$return["data"]["imported"] = $import_data["imported"];
			$return["data"]["failed"] = $import_data["failed"];
		}
		
		return $return;
	}
	
	/**
	 * Return import data as array
	 * @param array $data
	 * @return array
	 */
	public function get_import_data($id_import){		
		$this->DB->select("id, data");
		$this->DB->from(IMPORT_DATA_TABLE);
		$this->DB->where("id_import", $id_import);
		$this->DB->where("errors", "");
		$this->DB->where("status", 1);
		$this->DB->limit($this->import_limit);
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			foreach($results as $r){
				$data[$r["id"]] = unserialize($r["data"]);
			}
			return $data;
		}
		return array();
	}
	
	/**
	 * Save import data
	 * @param array $data
	 * @return boolean
	 */
	public function save_import_data($id_import, $import_data){		
		foreach($import_data as $row){
			$data = array(
				"id_import"=>$id_import,
				"data"=>serialize($row),
				"status"=>0,
			);
			$this->DB->insert(IMPORT_DATA_TABLE, $data);
		}
		return true;
	}
	
	/**
	 * Approve import data
	 * @param array $data
	 * @return boolean
	 */
	public function approve_import_data($id_import){		
		$data["status"] = 1;
		$this->DB->where("id_import", $id_import);
		$this->DB->update(IMPORT_DATA_TABLE, $data);
		return true;
	}
	
	/**
	 * Decline import data
	 * @param array $data
	 * @return boolean
	 */
	public function decline_import_data($id_import){		
		$this->DB->where("id_import", $id_import);
		$this->DB->where("status", 0);
		$this->DB->delete(IMPORT_DATA_TABLE);
		return true;
	}
	
	/**
	 * Remove import data
	 * @param array $data
	 * @return boolean
	 */
	public function delete_import_data($id){		
		$this->DB->where("id", $id);
		$this->DB->delete(IMPORT_DATA_TABLE);
		return true;
	}
	
	/**
	 * Fail import data
	 * @param array $data
	 * @return boolean
	 */
	public function fail_import_data($id, $errors){		
		$data["errors"] = serialize($errors);
		$this->DB->where("id", $id);
		$this->DB->update(IMPORT_DATA_TABLE, $data);
		return true;
	}
}
