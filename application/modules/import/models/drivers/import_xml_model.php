<?php 
/**
 * XML import driver model
 * 
* @package PG_RealEstate
 * @subpackage application
 * @category	modules
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/

if(!defined("BASEPATH")) exit("No direct script access allowed");

class Import_xml_model extends Model{
	
	/**
	 * Link to CodeIgniter object
	 * @var object
	 */
	private $CI;
	
	/**
	 * Allowed file extensions
	 * @var string
	 */
	public $allowed_types = "xml|zip";
	
	/**
	 * Import file extension
	 * @var string
	 */
	public $extension = "xml";
	
	/**
	 * Two step
	 * @param boolean
	 */
	public $two_step = false;
	
	/**
	 * Driver settings
	 * @var array
	 */
	public $settings = array();
	
	/**
	 * Rows limit
	 * @var integer
	 */
	private $rows_per_request = 1;
	
	/**
	 * Elements depth
	 * @param integer
	 */
	private static $_depth = 0;
	
	/**
	 * Open items element
	 * @var boolean
	 */
	private static $_open1 = false;
	
	/**
	 * Open item element
	 * @var boolean
	 */
	private static $_open2 = false;
	
	/**
	 * Elements list
	 * @var array
	 */
	private static $_elements = array();
	
	/**
	 * Row
	 * @var array
	 */
	private static $_element = array();
	
	/**
	 * Column
	 * @var array
	 */
	private static $_column = array();
	
	/**
	 * Rows
	 * @var integer
	 */
	private static $_rows = 0;
	
	/**
	 * Constructor
	 * @return Import_xml_model
	 */
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
	}
	
	/**
	 * Get start element
	 */
	private function _start_element($parser, $name, $attrs){
		Import_xml_model::$_depth++;
		switch(Import_xml_model::$_depth){
			case 1:
				if($name == "ITEMS"){
					Import_xml_model::$_elements = array();
					Import_xml_model::$_open1 = true;					
				}
			break;
			case 2:
				if(Import_xml_model::$_open1 && $name == "ITEM"){
					Import_xml_model::$_element = array();
					Import_xml_model::$_open2 = true;					
				}
			break;
			case 3:
				if(Import_xml_model::$_open2){
					Import_xml_model::$_column = array("name"=>$name, "value"=>'');
				}
			break;
		}
		
	}
	
	/**
	 * Get end element
	 */
	private function _end_element($parser, $name){
		switch(Import_xml_model::$_depth){
			case 1:
				if(Import_xml_model::$_open1){
					Import_xml_model::$_open1 = false;
				}
			break;
			case 2:
				if(Import_xml_model::$_open2){
					Import_xml_model::$_open2 = false;
					Import_xml_model::$_elements[] = Import_xml_model::$_element;
					Import_xml_model::$_rows++;
				}			
			break;
			case 3:
				if(Import_xml_model::$_open2){		
					Import_xml_model::$_element[] = Import_xml_model::$_column;
				}
			break;
		}		
		Import_xml_model::$_depth--;
	}	
	
	/**
	 * Get data
	 */
	private function _data_element($parser, $data){
		if(Import_xml_model::$_open2 && Import_xml_model::$_depth == 3) Import_xml_model::$_column["value"] .= $data;
	}
	
	/**
	 * Analyze uploaded file
	 * @param string $filename import file name
	 * @param array $settings driver settings
	 */
	public function analyze($filename, $settings){
		$return = array("errors"=>array(), "data"=>array());
	
		$rows = 0;
		$text = "";
		$relations = array();
		
		$xml_parser = xml_parser_create();
		xml_parser_set_option($xml_parser, XML_OPTION_CASE_FOLDING, true);
		xml_set_element_handler($xml_parser, array("Import_xml_model", "_start_element"), array("Import_xml_model", "_end_element"));
		xml_set_character_data_handler($xml_parser, array("Import_xml_model", "_data_element"));
		
		$handle = fopen($filename, "rb");
		
		while(!feof($handle) && ($str=fgets($handle, 4096))){
			$text = preg_replace("/<\?xml.*\?>/im", "", $text.$str);
			$pos=strrpos($text, ">");
			if($pos === false) continue;					
			if(!xml_parse($xml_parser, $text)){
				$message = "XML Error: ".xml_error_string(xml_get_error_code($xml_parser)).
						   " at line ".xml_get_current_line_number($xml_parser);
				throw new Exception($message);
			}
			
			$text = substr($text, $pos+1);
			
			if(Import_xml_model::$_rows > 1) continue;
			
			foreach(Import_xml_model::$_element as $i=>$column){
				$relations[$i] = array("name"=>$column["name"], "type"=>"", "link"=>"");
			}
		}			
		
		fclose($handle);
	
		$return["data"]["relations"] = $relations; 
		$return["data"]["total"] = Import_xml_model::$_rows;

		return $return;
	}
	
	/**
	 * Parse uploaded file
	 * @param string $filename import file name
	 * @param array $settings driver settings
	 * @param array $relations field relations
	 * @param integer $position start position
	 * @param boolean $repeat repeat call
	 */
	public function parse($filename, $settings, $relations, $position, $repeat=false){
		$return = array("data"=>array(), "position"=>0, "status"=>1, "processed"=>0);
		
		$xml_parser = xml_parser_create();
		xml_parser_set_option($xml_parser, XML_OPTION_CASE_FOLDING, true);
		xml_set_element_handler($xml_parser, array("Import_xml_model", "_start_element"), array("Import_xml_model", "_end_element"));
		xml_set_character_data_handler($xml_parser, array("Import_xml_model", "_data_element"));
		
		$handle = fopen($filename, "rb");
		
		if(!$handle) return $return;
		
		fseek($handle, $position);
		
		if($repeat){
			Import_xml_model::$_depth++;
			Import_xml_model::$_elements = array();
			Import_xml_model::$_open1 = true;	
		}
		
		$text = "";
		while(!feof($handle) && ($str=fgets($handle, 4096))){
			$text = preg_replace("/<\?xml.*\?>/uim", "", $text.$str);
			$pos = mb_strrpos($text, ">");
			if($pos === false) continue;
			if(!xml_parse($xml_parser, mb_substr($text, 0, $pos+1))){
				$message = "XML Error: ".xml_error_string(xml_get_error_code($xml_parser)).
						   " at line ".xml_get_current_line_number($xml_parser);
				throw new Exception($message);
			}
			$text = mb_substr($text, $pos+1);
			if(Import_xml_model::$_rows < $this->rows_per_request) continue;
			$return["status"] = 0;
			break;
		}
		
		$return["position"] = ftell($handle);
		
		fclose($handle);
	
		$data = array();
		foreach(Import_xml_model::$_elements as $i=>$element){
			foreach($element as $column){
				foreach($relations as $index=>$relation){
					if($column["name"] == $relation["name"]){
						$data[$i][$index] = $column["value"];
						break;
					}
				}
			}
		}
	
		$return["data"] = $data;
		$return["processed"] = Import_xml_model::$_rows;
	
		return $return;
	}
}
