<?php
/**
* Import install model
* 
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/
class Import_install_model extends Model{
	
	/**
	 * Link to CodeIgniter object
	 * @var object
	 */
	private $CI;

	/**
	 * Menu configuration
	 * @var array
	 */
	private $menu = array(
		"admin_menu" => array(
			"action" => "none",
			"items" => array(
				"exp-import-items" => array(
					"action" => "none",
					"items" => array(
						"import_menu_item" => array("action" => "create", "link" => "admin/import/index", "status" => 1, "sorter" => 1),
					),
				),
			),			
		),
		
		"admin_import_menu" => array(
			"action" => "create",
			"name" => "Admin mode - Import listings",
			"items" => array(
				"import_data_item" => array("action" => "create", "link" => "admin/import/index", "status" => 1),
				"import_drivers_item" => array("action" => "create", "link" => "admin/import/drivers", "status" => 1),
			),
		),
	);
	
	/**
	 * Ausers configuration
	 * @var array
	 */
	private $ausers = array(
		array("module" => "import", "method" => "index", "is_default" => 1),
		array("module" => "import", "method" => "drivers", "is_default" => 0),
	);
	
	/**
	 * Driver configuration
	 * @var array
	 */
	private $drivers = array(
		array(
			"gid" => "csv", 
			"link" => "", 
			"status" => 1,
		),
		array(
			"gid" => "xml", 
			"link" => "", 
			"status" => 1,
		),
	);
	
	/**
	 * Constructor
	 *
	 * @return Install object
	 */
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
	}
	
	/**
	 * Check system requirements of module
	 */
	public function _validate_requirements(){
		$result = array("data"=>array(), "result" => true);

		//check for Mbstring
		$good			= function_exists("mb_convert_encoding");
		$result["data"][] = array(
			"name" => "Mbstring extension (required for feeds parsing) is installed",
			"value" => $good?"Yes":"No",
			"result" => $good,
		);
		$result["result"] = $result["result"] && $good;

		//check for iconv
		$good			= function_exists("iconv");
		$result["data"][] = array(
			"name" => "Iconv extension (required for feeds parsing) is installed",
			"value" => $good?"Yes":"No",
			"result" => $good,
		);
		$result["result"] = $result["result"] && $good;
		return $result;
	}

	/**
	 * Install links to menu module
	 */
	public function install_menu(){
		
		$this->CI->load->helper("menu");

		foreach($this->menu as $gid => $menu_data){
			$this->menu[$gid]["id"] = linked_install_set_menu($gid, $menu_data["action"], $menu_data["name"]);
			linked_install_process_menu_items($this->menu, "create", $gid, 0, $this->menu[$gid]["items"]);
		}
	}
	
	/**
	 * Import languages
	 * @param array $langs_ids
	 */
	public function install_menu_lang_update($langs_ids=null){
		if(empty($langs_ids)) return false;
		$langs_file = $this->CI->Install_model->language_file_read("import", "menu", $langs_ids);

		if(!$langs_file){log_message("info", "Empty menu langs data"); return false;}

		$this->CI->load->helper("menu");

		foreach($this->menu as $gid => $menu_data){
			linked_install_process_menu_items($this->menu, "update", $gid, 0, $this->menu[$gid]["items"], $gid, $langs_file);
		}
		return true;
	}

	/**
	 * Export languages
	 * @param array $langs_ids
	 */
	public function install_menu_lang_export($langs_ids){
		if(empty($langs_ids)) return false;
		$this->CI->load->helper("menu");

		$return = array();
		foreach($this->menu as $gid => $menu_data){
			$temp = linked_install_process_menu_items($this->menu, "export", $gid, 0, $this->menu[$gid]["items"], $gid, $langs_ids);
			$return = array_merge($return, $temp);
		}
		return array("menu" => $return);
	}
	
	/**
	 * Uninstall menu
	 */
	public function deinstall_menu(){
		
		$this->CI->load->helper("menu");
		foreach($this->menu as $gid => $menu_data){
			if($menu_data["action"] == "create"){
				linked_install_set_menu($gid, "delete");
			}else{
				linked_install_delete_menu_items($gid, $this->menu[$gid]["items"]);
			}
		}
	}
	
	/**
	 * Install links to ausers module
	 */
	public function install_ausers(){
		// install ausers permissions
		$this->CI->load->model("Ausers_model");
		
		foreach((array)$this->ausers as $method_data){
			//$validate_data = $this->CI->Ausers_model->validate_method($method_data, true);
			$validate_data = array("errors" => array(), "data" => $method_data);
			if(!empty($validate_data["errors"])) continue;
			$this->CI->Ausers_model->save_method(null, $validate_data["data"]);
		}
	}
	
	/**
	 * Import moderators languages
	 * @param array $langs_ids
	 */
	public function install_ausers_lang_update($langs_ids=null){
		$langs_file = $this->CI->Install_model->language_file_read("import", "ausers", $langs_ids);
		if(!$langs_file){log_message("info", "Empty ausers langs data");return false;}

		// install ausers permissions
		$this->CI->load->model("Ausers_model");
		$params["where"]["module"] = "import";
		$methods = $this->CI->Ausers_model->get_methods_lang_export($params);

		foreach($methods as $method){
			if(!empty($langs_file[$method["method"]])){
				$this->CI->Ausers_model->save_method($method["id"], array(), $langs_file[$method["method"]]);
			}
		}
	}
	
	/**
	 * Export moderators languages
	 * @param array $langs_ids
	 */
	public function install_ausers_lang_export($langs_ids){
		$this->CI->load->model("Ausers_model");
		$params["where"]["module"] = "import";
		$methods = $this->CI->Ausers_model->get_methods_lang_export($params, $langs_ids);
		foreach($methods as $method){
			$return[$method["method"]] = $method["langs"];
		}
		return array("ausers" => $return);
	}
	
	/**
	 * Uninstall links to ausers module
	 */
	public function deinstall_ausers(){
		$this->CI->load->model("Ausers_model");
		$params = array();
		$params["where"]["module"] = "import";
		$this->CI->Ausers_model->delete_methods($params);
	}	

	/**
	 * Install module actions
	 */
	public function _arbitrary_installing(){
		///// drivers
		$this->CI->load->model("import/models/Import_driver_model");
		
		foreach((array)$this->drivers as $driver_data){
			$validate_data = $this->CI->Import_driver_model->validate_driver(null, $driver_data);
			if(!empty($validate_data["errors"])) continue;
			$this->CI->Import_driver_model->save_driver(null, $validate_data["data"]);
		}
	}
	
	/**
	 * Import module languages
	 */
	public function _arbitrary_lang_install(){
		$langs_file = $this->CI->Install_model->language_file_read("import", "arbitrary");
		if(!$langs_file){log_message("info", "Empty arbitrary langs data");return false;}
	}

	/**
	 * Export module languages
	 * @param array $langs_ids
	 */
	public function _arbitrary_lang_export($langs_ids=null){
		return array();
	}

	/**
	 * Unistall module actions
	 */
	public function _arbitrary_deinstalling(){
		///// drivers
		$this->CI->load->model("import/models/Import_driver_model");
		
		foreach((array)$this->drivers as $driver_data){
			$this->CI->Import_driver_model->remove_driver($driver_data["gid"]);
		}
	}
}
