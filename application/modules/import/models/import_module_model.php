<?php  
/**
 * Import module model
 * 
 * @package PG_RealEstate
 * @subpackage application
 * @category	modules
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/

if(!defined("BASEPATH")) exit("No direct script access allowed");

define("IMPORT_MODULES_TABLE", DB_PREFIX."import_modules");

class Import_module_model extends Model{
	
	/**
	 * Link to CodeIgniter model
	 * @var object
	 */
	private $CI;
	
	/**
	 * Link to database object
	 * @var object
	 */
	private $DB;

	/**
	 * Import data
	 * @var array
	 */
	private $attrs = array(
		"id", 
		"module", 
		"model",
		"callback_get_fields",
		"callback_import_data",
		"sorter",
		"date_created", 
		"date_modified",
	);

	/**
	 * Cache module data
	 * @var array
	 */
	private $module_cache = array();

	/**
	 * Constructor
	 * @return Import_module_model
	 */
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
		$this->DB = &$this->CI->db;
	}

	/**
	 * Return module by id
	 * @param string $id module id
	 * @param boolean $formatted format results
	 * @return array
	 */
	public function get_module_by_id($id, $formatted=true){
		if(empty($this->module_cache[$id])){
			$this->DB->select(implode(", ", $this->attrs))->from(IMPORT_MODULES_TABLE)->where("id", $id);
			$results = $this->DB->get()->result_array();
			if(!empty($results) && is_array($results)){
				if($formatted){
					$this->module_cache[$id] = $this->format_module($results[0]);
				}else{
					return $results[0];
				}
			}
		}
		return $this->module_cache[$id];
	}

	/**
	 * Return list of modules as array
	 * @param boolean $formatted format results
	 * @return array
	 */
	public function get_modules($formatted=true){
		$data = array();
		$this->DB->select(implode(", ", $this->attrs))->from(IMPORT_MODULES_TABLE)->order_by('sorter');
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			foreach($results as $result){
				if($formatted){
					$data[] = $this->format_module($result);
				}else{
					$data[] = $result;
				}
			}
		}
		return $data;
	}
	
	/**
	 * Save module data
	 * @param array $data
	 * @return boolean
	 */
	public function save_module($id, $data){
		if(!$id){
			$data["date_created"] = date("Y-m-d H:i:s");
			$data["date_modified"] = date("Y-m-d H:i:s");
			$this->DB->insert(IMPORT_MODULES_TABLE, $data);		
			$id = $this->DB->insert_id();
		}else{
			$this->DB->where("id", $id);
			$this->DB->update(IMPORT_MODULES_TABLE, $data);
		}
		return $id;
	}
	
	/**
	 * Remove import module by id
	 * @param string $module_gid module guid
	 */
	public function remove_module($module_gid){
		$this->DB->where("module", $module_gid);
		$this->DB->delete(IMPORT_MODULES_TABLE);
	}
	
	/**
	 * Validate module data
	 * @param array $data data for validation
	 * @return array
	 */
	public function validate_module($id, $data){
		$return = array("errors"=> array(), "data" => array());
		
		if(isset($data['id'])){
			$return['data']['id'] = intval($data['id']);
			if(empty($return['data']['id'])) unset($return['data']['id']);
		}
		
		if(isset($data['module'])){
			$return['data']['module'] = trim(strip_tags($data["module"]));
			if(empty($return['data']['module'])) $return["errors"][] = l("error_empty_module", "import");
		}elseif(!$id){
			$return["errors"][] = l("error_empty_module", "export");
		}
		
		if(isset($data['model'])){
			$return['data']['model'] = trim(strip_tags($data["model"]));
			if(empty($return['data']['model'])) $return["errors"][] = l("error_empty_model", "import");
		}elseif(!$id){
			$return["errors"][] = l("error_empty_model", "import");
		}
		
		if(isset($data['callback_get_fields'])){
			$return['data']['callback_get_fields'] = trim(strip_tags($data["callback_get_fields"]));
			if(empty($return['data']['callback_get_fields'])) $return["errors"][] = l("error_empty_callback_get_fields", "import");
		}elseif(!$id){
			$return["errors"][] = l("error_empty_callback_get_fields", "import");
		} 
		
		if(isset($data['callback_import_data'])){
			$return['data']['callback_import_data'] = trim(strip_tags($data["callback_import_data"]));
			if(empty($return['data']['callback_import_data'])) $return["errors"][] = l("error_empty_callback_import_data", "import");
		}elseif(!$id){
			$return["errors"][] = l("error_empty_callback_import_data", "import");
		} 
		
		if(isset($data['sorter'])){
			$return['data']['sorter'] = intval($data['sorter']);
		}
		
		if(isset($data['date_created'])){
			$value = strtotime($data['date_created']);
			if($value > 0) $return['data']['date_created'] = date("Y-m-d", $value);
		}
		
		if(isset($data['date_modified'])){
			$value = strtotime($data['date_modified']);
			if($value > 0) $return['data']['date_modified'] = date("Y-m-d", $value);
		}
		
		return $return;
	}
	
	/**
	 * Format module data
	 * @param array
	 * @return array
	 */
	public function format_module($data){
		$data["output_name"] = l("module_".$data["module"], "import");
		return $data;
	}
	
	/**
	 * Return default module
	 * @return array
	 */
	public function format_default_module($module_id){
		return null;
	}
	
	/**
	 * Return custom fields which available for import
	 * @param integer $module_id module identifier
	 * @return array
	 */
	public function get_module_fields($module_id){
		$module = $this->get_module_by_id($module_id);
		if(empty($module["module"]) || empty($module["model"]) ||
		   empty($module["callback_get_fields"])) return array();
		$this->CI->load->model($module["module"]."/models/".$module["model"], $module["model"]);
		$fields = $this->CI->{$module["model"]}->{$module["callback_get_fields"]}(false);
		return $fields;
	}
	
	/**
	 * Return import data from module
	 * @param array $data selected values
	 * @param integer $module_id module identifier
	 * @param array $relations relations between fields
	 * @param integer $user_id user identifier
	 * @return array
	 */
	public function import_data($data=array(), $module_id, $relations=array(), $user_id=0, $use_moderation=false){
		$module = $this->get_module_by_id($module_id);
		if(empty($module["module"]) || empty($module["model"]) ||
		   empty($module["callback_import_data"])) return array();
		$this->CI->load->model($module["module"]."/models/".$module["model"], $module["model"]);
		return $this->CI->{$module["model"]}->{$module["callback_import_data"]}($data, $relations, $user_id);
	}
	
	/**
	 * Update languages
	 * @param array $data
	 * @param array $langs_file
	 * @param array $langs_ids
	 */
	public function update_lang($data, $langs_file, $langs_ids){
		foreach($data as $value){
			$this->CI->pg_language->pages->set_string_langs(
				"import", 
				$value, 
				$langs_file[$value], 
				$langs_ids);
		}
	}
	
	/**
	 * Export languages
	 * @param array $data
	 * @param array $langs_ids
	 */
	public function export_lang($data, $langs_ids){
		$langs = array();
		return array_merge($langs, $this->CI->pg_language->export_langs("export", (array)$data, $langs_ids));
	}
}
