<?php  
/**
 * Import driver model
 * 
 * @package PG_RealEstate
 * @subpackage application
 * @category	modules
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/

if(!defined("BASEPATH")) exit("No direct script access allowed");

define("IMPORT_DRIVERS_TABLE", DB_PREFIX."import_drivers");

class Import_driver_model extends Model{
	
	/**
	 * Link to CodeIgniter model
	 * @var object
	 */
	private $CI;
	
	/**
	 * Link to database object
	 * @var object
	 */
	private $DB;

	/**
	 * Import data
	 * @var array
	 */
	private $_fields = array(
		"id", 
		"gid", 
		"link",
		"date_created", 
		"date_modified", 
		"status",
	);

	/**
	 * Cache driver data
	 * @var array
	 */
	private $driver_cache = array();
	
	/**
	 * Field types
	 * @var array
	 */
	public $field_types = array("text", "int", "file");

	/**
	 * Driver settings
	 * @var array
	 */
	public $settings = array();

	/**
	 * Constructor
	 * @return Import_driver_model
	 */
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
		$this->DB = &$this->CI->db;
	}

	/**
	 * Return driver by gid
	 * @param string $gid driver guid
	 * @param boolean $formatted format results
	 * @return array
	 */
	public function get_driver_by_gid($gid, $formatted=true){
		if(empty($this->driver_cache[$gid])){
			$this->DB->select(implode(", ", $this->_fields))->from(IMPORT_DRIVERS_TABLE)->where("gid", $gid);
			$results = $this->DB->get()->result_array();
			if(!empty($results) && is_array($results)){
				if($formatted){
					$this->driver_cache[$gid] = $this->format_driver($results[0]);
				}else{
					return $results[0];
				}
			}
		}
		return $this->driver_cache[$gid];
	}

	/**
	 * Return list of drivers as array
	 * @param boolean $formatted format results
	 * @return array
	 */
	public function get_drivers($formatted=true){
		$data = array();
		$this->DB->select(implode(", ", $this->_fields))->from(IMPORT_DRIVERS_TABLE);
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			foreach($results as $result){
				if($formatted){
					$data[] = $this->format_driver($result);
				}else{
					$data[] = $result;
				}
			}
		}
		return $data;
	}
	
	/**
	 * Save driver data
	 * @param array $data
	 * @return boolean
	 */
	public function save_driver($gid, $data){
		if(!$gid){
			$data["date_created"] = date("Y-m-d H:i:s");
			$data["date_modified"] = date("Y-m-d H:i:s");
			$this->DB->insert(IMPORT_DRIVERS_TABLE, $data);		
		}else{
			$this->DB->where("gid", $gid);
			$this->DB->update(IMPORT_DRIVERS_TABLE, $data);
		}
	}
	
	/**
	 * Remove import driver by gid
	 * @param integer $driver_gid driver guid
	 */
	public function remove_driver($driver_gid){
		$this->DB->where("gid", $driver_gid);
		$this->DB->delete(IMPORT_DRIVERS_TABLE);
	}
	
	/**
	 * Activate driver
	 * @param string $driver_gid driver guid
	 */
	public function activate_driver($driver_gid){
		$data["gid"]	= $driver_gid;	
		$data["status"] = "1";
		$this->save_driver($data);
	}
	
	/**
	 * Deactivate driver
	 * @param string $driver_gid driver guid
	 */
	public function deactivate_driver($driver_gid){
		$data["gid"]	= $driver_gid;	
		$data["status"] = "0";
		$this->save_driver($data);
	}

	/**
	 * Validate driver data
	 * @param array $data data for validation
	 * @return array
	 */
	public function validate_driver($gid, $data){
		$return = array("errors"=> array(), "data" => array());
		
		if(isset($data['id'])){
			$return['data']['id'] = intval($data['id']);
			if(empty($return['data']['id'])) unset($return['data']['id']);
		}
		
		if(isset($data['gid'])){
			$return['data']['gid'] = trim(strip_tags($data["gid"]));
			if(empty($return['data']['gid'])) $return["errors"][] = l("error_empty_gid", "export");
		}elseif(!$gid){
			$return["errors"][] = l("error_empty_gid", "export");
		}
		
		if(isset($data['link'])){
			$return['data']['link'] = trim(strip_tags($data["link"]));
			if(empty($return['data']['link'])) unset($return['data']['link']);
		}
		
		if(isset($data['date_created'])){
			$value = strtotime($data['date_created']);
			if($value > 0) $return['data']['date_created'] = date("Y-m-d", $value);
		}
		
		if(isset($data['date_modified'])){
			$value = strtotime($data['date_modified']);
			if($value > 0) $return['data']['date_modified'] = date("Y-m-d", $value);
		}
		
		if(isset($data["status"])){
			$return["data"]["status"] = intval($data["status"]);
		}
		
		return $return;
	}
	
	/**
	 * Format data
	 * @param array
	 * @return array
	 */
	public function format_driver($data){
		$data["output_name"] = l("driver_name_".$data["gid"], "import");
		return $data;
	}
	
	/**
	 * Return default driver
	 * @return array
	 */
	public function format_default_driver($driver_gid){
		return null;
	}
	
	/**
	 * Parse uploaded file
	 * @param string $filename import file name
	 * @param array $settings driver settings
	 * @param integer $position start position
	 */
	public function parse($filename, $settings, $position){
		
	}
}
