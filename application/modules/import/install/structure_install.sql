DROP TABLE IF EXISTS `[prefix]import`;
CREATE TABLE IF NOT EXISTS `[prefix]import` (
  `id` smallint(5) NOT NULL auto_increment,
  `gid_driver` varchar(50) NOT NULL,
  `id_object` smallint(5) NOT NULL,
  `settings` text NULL,
  `relations` text NULL,
  `filename` varchar(255) NOT NULL,
  `position` int(3) NOT NULL,
  `total` int(3) NOT NULL,
  `processed` int(3) NOT NULL,
  `imported` int(3) NOT NULL,
  `failed` int(3) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `gid_driver` (`gid_driver`, `id_object`)  
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `[prefix]import_data`;
CREATE TABLE IF NOT EXISTS `[prefix]import_data` (
  `id` smallint(5) NOT NULL auto_increment,
  `id_import` varchar(50) NOT NULL,
  `data` mediumtext NULL,
  `errors` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_import` (`id_import`)  
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `[prefix]import_drivers`;
CREATE TABLE IF NOT EXISTS `[prefix]import_drivers` (
  `id` tinyint(3) NOT NULL auto_increment,
  `gid` varchar(50) NOT NULL,
  `link` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `gid` (`gid`)  
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `[prefix]import_modules`;
CREATE TABLE IF NOT EXISTS `[prefix]import_modules` (
  `id` smallint(5) NOT NULL auto_increment,
  `module` varchar(50) NOT NULL,
  `model` varchar(50) NOT NULL,
  `callback_get_fields` varchar(50) NOT NULL,
  `callback_import_data` varchar(50) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `sorter` tinyint(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
