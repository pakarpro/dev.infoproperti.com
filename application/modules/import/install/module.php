<?php

$module["module"] = "import";
$module["install_name"] = "Import";
$module["install_descr"] = "Content import";
$module["version"] = "1.01";
$module["files"] = array(
	array("file", "read", "application/modules/import/controllers/admin_import.php"),
	array("file", "read", "application/modules/import/controllers/api_import.php"),
	array("file", "read", "application/modules/import/controllers/import.php"),
	array("file", "read", "application/modules/import/install/module.php"),
	array("file", "read", "application/modules/import/install/permissions.php"),
	array("file", "read", "application/modules/import/install/structure_deinstall.sql"),
	array("file", "read", "application/modules/import/install/structure_install.sql"),
	array("file", "read", "application/modules/import/js/import.js"),
	array("file", "read", "application/modules/import/models/drivers/import_csv_model.php"),
	array("file", "read", "application/modules/import/models/drivers/import_xml_model.php"),
	array("file", "read", "application/modules/import/models/import_driver_model.php"),
	array("file", "read", "application/modules/import/models/import_install_model.php"),
	array("file", "read", "application/modules/import/models/import_model.php"),
	array("file", "read", "application/modules/import/models/import_module_model.php"),
	array("file", "read", "application/modules/import/views/admin/drivers_list.tpl"),
	array("file", "read", "application/modules/import/views/admin/edit.tpl"),
	array("file", "read", "application/modules/import/views/admin/list.tpl"),
	array("file", "read", "application/modules/import/views/admin/relations.tpl"),
	array("file", "read", "application/modules/import/views/admin/upload_form_settings.tpl"),
	array("file", "read", "application/modules/import/views/admin/upload_form.tpl"),
	array("dir", "read", "application/modules/import/langs"),
);

$module["dependencies"] = array(
	"start" => array("version"=>"1.01"),
	"menu" => array("version"=>"1.01"),
);

$module["linked_modules"] = array(
	"install" => array(
		"menu"			=> "install_menu",
		"ausers"		=> "install_ausers",
	),
	"deinstall" => array(
		"menu"			=> "deinstall_menu",
		"ausers"		=> "deinstall_ausers",
	)
);

