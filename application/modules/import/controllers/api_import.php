<?php
/**
* Import api side controller
* 
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/
class Api_import extends Controller{
	
	/**
	 * Constructor
	 *
	 * @return Api_import
	 */
	public function __construct(){
		parent::Controller();
	}
	
	/**
	 * Render import form action
	 * @param integer $selection_id selection identifier
	 */
	public function generate($selection_id){
		if($this->session->userdata("auth_type") != "user"){show_404();	return;}
		
		$this->load->model("import/models/Import_module_model");
		$this->load->model("Import_model");

		$data = $this->Import_model->get_selection_by_id($selection_id, true);
		if($data["output_type"] != "browser" || !$data["published"] || $data["editable"]){show_404();return;}		

		$user_id = $this->session->userdata("user_id");
		$this->Import_model->generate_selection($selection_id, array(), $user_id);
	}
}
