function Import(optionArr){
	this.properties = {
		siteUrl: '',
		parseId: 'parse_link',
		makeId: 'make_link',
		idForParse: 'processed',
		idForMake: 'imported',
		urlParse: 'admin/import/ajax_parse/',
		urlImport: 'admin/import/ajax_make/',
		importId: 0,
		twoPass: false,
		errorObj: new Errors,
	};

	var _self = this;

	this.Init = function(options){
		_self.properties = $.extend(_self.properties, options);		
		if(_self.twoPass){
			$('#'+_self.properties.parseId).bind('click', function(){
				_self.parse_data();
				return false;
			});
		}
		$('#'+_self.properties.makeId).bind('click', function(){
			_self.make_data();
			return false;
		});
	}
	
	this.parse_data = function(){
		$.ajax({
			url: _self.properties.siteUrl + _self.properties.urlParse + _self.properties.importId,
			type: 'POST',
			dataType: 'json',
			cache: false,
			success: function(data){alert(2);
				if(typeof(data.error) != 'undefined' && data.error != ''){
					_self.properties.errorObj.show_error_block(data.error, 'error');
				}else{
					
				}
			}
		});		
		return false;
	}
	
	this.make_data = function(){
		$.ajax({
			url: _self.properties.siteUrl + _self.properties.urlImport + _self.properties.importId,
			type: 'POST',
			dataType: 'json',
			cache: false,
			success: function(data){
				$('#'+_self.properties.idForMake).html(data.total);
				if(data.total){
					_self.make_data();
					return;
				}
					
				if(typeof(data.error) != 'undefined' && data.error != ''){
					_self.properties.errorObj.show_error_block(data.error, 'error');
				}else{
					_self.properties.errorObj.show_error_block(data.success, 'success');
				}
			}
		});		
		return false;
	}

	_self.Init(optionArr);

}
