<?php
/**
* Content install model
*
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/
class Content_install_model extends Model
{
	private $CI;
	private $menu = array(
		'admin_menu' => array(
			'action' => 'none',
			'items' => array(
				'settings_items' => array(
					'action' => 'none',
					'items' => array(
						'content_items' => array(
							'action'=>'none',
							'items' => array(
								'content_menu_item' => array('action' => 'create', 'link' => 'admin/content', 'status' => 1, 'sorter' => 3),
							)
						),
						'interface-items' => array(
							'action'=>'none',
							'items' => array(
								'promo_menu_item' => array('action' => 'create', 'link' => 'admin/content/promo', 'status' => 1, 'sorter' => 5)
							)
						)
					)
				)
			)
		),
		
		'user_footer_menu' => array(
			'action' => 'none',
			'items' => array(
				'footer-menu-about-us-item' => array('action' => 'create', 'link' => 'content/view/about_us', 'status' => 1, 'sortet' => 1),
				'footer-menu-advertise-item' => array('action' => 'create', 'link' => 'content/view/advertising', 'status' => 1, 'sortet' => 3),
				'footer-menu-terms-item' => array('action' => 'create', 'link' => 'content/view/legal-terms', 'status' => 1, 'sortet' => 4),
			)
		),
		
		'private_main_menu' => array(
			'action' => 'none',
			'items' => array(				
				'private-main-help-item' => array('action' => 'create', 'link' => 'content/view/help', 'status' => 1, 'sorter' => 11),
			),
		),
		'company_main_menu' => array(
			'action' => 'none',
			'items' => array(
				'company-main-help-item' => array('action' => 'create', 'link' => 'content/view/help', 'status' => 1, 'sorter' => 11),
			),
		),
		
		'agent_main_menu' => array(
			'action' => 'none',
			'items' => array(
				'agent-main-help-item' => array('action' => 'create', 'link' => 'content/view/help', 'status' => 1, 'sorter' => 11),
			),
		),
		'guest_main_menu' => array(
			'action' => 'none',
			'items' => array(
				'guest-main-help-item' => array('action' => 'create', 'link' => 'content/view/help', 'status' => 1, 'sorter' => 11)
			),
		)
	);
	private $ausers_methods = array(
		array('module' => 'content', 'method' => 'index', 'is_default' => 1),
		array('module' => 'content', 'method' => 'promo', 'is_default' => 0)
	);
	private $dynamic_blocks = array(
		array(
			'gid'		=> 'content_promo_block',
			'module'	=> 'content',
			'model'		=> 'Content_promo_model',
			'method'	=> '_dynamic_block_get_content_promo',
			'params'	=> '',
			'views'		=> 'a:1:{i:0;s:7:"default";}',
		),
		array(
			'gid'		=> 'info_pages_block',
			'module'	=> 'content',
			'model'		=> 'Content_model',
			'method'	=> '_dynamic_block_get_info_pages',
			'params'	=> 'a:1:{s:7:"keyword";s:0:"";}',
			'views'		=> 'a:1:{i:0;s:7:"default";}'
		),
	);

	/**
	 * Constructor
	 *
	 * @return Install object
	 */
	function Content_install_model()
	{
		parent::Model();
		$this->CI = & get_instance();
		$this->CI->load->model('Install_model');
	}

	public function install_menu() {
		$this->CI->load->helper('menu');
		foreach($this->menu as $gid => $menu_data){
			$this->menu[$gid]['id'] = linked_install_set_menu($gid, $menu_data["action"], $menu_data["name"]);
			linked_install_process_menu_items($this->menu, 'create', $gid, 0, $this->menu[$gid]["items"]);
		}
	}

	public function install_menu_lang_update($langs_ids = null) {
		if(empty($langs_ids)) return false;
		$langs_file = $this->CI->Install_model->language_file_read('content', 'menu', $langs_ids);

		if(!$langs_file) { log_message('info', 'Empty menu langs data'); return false; }

		$this->CI->load->helper('menu');

		foreach($this->menu as $gid => $menu_data){
			linked_install_process_menu_items($this->menu, 'update', $gid, 0, $this->menu[$gid]["items"], $gid, $langs_file);
		}
		return true;
	}

	public function install_menu_lang_export($langs_ids) {
		if(empty($langs_ids)) return false;
		$this->CI->load->helper('menu');

		$return = array();
		foreach($this->menu as $gid => $menu_data){
			$temp = linked_install_process_menu_items($this->menu, 'export', $gid, 0, $this->menu[$gid]["items"], $gid, $langs_ids);
			$return = array_merge($return, $temp);
		}
		return array( "menu" => $return );
	}

	public function deinstall_menu() {
		$this->CI->load->helper('menu');
		foreach($this->menu as $gid => $menu_data){
			if($menu_data['action'] == 'create'){
				linked_install_set_menu($gid, 'delete');
			}else{
				linked_install_delete_menu_items($gid, $this->menu[$gid]['items']);
			}
		}
	}

	public function install_site_map() {
		///// site map
		$this->CI->load->model('Site_map_model');
		$site_map_data = array(
			'module_gid' => 'content',
			'model_name' => 'Content_model',
			'get_urls_method' => 'get_sitemap_urls',
		);
		$this->CI->Site_map_model->set_sitemap_module('content', $site_map_data);
	}

	public function install_banners() {
		///// add banners module
		$this->CI->load->model('Content_model');
		$this->CI->load->model('banners/models/Banner_group_model');
		$this->CI->load->model('banners/models/Banner_place_model');
		$this->CI->Banner_group_model->set_module("content", "Content_model", "_banner_available_pages");

		$group_id = $this->CI->Banner_group_model->get_group_id_by_gid('content_groups');
		///add pages in group
		$pages = $this->CI->Content_model->_banner_available_pages();
		if ($pages){
			foreach($pages  as $key => $value){
				$page_attrs = array(
					'group_id' => $group_id,
					'name' => $value['name'],
					'link' => $value['link'],
				);
				$this->CI->Banner_group_model->add_page($page_attrs);
			}
		}
	}

	/**
	 * Ausers module methods
	 */
	public function install_ausers() {
		// install ausers permissions
		$this->CI->load->model('Ausers_model');

		foreach($this->ausers_methods as $method){
			$this->CI->Ausers_model->save_method(null, $method);
		}
	}

	public function install_ausers_lang_update($langs_ids = null) {
		$langs_file = $this->CI->Install_model->language_file_read('content', 'ausers', $langs_ids);

		// install ausers permissions
		$this->CI->load->model('Ausers_model');
		$params['where']['module'] = 'content';
		$methods = $this->CI->Ausers_model->get_methods_lang_export($params);

		foreach($methods as $method){
			if(!empty($langs_file[$method['method']])){
				$this->CI->Ausers_model->save_method($method['id'], array(), $langs_file[$method['method']]);
			}
		}
	}

	public function install_ausers_lang_export($langs_ids) {
		$this->CI->load->model('Ausers_model');
		$params['where']['module'] = 'content';
		$methods =  $this->CI->Ausers_model->get_methods_lang_export($params, $langs_ids);
		foreach($methods as $method){
			$return[$method['method']] = $method['langs'];
		}
		return array('ausers' => $return);
	}

	public function deinstall_ausers() {
		// delete moderation methods in ausers
		$this->CI->load->model('Ausers_model');
		$params['where']['module'] = 'contents';
		$this->CI->Ausers_model->delete_methods($params);
	}

	public function install_uploads() {
		///// add uploads configs for promo
		$this->CI->load->model('uploads/models/Uploads_config_model');
		$config_data = array(
			'gid' => 'promo-content-img',
			'name' => 'Promo content image',
			'max_height' => 2000,
			'max_width' => 1200,
			'max_size' => 1000000,
			'name_format' => 'generate',
			'file_formats' => 'a:3:{i:0;s:3:"jpg";i:1;s:3:"gif";i:2;s:3:"png";}',
			'default_img' => '',
			'date_add' => date('Y-m-d H:i:s'),
		);
		$this->CI->Uploads_config_model->save_config(null, $config_data);
	}

	public function install_file_uploads() {
		$this->CI->load->model('file_uploads/models/File_uploads_config_model');
		$config_data = array(
			'gid' => 'promo-content-flash',
			'name' => 'Promo content flash',
			'max_size' => 1000000,
			'name_format' => 'generate',
			'file_formats' => 'a:1:{i:0;s:3:"swf";}',
			'date_add' => date('Y-m-d H:i:s'),
		);
		$this->CI->File_uploads_config_model->save_config(null, $config_data);
	}

	public function install_social_networking() {
		///// add social netorking page
		$this->CI->load->model('social_networking/models/Social_networking_pages_model');
		$page_data = array(
			'controller' => 'content',
			'method' => 'view',
			'name' => 'View content page',
			'data' => 'a:3:{s:4:"like";a:3:{s:8:"facebook";s:2:"on";s:9:"vkontakte";s:2:"on";s:6:"google";s:2:"on";}s:5:"share";a:4:{s:8:"facebook";s:2:"on";s:9:"vkontakte";s:2:"on";s:8:"linkedin";s:2:"on";s:7:"twitter";s:2:"on";}s:8:"comments";s:1:"1";}',
		);
		$this->CI->Social_networking_pages_model->save_page(null, $page_data);
	}

	function _arbitrary_installing(){
		///// seo
		$seo_data = array(
			'module_gid' => 'content',
			'model_name' => 'Content_model',
			'get_settings_method' => 'get_seo_settings',
			'get_rewrite_vars_method' => 'request_seo_rewrite',
			'get_sitemap_urls_method' => 'get_sitemap_xml_urls',
		);
		$this->CI->pg_seo->set_seo_module('content', $seo_data);

		///// add entries for lang data updates
		$lang_dm_data = array(
			'module' => 'content',
			'model' => 'Content_promo_model',
			'method_add' => 'lang_dedicate_module_callback_add',
			'method_delete' => 'lang_dedicate_module_callback_delete'
		);
		$this->CI->pg_language->add_dedicate_modules_entry($lang_dm_data);

		$this->CI->load->model("content/models/Content_promo_model");
		foreach ($this->CI->pg_language->languages as $id => $value){
			$this->CI->Content_promo_model->lang_dedicate_module_callback_add($value['id']);
		}
	}
	
	public function _arbitrary_lang_install($langs_ids = null) {
		if(!is_array($langs_ids)) $langs_ids = (array)$langs_ids;
		$langs_file = $this->CI->Install_model->language_file_read('content', 'demo', $langs_ids);

		if(!$langs_file) {
			log_message('info', 'Empty arbitrary langs data');
			return false;
		}

		$this->CI->load->model("content/models/Content_promo_model");
		foreach($langs_ids as $lang_id){
			$promo["promo_text"] = $langs_file["content"][$lang_id];
			$this->CI->Content_promo_model->save_promo($lang_id, $promo);
		}
		return;
	}

	public function _arbitrary_lang_export($langs_ids = null) {
		if(!is_array($langs_ids)) $langs_ids = (array)$langs_ids;
		$this->CI->load->model("content/models/Content_promo_model");

		foreach($langs_ids as $lang_id){
			$promo = $this->CI->Content_promo_model->get_promo($lang_id);
			$langs["content"][$lang_id] = $promo["promo_text"];
		}
		return array('demo' => $langs);
	}

	public function deinstall_site_map() {
		$this->CI->load->model('Site_map_model');
		$this->CI->Site_map_model->delete_sitemap_module('content');
	}

	public function deinstall_banners() {
		///// delete banners module
		$this->CI->load->model("banners/models/Banner_group_model");
		$this->CI->Banner_group_model->delete_module("content");
	}

	public function deinstall_uploads() {
		///// remove upload config
		$this->CI->load->model('uploads/models/Uploads_config_model');
		$config_data = $this->CI->Uploads_config_model->get_config_by_gid('promo-content-img');
		if(!empty($config_data["id"])){
			$this->CI->Uploads_config_model->delete_config($config_data["id"]);
		}
	}

	public function deinstall_file_uploads() {
		$this->CI->load->model('file_uploads/models/File_uploads_config_model');
		$config_data = $this->CI->File_uploads_config_model->get_config_by_gid('promo-content-flash');
		if(!empty($config_data["id"])){
			$this->CI->File_uploads_config_model->delete_config($config_data["id"]);
		}
	}

	public function deinstall_social_networking() {
		///// delete social netorking page
		$this->CI->load->model('social_networking/models/Social_networking_pages_model');
		$this->CI->Social_networking_pages_model->delete_pages_by_controller('content');
	}

	function _arbitrary_deinstalling(){
		$this->CI->pg_seo->delete_seo_module('content');

		/// delete entries in dedicate modules
		$lang_dm_data['where'] = array(
			'module' => 'content',
			'model' => 'Content_promo_model',
		);
		$this->CI->pg_language->delete_dedicate_modules_entry($lang_dm_data);
	}

	public function install_dynamic_blocks() {
		$this->CI->load->model('Dynamic_blocks_model');

		foreach($this->dynamic_blocks as $block) {
			$block_data = array(
				'gid'		=> $block['gid'],
				'module'	=> $block['module'],
				'model'		=> $block['model'],
				'method'	=> $block['method'],
				'params'	=> $block['params'],
				'views'		=> $block['views']
			);
			$id_block = $this->CI->Dynamic_blocks_model->save_block(null, $block_data);
			if(!empty($block['area'])) {
				$area_index = $this->CI->Dynamic_blocks_model->get_area_by_gid($block['area']['gid']);
				$area_data = array(
					'id_area' => $area_index['id'],
					'id_block' => $id_block,
					'params' => $block['area']['params'],
					'view_str' => $block['area']['view_str'],
					'cache_time' => $block['area']['cache_time']
				);
				$temp_id = $this->CI->Dynamic_blocks_model->save_area_block(null, $area_data);
				$this->CI->Dynamic_blocks_model->save_area_block_sorter($temp_id, $block['area']['sorter']);
			}
		}
	}

	public function install_dynamic_blocks_lang_update($langs_ids = null) {
		if(empty($langs_ids)) return false;
		$langs_file = $this->CI->Install_model->language_file_read('content', 'dynamic_blocks', $langs_ids);
		if(!$langs_file) { log_message('info', 'Empty dynamic_blocks langs data'); return false; }
		$this->CI->load->model('Dynamic_blocks_model');
		$data = array();
		
		foreach((array)$this->dynamic_blocks as $block_data){
			$block = $this->CI->Dynamic_blocks_model->get_block_by_gid($block_data["gid"]);
			if(!$block) continue;
			$data[] = $block;
		}
		
		$this->CI->Dynamic_blocks_model->update_langs($data, $langs_file, $langs_ids);
	}

	public function install_dynamic_blocks_lang_export($langs_ids = null) {
		if(!is_array($langs_ids)) $langs_ids = (array)$langs_ids;
		$this->CI->load->model("Dynamic_blocks_model");
		$data = array();
		
		foreach((array)$this->dynamic_blocks as $block_data){
			$block = $this->CI->Dynamic_blocks_model->get_block_by_gid($block_data["gid"]);
			if(!$block) continue;
			$data[] = $block;
		}
		
		$langs = $this->CI->Dynamic_blocks_model->export_langs($data, $langs_ids);
		return array("dynamic_blocks" => $langs);
	}

	public function deinstall_dynamic_blocks(){
		$this->CI->load->model('Dynamic_blocks_model');
		foreach($this->dynamic_blocks as $block) {
			$this->CI->Dynamic_blocks_model->delete_block_by_gid($block['gid']);
		}
	}	
}
