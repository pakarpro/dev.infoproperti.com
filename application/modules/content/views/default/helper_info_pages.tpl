{if $pages}
<h2>{if $section.title}{$section.title}{else}{l i='header_info_pages' gid='content'}{/if}</h2>
<div class="info_pages_block">
	{foreach item=item key=key from=$pages}
	<div class="info_page">
		<a href="{seolink module='content' method='view' data=$item}">{$item.title|truncate:100}</a><br>
		{$item.content|truncate:200}<br>
		<a href="{seolink module='content' method='view' data=$item}">{l i='link_view_more' gid='content'}</a>
	</div>
	{/foreach}
	<div class="clr"></div>
</div>
{/if}
