{include file="header.tpl"}

<form method="post" action="" name="save_form">
	<div class="edit-form n150">
		<div class="row header">{if $data.id}{l i='admin_header_page_change' gid='content'}{else}{l i='admin_header_page_add' gid='content'}{/if}</div>
		{if $data.id}
		<div class="row">
			<div class="h">{l i='field_view_link' gid='content'}: </div>
			<div class="v"><a href="{$site_url}content/view/{$data.gid}">{$site_url}content/view/{$data.gid}</a>&nbsp;</div>
		</div>
		{/if}
		<div class="row zebra">
			<div class="h">{l i='field_lang' gid='content'}: </div>
			<div class="v">{$languages[$current_lang].name}</div>
		</div>
		<div class="row">
			<div class="h">{l i='field_gid' gid='content'}:&nbsp;* </div>
			<div class="v"><input type="text" value="{$data.gid}" name="gid"></div>
		</div>
		<div class="row zebra">
			<div class="h">{l i='field_title' gid='content'}:&nbsp;* </div>
			<div class="v"><input type="text" value="{$data.title}" name="title" class="long"></div>
		</div>
		<div class="row content">
			{$data.content_fck}
		</div>

	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="{l i='btn_save' gid='start' type='button'}"></div></div>
	<a class="cancel" href="{$site_url}admin/content/index/{$current_lang}">{l i='btn_cancel' gid='start'}</a>
</form>
<div class="clr"></div>
{include file="footer.tpl"}
