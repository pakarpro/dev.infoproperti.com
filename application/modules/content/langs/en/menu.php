<?php

$install_lang["admin_menu_settings_items_content_items_content_menu_item"] = "Info pages";
$install_lang["admin_menu_settings_items_content_items_content_menu_item_tooltip"] = "Add, translate & sort info pages";
$install_lang["admin_menu_settings_items_interface-items_promo_menu_item"] = "Promo block";
$install_lang["admin_menu_settings_items_interface-items_promo_menu_item_tooltip"] = "Promo image, text, flash on site front page";
$install_lang["agent_main_menu_agent-main-help-item"] = "Help";
$install_lang["agent_main_menu_agent-main-help-item_tooltip"] = "";
$install_lang["company_main_menu_company-main-help-item"] = "Help";
$install_lang["company_main_menu_company-main-help-item_tooltip"] = "";
$install_lang["guest_main_menu_guest-main-help-item"] = "Help";
$install_lang["guest_main_menu_guest-main-help-item_tooltip"] = "";
$install_lang["private_main_menu_private-main-help-item"] = "Help";
$install_lang["private_main_menu_private-main-help-item_tooltip"] = "";
$install_lang["user_footer_menu_footer-menu-about-us-item"] = "About us";
$install_lang["user_footer_menu_footer-menu-about-us-item_tooltip"] = "";
$install_lang["user_footer_menu_footer-menu-advertise-item"] = "Advertise";
$install_lang["user_footer_menu_footer-menu-advertise-item_tooltip"] = "";
$install_lang["user_footer_menu_footer-menu-terms-item"] = "Terms of use";
$install_lang["user_footer_menu_footer-menu-terms-item_tooltip"] = "";

