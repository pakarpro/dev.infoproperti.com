<?php
$module['module'] = 'services';
$module['install_name'] = 'Services';
$module['install_descr'] = 'Services settings & logs';
$module['version'] = '1.02';
$module['files'] = array(
	array('file', 'read', "application/modules/services/controllers/admin_services.php"),
	array('file', 'read', "application/modules/services/controllers/api_services.php"),
	array('file', 'read', "application/modules/services/controllers/services.php"),
	array('file', 'read', "application/modules/services/install/module.php"),
	array('file', 'read', "application/modules/services/install/permissions.php"),
	array('file', 'read', "application/modules/services/install/settings.php"),
	array('file', 'read', "application/modules/services/install/structure_deinstall.sql"),
	array('file', 'read', "application/modules/services/install/structure_install.sql"),
	array('file', 'read', "application/modules/services/models/services_install_model.php"),
	array('file', 'read', "application/modules/services/models/services_model.php"),
	array('file', 'read', "application/modules/services/views/admin/block_service_param.tpl"),
	array('file', 'read', "application/modules/services/views/admin/edit_templates.tpl"),
	array('file', 'read', "application/modules/services/views/admin/edit.tpl"),
	array('file', 'read', "application/modules/services/views/admin/list_templates.tpl"),
	array('file', 'read', "application/modules/services/views/admin/list.tpl"),
	array('file', 'read', "application/modules/services/views/default/service_form.tpl"),
	array('dir', 'read', 'application/modules/services/langs'),
);

$module['dependencies'] = array(
	'start' => array('version'=>'1.01'),
	'menu' => array('version'=>'1.01'),
	'payments' => array('version'=>'1.01'),
	'users' => array('version'=>'1.01')
);

$module['linked_modules'] = array(
	'install' => array(
		'menu'		=> 'install_menu',
		'payments'	=> 'install_payments'
	),
	'deinstall' => array(
		'menu'		=> 'deinstall_menu',
		'payments'	=> 'deinstall_payments'
	)
);
