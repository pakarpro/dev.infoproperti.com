DROP TABLE IF EXISTS `[prefix]services`;
CREATE TABLE IF NOT EXISTS `[prefix]services` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `gid` varchar(40) NOT NULL,
  `template_gid` varchar(40) NOT NULL,
  `pay_type` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `price` float NOT NULL,
  `data_admin` text NOT NULL,
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `gid` (`gid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `[prefix]services_log`;
CREATE TABLE IF NOT EXISTS `[prefix]services_log` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `id_user` int(3) NOT NULL,
  `id_service` int(3) NOT NULL,
  `user_data` text NOT NULL,
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`),
  KEY `id_service` (`id_service`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `[prefix]services_templates`;
CREATE TABLE IF NOT EXISTS `[prefix]services_templates` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `gid` varchar(40) NOT NULL,
  `callback_module` varchar(50) NOT NULL,
  `callback_model` varchar(50) NOT NULL,
  `callback_method` varchar(150) NOT NULL,
  `callback_validate_method` varchar(150) NOT NULL,
  `price_type` tinyint(3) NOT NULL,
  `data_admin` text NOT NULL,
  `data_user` text NOT NULL,
  `date_add` datetime NOT NULL,
  `moveable` tinyint(3) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `gid` (`gid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
