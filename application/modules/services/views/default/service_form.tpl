{include file="header.tpl"}
{include file="left_panel.tpl" module="start"}
<div class="rc">
	<div class="content-block">
        <h1>{l i='header_service_settings' gid='services'}: {$data.name}</h1>
    
        <div class="content-value">
            {*<p>{l i='text_service_form' gid='services'}</p>*}
        
            <form method="post" action="">
                <div class="edit_block">
                    <div class="payment_table">
                        <table>
                        {foreach item=item from=$data.template.data_admin_array}
                            <tr>
                                <td>{$item.name}:</td>
                                <td class="value">
                                {if $item.type eq 'string' || $item.type eq 'int' || $item.type eq 'text'}
                                    {$item.value}
                                {elseif $item.type eq 'price'}
                                    {block name=currency_format_output module=start value=$item.value cur_gid=$base_currency.gid}
                                {elseif $item.type eq 'checkbox'}
                                    {if $item.value eq '1'}{l i='yes_checkbox_value' gid='services'}{else}{l i='no_checkbox_value' gid='services'}{/if}
                                {/if}
                                </td>
                            </tr>
                        {/foreach}
                 
                        {foreach item=item from=$data.template.data_user_array}
                        {if $item.type eq 'hidden'}
                        <input type="hidden" value="{$item.value}" name="data_user[{$item.gid}]">
                        {else}
                            <tr>
                                <td>{$item.name}:</td>
                                {if $item.type eq 'string'}
                                <td class="value"><input type="text" value="{$item.value}" name="data_user[{$item.gid}]"></td>
                                {elseif $item.type eq 'int'}
                                <td class="value"><input type="text" value="{$item.value}" name="data_user[{$item.gid}]" class="short"></td>
                                {elseif $item.type eq 'price'}
                                <td class="value"><input type="text" value="{$item.value}" name="data_user[{$item.gid}]" class="short"> {block name=currency_format_output module=start cur_gid=$base_currency.gid}</td>
                                {elseif $item.type eq 'text'}
                                <td class="value"class="value"><textarea name="data_user[{$item.gid}]">{$item.value}</textarea></td>
                                {elseif $item.type eq 'checkbox'}
                                <td class="value"><input type="checkbox" value="1" name="data_user[{$item.gid}]" {if $item.value eq '1'}checked{/if}></td>
                                {/if}
                            </tr>
                        {/if}
                        {/foreach}
                        
                        {if $data.template.price_type eq '2' }
                            <tr>
                                <td>{l i='field_your_price' gid='services'}:</td>
                                <td class="value"><input type="text" value="{$data.price}" name="price" class="short"> <b>{block name=currency_format_output module=start cur_gid=$base_currency.gid}</b></td>
                            </tr>
                        {elseif $data.template.price_type eq '3' }
                            <tr>
                                <td>{l i='field_price' gid='services'}:</td>
                                <td class="value"><input type="hidden" value="{$data.price}" name="price" class="short"><b>{block name=currency_format_output module=start value=$data.price cur_gid=$base_currency.gid}</b></td>
                            </tr>
                        {else}
                            <tr>
                                <td>{l i='field_price' gid='services'}:</td>
                                <td class="value"><b>{block name=currency_format_output module=start value=$data.price cur_gid=$base_currency.gid}</b></td>
                            </tr>
                        {/if}
                        </table>
                    </div>
                </div>
                
                {if $data.gid == 'listings_slider_service'}
                	{if $data.count_photo == '0'}
                    	<h2 class="line top bottom">Anda belum mengupload gambar untuk listing Anda. Silahkan klik <a href="{$site_url}listings/edit/{$item.value}/gallery">link</a> berikut untuk mengupload gambar.</h2>
                    {else}
                        {if $data.free_activate}
                        <input type="submit" class='btn' value="{l i='btn_activate_free' gid='services' type='button'}" name="btn_account">
                        {else}
                            {if ($data.pay_type eq 1 || $data.pay_type eq 2) && $is_module_installed}
                                <h2 class="line top bottom">{l i='account_payment' gid='services'}</h2>
                                {if $data.disable_account_pay}{l i='error_account_less_then_service_price' gid='services'} <a href="{seolink module='users' method='account'}">{l i="link_add_founds" gid='services'}</a>
                                {else}
                                    {l i='on_your_account_now' gid='services'}: <b>{block name=currency_format_output module=start value=$data.user_account cur_gid=$base_currency.gid}</b>
                                    <div class="b outside">
                                        <input type="submit" value="{l i='btn_pay_account' gid='services' type='button'}" name="btn_account">
                                    </div>
                                {/if}
                            {/if}
                            {*if $data.pay_type eq 2 && $billing_systems}
                                <br>{l i='payment_type_or' gid='services'}<br>
                            {/if*}
                            {if $data.pay_type eq 2 || $data.pay_type eq 3}
                                <h2 class="line top bottom">{l i='payment_systems' gid='services'}</h2>
                                {if $billing_systems}
                                {l i='error_select_payment_system' gid='services'}&nbsp;*<br>
                                <select name="system_gid" id="system_gid"><option value="">...</option>{foreach item=item from=$billing_systems}<option value="{$item.gid}">{$item.name}</option>{/foreach}</select>
                                {foreach item=item from=$billing_systems}
                                <div class="hide" id="details_{$item.gid}">
                                <br>{l i='field_info_data' gid='payments'}:<br>
                                {$item.info_data}
                                </div>
                                {/foreach}
                                <div class="b outside">
                                    <input type="submit" value="{l i='btn_pay_systems' gid='services' type='button'}" name="btn_system" class="btn">
                                    <a class="btn-link" href="{$site_url}users_services/my_services">
                                        <ins class="with-icon i-larr"></ins>
                                        {l i='back_to_payment_services' gid='services'}
                                    </a>
                                </div>
                                <script>{literal}
                                    $(function(){
                                        $('#system_gid').bind('change', function(){
                                            {/literal}{foreach item=item from=$billing_systems}{literal}
                                            $('#details_{/literal}{$item.gid}{literal}').hide();
                                            {/literal}{if $item.info_data}{literal}
                                            if($(this).val() == '{/literal}{$item.gid}{literal}') 
                                                $('#details_{/literal}{$item.gid}{literal}').show();
                                            {/literal}{/if}{literal}
                                            {/literal}{/foreach}{literal}
                                        });
                                    });
                                {/literal}</script>
                                {elseif $data.pay_type eq 3}
                                {l i='error_empty_billing_system_list' gid='service'}
                                {/if}
                            {/if}
                        {/if}
                    {/if}  <!-- end of photo count -->
					
                {else}
                    
					{if $data.free_activate}
                    <input type="submit" class='btn' value="{l i='btn_activate_free' gid='services' type='button'}" name="btn_account">
                    {else}
                        {if ($data.pay_type eq 1 || $data.pay_type eq 2) && $is_module_installed}
                            <h2 class="line top bottom">{l i='account_payment' gid='services'}</h2>
                            {if $data.disable_account_pay}{l i='error_account_less_then_service_price' gid='services'} <a href="{seolink module='users' method='account'}">{l i="link_add_founds" gid='services'}</a>
                            {else}
                                {l i='on_your_account_now' gid='services'}: <b>{block name=currency_format_output module=start value=$data.user_account cur_gid=$base_currency.gid}</b>
                                <div class="b outside">
                                    <input type="submit" value="{l i='btn_pay_account' gid='services' type='button'}" name="btn_account">
                                </div>
                            {/if}
                        {/if}
                        {*if $data.pay_type eq 2 && $billing_systems}
                            <br>{l i='payment_type_or' gid='services'}<br>
                        {/if*}
                        {if $data.pay_type eq 2 || $data.pay_type eq 3}
                            <h2 class="line top bottom">{l i='payment_systems' gid='services'}</h2>
                            {if $billing_systems}
                            {l i='error_select_payment_system' gid='services'}&nbsp;*<br>
                            <select name="system_gid" id="system_gid"><option value="">...</option>{foreach item=item from=$billing_systems}<option value="{$item.gid}">{$item.name}</option>{/foreach}</select>
                            {foreach item=item from=$billing_systems}
                            <div class="hide" id="details_{$item.gid}">
                            <br>{l i='field_info_data' gid='payments'}:<br>
                            {$item.info_data}
                            </div>
                            {/foreach}
                            <div class="b outside">
                                <input type="submit" value="{l i='btn_pay_systems' gid='services' type='button'}" name="btn_system" class="btn">
                                <a class="btn-link" href="{$site_url}users_services/my_services">
                                    <ins class="with-icon i-larr"></ins>
                                    {l i='back_to_payment_services' gid='services'}
                                </a>
                            </div>
                            <script>{literal}
                                $(function(){
                                    $('#system_gid').bind('change', function(){
                                        {/literal}{foreach item=item from=$billing_systems}{literal}
                                        $('#details_{/literal}{$item.gid}{literal}').hide();
                                        {/literal}{if $item.info_data}{literal}
                                        if($(this).val() == '{/literal}{$item.gid}{literal}') 
                                            $('#details_{/literal}{$item.gid}{literal}').show();
                                        {/literal}{/if}{literal}
                                        {/literal}{/foreach}{literal}
                                    });
                                });
                            {/literal}</script>
                            {elseif $data.pay_type eq 3}
                            {l i='error_empty_billing_system_list' gid='service'}
                            {/if}
                        {/if}
                    {/if}
                {/if} <!-- end of service gid type -->
            </form>
        </div>
        <!-- end of div with class content-value -->
	</div>
    <!-- end of div with class content-block -->
</div>
<div class="clr"></div>
{include file="footer.tpl"}
