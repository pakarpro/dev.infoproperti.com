<?php  
/**
* Seo model model
* 
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Seo_model extends Model
{
	private $CI;

	var $route_xml_file = "config/seo_module_routes.xml";
	var $route_php_file = "config/seo_module_routes.php";
	/**
	 * Constructor
	 *
	 * @return
	 */
	function __construct()
	{
		parent::Model();
		$this->CI = & get_instance();
	}

	public function get_xml_route_file_content(){
		$file = APPPATH . $this->route_xml_file;
		$xml = simplexml_load_file($file);
		if(!is_object($xml)){
			return array();
		}
		$return = array();
		foreach($xml as $module=>$module_data){
			$module_name = strval($module);
			foreach($module_data->method as $key=>$method){
				$method_name = strval($method["name"]);
				$link = strval($method["link"]);
				$return[$module_name][$method_name] = $link;
			}
		}
		return $return;
	}

	public function set_xml_route_file_content($data){
		$xml = '<?xml version="1.0" encoding="utf-8"?>'."\n";
		$xml .= "<routes>\n";
		foreach($data as $model => $model_data){
			$xml .= "<".$model.">\n";
			foreach($model_data as $method => $pattern){
				if(!empty($pattern)){
					$xml .= '<method name="'.$method.'" link="'.$pattern.'" />'."\n";
				}
			}
			$xml .= "</".$model.">\n";
		}
		$xml .= "</routes>\n";
		$file = APPPATH . $this->route_xml_file;
		$h = fopen($file, "w");
		if($h){
			fwrite($h, $xml);
			fclose($h);
		}
	}

	public function rewrite_route_php_file(){
		$data = $this->get_xml_route_file_content();
		if(empty($data)){
			return false;
		}

		$php = '<?php'."\n\n";
		foreach($data as $model => $model_data){
			foreach($model_data as $method => $pattern){
				if(!empty($pattern)){
					$string = $this->CI->pg_seo->url_template_transform($model, $method, $pattern, 'xml', 'rule');
					$php .= $string."\n";
				}
			}
		}
		$file = APPPATH . $this->route_php_file;
		$h = fopen($file, "w");
		if($h){
			fwrite($h, $php);
			fclose($h);
		}
	}

	public function validate_tracker($data){
		$return = array("errors"=> array(), "data" => array());

		if(isset($data["seo_ga_default_activate"])){
			$return["data"]["seo_ga_default_activate"] = intval($data["seo_ga_default_activate"]);
		}

		if(isset($data["seo_ga_default_account_id"])){
			$return["data"]["seo_ga_default_account_id"] = strip_tags($data["seo_ga_default_account_id"]);
		}

		if(isset($data["seo_ga_default_placement"])){
			$return["data"]["seo_ga_default_placement"] = strval($data["seo_ga_default_placement"]);
		}

		if(isset($data["seo_ga_manual_activate"])){
			$return["data"]["seo_ga_manual_activate"] = intval($data["seo_ga_manual_activate"]);
		}

		if(isset($data["seo_ga_manual_placement"])){
			$return["data"]["seo_ga_manual_placement"] = strval($data["seo_ga_manual_placement"]);
		}

		if(isset($data["seo_ga_manual_tracker_code"])){
			$return["data"]["seo_ga_manual_tracker_code"] = trim($data["seo_ga_manual_tracker_code"]);
		}
		
		if($return["data"]["seo_ga_default_activate"] && empty($return["data"]["seo_ga_default_account_id"])){
			$return["errors"][] = l('error_ga_account_id_empty', 'seo');
		}
		
		if($return["data"]["seo_ga_manual_activate"] && empty($return["data"]["seo_ga_manual_tracker_code"])){
			$return["errors"][] = l('error_tracker_code_empty', 'seo');
		}

		return $return;
	}

	public function get_tracker_html($placement='top'){
		$output = false;
		$ga_default_activate = $this->CI->pg_module->get_module_config('seo', 'seo_ga_default_activate');
		if($ga_default_activate){
			$ga_default_placement = $this->CI->pg_module->get_module_config('seo', 'seo_ga_default_placement');
			if($ga_default_placement == $placement){
				$this->CI->template_lite->assign('ga_default_account_id', $this->CI->pg_module->get_module_config('seo', 'seo_ga_default_account_id'));
				$output = true;
			}
		}

		$manual_activate = $this->CI->pg_module->get_module_config('seo', 'seo_ga_manual_activate');
		if($manual_activate){
			$manual_placement = $this->CI->pg_module->get_module_config('seo', 'seo_ga_manual_placement');
			if($manual_placement == $placement){
				$this->CI->template_lite->assign('tracker_code', $this->CI->pg_module->get_module_config('seo', 'seo_ga_manual_tracker_code'));
				$output = true;
			}
		}
		return ($output)?$this->CI->template_lite->fetch('tracker_block', 'user', 'seo'):"";
	}

	public function get_robots_content(){
		$return = array("errors"=> array(), "data" => '');
		$file = SITE_PHYSICAL_PATH . 'robots.txt';
		$error = false;

		if (!file_exists($file)){
			$return['errors'][] = l('error_robots_txt_not_exist', 'seo');
			$error = true;
		}

		// check writable
		if (!is_writable($file)){
			$return['errors'][] = l('error_robots_txt_not_writable', 'seo');
			$error = true;
		}

		// create handler
		if (!$error){
			$fp = fopen($file, 'r');
			if (filesize($file) > 0){
				$return["data"] = trim(fread($fp, filesize($file)));
			}
			fclose($fp);
		}
		return $return;
	}

	public function set_robots_content($content){
		$return = array("errors"=> array());
		$file = SITE_PHYSICAL_PATH . 'robots.txt';
		$error = false;

		if (!file_exists($file)){
			$return['errors'][] = l('error_robots_txt_not_exist', 'seo');
			$error = true;
		}

		// check writable
		if (!is_writable($file)){
			$return['errors'][] = l('error_robots_txt_not_writable', 'seo');
			$error = true;
		}

		if (!$error){
			$fp = fopen($file, 'w+');
			$result = (bool) fwrite($fp, $content);
			fclose($fp);

			if (!empty($content) && !$result){
				$return['errors'][] = l('error_robots_txt_error_save', 'seo');
			}
		}
		return $return;
	}

	public function get_sitemap_data(){
		$return = array("errors"=> array(), "data" => '');
		$file = SITE_PHYSICAL_PATH . 'sitemap.xml';
		$error = false;

		if (!file_exists($file)){
			$return['errors'][] = l('error_sitemap_xml_not_exist', 'seo');
			$error = true;
		}

		// check writable
		if (!is_writable($file)){
			$return['errors'][] = l('error_sitemap_xml_not_writable', 'seo');
			$error = true;
		}

		// create handler
		if (!$error){
			$data = stat($file);
			$return["data"]["mtime"] = $data["mtime"];
		}
		return $return;
	}	
	
	public function generate_sitemap_xml($params){
		$return = array("errors"=> array(), "data" => '');
		$sitemap_data = $this->get_sitemap_data();

		if(!empty($sitemap_data["errors"])){
			$return["errors"] = $sitemap_data["errors"];
			return $return;
		}

		$params["lastmod_date"] = date('c', strtotime($params["lastmod_date"]));
		$this->clear_sitemap_xml();
		
		$file = SITE_PHYSICAL_PATH . 'sitemap.xml';
		$fp = fopen($file, 'a+');

		$output = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";
		$output .= "\t<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\r\n";
		fwrite($fp, $output);
		
		$modules = $this->CI->pg_seo->get_seo_modules();
		
		foreach($modules as $module){
			if(!empty($module["get_sitemap_urls_method"])){
				$this->CI->load->model($module["module_gid"]."/models/".$module["model_name"]);
				$urls = $this->CI->$module["model_name"]->$module["get_sitemap_urls_method"]();

				if(!empty($urls)){
					$output = "";
					$current_date = date('c');
					foreach($urls as $url_data){
						$output .= "\t\t<url>\r\n";
						$output .= "\t\t\t<loc>".$url_data["url"]."</loc>\r\n";

						if($params["lastmod"] == 1){
							$output .= "\t\t\t<lastmod>".$current_date."</lastmod>\r\n";
						}elseif($params["lastmod"] == 2){
							$output .= "\t\t\t<lastmod>".$params["lastmod_date"]."</lastmod>\r\n";
						}

						if($params["priority"]){
							$output .= "\t\t\t<priority>".$url_data["priority"]."</priority>\r\n";
						}

						if($params["changefreq"]){
							$output .= "\t\t\t<changefreq>".$params["changefreq"]."</changefreq>\r\n";
						}

						$output .= "\t\t</url>\r\n";
					}
					fwrite($fp, $output);
				}
			}
		}

		$output = "\t</urlset>";
		fwrite($fp, $output);

		return $return;
	}
	
	public function clear_sitemap_xml(){
		$file = SITE_PHYSICAL_PATH . 'sitemap.xml';
		$fp = fopen($file, 'w');
		$result = (bool) fwrite($fp, '');
		fclose($fp);
	}
	
	public function lang_dedicate_module_callback_add($lang_id = false) {
		$this->update_route_langs();
	}

	public function lang_dedicate_module_callback_delete($lang_id = false) {
		$this->update_route_langs();
	}

	/**
	 * Update or create file config/langs_route.php
	 *
	 * @return boolean
	 */
	private function update_route_langs() {

		$langs = $this->CI->pg_language->get_langs();
		if (0 == count($langs)) {
			return false;
		}
		foreach($langs as $lang){
			$content_langs[] = "'". $lang['code'] ."'";
		}

		$file_content = "<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');\n\n";
		$file_content .= "\$config['langs_route'] = array(";
		$file_content .= implode(", ", $content_langs);
		$file_content .= ');';

		$file = APPPATH . 'config/langs_route' . EXT;
		try {
			$handle = fopen($file, 'w');
			fwrite($handle, $file_content);
			fclose($handle);
		} catch (Exception $e) {
			log_message('error','Error while updating langs_route' . EXT . '(' . $e->getMessage() . ') in ' . $e->getFile());
			throw $e;
		}
		return true;
	}
}
