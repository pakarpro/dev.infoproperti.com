<?php

if ( ! function_exists('seo_traker'))
{
	function seo_traker($placement='top')
	{
		$CI = &get_instance();
		$CI->load->model('Seo_model');
		$return = $CI->Seo_model->get_tracker_html($placement);
		echo $return;
	}

}
