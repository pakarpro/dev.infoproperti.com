{include file="header.tpl"}
<form method="post" action="{$data.action}" name="save_form">
	<div class="edit-form n150">
		<div class="row header">{l i='admin_header_global_seo_settings_editing' gid='seo'} : {if $controller eq 'admin'}{l i='default_seo_admin_field' gid='seo'}{else}{l i='default_seo_user_field' gid='seo'}{/if}</div>
		<div class="row">
			<div class="h">{l i='field_title_default' gid='seo'}: </div>
			<div class="v">{$default_settings.title}&nbsp;</div>
		</div>
		<div class="row zebra">
			<div class="h">{l i='field_title_default_use' gid='seo'}: </div>
			<div class="v"><input type="checkbox" value="1" name="default_title" {if $default_settings.default_title}checked="checked"{/if} id="default-title"></div>
		</div>
		{foreach item=item key=key from=$languages}
		<div class="row">
			<div class="h">{l i='field_title' gid='seo'}({$item.name}): </div>
			<div class="v"><input type="text" value="{$user_settings.title[$key]}" name="title[{$key}]" {if $default_settings.default_title}disabled{/if} class="long input-default-title"></div>
		</div>
		{/foreach}
		<br>

		<div class="row">
			<div class="h">{l i='field_keyword_default' gid='seo'}: </div>
			<div class="v">{$default_settings.keyword}&nbsp;</div>
		</div>
		<div class="row zebra">
			<div class="h">{l i='field_keyword_default_use' gid='seo'}: </div>
			<div class="v"><input type="checkbox" value="1" name="default_keyword" {if $default_settings.default_keyword}checked="checked"{/if} id="default-keyword"></div>
		</div>
		{foreach item=item key=key from=$languages}
		<div class="row">
			<div class="h">{l i='field_keyword' gid='seo'}({$item.name}): </div>
			<div class="v"><textarea name="keyword[{$key}]" {if $default_settings.default_keyword}disabled{/if} class="input-default-keyword">{$user_settings.keyword[$key]}</textarea></div>
		</div>
		{/foreach}
		<br>
	
		<div class="row">
			<div class="h">{l i='field_description_default' gid='seo'}: </div>
			<div class="v">{$default_settings.description}&nbsp;</div>
		</div>
		<div class="row zebra">
			<div class="h">{l i='field_description_default_use' gid='seo'}: </div>
			<div class="v"><input type="checkbox" value="1" name="default_description" {if $default_settings.default_description}checked="checked"{/if} id="default-description"></div>
		</div>
		{foreach item=item key=key from=$languages}
		<div class="row">
			<div class="h">{l i='field_description' gid='seo'}({$item.name}): </div>
			<div class="v"><textarea name="description[{$key}]" {if $default_settings.default_description}disabled{/if} class="input-default-description">{$user_settings.description[$key]}</textarea></div>
		</div>
		{/foreach}
		<div class="row zebra">
			<div class="h">{l i='field_lang_in_url' gid='seo'}: </div>
			<div class="v">
				<input type="checkbox" value="1" name="lang_in_url" {if $default_settings.lang_in_url}checked="checked"{/if} id="lang_in_url">
			</div>
		</div>
	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="{l i='btn_save' gid='start' type='button'}"></div></div>
	<a class="cancel" href="{$site_url}admin/seo/index">{l i='btn_cancel' gid='start'}</a>
</form>
<div class="clr"></div>
<script>
{literal}
$(function(){
	$(':checkbox').bind('change', function(){
		var id = $(this).attr('id');
		if(this.checked){
			$('.input-'+id).attr('disabled', 'disabled');
		}else{
			$('.input-'+id).removeAttr("disabled");
		}
	});

});
{/literal}
</script>

{include file="footer.tpl"}