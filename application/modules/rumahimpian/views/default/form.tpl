{include file="header.tpl"}
<!--
	### Disable original ###
	{include file="left_panel.tpl" module="start"}
-->

<!-- MOD, temporarily added -->
<div class="lc">
	<div class="inside account_menu">
		{helper func_name=get_content_tree helper_name=content func_param=$page.id}
	</div>
	{helper func_name=show_banner_place module=banners func_param='left-banner'}
</div>
<!-- End MOD -->

<div class="rc">
	<div class="content-block">
		<h1>{seotag tag='header_text'}</h1>

		<div class="content-value">
		<p>{l i='text_contact_form_edit' gid='contact_us'}</p>

		<div class="edit_block">

		<form action="" method="post">
		{if $reasons}
		<div class="r">
			<div class="f">{l i='field_reason' gid='contact_us'}:&nbsp;* </div>
			<div class="v"><select name="id_reason">{foreach item=item from=$reasons}<option value="{$item.id}" {if $data.id_reason eq $item.id}selected{/if}>{$item.name}</option>{/foreach}</select></div>
		</div>
		{/if}
		<div class="r">
			<div class="f">{l i='field_user_name' gid='contact_us'}:&nbsp;* </div>
			<div class="v"><input type="text" name="user_name" value="{$data.user_name}" ></div>
		</div>
		<div class="r">
			<div class="f">{l i='field_user_email' gid='contact_us'}:&nbsp;* </div>
			<div class="v"><input type="text" name="user_email" value="{$data.user_email}"></div>
		</div>
		<div class="r">
			<div class="f">{l i='field_subject' gid='contact_us'}:&nbsp;* </div>
			<div class="v"><input type="text" name="subject" value="{$data.subject}" class="long"></div>
		</div>
		<div class="r">
			<div class="f">{l i='field_message' gid='contact_us'}:&nbsp;* </div>
			<div class="v"><textarea name="message">{$data.message}</textarea></div>
		</div>
		<div class="r">
			<div class="f">{l i='field_security_code' gid='contact_us'}:&nbsp;* </div>
			<div class="v captcha">{$data.captcha}<input type="text" name="captcha_code" value=""></div>
		</div>
		<br>

		<div class="r">
			<div class="l"><input type="submit" class='btn' value="{l i='btn_send' gid='start' type='button'}" name="btn_save"></div>
			<div class="b">&nbsp;</div>
		</div>
		</form>

		</div>

	</div>
	</div>

	{block name=show_social_networks_like module=social_networking}
	{block name=show_social_networks_share module=social_networking}
	{block name=show_social_networks_comments module=social_networking}
	<br><br><br>
</div>
<div class="clr"></div>
{include file="footer.tpl"}
