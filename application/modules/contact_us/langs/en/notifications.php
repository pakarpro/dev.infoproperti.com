<?php

$install_lang["notification_contact_us_form"] = "Contact us form";
$install_lang["tpl_contact_us_form_content"] = "Contact us form is filled in:\n\n\n\n___________________________________\n\n\n\nReason: [reason];\n\nName: [user_name];\n\nUser email: [user_email];\n\nSubject: [subject];\n\nMessage:\n\n[message] \n\n___________________________________";
$install_lang["tpl_contact_us_form_subject"] = "[domain] | New contact us form";

