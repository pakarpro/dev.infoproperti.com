<?php
/**
* Social networking vkontakte widgets model
* 
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Vkontakte_widgets_model extends Model {

	public $CI;
	public $widget_types = array(
		'comments',
		'like',
		'share',
	);
	public $openapi = false;
	public $head_loaded = false;
	private $has_key = false;

	function __construct() {
		parent::__construct();
		$this->CI = & get_instance();
	}

	public function get_header($service_data = array(), $locale = '', $types = array()) {
		$header = '';
		$appid = isset($service_data['app_key']) ? $service_data['app_key'] : false;
		if($appid) {
			$this->has_key = true;
		}
		if (!$this->openapi) {
			$header .= $appid ? ('<script type="text/javascript" src="//vk.com/js/api/openapi.js"></script><script type="text/javascript">VK.init({apiId: ' . $appid . ', onlyWidgets: true});</script>') : '';
			$this->openapi = true;
		}
		$header .= '<script type="text/javascript" src="http://vk.com/js/api/share.js" charset="windows-1251"></script><script type="text/javascript" src="http://vk.com/js/api/share.js?11" charset="windows-1251"></script>';
		$this->head_loaded = $header ? true : false;
		return $header;
	}

	public function get_like() {
		return $this->head_loaded && $this->openapi  && $this->has_key ? '<div id="vk_like"></div><script type="text/javascript">VK.Widgets.Like("vk_like", {type: "button", height: 28});</script>' : '';
	}

	public function get_share() {
		return $this->head_loaded ? '<script type="text/javascript">document.write(VK.Share.button(false,{type: "button", text:"'.l('text_vk_share', 'social_networking', '', 'javascript').'"}));</script>' : '';
	}

	public function get_comments() {
		return $this->head_loaded && $this->openapi && $this->has_key ? '<div id="vk_comments"></div><script type="text/javascript">VK.Widgets.Comments("vk_comments", {limit: 10, width: "496", attach: "*"});</script>' : '';
	}
}
