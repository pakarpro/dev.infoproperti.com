<?php

$install_lang["admin_menu_settings_items_content_items_news_menu_item"] = "Новости";
$install_lang["admin_menu_settings_items_content_items_news_menu_item_tooltip"] = "Добавление новостей и новостных каналов";
$install_lang["admin_news_menu_feeds_list_item"] = "Каналы новостей";
$install_lang["admin_news_menu_feeds_list_item_tooltip"] = "";
$install_lang["admin_news_menu_news_list_item"] = "Новости";
$install_lang["admin_news_menu_news_list_item_tooltip"] = "";
$install_lang["admin_news_menu_settings_list_item"] = "Настройки";
$install_lang["admin_news_menu_settings_list_item_tooltip"] = "";
$install_lang["user_footer_menu_footer-menu-news-item"] = "Новости";
$install_lang["user_footer_menu_footer-menu-news-item_tooltip"] = "";

