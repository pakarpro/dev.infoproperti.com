<?php
/**
* News main model
*
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/


if (!defined('BASEPATH')) exit('No direct script access allowed');

define('NEWS_TABLE', DB_PREFIX.'news');
define('RESIDENTIAL', DB_PREFIX.'listings_residential');

class News_model extends Model
{
	private $CI;
	private $DB;

	private $fields_news = array(
		'id',
		'gid',
		'name',
		'annotation',
		'content',
		'img',
		'status',
		'id_lang',
		'news_type',
		'date_add',
		'feed_link',
		'feed_id',
		'feed_unique_id',
		'video',
		'video_image',
		'video_data',
		'scheduling'
	);

	private $fields_news_cute = array(
		'id',
		'gid',
		'name',
		'annotation',
		'img',
		'status',
		'id_lang',
		'news_type',
		'date_add',
		'feed_link',
		'feed_id',
		'feed_unique_id',
	);
	#MOD FOR EXTERNAL NEWS
	private $internal_news = array(
		'pg_news.id',
		'pg_news.gid',
		'pg_news.name',
		'pg_news.annotation',
		'pg_news.img',
		'pg_news.status',
		'pg_news.id_lang',
		'pg_news.news_type',
		'pg_news.date_add',
		'pg_news.feed_link',
		'pg_news.feed_id',
		'pg_news.feed_unique_id',
		'pg_news_feeds.external',
	);
	#END OF MOD
	private $residential = array(
		'id',
		'id_listing',
		'fe_bd_rooms_1',
		'fe_bth_rooms_1',
		'fe_garages_1',
		'fe_foundation_1',
		'fe_total_floors_1',
	);

	public $upload_config_id = 'news-logo';
	public $video_config_id = 'news-video';
	public $rss_config_id = 'rss-logo';
	/**
	 * Constructor
	 *
	 * @return Install object
	 */
	function __construct()
	{
		parent::Model();
		$this->CI = & get_instance();
		$this->DB = &$this->CI->db;
	}

	public function get_news_by_id($id){
		$result = $this->DB->select(implode(", ", $this->fields_news))->from(NEWS_TABLE)->where("id", $id)->get()->result_array();
		if(empty($result)){
			return false;
		}else{
			$data = $result[0];
			return $data;
		}
	}

	public function get_news_by_gid($gid){
		$result = $this->DB->select(implode(", ", $this->fields_news))->from(NEWS_TABLE)->where("gid", $gid)->get()->result_array();
		if(empty($result)){
			return false;
		}else{
			$data = $result[0];
			return $data;
		}
	}

	public function format_news($news){
		$this->CI->load->model('Uploads_model');
		$this->CI->load->model('Video_uploads_model');
		$feeds = array();

		foreach($news as $key => $data){
			if(!empty($data["id"])){
				$data["prefix"] = date("Y/m/d/", strtotime($data["date_add"])).$data["id"]."";
			}
			if($this->pg_module->is_module_installed('uploads')) {
				if(!empty($data["img"])){
					$data["media"]["img"] = $this->CI->Uploads_model->format_upload($this->upload_config_id, $data["prefix"], $data["img"]);
				}else{
					$data["media"]["img"] = $this->CI->Uploads_model->format_default_upload($this->upload_config_id);
				}
			}
			if(!empty($data["video_data"])){
				$data["video_data"] = unserialize($data["video_data"]);
			}

			if(!empty($data["video"]) && $data["video_data"]["status"] == "end"){
				$data["video_content"] = $this->CI->Video_uploads_model->format_upload($this->video_config_id, $data["prefix"], $data["video"], $data["video_image"], $data["video_data"]["data"]["upload_type"]);
			}

			$news[$key] = $data;

			if(!empty($data["feed_id"]) && !in_array($data["feed_id"], $feeds)){
				$feeds[] = $data["feed_id"];
			}
		}

		if(!empty($feeds)){
			$this->CI->load->model('news/models/Feeds_model');
			$temp = $this->CI->Feeds_model->get_feeds_list(null, null, null, array(), $feeds);
			if(!empty($temp)){
				foreach($temp as $feed) $feeds_list[$feed["id"]] = $feed;
				foreach($news as $key => $data){
					if(!empty($data["feed_id"]) && !empty($feeds_list[$data["feed_id"]])){
						$news[$key]["feed"] = $feeds_list[$data["feed_id"]];
					}
				}
			}
		}
		return $news;
	}

	public function format_single_news($news){
		$data = $this->format_news(array($news));
		return $data[0];
	}

	public function get_news_list($page=null, $items_on_page=null, $order_by=null, $params=array(), $filter_object_ids=null, $formated=true){
		$this->DB->select(implode(", ", $this->fields_news_cute));
		$this->DB->from(NEWS_TABLE);

		if(isset($params["where"]) && is_array($params["where"]) && count($params["where"])){
			foreach($params["where"] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])){
			foreach($params["where_in"] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])){
			foreach($params["where_sql"] as $value){
				$this->DB->where($value);
			}
		}

		if(isset($filter_object_ids) && is_array($filter_object_ids) && count($filter_object_ids)){
			$this->DB->where_in("id", $filter_object_ids);
		}

		if(is_array($order_by) && count($order_by)>0){
			foreach($order_by as $field => $dir){
				if(in_array($field, $this->fields_news)){
					$this->DB->order_by($field." ".$dir);
				}
			}
		}

		if(!is_null($page) ){
			$page = intval($page)?intval($page):1;
			$this->DB->limit($items_on_page, $items_on_page*($page-1));
		}
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			foreach($results as $r){
				$data[] = $r;
			}
			return ($formated)?$this->format_news($data):$data;
		}
		return array();
	}

	public function get_news_count($params=array(), $filter_object_ids=null){
		$this->DB->select("COUNT(*) AS cnt");
		$this->DB->from(NEWS_TABLE);

		if(isset($params["where"]) && is_array($params["where"]) && count($params["where"])){
			foreach($params["where"] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])){
			foreach($params["where_in"] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])){
			foreach($params["where_sql"] as $value){
				$this->DB->where($value);
			}
		}

		if(isset($filter_object_ids) && is_array($filter_object_ids) && count($filter_object_ids)){
			$this->DB->where_in("id", $filter_object_ids);
		}

		$result = $this->DB->get()->result();
		if(!empty($result)){
			return intval($result[0]->cnt);
		}else{
			return 0;
		}
	}

	public function save_news($id, $data, $file_name="", $video_name=""){
		if (empty($id)){
			$data["date_add"] = date("Y-m-d H:i:s");
			$this->DB->insert(NEWS_TABLE, $data);
			$id = $this->DB->insert_id();
		}else{
			$this->DB->where('id', $id);
			$this->DB->update(NEWS_TABLE, $data);
		}

		if(!empty($file_name) && !empty($id) && isset($_FILES[$file_name]) && is_array($_FILES[$file_name]) && is_uploaded_file($_FILES[$file_name]["tmp_name"])){
			$news_data = $this->get_news_by_id($id);
			$news_data = $this->format_single_news($news_data);

			$this->CI->load->model("Uploads_model");
			$img_return = $this->CI->Uploads_model->upload($this->upload_config_id, $news_data["prefix"], $file_name);

			if(empty($img_return["errors"])){
				$img_data["img"] = $img_return["file"];
				$this->save_news($id, $img_data);
			}
		}

		if(!empty($video_name) && !empty($id) && isset($_FILES[$video_name]) && is_array($_FILES[$video_name]) && is_uploaded_file($_FILES[$video_name]["tmp_name"])){
			if(!isset($news_data)){
				$news_data = $this->get_news_by_id($id);
				$news_data = $this->format_single_news($news_data);
			}
			$this->CI->load->model("Video_uploads_model");
			$video_data = array(
				"name" => $news_data["name"],
				"description" => $news_data["annotation"],
			);
			$video_return = $this->CI->Video_uploads_model->upload($this->video_config_id, $news_data["prefix"], $video_name, $id, $video_data);
		}
		return $id;
	}
	#MOD#
	public function save_news_feeds($id, $data, $file_name="", $video_name=""){
		if(!empty($data["img"]))
		{
			$img_source = $data["img"];
			$image_extension = explode('.', $img_source);
			$data["img"] = date('H-i-s').'.'.end($image_extension);
		}
		if (empty($id)){
			$data["date_add"] = date("Y-m-d H:i:s");
			$this->DB->insert(NEWS_TABLE, $data);
			$id = $this->DB->insert_id();
			if(!empty($data["img"]))
			{
				$this->CI->load->model("Uploads_model");
				$path = "/opt/ip/uploads/news-logo/".date("Y")."/".date("m")."/".date("d")."/";
				if(!is_dir($path))
				{
					#echo 'WHAT THE FAK';echo '<br/><br/>';	
					if (!mkdir($path, 0777, TRUE)) {
						die('Failed to create folders...');
					}					
					#mkdir($path, 0777, TRUE);	
				}
				
				$image = $path.$data["img"];
				file_put_contents($image, file_get_contents($img_source));
				$config_gid = 'feeds-logo';
				$config_data = $this->CI->Uploads_model->get_config($config_gid);
				if(isset($config_data["thumbs"]) && !empty($config_data["thumbs"])){
					$this->CI->Uploads_model->create_thumbs($data["img"], $path, $config_data["thumbs"]);
				}
				/*
				print_r($path);
				echo '<br/><br/>';	
				print_r($data["img"]);
				echo '<br/><br/>';	
				print_r($image);	
				echo '<br/><br/>';	
				print_r($config_data);
				exit;	
				*/	
				/*
				$config_gid = 'feeds-logo';
				$config_data = $this->Uploads_model->get_config($config_gid);
				print_r($config_data);exit;
				if(isset($config_data["thumbs"]) && !empty($config_data["thumbs"])){
					$this->Uploads_model->create_thumbs($data["img"], $image, $config_data["thumbs"]);
				}
				*/
			}			
		}
		else
		{
			$this->DB->where('id', $id);
			$this->DB->update(NEWS_TABLE, $data);
		}
		return $id;
	}
	
	public function save_news_feeds_cron($id, $data, $file_name="", $video_name=""){
		#MOD#
		if(!empty($data["img"]))
		{
			$img_source = $data["img"];
			$image_extension = explode('.', $img_source);
			$data["img"] = date('H-i-s').'.'.end($image_extension);
		}
		#print_r($data);exit;
		if (empty($id)){
			$data["date_add"] = date("Y-m-d H:i:s");
			$this->DB->insert(NEWS_TABLE, $data);
			$id = $this->DB->insert_id();
			if(!empty($data["img"]))
			{
				$this->CI->load->model("Uploads_model");
				$path = "/opt/ip/uploads/news-logo/".date("Y")."/".date("m")."/".date("d")."/".$id."/";
				if(!is_dir($path))
				{
					mkdir($path, 0777, TRUE);	
				}
				
				$image = $path.$data["img"];
				file_put_contents($image, file_get_contents($img_source));
				$config_gid = 'feeds-logo';
				$config_data = $this->CI->Uploads_model->get_config($config_gid);
				if(isset($config_data["thumbs"]) && !empty($config_data["thumbs"])){
					$this->CI->Uploads_model->create_thumbs($data["img"], $path, $config_data["thumbs"]);
				}
				
				/*
				$config_gid = 'feeds-logo';
				$config_data = $this->Uploads_model->get_config($config_gid);
				print_r($config_data);exit;
				if(isset($config_data["thumbs"]) && !empty($config_data["thumbs"])){
					$this->Uploads_model->create_thumbs($data["img"], $image, $config_data["thumbs"]);
				}
				*/
			}
			#MOD#
			/*
			if(!empty($data["img"]))
			{
				$this->CI->load->model("Uploads_model");
				$news_data = $this->get_news_by_id($id);
				$news_data = $this->format_single_news($news_data);
				$path = $this->Uploads_model->get_media_path($this->upload_config_id, $news_data["prefix"]);
				if(!is_dir($path)){
					@mkdir($path, 0777, TRUE);
				}
				#$image_extension = explode('.', $data["img"]);
				#$image_extension = end($image_extension);
				#$image_name = date('H-i-s').'.'.$image_extension;			
				
				#$image_name = explode('/', $data["img"]);
				#$image_name = end($image_name);
				
				#$url = $data["img"];
				/*
				$img = $path.$data["img"];
				file_put_contents($img, file_get_contents($img_source));
				$config_gid = 'feeds-logo';
				$config_data = $this->Uploads_model->get_config($config_gid);
				if(isset($config_data["thumbs"]) && !empty($config_data["thumbs"])){
					$this->Uploads_model->create_thumbs($data["img"], $path, $config_data["thumbs"]);
				}
				*/
				#$this->db->where('id', $news_data["id"]);
				#$this->db->where('id', $id);
				#$this->db->update('pg_news', $image_data);
			#}
			#END OF MOD#
			
		}else{
			$this->DB->where('id', $id);
			$this->DB->update(NEWS_TABLE, $data);
		}
		/*
		if(!empty($file_name) && !empty($id) && isset($_FILES[$file_name]) && is_array($_FILES[$file_name]) && is_uploaded_file($_FILES[$file_name]["tmp_name"])){
			$news_data = $this->get_news_by_id($id);
			$news_data = $this->format_single_news($news_data);
			$this->CI->load->model("Uploads_model");
			$img_return = $this->CI->Uploads_model->upload($this->upload_config_id, $news_data["prefix"], $file_name);

			if(empty($img_return["errors"])){
				$img_data["img"] = $img_return["file"];
				$this->save_news($id, $img_data);
			}
		}
		if(!empty($video_name) && !empty($id) && isset($_FILES[$video_name]) && is_array($_FILES[$video_name]) && is_uploaded_file($_FILES[$video_name]["tmp_name"])){
			if(!isset($news_data)){
				$news_data = $this->get_news_by_id($id);
				$news_data = $this->format_single_news($news_data);
			}
			$this->CI->load->model("Video_uploads_model");
			$video_data = array(
				"name" => $news_data["name"],
				"description" => $news_data["annotation"],
			);
			$video_return = $this->CI->Video_uploads_model->upload($this->video_config_id, $news_data["prefix"], $video_name, $id, $video_data);
		}
		*/
		return $id;
	}
	
	#END OF MOD#


	public function validate_news($id, $data, $file_name="", $video_name=""){
		$return = array("errors"=> array(), "data" => array());

		if(isset($data["name"])){
			$return["data"]["name"] = strip_tags($data["name"]);

			if(empty($return["data"]["name"]) ){
				$return["errors"][] = l('error_name_incorrect', 'news');
			}
		}

		if(isset($data["annotation"])){
			$return["data"]["annotation"] = strip_tags($data["annotation"]);
		}

		if(isset($data["content"])){
			$return["data"]["content"] = $data["content"];
		}
		#MOD#
		if(isset($data["image_file"])){
			$return["data"]["img"] = $data["image_file"];
		}
		//this mod is for scheduling news
		if(isset($data["scheduling"])&&$data["scheduling_flag"]==1){
			$data["scheduling"] = date("Y-m-d", strtotime($data["scheduling"]));  
			if($data["scheduling_time"]==1)
			{
				$data["scheduling_time"]= date("H:i:s");	
			}
			$data["scheduling"] = $data["scheduling"].' '.$data["scheduling_time"];
			#$return["data"]["scheduling"] = $data["scheduling"].' '.$data["scheduling_time"];
			$return["data"]["scheduling"] = $data["scheduling"];
		}
		else
		{
			$return["data"]["scheduling"] = '';
		}
		if($data["scheduling_flag"]&&$data["scheduling"]=="")
		{
			$return["errors"][] = "Active scheduling requires a valid date";	
		}
		#END OF MOD#
		if(isset($data["id_lang"])){
			$return["data"]["id_lang"] = intval($data["id_lang"]);
		}

		if(isset($data["feed_id"])){
			$return["data"]["feed_id"] = intval($data["feed_id"]);
		}

		if(isset($data["feed_link"])){
			$return["data"]["feed_link"] = trim(strip_tags($data["feed_link"]));
		}

		if(isset($data["feed_unique_id"])){
			$return["data"]["feed_unique_id"] = trim(strip_tags($data["feed_unique_id"]));
			if(!empty($return["data"]["feed_unique_id"]) && empty($id)){
				$feed_params["where"]["feed_unique_id"] = $return["data"]["feed_unique_id"];
				$count = $this->get_news_count($feed_params);
				if($count > 0){
					$return["errors"][] = l('error_feed_news_exists', 'news');
				}
			}
		}

		if(isset($data["news_type"])){
			$return["data"]["news_type"] = trim($data["news_type"]);
		}

		if(isset($data["status"])){
			$return["data"]["status"] = intval($data["status"]);
		}

		if(isset($data["video"])){
			$return["data"]["video"] = strval($data["video"]);
		}

		if(isset($data["video_data"])){
			$return["data"]["video_data"] = $data["video_data"];
		}

		if(isset($data["gid"])){
			$this->CI->config->load("reg_exps", TRUE);
			$reg_exp = $this->CI->config->item("not_literal", "reg_exps");
			$temp_gid = $return["data"]["gid"] = strtolower(trim(strip_tags($data["gid"])));
			if(!empty($temp_gid)){
				$return["data"]["gid"] = preg_replace($reg_exp, '-', $return["data"]["gid"]);
				$return["data"]["gid"] = preg_replace("/[\-]{2,}/i", '-', $return["data"]["gid"]);
				$return["data"]["gid"] = trim($return["data"]["gid"], '-');
				if(empty($return["data"]["gid"])){
					$return["data"]["gid"] = md5($temp_gid);
				}
			}else{
				$return["errors"][] = l('error_gid_incorrect', 'news');
			}

			$params["where"]["id_lang"] = $return["data"]["id_lang"];
			$params["where"]["gid"] = $return["data"]["gid"];
			if($id) $params["where"]["id <>"] = $id;
			$count = $this->get_news_count($params);
			if($count > 0){
				$return["errors"][] = l('error_gid_already_exists', 'news');
			}
		}

		if(!empty($file_name) && isset($_FILES[$file_name]) && is_array($_FILES[$file_name]) && is_uploaded_file($_FILES[$file_name]["tmp_name"])){
			$this->CI->load->model("Uploads_model");
			$img_return = $this->CI->Uploads_model->validate_upload($this->upload_config_id, $file_name);
			if(!empty($img_return["error"])){
				$return["errors"][] = implode("<br>", $img_return["error"]);
			}
		}

		if(!empty($video_name) && isset($_FILES[$video_name]) && is_array($_FILES[$video_name]) && is_uploaded_file($_FILES[$video_name]["tmp_name"])){
			$this->CI->load->model("Video_uploads_model");
			$video_return = $this->CI->Video_uploads_model->validate_upload($this->video_config_id, $video_name);
			if(!empty($video_return["error"])){
				$return["errors"][] = implode("<br>", $video_return["error"]);
			}
		}
		return $return;
	}

	public function delete_news($id){
		if(!empty($id)){
			$news_data = $this->get_news_by_id($id);

			$this->DB->where('id', $id);
			$this->DB->delete(NEWS_TABLE);

			if(!empty($news_data["img"])){
				$news_data = $this->format_single_news($news_data);
				$this->CI->load->model("Uploads_model");
				$this->CI->Uploads_model->delete_upload($this->upload_config_id, $news_data["prefix"], $news_data["img"]);
			}

			if(!empty($news_data["video"])){
				$news_data = $this->format_single_news($news_data);
				$this->CI->load->model("Video_uploads_model");
				$this->CI->Video_uploads_model->delete_upload($this->video_config_id, $news_data["prefix"], $news_data["video"], $news_data["video_data"]["data"]["upload_type"]);
			}
		}
		return;
	}

	public function validate_rss_settings($data, $file_name){
		$return = array("errors"=> array(), "data" => array());

		if(isset($data["rss_use_feeds_news"])){
			$return["data"]["rss_use_feeds_news"] = intval($data["rss_use_feeds_news"]);
		}

		if(isset($data["rss_news_max_count"])){
			$return["data"]["rss_news_max_count"] = intval($data["rss_news_max_count"]);

			if(empty($return["data"]["rss_news_max_count"]) ){
				$return["errors"][] = l('error_sett_rss_news_count_incorrect', 'news');
			}
		}

		if(isset($data["userside_items_per_page"])){
			$return["data"]["userside_items_per_page"] = intval($data["userside_items_per_page"]);

			if(empty($return["data"]["userside_items_per_page"]) ){
				$return["errors"][] = l('error_sett_userside_page_incorrect', 'news');
			}
		}

		if(isset($data["rss_feed_channel_title"])){
			$return["data"]["rss_feed_channel_title"] = trim(strip_tags($data["rss_feed_channel_title"]));

			if(empty($return["data"]["rss_feed_channel_title"]) ){
				$return["errors"][] = l('error_sett_feed_channel_title_incorrect', 'news');
			}
		}

		if(isset($data["rss_feed_channel_description"])){
			$return["data"]["rss_feed_channel_description"] = trim(strip_tags($data["rss_feed_channel_description"]));

			if(empty($return["data"]["rss_feed_channel_description"]) ){
				$return["errors"][] = l('error_sett_feed_channel_description_incorrect', 'news');
			}
		}

		if(isset($data["rss_feed_image_title"])){
			$return["data"]["rss_feed_image_title"] = trim(strip_tags($data["rss_feed_image_title"]));

			if(empty($return["data"]["rss_feed_image_title"]) ){
				$return["errors"][] = l('error_sett_feed_image_title_incorrect', 'news');
			}
		}

		if(!empty($file_name) && isset($_FILES[$file_name]) && is_array($_FILES[$file_name]) && is_uploaded_file($_FILES[$file_name]["tmp_name"])){
			$this->CI->load->model("Uploads_model");
			$img_return = $this->CI->Uploads_model->validate_upload($this->rss_config_id, $file_name);
			if(!empty($img_return["error"])){
				$return["errors"][] = implode("<br>", $img_return["error"]);
			}
		}

		return $return;
	}

	public function get_rss_settings(){
		$data = array(
			"userside_items_per_page" => $this->CI->pg_module->get_module_config('news', 'userside_items_per_page'),
			"userhelper_items_per_page" => $this->CI->pg_module->get_module_config('news', 'userhelper_items_per_page'),
			"rss_feed_channel_title" => $this->CI->pg_module->get_module_config('news', 'rss_feed_channel_title'),
			"rss_feed_channel_description" => $this->CI->pg_module->get_module_config('news', 'rss_feed_channel_description'),
			"rss_feed_image_url" => $this->CI->pg_module->get_module_config('news', 'rss_feed_image_url'),
			"rss_feed_image_title" => $this->CI->pg_module->get_module_config('news', 'rss_feed_image_title'),
			"rss_use_feeds_news" => $this->CI->pg_module->get_module_config('news', 'rss_use_feeds_news'),
			"rss_news_max_count" => $this->CI->pg_module->get_module_config('news', 'rss_news_max_count'),
		);

		if($data["rss_feed_image_url"]){
			$this->CI->load->model('Uploads_model');
			$data["rss_feed_image_media"] = $this->CI->Uploads_model->format_upload($this->rss_config_id, "", $data["rss_feed_image_url"]);
		}

		return $data;
	}


	public function set_rss_settings($data, $file_name=''){
		foreach($data as $setting => $value){
			$this->CI->pg_module->set_module_config('news', $setting, $value);
		}

		if(!empty($file_name) && isset($_FILES[$file_name]) && is_array($_FILES[$file_name]) && is_uploaded_file($_FILES[$file_name]["tmp_name"])){
			$this->CI->load->model("Uploads_model");
			$img_return = $this->CI->Uploads_model->upload($this->rss_config_id, "", $file_name);

			if(empty($img_return["errors"])){
				$this->CI->pg_module->set_module_config('news', "rss_feed_image_url", $img_return["file"]);
			}
		}
		return;
	}


	////// seo
	function get_seo_settings($method='', $lang_id=''){
		if(!empty($method)){
			return $this->_get_seo_settings($method, $lang_id);
		}else{
			$actions = array('index', 'view');
			$return = array();
			foreach($actions as $action){
				$return[$action] = $this->_get_seo_settings($action, $lang_id);
			}
			return $return;
		}
	}

	function _get_seo_settings($method, $lang_id=''){
		if($method == "index"){
			return array(
				"title" => l('seo_tags_index_title', 'news', $lang_id, 'seo'),
				"keyword" => l('seo_tags_index_keyword', 'news', $lang_id, 'seo'),
				"description" => l('seo_tags_index_description', 'news', $lang_id, 'seo'),
				"templates" => array(),
				"header" => l('seo_tags_index_header', 'news', $lang_id, 'seo'),
				"url_vars" => array()
			);
		}elseif($method == "view"){
			return array(
				"title" => l('seo_tags_view_title', 'news', $lang_id, 'seo'),
				"keyword" => l('seo_tags_view_keyword', 'news', $lang_id, 'seo'),
				"description" => l('seo_tags_view_description', 'news', $lang_id, 'seo'),
				"templates" => array('gid', 'name', 'annotation', 'date_add', 'feed_unique_id'),
				"header" => l('seo_tags_view_header', 'news', $lang_id, 'seo'),
				"url_vars" => array(
					"id" => array("gid"=>'literal')
				)
			);
		}
	}

	function request_seo_rewrite($var_name_from, $var_name_to, $value){
		$user_data = array();

		if($var_name_from == $var_name_to){
			return $value;
		}

		if($var_name_from == "gid"){
			$news_data = $this->get_news_by_gid($value);
		}

		if($var_name_to == "id"){
			return $news_data["id"];
		}
	}

	function get_sitemap_xml_urls(){
		$this->CI->load->helper('seo');
		$return = array(
			array(
				"url" => rewrite_link('news', 'index'),
				"priority" => 0.1
			)
		);
		return $return;
	}

	function get_sitemap_urls(){
		$this->CI->load->helper('seo');
		$auth = $this->CI->session->userdata("auth_type");

		$block[] = array(
			"name" => l('header_news', 'news'),
			"link" => rewrite_link('news', 'index'),
			"clickable" => true,
		);
		return $block;
	}


	////// banners callback method
	public function _banner_available_pages(){
		$return[] = array("link"=>"news/index", "name"=> l('header_index_news', 'news'));
		$return[] = array("link"=>"news/view", "name"=> l('header_view_news', 'news'));
		return $return;
	}

	//////subscription call method
	public function get_last_news($lang_id){
		//$result = array('content' => 'test content test content test content test content with lang_id:'.$lang_id);
		$d = mktime();
		$last_week = mktime(date("H", $d), date("i", $d), date("s", $d), date("n", $d), date("j", $d) - 7, date("Y", $d));
		$attrs["where"]["id_lang"] = $lang_id;
		$attrs["where"]["status"] = "1";
		$attrs["where"]["date_add >"]  = date("Y-m-d H:i:s", $last_week);
		$attrs["where"]["set_to_subscribe"] = "0";
		$content = '';
		$last_news = $this->get_news_list(null, null, array('date_add' => "DESC"), $attrs);
		if ($last_news){
			foreach($last_news as $k => $item){
				$content .= $item['date_add']."\r\n".$item['annotation']."\r\n".l('link_view_more', 'news', $lang_id).' '.site_url().'news/view/'.$item['id']."\r\n"."\r\n";
			}
			//$this->update_news_subscription_status($attrs);
		}
		$result = array('content' => $content);
		return $result;
	}

	function update_news_subscription_status($params){
		$data = array('set_to_subscribe' => '1');

		if(isset($params["where"]) && is_array($params["where"]) && count($params["where"])){
			foreach($params["where"] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])){
			foreach($params["where_in"] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])){
			foreach($params["where_sql"] as $value){
				$this->DB->where($value);
			}
		}

		if(isset($filter_object_ids) && is_array($filter_object_ids) && count($filter_object_ids)){
			$this->DB->where_in("id", $filter_object_ids);
		}
		$this->DB->update(NEWS_TABLE, $data);
	}

	////// video callback
	public function video_callback($id, $status, $data, $errors){

		$news_data = $this->get_news_by_id($id);
		$news_data = $this->format_single_news($news_data);

		if(isset($data["video"])){
			$update["video"] = $data["video"];
		}
		if(isset($data["image"])){
			$update["video_image"] = $data["image"];
		}

		$update["video_data"] = $news_data["video_data"];

		if($status == 'start'){
			$update["video_data"] = array();
		}

		if(!isset($update["video_data"]["data"])){
			$update["video_data"]["data"] = array();
		}

		if(!empty($data)){
			$update["video_data"]["data"] = array_merge($update["video_data"]["data"], $data);
		}

		$update["video_data"]["status"] = $status;
		$update["video_data"]["errors"] = $errors;
		$update["video_data"] = serialize($update["video_data"]);
		$this->save_news($id, $update);
	}
	
	/**
	 * Dynamic block callback method for returning latest added news
	 * @param array $params
	 * @param string $view
	 * @return string
	 */
	 
	public function _dynamic_block_get_latest_added_news($params, $view=""){
		date_default_timezone_set("Asia/Jakarta");
		$current_date = date("Y-m-d H:i:s");
		//$news_headline = $this->get_news_list_external(1, $count, array("date_add" => "DESC"), array("where"=>array("pg_news.status"=>1, "pg_news.id_lang"=>$current_lang_id, "pg_news_feeds.external"=>0), "or_where"=>array("pg_news.news_type"=>"news", "pg_news.scheduling <="=>$current_date)));

		$count = $params["count"] ? intval($params["count"]) : 8;
		
		$current_lang_id = $this->pg_language->current_lang_id;
		#MOD#
		$news_latest_id = $this->get_latest_news_id($current_lang_id);
		$this->CI->template_lite->assign("news_latest_id", $news_latest_id);
		#END OF MOD#
		#MOD#
		//$news_headline grab all latest news item
		#$news_headline = $this->get_news_list(1, $count, array("date_add" => "DESC"), array("where"=>array("status"=>1, "id_lang"=>$current_lang_id)));
		#$news_headline = $this->get_news_list_external(1, $count, array("date_add" => "DESC"), array("where"=>array("pg_news.status"=>1, "pg_news.id_lang"=>$current_lang_id, "pg_news_feeds.external"=>0), "or_where"=>array("pg_news.news_type"=>"news")));
		$news_headline = $this->get_news_list_external(1, $count, array("date_add" => "DESC"), array("where"=>array("pg_news.status"=>1, "pg_news.id_lang"=>$current_lang_id, "pg_news_feeds.external"=>0), "or_where"=>array("pg_news.news_type"=>"news", "pg_news.scheduling <="=>$current_date)));
		#external news is an added feature to separate news by feed source (if no feed source is triggered as an external news source, this will return empty
		$external_news = $this->get_news_list_external(1, 1, array("date_add" => "DESC"), array("where"=>array("pg_news.status"=>1, "pg_news.id_lang"=>$current_lang_id, "pg_news_feeds.external"=>1)));
		#MOD to add random factor for default images#
		$image_random_count = 1;
		foreach($news_headline as $key => $subarray) {
			#another mod to check if the image is broken#
			if(preg_match('/.*?http\:.*?/', $news_headline[$key]['media']['img']['file_name']))
			{
				unset($news_headline[$key]['media']['img']);
				$news_headline[$key]['media']['img']['file_name'] = 'default_news-logo.jpg';
			}
			#end of mod#
			if($news_headline[$key]['media']['img']['file_name'] = 'default_news-logo.jpg')
			{
			   $news_headline[$key]['image_randomizer'] = $image_random_count;
			   $image_random_count++;
			}
			else
			{
			   $news_headline[$key]['image_randomizer'] = rand(1,5);
			}
		}
		#end of mod#
		#print_r($random);exit;
		//$news then is a result of splice (taking off 2 top array element, resulting in $news having the leftover array while $news_headline will have the first 2 array element, making it a headline
		$news = array_splice($news_headline, 1); 
		#END OF MOD
		#MOD INCASE THERE IS NO EXTERNAL NEWS, RIGHT NEWS SPOT WILL BE TAKEN FROM THE NEWS#
		if(empty($external_news))
		{
			$external_news = $news;
			$news = array_splice($external_news, 1);	
		}
		#END OF MOD#
		//original
		//$news = $this->get_news_list(1, $count, array("date_add" => "DESC"), array("where"=>array("status"=>1, "id_lang"=>$current_lang_id)));
		$this->CI->template_lite->assign("news_headline", $news_headline);
		$this->CI->template_lite->assign("external_news", $external_news);
		$this->CI->template_lite->assign("news", $news);
		return $this->CI->template_lite->fetch("helper_latest_added_news", "user", "news");
	}
	 
	/* old one without external news 
	public function _dynamic_block_get_latest_added_news($params, $view=""){
		$count = $params["count"] ? intval($params["count"]) : 8;

		$current_lang_id = $this->pg_language->current_lang_id;
		
		#MOD#
		$news_latest_id = $this->get_latest_news_id($current_lang_id);
		$this->CI->template_lite->assign("news_latest_id", $news_latest_id);
		#END OF MOD#
		#MOD#
		//$news_headline grab all latest news item
		$news_headline = $this->get_news_list(1, $count, array("date_add" => "DESC"), array("where"=>array("status"=>1, "id_lang"=>$current_lang_id)));
		#MOD to add random factor for default images#
		$image_random_count = 1;
		foreach($news_headline as $key => $subarray) {
			if($news_headline[$key]['media']['img']['file_name'] = 'default_news-logo.jpg')
			{
			   $news_headline[$key]['image_randomizer'] = $image_random_count;
			   $image_random_count++;
			}
			else
			{
			   $news_headline[$key]['image_randomizer'] = rand(1,5);
			}
		}
		#end of mod#
		#print_r($random);exit;
		//$news then is a result of splice (taking off 2 top array element, resulting in $news having the leftover array while $news_headline will have the first 2 array element, making it a headline
		$news = array_splice($news_headline, 2); 
		#END OF MOD
		//original
		//$news = $this->get_news_list(1, $count, array("date_add" => "DESC"), array("where"=>array("status"=>1, "id_lang"=>$current_lang_id)));
		$this->CI->template_lite->assign("news_headline", $news_headline);
		$this->CI->template_lite->assign("news", $news);
		return $this->CI->template_lite->fetch("helper_latest_added_news", "user", "news");
	}
	*/
	#MOD#
	function get_latest_news_id($langs)
	{
		 $this->db->select('id');
		 $this->db->from('pg_news');
		 $this->db->where('id_lang', $langs);
		 $this->db->order_by('id', 'desc');
		 $this->db->limit(2);
		
		 $query = $this -> db -> get();
		 if($query -> num_rows>= 1)
		 {
			$news_latest_id = $query->result();
			return $news_latest_id;
		 }
		 else
		 {
			return false;
		 }
	}
	public function get_news_list_external($page=null, $items_on_page=null, $order_by=null, $params=array(), $filter_object_ids=null, $formated=true){
		/*original
		$this->DB->select(implode(", ", $this->fields_news_cute));
		$this->DB->from(NEWS_TABLE);
		*/
		#MOD FOR EXTERNAL NEWS#
		$this->DB->select(implode(", ", $this->internal_news));
		$this->DB->from('pg_news');	
		$this->DB->join('pg_news_feeds', 'pg_news.feed_id = pg_news_feeds.id', 'left');	
		#END OF EXTERNAL NEWS MOD#
		if(isset($params["where"]) && is_array($params["where"]) && count($params["where"])){
			foreach($params["where"] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])){
			foreach($params["where_in"] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])){
			foreach($params["where_sql"] as $value){
				$this->DB->where($value);
			}
		}
		#mod to put an alternative condition on a failed joint table to allow news#
		if(isset($params["or_where"]) && is_array($params["or_where"]) && count($params["or_where"])){
			$first=true;
			foreach($params["or_where"] as $field=>$value){
				if($first){
					$this->DB->or_where($field, $value);
					$first=false;
				}
				else
				{	
					$this->DB->where($field, $value);
				}
			}
		}
		#end of mod#
		if(isset($filter_object_ids) && is_array($filter_object_ids) && count($filter_object_ids)){
			$this->DB->where_in("id", $filter_object_ids);
		}

		if(is_array($order_by) && count($order_by)>0){
			foreach($order_by as $field => $dir){
				if(in_array($field, $this->fields_news)){
					$this->DB->order_by($field." ".$dir);
				}
			}
		}

		if(!is_null($page) ){
			$page = intval($page)?intval($page):1;
			$this->DB->limit($items_on_page, $items_on_page*($page-1));
		}
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			foreach($results as $r){
				$data[] = $r;
			}
			return ($formated)?$this->format_news($data):$data;
		}
		return array();
	}
	#END OF MOD#
	public function get_residential(){
		$this->DB->select(implode(", ", $this->residential));
		$this->DB->from(RESIDENTIAL);
		$results = $this->DB->get()->result_array();
		//return array();
		return $results;
	}
	
}
