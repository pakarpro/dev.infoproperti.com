{include file="header.tpl"}
{include file="news_panel.tpl" module="start"}
<div class="rc">
	<div class="content-block">
	<h1>
		{seotag tag='header_text'}
		<div class="fright"><a class="btn-link" href="{$site_url}news/rss">
		<ins class="with-icon i-rss"></ins>
		</a></div>
	</h1>
	{foreach item=item from=$news}
		<div class="news">
            <!-- {foreach item=residen from=$jono}
                Id Listing : {$residen.id_listing}<br/>
                Bd Room : {$residen.fe_bd_rooms_1}<br/>
                Bth Room : {$residen.fe_bth_rooms_1}<br/>
                Garages : {$residen.fe_garages_1}<br/>
                Total(s) Floors : {$residen.fe_total_floors_1} <br/>
            {/foreach} -->
			{if $item.img}
			<div class="image">
				<a href="{seolink module='news' method='view' data=$item}"><img src="{$item.media.img.thumbs.small}" align="left" /></a>
			</div>
			<div class="body">
			{/if}
			<span class="date">{$item.date_add|date_format:$page_data.date_format}</span>
			<h3><a href="{seolink module='news' method='view' data=$item}">{$item.name}</a></h3>
			
			<span class="annotation">{$item.annotation}</span><br>
			<div class="links">
				{if $item.feed}{l i='feed_source' gid='news'}: <a href="{$item.feed.site_link}">{$item.feed.title}</a><br>{/if}
			</div>
			{if $item.img}
			</div>
			{/if}
			<div class="clr"></div>
		</div>
	{foreachelse}
	<div class="empty">{l i="no_news_yet_header" gid='news'}</div>
	{/foreach}
		<div class="clr"></div>
	</div>
</div>
<div class="clr"></div>
{include file="footer.tpl"}
