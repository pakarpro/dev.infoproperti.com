{include file="header.tpl"}
{include file="news_panel.tpl" module="start"}
<!-- news detail view -->
<div class="rc">
	<div class="content-block">
		<h1>{seotag tag='header_text'}</h1>
		<div class="news-view">
			<span class="date">{$data.date_add|date_format:$page_data.date_format}</span>
            <!-- #MOD# only show big image if it's news original code is if $data.img  -->
			{if $data.img&&$data.news_type==news}
			<img src="{$data.media.img.thumbs.big}" align="left" />
			<div class="clr"></div>
			{/if}
			{if !$data.content}{$data.annotation}{else}{$data.content}{/if}
			{if $data.video_content.embed}
				<p>{$data.video_content.embed}</p>
			{/if}

			{if $data.feed_link}{l i='feed_source' gid='news'}: <a href="{$data.feed_link}">{$data.feed.title}</a>{/if}
			<div class="clr"></div>
			<br>
			<div class="a-link">
				<a href="{seolink module='news' method='index'}">{l i='link_back_to_news' gid='news'}</a>
			</div>
		</div>
	</div>

	{block name=show_social_networks_like module=social_networking}
	{block name=show_social_networks_share module=social_networking}
	{block name=show_social_networks_comments module=social_networking}
</div>
<div class="clr"></div>
{include file="footer.tpl"}
