{if $news}
<div id="news-n-fb" class="clearfix">
	<div id="news-section">
		<h2 class="head-title">{l i='header_latest_added_news' gid='news'}</h2>
        {if $news_privilege}
        	<a href="{$base_url}news-add_news_edit">Add news</a>
        {/if}        
		<!--<div class="latest_added_news_block">-->
				<div class="latestnews clearfix">
					{foreach item=item key=key from=$news_headline}
					<div class="news-entry clearfix">
                    <!-- old twin box style is used when no external feeds or news source exists-->
                    
                    {if !$external_news}
                        {if $item.img}
                            <div class="image"><img src="{$item.media.img.thumbs.headline}" style="margin-right:15px;" align="left" /></div>
                            {elseif $item.img eq NULL || $item.img eq ''}
                            <div class="image"><img src="{$base_url}uploads/news-logo/default/logo-headline{$item.image_randomizer}.jpg" style="margin-right:15px;" align="left" /></div>
                        {/if}
						<div class="body">
							<b>{$item.name|truncate:100}</b>
							{$item.annotation|truncate:180}<br />
							<a href="{seolink module='news' method='view' data=$item}">{l i='link_view_more' gid='news'}</a>
                        </div>
              		{else}	
                    <!-- else for when there is an external news items -->
                    	<div style="width:340px; float:left; margin:4px;">
                        	{if $item.img}
                            <div class="image"><img src="{$item.media.img.thumbs.headline}" style=" margin:0 auto;" align="center" /></div>
                            {elseif $item.img eq NULL || $item.img eq ''}
                            <div class="image"><img src="{$base_url}uploads/news-logo/default/logo-headline{$item.image_randomizer}.jpg" style=" margin:0 auto;" align="center" /></div>
                            {/if}
                            <div style="margin-top:10px;">
                                <b>{$item.name|truncate:100}</b><br/>
                                {$item.annotation|truncate:180}<br />
                                <a href="{seolink module='news' method='view' data=$item}">{l i='link_view_more' gid='news'}</a>
                            </div>
                        </div>
                        
                    {/if}
                    <!-- ORIGINAL TWIN BOX STYLE FOR HEADLINE
                    	<div style="width:340px; float:left; margin:4px;">
                        	{if $item.img}
                            <div class="image"><img src="{$item.media.img.thumbs.headline}" style=" margin:0 auto;" align="center" /></div>
                            {elseif $item.img eq NULL || $item.img eq ''}
                            <div class="image"><img src="{$base_url}uploads/news-logo/default/logo-headline{$item.image_randomizer}.jpg" style=" margin:0 auto;" align="center" /></div>
                            {/if}
                            <div style="margin-top:10px;">
                                <b>{$item.name|truncate:100}</b><br/>
                                {$item.annotation|truncate:180}<br />
                                <a href="{seolink module='news' method='view' data=$item}">{l i='link_view_more' gid='news'}</a>
                            </div>
                        </div>
                    -->
					{/foreach}
                    <!-- external news -->
					{foreach item=item key=key from=$external_news}
                    	<div style="width:340px; float:left; margin:4px;">
                        	{if $item.img}
                            <div class="image"><img src="{$item.media.img.thumbs.headline}" style=" margin:0 auto;" align="center" /></div>
                            {elseif $item.img eq NULL || $item.img eq ''}
                            <div class="image"><img src="{$base_url}uploads/news-logo/default/logo-headline2.jpg" style=" margin:0 auto;" align="center" /></div>
                            {/if}
                            <div style="margin-top:10px;">
                                <b>{$item.name|truncate:100}</b><br/>
                                {$item.annotation|truncate:180}<br />
                                <a href="{seolink module='news' method='view' data=$item}">{l i='link_view_more' gid='news'}</a>
                            </div>
                        </div>                    
					{/foreach} 
					</div>
                                       
					{foreach item=item key=key from=$news}
					<div class="news-entry clearfix">
						{if $item.img}
                        <div class="image">
                        	<a href="{seolink module='news' method='view' data=$item}">
                            <!-- mod -->
                            	<img src="{$item.media.img.thumbs.small}" align="left" />
                            <!-- end of mod -->
                            </a>
                        </div>
                        {else}
                        <div class="image">
                        	<a href="{seolink module='news' method='view' data=$item}">
                            <!-- mod -->
                            	<img src="{$base_url}uploads/news-logo/default/logo-small{$item.image_randomizer}.jpg" align="left" />
                            <!-- end of mod -->
                            </a>
                        </div>                        
                        {/if}
                        <!--
                        {if $item.img}
						<div class="body">
                        {/if}
                        -->
						<div class="body">                        
							<b>{$item.name|truncate:100}</b>
							{$item.annotation|truncate:180}<br />
							<a href="{seolink module='news' method='view' data=$item}">{l i='link_view_more' gid='news'}</a>
						</div>
					</div>
				{/foreach}
				</div>
		<!-- </div>-->
	</div>
	<!-- banner mod -->
	<div style="float:left; margin-left:50px; padding-top:76px;">
	<!--{helper func_name=show_banner_place module=banners func_param='right-news-banner'}-->
    {helper func_name=show_banner_place module=banners func_param='right-tall-news-bann'}
    </div>
    <!-- end of banner mod-->
	<!--<p><a href="{seolink module='news' method='index'}">{l i='link_read_more' gid='news'}</a></p>-->
</div>

{/if}