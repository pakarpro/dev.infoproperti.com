{if $news}
<div id="news-n-fb" class="clearfix">
	<div id="news-section">
		<h2 class="head-title">{l i='header_latest_added_news' gid='news'}</h2>
		<!--<div class="latest_added_news_block">-->
				<div class="latestnews clearfix">
					{foreach item=item key=key from=$news}
					<div class="news-entry clearfix">
						{if $item.img}
                        <div class="image">
                        	<a href="{seolink module='news' method='view' data=$item}">
                            <!-- mod -->
								{if $news_latest_id==$item.id}
                            	<img src="{$item.media.img.thumbs.headline}" style="margin-right:15px;" align="left" />
                                {else if}
                            	<img src="{$item.media.img.thumbs.small}" align="left" />
                                {/if}
                            <!-- end of mod -->
                            </a>
                        </div>
                        {/if}
						<div class="body">
							<b>{$item.name|truncate:100}</b>
							{$item.annotation|truncate:180}<br />
							<a href="{seolink module='news' method='view' data=$item}">{l i='link_view_more' gid='news'}</a>
						{if $item.img}
						</div>
						{/if}		
					</div>
				{/foreach}
				</div>
		<!-- </div>-->
	</div>

	<!--<p><a href="{seolink module='news' method='index'}">{l i='link_read_more' gid='news'}</a></p>-->
</div>

{/if}