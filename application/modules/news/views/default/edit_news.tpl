{include file="header.tpl"}
<!-- MOD -->
<script type='text/javascript' src='http://pakarpro.com/application/js/jquery-ui.custom.min.js'></script>
<script type='text/javascript' src='http://pakarpro.com/application/js/jquery.jeditable.mini.js'></script>
<LINK href='http://pakarpro.com/application/js/jquery-ui/jquery-ui.custom.css' rel='stylesheet' type='text/css'>


<!-- end of mod -->
<form method="post" action="{$data.action}" name="save_form" enctype="multipart/form-data">
	<div class="edit-form n150">
		<div class="row header">{if $data.id}{l i='admin_header_news_change' gid='news'}{else}{l i='admin_header_news_add' gid='news'}{/if}</div>
		<div class="row">
			<div class="h">{l i='field_name' gid='news'}:&nbsp;* </div>
			<div class="v"><input type="text" value="{$data.name}" name="name" class="long"></div>
		</div>
		<div class="row">
			<div class="h">{l i='field_gid' gid='news'}:&nbsp;* </div>
			<div class="v"><input type="text" value="{$data.gid}" name="gid" class="long"></div>
		</div>
        <!-- mod for tags -->
		<div class="row">
			<div class="h">{l i='field_news_tags' gid='news'}:&nbsp; </div>
			<div class="v"><input type="text" value="" name="tags" class="long"></div>
		</div>
		<div class="row">
			<div class="h"></div>
			<div class="v">
            	{foreach item=item from=$existing_tag}
                <div style="min-height:10px; display:inline-block; position:relative; margin-right:5px; background-color:white; border-bottom:1px solid black;">
                	<div style="position:relative; float:left; margin-right:5px;">
                		{$item.tag_name}                    
                    </div>
                    <a href="{$site_url}admin/news/delete_news_tag/{$item.news_id}/{$item.tag_id}">
                        <div style="position:relative; float:right; text-align:right;">
                                X
                        </div>
                    </a>                                        
                </div>
                {/foreach}
            </div>
		</div>        
        <!-- end of mod -->        
		<div class="row">
			<div class="h">{l i='field_news_lang' gid='news'}: </div>
			<div class="v"><select name="id_lang">{foreach item=item from=$languages}<option value="{$item.id}"{if $item.id eq $data.id_lang} selected{/if}>{$item.name}</option>{/foreach}</select></div>
		</div>
		<div class="row">
			<div class="h">{l i='field_icon' gid='news'}: </div>
			<div class="v">
				<input type="file" name="news_icon">
				{if $data.img}
				<br><img src="{$data.media.img.thumbs.small}"  hspace="2" vspace="2" />
				<br><input type="checkbox" name="news_icon_delete" value="1" id="uichb"><label for="uichb">{l i='field_icon_delete' gid='news'}</label>
				{/if}
			</div>
		</div>
		<div class="row">
			<div class="h">{l i='field_video' gid='news'}: </div>
			<div class="v">
				<input type="file" name="news_video">
				{if $data.video}
					<br>{l i='field_video_status' gid='news'}: 
					{if $data.video_data.status eq 'end' && $data.video_data.errors}
						<font color="red">{foreach item=item from=$data.video_data.errors}{$item}<br>{/foreach}</font>
					{elseif $data.video_data.status eq 'end'}	<font color="green">{l i='field_video_status_end' gid='news'}</font><br>
					{elseif $data.video_data.status eq 'images'}	<font color="yellow">{l i='field_video_status_images' gid='news'}</font><br>
					{elseif $data.video_data.status eq 'waiting'} <font color="yellow">{l i='field_video_status_waiting' gid='news'}</font><br>
					{elseif $data.video_data.status eq 'start'} <font color="yellow">{l i='field_video_status_start' gid='news'}</font><br>
					{/if}
					{if $data.video_content.thumbs.small}
					<br><img src="{$data.video_content.thumbs.small}"  hspace="2" vspace="2" />
					{/if}
					<br><input type="checkbox" name="news_video_delete" value="1" id="uvchb"><label for="uvchb">{l i='field_video_delete' gid='news'}</label>
				{/if}
			</div>
		</div>
		<div class="row">
			<div class="h">{l i='field_annotation' gid='news'}: </div>
			<div class="v"><textarea name="annotation">{$data.annotation}</textarea><br><i>{l i='field_annotation_text' gid='news'}</i></div>
		</div>
		<div class="row">
			<div class="h">{l i='field_content' gid='news'}: </div>
			<div class="v">{$data.content_fck}&nbsp;</div>
		</div>
		<!--MOD FOR SCHEDULING-->
		<div class="row">
			<div class="h">Scheduling</div>
			<div class="v">
            	<input type="checkbox" name="scheduling_flag" {if $data.scheduling && $data.scheduling ne '0000-00-00'}checked='checked'{/if} value="1"><label>Use scheduling</label><br />
				<input type="text" name="scheduling" id="scheduling" {if $data.scheduling ne '0000-00-00'}value="{$data.scheduling}"{/if} class="middle" /> 
				<input type="hidden" name="scheduling_hidden" {if $data.scheduling ne '0000-00-00'}value="{$data.scheduling}"{/if} id="scheduling_hidden">
                <select name="scheduling_time">
                	<option value="{$current_time}">{$current_time}</option>
                	<option value="00:00:00">00:00:00</option>
                    <option value="00:30:00">00:30:00</option>
                    <option value="01:00:00">01:00:00</option>
                    <option value="01:30:00">01:30:00</option>
                	<option value="02:00:00">02:00:00</option>
                    <option value="02:30:00">02:30:00</option>
                    <option value="03:00:00">03:00:00</option>
                    <option value="03:30:00">03:30:00</option>
                	<option value="04:00:00">04:00:00</option>
                    <option value="04:30:00">04:30:00</option>
                    <option value="05:00:00">05:00:00</option>
                    <option value="05:30:00">05:30:00</option>
                	<option value="06:00:00">06:00:00</option>
                    <option value="06:30:00">06:30:00</option>
                    <option value="07:00:00">07:00:00</option>
                    <option value="07:30:00">07:30:00</option>
                	<option value="08:00:00">08:00:00</option>
                    <option value="08:30:00">08:30:00</option>
                    <option value="09:00:00">09:00:00</option>
                    <option value="09:30:00">09:30:00</option>
                	<option value="10:00:00">10:00:00</option>
                    <option value="10:30:00">10:30:00</option>
                    <option value="11:00:00">11:00:00</option>
                    <option value="11:30:00">11:30:00</option>          
                	<option value="12:00:00">12:00:00</option>
                    <option value="12:30:00">12:30:00</option>
                    <option value="13:00:00">13:00:00</option>
                    <option value="13:30:00">13:30:00</option>
                	<option value="14:00:00">14:00:00</option>
                    <option value="14:30:00">14:30:00</option>
                    <option value="15:00:00">15:00:00</option>
                    <option value="15:30:00">15:30:00</option>
                	<option value="16:00:00">16:00:00</option>
                    <option value="16:30:00">16:30:00</option>
                    <option value="17:00:00">17:00:00</option>
                    <option value="17:30:00">17:30:00</option>
                	<option value="18:00:00">18:00:00</option>
                    <option value="18:30:00">18:30:00</option>
                    <option value="19:00:00">19:00:00</option>
                    <option value="19:30:00">19:30:00</option>
                	<option value="20:00:00">20:00:00</option>
                    <option value="20:30:00">20:30:00</option>
                    <option value="21:00:00">21:00:00</option>
                    <option value="21:30:00">21:30:00</option>
                	<option value="22:00:00">22:00:00</option>
                    <option value="22:30:00">22:30:00</option>
                    <option value="23:00:00">23:00:00</option>
                    <option value="23:30:00">23:30:00</option>                                                  
                </select>
                
				<script>{literal}
					$(function(){
						$('#scheduling').datepicker({dateFormat: '{/literal}{$page_data.datepicker_date_format}{literal}', altFormat: '{/literal}{$page_data.datepicker_alt_format}{literal}', altField: '#scheduling_hidden', showOn: 'both'});
					});
				{/literal}</script>
			</div>
		</div>
        <!--END OF SCHEDULING MOD-->        
	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="{l i='btn_save' gid='start' type='button'}"></div></div>
	<a class="cancel" href="{$site_url}admin/news">{l i='btn_cancel' gid='start'}</a>
</form>
<div class="clr"></div>
<script>{literal}
$(function(){
	$("div.row:odd").addClass("zebra");
});
{/literal}</script>

{include file="footer.tpl"}
