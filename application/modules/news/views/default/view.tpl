{include file="header.tpl"}
<!-- fb share feature -->
<div id="fb-root"></div>
<script language="javascript">
{literal}
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '868222196624062',
      xfbml      : true,
      version    : 'v2.5'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

   //twitter js
	!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');

	//google+ JS
  (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/platform.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
{/literal}
   
   
{/literal}
</script>
<!-- end of fb share mod -->
{include file="news_panel.tpl" module="start"}
<!-- news detail view -->
<div class="rc">
	<div class="content-block">
    	<!-- mod for tags -->
        {if $existing_tag}
        Tag:&nbsp;        
            {foreach item=item from=$existing_tag}
                <div style="min-height:10px; display:inline-block; position:relative; margin-right:5px; background-color:white; border-bottom:1px solid black;">
                    <a href="{$site_url}news/tags/{$item.tag_name}">
                        <div style="position:relative; float:left;">
                            {$item.tag_name}                    
                        </div>
                    </a>                                        
                </div>
            {/foreach}
        {/if}    
        <!-- end of mod -->    
		<h1>
        	{seotag tag='header_text'}
            <br/>
            <!-- Twitter tweet button -->
            <a href="https://twitter.com/share" class="twitter-share-button" data-text="{seotag tag='header_text'}" data-via="inproperti" data-hashtags="infoproperti, rumah"{if $googl_url}data-url="{$googl_url}"{/if}>Tweet</a>                
            <!-- FB share button -->
            <div class="fb-share-button" data-href="{$fb_share_url}" data-layout="button_count" style="bottom:9px;"></div> 
            <!-- Place this tag where you want the +1 button to render. -->
            <div class="g-plusone" data-href="{$fb_share_url}" data-size="medium"></div>                    
        </h1>
		<div class="news-view">
			<span class="date">{$data.date_add|date_format:$page_data.date_format}</span>
            <!-- #MOD# only show big image if it's news original code is if $data.img  -->
			
            {if getimagesize($data.media.img.thumbs.big)&&$data.news_type=='news'}
			<img src="{$data.media.img.thumbs.big}" align="left" />
			<div class="clr"></div>
			{/if}
			{if !$data.content}{$data.annotation}{else}{$data.content}{/if}
			{if $data.video_content.embed}
				<p>{$data.video_content.embed}</p>
			{/if}

			{if $data.feed_link}{l i='feed_source' gid='news'}: <a href="{$data.feed_link}">{$data.feed.title}</a>{/if}
			<div class="clr"></div>
			<br>
			<div class="a-link">
				<a href="{seolink module='news' method='index'}">{l i='link_back_to_news' gid='news'}</a>
			</div>
		</div>
        <div class="fb-comments" data-href="{$current_url}" data-width="783" data-numposts="10"></div>        
	</div>

	{block name=show_social_networks_like module=social_networking}
	{block name=show_social_networks_share module=social_networking}
	{block name=show_social_networks_comments module=social_networking}
</div>
<div class="clr"></div>
{include file="footer.tpl"}
