{include file="header.tpl"}
{helper func_name=get_admin_level1_menu helper_name=menu func_param='admin_news_menu'}
<div class="actions">
	<ul>
		<li><div class="l"><a href="{$site_url}admin/news/edit">{l i='link_add_news' gid='news'}</a></div></li>
	</ul>
	&nbsp;
</div>

<div class="menu-level3">
	<ul>
		{foreach item=item key=lid from=$languages}
		<li class="{if $lid eq $id_lang}active{/if}{if !$filter_data[$lid]} hide{/if}"><a href="{$site_url}admin/news/index/{$lid}">{$item.name} ({$filter_data[$lid]})</a></li>
		{/foreach}
	</ul>
	&nbsp;
</div>
    <form action="" method="post" enctype="multipart/form-data">
    <table>
        <tr>
        	<td><input type="submit" value="search" name="btn_save"/></td>
            <td><input type="text" name="search" value="{$search}"/></td>
        </tr>
	{if $search ne ''}
        <tr>
        	<td></td>
            <td align="right"><input type="submit" name="cancel_search" value="Cancel search"/></td>
        </tr>
    {/if}                
    </table>
    </form>
    
<table cellspacing="0" cellpadding="0" class="data" width="100%">
<tr>
	<th class="first w150">Tags</th>
</tr>

{counter print=false assign=counter}
<tr{if $counter is div by 2} class="zebra"{/if}>
	<td>
    {foreach item=item from=$tags}
                <div style="min-height:10px; display:inline-block; position:relative; margin-right:5px; background-color:white; border-bottom:1px solid black;">
                	<div style="position:relative; float:left; margin-right:5px;">
                		{$item.tag_name}                    
                    </div>
                    <a href="{$site_url}admin/news/delete_tag/{$item.tag_id}">
                        <div style="position:relative; float:right; text-align:right;">
                                X
                        </div>
                    </a>                                        
                </div>,
    {foreachelse}
		No tags
	{/foreach}
    </td>
</tr>
</table>
{include file="pagination.tpl"}


{include file="footer.tpl"}
