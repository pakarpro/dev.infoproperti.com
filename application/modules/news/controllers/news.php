<?php
/**
* News user side controller
*
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/


Class News extends Controller
{
	/**
	 * link to CodeIgniter object
	 * @var object
	 */

	/**
	 * Constructor
	 */
	function __construct(){
		parent::Controller();
		$this->load->model("News_model");

		$this->load->model('Menu_model');
		$this->Menu_model->breadcrumbs_set_parent('footer-menu-news-item');
	}

	public function index($page=1){
		$attrs = array();
		$attrs["where"]["id_lang"] = $this->pg_language->current_lang_id;
		$attrs["where"]["status"] = "1";
		#mod for news search#
		if($this->input->post("btn_save"))
		{
			//hashtag results in searching tag
			if(preg_match('/\#/',$this->input->post('search')))
			{
				$tag_search = preg_replace('/\#/','', $this->input->post('search'));
				redirect(site_url()."news/tags/".$tag_search);
			}
			else
			{
				$search = $this->input->post('search');
				redirect(site_url()."news/search/".$search);
			}
			#$attrs["like"]["name"] = '%'.$this->input->post('search').'%';
		}	
		#end for mod#		
		
		$news_count = $this->News_model->get_news_count($attrs);

		$items_on_page = $this->pg_module->get_module_config('news', 'userside_items_per_page');
		$this->load->helper('sort_order');
		$page = get_exists_page_number($page, $news_count, $items_on_page);

		if ($news_count > 0){
			$news = $this->News_model->get_news_list($page, $items_on_page, array('date_add' => "DESC"), $attrs);
			$this->template_lite->assign('news', $news);
		}
		$this->load->helper("navigation");
		$this->config->load('date_formats', TRUE);
		$url = rewrite_link('news', 'index')."/";
		$page_data = get_user_pages_data($url, $news_count, $items_on_page, $page, 'briefPage');
		$page_data["date_format"] = $this->config->item('st_format_date_time_literal', 'date_formats');
		$this->template_lite->assign('page_data', $page_data);
				
		$this->template_lite->view('list');
	}

	public function view($id){
		if(!$id) show_404();
		//get current URL for facebook plugin
		$current_url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		$this->template_lite->assign('current_url', $current_url);
		#MOD FOR GOO.GL#
		//get goo.gl link from database
		$googl_url = $this->News_model->get_short_link($id);
		$this->template_lite->assign('googl_url', $googl_url);	
		#print_r($googl_url);exit;	
		if($googl_url==""||$googl_url==NULL)
		{
			$googl_url = file_get_contents(base_url().'ext/plugins/googl/shorten_url.php?url='.$current_url);
			$this->template_lite->assign('googl_url', $googl_url);	
			$short_link = array(
				"news_id" => $id,
				"link" => $googl_url
			);
			$this->News_model->save_short_link($short_link);
		}
		#END OF MOD FOR GOO.GL#
		
		$news = $this->News_model->get_news_by_id($id);
		
		#mod for tags
		//grab tags for associated news
		$existing_tag = $this->News_model->get_tags_by_news($id);
		$this->template_lite->assign('existing_tag', $existing_tag);
		#end of mod for tags		
		
		if(!$news) show_404();
		$news = $this->News_model->format_single_news($news);
		$this->template_lite->assign('data', $news);

		$this->News_model->get_last_news(2);

		$this->config->load('date_formats', TRUE);
		$page_data["date_format"] = $this->config->item('st_format_date_time_literal', 'date_formats');
		$this->template_lite->assign('page_data', $page_data);
		#mod for fb share url#
		$fb_share_url = $_SERVER['REQUEST_URI'];
		$this->template_lite->assign('fb_share_url', $fb_share_url);
		#mod for fb share
		$this->pg_seo->set_seo_data($news);

		$this->Menu_model->breadcrumbs_set_active($news['name']);
		$this->Menu_model->get_breadcrumbs();
		$this->template_lite->view('view');
	}

	public function rss(){
		$rss_settings = $this->News_model->get_rss_settings();
		$this->load->library('rssfeed');
		$current_lang = $this->pg_language->languages[$this->pg_language->current_lang_id];

		$this->rssfeed->set_channel(
			site_url(),
			$rss_settings["rss_feed_channel_title"],
			$rss_settings["rss_feed_channel_description"],
			$current_lang["code"]
		);

		if($rss_settings["rss_feed_image_url"]){
			$this->rssfeed->set_image(
				$rss_settings["rss_feed_image_media"]["thumbs"]["rss"],
				$rss_settings["rss_feed_image_title"],
				site_url()
			);
		}

		$attrs["where"]["id_lang"] = $this->pg_language->current_lang_id;
		$attrs["where"]["status"] = "1";
		if(!$rss_settings["rss_use_feeds_news"]){
			$attrs["where"]["feed_id"] = "";
		}

		$news = $this->News_model->get_news_list(1, $rss_settings["rss_news_max_count"], array('date_add' => "DESC"), $attrs);
		if(!empty($news)){
			$this->load->helper('seo');
			foreach($news as $item){
				$url = rewrite_link("news", "view", $item);
				$this->rssfeed->set_item($url, $item["name"], $item["annotation"], $item["date_add"]);
			}
		}
		$this->rssfeed->send();
		return;
	}
#MOD TO ADD NEWS CMS#
	public function edit($id=null){
		if($this->session->userdata('news_privilege')!=1)
		{
			show_404();
		}
		else
		{
			date_default_timezone_set("Asia/Jakarta");
			$current_date = date("Y-m-d H:i:s");
			$current_time = date("H:i:s");
			if($id){
				$data = $this->News_model->get_news_by_id($id);
				#mod for tags
				//grab tags for associated news
				$existing_tag = $this->News_model->get_tags_by_news($id);
				$this->template_lite->assign('existing_tag', $existing_tag);
				#print_r($existing_tag);exit;
				#end of mod for tags				
			}else{
				$data["id_lang"] = $this->pg_language->current_lang_id;
			}
			if($this->input->post('btn_save')){
				$post_data = array(
					"name" => $this->input->post('name', true),
					"gid" => $this->input->post('gid', true),
					"id_lang" => $this->input->post('id_lang', true),
					"annotation" => $this->input->post('annotation', true),
					"content" => $this->input->post('content', true),
					"news_type" => "news",
					"scheduling" => $this->input->post('scheduling_hidden', true),
					"scheduling_flag" => $this->input->post('scheduling_flag', true),
					"scheduling_time" => $this->input->post('scheduling_time', true)
				);
				$validate_data = $this->News_model->validate_news($id, $post_data, 'news_icon', 'news_video');
				#print_r($validate_data);exit;
				if(!empty($validate_data["errors"])){
					$this->system_messages->add_message('error', $validate_data["errors"]);
				}else{
	
					if($this->input->post('news_icon_delete') && $id && $data["img"]){
						$this->load->model("Uploads_model");
						$format = $this->News_model->format_single_news($data);
						$this->Uploads_model->delete_upload($this->News_model->upload_config_id, $format["prefix"], $format["img"]);
						$validate_data["data"]["img"] = '';
					}
	
					if($this->input->post('news_video_delete') && $id && $data["video"]){
						$this->load->model("Video_uploads_model");
						$format = $this->News_model->format_single_news($data);
						$this->Video_uploads_model->delete_upload($this->News_model->video_config_id, $format["prefix"], $format["video"], $format["video_image"], $format["video_data"]["data"]["upload_type"]);
						$validate_data["data"]["video"] = $validate_data["data"]["video_image"] = $validate_data["data"]["video_data"] = '';
					}
	
					$flag_add = empty($id)?true:false;
					if($flag_add){
						$validate_data["data"]["status"] = 1;
					}
					$id = $this->News_model->save_news($id, $validate_data["data"], 'news_icon', 'news_video');


				#mod for tags#
				$tags = $this->input->post('tags', true);
				if(!empty($tags)){
					$tags = explode(",",$tags);
					foreach($tags as $key => $tag){
						$tag = trim($tag);
						//check tag if it exists on database or not
						$tag_check = $this->News_model->get_tags_by_value($tag);
						$tag_check = $tag_check['tag_id'];
						#print_r($tag_check);exit;
						if($tag_check){
							$tag_id=$tag_check;
							$news_id=$id;
							$news_tags_exists = $this->News_model->check_exist_news_tags($tag_id, $news_id);
							if(!$news_tags_exists)
							{
								$news_tag = array(
									"tag_id" => $tag_check,
									"news_id" => $id
								);
								$news_tag_id = $this->News_model->save_news_tags($news_tag);
							}
						}
						else
						{
							$save_tag = array(
								"tag_name" => $tag
							);
							$tag_id = $this->News_model->save_tags($save_tag);
							$news_tag = array(
								"tag_id" => $tag_id,
								"news_id" => $id
							);
							$news_tag_id = $this->News_model->save_news_tags($news_tag);											
						}
						#print_r($tag_check);echo '<br/>';
					}
				}
				#print_r($tags);exit;
				#end of mod#						



					$this->system_messages->add_message('success', (!$flag_add)?l('success_update_news', 'news'):l('success_add_news', 'news'));
					$cur_set = $_SESSION["news_list"];
					redirect(site_url()."news-add_news_edit/".$cur_set["id_lang"]."/".$cur_set["order"]."/".$cur_set["order_direction"]."/".$cur_set["page"]);
				}
				$data = array_merge($data, $validate_data["data"]);
			}
			//mod to add time picker
			$hour = 0;
			$flag = 0;
			while($hour<24)
			{
				$time['hour'][$flag] = sprintf("%02d", $hour);
				if($flag%2!=0)
				{
					$time['time'][$flag] = $time['hour'][$flag].':30:00';
					$hour++;
				}
				else
				{
					$time['time'][$flag] = $time['hour'][$flag].':00:00';
				}
				$flag++;	
			}
			//end of mod
			$data = $this->News_model->format_single_news($data);
			//mod for date picker
			$this->config->load('date_formats', TRUE);
					$page_data['date_format'] = $this->config->item('st_format_date_literal', 'date_formats');
					$page_data['date_time_format'] = $this->config->item('st_format_date_time_literal', 'date_formats');
					$page_data['datepicker_date_format'] = $this->config->item('ui_format_date_literal', 'date_formats');
					$page_data['datepicker_alt_format'] = $this->config->item('ui_format_date_numeric', 'date_formats');
			//end of mod		
			#print_r($page_data);exit;
			$this->load->plugin('fckeditor');
			$data["content_fck"] = create_editor("content", isset($data["content"]) ? $data["content"] : "", 550, 400, 'Middle');
			$this->template_lite->assign('data', $data);
			$this->template_lite->assign('page_data', $page_data);
			$this->template_lite->assign('languages', $this->pg_language->languages);
			$this->template_lite->assign('current_time', $current_time);
			$this->Menu_model->set_menu_active_item('admin_news_menu', 'news_list_item');
			$this->system_messages->set_data('header', l('admin_header_news_list', 'news'));
			$this->template_lite->view('edit_news');
		}
	}

#END OF MOD#

#MOD FOR NEWS TAGS#
	public function tags($tag='', $page=1){
		//get list of news id from here to put into param using join table
		$tag_id = $this->News_model->get_tag_id($tag);
		$tag_id = $tag_id[0]['tag_id'];
		#print_r($tag_id);exit;
		$attrs = array();
		$attrs["where"]["pg_news.id_lang"] = $this->pg_language->current_lang_id;
		$attrs["where"]["pg_news.status"] = "1";
		$attrs["where"]["tag_id"] = $tag_id;
		$news_count = $this->News_model->get_news_count_by_tag($attrs);
		#print_r($news_count);exit;
		$items_on_page = 10;
		$this->load->helper('sort_order');
		$page = get_exists_page_number($page, $news_count, $items_on_page);
		#print_r($page);exit;
		if ($news_count > 0){
			$news = $this->News_model->get_news_list_by_tag($page, $items_on_page, array('date_add' => "DESC"), $attrs);
			$this->template_lite->assign('news', $news);
		}
		#print_r($news);exit;
		$this->load->helper("navigation");
		$this->config->load('date_formats', TRUE);
		$url = site_url()."news/tags/".$tag."/";		
		#$url = rewrite_link('news', 'index')."/";
		$page_data = get_user_pages_data($url, $news_count, $items_on_page, $page, 'briefPage');
		$page_data["date_format"] = $this->config->item('st_format_date_time_literal', 'date_formats');
		$this->template_lite->assign('page_data', $page_data);
				
		$this->template_lite->view('list');
	}

#END OF MOD#

#MOD FOR NEWS SEARCH#
	public function search($search='', $page=1){
		$query = '%'.$search.'%';
		$query = preg_replace('/\s/','%',$query);
		$attrs = array();
		$attrs["where"]["pg_news.id_lang"] = $this->pg_language->current_lang_id;
		$attrs["where"]["pg_news.status"] = "1";
		$attrs["like"]["name"] = $query;		
		$news_count = $this->News_model->get_news_count($attrs);
		#print_r($news_count);exit;
		$items_on_page = 10;
		$this->load->helper('sort_order');
		$page = get_exists_page_number($page, $news_count, $items_on_page);
		#print_r($page);exit;
		if ($news_count > 0){
			$news = $this->News_model->get_news_list($page, $items_on_page, array('date_add' => "DESC"), $attrs);
			$this->template_lite->assign('news', $news);
		}
		#print_r($news);exit;
		$this->load->helper("navigation");
		$this->config->load('date_formats', TRUE);
		if($search!=NULL||$search!=''){
			$url = site_url()."news/search/".$search."/";	
		}
		else
		{
			$url = site_url()."news/index/";	
		}
		#$url = rewrite_link('news', 'index')."/";
		$page_data = get_user_pages_data($url, $news_count, $items_on_page, $page, 'briefPage');
		$page_data["date_format"] = $this->config->item('st_format_date_time_literal', 'date_formats');
		$this->template_lite->assign('page_data', $page_data);
				
		$this->template_lite->view('list');
	}

#END OF MOD#
}
