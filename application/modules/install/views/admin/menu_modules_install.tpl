{if $auth_type eq 'module'}
	<div class="menu">
		<div class="t">
			<div class="b">
				<ul>
					<li{if $step eq 'modules'} class="active"{/if}><div class="r"><a href="{$site_url}admin/install/modules">Installed modules</a></div></li>
					<li{if $step eq 'enable_modules'} class="active"{/if}><div class="r">
						{if $enabled && $step ne 'enable_modules'}<div class="num"><div class="l">{$enabled}</div></div>{/if}
						<a href="{$site_url}admin/install/enable_modules">Enable modules</a>
					</div></li>
					<li{if $step eq 'updates'} class="active"{/if}><div class="r">
						{if $updates && $step ne 'updates'}<div class="num"><div class="l">{$updates}</div></div>{/if}
						<a href="{$site_url}admin/install/updates">Enable updates</a>
					</div></li>
					<li{if $step eq 'product_updates'} class="active"{/if}><div class="r">
						{if $product_updates && $step ne 'product_updates'}<div class="num"><div class="l">{$product_updates}</div></div>{/if}
						<a href="{$site_url}admin/install/product_updates">Enable product updates</a>
					</div></li>
					<li{if $step eq 'libraries'} class="active"{/if}><div class="r"><a href="{$site_url}admin/install/libraries">Installed libraries</a></div></li>
					<li{if $step eq 'enable_libraries'} class="active"{/if}><div class="r">
						{if $enabled_libraries && $step ne 'enable_libraries'}<div class="num"><div class="l">{$enabled_libraries}</div></div>{/if}
						<a href="{$site_url}admin/install/enable_libraries">Enable libraries</a>
					</div></li>
					<li{if $step eq 'langs'} class="active"{/if}><div class="r">
						<a href="{$site_url}admin/install/langs">Languages</a>
					</div></li>
					<li{if $step eq 'ftp'} class="active"{/if}><div class="r"><a href="{$site_url}admin/install/installer_settings">Panel settings</a></div></li>
				</ul>
			</div>
		</div>
	</div>
{/if}