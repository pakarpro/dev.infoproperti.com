<?php
/**
* Upload Gallery admin side controller
* 
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/
class Admin_Upload_gallery extends Controller{
	/**
	 * Allow create gallery 
	 */
	private $allow_gallery_add = false;

	/**
	 * Constructor
	 */
	public function __construct(){
		parent::Controller();
		$this->load->model("Menu_model");
		$this->Menu_model->set_menu_active_item("admin_menu", "uploads-items");
		$this->load->model("Upload_gallery_model");
	}
	
	/**
	 * Render index gallery action
	 */
	public function index(){
		$types = $this->Upload_gallery_model->get_types_list();
		$this->template_lite->assign("types", $types);

		$this->config->load("date_formats", TRUE);
		$page_data["date_format"] = $this->config->item("st_format_date_time_literal", "date_formats");
		$this->template_lite->assign("page_data", $page_data);

		$this->template_lite->assign("allow_add", $this->allow_gallery_add);
		$this->system_messages->set_data("header", l("admin_header_list", "upload_gallery"));
		$this->template_lite->view("list");
	}

	/**
	 * Edit gallery action
	 * @param integer $id
	 */
	public function edit($id=null){
		$this->load->model("Upload_gallery_model");
		$data = ($id)?$this->Upload_gallery_model->get_type_by_id($id):array();

		if($this->input->post("btn_save")){
			$post_data = $this->input->post("data", true);
			
			if(!$this->allow_gallery_add && isset($post_data["gid"])) unset($post_data["gid"]);
				
			$validate_data = $this->Upload_gallery_model->validate_type($id, $post_data);
			if(!empty($validate_data["errors"])){
				$this->system_messages->add_message("error", $validate_data["errors"]);
			}else{
				if($id){
					$this->Upload_gallery_model->set_type($id, $validate_data["data"]);
					$this->system_messages->add_message("success", l("success_update_type", "upload_gallery"));
				}else{
					$this->Upload_gallery_model->set_type($id, $validate_data["data"]);
					$this->system_messages->add_message("success", l("success_update_type", "upload_gallery"));
				}
				redirect(site_url()."admin/upload_gallery/index");
			}
			$data = array_merge($data, $validate_data["data"]);
		}
		$this->template_lite->assign("data", $data);
		$this->template_lite->assign("allow_add", $this->allow_gallery_add);

		///// Configs
		$this->load->model("uploads/models/Uploads_config_model");
		$settings = $this->Uploads_config_model->get_config_list();
		$this->template_lite->assign("upload_config", $settings);

		$this->system_messages->set_data("header", l("admin_header_edit", "upload_gallery"));
		$this->template_lite->view("edit_form");
	}

	/**
	 * Remove gallery action
	 * @param integer $id
	 */
	public function delete($id){
		if(!empty($id)){
			$this->Upload_gallery_model->delete_type($id);
		}
		redirect(site_url()."admin/upload_gallery/index");
	}

}
