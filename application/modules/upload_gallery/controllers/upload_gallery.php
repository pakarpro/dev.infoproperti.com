<?php
/**
* Upload gallery user side controller
* 
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/

class Upload_gallery extends Controller{
		
	/**
	 * Recrop upload
	 * @param integer $upload_id upload identifier
	 * @param string $thumb_prefix thumb prefix
	 */
	public function ajax_recrop($upload_id, $thumb_prefix=''){
		$this->load->model("Upload_gallery_model");	
		if(!$thumb_prefix) $thumb_prefix = $this->input->post("prefix", true);	
		$this->Upload_gallery_model->recrop_file($upload_id, $thumb_prefix);
	}
	
	/**
	 * Rotate upload
	 * @param integer $upload_id upload identifier
	 * @param integer/string $angle rotate angle
	 */
	public function ajax_rotate($upload_id, $angle=90){
		$this->load->model("Upload_gallery_model");
	
		if($angle < 0) $angle += 360;
	
		$data = $this->Upload_gallery_model->rotate_file($upload_id, $angle);
		
		echo json_encode(array("width"=>$data["settings"]["width"], "height"=>$data["settings"]["height"]));
		exit;
	}
}

