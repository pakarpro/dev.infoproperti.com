<?php
$module["module"] = "upload_gallery";
$module["install_name"] = "Upload gallery";
$module["install_descr"] = "Gallery types & settings";
$module["version"] = "2.01";
$module["files"] = array(
	array("file", "read", "application/modules/upload_gallery/controllers/admin_upload_gallery.php"),
	array("file", "read", "application/modules/upload_gallery/controllers/upload_gallery.php"),
	array("file", "read", "application/modules/upload_gallery/install/module.php"),
	array("file", "read", "application/modules/upload_gallery/install/permissions.php"),
	array("file", "read", "application/modules/upload_gallery/install/settings.php"),
	array("file", "read", "application/modules/upload_gallery/install/structure_deinstall.sql"),
	array("file", "read", "application/modules/upload_gallery/install/structure_install.sql"),
	array("file", "read", "application/modules/upload_gallery/models/upload_gallery_install_model.php"),
	array("file", "read", "application/modules/upload_gallery/models/upload_gallery_model.php"),
	array("file", "read", "application/modules/upload_gallery/views/admin/edit_form.tpl"),
	array("file", "read", "application/modules/upload_gallery/views/admin/list.tpl"),
	array("file", "read", "application/modules/upload_gallery/views/admin/moder_block.tpl"),
	
	array("dir", "read", "application/modules/upload_gallery/langs"),
);
$module["dependences"] = array(
	"menu" 			=> array("version"=>"1.01"),
	"start" 		=> array("version"=>"1.01"),
	"uploads" 		=> array("version"=>"1.01"),
	"moderation" 	=> array("version"=>"1.01"),	
);

$module["linked_modules"] = array(
	"install" => array(
		"menu"				=> "install_menu",
		"uploads"			=> "install_uploads",
		"moderation"		=> "install_moderation",
	),
	"deinstall" => array(
		"menu"				=> "deinstall_menu",
		"uploads"			=> "deinstall_uploads",
		"moderation"		=> "deinstall_moderation",
	),
);
