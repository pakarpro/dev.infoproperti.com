function photoEdit(optionArr){
	this.properties = {
		siteUrl: '',
		imageWidth: 0,
		imageHeight: 0,
		idPhoto: 0,
		viewUrl: 'upload_gallery/ajax_view/',
		commentUrl: 'upload_gallery/ajax_comment/',
		rotateUrl: 'upload_gallery/ajax_rotate/',
		recropUrl: 'upload_gallery/ajax_recrop/',
		saveUrl: 'listings/ajax_save_photo_data/',
		recropContainer: '#photo_source_recrop_box',
		parentImage: '',
		parentThumb: '',
		viewAjaxUrl: 'upload_gallery/ajax_get_section/',
		listBlockId: 'photo_edit_block',
		sectionId: 'photo_edit_sections',
		tryDisplayWithoutAjax: true,
		containerPrefix:'content_',
		errorObj: new Errors(),
		available_view: new available_view(),
		currentSection: 'm_view',
		idListing: 0,
	};
	
	this.imageareaselect = null;
	
	this.selections = [];
	
	this.index = 0;
	
	var _self = this;

	this.Init = function(options){
		_self.properties = $.extend(_self.properties, options);

		$('.imgareaselect-outer').live('click', function(){
			if(!_self.imageareaselect) return false;
			var selection = _self.imageareaselect.getSelection();
			if(selection.width && selection.height) return false;
			var selection = _self.selections[_self.index];
			_self.imageareaselect.setOptions({
				x1: selection.x1, 
				y1: selection.y1, 
				x2: selection.x1 + selection.width, 
				y2: selection.y1 + selection.height,
			});
			_self.imageareaselect.update(false);
			return false;
		});
		
		$('#photo_rotate_left').bind('click', function(){
			$.getJSON(
				_self.properties.siteUrl + _self.properties.rotateUrl + _self.properties.idPhoto + '/90', {},
				function(data){
					var photo_source = $('#photo_source_view, #photo_source_rotate, #photo_source_recrop');
					photo_source.attr({src: photo_source[0].src+'?'+(new Date().getTime())});
					if(4*data.width > 5*data.height){
						$('#photo_source_rotate').removeAttr('height').attr({width: Math.min(data.width, 500)});
					}else{
						$('#photo_source_rotate').removeAttr('width').attr({height: Math.min(data.height, 400)});
					}
					if(344*data.width > 500*data.height){
						$('#photo_source_view').removeAttr('height').attr({width: Math.min(data.width, 500)});
					}else{
						$('#photo_source_view').removeAttr('width').attr({height: Math.min(data.height, 344)});
					}
					if(344*data.width > 500*data.height){
						$('#photo_source_recrop').removeAttr('height').attr({width: Math.min(data.width, 500)});
					}else{
						$('#photo_source_recrop').removeAttr('width').attr({height: Math.min(data.height, 344)});
					}
					_self.properties.imageWidth = data.width;
					_self.properties.imageHeight = data.height;
					for(var i in _self.selections){
						_self.selections[i].width = Math.min(data.width, _self.selections[i].awidth);
						_self.selections[i].height = Math.min(data.height, _self.selections[i].aheight);
						
						_self.selections[i].min_width = Math.min(data.width, _self.selections[i].awidth);
						_self.selections[i].min_height = Math.min(data.height, _self.selections[i].aheight);
					}
					if(_self.properties.parentImage){
						var parent_image = $('#'+_self.properties.parentImage);
						parent_image.attr('src', parent_image[0].src+'?'+(new Date().getTime()));
					}
				}
			);			
			return false;
		});
		
		$('#photo_rotate_right').bind('click', function(){
			$.getJSON(
				_self.properties.siteUrl + _self.properties.rotateUrl + _self.properties.idPhoto + '/-90', {},
				function(data){
					var photo_source = $('#photo_source_view, #photo_source_rotate, #photo_source_recrop');
					photo_source.attr({src: photo_source[0].src+'?'+(new Date().getTime())});
					if(4*data.width > 5*data.height){
						$('#photo_source_rotate').removeAttr('height').attr({width: Math.min(data.width, 500)});
					}else{
						$('#photo_source_rotate').removeAttr('width').attr({height: Math.min(data.height, 400)});
					}
					if(344*data.width > 500*data.height){
						$('#photo_source_view').removeAttr('height').attr({width: Math.min(data.width, 500)});
					}else{
						$('#photo_source_view').removeAttr('width').attr({height: Math.min(data.height, 344)});
					}
					if(344*data.width > 500*data.height){
						$('#photo_source_recrop').removeAttr('height').attr({width: Math.min(data.width, 500)});
					}else{
						$('#photo_source_recrop').removeAttr('width').attr({height: Math.min(data.height, 344)});
					}
					_self.properties.imageWidth = data.width;
					_self.properties.imageHeight = data.height;
					for(var i in _self.selections){
						_self.selections[i].width = Math.min(data.width, _self.selections[i].awidth);
						_self.selections[i].height = Math.min(data.height, _self.selections[i].aheight);
						
						_self.selections[i].min_width = Math.min(data.width, _self.selections[i].awidth);
						_self.selections[i].min_height = Math.min(data.height, _self.selections[i].aheight);
					}
					if(_self.properties.parentImage){
						var parent_image = $('#'+_self.properties.parentImage);
						parent_image.attr('src', parent_image[0].src+'?'+(new Date().getTime()));
					}
				}
			);			
			return false;
		});
		
		$('#recrop_btn').bind('click', function(){
			$.post(
				_self.properties.siteUrl + _self.properties.recropUrl + _self.properties.idPhoto,
				_self.selections[_self.index],
				function(data){
					if(_self.index + 1 < _self.selections.length) _self.index++;
					var selection = _self.selections[_self.index]; 
					_self.imageareaselect.setOptions({
						aspectRatio: selection.min_width + ':' + selection.min_height,
						minWidth: selection.min_width,
						minHeight: selection.min_height,
						x1: selection.x1, 
						y1: selection.y1, 
						x2: selection.x1 + selection.width, 
						y2: selection.y1 + selection.height
					});
					_self.imageareaselect.update(false);
					if(_self.properties.parentImage && _self.properties.parentThumb == selection.prefix){
						var parent_image = $('#'+_self.properties.parentImage);
						parent_image.attr('src', parent_image[0].src+'?'+(new Date().getTime()));
					}
					_self.change_size(_self.selections[_self.index].prefix);
				},
				'json'
			);
			return false;
		});
		
		$('.photo_size').bind('click', function(){
			var size_index = $(this).prevAll().length/2;
			if(size_index >= _self.selections.length || size_index == _self.index) return false;
			_self.index = size_index;
			var selection = _self.selections[_self.index];
			_self.imageareaselect.setOptions({
				aspectRatio: selection.min_width + ':' + selection.min_height,
				minWidth: selection.min_width,
				minHeight: selection.min_height,
				x1: selection.x1, 
				y1: selection.y1, 
				x2: selection.x1 + selection.width, 
				y2: selection.y1 + selection.height,
			});
			_self.imageareaselect.update(false);
			_self.change_size(selection.prefix);
			return false;
		});
		
		$('#' + _self.properties.sectionId + ' li').bind('click', function(){
			var id =$(this).attr('id');
			_self.propertiesCurrentSection = id;
			
			switch(id){
				default:
					_self.show_tab(id, false);
				break;
			}
			return false;
		});
	}
	
	this.show_tab = function(id, load){
		$('#' + _self.properties.sectionId + ' li').removeClass('active');
		$('#'+id).addClass('active');
			
		$('#'+_self.properties.listBlockId + ' .view-section').hide();
		_self.show_block(id, load);
	}
	
	this.show_block = function(id, load){
		var section_gid = $('#'+id).attr('sgid');
		if(load){
			$.ajax({
				url: _self.properties.siteUrl + _self.properties.viewAjaxUrl + _self.properties.idListing + '/' + section_gid, 
				type: 'GET',
				cache: false,
				success: function(data){
					$('#content_m_'+section_gid).html(data).show();
				}
			});
		}else{
			$('#content_m_'+section_gid).show();
		}
		if(section_gid == 'recrop'){
			_self.init_imageareaselect();
		}else{
			$('#photo_source_recrop').imgAreaSelect({remove: true});
			_self.imageareaselect = null;
		}
	}
	
	this.add_selection = function(prefix, x1, y1, width, height, awidth, aheight){
		_self.selections.push({prefix: prefix, x1: x1, y1: y1, width: width, height: height, min_width: width, min_height: height, awidth: awidth, aheight: aheight});
	}
	
	this.init_imageareaselect = function(){
		if(_self.selections.length < 0) return;
		_self.imageareaselect = $('#photo_source_recrop').imgAreaSelect({
			handles: true,
			aspectRatio: _self.selections[_self.index].min_width + ':' + _self.selections[_self.index].min_height,
			minWidth: _self.selections[_self.index].min_width,
			minHeight: _self.selections[_self.index].min_height,
			imageWidth: _self.properties.imageWidth,
			imageHeight: _self.properties.imageHeight,
			instance: true,
			parent: _self.properties.recropContainer,
			x1: _self.selections[_self.index].x1,
			y1: _self.selections[_self.index].y1,
			x2: _self.selections[_self.index].x1 + _self.selections[_self.index].width,
			y2: _self.selections[_self.index].y1 + _self.selections[_self.index].height,
			zIndex: 2,
			onSelectStart: function(image, selection){
			},
			onSelectEnd: function(image, selection){
				if(!selection.width || !selection.height) return;
				_self.selections[_self.index].x1 = Math.min(selection.x1, selection.x2);
				_self.selections[_self.index].y1 = Math.min(selection.y1, selection.y2);
				_self.selections[_self.index].width = selection.width;
				_self.selections[_self.index].height = selection.height;
			},
			onSelectChange: function(image, selection){
				if(!selection.width || !selection.height) return;
				_self.selections[_self.index].x1 = Math.min(selection.x1, selection.x2);
				_self.selections[_self.index].y1 = Math.min(selection.y1, selection.y2);
				_self.selections[_self.index].width = selection.width;
				_self.selections[_self.index].height = selection.height;
			},
			onInit: function(image, selection){
			},
		});
	}
	
	this.change_size = function(prefix){
		$('#photo_sizes li').removeClass('active');
		$('#photo_size_'+prefix).addClass('active');
	}
	
	_self.Init(optionArr);
}
