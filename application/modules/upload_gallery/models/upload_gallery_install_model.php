<?php
/**
* Upload Gallery Install Model
* 
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/
class Upload_gallery_install_model extends Model{
	
	/**
	 * Link to CodeIgniter object
	 * @var object
	 */
	private $CI;

	/**
	 * Menu configuration
	 * @var array
	 */
	private $menu = array(
		"admin_menu" => array(
			"action" => "none",
			"items" => array(
				'settings_items' => array(
					'action' => 'none',
					'items' => array(
						'uploads-items' => array(
							'action'=>'none',
							'items' => array(
								'gallery_menu_item' => array('action' => 'create', 'link' => 'admin/upload_gallery', 'status' => 1, 'sorter' => 4)
							)
						)
					)
				)
			),
		),
	);

	/**
	 * Moderation configuration
	 * @var array
	 */
	private $moderation_types = array(
		array(
			"name" => "upload_gallery",
			"mtype" => "2",
			"module" => "upload_gallery",
			"model" => "Upload_gallery_model",
			"check_badwords" => "1",
			"method_get_list" => "_moder_get_list",
			"method_set_status" => "_moder_set_status",
			"method_delete_object" => "_moder_delete",
			"allow_to_decline" => "0",
			"template_list_row" => "moder_block",
		),
	);

	/**
	 * Uploads configuration
	 * @var array
	 */
	private $uploads = array(
	
	);
	
	/**
	 * Gallery configuration
	 * @var array
	 */
	private $gallery_data = array(
		array(
			"gid" 				=> "listings-photo",
			"name" 				=> "Listings photos",
			"gid_upload_config" => "listings-photo",
			"max_items_count" 	=> 5,
			"use_moderation"	=> 1,
			//"date_add" 			=> date("Y-m-d H:i:s"),
		),
	);

	/**
	 * Constructor
	 *
	 * @return Install object
	 */
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
	}

	/**
	 * Install links to menu module
	 */
	public function install_menu(){
		
		$this->CI->load->helper("menu");

		foreach($this->menu as $gid => $menu_data){
			$this->menu[$gid]["id"] = linked_install_set_menu($gid, $menu_data["action"], $menu_data["name"]);
			linked_install_process_menu_items($this->menu, "create", $gid, 0, $this->menu[$gid]["items"]);
		}
	}
	
	/**
	 * Update languages
	 * @param array $langs_ids
	 */
	public function install_menu_lang_update($langs_ids=null){
		if(empty($langs_ids)) return false;
		$langs_file = $this->CI->Install_model->language_file_read("upload_gallery", "menu", $langs_ids);

		if(!$langs_file){log_message("info", "Empty menu langs data"); return false;}

		$this->CI->load->helper("menu");

		foreach($this->menu as $gid => $menu_data){
			linked_install_process_menu_items($this->menu, "update", $gid, 0, $this->menu[$gid]["items"], $gid, $langs_file);
		}
		return true;
	}

	/**
	 * Export languages
	 * @param array $langs_ids
	 */
	public function install_menu_lang_export($langs_ids){
		if(empty($langs_ids)) return false;
		$this->CI->load->helper("menu");

		$return = array();
		foreach($this->menu as $gid => $menu_data){
			$temp = linked_install_process_menu_items($this->menu, "export", $gid, 0, $this->menu[$gid]["items"], $gid, $langs_ids);
			$return = array_merge($return, $temp);
		}
		return array("menu" => $return);
	}
	
	/**
	 * Uninstall menu
	 */
	public function deinstall_menu(){
		$this->CI->load->helper("menu");
		foreach($this->menu as $gid => $menu_data){
			if($menu_data["action"] == "create"){
				linked_install_set_menu($gid, "delete");
			}else{
				linked_install_delete_menu_items($gid, $this->menu[$gid]["items"]);
			}
		}	
	}
	
	/**
	 * Install uploades
	 */
	public function install_uploads(){
		///// upload config
		$this->CI->load->model("uploads/models/Uploads_config_model");
		
		$watermark_ids = array();
		
		foreach((array)$this->uploads as $upload_data){
			$config_data = array(
				"gid" 			=> $upload_data["gid"],
				"name" 			=> $upload_data["name"],
				"max_height" 	=> $upload_data["max_height"],
				"max_width" 	=> $upload_data["max_width"],
				"max_size" 		=> $upload_data["max_size"],
				"name_format" 	=> $upload_data["name_format"],
				"file_formats" 	=> serialize((array)$upload_data["file_formats"]),
				"default_img" 	=> $upload_data["default_img"],
				"date_add" => date("Y-m-d H:i:s"),
			);
			$config_id = $this->CI->Uploads_config_model->save_config(null, $config_data);
		
			$wm_data = $this->CI->Uploads_config_model->get_watermark_by_gid("image-wm");
			$wm_id = isset($wm_data["id"])?$wm_data["id"]:0;
			
			foreach((array)$upload_data["thumbs"] as $thumb_gid => $thumb_data){
				if(isset($thumb_data["watermark"])){
					if(!isset($watermark_ids[$thumb_data["watermark"]])){
						$wm_data = $this->CI->Uploads_config_model->get_watermark_by_gid($thumb_data["watermark"]);
						$watermark_ids[$thumb_data["watermark"]] = isset($wm_data["id"])?$wm_data["id"]:0;
					}
					$watermark_id = $watermark_ids[$thumb_data["watermark"]];
				}else{
					$watermark_id = 0;
				}
				
				$thumb_data = array(
					"config_id" 	=> $config_id,
					"prefix"		=> $thumb_gid,
					"width" 		=> $thumb_data["width"],
					"height" 		=> $thumb_data["height"],
					"effect" 		=> "none",
					"watermark_id" 	=> $watermark_id,
					"crop_param" 	=> $thumb_data["crop_param"],
					"crop_color" 	=> $thumb_data["crop_color"],
					"date_add" 		=> date("Y-m-d H:i:s"),
				);
				$this->CI->Uploads_config_model->save_thumb(null, $thumb_data);
			}
		}
	}

	/**
	 * Uninstall uploads links
	 */
	public function deinstall_uploads(){
		$this->CI->load->model("uploads/models/Uploads_config_model");
		
		foreach((array)$this->uploads as $upload_data){
			$config_data = $this->CI->Uploads_config_model->get_config_by_gid($upload_data["gid"]);
			if(!empty($config_data["id"])){
				$this->CI->Uploads_config_model->delete_config($config_data["id"]);
			}
		}
	}
	
	/**
	 * Install moderation links
	 */
	public function install_moderation(){
		$this->CI->load->model('moderation/models/Moderation_type_model');
		foreach($this->moderation_types as $mtype) {
			$mtype['date_add'] = date("Y-m-d H:i:s");
			$this->CI->Moderation_type_model->save_type(null, $mtype);
		}
	}


	/**
	 * Untall moderation languages
	 * @param array $langs_ids
	 */
	public function install_moderation_lang_update($langs_ids=null){
		if(!is_array($langs_ids)) $langs_ids = (array)$langs_ids;
		$langs_file = $this->CI->Install_model->language_file_read('upload_gallery', 'moderation', $langs_ids);

		if(!$langs_file){log_message('info', 'Empty moderation langs data'); return false;}

		$this->CI->load->model('moderation/models/Moderation_type_model');
		$this->CI->Moderation_type_model->update_langs($this->moderation_types, $langs_file);
	}

	/**
	 * Export languages
	 * @param array $langs_ids
	 */
	public function install_moderation_lang_export($langs_ids=null){
		if(!is_array($langs_ids)) $langs_ids = (array)$langs_ids;
		$this->CI->load->model('moderation/models/Moderation_type_model');
		return array('moderation' => $this->CI->Moderation_type_model->export_langs($this->moderation_types, $langs_ids));
	}

	/**
	 * Uninstall moderation
	 */
	public function deinstall_moderation(){
		$this->CI->load->model('moderation/models/Moderation_type_model');
		foreach($this->moderation_types as $mtype) {
			$type = $this->CI->Moderation_type_model->get_type_by_name($mtype["name"]);
			$this->CI->Moderation_type_model->delete_type($type['id']);
		}	
	}
}
