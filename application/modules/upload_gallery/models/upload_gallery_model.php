<?php
/**
 * Upload Gallery Model
 * 
 * @package PG_RealEstate
 * @subpackage application
 * @category	modules
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/

define("GALLERY_TYPES_TABLE", DB_PREFIX."gallery_types");
define("GALLERY_TABLE", DB_PREFIX."gallery");

class Upload_gallery_model extends Model{
	/**
	 * Link to CodeIgniter object
	 * @var object
	 */
	private $CI;
	
	/**
	 * Link to database object
	 * @var object
	 */ 
	private $DB;
	
	/**
	 * Cache of types
	 * @var array
	 */
	private $type_cache=array();

	/**
	 * Data source fields of type
	 * @var array
	 */
	private $type_attrs = array(
		"id", 
		"gid", 
		"name", 
		"date_add", 
		"gid_upload_config", 
		"max_items_count", 
		"use_moderation",
		"module",
		"model",
		"callback",
	);
	
	/**
	 * Data source fields of file
	 * @var array
	 */
	private $file_attrs = array(
		"id", 
		"type_id",
		"object_id",
		"date_add", 
		"date_update", 
		"status", 
		"comment", 
		"file_name", 
		"settings",
		"sorter",
	);

	/**
	 * Moderation object
	 * @var string
	 */
	private $moderation_type = "upload_gallery";

	/**
	 * Constructor
	 * @return Upload_gallery_model
	 */
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
		$this->DB = &$this->CI->db;
	}

	/**
	 * Return type data by id
	 * @param integer $type_id
	 * @return array
	 */
	public function get_type_by_id($type_id){
		if(!$type_id) return false;

		if(!isset($this->type_cache["id"][$type_id]) || empty($this->type_cache["id"][$type_id])){
			$type_id = intval($type_id);
			$this->DB->select(implode(", ", $this->type_attrs))->from(GALLERY_TYPES_TABLE)->where("id", $type_id);
			$result = $this->DB->get()->result_array();
			if(!empty($result)){
				$rt = $result[0];
				$this->type_cache["id"][$rt["id"]] = $rt;
				$this->type_cache["gid"][$rt["gid"]] = $rt;
			}else
				$this->type_cache["id"][$type_id] = array();
		}
		return $this->type_cache["id"][$type_id];
	}

	/**
	 * Return type data by gid
	 * @param string $type_gid
	 * @return array
	 */
	public function get_type_by_gid($type_gid){
		if(!$type_gid) return false;

		if(!isset($this->type_cache["gid"][$type_gid]) || empty($this->type_cache["gid"][$type_gid])){
			$this->DB->select(implode(", ", $this->type_attrs))->from(GALLERY_TYPES_TABLE)->where("gid", $type_gid);
			$result = $this->DB->get()->result_array();
			if(!empty($result)){
				$rt = $result[0];
				$this->type_cache["id"][$rt["id"]] = $rt;
				$this->type_cache["gid"][$rt["gid"]] = $rt;
			}else{
				$this->type_cache["gid"][$type_gid] = array();
			}
		}
		return $this->type_cache["gid"][$type_gid];
	}

	/**
	 * Return types as array
	 * @return array
	 */
	public function get_types_list(){
		$this->DB->select(implode(", ", $this->type_attrs))->from(GALLERY_TYPES_TABLE)->order_by("id ASC");
		$result = $this->DB->get()->result_array();
		if(!empty($result)){
			foreach($result as $rt){
				$this->type_cache["gid"][$rt["gid"]] = $rt;
				$this->type_cache["id"][$rt["id"]] = $rt;
				$res[] = $rt;
			}
			return $res;
		}else{
			return array();
		}
	}

	/**
	 * Return number of types
	 * @return integer
	 */
	public function get_types_count(){
		$this->DB->select("COUNT(*) AS cnt")->from(GALLERY_TYPES_TABLE);
		$result = $this->DB->get()->result();
		if(!empty($result)){
			return intval($result[0]->cnt);
		}else{
			return 0;
		}
	}

	/**
	 * Validate type data
	 * @param integer $type_id
	 * @param array $data
	 * @return array
	 */
	public function validate_type($type_id, $data){
		$return = array("errors"=> array(), "data" => array());
	
		if(isset($data["gid"])){
			$return["data"]["gid"] = strtolower(preg_replace("/[\s\n\r_]{1,}/", "-", trim(preg_replace("/[^a-z_0-9\-]/i", "-", strip_tags($data["gid"])))));
			if(empty($return["data"]["gid"])) $return["errors"][] = l("error_gid_empty", "upload_gallery");
		}

		if(isset($data["name"])){
			$return["data"]["name"] = strip_tags($data["name"]);
			if(empty($return["data"]["name"])) $return["errors"][] = l("error_name_empty", "upload_gallery");
		}

		if(isset($data["gid_upload_config"])) $return["data"]["gid_upload_config"] = strval($data["gid_upload_config"]);
		if(isset($data["max_items_count"])) $return["data"]["max_items_count"] = intval($data["max_items_count"]);
		if(isset($data["use_moderation"])) $return["data"]["use_moderation"] = intval($data["use_moderation"]);
		
		if(isset($data["module"])) $return["data"]["module"] = $data["module"];
		if(isset($data["model"])) $return["data"]["model"] = $data["model"];
		if(isset($data["callback"])) $return["data"]["callback"] = $data["callback"];
		
		return $return;
	}

	/**
	 * Save type data to data source
	 * @param integer $type_id
	 * @param array $attrs
	 * @return integer
	 */
	public function set_type($type_id, $attrs){
		if(!is_array($attrs) || !count($attrs)) return false;
		$type_id = intval($type_id);
		if(!$type_id){
			$attrs["date_add"] = date("Y-m-d H:i:s");
			$this->DB->insert(GALLERY_TYPES_TABLE, $attrs);
			$type_id = $this->DB->insert_id();
		}else{
			$this->DB->where("id", $type_id);
			$this->DB->update(GALLERY_TYPES_TABLE, $attrs);
		}

		return $type_id;
	}

	/**
	 * Remove type data from data source
	 * @param integer $type_id
	 */
	public function delete_type($type_id){
		$this->DB->where("id", $type_id);
		$this->DB->delete(GALLERY_TYPES_TABLE);
	}

	/**
	 * Return file data by id
	 * @param integer $id
	 * @return array 
	 */
	public function get_file_by_id($id){
		$this->DB->select(implode(", ", $this->file_attrs))->from(GALLERY_TABLE)->where("id", $id);
		$result = $this->DB->get()->result_array();
		if(!empty($result)){
			$return = $this->format_file($result[0]);
		}else{
			$return = array();
		}
		return $return;
	}

	#--------------------------------------------------## MOD get data by id_listing #--------------------------------------------------##
	/**
	 * Return file data by id
	 * @param integer $id
	 * @return array 
	 */
	public function get_file_by_object_id($id){
		$this->DB->select(implode(", ", $this->file_attrs))
				->from(GALLERY_TABLE)
				->where("object_id", $id);
		$result  =$this->DB->get()->result_array();
		
		if(!empty($result)) {
			$return = $this->format_file($result[0]);
		} else {
			$return = array();
		}
		
		return $return;
	}
	#--------------------------------------------------## MOD get data by id_listing #--------------------------------------------------##

	/**
	 * Return file cover
	 * @param integer $count
	 */
	public function get_cover($count=1){
	
	}

	/**
	 * Return file cover list
	 * @param integer $count
	 */
	public function get_covers($count=1){
	
	}

	/**
	 * Return files as array
	 * @param array $params
	 * @param string $limit
	 * @param boolean $formatted
	 * @return array
	 */
	public function get_files_by_param($params, $limit=false, $formated=true){
		$this->DB->select(implode(", ", $this->file_attrs));
		$this->DB->from(GALLERY_TABLE);

		if(isset($params["where"]) && is_array($params["where"]) && count($params["where"])){
			foreach($params["where"] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])){
			foreach($params["where_in"] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])){
			foreach($params["where_sql"] as $value){
				$this->DB->where($value);
			}
		}
		$this->DB->order_by("sorter ASC");

		if(!empty($limit)) $this->DB->limit($limit, 0);
		
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			foreach($results as $r){
				$data[] = ($formated)?$this->format_file($r):$r;
			}
			return $data;
		}
		return array();
	}

	/**
	 * Return number of files by params
	 * @param array $params
	 * @return integer
	 */
	public function get_files_by_param_count($params){
		$this->DB->select("COUNT(*) AS cnt");
		$this->DB->from(GALLERY_TABLE);

		if(isset($params["where"]) && is_array($params["where"]) && count($params["where"])){
			foreach($params["where"] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])){
			foreach($params["where_in"] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])){
			foreach($params["where_sql"] as $value){
				$this->DB->where($value);
			}
		}
		$this->DB->order_by("sorter ASC");

		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			return intval($results[0]["cnt"]);
		}
		return 0;
	}

	/**
	 * Format file data
	 */
	public function format_file($data){
		$data["type_data"] = $type_data = $this->get_type_by_id($data["type_id"]);

		if(!empty($data["id"])){
			$data["postfix"] = $type_data["gid"]."/".$data["object_id"]."/".$data["id"];
		}

		$this->CI->load->model("Uploads_model");
		if(!empty($data["file_name"])){
			$data["media"] = $this->CI->Uploads_model->format_upload($type_data["gid_upload_config"], $data["postfix"], $data["file_name"]);
		}else{
			$data["media"] = $this->CI->Uploads_model->format_default_upload($type_data["gid_upload_config"]);
		}
		
		$data["settings"] = $data["settings"] ? (array)unserialize($data["settings"]) : array();
		
		return $data;
	}

	/**
	 * Format files data
	 */
	public function format_files(){
	
	}

	/**
	 * Validate file data
	 * @param integer $id
	 * @param integer $type_id
	 * @param array $data
	 * @return array
	 */
	public function validate_file_data($id, $type_id, $data){
		$return = array("errors"=> array(), "data" => array());
	
		if(isset($data["comment"])){
			$return["data"]["comment"] = strip_tags($data["comment"]);

			$type_data = $this->get_type_by_id($type_id);
			if($type_data["use_moderation"] && !empty($return["data"]["comment"])){
				$this->CI->load->model("moderation/models/Moderation_badwords_model");
				$bw_count = $this->CI->Moderation_badwords_model->check_badwords($this->moderation_type, $return["data"]["comment"]);
				if($bw_count) $return["errors"][] = l("error_badwords_comment", "upload_gallery");
			}
		}
		
		if(isset($data["settings"])){
			$return["data"]["settings"] = serialize($data["settings"]);
		}

		return $return;
	}

	/**
	 * Save file data
	 * @param integer $id
	 * @param array $data
	 * @param string $upload_name
	 * @param boolean $moderation_use
	 * @return array
	 */
	public function save_file($id, $data, $upload_name="", $moderation_use=true){
		$this->CI->load->model("Moderation_model");
		$return = array("id"=>$id, "errors"=>array());
		$id = intval($id);
		$flag_new_photo = ($id)?false:true;

		///// save text data
		if(!$id){
			$sorter_param["where"]["type_id"] = $data["type_id"];
			$sorter_param["where"]["object_id"] = $data["object_id"];
			$data["sorter"] = $this->get_files_by_param_count($sorter_param)+1;

			$data["status"] = 1;
			if($data["type_id"] ){
				$type_data = $this->get_type_by_id($data["type_id"]);
				if($type_data['max_items_count']){
					if($data["sorter"] > $type_data['max_items_count']){
						$return['errors'][] = l('error_max_items_count', 'upload_gallery');
						return $return;
					}
				}
				
				if($moderation_use && $type_data["use_moderation"]){
					$data["status"] = intval($this->CI->Moderation_model->get_moderation_type_status($this->moderation_type));
				}
			}

			$data["date_update"] = $data["date_add"] = date("Y-m-d H:i:s");
			$this->DB->insert(GALLERY_TABLE, $data);
			$return["id"] = $id = $this->DB->insert_id();
		}else{
			$data["date_update"] = date("Y-m-d H:i:s");
			$this->DB->where("id", $id);
			$this->DB->update(GALLERY_TABLE, $data);
		}

		////// add moderation alert if type use moderation
		if($moderation_use){
			$data = $this->get_file_by_id($id);
			$type_data = $this->get_type_by_id($data["type_id"]);
			if($type_data["use_moderation"]){
				$this->CI->Moderation_model->add_moderation_item($this->moderation_type, $id);
			}
		}

		////// upload file
		if(!empty($upload_name) && isset($_FILES[$upload_name]) && 
			is_array($_FILES[$upload_name]) && is_uploaded_file($_FILES[$upload_name]["tmp_name"])){
				
			$data = $this->get_file_by_id($id);
			$type_data = $this->get_type_by_id($data["type_id"]);
			
			$this->CI->load->model("Uploads_model");
			$img_return = $this->CI->Uploads_model->upload($type_data["gid_upload_config"], $data["postfix"], $upload_name);
			if(!empty($img_return["errors"])){
				if($flag_new_photo)	$this->delete_file($id);
				$return["errors"] = $img_return["errors"];
			}else{
				$upload = $this->CI->Uploads_model->format_upload($type_data["gid_upload_config"], $data["postfix"], $img_return["file"]);
				$image_size = getimagesize($upload["file_path"]);
				$image_data["file_name"] = $img_return["file"];
				if(isset($image_data["settings"])){
					$image_data["settings"] = unserialize($image_data["settings"]);
				}else{
					$image_data["settings"] = array();
				}				
				$image_data["settings"]["width"] = $image_size[0];
				$image_data["settings"]["height"] = $image_size[1];
				$image_data["settings"] = serialize($image_data["settings"]);
				$this->save_file($id, $image_data, "", $moderation_use);
			}
		}else{
			if($flag_new_photo)	$this->delete_file($id);
		}
		return $return;
	}
	
	/**
	 * Save local file data
	 * @param integer $id
	 * @param array $data
	 * @param string $file_path
	 * @param boolean $moderation_use
	 * @return array
	 */
	public function save_local_file($id, $data, $file_path="", $moderation_use=true){
		$this->CI->load->model("Moderation_model");
		$return = array("id"=>$id, "errors"=>array());
		$id = intval($id);
		$flag_new_photo = ($id)?false:true;

		///// save text data
		if(!$id){
			$sorter_param["where"]["type_id"] = $data["type_id"];
			$sorter_param["where"]["object_id"] = $data["object_id"];
			$data["sorter"] = $this->get_files_by_param_count($sorter_param)+1;

			$data["status"] = 1;
			if($data["type_id"] && $moderation_use){
				$type_data = $this->get_type_by_id($data["type_id"]);
				if($type_data["use_moderation"]){
					$data["status"] = intval($this->CI->Moderation_model->get_moderation_type_status($this->moderation_type));
				}
			}

			$data["date_update"] = $data["date_add"] = date("Y-m-d H:i:s");
			$this->DB->insert(GALLERY_TABLE, $data);
			$return["id"] = $id = $this->DB->insert_id();
		}else{
			$data["date_update"] = date("Y-m-d H:i:s");
			$this->DB->where("id", $id);
			$this->DB->update(GALLERY_TABLE, $data);
		}

		////// add moderation alert if type use moderation
		if($moderation_use){
			$data = $this->get_file_by_id($id);
			$type_data = $this->get_type_by_id($data["type_id"]);
			if($type_data["use_moderation"]){
				$this->CI->Moderation_model->add_moderation_item($this->moderation_type, $id);
			}
		}

		////// upload file
		if(!empty($file_path)){
				
			$data = $this->get_file_by_id($id);
			$type_data = $this->get_type_by_id($data["type_id"]);

			$this->CI->load->model("Uploads_model");
			$img_return = $this->CI->Uploads_model->upload_exist($type_data["gid_upload_config"], $data["postfix"], $file_path);

			if(!empty($img_return["errors"])){
				if($flag_new_photo){
					$this->delete_file($id);
				}
				$return["errors"] = $img_return["errors"];
			}else{
				$image_size = getimagesize($file_path);
				$image_data["file_name"] = $img_return["file"];
				if(isset($image_data["settings"])){
					$image_data["settings"] = unserialize($image_data["settings"]);
				}else{
					$image_data["settings"] = array();
				}				
				$image_data["settings"]["width"] = $image_size[0];
				$image_data["settings"]["height"] = $image_size[1];
				$image_data["settings"] = serialize($image_data["settings"]);
				$this->save_file($id, $image_data, "", $moderation_use);
			}
		}
		return $return;
	}
	
	/**
	 * Save file data
	 * @param integer $id
	 * @param array $data
	 * @param string $upload_name
	 * @param boolean $moderation_use
	 * @return array
	 */
	public function save_file_data($id, $data, $moderation_use=true){
		$this->CI->load->model("Moderation_model");
		$return = array("id"=>$id, "errors"=>array());
		$id = intval($id);
	
		///// save text data
		if(!$id){
			$sorter_param["where"]["type_id"] = $data["type_id"];
			$sorter_param["where"]["object_id"] = $data["object_id"];
			$data["sorter"] = $this->get_files_by_param_count($sorter_param)+1;

			$data["status"] = 1;
			if($data["type_id"] && $moderation_use){
				$type_data = $this->get_type_by_id($data["type_id"]);
				if($type_data["use_moderation"]){
					$data["status"] = intval($this->CI->Moderation_model->get_moderation_type_status($this->moderation_type));
				}
			}

			$data["date_update"] = $data["date_add"] = date("Y-m-d H:i:s");
			$this->DB->insert(GALLERY_TABLE, $data);
			$return["id"] = $id = $this->DB->insert_id();
		}else{
			$data["date_update"] = date("Y-m-d H:i:s");
			$this->DB->where("id", $id);
			$this->DB->update(GALLERY_TABLE, $data);
		}

		////// add moderation alert if type use moderation
		if($moderation_use){
			$data = $this->get_file_by_id($id);
			$type_data = $this->get_type_by_id($data["type_id"]);
			if($type_data["use_moderation"]){
				$this->CI->Moderation_model->add_moderation_item($this->moderation_type, $id);
			}
		}

		return $return;
	}

	/**
	 * Remove file from source
	 * @param integer $id
	 */
	public function delete_file($id){
		$data = $this->get_file_by_id($id);
		$type_data = $this->get_type_by_id($data["type_id"]);
		
		if(!empty($data["file_name"])){
			$this->CI->load->model("Uploads_model");
			$this->CI->Uploads_model->delete_upload($type_data["gid_upload_config"], $data["postfix"], $data["file_name"]);
		}
		$this->CI->Uploads_model->delete_path($type_data["gid_upload_config"], $data["postfix"]);
		
		$this->DB->where("id", $id);
		$this->DB->delete(GALLERY_TABLE);

		//// update sorter
		$this->refresh_sorter($data["type_id"], $data["object_id"]);
		
		//// delete moderation items
		$this->CI->load->model("Moderation_model");
		$this->CI->Moderation_model->delete_moderation_item_by_obj($this->moderation_type, $id);
	}
	
	/**
	 * Save sort order
	 * @param integer $id
	 * @param integer $sorter
	 */
	public function save_sorter($id, $sorter){
		$data["sorter"] = intval($sorter);
		$data["date_update"] = date("Y-m-d H:i:s");
		$this->DB->where("id", $id);
		$this->DB->update(GALLERY_TABLE, $data);
	}

	/**
	 * Remove file by param
	 * @param array $params
	 */
	public function delete_files_by_param($params){
		$files = $this->CI->Upload_gallery_model->get_files_by_param($params);
		foreach($files as $file){
			$this->CI->Upload_gallery_model->delete_file($file["id"]);
		}
		unset($files);
	}
	
	/**
	 * Resort files
	 * @param integer $type_id
	 * @param integer $object_id
	 */
	public function refresh_sorter($type_id, $object_id){
		$results = $this->DB->select("id, sorter")->from(GALLERY_TABLE)->where("type_id", $type_id)->where("object_id", $object_id)->order_by('sorter ASC')->get()->result_array();
		if(!empty($results)){
			$i = 1;
			foreach($results as $r){
				$data["sorter"] = $i;
				$this->DB->where("id", $r["id"]);
				$this->DB->update(GALLERY_TABLE, $data);
				$i++;
			}
		}
	}

	/**
	 * Create files rotation
	 */
	public function create_rotation(){
	
	}

	///// moderation functions
	/**
	 * Moderate objects
	 * @param array @object_ids
	 */
	public function _moder_get_list($object_ids){
		$params["where_in"]["id"] = $object_ids;
		$photos = $this->get_files_by_param($params);

		if(!empty($photos)){
			foreach($photos as $photo){
				$min_thumb = null;
				if(!isset($photo['media']['thumbs']['small'])){
					foreach($photo['media']['thumbs'] as $thumb){
						if(!$min_thumb || ($thumb['width']<$min_thumb['width'] && $thumb['height']<$min_thumb['height'])) $min_thumb = $thumb; 
					}
					if($min_thumb) $photo['media']['thumbs']['small'] = $thumb;
				}
				$return[$photo["id"]] = $photo;
			}
			return $return;
		}else{
			return array();
		}
	}

	/**
	 * Approve object
	 * @param integer $object_id
	 * @param integer $status
	 */
	public function _moder_set_status($object_id, $status){
		$data["status"] = intval($status);
		$data["date_update"] = date("Y-m-d H:i:s");
		$this->DB->where("id", $object_id);
		$this->DB->update(GALLERY_TABLE, $data);
		
		$file = $this->get_file_by_id($object_id);
		if($file["type_data"]["module"] && $file["type_data"]["model"] && $file["type_data"]["callback"]){
			try{
				$this->CI->load->model($file["type_data"]["module"]."/models/".$file["type_data"]["model"]);
				$this->CI->{$file["type_data"]["model"]}->{$file["type_data"]["callback"]}("moder_set_status", $object_id, $status);
			}catch(Exception $e){
			}
		}
	}

	/**
	 * Decline object
	 * @param integer $object_id
	 */
	public function _moder_delete($object_id){
		$file = $this->get_file_by_id($object_id);
		
		$this->delete_file($object_id);
		
		if($file["type_data"]["module"] && $file["type_data"]["model"] && $file["type_data"]["callback"]){
			try{
				$this->CI->load->model($file["type_data"]["module"]."/models/".$file["type_data"]["model"]);
				$this->CI->{$file["type_data"]["model"]}->{$file["type_data"]["callback"]}("moder_delete", $file['object_id'], $file['status']);
			}catch(Exception $e){
			}
		}
	}
	
	/**
	 * Rempove path
	 * @param array $type_data type data
	 * @param integer $object_id object idebtifier
	 */
	public function delete_path($type_data, $object_id){
		$this->CI->Uploads_model->delete_path($type_data["gid_upload_config"], $type_data["gid"]."/".$object_id);
	}
	
	/**
	 * Recrop file
	 * @param array $file_id file identifier
	 * @param integer $thumb_prefix thumb prefix
	 */
	public function recrop_file($file_id, $thumb_prefix){
		$photo = $this->get_file_by_id($file_id);
		
		$recrop_data["x1"] = $this->input->post("x1", true);
		$recrop_data["y1"] = $this->input->post("y1", true);
		$recrop_data["width"] = $this->input->post("width", true);
		$recrop_data["height"] = $this->input->post("height", true);
		
		$this->CI->load->model("Uploads_model");
		$this->CI->Uploads_model->recrop_upload($photo["type_data"]["gid_upload_config"], $photo["postfix"], $photo["file_name"], $recrop_data, $thumb_prefix);
		
		if($photo["type_data"]["module"] && $photo["type_data"]["model"] && $photo["type_data"]["callback"]){
			try{
				$this->CI->load->model($photo["type_data"]["module"]."/models/".$photo["type_data"]["model"]);
				$this->CI->{$photo["type_data"]["model"]}->{$photo["type_data"]["callback"]}("recrop", $file_id, array('thumb_prefix'=>$thumb_prefix, 'settings'=>$photo['settings']));
			}catch(Exception $e){
			}
		}
	}
	
	/**
	 * Rotate file
	 * @param array $file_id file identifier
	 * @param integer $angle rotate angle
	 */
	public function rotate_file($upload_id, $angle){
		$photo = $this->get_file_by_id($upload_id);
		
		$this->CI->load->model("Uploads_model");
		$this->CI->Uploads_model->rotate_upload($photo["type_data"]["gid_upload_config"], $photo["postfix"], $photo["file_name"], $angle);
		
		$data["settings"]["width"] = $photo["settings"]["height"];
		$data["settings"]["height"] = $photo["settings"]["width"];
		
		$validation_data = $this->validate_file_data($upload_id, $photo["type_id"], $data);
		$this->save_file($upload_id, $validation_data["data"], "", false);
		
		if($photo["type_data"]["module"] && $photo["type_data"]["model"] && $photo["type_data"]["callback"]){
			try{
				$this->CI->load->model($photo["type_data"]["module"]."/models/".$photo["type_data"]["model"]);
				$this->CI->{$photo["type_data"]["model"]}->{$photo["type_data"]["callback"]}("rotate", $upload_id, array('settings'=>$photo['settings']));
			}catch(Exception $e){
			}
		}
		
		return $data;
	}
}
