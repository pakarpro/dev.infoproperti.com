<?php

$install_lang["admin_header_ds_item_add"] = "Добавить вариант";
$install_lang["admin_header_ds_item_change"] = "Редактировать вариант";
$install_lang["admin_header_properties"] = "Виды недвижимости";
$install_lang["admin_header_property_item_edit"] = "Редактировать вариант";
$install_lang["api_error_category_not_found"] = "Категория не найдена";
$install_lang["api_error_empty_category_id"] = "Отсутствует id категории";
$install_lang["field_gid"] = "Системное имя";
$install_lang["from"] = "из";
$install_lang["header_category_select"] = "Выбор категории";
$install_lang["header_job_categories"] = "Категории";
$install_lang["job_categories"] = "Категории";
$install_lang["link_add_ds_item"] = "Добавить вариант";
$install_lang["link_reset_all"] = "Сбросить";
$install_lang["link_resort_items"] = "Изменить последовательность";
$install_lang["link_select_another_category"] = "Назад";
$install_lang["link_select_categories"] = "Выбрать категории";
$install_lang["link_select_category"] = "Выбрать категорию";
$install_lang["note_delete_ds_item"] = "Вы уверены, что хотите удалить этот вариант?";
$install_lang["properties"] = "Справочные данные";
$install_lang["select_empty_option"] = "Все категории";
$install_lang["selected"] = "выбрано";
$install_lang["success_added_ds_item"] = "Вариант успешно добавлен";
$install_lang["success_added_property_item"] = "Вариант недвижимости успешно добавлен";
$install_lang["success_updated_ds_item"] = "Вариант успешно обновлен";
$install_lang["success_updated_property_item"] = "Вариант успешно изменен";
$install_lang["text_availbale_select_categories"] = "Вы можете выбрать из следующих категорий:";

