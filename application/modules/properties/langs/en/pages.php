<?php

$install_lang["admin_header_ds_item_add"] = "Add option";
$install_lang["admin_header_ds_item_change"] = "Change option";
$install_lang["admin_header_properties"] = "Property types";
$install_lang["admin_header_property_item_edit"] = "Edit property option";
$install_lang["api_error_category_not_found"] = "Category is not found";
$install_lang["api_error_empty_category_id"] = "Empty category id";
$install_lang["field_gid"] = "Key";
$install_lang["from"] = "from";
$install_lang["header_category_select"] = "Category selector";
$install_lang["header_job_categories"] = "Categories";
$install_lang["job_categories"] = "Categories";
$install_lang["link_add_ds_item"] = "Add option";
$install_lang["link_reset_all"] = "Reset all";
$install_lang["link_resort_items"] = "Change order";
$install_lang["link_select_another_category"] = "Back";
$install_lang["link_select_categories"] = "Choose categories";
$install_lang["link_select_category"] = "Choose a category";
$install_lang["note_delete_ds_item"] = "Are you sure you want to delete the option?";
$install_lang["properties"] = "References";
$install_lang["select_empty_option"] = "All categories";
$install_lang["selected"] = "selected";
$install_lang["success_added_ds_item"] = "Option is successfully added";
$install_lang["success_added_property_item"] = "Property option is successfully added";
$install_lang["success_updated_ds_item"] = "Option is successfully updated";
$install_lang["success_updated_property_item"] = "Property option is successfully changed";
$install_lang["text_availbale_select_categories"] = "You can still choose:";

