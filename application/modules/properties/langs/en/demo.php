<?php

$install_lang["property_types"]["header"] = "For sale/buy";
$install_lang["property_types"]["option"]["1"] = "Residential";
$install_lang["property_types"]["option"]["2"] = "Commercial";
$install_lang["property_types"]["option"]["3"] = "Lots/lands";

$install_lang["property_type_1"]["header"] = "Residential";
$install_lang["property_type_1"]["option"]["1"] = "Apartment";
$install_lang["property_type_1"]["option"]["2"] = "Room";
$install_lang["property_type_1"]["option"]["3"] = "Duplex";
$install_lang["property_type_1"]["option"]["4"] = "Single Family Home";
$install_lang["property_type_1"]["option"]["5"] = "Multi Family Home";
$install_lang["property_type_1"]["option"]["6"] = "Condo";
$install_lang["property_type_1"]["option"]["7"] = "Townhouse";
$install_lang["property_type_1"]["option"]["8"] = "Mobile home";
$install_lang["property_type_1"]["option"]["9"] = "Other";

$install_lang["property_type_2"]["header"] = "Commercial";
$install_lang["property_type_2"]["option"]["1"] = "Café";
$install_lang["property_type_2"]["option"]["2"] = "Office";
$install_lang["property_type_2"]["option"]["3"] = "Shop";
$install_lang["property_type_2"]["option"]["4"] = "Restaurant";
$install_lang["property_type_2"]["option"]["5"] = "Hotel";
$install_lang["property_type_2"]["option"]["6"] = "Sports facility";
$install_lang["property_type_2"]["option"]["7"] = "Warehouse";
$install_lang["property_type_2"]["option"]["8"] = "Manufactured";
$install_lang["property_type_2"]["option"]["9"] = "Other";

$install_lang["property_type_3"]["header"] = "Lots/lands";
$install_lang["property_type_3"]["option"]["1"] = "Farm";
$install_lang["property_type_3"]["option"]["2"] = "Pasture";
$install_lang["property_type_3"]["option"]["3"] = "Timberland";
$install_lang["property_type_3"]["option"]["4"] = "Undeveloped land";
$install_lang["property_type_3"]["option"]["5"] = "Other";
