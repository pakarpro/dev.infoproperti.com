<?php

if(!function_exists('categories_select')){
	function categories_select($params){
		$CI = & get_instance();
		$CI->load->model("Properties_model");
		
		extract($params);
		
		if(!$categories_gid) $categories_gid = current($CI->Properties_model->categories);
		if(!$lang_id) $lang_id = $CI->pg_language->current_lang_id;
		if(!$var_name) $var_name = 'id_category';
		if(!$js_var_name) $js_var_name = '';
		$selected = strval($selected);

		$data = $CI->Properties_model->get_categories($categories_gid, $lang_id);

		if(empty($data)){
			return false;
		}

		$select = array(
			'options' => $data,
			'name' => $var_name,
			'id' => $js_var_name,
			'selected' => $selected,
		);
		$CI->template_lite->assign('categories_helper_data', $select);
		return $CI->template_lite->fetch('helper_categories_select', 'user', 'properties');
	}
}

if ( ! function_exists('properties_select'))
{
	///// categories = array of ids
	function properties_select($params){
		$CI = & get_instance();
		$CI->load->model("Properties_model");
		
		extract($params);
		
		if(!$categories_gid) $categories_gid = current($CI->Properties_model->categories);
		if(!$lang_id) $lang_id = $CI->pg_language->current_lang_id;
		if(!$var_name) $var_name = 'id_category';
		if(!$js_var_name) $js_var_name = '';
		$selected = strval($selected);
		$cat_select = (int)$cat_select;
		
		$categories = $CI->Properties_model->get_categories($categories_gid, $lang_id);
		
		if(!isset($id_category) || empty($id_category)){
		    $properties = $CI->Properties_model->get_all_properties($categories_gid, $lang_id);
		    foreach($categories as $cid => $cdata){
				if(!empty($options)) unset($options);
				foreach($properties[$cid] as $key => $name){
					$options[$key] = array(
						'id' => $key,
						'value' => $cid.'_'.$key,
						'name' => $name
					);
				}			
			
				$data[$cid] = array(
					'id' => $cid,
					'value' => $cid,
					'name' => $cdata,
					'options' => $options,
				);
		    }
		}
		else{
		    $properties = $CI->Properties_model->get_properties($id_category, $lang_id);
		    foreach($properties as $key => $name){
				$options[$key] = array(
				'id' => $key,
				'value' => $id_category.'_'.$key,
				'name' => $name
				);
			}
		    
		    $data[$id_category] = array(
			'id' => $id_category,
			'value' => $id_category,
			'options' => $options,
		    );
		}
		$helper_data = array(
			'data' => $data,
			'name' => $var_name,
			'id' => $js_var_name,
			'cat_select_available' => $cat_select,
			'selected' => $selected,
			'id_category' => $id_category,
			'category_str' => $category_str
		);
		$CI->template_lite->assign('properties_helper_data', $helper_data);
		return $CI->template_lite->fetch('helper_properties_select', 'user', 'properties');
	}
}

if ( ! function_exists('category_value'))
{
	///// categories = array of ids
	function category_value($params){
		$CI = & get_instance();
		$CI->load->model("Properties_model");
		
		extract($params);
		
		if(!$categories_gid) $categories_gid = current($CI->Properties_model->categories);
		if(!$lang_id) $lang_id = $CI->pg_language->current_lang_id;
		if(!$id_category) $id_category = 1;

		$categories = $CI->Properties_model->get_categories($categories_gid, $lang_id);
		return $categories[$id_category];
	}
}

if ( ! function_exists('property_value'))
{
	///// categories = array of ids
	function property_value($params){
		$CI = & get_instance();
		$CI->load->model("Properties_model");
		
		extract($params);
		
		if(!$categories_gid) $categories_gid = current($CI->Properties_model->categories);
		if(!$lang_id) $lang_id = $CI->pg_language->current_lang_id;
		if(!$id_category) $id_category = 1;
		if(!$property_type) $property_type = 1;
		if(!isset($full_output)) $full_output = false;

		$categories = $CI->Properties_model->get_categories($categories_gid, $lang_id);
		$properties = $CI->Properties_model->get_properties($id_category, $lang_id);
		
		$return = $properties[$property_type];
		if($full_output) $return = $categories[$id_category].', '.$return;
		return $return;
	}
}
