<?php
/**
* Properties main model
* 
* @package PG_Job
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/

class Properties_model extends Model {
	private $CI;
	private $DB;
	private $attrs = array();
	
	/**
	 * Categories GUIDs
	 * @var array
	 */
	public $categories = array('property_types');
	
	/**
	 * Properties GUIDs
	 */
	public $properties = array(
		'property_type_1', 
		'property_type_2', 
		'property_type_3', 
		'property_type_4', 
		'property_type_5', 
		'property_type_6', 
	);
	
	/**
	 * Property type prefix
	 * @var string
	 */
	public $property_prefix = 'property_type_';
	
	/**
	 * Module GUID
	 * @var string
	 */
	public $module_gid = 'data_properties';

	/**
	 * Class constructor
	 * @return Properties_model
	 */
	public function __construct()
	{
		parent::Model();
		$this->CI = & get_instance();
		$this->DB = &$this->CI->db;
	}
	
	public function get_property($ds_gid, $lang_id = null){
		if (!$ds_gid){
			return null;
		}

		if(!$lang_id) $lang_id = $this->CI->session->userdata('lang_id');
		return $this->CI->pg_language->ds->get_reference($this->module_gid, $ds_gid, $lang_id);
	}
	
	public function get_categories($categories_gid, $lang_id = null){
		if(!in_array($categories_gid, $this->categories)) $categories_gid = current($this->categories);
		if(!$lang_id) $lang_id = $this->CI->session->userdata('lang_id');
		$categories = $this->CI->pg_language->ds->get_reference($this->module_gid, $categories_gid, $lang_id);
		return $categories['option'];
	}
	
	public function get_properties($num, $lang_id = null){
		if(!$lang_id) $lang_id = $this->CI->session->userdata('lang_id');
		$properties = $this->CI->pg_language->ds->get_reference($this->module_gid, $this->property_prefix.$num, $lang_id);
		return $properties['option'];
	}
	
	public function get_all_properties($categories_gid, $lang_id = null){
		if(!$lang_id) $lang_id = $this->CI->session->userdata('lang_id');
		$properties = array();
		$categories = $this->get_categories($categories_gid, $lang_id);
		foreach($categories as $cid => $name){
			$t = $this->CI->pg_language->ds->get_reference($this->module_gid, $this->property_prefix.$cid, $lang_id);
			$properties[$cid] = $t['option'];
		}
		return $properties;
	}
}
