<select name="{$categories_helper_data.name}" {if $categories_helper_data.id}id="{$categories_helper_data.id}"{/if}>
<option value="0">{l i='select_empty_option' gid='properties'}</option>
{foreach item=item key=key from=$categories_helper_data.options}
<option value="{$key}"{if $categories_helper_data.selected eq $key}selected{/if}>{$item}</option>
{/foreach}
</select>
