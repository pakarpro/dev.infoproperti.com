{include file="header.tpl"}
{foreach item=item key=gid from=$categories}
	{foreach item=item2 key=gid2 from=$properties}
	{if $gid2|replace:$property_prefix:''|array_key_exists:$item.option}
	<div class="settings-block with-{$gid2}" onclick="javascript: location.href='{$site_url}admin/properties/property/{$gid2}';">
		<a href="{$site_url}admin/properties/property/{$gid2}"><h6>{$item2.header}</h6></a>
	</div>
	{/if}
	{/foreach}
	<div class="clr"></div>
{/foreach}
{include file="footer.tpl"}
