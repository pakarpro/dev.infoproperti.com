<?php
/**
* Banners install model
*
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/
class Banners_install_model extends Model
{
	private $CI;
	private $menu = array(
		'admin_menu' => array(
			'action' => 'none',
			'items' => array(
				'other_items' => array(
					'action' =>'none',
					'items' => array(
						'banners_menu_item' => array('action' => 'create', 'link' => 'admin/banners', 'status' => 1, 'sorter' => 2)
					)
				)
			)
		),
		"private_account_menu" => array(
			"action" => "none",
			"items" => array(
				"private_my_banners_item" => array("action" => "create", "link" => "banners/my", "status" => 1, "sorter" => 9),
			),
		),
		"company_account_menu" => array(
			"action" => "none",
			"items" => array(
				"company_my_banners_item" => array("action" => "create", "link" => "banners/my", "status" => 1, "sorter" => 9),
			),
		),
		"agent_account_menu" => array(
			"action" => "none",
			"items" => array(
				"agent_my_banners_item" => array("action" => "create", "link" => "banners/my", "status" => 1, "sorter" => 9),
			),
		),
		'admin_banners_menu' => array(
			'action' => 'create',
			'name' => 'Admin mode - Banner ads',
			'items' => array(
				'banners_list_item' => array('action' => 'create', 'link' => 'admin/banners', 'status' => 1),
				'groups_list_item' => array('action' => 'create', 'link' => 'admin/banners/groups_list', 'status' => 1),
				'places_list_item' => array('action' => 'create', 'link' => 'admin/banners/places_list', 'status' => 1),
				'banners_settings_item' => array('action' => 'create', 'link' => 'admin/banners/settings', 'status' => 1),
			),
		)
	);
	
	/**
	 * Notifications configuration
	 * @var array
	 */
	private $notifications = array(
		"templates" => array(
			array("gid"=>"banner_need_moderate", "name"=>"New banner awaiting moderation", "vars"=>array(), "content_type"=>"text"),
			array("gid"=>"banner_status_updated", "name"=>"Banner status updated", "vars"=>array("user", "banner", "status"), "content_type"=>"text"),
		),
		"notifications" => array(
			array("gid"=>"banner_need_moderate", "template"=>"banner_need_moderate", "send_type"=>"simple"),
			array("gid"=>"banner_status_updated", "template"=>"banner_status_updated", "send_type"=>"simple"),
		),
	);

	private $lang_services = array(
		'service' => array('banner_service'),
		'template' => array('banner_template')
	);
	
	/**
	 * Constructor
	 *
	 * @return Install object
	 */
	function __construct()
	{
		parent::Model();
		$this->CI = & get_instance();
	}

	public function install_menu() {
		$this->CI->load->helper('menu');

		foreach($this->menu as $gid => $menu_data){
			$this->menu[$gid]['id'] = linked_install_set_menu($gid, $menu_data['action'], $menu_data['name']);
			linked_install_process_menu_items($this->menu, 'create', $gid, 0, $this->menu[$gid]['items']);
		}
	}

	public function install_menu_lang_update($langs_ids = null) {
		if(empty($langs_ids)) return false;
		$langs_file = $this->CI->Install_model->language_file_read('banners', 'menu', $langs_ids);

		if(!$langs_file) { log_message('info', 'Empty menu langs data'); return false; }

		$this->CI->load->helper('menu');

		foreach($this->menu as $gid => $menu_data){
			linked_install_process_menu_items($this->menu, 'update', $gid, 0, $this->menu[$gid]['items'], $gid, $langs_file);
		}
		return true;
	}

	public function install_menu_lang_export($langs_ids) {
		if(empty($langs_ids)) return false;
		$this->CI->load->helper('menu');

		$return = array();
		foreach($this->menu as $gid => $menu_data){
			$temp = linked_install_process_menu_items($this->menu, 'export', $gid, 0, $this->menu[$gid]['items'], $gid, $langs_ids);
			$return = array_merge($return, $temp);
		}
		return array( 'menu' => $return );
	}

	public function deinstall_menu() {
		$this->CI->load->helper('menu');
		foreach($this->menu as $gid => $menu_data){
			if($menu_data['action'] == 'create'){
				linked_install_set_menu($gid, 'delete');
			}else{
				linked_install_delete_menu_items($gid, $this->menu[$gid]['items']);
			}
		}
	}
	
	/**
	 * Install notifications
	 */
	public function install_notifications(){
		// add notification
		$this->CI->load->model("Notifications_model");
		$this->CI->load->model("notifications/models/Templates_model");

		$templates_ids = array();

		foreach((array)$this->notifications["templates"] as $template_data){
			if(is_array($template_data["vars"])) $template_data["vars"] = implode(", ", $template_data["vars"]);			
			$validate_data = $this->CI->Templates_model->validate_template(null, $template_data);
			if(!empty($validate_data["errors"])) continue;
			$templates_ids[$template_data['gid']] = $this->CI->Templates_model->save_template(null, $validate_data["data"]);			
		}
		
		foreach((array)$this->notifications["notifications"] as $notification_data){
			if(!isset($templates_ids[$notification_data["template"]])){
				 $template = $this->CI->Templates_model->get_template_by_gid($notification_data["template"]);
				 $templates_ids[$notification_data["template"]] = $template["id"];
			}
			$notification_data["id_template_default"] = $templates_ids[$notification_data["template"]];
			$validate_data = $this->CI->Notifications_model->validate_notification(null, $notification_data);
			if(!empty($validate_data["errors"])) continue;
			$this->CI->Notifications_model->save_notification(null, $validate_data["data"], $lang_data);
		}
	}
	
	/**
	 * Import notifiactions languages
	 * @param array $langs_ids
	 */
	public function install_notifications_lang_update($langs_ids=null){
		if(empty($langs_ids)) return false;
		$this->CI->load->model("Notifications_model");
		
		$langs_file = $this->CI->Install_model->language_file_read("banners", "notifications", $langs_ids);
		if(!$langs_file){log_message("info", "Empty notifications langs data");return false;}
		
		$this->CI->Notifications_model->update_langs($this->notifications, $langs_file, $langs_ids);
		return true;
	}
	
	/**
	 * Export notifications languages
	 * @param array $langs_ids
	 */
	public function install_notifications_lang_export($langs_ids=null){
		$this->CI->load->model("Notifications_model");
		$langs = $this->CI->Notifications_model->export_langs($this->notifications, $langs_ids);
		return array("notifications" => $langs);
	}
	
	/**
	 * Unistall notifacations
	 */
	public function deinstall_notifications(){
		$this->CI->load->model("Notifications_model");
		$this->CI->load->model("notifications/models/Templates_model");
		
		foreach((array)$this->notifications["notifications"] as $notification_data){
			$this->CI->Notifications_model->delete_notification_by_gid($notification_data["gid"]);
		}
		
		foreach((array)$this->notifications["templates"] as $template_data){
			$this->CI->Templates_model->delete_template_by_gid($template_data["gid"]);
		}
	}

	public function install_uploads () {
		$this->CI->load->model('uploads/models/Uploads_config_model');
		$config_data = array(
			'gid' => 'banner',
			'name' => 'Banner image file',
			'max_height' => 800,
			'max_width' => 800,
			'max_size' => 100000,
			'name_format' => 'generate',
			'file_formats' => 'a:3:{i:0;s:3:"jpg";i:1;s:3:"gif";i:2;s:3:"png";}',
			'default_img' => 'default_banner.gif',
			'date_add' => date('Y-m-d H:i:s'),
		);
		$this->CI->Uploads_config_model->save_config(null, $config_data);
	}

	public function install_services () {
		// add service type and service
		// create service template and service
		$this->CI->load->model("Services_model");
		$template_data = array(
			'gid' => "banner_template",
			'callback_module' => "banners",
			'callback_model' => "Banners_model",
			'callback_method' => "service_banner_activate",
			'callback_validate_method' => "service_banner_validate",
			'price_type' => 3,
			'data_admin' => "",
			'data_user'=> serialize(array("id_banner_payment" => "hidden")),
			'date_add' => date("Y-m-d H:i:s"),
			'moveable' => 0
		);
		$this->CI->Services_model->save_template(null, $template_data);

		$service_data = array(
			"gid" => "banner_service",
			"template_gid" => "banner_template",
			"pay_type" => 2,
			"status" => 1,
			"price" => 0,
			"data_admin" => "",
			"date_add" => date("Y-m-d H:i:s")
		);
		$this->CI->Services_model->save_service(null, $service_data);
	}

	public function install_services_lang_update($langs_ids = null) {
		if(!is_array($langs_ids)) $langs_ids = (array)$langs_ids;
		$this->CI->load->model('Services_model');
		$langs_file = $this->CI->Install_model->language_file_read('banners', 'services', $langs_ids);
		$this->CI->Services_model->update_langs($this->lang_services, $langs_file);
		return true;
	}

	public function install_services_lang_export($langs_ids = null) {
		if(!is_array($langs_ids)) $langs_ids = (array)$langs_ids;
		$this->CI->load->model('Services_model');
		return array( 'services' => $this->CI->Services_model->export_langs($this->lang_services, $langs_ids));
	}

	public function install_cronjob () {
		///// add cronjob ()
		$this->CI->load->model('Cronjob_model');
		$cron_data = array(
			"name" => "Update banner statistic",
			"module" => "banners",
			"model" => "Banners_model",
			"method" => "update_statistic",
			"cron_tab" => "*/10 * * * *",
			"status" => "1"
		);
		$this->CI->Cronjob_model->save_cron(null, $cron_data);
	}

	private $ausers_methods = array(
		array('module' => 'banners', 'method' => 'index', 'is_default' => 1),
		array('module' => 'banners', 'method' => 'groups_list', 'is_default' => 0),
		array('module' => 'banners', 'method' => 'places_list', 'is_default' => 0),
	);

	/**
	 * Ausers module methods
	 */
	public function install_ausers() {
		// install ausers permissions
		$this->CI->load->model('Ausers_model');

		foreach($this->ausers_methods as $method){
			$this->CI->Ausers_model->save_method(null, $method);
		}
	}

	public function install_ausers_lang_update($langs_ids = null) {
		$langs_file = $this->CI->Install_model->language_file_read('banners', 'ausers', $langs_ids);

		// install ausers permissions
		$this->CI->load->model('Ausers_model');
		$params['where']['module'] = 'banners';
		$methods = $this->CI->Ausers_model->get_methods_lang_export($params);

		foreach($methods as $method){
			if(!empty($langs_file[$method['method']])){
				$this->CI->Ausers_model->save_method($method['id'], array(), $langs_file[$method['method']]);
			}
		}
	}

	public function install_ausers_lang_export($langs_ids) {
		$this->CI->load->model('Ausers_model');
		$params['where']['module'] = 'banners';
		$methods =  $this->CI->Ausers_model->get_methods_lang_export($params, $langs_ids);
		foreach($methods as $method){
			$return[$method['method']] = $method['langs'];
		}
		return array( 'ausers' => $return );
	}

	public function deinstall_ausers() {
		// delete moderation methods in ausers
		$this->CI->load->model('Ausers_model');
		$params['where']['module'] = 'banners';
		$this->CI->Ausers_model->delete_methods($params);
	}

	function _arbitrary_installing(){
		if($this->pg_module->is_module_installed('ausers')){
			$this->CI->load->model("Ausers_model");
			$users = $this->CI->Ausers_model->get_users_list(null, 1, null, array('where'=>array('user_type'=>'admin')));
			if(!empty($users)){ 
				$this->CI->pg_module->set_module_config("banners", 'admin_moderation_emails', $users[0]["email"]);
			}
		}
		
		///// Add dynamic block
		$this->add_banners_to_site();
	}

	public function deinstall_uploads () {
		$this->CI->load->model('uploads/models/Uploads_config_model');
		$config_data = $this->CI->Uploads_config_model->get_config_by_gid('banner');
		if(!empty($config_data["id"])){
			$this->CI->Uploads_config_model->delete_config($config_data["id"]);
		}
	}

	public function deinstall_services () {
		$this->CI->load->model("Services_model");
		$this->CI->Services_model->delete_template_by_gid('banner_template');
		$this->CI->Services_model->delete_service_by_gid('banner_service');
	}

	public function deinstall_cronjob () {
		$this->CI->load->model('Cronjob_model');
		$cron_data = array();
		$cron_data["where"]["module"] = "banners";
		$this->CI->Cronjob_model->delete_cron_by_param($cron_data);
	}

	function _arbitrary_deinstalling(){
	}

	public function add_banners_to_site(){
		$this->CI->load->model('banners/models/Banner_group_model');
		$this->CI->load->model('Banners_model');

		$all_groups_ids = array_keys($this->CI->Banner_group_model->get_all_groups_key_id());
		$banners[] = array(
			'alt_text' => 'PG RealEstateSoftware',
			'approve' => 1,
			'banner_image' => '55286cb3e5.jpg',
			'banner_place_id' => 3,
			'banner_type' => 1,
			'decline_reason' => '',
			'expiration_date' => '0000-00-00 00:00:00',
			'html' => '',
			'link' => 'http://demo.realtysoft.pro/',
			'name' => 'Left banner №1',
			'new_window' => 1,
			'is_admin' => 1,
			'number_of_clicks' => 0,
			'number_of_views' => 0,
			'status' => 1,
			'user_id' => 0,
			'stat_clicks' => 0,
			'stat_views' => 0,
			'user_activate_info' => '',
			'banner_groups' => $all_groups_ids,
		);

		$banners[] = array(
			'alt_text' => 'PG RealEstateSoftware',
			'approve' => 1,
			'banner_image' => 'fcae62cd46.jpg',
			'banner_place_id' => 3,
			'banner_type' => 1,
			'decline_reason' => '',
			'expiration_date' => '0000-00-00 00:00:00',
			'html' => '',
			'link' => 'http://demo.realtysoft.pro/',
			'name' => 'Left banner №2',
			'new_window' => 1,
			'is_admin' => 1,
			'number_of_clicks' => 0,
			'number_of_views' => 0,
			'status' => 1,
			'user_id' => 0,
			'stat_clicks' => 0,
			'stat_views' => 0,
			'user_activate_info' => '',
			'banner_groups' => $all_groups_ids,
		);

		$banners[] = array(
			'alt_text' => 'PG RealEstateSoftware',
			'approve' => 1,
			'banner_image' => '93497f79ce.jpg',
			'banner_place_id' => 3,
			'banner_type' => 1,
			'decline_reason' => '',
			'expiration_date' => '0000-00-00 00:00:00',
			'html' => '',
			'link' => 'http://www.realtysoft.pro/pricing.php',
			'name' => 'Left banner №3',
			'new_window' => 1,
			'is_admin' => 1,
			'number_of_clicks' => 0,
			'number_of_views' => 0,
			'status' => 1,
			'user_id' => 0,
			'stat_clicks' => 0,
			'stat_views' => 0,
			'user_activate_info' => '',
			'banner_groups' => $all_groups_ids,
		);

		$banners[] = array(
			'alt_text' => 'PG RealEstateSoftware',
			'approve' => 1,
			'banner_image' => 'dfd669eba7.JPG',
			'banner_place_id' => 1,
			'banner_type' => 1,
			'decline_reason' => '',
			'expiration_date' => '0000-00-00 00:00:00',
			'html' => '',
			'link' => 'http://demo.realtysoft.pro/',
			'name' => 'Bottom banner №1',
			'new_window' => 1,
			'is_admin' => 1,
			'number_of_clicks' => 0,
			'number_of_views' => 0,
			'status' => 1,
			'user_id' => 0,
			'stat_clicks' => 0,
			'stat_views' => 0,
			'user_activate_info' => '',
			'banner_groups' => $all_groups_ids,
		);
		$banners[] = array(
			'alt_text' => 'PG RealEstateSoftware',
			'approve' => 1,
			'banner_image' => 'e9116ceea8.jpg',
			'banner_place_id' => 1,
			'banner_type' => 1,
			'decline_reason' => '',
			'expiration_date' => '0000-00-00 00:00:00',
			'html' => '',
			'link' => 'http://demo.realtysoft.pro/',
			'name' => 'Bottom banner №2',
			'new_window' => 1,
			'is_admin' => 1,
			'number_of_clicks' => 0,
			'number_of_views' => 0,
			'status' => 1,
			'user_id' => 0,
			'stat_clicks' => 0,
			'stat_views' => 0,
			'user_activate_info' => '',
			'banner_groups' => $all_groups_ids,
		);
		$banners[] = array(
			'alt_text' => 'PG RealEstateSoftware',
			'approve' => 1,
			'banner_image' => '58dc682f81.jpg',
			'banner_place_id' => 1,
			'banner_type' => 1,
			'decline_reason' => '',
			'expiration_date' => '0000-00-00 00:00:00',
			'html' => '',
			'link' => 'http://www.realtysoft.pro/pricing.php',
			'name' => 'Bottom banner №3',
			'new_window' => 1,
			'is_admin' => 1,
			'number_of_clicks' => 0,
			'number_of_views' => 0,
			'status' => 1,
			'user_id' => 0,
			'stat_clicks' => 0,
			'stat_views' => 0,
			'user_activate_info' => '',
			'banner_groups' => $all_groups_ids,
		);
		$banners[] = array(
			'alt_text' => 'PG RealEstateSoftware',
			'approve' => 1,
			'banner_image' => '537f411419.jpg',
			'banner_place_id' => 2,
			'banner_type' => 1,
			'decline_reason' => '',
			'expiration_date' => '0000-00-00 00:00:00',
			'html' => '',
			'link' => 'http://demo.realtysoft.pro/',
			'name' => 'Big left banner №1',
			'new_window' => 1,
			'is_admin' => 1,
			'number_of_clicks' => 0,
			'number_of_views' => 0,
			'status' => 1,
			'user_id' => 0,
			'stat_clicks' => 0,
			'stat_views' => 0,
			'user_activate_info' => '',
			'banner_groups' => $all_groups_ids,
		);
		$banners[] = array(
			'alt_text' => 'PG RealEstateSoftware',
			'approve' => 1,
			'banner_image' => 'b4ab5556ad.jpg',
			'banner_place_id' => 2,
			'banner_type' => 1,
			'decline_reason' => '',
			'expiration_date' => '0000-00-00 00:00:00',
			'html' => '',
			'link' => 'http://demo.realtysoft.pro/',
			'name' => 'Big left banner №2',
			'new_window' => 1,
			'is_admin' => 1,
			'number_of_clicks' => 0,
			'number_of_views' => 0,
			'status' => 1,
			'user_id' => 0,
			'stat_clicks' => 0,
			'stat_views' => 0,
			'user_activate_info' => '',
			'banner_groups' => $all_groups_ids,
		);
		$banners[] = array(
			'alt_text' => 'PG RealEstateSoftware',
			'approve' => 1,
			'banner_image' => 'c900b924eb.jpg',
			'banner_place_id' => 2,
			'banner_type' => 1,
			'decline_reason' => '',
			'expiration_date' => '0000-00-00 00:00:00',
			'html' => '',
			'link' => 'http://www.realtysoft.pro/pricing.php',
			'name' => 'Big left banner №3',
			'new_window' => 1,
			'is_admin' => 1,
			'number_of_clicks' => 0,
			'number_of_views' => 0,
			'status' => 1,
			'user_id' => 0,
			'stat_clicks' => 0,
			'stat_views' => 0,
			'user_activate_info' => '',
			'banner_groups' => $all_groups_ids,
		);
		$banners[] = array(
			'alt_text' => 'PG RealEstateSoftware',
			'approve' => 1,
			'banner_image' => '03041d2ac7.jpg',
			'banner_place_id' => 4,
			'banner_type' => 1,
			'decline_reason' => '',
			'expiration_date' => '0000-00-00 00:00:00',
			'html' => '',
			'link' => 'http://www.realtysoft.pro/pricing.php',
			'name' => 'Right banner №1',
			'new_window' => 1,
			'is_admin' => 1,
			'number_of_clicks' => 0,
			'number_of_views' => 0,
			'status' => 1,
			'user_id' => 0,
			'stat_clicks' => 0,
			'stat_views' => 0,
			'user_activate_info' => '',
			'banner_groups' => $all_groups_ids,
		);

		foreach($banners as $key => $banner){
			$this->CI->Banners_model->save(null, $banner);
		}
		return true;
	}
}
