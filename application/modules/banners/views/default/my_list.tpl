{include file="header.tpl"}

{include file="left_panel.tpl" module="start"}

<div class="rc">
	<div class="content-block">

		<h1>{l i='header_my_banners' gid='banners'}</h1>
		<div class="content-value">
			<a class="btn-link fright" href="{$site_url}banners/edit"><ins class="with-icon i-list-add"></ins><span>{l i='link_add_banner' gid='banners'}</span></a>
			<br><br>
			<table class="list">
			<tr id="sorter_block">
				<th class="w30">{l i='field_number' gid='banners'}</th>
				<th class="w30">&nbsp;</th>
				<th>{l i='field_name' gid='banners'}</th>
				<th>{l i='field_approve' gid='banners'}</th>
				<th class="w150">&nbsp;</th>
			</tr>
			{foreach from=$banners item=banner}
			{counter print=false assign=counter}
			<tr>
				<td class="centered">{$counter}</td>
				<td class="view-banner">
					<a href='#' onclick="return false;" id="view_{$banner.id}" class="btn-link" title="{l i='link_view_banner' gid='banners'}"><ins class="with-icon i-eye"></ins></a>
					<div id="view_{$banner.id}_content" style="display: none">
						{if $banner.banner_type == 1}<img src="{$banner.media.banner_image.file_url}" width="{$banner.banner_place_obj.width}" height="{$banner.banner_place_obj.height}" />{else}{$banner.html}{/if}
					</div>
				</td>
				<td>
				<b>{$banner.name}
				{if $banner.banner_place_obj}
				({$banner.banner_place_obj.name} {$banner.banner_place_obj.width}X{$banner.banner_place_obj.height})
				{/if}</b><br>
				{assign var="limit" value=''}
				{if $banner.number_of_views}
				{assign var="limit" value=true}
				{l i='shows' gid='banners'} - {$banner.number_of_views}
				<br/>
				{/if}
				{if $banner.number_of_clicks}
				{assign var="limit" value=true}
				{l i='clicks' gid='banners'} - {$banner.number_of_clicks}
				<br/>
				{/if}
				{if $banner.expiration_date and $banner.expiration_date != '0000-00-00 00:00:00'}
				{assign var="limit" value=true}
				{l i='till' gid='banners'} - {$banner.expiration_date}
				{/if}
				{if !$limit}{l i='never_stop' gid='banners'}{/if}

				</td>
				<td>
				{if $banner.approve eq '1'}<span class="status"><ins></ins>{l i='approved' gid='banners'}</span>
				{elseif $banner.approve eq '-1'}<span class="status decline"><ins></ins>{l i='declined' gid='banners'}</span>
				{else}<span class="status wait"><ins></ins>{l i='not_approved' gid='banners'}</span>{/if}
				</td>
				<td class="r">
					<a href="{$site_url}banners/delete/{$banner.id}" onclick="javascript: if(!confirm('{l i='note_delete_banner' gid='banners' type='js'}')) return false;" class="btn-link fright"><ins class="with-icon i-delete"></ins></a>
				{if $banner.approve eq '1'}
					<a href="{$site_url}banners/statistic/{$banner.id}" class="btn-link fright" title="{l i='link_banner_stat' gid='banners' type='button'}"><ins class="with-icon i-stat"></ins></a>
					{if !$banner.status}<a href="{$site_url}banners/activate/{$banner.id}" class="btn-link fright" title="{l i='link_banner_activate' gid='banners'}"><ins class="with-icon i-start"></ins></a>{/if}
				{/if}
				</td>
			</tr>
			{foreachelse}
			<tr>
				<td class="empty" colspan=5>{l i='no_banners' gid='banners'}</td>
			</tr>
			{/foreach}
			</table>

			{pagination data=$page_data type='full'}
			<br>

		</div>
	</div>
</div>
{js file='easyTooltip.min.js'}
<script type='text/javascript'>
{literal}
$(function(){
	$("td.view-banner > a").each(function(){
		var id = $(this).attr('id')+'_content';
		$(this).easyTooltip({useElement: id});
	});
});
{/literal}
</script>

<div class="clr"></div>
{include file="footer.tpl"}
