<?php 

$install_lang["notification_banner_need_moderate"] = "New banner awaiting moderation";
$install_lang["notification_banner_status_updated"] = "Banner status updated";
$install_lang["tpl_banner_need_moderate_content"] = "Hello admin,\n\nThere is a new banner awaiting moderation on [domain]. To view it, go to administration panel > Banners > User banners.\n\nBest regards,\n[name_from]";
$install_lang["tpl_banner_need_moderate_subject"] = "[domain] | New banner awaiting moderation";
$install_lang["tpl_banner_status_updated_content"] = "Hello [user],\n\nYour banner status is updated.\n[banner] - [status]\n\nBest regards,\n[name_from]";
$install_lang["tpl_banner_status_updated_subject"] = "[domain] | Banner status updated";
