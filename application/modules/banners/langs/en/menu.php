<?php

$install_lang["admin_banners_menu_banners_list_item"] = "Banners";
$install_lang["admin_banners_menu_banners_list_item_tooltip"] = "";
$install_lang["admin_banners_menu_banners_settings_item"] = "Settings";
$install_lang["admin_banners_menu_banners_settings_item_tooltip"] = "";
$install_lang["admin_banners_menu_groups_list_item"] = "Groups of pages";
$install_lang["admin_banners_menu_groups_list_item_tooltip"] = "";
$install_lang["admin_banners_menu_places_list_item"] = "Positions";
$install_lang["admin_banners_menu_places_list_item_tooltip"] = "";
$install_lang["admin_menu_other_items_banners_menu_item"] = "Banner ads";
$install_lang["admin_menu_other_items_banners_menu_item_tooltip"] = "";
$install_lang["agent_account_menu_agent_my_banners_item"] = "Banners";
$install_lang["agent_account_menu_agent_my_banners_item_tooltip"] = "";
$install_lang["company_account_menu_company_my_banners_item"] = "Banners";
$install_lang["company_account_menu_company_my_banners_item_tooltip"] = "";
$install_lang["private_account_menu_private_my_banners_item"] = "Banners";
$install_lang["private_account_menu_private_my_banners_item_tooltip"] = "";

