<?php

$install_lang["admin_menu_settings_items_uploads-items_uploads_menu_item"] = "Photos";
$install_lang["admin_menu_settings_items_uploads-items_uploads_menu_item_tooltip"] = "Settings of photos, thumbs & watermarks";
$install_lang["admin_uploads_menu_configs_list_item"] = "Settings";
$install_lang["admin_uploads_menu_configs_list_item_tooltip"] = "";
$install_lang["admin_uploads_menu_watermarks_list_item"] = "Watermarks";
$install_lang["admin_uploads_menu_watermarks_list_item_tooltip"] = "";

