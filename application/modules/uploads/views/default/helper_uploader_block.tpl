<link href="{$site_root}application/modules/uploads/views/default/css/style-{$_LANG.rtl}.css" rel="stylesheet" type="text/css" />
{js file='uploader.js'}

{if $use_drop_zone}
<div id="drop_zone_{$uploader_rand}" class="drop_zone hide">
	<p>{l i='load_files_by_drag_drop' gid='uploads'} &nbsp; <span>{l i='btn_choose_file' gid='uploads'}<input type="file" name="{$field_name}" multiple="true" id="upload_file_{$uploader_rand}"></span></p>
</div>
{/if}
<input type="file" name="{$field_name}" id="upload_file_simple_{$uploader_rand}" class="simple_uploader fleft {if $use_drop_zone}hide{/if}">
<input type="submit" class='btn' value="{l i='btn_add' gid='start' type='button'}" id="btn_upload_{$uploader_rand}">

<div class="clr"></div>

{if $use_drop_zone}
<script>{literal}
	var uploader_{/literal}{$uploader_rand}{literal};
	$(function(){
		uploader_{/literal}{$uploader_rand}{literal} = new uploader({
			siteUrl: '{/literal}{$site_url}{literal}',
			uploadUrl: '{/literal}{$upload_url}{literal}',
			zoneId: 'drop_zone_{/literal}{$uploader_rand}{literal}',
			fileId: 'upload_file_{/literal}{$uploader_rand}{literal}',
			{/literal}{if $uploader_form_id}formId: '{$uploader_form_id}',{/if}{literal}
			filebarId: 'filebar_{/literal}{$uploader_rand}{literal}',
			sendType: 'file',
			sendId: 'btn_upload_{/literal}{$uploader_rand}{literal}',
			maxFileSize: {/literal}{$max_file_size}{literal},
			mimeType:  ['image/jpg','image/png','image/jpeg','image/gif', 'video/x-flv','video/flv','video/x-matroska','application/octet-stream','video/x-msvideo','video/avi','video/msvideo','video/mpeg','video/x-ms-asf'],
			fieldName: '{/literal}{$field_name}{literal}',
			{/literal}{if $uploader_callback}callback: {strip}{$uploader_callback}{/strip},{/if}{literal}
			createThumb: true,
			thumbWidth: 100,
			thumbHeight: 100,
			thumbCrop: false,
			thumbJpeg: false,
			thumbBg: 'transparent',
			fileListInZone: true
		});
		if(uploader_{/literal}{$uploader_rand}{literal}.checkFileApi()){
			$('#drop_zone_{/literal}{$uploader_rand}{literal}').show();
		}else{
			$('#upload_file_simple_{/literal}{$uploader_rand}{literal}').after('<input type="hidden" name="no_ajax" value="1">').show();
			
			$('#btn_upload_{/literal}{$uploader_rand}{literal}').unbind().bind('click', function(){
				{/literal}{if $uploader_form_id}$('#{$uploader_form_id}').attr('action', '{$site_url}{$upload_url}').submit();{/if}{literal}
				return true;
			});
		}
	});
{/literal}</script>
{/if}
