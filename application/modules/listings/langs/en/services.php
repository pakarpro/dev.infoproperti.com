<?php

$install_lang["listings_featured_service"] = "Featured listings";
$install_lang["listings_featured_template"] = "Featured listings";
$install_lang["listings_featured_template_id_listing"] = "Listing";
$install_lang["listings_featured_template_period"] = "Period (days)";
$install_lang["listings_highlight_service"] = "Highlight listing in search";
$install_lang["listings_highlight_template"] = "Highlight listing in search";
$install_lang["listings_highlight_template_id_listing"] = "Listing";
$install_lang["listings_highlight_template_period"] = "Period (days)";
$install_lang["listings_lift_up_city_service"] = "Lift up listing in search for a selected city";
$install_lang["listings_lift_up_city_template"] = "Lift up listing in search for a selected city";
$install_lang["listings_lift_up_city_template_id_listing"] = "Listing";
$install_lang["listings_lift_up_city_template_period"] = "Period (days)";
$install_lang["listings_lift_up_country_service"] = "Lift up listing in search for a selected country";
$install_lang["listings_lift_up_country_template"] = "Lift up listing in search for a selected country";
$install_lang["listings_lift_up_country_template_id_listing"] = "Listing";
$install_lang["listings_lift_up_country_template_period"] = "Period (days)";
$install_lang["listings_lift_up_region_service"] = "Lift up listing in search for a selected region";
$install_lang["listings_lift_up_region_template"] = "Lift up listing in search for a selected region";
$install_lang["listings_lift_up_region_template_id_listing"] = "Listing";
$install_lang["listings_lift_up_region_template_period"] = "Period (days)";
$install_lang["listings_lift_up_service"] = "Lift up listing in search";
$install_lang["listings_lift_up_template"] = "Lift up listing in search";
$install_lang["listings_lift_up_template_id_listing"] = "Listing";
$install_lang["listings_lift_up_template_period"] = "Period (days)";
$install_lang["listings_slide_show_service"] = "Slide-show listing";
$install_lang["listings_slide_show_template"] = "Slide-show listing";
$install_lang["listings_slide_show_template_id_listing"] = "Listing";
$install_lang["listings_slide_show_template_period"] = "Period (days)";
#MOD#
$install_lang["listings_slider_service"] = "Slider listing";
$install_lang["listings_slider_template"] = "Slider listing";
$install_lang["listings_slider_template_id_listing"] = "Listing";
$install_lang["listings_slider_template_period"] = "Period (days)";
#MOD#