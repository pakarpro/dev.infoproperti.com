<?php

$install_lang["operation_type"]["header"] = "Тип объявления";
$install_lang["operation_type"]["option"]["sale"] = "продажа";
$install_lang["operation_type"]["option"]["buy"] = "покупка";
$install_lang["operation_type"]["option"]["rent"] = "аренда";
$install_lang["operation_type"]["option"]["lease"] = "арендую";

$install_lang["operation_type_search"]["header"] = "Тип объявления (в поиске)";
$install_lang["operation_type_search"]["option"]["sale"] = "Продажа";
$install_lang["operation_type_search"]["option"]["buy"] = "Покупка";
$install_lang["operation_type_search"]["option"]["rent"] = "Аренда";
$install_lang["operation_type_search"]["option"]["lease"] = "Арендую";

$install_lang["square_units"]["header"] = "Единицы измерения";
$install_lang["square_units"]["option"]["1"] = "кв.ф.";
$install_lang["square_units"]["option"]["2"] = "кв.м.";

$install_lang["posted_within"]["header"] = "Опубликовано";
$install_lang["posted_within"]["option"]["30"] = "за последние 30 дней";
$install_lang["posted_within"]["option"]["7"] = "за последние 7 дней";
$install_lang["posted_within"]["option"]["3"] = "за последние 3 дня";
$install_lang["posted_within"]["option"]["1"] = "вчера";

$install_lang["radius_data"]["header"] = "Радиус";
$install_lang["radius_data"]["option"]["1"] = "1 км";
$install_lang["radius_data"]["option"]["5"] = "5 км";
$install_lang["radius_data"]["option"]["10"] = "10 км";

$install_lang["price_period"]["header"] = "Период";
$install_lang["price_period"]["option"]["1"] = "за день";
$install_lang["price_period"]["option"]["2"] = "за месяц";

$install_lang["price_type"]["header"] = "Цена";
$install_lang["price_type"]["option"]["1"] = "за номер";
$install_lang["price_type"]["option"]["2"] = "за человека";

$install_lang["booking_status"]["header"] = "Статус бронирования";
$install_lang["booking_status"]["option"]["open"] = "Доступно";
$install_lang["booking_status"]["option"]["close"] = "Закрыто";
$install_lang["booking_status"]["option"]["book"] = "Занято";
$install_lang["booking_status"]["option"]["wait"] = "Ожидает рассмотрения";

$install_lang["booking_guests"]["header"] = "Гости";
$install_lang["booking_guests"]["option"]["1"] = "1";
$install_lang["booking_guests"]["option"]["2"] = "2";
$install_lang["booking_guests"]["option"]["3"] = "3";
$install_lang["booking_guests"]["option"]["4"] = "4";
$install_lang["booking_guests"]["option"]["5"] = "5";
$install_lang["booking_guests"]["option"]["6"] = "6";
$install_lang["booking_guests"]["option"]["7"] = "7";
$install_lang["booking_guests"]["option"]["8"] = "8";
$install_lang["booking_guests"]["option"]["9"] = "9";
$install_lang["booking_guests"]["option"]["10"] = "10";
$install_lang["booking_guests"]["option"]["11"] = "10+";
