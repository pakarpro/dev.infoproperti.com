<?php

$relations = array(
	array("type"=>"int", "link"=>"id"),
	array("type"=>"text", "link"=>"id_user"),
	array("type"=>"text", "link"=>"id_type"),
	array("type"=>"text", "link"=>"id_category"),
	array("type"=>"int", "link"=>"status"),
	array("type"=>"int", "link"=>"sold"),
	array("type"=>"text", "link"=>"date_open"),
	array("type"=>"text", "link"=>"date_available"),
	array("type"=>"text", "link"=>"date_created"),
	array("type"=>"text", "link"=>"date_modified"),
	array("type"=>"text", "link"=>"date_activity"),
	array("type"=>"text", "link"=>"date_expire"),
	array("type"=>"int", "link"=>"price"),
	array("type"=>"int", "link"=>"price_max"),
	array("type"=>"text", "link"=>"gid_currency"),
	array("type"=>"int", "link"=>"square"),
	array("type"=>"int", "link"=>"square_max"),
	array("type"=>"text", "link"=>"square_unit"),
	array("type"=>"text", "link"=>"address"),
	array("type"=>"text", "link"=>"id_city"),
	array("type"=>"text", "link"=>"id_region"),
	array("type"=>"text", "link"=>"id_country"),
	array("type"=>"text", "link"=>"zip"),
	array("type"=>"text", "link"=>"lat"),
	array("type"=>"text", "link"=>"lon"),
	array("type"=>"text", "link"=>"listing_file"),
	array("type"=>"text", "link"=>"listing_file_name"),
	array("type"=>"text", "link"=>"listing_file_comment"),
	array("type"=>"text", "link"=>"listing_file_date"),
	array("type"=>"text", "link"=>"listing_video"),
	array("type"=>"text", "link"=>"listing_video_image"),
	array("type"=>"text", "link"=>"listing_video_data"),
	array("type"=>"int", "link"=>"initial_moderation"),
	array("type"=>"int", "link"=>"initial_activity"),
	array("type"=>"text", "link"=>"logo_image"),
	array("type"=>"text", "link"=>"slider_image"),
	array("type"=>"text", "link"=>"listing_video"),
	array("type"=>"text", "link"=>"listing_video_image"),
	array("type"=>"text", "link"=>"listing_video_data"),
	array("type"=>"text", "link"=>"featured_date_end"),
	array("type"=>"text", "link"=>"lift_up_date_end"),
	array("type"=>"text", "link"=>"lift_up_country_date_end"),
	array("type"=>"text", "link"=>"lift_up_region_date_end"),
	array("type"=>"text", "link"=>"lift_up_city_date_end"),
	array("type"=>"text", "link"=>"highlight_date_end"),
	array("type"=>"text", "link"=>"slide_show_date_end"),
	array('type'=>'text', 'link'=>'id_wish_lists'),
	array("type"=>"int", "link"=>"hits"),
	array("type"=>"int", "link"=>"set_to_subscribe"),
	array("type"=>"int", "link"=>"closed"),
	array("type"=>"int", "link"=>"fe_construction_status_1"),
	array("type"=>"int", "link"=>"fe_live_square_1"),
	array("type"=>"int", "link"=>"fe_bd_rooms_1"),
	array("type"=>"int", "link"=>"fe_bth_rooms_1"),
	array("type"=>"int", "link"=>"fe_garages_1"),
	array("type"=>"int", "link"=>"fe_year_1"),
	array("type"=>"int", "link"=>"fe_foundation_1"),
	array("type"=>"int", "link"=>"fe_floor_number_1"),
	array("type"=>"int", "link"=>"fe_total_floors_1"),
	array("type"=>"int", "link"=>"fe_distance_to_subway_1"),
	array("type"=>"int", "link"=>"fe_furniture_1"),
	array("type"=>"text", "link"=>"fe_accomodation_1_option_1"),
	array("type"=>"text", "link"=>"fe_accomodation_1_option_2"),
	array("type"=>"text", "link"=>"fe_accomodation_1_option_3"),
	array("type"=>"text", "link"=>"fe_accomodation_1_option_4"),
	array("type"=>"text", "link"=>"fe_accomodation_1_option_5"),
	array("type"=>"text", "link"=>"fe_accomodation_1_option_6"),
	array("type"=>"text", "link"=>"fe_accomodation_1_option_7"),
	array("type"=>"text", "link"=>"fe_accomodation_1_option_8"),
	array("type"=>"text", "link"=>"fe_accomodation_1_option_9"),
	array("type"=>"text", "link"=>"fe_accomodation_1_option_10"),
	array("type"=>"text", "link"=>"fe_accomodation_1_option_11"),
	array("type"=>"text", "link"=>"fe_accomodation_1_option_12"),
	array("type"=>"text", "link"=>"fe_accomodation_1_option_13"),
	array("type"=>"text", "link"=>"fe_accomodation_1_option_14"),
	array("type"=>"text", "link"=>"fe_accomodation_1_option_15"),
	array("type"=>"text", "link"=>"fe_accomodation_1_option_16"),
	array("type"=>"text", "link"=>"fe_accomodation_1_option_17"),
	array("type"=>"text", "link"=>"fe_accomodation_1_option_18"),
	array("type"=>"text", "link"=>"fe_accomodation_1_option_19"),
	array("type"=>"text", "link"=>"fe_accomodation_1_option_20"),
	array("type"=>"text", "link"=>"fe_accomodation_1_option_21"),
	array("type"=>"text", "link"=>"fe_accomodation_1_option_22"),
	array("type"=>"text", "link"=>"fe_accomodation_1_option_23"),
	array("type"=>"text", "link"=>"fe_accomodation_1_option_24"),
	array("type"=>"text", "link"=>"fe_accomodation_1_option_25"),
	array("type"=>"text", "link"=>"fe_accomodation_1_option_26"),
	array("type"=>"text", "link"=>"fe_accomodation_1_option_27"),
	array("type"=>"text", "link"=>"fe_accomodation_1_option_28"),
	array("type"=>"text", "link"=>"fe_appliances_1_option_1"),
	array("type"=>"text", "link"=>"fe_appliances_1_option_2"),
	array("type"=>"text", "link"=>"fe_appliances_1_option_3"),
	array("type"=>"text", "link"=>"fe_appliances_1_option_4"),
	array("type"=>"text", "link"=>"fe_appliances_1_option_5"),
	array("type"=>"text", "link"=>"fe_appliances_1_option_6"),
	array("type"=>"text", "link"=>"fe_appliances_1_option_7"),
	array("type"=>"text", "link"=>"fe_appliances_1_option_8"),
	array("type"=>"text", "link"=>"fe_appliances_1_option_9"),
	array("type"=>"text", "link"=>"fe_appliances_1_option_10"),
	array("type"=>"text", "link"=>"fe_appliances_1_option_11"),
	array("type"=>"text", "link"=>"fe_appliances_1_option_12"),
	array("type"=>"text", "link"=>"fe_appliances_1_option_13"),
	array("type"=>"text", "link"=>"fe_appliances_1_option_14"),
	array("type"=>"text", "link"=>"fe_appliances_1_option_15"),
	array("type"=>"text", "link"=>"fe_appliances_1_option_16"),
	array("type"=>"text", "link"=>"fe_appliances_1_option_17"),
	array("type"=>"text", "link"=>"fe_appliances_1_option_18"),
	array("type"=>"text", "link"=>"fe_appliances_1_option_19"),
	array("type"=>"text", "link"=>"fe_amenities_1_option_1"),
	array("type"=>"text", "link"=>"fe_amenities_1_option_2"),
	array("type"=>"text", "link"=>"fe_amenities_1_option_3"),
	array("type"=>"text", "link"=>"fe_amenities_1_option_4"),
	array("type"=>"text", "link"=>"fe_amenities_1_option_5"),
	array("type"=>"text", "link"=>"fe_amenities_1_option_6"),
	array("type"=>"text", "link"=>"fe_amenities_1_option_7"),
	array("type"=>"text", "link"=>"fe_amenities_1_option_8"),
	array("type"=>"text", "link"=>"fe_amenities_1_option_9"),
	array("type"=>"text", "link"=>"fe_amenities_1_option_10"),
	array("type"=>"text", "link"=>"fe_amenities_1_option_11"),
	array("type"=>"text", "link"=>"fe_amenities_1_option_12"),
	array("type"=>"text", "link"=>"fe_amenities_1_option_13"),
	array("type"=>"text", "link"=>"fe_amenities_1_option_14"),
	array("type"=>"text", "link"=>"fe_amenities_1_option_15"),
	array("type"=>"text", "link"=>"fe_amenities_1_option_16"),
	array("type"=>"text", "link"=>"fe_amenities_1_option_17"),
	array("type"=>"text", "link"=>"fe_amenities_1_option_18"),
	array("type"=>"text", "link"=>"fe_environment_1_option_1"),
	array("type"=>"text", "link"=>"fe_environment_1_option_2"),
	array("type"=>"text", "link"=>"fe_environment_1_option_3"),
	array("type"=>"text", "link"=>"fe_environment_1_option_4"),
	array("type"=>"text", "link"=>"fe_environment_1_option_5"),
	array("type"=>"text", "link"=>"fe_environment_1_option_6"),
	array("type"=>"text", "link"=>"fe_environment_1_option_7"),
	array("type"=>"text", "link"=>"fe_environment_1_option_8"),
	array("type"=>"text", "link"=>"fe_environment_1_option_9"),
	array("type"=>"text", "link"=>"fe_environment_1_option_10"),
	array("type"=>"text", "link"=>"fe_environment_1_option_11"),
	array("type"=>"text", "link"=>"fe_environment_1_option_12"),
	array("type"=>"text", "link"=>"fe_environment_1_option_13"),
	array("type"=>"text", "link"=>"fe_environment_1_option_14"),
	array("type"=>"text", "link"=>"fe_environment_1_option_15"),
	array("type"=>"text", "link"=>"fe_environment_1_option_16"),
	array("type"=>"text", "link"=>"fe_environment_1_option_17"),
	array("type"=>"text", "link"=>"fe_environment_1_option_18"),
	array("type"=>"int", "link"=>"fe_construction_status_2"),
	array("type"=>"int", "link"=>"fe_year_2"),
	array("type"=>"int", "link"=>"fe_foundation_2"),
	array("type"=>"int", "link"=>"fe_revenue_2"),
	array("type"=>"int", "link"=>"fe_cash_flow_2"),
	array("type"=>"int", "link"=>"fe_years_established_2"),
	array("type"=>"int", "link"=>"fe_employees_2"),
	array("type"=>"text", "link"=>"fe_reason_for_selling_2"),
	array("type"=>"int", "link"=>"fe_irrigated_3"),
	array("type"=>"int", "link"=>"fe_residence_3"),
	array("type"=>"text", "link"=>"fe_check_in_4"),
	array("type"=>"text", "link"=>"fe_check_out_4"),
	array("type"=>"text", "link"=>"fe_room_type_4"),
	array("type"=>"text", "link"=>"fe_guests_4"),
	array("type"=>"text", "link"=>"fe_placement_4"),
	array("type"=>"int", "link"=>"fe_live_square_4"),
	array("type"=>"int", "link"=>"fe_bd_rooms_4"),
	array("type"=>"int", "link"=>"fe_bth_rooms_4"),
	array("type"=>"int", "link"=>"fe_garages_4"),
	array("type"=>"int", "link"=>"fe_year_4"),
	array("type"=>"int", "link"=>"fe_floor_number_4"),
	array("type"=>"int", "link"=>"fe_total_floors_4"),
	array("type"=>"int", "link"=>"fe_distance_to_subway_4"),
	array("type"=>"int", "link"=>"fe_furniture_4"),
	array("type"=>"text", "link"=>"fe_accomodation_4_option_1"),
	array("type"=>"text", "link"=>"fe_accomodation_4_option_2"),
	array("type"=>"text", "link"=>"fe_accomodation_4_option_3"),
	array("type"=>"text", "link"=>"fe_accomodation_4_option_4"),
	array("type"=>"text", "link"=>"fe_accomodation_4_option_5"),
	array("type"=>"text", "link"=>"fe_accomodation_4_option_6"),
	array("type"=>"text", "link"=>"fe_accomodation_4_option_7"),
	array("type"=>"text", "link"=>"fe_accomodation_4_option_8"),
	array("type"=>"text", "link"=>"fe_accomodation_4_option_9"),
	array("type"=>"text", "link"=>"fe_accomodation_4_option_10"),
	array("type"=>"text", "link"=>"fe_accomodation_4_option_11"),
	array("type"=>"text", "link"=>"fe_accomodation_4_option_12"),
	array("type"=>"text", "link"=>"fe_accomodation_4_option_13"),
	array("type"=>"text", "link"=>"fe_accomodation_4_option_14"),
	array("type"=>"text", "link"=>"fe_accomodation_4_option_15"),
	array("type"=>"text", "link"=>"fe_accomodation_4_option_16"),
	array("type"=>"text", "link"=>"fe_accomodation_4_option_17"),
	array("type"=>"text", "link"=>"fe_accomodation_4_option_18"),
	array("type"=>"text", "link"=>"fe_accomodation_4_option_19"),
	array("type"=>"text", "link"=>"fe_accomodation_4_option_20"),
	array("type"=>"text", "link"=>"fe_accomodation_4_option_21"),
	array("type"=>"text", "link"=>"fe_accomodation_4_option_22"),
	array("type"=>"text", "link"=>"fe_accomodation_4_option_23"),
	array("type"=>"text", "link"=>"fe_accomodation_4_option_24"),
	array("type"=>"text", "link"=>"fe_accomodation_4_option_25"),
	array("type"=>"text", "link"=>"fe_accomodation_4_option_26"),
	array("type"=>"text", "link"=>"fe_accomodation_4_option_27"),
	array("type"=>"text", "link"=>"fe_accomodation_4_option_28"),
	array("type"=>"text", "link"=>"fe_appliances_4_option_1"),
	array("type"=>"text", "link"=>"fe_appliances_4_option_2"),
	array("type"=>"text", "link"=>"fe_appliances_4_option_3"),
	array("type"=>"text", "link"=>"fe_appliances_4_option_4"),
	array("type"=>"text", "link"=>"fe_appliances_4_option_5"),
	array("type"=>"text", "link"=>"fe_appliances_4_option_6"),
	array("type"=>"text", "link"=>"fe_appliances_4_option_7"),
	array("type"=>"text", "link"=>"fe_appliances_4_option_8"),
	array("type"=>"text", "link"=>"fe_appliances_4_option_9"),
	array("type"=>"text", "link"=>"fe_appliances_4_option_10"),
	array("type"=>"text", "link"=>"fe_appliances_4_option_11"),
	array("type"=>"text", "link"=>"fe_appliances_4_option_12"),
	array("type"=>"text", "link"=>"fe_appliances_4_option_13"),
	array("type"=>"text", "link"=>"fe_appliances_4_option_14"),
	array("type"=>"text", "link"=>"fe_appliances_4_option_15"),
	array("type"=>"text", "link"=>"fe_appliances_4_option_16"),
	array("type"=>"text", "link"=>"fe_appliances_4_option_17"),
	array("type"=>"text", "link"=>"fe_appliances_4_option_18"),
	array("type"=>"text", "link"=>"fe_appliances_4_option_19"),
	array("type"=>"text", "link"=>"fe_amenities_4_option_1"),
	array("type"=>"text", "link"=>"fe_amenities_4_option_2"),
	array("type"=>"text", "link"=>"fe_amenities_4_option_3"),
	array("type"=>"text", "link"=>"fe_amenities_4_option_4"),
	array("type"=>"text", "link"=>"fe_amenities_4_option_5"),
	array("type"=>"text", "link"=>"fe_amenities_4_option_6"),
	array("type"=>"text", "link"=>"fe_amenities_4_option_7"),
	array("type"=>"text", "link"=>"fe_amenities_4_option_8"),
	array("type"=>"text", "link"=>"fe_amenities_4_option_9"),
	array("type"=>"text", "link"=>"fe_amenities_4_option_10"),
	array("type"=>"text", "link"=>"fe_amenities_4_option_11"),
	array("type"=>"text", "link"=>"fe_amenities_4_option_12"),
	array("type"=>"text", "link"=>"fe_amenities_4_option_13"),
	array("type"=>"text", "link"=>"fe_amenities_4_option_14"),
	array("type"=>"text", "link"=>"fe_amenities_4_option_15"),
	array("type"=>"text", "link"=>"fe_amenities_4_option_16"),
	array("type"=>"text", "link"=>"fe_amenities_4_option_17"),
	array("type"=>"text", "link"=>"fe_amenities_4_option_18"),
	array("type"=>"text", "link"=>"fe_environment_4_option_1"),
	array("type"=>"text", "link"=>"fe_environment_4_option_2"),
	array("type"=>"text", "link"=>"fe_environment_4_option_3"),
	array("type"=>"text", "link"=>"fe_environment_4_option_4"),
	array("type"=>"text", "link"=>"fe_environment_4_option_5"),
	array("type"=>"text", "link"=>"fe_environment_4_option_6"),
	array("type"=>"text", "link"=>"fe_environment_4_option_7"),
	array("type"=>"text", "link"=>"fe_environment_4_option_8"),
	array("type"=>"text", "link"=>"fe_environment_4_option_9"),
	array("type"=>"text", "link"=>"fe_environment_4_option_10"),
	array("type"=>"text", "link"=>"fe_environment_4_option_11"),
	array("type"=>"text", "link"=>"fe_environment_4_option_12"),
	array("type"=>"text", "link"=>"fe_environment_4_option_13"),
	array("type"=>"text", "link"=>"fe_environment_4_option_14"),
	array("type"=>"text", "link"=>"fe_environment_4_option_15"),
	array("type"=>"text", "link"=>"fe_environment_4_option_16"),
	array("type"=>"text", "link"=>"fe_environment_4_option_17"),
	array("type"=>"text", "link"=>"fe_environment_4_option_18"),
	array("type"=>"int", "link"=>"fe_year_5"),
	array("type"=>"int", "link"=>"fe_foundation_5"),
	array("type"=>"int", "link"=>"fe_revenue_5"),
	array("type"=>"int", "link"=>"fe_cash_flow_5"),
	array("type"=>"int", "link"=>"fe_years_established_5"),
	array("type"=>"int", "link"=>"fe_employees_5"),
	array("type"=>"text", "link"=>"fe_reason_for_selling_5"),
	array("type"=>"int", "link"=>"fe_irrigated_6"),
	array("type"=>"int", "link"=>"fe_residence_6"),
);

$demo_content["realty_1"]["id"] = 1;
$demo_content["realty_1"]["id_user"] = "Pilot Group";
$demo_content["realty_1"]["id_type"] = "sale";
$demo_content["realty_1"]["id_category"] = "Residential";
$demo_content["realty_1"]["property_type"] = "Apartment";
$demo_content["realty_1"]["status"] = 1;
$demo_content["realty_1"]["sold"] = 1;
$demo_content["realty_1"]["date_available"] = "2013-07-07 00:00:00";
$demo_content["realty_1"]["date_open"] = "2013-09-01 00:00:00";
$demo_content["realty_1"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_1"]["price"] = 529900;
$demo_content["realty_1"]["gid_currency"] = "USD";
$demo_content["realty_1"]["square"] = 4073;
$demo_content["realty_1"]["square_unit"] = "sqft";
$demo_content["realty_1"]["address"] = "255 Broadway";
$demo_content["realty_1"]["id_country"] = "United States";
$demo_content["realty_1"]["id_region"] = "New York";
$demo_content["realty_1"]["id_city"] = "New York City";
$demo_content["realty_1"]["zip"] = "10007";
$demo_content["realty_1"]["lat"] = "40.7132997";
$demo_content["realty_1"]["lon"] = "-74.0070671";
$demo_content["realty_1"]["logo_image"] = "6d7b10a740.jpg";
$demo_content["realty_1"]["slider_image"] = "6d7b10a740.jpg";
$demo_content["realty_1"]["listing_file"] = "ae6f3bf295.docx";
$demo_content["realty_1"]["listing_file_comment"] = "In architecture and building engineering, a floor plan otherwise known as a scottish plan is a drawing to scale, showing a view from above, of the relationships between rooms, spaces and other physical features at one level of a structure. Dimensions are usually drawn between the walls to specify room sizes and wall lengths. Floor plans may also include details of fixtures like sinks, water heaters, furnaces, etc. Floor plans may include notes for construction to specify finishes, construction methods, or symbols for electrical items.";
$demo_content["realty_1"]["listing_file_date"] = "2013-08-05 05:51:28";
$demo_content["realty_1"]["listing_video"] = "101_01c0be62.flv";
$demo_content["realty_1"]["listing_video_image"] = "101_01c0be62.jpg";
$demo_content["realty_1"]["listing_video_data"] = array("data"=>array("file_name"=>"101_01c0be62.avi","file_path"=>"/home/svinokurov/public_html/pg_realestate2/uploads/video/listing-video/1/1/","file_data"=>array("name"=>"United States, New York City, 255 Broadway, 10007","description"=>"","ext"=>"avi","type"=>"video/x-msvideo"),"status"=>"start","id_object"=>"1","video_upload_gid"=>"listing-video","video"=>"101_01c0be62.flv","upload_type"=>"local", "image"=>"101_01c0be62.jpg"),"status"=>"end","errors"=>array());
$demo_content["realty_1"]["id_wish_lists"] = array(1);
$demo_content["realty_1"]["featured_date_end"] = "2020-01-01 00:00";
$demo_content["realty_1"]["fe_construction_status_1"] = 1;
$demo_content["realty_1"]["fe_live_square_1"] = 3600;
$demo_content["realty_1"]["fe_bd_rooms_1"] = 3;
$demo_content["realty_1"]["fe_bth_rooms_1"] = 1;
$demo_content["realty_1"]["fe_garages_1"] = 1;
$demo_content["realty_1"]["fe_year_1"] = 1980;
$demo_content["realty_1"]["fe_foundation_1"] = 5;
$demo_content["realty_1"]["fe_floor_number_1"] = 2;
$demo_content["realty_1"]["fe_total_floors_1"] = 9;
$demo_content["realty_1"]["fe_distance_to_subway_1"] = 5;
$demo_content["realty_1"]["fe_furniture_1"] = 1;
$demo_content["realty_1"]["fe_accomodation_1_option_1"] = 1;
$demo_content["realty_1"]["fe_accomodation_1_option_2"] = 1;
$demo_content["realty_1"]["fe_accomodation_1_option_3"] = 1;
$demo_content["realty_1"]["fe_accomodation_1_option_5"] = 1;
$demo_content["realty_1"]["fe_accomodation_1_option_9"] = 1;
$demo_content["realty_1"]["fe_appliances_1_option_1"] = 1;
$demo_content["realty_1"]["fe_appliances_1_option_2"] = 1;
$demo_content["realty_1"]["fe_appliances_1_option_3"] = 1;
$demo_content["realty_1"]["fe_appliances_1_option_5"] = 1;
$demo_content["realty_1"]["fe_appliances_1_option_9"] = 1;
$demo_content["realty_1"]["fe_amenities_1_option_1"] = 1;
$demo_content["realty_1"]["fe_amenities_1_option_2"] = 1;
$demo_content["realty_1"]["fe_amenities_1_option_3"] = 1;
$demo_content["realty_1"]["fe_amenities_1_option_4"] = 1;
$demo_content["realty_1"]["fe_amenities_1_option_5"] = 1;
$demo_content["realty_1"]["fe_environment_1_option_1"] = 1;
$demo_content["realty_1"]["fe_environment_1_option_2"] = 1;
$demo_content["realty_1"]["fe_environment_1_option_3"] = 1;
$demo_content["realty_1"]["fe_environment_1_option_4"] = 1;
$demo_content["realty_1"]["fe_environment_1_option_5"] = 1;
$demo_content["realty_1"]["headline"] = "Spotless Clarkson Bungalow On Mature Beautifully Treed 60'X125' Lot. Steps To Clarkson Village, Parks & Public Transit. Lorne Park School District.";
$demo_content["realty_1"]["fe_comments_1"] = "3 family Brownstone. Lots of original details. Needs to be seen. View of the Empire State Building from the top floor. 2bd/2bd/4bd duplex/semi-finished basement. Will be delivered  vacant.";

$demo_content["realty_2"]["id"] = 2;
$demo_content["realty_2"]["id_user"] = "Pilot Group";
$demo_content["realty_2"]["id_type"] = "sale";
$demo_content["realty_2"]["id_category"] = "Commercial";
$demo_content["realty_2"]["property_type"] = "Café";
$demo_content["realty_2"]["status"] = 1;
$demo_content["realty_2"]["sold"] = 1;
$demo_content["realty_2"]["date_open"] = "2015-05-01 00:00:00";
$demo_content["realty_2"]["date_available"] = "2020-01-01 00:00:00";
$demo_content["realty_2"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_2"]["price"] = 1050000;
$demo_content["realty_2"]["gid_currency"] = "USD";
$demo_content["realty_2"]["square"] = 3833;
$demo_content["realty_2"]["square_unit"] = "sqft";
$demo_content["realty_2"]["address"] = "45 Maydwell House Thomas Road";
$demo_content["realty_2"]["id_country"] = "United Kingdom";
$demo_content["realty_2"]["id_region"] = "Greater London";
$demo_content["realty_2"]["id_city"] = "London";
$demo_content["realty_2"]["zip"] = "E14 7AP";
$demo_content["realty_2"]["lat"] = "51.5112139";
$demo_content["realty_2"]["lon"] = "-0.1198244";
$demo_content["realty_2"]["logo_image"] = "bb53142478.jpg";
$demo_content["realty_2"]["slider_image"] = "bb53142478.jpg";
$demo_content["realty_2"]["id_wish_lists"] = array(1);
$demo_content["realty_2"]["featured_date_end"] = "2020-01-01 00:00";
$demo_content["realty_2"]["highlight_date_end"] = "2020-01-01 00:00";
$demo_content["realty_2"]["fe_construction_status_2"] = 1;
$demo_content["realty_2"]["fe_year_2"] = 1994;
$demo_content["realty_2"]["fe_foundation_2"] = 3;
$demo_content["realty_2"]["fe_revenue_2"] = 65;
$demo_content["realty_2"]["fe_cash_flow_2"] = 10000;
$demo_content["realty_2"]["fe_years_established_2"] = 10;
$demo_content["realty_2"]["fe_employees_2"] = 5;
$demo_content["realty_2"]["fe_reason_for_selling_2"] = "";
$demo_content["realty_2"]["headline"] = "This Stunning Bungalow In Clarkson Is Ideally Suited For A Young Professional Couple. 2+1 Bedroom Home Is Move-In Ready. Completely Renovated From Top To Bottom With A Fully Finished Basement, This Home Is An Entertainer's Delight!";
$demo_content["realty_2"]["fe_comments_2"] = "HOUSE MUST BE MOVED!! House is in good condition with all appliances.";

$demo_content["realty_3"]["id"] = 3;
$demo_content["realty_3"]["id_user"] = "Pilot Group";
$demo_content["realty_3"]["id_type"] = "sale";
$demo_content["realty_3"]["id_category"] = "Lots/lands";
$demo_content["realty_3"]["property_type"] = "Farm";
$demo_content["realty_3"]["status"] = 1;
$demo_content["realty_3"]["sold"] = 0;
$demo_content["realty_3"]["date_open"] = "2020-01-01 00:00:00";
$demo_content["realty_3"]["date_available"] = "2014-01-07 00:00:00";
$demo_content["realty_3"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_3"]["price"] = 350000;
$demo_content["realty_3"]["gid_currency"] = "USD";
$demo_content["realty_3"]["square"] = 3550;
$demo_content["realty_3"]["square_unit"] = "sqft";
$demo_content["realty_3"]["address"] = "73 rue du Faubourg Saint-Honore";
$demo_content["realty_3"]["id_country"] = "France";
$demo_content["realty_3"]["id_region"] = "Région Île-de-France";
$demo_content["realty_3"]["id_city"] = "Paris";
$demo_content["realty_3"]["zip"] = "75007";
$demo_content["realty_3"]["lat"] = "48.8715811";
$demo_content["realty_3"]["lon"] = "2.3147366";
$demo_content["realty_3"]["logo_image"] = "d81e88ba53.jpg";
$demo_content["realty_3"]["slider_image"] = "d81e88ba53.jpg";
$demo_content["realty_3"]["listing_file"] = "ae6f3bf295.docx";
$demo_content["realty_3"]["listing_file_comment"] = "In architecture and building engineering, a floor plan otherwise known as a scottish plan is a drawing to scale, showing a view from above, of the relationships between rooms, spaces and other physical features at one level of a structure. Dimensions are usually drawn between the walls to specify room sizes and wall lengths. Floor plans may also include details of fixtures like sinks, water heaters, furnaces, etc. Floor plans may include notes for construction to specify finishes, construction methods, or symbols for electrical items.";
$demo_content["realty_3"]["listing_file_date"] = "2013-08-05 05:51:28";
$demo_content["realty_3"]["listing_video"] = "101_01c0be62.flv";
$demo_content["realty_3"]["listing_video_image"] = "101_01c0be62.jpg";
$demo_content["realty_3"]["listing_video_data"] = array("data"=>array("file_name"=>"101_01c0be62.avi","file_path"=>"/home/svinokurov/public_html/pg_realestate2/uploads/video/listing-video/1/3/","file_data"=>array("name"=>"France, Paris, 73 rue du Faubourg Saint-Honore, 75007","description"=>"","ext"=>"avi","type"=>"video/x-msvideo"),"status"=>"start","id_object"=>"3","video_upload_gid"=>"listing-video","video"=>"101_01c0be62.flv","upload_type"=>"local", "image"=>"101_01c0be62.jpg"),"status"=>"end","errors"=>array());
$demo_content["realty_3"]["fe_irrigated_3"] = 1;
$demo_content["realty_3"]["fe_residence_3"] = 1;
$demo_content["realty_3"]["headline"] = "Great Property In Super Location Backing Onto A Open Space. New S/S Fridge And Stove. New Tiles In The Kitchen. Hardwood Floors. Finished Basement With A Separate Entrance.";
$demo_content["realty_3"]["fe_comments_3"] = "****AFFORTABLE HOME WITH A GREAT VIEW OF MT.HOOD* **** ***WASHOUGAL WASHINTION*** NEW HOME, NEVER LIVED IN --- BUILD IN 2007 --- 3504 Sq Ft, 3 Bedroom 2.5 Bathrooms & Office --- Hardwood & Title Floors --- Vaulted Ceiling in All Bedrooms --- Formal Dining Room w/ Built- in Sideboard --- Big Room with Fireplace, Sound System --- Gourmet Kitchen w/ SlabGranite Countertops, Island, Eat Bar, Stainless Steel Appliances, Pantry --- Nook with Build- in Desk & Cabinets, Access to Covered Desk & Big Patio --- Master Suite w/ Jet- Tub Shower, Walk-in Closet, Dual Vanities, Linen Closet --- Laundry Room w/ Cabinets, Sink & Ironing Board --- Landscape w/ Sprinklers --- 3 Car Tandem Garage --- Central Heating & A/C, Central Vacuum System --- Excellent Camas Schools, Great Location --- VIEW TO INJOY!!!";

$demo_content["realty_4"]["id"] = 4;
$demo_content["realty_4"]["id_user"] = "Mark Louis";
$demo_content["realty_4"]["id_type"] = "sale";
$demo_content["realty_4"]["id_category"] = "Residential";
$demo_content["realty_4"]["property_type"] = "Room";
$demo_content["realty_4"]["status"] = 1;
$demo_content["realty_4"]["sold"] = 0;
$demo_content["realty_4"]["date_open"] = "2012-06-11 00:00:00";
$demo_content["realty_4"]["date_available"] = "2013-06-27 00:00:00";
$demo_content["realty_4"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_4"]["price"] = 180000;
$demo_content["realty_4"]["gid_currency"] = "USD";
$demo_content["realty_4"]["square"] = 1800;
$demo_content["realty_4"]["square_unit"] = "sqft";
$demo_content["realty_4"]["address"] = "Budapesterstr. 45, Im europe center";
$demo_content["realty_4"]["id_country"] = "Germany";
$demo_content["realty_4"]["id_region"] = "Land Berlin";
$demo_content["realty_4"]["id_city"] = "Berlin";
$demo_content["realty_4"]["zip"] = "10787";
$demo_content["realty_4"]["lat"] = "52.6335256";
$demo_content["realty_4"]["lon"] = "13.3321539";
$demo_content["realty_4"]["logo_image"] = "ac5dccb860.jpg";
$demo_content["realty_4"]["slider_image"] = "ac5dccb860.jpg";
$demo_content["realty_4"]["id_wish_lists"] = array(1, 2);
$demo_content["realty_4"]["slide_show_date_end"] = "2020-01-01 00:00";
$demo_content["realty_4"]["lift_up_date_end"] = "2020-01-01 00:00";
$demo_content["realty_4"]["fe_construction_status_1"] = 2;
$demo_content["realty_4"]["fe_live_square_1"] = 1600;
$demo_content["realty_4"]["fe_bd_rooms_1"] = 2;
$demo_content["realty_4"]["fe_bth_rooms_1"] = 1;
$demo_content["realty_4"]["fe_garages_1"] = 0;
$demo_content["realty_4"]["fe_year_1"] = 1964;
$demo_content["realty_4"]["fe_foundation_1"] = 1;
$demo_content["realty_4"]["fe_floor_number_1"] = 4;
$demo_content["realty_4"]["fe_total_floors_1"] = 5;
$demo_content["realty_4"]["fe_distance_to_subway_1"] = 5;
$demo_content["realty_4"]["fe_furniture_1"] = 1;
$demo_content["realty_4"]["fe_accomodation_1_option_1"] = 1;
$demo_content["realty_4"]["fe_accomodation_1_option_8"] = 1;
$demo_content["realty_4"]["fe_accomodation_1_option_15"] = 1;
$demo_content["realty_4"]["fe_accomodation_1_option_19"] = 1;
$demo_content["realty_4"]["fe_accomodation_1_option_24"] = 1;
$demo_content["realty_4"]["fe_appliances_1_option_1"] = 1;
$demo_content["realty_4"]["fe_appliances_1_option_7"] = 1;
$demo_content["realty_4"]["fe_appliances_1_option_11"] = 1;
$demo_content["realty_4"]["fe_appliances_1_option_12"] = 1;
$demo_content["realty_4"]["fe_appliances_1_option_17"] = 1;
$demo_content["realty_4"]["fe_amenities_1_option_1"] = 1;
$demo_content["realty_4"]["fe_amenities_1_option_2"] = 1;
$demo_content["realty_4"]["fe_amenities_1_option_3"] = 1;
$demo_content["realty_4"]["fe_amenities_1_option_4"] = 1;
$demo_content["realty_4"]["fe_amenities_1_option_5"] = 1;
$demo_content["realty_4"]["fe_environment_1_option_1"] = 1;
$demo_content["realty_4"]["fe_environment_1_option_2"] = 1;
$demo_content["realty_4"]["fe_environment_1_option_3"] = 1;
$demo_content["realty_4"]["fe_environment_1_option_4"] = 1;
$demo_content["realty_4"]["fe_environment_1_option_5"] = 1;
$demo_content["realty_4"]["headline"] = "Prime Location, Newer Home With Gorgeous Layout";
$demo_content["realty_4"]["fe_comments_1"] = "Great location in Salmon creek built in 2000 on cul-de-sac in the Vancouver school district. This is a 4 bedroom 2 1/2 bath, 1980 square feet, great room concept, with formal dining and a large bonus room. The multi level deck leads out to a semi private backyard that is a tropical oasis in the summertime. It includes a nice size tool shed on the side of the house. This home has a large storage area underneath house and RV Parking on side. It has an over size 2 car garage with work bench. All appliances will stay, including Fridge ,washer and dryer. The house is built with plenty of space on each side of the house, 10 feet from fence where other houses in the neighborhood only have five";

$demo_content["realty_5"]["id"] = 5;
$demo_content["realty_5"]["id_user"] = "Pilot Group";
$demo_content["realty_5"]["id_type"] = "sale";
$demo_content["realty_5"]["id_category"] = "Commercial";
$demo_content["realty_5"]["property_type"] = "Office";
$demo_content["realty_5"]["status"] = 1;
$demo_content["realty_5"]["sold"] = 0;
$demo_content["realty_5"]["date_open"] = "2013-12-01 00:00:00";
$demo_content["realty_5"]["date_available"] = "2013-10-08 00:00:00";
$demo_content["realty_5"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_5"]["price"] = 1200000;
$demo_content["realty_5"]["price_reduced"] = 1100000;
$demo_content["realty_5"]["gid_currency"] = "USD";
$demo_content["realty_5"]["square"] = 3810;
$demo_content["realty_5"]["square_unit"] = "sqft";
$demo_content["realty_5"]["address"] = "Plaza del Conde de Miranda, 1";
$demo_content["realty_5"]["id_country"] = "Spain";
$demo_content["realty_5"]["id_region"] = "Comunidad de Madrid";
$demo_content["realty_5"]["id_city"] = "Madrid";
$demo_content["realty_5"]["zip"] = "28046";
$demo_content["realty_5"]["lat"] = "40.4147460";
$demo_content["realty_5"]["lon"] = "-3.7094264";
$demo_content["realty_5"]["logo_image"] = "07ddc13910.jpg";
$demo_content["realty_5"]["slider_image"] = "07ddc13910.jpg";
$demo_content["realty_5"]["id_wish_lists"] = array(1, 4);
$demo_content["realty_5"]["highlight_date_end"] = "2020-01-01 00:00";
$demo_content["realty_5"]["fe_construction_status_2"] = 2;
$demo_content["realty_5"]["fe_year_2"] = 2004;
$demo_content["realty_5"]["fe_foundation_2"] = 1;
$demo_content["realty_5"]["fe_revenue_2"] = 78;
$demo_content["realty_5"]["fe_cash_flow_2"] = 30000;
$demo_content["realty_5"]["fe_years_established_2"] = 8;
$demo_content["realty_5"]["fe_employees_2"] = 7;
$demo_content["realty_5"]["fe_reason_for_selling_2"] = "";
$demo_content["realty_5"]["headline"] = "Gorgeous 3 Bedroom Semi_detached Home In Prime Location Of Heartland Centre";
$demo_content["realty_5"]["fe_comments_2"] = "45 min. from Missoula, MT; 3 miles east of St. Ignatius, Mt in the beautiful and historic Mission Valley. Paved road, great wells, fenced, irrigated, great neighbors. Only 2 miles from Mission Mountain Wilderness. All sorts of both summer and winter outdoor recreational activities are available. Short distances to the National Bison Range, Glacier Park, the Bob Marshall Wilderness and Flathead Lake. No covenants.";

$demo_content["realty_6"]["id"] = 6;
$demo_content["realty_6"]["id_user"] = "Pilot Group";
$demo_content["realty_6"]["id_type"] = "sale";
$demo_content["realty_6"]["id_category"] = "Lots/lands";
$demo_content["realty_6"]["property_type"] = "Pasture";
$demo_content["realty_6"]["status"] = 1;
$demo_content["realty_6"]["sold"] = 0;
$demo_content["realty_6"]["date_open"] = "2013-05-01 00:00:00";
$demo_content["realty_6"]["date_available"] = "2013-09-02 00:00:00";
$demo_content["realty_6"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_6"]["price"] = 1000000;
$demo_content["realty_6"]["gid_currency"] = "USD";
$demo_content["realty_6"]["square"] = 2100;
$demo_content["realty_6"]["square_unit"] = "sqft";
$demo_content["realty_6"]["address"] = "112 Queen's Park Crescent West";
$demo_content["realty_6"]["id_country"] = "Canada";
$demo_content["realty_6"]["id_region"] = "Ontario";
$demo_content["realty_6"]["id_city"] = "Toronto";
$demo_content["realty_6"]["zip"] = "M5S 1A8";
$demo_content["realty_6"]["lat"] = "43.6658160";
$demo_content["realty_6"]["lon"] = "-79.3934345";
$demo_content["realty_6"]["logo_image"] = "25036c35d9.jpg";
$demo_content["realty_6"]["slider_image"] = "25036c35d9.jpg";
$demo_content["realty_6"]["listing_file"] = "ae6f3bf295.docx";
$demo_content["realty_6"]["listing_file_comment"] = "In architecture and building engineering, a floor plan otherwise known as a scottish plan is a drawing to scale, showing a view from above, of the relationships between rooms, spaces and other physical features at one level of a structure. Dimensions are usually drawn between the walls to specify room sizes and wall lengths. Floor plans may also include details of fixtures like sinks, water heaters, furnaces, etc. Floor plans may include notes for construction to specify finishes, construction methods, or symbols for electrical items.";
$demo_content["realty_6"]["listing_file_date"] = "2013-08-05 05:51:28";
$demo_content["realty_6"]["listing_video"] = "101_01c0be62.flv";
$demo_content["realty_6"]["listing_video_image"] = "101_01c0be62.jpg";
$demo_content["realty_6"]["listing_video_data"] = array("data"=>array("file_name"=>"101_01c0be62.avi","file_path"=>"/home/svinokurov/public_html/pg_realestate2/uploads/video/listing-video/1/6/","file_data"=>array("name"=>"Canada, Toronto, 112 Queen's Park Crescent West, M5S 1A8","description"=>"","ext"=>"avi","type"=>"video/x-msvideo"),"status"=>"start","id_object"=>"6","video_upload_gid"=>"listing-video","video"=>"101_01c0be62.flv","upload_type"=>"local", "image"=>"101_01c0be62.jpg"),"status"=>"end","errors"=>array());
$demo_content["realty_6"]["id_wish_lists"] = array(1);
$demo_content["realty_6"]["fe_irrigated_3"] = 1;
$demo_content["realty_6"]["fe_residence_3"] = 0;
$demo_content["realty_6"]["headline"] = "Beautiful Freehold Townhome End Unit Looks Like Semi, Biggest Lot, Very Spacious, Bright And Well Maintained. No Carpet Through Out. Open Concept Living/Dining Room.";
$demo_content["realty_6"]["fe_comments_3"] = "Electrical Light Fixtures, Window Coverings, Fridge, Stove, Washer, Dryer, B/I Dishwasher, Gas Fireplace, Fully Fenced Yard.";

$demo_content["realty_7"]["id"] = 7;
$demo_content["realty_7"]["id_user"] = "Pilot Group";
$demo_content["realty_7"]["id_type"] = "sale";
$demo_content["realty_7"]["id_category"] = "Residential";
$demo_content["realty_7"]["property_type"] = "Duplex";
$demo_content["realty_7"]["status"] = 1;
$demo_content["realty_7"]["sold"] = 0;
$demo_content["realty_7"]["date_open"] = "2013-07-01 00:00:00";
$demo_content["realty_7"]["date_available"] = "2013-04-29 00:00:00";
$demo_content["realty_7"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_7"]["price"] = 500000;
$demo_content["realty_7"]["gid_currency"] = "USD";
$demo_content["realty_7"]["square"] = 2600;
$demo_content["realty_7"]["square_unit"] = "sqft";
$demo_content["realty_7"]["address"] = "19 Novaya St., Reutov";
$demo_content["realty_7"]["id_country"] = "Russia";
$demo_content["realty_7"]["id_region"] = "Moscow";
$demo_content["realty_7"]["id_city"] = "Moscow";
$demo_content["realty_7"]["zip"] = "143966";
$demo_content["realty_7"]["lat"] = "55.7591220";
$demo_content["realty_7"]["lon"] = "37.8595060";
$demo_content["realty_7"]["logo_image"] = "e46c99a2e9.jpg";
$demo_content["realty_7"]["slider_image"] = "e46c99a2e9.jpg";
$demo_content["realty_7"]["id_wish_lists"] = array(1, 3);
$demo_content["realty_7"]["featured_date_end"] = "2020-01-01 00:00";
$demo_content["realty_7"]["slide_show_date_end"] = "2020-01-01 00:00";
$demo_content["realty_7"]["fe_construction_status_1"] = 1;
$demo_content["realty_7"]["fe_live_square_1"] = 2400;
$demo_content["realty_7"]["fe_bd_rooms_1"] = 3;
$demo_content["realty_7"]["fe_bth_rooms_1"] = 1;
$demo_content["realty_7"]["fe_garages_1"] = 2;
$demo_content["realty_7"]["fe_year_1"] = 1997;
$demo_content["realty_7"]["fe_foundation_1"] = 3;
$demo_content["realty_7"]["fe_floor_number_1"] = 1;
$demo_content["realty_7"]["fe_total_floors_1"] = 1;
$demo_content["realty_7"]["fe_distance_to_subway_1"] = 15;
$demo_content["realty_7"]["fe_furniture_1"] = 1;
$demo_content["realty_7"]["fe_accomodation_1_option_1"] = 1;
$demo_content["realty_7"]["fe_accomodation_1_option_5"] = 1;
$demo_content["realty_7"]["fe_accomodation_1_option_6"] = 1;
$demo_content["realty_7"]["fe_accomodation_1_option_8"] = 1;
$demo_content["realty_7"]["fe_accomodation_1_option_11"] = 1;
$demo_content["realty_7"]["fe_appliances_1_option_1"] = 1;
$demo_content["realty_7"]["fe_appliances_1_option_4"] = 1;
$demo_content["realty_7"]["fe_appliances_1_option_5"] = 1;
$demo_content["realty_7"]["fe_appliances_1_option_12"] = 1;
$demo_content["realty_7"]["fe_appliances_1_option_14"] = 1;
$demo_content["realty_7"]["fe_amenities_1_option_1"] = 1;
$demo_content["realty_7"]["fe_amenities_1_option_8"] = 1;
$demo_content["realty_7"]["fe_amenities_1_option_9"] = 1;
$demo_content["realty_7"]["fe_amenities_1_option_11"] = 1;
$demo_content["realty_7"]["fe_amenities_1_option_14"] = 1;
$demo_content["realty_7"]["fe_environment_1_option_5"] = 1;
$demo_content["realty_7"]["fe_environment_1_option_7"] = 1;
$demo_content["realty_7"]["fe_environment_1_option_11"] = 1;
$demo_content["realty_7"]["fe_environment_1_option_14"] = 1;
$demo_content["realty_7"]["fe_environment_1_option_15"] = 1;
$demo_content["realty_7"]["headline"] = "Beautiful Freehold Townhome End Unit Looks Like Semi, Biggest Lot, Very Spacious, Bright And Well Maintained. No Carpet Through Out. Open Concept Living/Dining Room. Family Size Kitchen With Breakfast Area, New Kitchen With S/S Appliances Under Warranty, B/I Microwave. Marble & Granite Backspalsh.";
$demo_content["realty_7"]["fe_comments_1"] = "Freshly Painted. Sep Entrance To Basement Apartment. New Roof (2011), New Windows In The Main Level, New Front Door (2011). Nice Interlocking Brick Patio. Perfect For 1st Time Home Buyers,Someone Looking To Downsize Or An Investor For A Good Rental Property. Quiet Street. Appliances Incl: S/S Fridge, S/S Stove,S/S Dish Washer,S/S B/I Microwave, Washer & Dryer,Gdo W/Remote,Elf,Draperies";

$demo_content["realty_8"]["id"] = 8;
$demo_content["realty_8"]["id_user"] = "Pilot Group";
$demo_content["realty_8"]["id_type"] = "sale";
$demo_content["realty_8"]["id_category"] = "Commercial";
$demo_content["realty_8"]["property_type"] = "Shop";
$demo_content["realty_8"]["status"] = 1;
$demo_content["realty_8"]["sold"] = 0;
$demo_content["realty_8"]["date_open"] = "2013-03-21 00:00:00";
$demo_content["realty_8"]["date_available"] = "2014-02-04 00:00:00";
$demo_content["realty_8"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_8"]["price"] = 215000;
$demo_content["realty_8"]["price_reduced"] = 214999;
$demo_content["realty_8"]["gid_currency"] = "USD";
$demo_content["realty_8"]["square"] = 2350;
$demo_content["realty_8"]["square_unit"] = "sqft";
$demo_content["realty_8"]["address"] = "423 E. 14th St";
$demo_content["realty_8"]["id_country"] = "United States";
$demo_content["realty_8"]["id_region"] = "New York";
$demo_content["realty_8"]["id_city"] = "New York City";
$demo_content["realty_8"]["zip"] = "10007";
$demo_content["realty_8"]["lat"] = "40.7305217";
$demo_content["realty_8"]["lon"] = "-73.9806163";
$demo_content["realty_8"]["logo_image"] = "9d371eeb80.jpg";
$demo_content["realty_8"]["slider_image"] = "9d371eeb80.jpg";
$demo_content["realty_8"]["id_wish_lists"] = array(1, 2);
$demo_content["realty_8"]["featured_date_end"] = '2020-01-01 00:00:00';
$demo_content["realty_8"]["lift_up_date_end"] = "2020-01-01 00:00"; 
$demo_content["realty_8"]["fe_construction_status_2"] = 2;
$demo_content["realty_8"]["fe_year_2"] = 1988;
$demo_content["realty_8"]["fe_foundation_2"] = 2;
$demo_content["realty_8"]["fe_revenue_2"] = 53;
$demo_content["realty_8"]["fe_cash_flow_2"] = 230000;
$demo_content["realty_8"]["fe_years_established_2"] = 12;
$demo_content["realty_8"]["fe_employees_2"] = 50;
$demo_content["realty_8"]["fe_reason_for_selling_2"] = "";
$demo_content["realty_8"]["headline"] = "Beautifully Renovated 2+1 Br/2 Bath Home On Wonderful Quiet Street On 50X110 Ft Lot. Kit With Granite,Brkfst Bar,Glass Cabinets & Walk-Thru To Din Rm. Open Living/Dining With Crown Moulding.";
$demo_content["realty_8"]["fe_comments_2"] = "THIS ONE HAS IT ALL!!! Sitting high above the Town of Burnsville at approx. 3200 feet elevation this quality home offers convenient living + PANORAMIC MOUNTAIN VIEW! Kitchen with cabinets, cabinets, cabinets and walk-in pantry. Den features a stone veneer fireplace with gas logs. Formal entry hall and living room. Master Bedroom Suite with view and private access to porch/deck. Full basement has 3rd bedroom and bath. Laundry facility on each level. Paved circular drive & parking. Beautifully landscaped with small babbling brooke along property boundary. MOVE-IN READY!";

$demo_content["realty_9"]["id"] = 9;
$demo_content["realty_9"]["id_user"] = "Lucy Price";
$demo_content["realty_9"]["id_type"] = "sale";
$demo_content["realty_9"]["id_category"] = "Lots/lands";
$demo_content["realty_9"]["property_type"] = "Timberland";
$demo_content["realty_9"]["status"] = 1;
$demo_content["realty_9"]["sold"] = 0;
$demo_content["realty_9"]["date_open"] = "2020-01-01 00:00:00";
$demo_content["realty_9"]["date_available"] = "2013-09-11 00:00:00";
$demo_content["realty_9"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_9"]["price"] = 314900;
$demo_content["realty_9"]["gid_currency"] = "USD";
$demo_content["realty_9"]["square"] = 2100;
$demo_content["realty_9"]["square_unit"] = "sqft";
$demo_content["realty_9"]["address"] = "7 Percy Street";
$demo_content["realty_9"]["id_country"] = "United Kingdom";
$demo_content["realty_9"]["id_region"] = "Greater London";
$demo_content["realty_9"]["id_city"] = "London";
$demo_content["realty_9"]["zip"] = "EC1R 3HN";
$demo_content["realty_9"]["lat"] = "51.5181303";
$demo_content["realty_9"]["lon"] = "-0.1337332";
$demo_content["realty_9"]["logo_image"] = "74cd9fae66.jpg";
$demo_content["realty_9"]["slider_image"] = "74cd9fae66.jpg";
$demo_content["realty_9"]["listing_file"] = "ae6f3bf295.docx";
$demo_content["realty_9"]["listing_file_comment"] = "In architecture and building engineering, a floor plan otherwise known as a scottish plan is a drawing to scale, showing a view from above, of the relationships between rooms, spaces and other physical features at one level of a structure. Dimensions are usually drawn between the walls to specify room sizes and wall lengths. Floor plans may also include details of fixtures like sinks, water heaters, furnaces, etc. Floor plans may include notes for construction to specify finishes, construction methods, or symbols for electrical items.";
$demo_content["realty_9"]["listing_file_date"] = "2013-08-05 05:51:28";
$demo_content["realty_9"]["listing_video"] = "101_01c0be62.flv";
$demo_content["realty_9"]["listing_video_image"] = "101_01c0be62.jpg";
$demo_content["realty_9"]["listing_video_data"] = array("data"=>array("file_name"=>"101_01c0be62.avi","file_path"=>"/home/svinokurov/public_html/pg_realestate2/uploads/video/listing-video/21/9/","file_data"=>array("name"=>"United Kingdom, London, 7 Percy Street, EC1R 3HN","description"=>"","ext"=>"avi","type"=>"video/x-msvideo"),"status"=>"start","id_object"=>"9","video_upload_gid"=>"listing-video","video"=>"101_01c0be62.flv","upload_type"=>"local", "image"=>"101_01c0be62.jpg"),"status"=>"end","errors"=>array());
$demo_content["realty_9"]["id_wish_lists"] = array(1);
$demo_content["realty_9"]["fe_irrigated_3"] = 0;
$demo_content["realty_9"]["fe_residence_3"] = 1;
$demo_content["realty_9"]["headline"] = "Exclusive Churchill Meadows Area! Beautiful Open Concept,Semi. Main Floor Family Room With Gas Fireplace.";
$demo_content["realty_9"]["fe_comments_3"] = "Cheap and luxury holiday house!";

$demo_content["realty_10"]["id"] = 10;
$demo_content["realty_10"]["id_user"] = "Kevin Johnson";
$demo_content["realty_10"]["id_type"] = "sale";
$demo_content["realty_10"]["id_category"] = "Residential";
$demo_content["realty_10"]["property_type"] = "Single Family Home";
$demo_content["realty_10"]["status"] = 1;
$demo_content["realty_10"]["sold"] = 0;
$demo_content["realty_10"]["date_open"] = "2013-02-22 00:00:00";
$demo_content["realty_10"]["date_available"] = "2013-11-17 00:00:00";
$demo_content["realty_10"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_10"]["price"] = 295000;
$demo_content["realty_10"]["gid_currency"] = "USD";
$demo_content["realty_10"]["square"] = 1900;
$demo_content["realty_10"]["square_unit"] = "sqft";
$demo_content["realty_10"]["address"] = "5 avenue Anatole France";
$demo_content["realty_10"]["id_country"] = "France";
$demo_content["realty_10"]["id_region"] = "Région Île-de-France";
$demo_content["realty_10"]["id_city"] = "Paris";
$demo_content["realty_10"]["zip"] = "75007";
$demo_content["realty_10"]["lat"] = "48.8597682";
$demo_content["realty_10"]["lon"] = "2.3280792";
$demo_content["realty_10"]["logo_image"] = "ed57bb38f8.jpg";
$demo_content["realty_10"]["slider_image"] = "ed57bb38f8.jpg";
$demo_content["realty_10"]["id_wish_lists"] = array(1, 4);
$demo_content["realty_10"]["featured_date_end"] = '2020-01-01 00:00:00'; 
$demo_content["realty_10"]["fe_construction_status_1"] = 2;
$demo_content["realty_10"]["fe_live_square_1"] = 1800;
$demo_content["realty_10"]["fe_bd_rooms_1"] = 2;
$demo_content["realty_10"]["fe_bth_rooms_1"] = 1;
$demo_content["realty_10"]["fe_garages_1"] = 1;
$demo_content["realty_10"]["fe_year_1"] = 2002;
$demo_content["realty_10"]["fe_foundation_1"] = 2;
$demo_content["realty_10"]["fe_floor_number_1"] = 12;
$demo_content["realty_10"]["fe_total_floors_1"] = 24;
$demo_content["realty_10"]["fe_distance_to_subway_1"] = 5;
$demo_content["realty_10"]["fe_furniture_1"] = 2;
$demo_content["realty_10"]["fe_accomodation_1_option_4"] = 1;
$demo_content["realty_10"]["fe_accomodation_1_option_5"] = 1;
$demo_content["realty_10"]["fe_accomodation_1_option_6"] = 1;
$demo_content["realty_10"]["fe_accomodation_1_option_7"] = 1;
$demo_content["realty_10"]["fe_accomodation_1_option_8"] = 1;
$demo_content["realty_10"]["fe_appliances_1_option_2"] = 1;
$demo_content["realty_10"]["fe_appliances_1_option_8"] = 1;
$demo_content["realty_10"]["fe_appliances_1_option_12"] = 1;
$demo_content["realty_10"]["fe_appliances_1_option_14"] = 1;
$demo_content["realty_10"]["fe_appliances_1_option_16"] = 1;
$demo_content["realty_10"]["fe_amenities_1_option_1"] = 1;
$demo_content["realty_10"]["fe_amenities_1_option_3"] = 1;
$demo_content["realty_10"]["fe_amenities_1_option_5"] = 1;
$demo_content["realty_10"]["fe_amenities_1_option_7"] = 1;
$demo_content["realty_10"]["fe_amenities_1_option_9"] = 1;
$demo_content["realty_10"]["fe_environment_1_option_1"] = 1;
$demo_content["realty_10"]["fe_environment_1_option_2"] = 1;
$demo_content["realty_10"]["fe_environment_1_option_3"] = 1;
$demo_content["realty_10"]["fe_environment_1_option_4"] = 1;
$demo_content["realty_10"]["fe_environment_1_option_12"] = 1;
$demo_content["realty_10"]["headline"] = "Nestled On A 100X200 Lot On One Of The Most Prestigious Streets In Mineola West! 4 Bedrm.";
$demo_content["realty_10"]["fe_comments_1"] = "Beautifully updated and remodeled thru out. Smooth ceilings, new paint and carpet, tile floors.";

$demo_content["realty_11"]["id"] = 11;
$demo_content["realty_11"]["id_user"] = "Marina Group Ltd";
$demo_content["realty_11"]["id_type"] = "sale";
$demo_content["realty_11"]["id_category"] = "Commercial";
$demo_content["realty_11"]["property_type"] = "Restaurant";
$demo_content["realty_11"]["status"] = 1;
$demo_content["realty_11"]["sold"] = 0;
$demo_content["realty_11"]["date_open"] = "2013-11-01 00:00:00";
$demo_content["realty_11"]["date_available"] = "2015-03-22 00:00:00";
$demo_content["realty_11"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_11"]["price"] = 300000;
$demo_content["realty_11"]["price_reduced"] = 290000;
$demo_content["realty_11"]["gid_currency"] = "USD";
$demo_content["realty_11"]["square"] = 2200;
$demo_content["realty_11"]["square_unit"] = "sqft";
$demo_content["realty_11"]["address"] = "Prinzenstr. 85f";
$demo_content["realty_11"]["id_country"] = "Germany";
$demo_content["realty_11"]["id_region"] = "Land Berlin";
$demo_content["realty_11"]["id_city"] = "Berlin";
$demo_content["realty_11"]["zip"] = "10969";
$demo_content["realty_11"]["lat"] = "52.5191710";
$demo_content["realty_11"]["lon"] = "13.4060912";
$demo_content["realty_11"]["logo_image"] = "aa067c5a3b.jpg";
$demo_content["realty_11"]["slider_image"] = "aa067c5a3b.jpg";
$demo_content["realty_11"]["listing_file"] = "ae6f3bf295.docx";
$demo_content["realty_11"]["listing_file_comment"] = "In architecture and building engineering, a floor plan otherwise known as a scottish plan is a drawing to scale, showing a view from above, of the relationships between rooms, spaces and other physical features at one level of a structure. Dimensions are usually drawn between the walls to specify room sizes and wall lengths. Floor plans may also include details of fixtures like sinks, water heaters, furnaces, etc. Floor plans may include notes for construction to specify finishes, construction methods, or symbols for electrical items.";
$demo_content["realty_11"]["listing_file_date"] = "2013-08-05 05:51:28";
$demo_content["realty_11"]["listing_video"] = "101_01c0be62.flv";
$demo_content["realty_11"]["listing_video_image"] = "101_01c0be62.jpg";
$demo_content["realty_11"]["listing_video_data"] = array("data"=>array("file_name"=>"101_01c0be62.avi","file_path"=>"/home/svinokurov/public_html/pg_realestate2/uploads/video/listing-video/2/11/","file_data"=>array("name"=>"Germany, Berlin, Prinzenstr. 85f, 10969","description"=>"","ext"=>"avi","type"=>"video/x-msvideo"),"status"=>"start","id_object"=>"11","video_upload_gid"=>"listing-video","video"=>"101_01c0be62.flv","upload_type"=>"local", "image"=>"101_01c0be62.jpg"),"status"=>"end","errors"=>array());
$demo_content["realty_11"]["id_wish_lists"] = array(1);
$demo_content["realty_11"]["highlight_date_end"] = "2020-01-01 00:00";
$demo_content["realty_11"]["fe_construction_status_2"] = 1;
$demo_content["realty_11"]["fe_year_2"] = 1991;
$demo_content["realty_11"]["fe_foundation_2"] = 5;
$demo_content["realty_11"]["fe_revenue_2"] = 85;
$demo_content["realty_11"]["fe_cash_flow_2"] = 50000;
$demo_content["realty_11"]["fe_years_established_2"] = 7;
$demo_content["realty_11"]["fe_employees_2"] = 12;
$demo_content["realty_11"]["fe_reason_for_selling_2"] = "";
$demo_content["realty_11"]["headline"] = "Cachet Lanes, Prince Manor Model. Elegant New Flagstone Walkway/Steps & Prof. Lndscpd Gdns. Huge Wndows In Main Rms Invite Sunshine & Views. Lrge Formal Dng Rm. Kitchen + Bkfst Areas O/L Sunken Lvg Rm With Soaring 11'8' Ceiling & Gas F/P. W/O To Charming Private Prof.";
$demo_content["realty_11"]["fe_comments_2"] = "Circular drive leads to this beautiful home. Stone floor entry opens to elegant living room.";

$demo_content["realty_12"]["id"] = 12;
$demo_content["realty_12"]["id_user"] = "Michael Kreick";
$demo_content["realty_12"]["id_type"] = "sale";
$demo_content["realty_12"]["id_category"] = "Lots/lands";
$demo_content["realty_12"]["property_type"] = "Undeveloped land";
$demo_content["realty_12"]["status"] = 1;
$demo_content["realty_12"]["sold"] = 0;
$demo_content["realty_12"]["date_open"] = "2013-03-07 00:00:00";
$demo_content["realty_12"]["date_available"] = "2014-10-04 00:00:00";
$demo_content["realty_12"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_12"]["price"] = 769000;
$demo_content["realty_12"]["gid_currency"] = "USD";
$demo_content["realty_12"]["square"] = 1900;
$demo_content["realty_12"]["square_unit"] = "sqft";
$demo_content["realty_12"]["address"] = "Cl Dublin, 7";
$demo_content["realty_12"]["id_country"] = "Spain";
$demo_content["realty_12"]["id_region"] = "Comunidad de Madrid";
$demo_content["realty_12"]["id_city"] = "Madrid";
$demo_content["realty_12"]["zip"] = "10010";
$demo_content["realty_12"]["lat"] = "40.4260105";
$demo_content["realty_12"]["lon"] = "-3.5229588";
$demo_content["realty_12"]["logo_image"] = "56225c3bac.jpg";
$demo_content["realty_12"]["slider_image"] = "56225c3bac.jpg";
$demo_content["realty_12"]["id_wish_lists"] = array(1, 2);
$demo_content["realty_12"]["fe_irrigated_3"] = 0;
$demo_content["realty_12"]["fe_residence_3"] = 0;
$demo_content["realty_12"]["headline"] = "Spacious Beautifully Maintained 4 Bedroom Detached W/Double Car Garage On A Premium 50 Ft Lot. Huge Master Bedroom With His/Hers Closets.";
$demo_content["realty_12"]["fe_comments_3"] = "House very bright with lots of windows. Private entrance. Wall to wall carpet. High hats, and ceiling fans. Electric (range and dryer). Curtains and Valances by windows. Access to the attic with lots of storage. Public transportation. Base board heating. Tenant is responsible for Electric bill, Phone, and Cable. Credit & Reference Checks Required.";

$demo_content["realty_13"]["id"] = 13;
$demo_content["realty_13"]["id_user"] = "Michael Kreick";
$demo_content["realty_13"]["id_type"] = "sale";
$demo_content["realty_13"]["id_category"] = "Residential";
$demo_content["realty_13"]["property_type"] = "Multi Family Home";
$demo_content["realty_13"]["status"] = 1;
$demo_content["realty_13"]["sold"] = 0;
$demo_content["realty_13"]["date_open"] = "2013-05-05 00:00:00";
$demo_content["realty_13"]["date_available"] = "2013-02-08 00:00:00";
$demo_content["realty_13"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_13"]["price"] = 10000;
$demo_content["realty_13"]["gid_currency"] = "USD";
$demo_content["realty_13"]["square"] = 100;
$demo_content["realty_13"]["square_unit"] = "sqft";
$demo_content["realty_13"]["address"] = "446 Spadina Road #106";
$demo_content["realty_13"]["id_country"] = "Canada";
$demo_content["realty_13"]["id_region"] = "Ontario";
$demo_content["realty_13"]["id_city"] = "Toronto";
$demo_content["realty_13"]["zip"] = "M5P 3M2";
$demo_content["realty_13"]["lat"] = "43.6889655";
$demo_content["realty_13"]["lon"] = "-79.4128275";
$demo_content["realty_13"]["logo_image"] = "895f0a22bf.jpg";
$demo_content["realty_13"]["slider_image"] = "895f0a22bf.jpg";
$demo_content["realty_13"]["id_wish_lists"] = array(1);
$demo_content["realty_13"]["featured_date_end"] = "2020-01-01 00:00";
$demo_content["realty_13"]["featured_date_end"] = '2020-01-01 00:00:00'; 
$demo_content["realty_13"]["fe_construction_status_1"] = 1;
$demo_content["realty_13"]["fe_live_square_1"] = 70;
$demo_content["realty_13"]["fe_bd_rooms_1"] = 3;
$demo_content["realty_13"]["fe_bth_rooms_1"] = 1;
$demo_content["realty_13"]["fe_garages_1"] = 0;
$demo_content["realty_13"]["fe_year_1"] = 2009;
$demo_content["realty_13"]["fe_foundation_1"] = 4;
$demo_content["realty_13"]["fe_floor_number_1"] = 3;
$demo_content["realty_13"]["fe_total_floors_1"] = 9;
$demo_content["realty_13"]["fe_distance_to_subway_1"] = 0;
$demo_content["realty_13"]["fe_furniture_1"] = 1;
$demo_content["realty_13"]["fe_accomodation_1_option_1"] = 1;
$demo_content["realty_13"]["fe_accomodation_1_option_2"] = 1;
$demo_content["realty_13"]["fe_accomodation_1_option_3"] = 1;
$demo_content["realty_13"]["fe_accomodation_1_option_5"] = 1;
$demo_content["realty_13"]["fe_accomodation_1_option_6"] = 1;
$demo_content["realty_13"]["fe_appliances_1_option_1"] = 1;
$demo_content["realty_13"]["fe_appliances_1_option_2"] = 1;
$demo_content["realty_13"]["fe_appliances_1_option_3"] = 1;
$demo_content["realty_13"]["fe_appliances_1_option_5"] = 1;
$demo_content["realty_13"]["fe_appliances_1_option_6"] = 1;
$demo_content["realty_13"]["fe_amenities_1_option_1"] = 1;
$demo_content["realty_13"]["fe_amenities_1_option_2"] = 1;
$demo_content["realty_13"]["fe_amenities_1_option_3"] = 1;
$demo_content["realty_13"]["fe_amenities_1_option_4"] = 1;
$demo_content["realty_13"]["fe_amenities_1_option_5"] = 1;
$demo_content["realty_13"]["fe_environment_1_option_1"] = 1;
$demo_content["realty_13"]["fe_environment_1_option_2"] = 1;
$demo_content["realty_13"]["fe_environment_1_option_3"] = 1;
$demo_content["realty_13"]["fe_environment_1_option_4"] = 1;
$demo_content["realty_13"]["fe_environment_1_option_5"] = 1;
$demo_content["realty_13"]["headline"] = "Sensational Updated 4-Level Sidesplit On A Cul-De-Sac Rattray Marsh Conservation Area Within Easy Walk To Great Schools, Lake Ontario & Clarkson Village.";
$demo_content["realty_13"]["fe_comments_1"] = "1800 Sq. feet Two Story Home w/ Professionally Selected Colors for Interior and Exterior. Master Bedroom with Bridge to Private Veranda/Balcony Overlooking Park, Walk-in Closet, and Master Bath w/ Jacuzzi Bathtub and Walk-in Shower. Living Rm, Dining Rm, and Family Rm w/ Fireplace. Kitchen w/ Breakfast Nook. All Kitchen and Laundry Appliances Included w/ Laundry Upstairs. Vaulted Ceilings and Hardwood Floors in Entire First Floor. Great Park Across the Street, Close to Downtown HB and Freeways";

$demo_content["realty_14"]["id"] = 14;
$demo_content["realty_14"]["id_user"] = "Randy Ingraham";
$demo_content["realty_14"]["id_type"] = "sale";
$demo_content["realty_14"]["id_category"] = "Commercial";
$demo_content["realty_14"]["property_type"] = "Hotel";
$demo_content["realty_14"]["status"] = 1;
$demo_content["realty_14"]["sold"] = 0;
$demo_content["realty_14"]["date_open"] = "2020-01-01 00:00:00";
$demo_content["realty_14"]["date_available"] = "2013-08-17 00:00:00";
$demo_content["realty_14"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_14"]["price"] = 209700;
$demo_content["realty_14"]["gid_currency"] = "USD";
$demo_content["realty_14"]["square"] = 1600;
$demo_content["realty_14"]["square_unit"] = "sqft";
$demo_content["realty_14"]["address"] = "Prechistenskaya nab., 9";
$demo_content["realty_14"]["id_country"] = "Russia";
$demo_content["realty_14"]["id_region"] = "Moscow";
$demo_content["realty_14"]["id_city"] = "Moscow";
$demo_content["realty_14"]["zip"] = "119034";
$demo_content["realty_14"]["lat"] = "55.7372740";
$demo_content["realty_14"]["lon"] = "37.6023540";
$demo_content["realty_14"]["logo_image"] = "a30a72ac25.jpg";
$demo_content["realty_14"]["slider_image"] = "a30a72ac25.jpg";
$demo_content["realty_14"]["listing_file"] = "ae6f3bf295.docx";
$demo_content["realty_14"]["listing_file_comment"] = "In architecture and building engineering, a floor plan otherwise known as a scottish plan is a drawing to scale, showing a view from above, of the relationships between rooms, spaces and other physical features at one level of a structure. Dimensions are usually drawn between the walls to specify room sizes and wall lengths. Floor plans may also include details of fixtures like sinks, water heaters, furnaces, etc. Floor plans may include notes for construction to specify finishes, construction methods, or symbols for electrical items.";
$demo_content["realty_14"]["listing_file_date"] = "2013-08-05 05:51:28";
$demo_content["realty_14"]["listing_video"] = "101_01c0be62.flv";
$demo_content["realty_14"]["listing_video_image"] = "101_01c0be62.jpg";
$demo_content["realty_14"]["listing_video_data"] = array("data"=>array("file_name"=>"101_01c0be62.avi","file_path"=>"/home/svinokurov/public_html/pg_realestate2/uploads/video/listing-video/6/14/","file_data"=>array("name"=>"Russia, Moscow, Prechistenskaya nab., 9, 119034","description"=>"","ext"=>"avi","type"=>"video/x-msvideo"),"status"=>"start","id_object"=>"14","video_upload_gid"=>"listing-video","video"=>"101_01c0be62.flv","upload_type"=>"local", "image"=>"101_01c0be62.jpg"),"status"=>"end","errors"=>array());
$demo_content["realty_14"]["id_wish_lists"] = array(1, 3);
$demo_content["realty_14"]["featured_date_end"] = '2020-01-01 00:00:00';
$demo_content["realty_14"]["fe_construction_status_2"] = 1;
$demo_content["realty_14"]["fe_year_2"] = 2002;
$demo_content["realty_14"]["fe_foundation_2"] = 2;
$demo_content["realty_14"]["fe_revenue_2"] = 45;
$demo_content["realty_14"]["fe_cash_flow_2"] = 45000;
$demo_content["realty_14"]["fe_years_established_2"] = 15;
$demo_content["realty_14"]["fe_employees_2"] = 6;
$demo_content["realty_14"]["fe_reason_for_selling_2"] = ""; 
$demo_content["realty_14"]["headline"] = "Wonderful Cozy Family Home On A Beautifully Landscaped Lot Featuring A Garden Pond, Flagstone Patio And A Siziable Outdoor Shed.";
$demo_content["realty_14"]["fe_comments_2"] = "Walk to downtown Palatine and train station, great location. large unit with 2 bedroom 2 full and 2 1/2 baths. Open and airy floor plan. This home has a double master suite with 2nd floor laundry, washer/dryer included. First floor boost hardwood floors, kitchen island, maple cabinets and all appliances are included. Lower level basement is finished with den and 1/2 bath. This unit has never been lived in.";

$demo_content["realty_15"]["id"] = 15;
$demo_content["realty_15"]["id_user"] = "The Grey Rider";
$demo_content["realty_15"]["id_type"] = "sale";
$demo_content["realty_15"]["id_category"] = "Lots/lands";
$demo_content["realty_15"]["property_type"] = "Other";
$demo_content["realty_15"]["status"] = 1;
$demo_content["realty_15"]["sold"] = 0;
$demo_content["realty_15"]["date_open"] = "2012-11-11 00:00:00";
$demo_content["realty_15"]["date_available"] = "2013-09-01 00:00:00";
$demo_content["realty_15"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_15"]["price"] = 17000;
$demo_content["realty_15"]["price_reduced"] = 15500;
$demo_content["realty_15"]["gid_currency"] = "USD";
$demo_content["realty_15"]["square"] = 500;
$demo_content["realty_15"]["square_unit"] = "sqft";
$demo_content["realty_15"]["address"] = "350 Canal St";
$demo_content["realty_15"]["id_country"] = "United States";
$demo_content["realty_15"]["id_region"] = "New York";
$demo_content["realty_15"]["id_city"] = "New York City";
$demo_content["realty_15"]["zip"] = "10013";
$demo_content["realty_15"]["lat"] = "40.7203167";
$demo_content["realty_15"]["lon"] = "-74.0033929";
$demo_content["realty_15"]["logo_image"] = "9844f48b38.jpg";
$demo_content["realty_15"]["slider_image"] = "9844f48b38.jpg";
$demo_content["realty_15"]["id_wish_lists"] = array(1, 4);
$demo_content["realty_15"]["slide_show_date_end"] = "2020-01-01 00:00";
$demo_content["realty_15"]["fe_irrigated_3"] = 1;
$demo_content["realty_15"]["fe_residence_3"] = 0;
$demo_content["realty_15"]["headline"] = "Absolute One Of A Kind, Muskoka-In-The-City,Spectacular 100X323'Treed Ravine Lot With Private Stream. Best Location In Mineola West.";
$demo_content["realty_15"]["fe_comments_3"] = "Custom Gazebo, Walk Out Basement To Private Patio. Exceptional Schools. Walk To Go. New Roof";

$demo_content["realty_16"]["id"] = 16;
$demo_content["realty_16"]["id_user"] = "The Grey Rider";
$demo_content["realty_16"]["id_type"] = "sale";
$demo_content["realty_16"]["id_category"] = "Residential";
$demo_content["realty_16"]["property_type"] = "Condo";
$demo_content["realty_16"]["status"] = 1;
$demo_content["realty_16"]["sold"] = 0;
$demo_content["realty_16"]["date_open"] = "2013-02-11 00:00:00";
$demo_content["realty_16"]["date_available"] = "2013-08-08 00:00:00";
$demo_content["realty_16"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_16"]["price"] = 125000;
$demo_content["realty_16"]["gid_currency"] = "USD";
$demo_content["realty_16"]["square"] = 1500;
$demo_content["realty_16"]["square_unit"] = "sqft";
$demo_content["realty_16"]["address"] = "14 Pier Walk";
$demo_content["realty_16"]["id_country"] = "United Kingdom";
$demo_content["realty_16"]["id_region"] = "Greater London";
$demo_content["realty_16"]["id_city"] = "London";
$demo_content["realty_16"]["zip"] = "SE10 0ES";
$demo_content["realty_16"]["lat"] = "51.5010826";
$demo_content["realty_16"]["lon"] = "0.0061959";
$demo_content["realty_16"]["logo_image"] = "0845ce641c.jpg";
$demo_content["realty_16"]["slider_image"] = "0845ce641c.jpg";
$demo_content["realty_16"]["id_wish_lists"] = array(1, 2);
$demo_content["realty_16"]["fe_construction_status_1"] = 2;
$demo_content["realty_16"]["fe_live_square_1"] = 1350;
$demo_content["realty_16"]["fe_bd_rooms_1"] = 2;
$demo_content["realty_16"]["fe_bth_rooms_1"] = 1;
$demo_content["realty_16"]["fe_garages_1"] = 0;
$demo_content["realty_16"]["fe_year_1"] = 1998;
$demo_content["realty_16"]["fe_foundation_1"] = 2;
$demo_content["realty_16"]["fe_floor_number_1"] = 3;
$demo_content["realty_16"]["fe_total_floors_1"] = 5;
$demo_content["realty_16"]["fe_distance_to_subway_1"] = 0;
$demo_content["realty_16"]["fe_furniture_1"] = 1;
$demo_content["realty_16"]["fe_accomodation_1_option_1"] = 1;
$demo_content["realty_16"]["fe_accomodation_1_option_2"] = 1;
$demo_content["realty_16"]["fe_accomodation_1_option_3"] = 1;
$demo_content["realty_16"]["fe_accomodation_1_option_4"] = 1;
$demo_content["realty_16"]["fe_accomodation_1_option_5"] = 1;
$demo_content["realty_16"]["fe_accomodation_1_option_6"] = 1;
$demo_content["realty_16"]["fe_accomodation_1_option_7"] = 1;
$demo_content["realty_16"]["fe_accomodation_1_option_8"] = 1;
$demo_content["realty_16"]["fe_accomodation_1_option_9"] = 1;
$demo_content["realty_16"]["fe_accomodation_1_option_10"] = 1;
$demo_content["realty_16"]["fe_appliances_1_option_1"] = 1;
$demo_content["realty_16"]["fe_appliances_1_option_3"] = 1;
$demo_content["realty_16"]["fe_appliances_1_option_8"] = 1;
$demo_content["realty_16"]["fe_appliances_1_option_15"] = 1;
$demo_content["realty_16"]["fe_appliances_1_option_19"] = 1;
$demo_content["realty_16"]["fe_amenities_1_option_4"] = 1;
$demo_content["realty_16"]["fe_amenities_1_option_6"] = 1;
$demo_content["realty_16"]["fe_amenities_1_option_9"] = 1;
$demo_content["realty_16"]["fe_amenities_1_option_12"] = 1;
$demo_content["realty_16"]["fe_amenities_1_option_15"] = 1;
$demo_content["realty_16"]["fe_environment_1_option_10"] = 1;
$demo_content["realty_16"]["fe_environment_1_option_11"] = 1;
$demo_content["realty_16"]["fe_environment_1_option_12"] = 1;
$demo_content["realty_16"]["fe_environment_1_option_13"] = 1;
$demo_content["realty_16"]["fe_environment_1_option_14"] = 1;
$demo_content["realty_16"]["headline"] = "One Of The Nicest Homes In Mississauga In This Price Range. Don't Miss Out! Excitement Is The Only Word To Describe How You Will Feel Coming Home To This 6500+Sqft Showpiece.";
$demo_content["realty_16"]["fe_comments_1"] = "Situated in the Gallery Row District of downtown Los Angeles, Santa Fe Lofts offers live-work spaces for rent in two historic buildings. Each loft boasts classic downtown views from oversized windows; other timeless features could include original hexagonal plaster columns, transoms and doors with new wire glass, skylights and lightwells, mosaic tile and sealed or polished distressed concrete floors. With kitchens and baths designed for modern comfort with style, spaces blending vintage details with updated amenities for modern life and work, and a rooftop sun deck.";

$demo_content["realty_17"]["id"] = 17;
$demo_content["realty_17"]["id_user"] = "The Grey Rider";
$demo_content["realty_17"]["id_type"] = "sale";
$demo_content["realty_17"]["id_category"] = "Commercial";
$demo_content["realty_17"]["property_type"] = "Sports facility";
$demo_content["realty_17"]["status"] = 1;
$demo_content["realty_17"]["sold"] = 0;
$demo_content["realty_17"]["date_open"] = "2013-06-06 00:00:00";
$demo_content["realty_17"]["date_available"] = "2014-02-13 00:00:00";
$demo_content["realty_17"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_17"]["price"] = 1150000;
$demo_content["realty_17"]["gid_currency"] = "USD";
$demo_content["realty_17"]["square"] = 4100;
$demo_content["realty_17"]["square_unit"] = "sqft";
$demo_content["realty_17"]["address"] = "104, av. du President Kennedy";
$demo_content["realty_17"]["id_country"] = "France";
$demo_content["realty_17"]["id_region"] = "Région Île-de-France";
$demo_content["realty_17"]["id_city"] = "Paris";
$demo_content["realty_17"]["zip"] = "75016";
$demo_content["realty_17"]["lat"] = "48.8533231";
$demo_content["realty_17"]["lon"] = "2.2813768";
$demo_content["realty_17"]["logo_image"] = "613e234a66.jpg";
$demo_content["realty_17"]["slider_image"] = "613e234a66.jpg";
$demo_content["realty_17"]["id_wish_lists"] = array(1);
$demo_content["realty_17"]["slide_show_date_end"] = "2020-01-01 00:00";
$demo_content["realty_17"]["lift_up_date_end"] = "2020-01-01 00:00";
$demo_content["realty_17"]["fe_construction_status_2"] = 1;
$demo_content["realty_17"]["fe_year_2"] = 1964;
$demo_content["realty_17"]["fe_foundation_2"] = 3;
$demo_content["realty_17"]["fe_revenue_2"] = 25;
$demo_content["realty_17"]["fe_cash_flow_2"] = 80000;
$demo_content["realty_17"]["fe_years_established_2"] = 9;
$demo_content["realty_17"]["fe_employees_2"] = 15;
$demo_content["realty_17"]["fe_reason_for_selling_2"] = "";
$demo_content["realty_17"]["headline"] = "Beautiful 3 Bedroom + 3 Washroom Semi In Most Desirable Neighbourhood";
$demo_content["realty_17"]["fe_comments_2"] = "Exquisite and beautiful apartment";

$demo_content["realty_18"]["id"] = 18;
$demo_content["realty_18"]["id_user"] = "The Grey Rider";
$demo_content["realty_18"]["id_type"] = "sale";
$demo_content["realty_18"]["id_category"] = "Lots/lands";
$demo_content["realty_18"]["property_type"] = "Farm";
$demo_content["realty_18"]["status"] = 1;
$demo_content["realty_18"]["sold"] = 0;
$demo_content["realty_18"]["date_open"] = "2013-05-23 00:00:00";
$demo_content["realty_18"]["date_available"] = "2013-06-05 00:00:00";
$demo_content["realty_18"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_18"]["price"] = 62000;
$demo_content["realty_18"]["gid_currency"] = "USD";
$demo_content["realty_18"]["square"] = 1200;
$demo_content["realty_18"]["square_unit"] = "sqft";
$demo_content["realty_18"]["address"] = "Augsburger 41";
$demo_content["realty_18"]["id_country"] = "Germany";
$demo_content["realty_18"]["id_region"] = "Land Berlin";
$demo_content["realty_18"]["id_city"] = "Berlin";
$demo_content["realty_18"]["zip"] = "10789";
$demo_content["realty_18"]["lat"] = "52.5191710";
$demo_content["realty_18"]["lon"] = "13.4060912";
$demo_content["realty_18"]["logo_image"] = "98d53453ea.jpg";
$demo_content["realty_18"]["slider_image"] = "98d53453ea.jpg";
$demo_content["realty_18"]["listing_file"] = "ae6f3bf295.docx";
$demo_content["realty_18"]["listing_file_comment"] = "In architecture and building engineering, a floor plan otherwise known as a scottish plan is a drawing to scale, showing a view from above, of the relationships between rooms, spaces and other physical features at one level of a structure. Dimensions are usually drawn between the walls to specify room sizes and wall lengths. Floor plans may also include details of fixtures like sinks, water heaters, furnaces, etc. Floor plans may include notes for construction to specify finishes, construction methods, or symbols for electrical items.";
$demo_content["realty_18"]["listing_file_date"] = "2013-08-05 05:51:28";
$demo_content["realty_18"]["listing_video"] = "101_01c0be62.flv";
$demo_content["realty_18"]["listing_video_image"] = "101_01c0be62.jpg";
$demo_content["realty_18"]["listing_video_data"] = array("data"=>array("file_name"=>"101_01c0be62.avi","file_path"=>"/home/svinokurov/public_html/pg_realestate2/uploads/video/listing-video/7/18/","file_data"=>array("name"=>"Germany, Berlin, Augsburger 41, 10789","description"=>"","ext"=>"avi","type"=>"video/x-msvideo"),"status"=>"start","id_object"=>"18","video_upload_gid"=>"listing-video","video"=>"101_01c0be62.flv","upload_type"=>"local", "image"=>"101_01c0be62.jpg"),"status"=>"end","errors"=>array());
$demo_content["realty_18"]["id_wish_lists"] = array(1);
$demo_content["realty_18"]["featured_date_end"] = '2020-01-01 00:00:00'; 
$demo_content["realty_18"]["fe_irrigated_3"] = 1;
$demo_content["realty_18"]["fe_residence_3"] = 1;
$demo_content["realty_18"]["headline"] = "Award Winning David Small Designed Home Across The Street From Lake Ontario In Old Port Credit.Approx 4000Sq Ft Of Luxurious Liv Space.";
$demo_content["realty_18"]["fe_comments_3"] = "This two bedroom, two full bath Downtown Atlanta condo is located in the Fairlie-Poplar neighborhood near Georgia State University. The Healey Building was built by real estate developer William T. Healey and designed by Walter T. Downing. The Healey is listed with the National Register of Historic Places and has distinguished Landmark status. In 2001 the building was converted to condominiums with retail space below. The ground floor of the building, which encompasses an entire city block, includes numerous restaurants, salons, clothing stores, offices, and much more. It is within walking distance to two Marta stations and Woodruff Park. The building features 24-hr concierge, a guest suite, and fitness room. Condo 5A features a great city view from multiple 8 foot windows. This home features hardwood floors and crown molding throughout and an open living room/ dining room/ kitchen. The kitchen features stainless steel appliances, granite countertops, wood cabinets, and an island cooktop. Both bedrooms are spacious and feature custom closet shelving. The master bathroom is equipped with a jetted garden tub and separate shower. This unit is the perfect home for a buyer wanting city living near the office or someone wanting a second home near major Atlanta attractions.";

$demo_content["realty_19"]["id"] = 19;
$demo_content["realty_19"]["id_user"] = "Coldwell Banker Dolphin Realty";
$demo_content["realty_19"]["id_type"] = "sale";
$demo_content["realty_19"]["id_category"] = "Residential";
$demo_content["realty_19"]["property_type"] = "Townhouse";
$demo_content["realty_19"]["status"] = 1;
$demo_content["realty_19"]["sold"] = 0;
$demo_content["realty_19"]["date_open"] = "2013-05-16 00:00:00";
$demo_content["realty_19"]["date_available"] = "2014-01-02 00:00:00";
$demo_content["realty_19"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_19"]["price"] = 980000;
$demo_content["realty_19"]["price"] = 970000;
$demo_content["realty_19"]["gid_currency"] = "USD";
$demo_content["realty_19"]["square"] = 3200;
$demo_content["realty_19"]["square_unit"] = "sqft";
$demo_content["realty_19"]["address"] = "Avenida de la Hispanidad, 4";
$demo_content["realty_19"]["id_country"] = "Spain";
$demo_content["realty_19"]["id_region"] = "Comunidad de Madrid";
$demo_content["realty_19"]["id_city"] = "Madrid";
$demo_content["realty_19"]["zip"] = "28042";
$demo_content["realty_19"]["lat"] = "40.4528898";
$demo_content["realty_19"]["lon"] = "-3.5875343";
$demo_content["realty_19"]["logo_image"] = "33dc9cf44d.jpg";
$demo_content["realty_19"]["slider_image"] = "33dc9cf44d.jpg";
$demo_content["realty_19"]["id_wish_lists"] = array(1);
$demo_content["realty_19"]["featured_date_end"] = "2020-01-01 00:00";
$demo_content["realty_19"]["fe_construction_status_1"] = 2;
$demo_content["realty_19"]["fe_live_square_1"] = 3000;
$demo_content["realty_19"]["fe_bd_rooms_1"] = 4;
$demo_content["realty_19"]["fe_bth_rooms_1"] = 2;
$demo_content["realty_19"]["fe_garages_1"] = 1;
$demo_content["realty_19"]["fe_year_1"] = 2002;
$demo_content["realty_19"]["fe_foundation_1"] = 2;
$demo_content["realty_19"]["fe_floor_number_1"] = 5;
$demo_content["realty_19"]["fe_total_floors_1"] = 10;
$demo_content["realty_19"]["fe_distance_to_subway_1"] = 0;
$demo_content["realty_19"]["fe_furniture_1"] = 1;
$demo_content["realty_19"]["fe_accomodation_1_option_1"] = 1;
$demo_content["realty_19"]["fe_accomodation_1_option_2"] = 1;
$demo_content["realty_19"]["fe_accomodation_1_option_3"] = 1;
$demo_content["realty_19"]["fe_accomodation_1_option_5"] = 1;
$demo_content["realty_19"]["fe_accomodation_1_option_6"] = 1;
$demo_content["realty_19"]["fe_accomodation_1_option_7"] = 1;
$demo_content["realty_19"]["fe_accomodation_1_option_10"] = 1;
$demo_content["realty_19"]["fe_appliances_1_option_1"] = 1;
$demo_content["realty_19"]["fe_appliances_1_option_3"] = 1;
$demo_content["realty_19"]["fe_appliances_1_option_8"] = 1;
$demo_content["realty_19"]["fe_appliances_1_option_15"] = 1;
$demo_content["realty_19"]["fe_appliances_1_option_19"] = 1;
$demo_content["realty_19"]["fe_amenities_1_option_4"] = 1;
$demo_content["realty_19"]["fe_amenities_1_option_6"] = 1;
$demo_content["realty_19"]["fe_amenities_1_option_9"] = 1;
$demo_content["realty_19"]["fe_amenities_1_option_12"] = 1;
$demo_content["realty_19"]["fe_amenities_1_option_15"] = 1;
$demo_content["realty_19"]["fe_environment_1_option_10"] = 1;
$demo_content["realty_19"]["fe_environment_1_option_11"] = 1;
$demo_content["realty_19"]["fe_environment_1_option_12"] = 1;
$demo_content["realty_19"]["fe_environment_1_option_13"] = 1;
$demo_content["realty_19"]["fe_environment_1_option_14"] = 1;
$demo_content["realty_19"]["headline"] = "This House Sparkles From Top To Bottom **Stunning Masterpiece Loaded With Upgrades.";
$demo_content["realty_19"]["fe_comments_1"] = "Villa at Papago Park";

$demo_content["realty_20"]["id"] = 20;
$demo_content["realty_20"]["id_user"] = "Barbara Close";
$demo_content["realty_20"]["id_type"] = "sale";
$demo_content["realty_20"]["id_category"] = "Commercial";
$demo_content["realty_20"]["property_type"] = "Warehouse";
$demo_content["realty_20"]["status"] = 1;
$demo_content["realty_20"]["sold"] = 0;
$demo_content["realty_20"]["date_open"] = "2013-05-14 00:00:00";
$demo_content["realty_20"]["date_available"] = "2013-04-27 00:00:00";
$demo_content["realty_20"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_20"]["price"] = 249900;
$demo_content["realty_20"]["gid_currency"] = "USD";
$demo_content["realty_20"]["square"] = 2300;
$demo_content["realty_20"]["square_unit"] = "sqft";
$demo_content["realty_20"]["address"] = "1622 Bloor W";
$demo_content["realty_20"]["id_country"] = "Canada";
$demo_content["realty_20"]["id_region"] = "Ontario";
$demo_content["realty_20"]["id_city"] = "Toronto";
$demo_content["realty_20"]["zip"] = "M5W 1E6";
$demo_content["realty_20"]["lat"] = "43.6556100";
$demo_content["realty_20"]["lon"] = "-79.4557795";
$demo_content["realty_20"]["logo_image"] = "8172c45035.jpg";
$demo_content["realty_20"]["slider_image"] = "8172c45035.jpg";
$demo_content["realty_20"]["id_wish_lists"] = array(1, 4);
$demo_content["realty_20"]["fe_construction_status_2"] = 1;
$demo_content["realty_20"]["fe_year_2"] = 1998;
$demo_content["realty_20"]["fe_foundation_2"] = 1;
$demo_content["realty_20"]["fe_revenue_2"] = 85;
$demo_content["realty_20"]["fe_cash_flow_2"] = 20000;
$demo_content["realty_20"]["fe_years_established_2"] = 18;
$demo_content["realty_20"]["fe_employees_2"] = 3;
$demo_content["realty_20"]["fe_reason_for_selling_2"] = "";
$demo_content["realty_20"]["headline"] = "You Will Love This Absolutely Stunning 3 Bedroom Home In Lisgar! Fantastic Open-Concept Floor Plan With Walk-Out To A Beautiful Private Garden.";
$demo_content["realty_20"]["fe_comments_2"] = "Luxury apartment you deserve!";

$demo_content["realty_21"]["id"] = 21;
$demo_content["realty_21"]["id_user"] = "The Gouglas Realty";
$demo_content["realty_21"]["id_type"] = "sale";
$demo_content["realty_21"]["id_category"] = "Lots/lands";
$demo_content["realty_21"]["property_type"] = "Pasture";
$demo_content["realty_21"]["status"] = 1;
$demo_content["realty_21"]["sold"] = 0;
$demo_content["realty_21"]["date_open"] = "2013-07-18 00:00:00";
$demo_content["realty_21"]["date_available"] = "2013-12-09 00:00:00";
$demo_content["realty_21"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_21"]["price"] = 665000;
$demo_content["realty_21"]["gid_currency"] = "USD";
$demo_content["realty_21"]["square"] = 2700;
$demo_content["realty_21"]["square_unit"] = "sqft";
$demo_content["realty_21"]["address"] = "Vorontsovskaya, 35";
$demo_content["realty_21"]["id_country"] = "Russia";
$demo_content["realty_21"]["id_region"] = "Moscow";
$demo_content["realty_21"]["id_city"] = "Moscow";
$demo_content["realty_21"]["zip"] = "109147";
$demo_content["realty_21"]["lat"] = "55.7343065";
$demo_content["realty_21"]["lon"] = "37.6625012";
$demo_content["realty_21"]["logo_image"] = "a275e1a259.jpg";
$demo_content["realty_21"]["slider_image"] = "a275e1a259.jpg";
$demo_content["realty_21"]["fe_irrigated_3"] = 1;
$demo_content["realty_21"]["fe_residence_3"] = 0;
$demo_content["realty_21"]["headline"] = "Welcome To This Remarkable Home In Family Friendly Neighborhood! This 3Bed Det Home Freshly Painted Offers!Great Size Rooms,Recently Fin Bsmt!Sep Laundry!";
$demo_content["realty_21"]["fe_comments_3"] = "Located in nice cul-de-sac street. This mini estate has lovely grounds with lovely tall mature trees and landscape. There is a \"separate guest unit\" and pool. Lots of bedrooms in main house.";

$demo_content["realty_22"]["id"] = 22;
$demo_content["realty_22"]["id_user"] = "Carolina Mountain Realty, Inc.";
$demo_content["realty_22"]["id_type"] = "sale";
$demo_content["realty_22"]["id_category"] = "Residential";
$demo_content["realty_22"]["property_type"] = "Mobile home";
$demo_content["realty_22"]["status"] = 1;
$demo_content["realty_22"]["sold"] = 0;
$demo_content["realty_22"]["date_open"] = "2013-08-08 00:00:00";
$demo_content["realty_22"]["date_available"] = "2013-07-17 00:00:00";
$demo_content["realty_22"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_22"]["price"] = 90000;
$demo_content["realty_22"]["price"] = 80500;
$demo_content["realty_22"]["gid_currency"] = "USD";
$demo_content["realty_22"]["square"] = 1300;
$demo_content["realty_22"]["square_unit"] = "sqft";
$demo_content["realty_22"]["address"] = "696 Avenue Of The Americas";
$demo_content["realty_22"]["id_country"] = "United States";
$demo_content["realty_22"]["id_region"] = "New York";
$demo_content["realty_22"]["id_city"] = "New York City";
$demo_content["realty_22"]["zip"] = "10010";
$demo_content["realty_22"]["lat"] = "40.7421338";
$demo_content["realty_22"]["lon"] = "-73.9932340";
$demo_content["realty_22"]["logo_image"] = "46b9ae2141.jpg";
$demo_content["realty_22"]["slider_image"] = "46b9ae2141.jpg";
$demo_content["realty_22"]["listing_file"] = "ae6f3bf295.docx";
$demo_content["realty_22"]["listing_file_comment"] = "In architecture and building engineering, a floor plan otherwise known as a scottish plan is a drawing to scale, showing a view from above, of the relationships between rooms, spaces and other physical features at one level of a structure. Dimensions are usually drawn between the walls to specify room sizes and wall lengths. Floor plans may also include details of fixtures like sinks, water heaters, furnaces, etc. Floor plans may include notes for construction to specify finishes, construction methods, or symbols for electrical items.";
$demo_content["realty_22"]["listing_file_date"] = "2013-08-05 05:51:28";
$demo_content["realty_22"]["listing_video"] = "101_01c0be62.flv";
$demo_content["realty_22"]["listing_video_image"] = "101_01c0be62.jpg";
$demo_content["realty_22"]["listing_video_data"] = array("data"=>array("file_name"=>"101_01c0be62.avi","file_path"=>"/home/svinokurov/public_html/pg_realestate2/uploads/video/listing-video/10/22/","file_data"=>array("name"=>"United States, New York City, 696 Avenue Of The Americas, 10010","description"=>"","ext"=>"avi","type"=>"video/x-msvideo"),"status"=>"start","id_object"=>"22","video_upload_gid"=>"listing-video","video"=>"101_01c0be62.flv","upload_type"=>"local", "image"=>"101_01c0be62.jpg"),"status"=>"end","errors"=>array());
$demo_content["realty_22"]["id_wish_lists"] = array(3);
$demo_content["realty_22"]["lift_up_date_end"] = "2020-01-01 00:00";
$demo_content["realty_22"]["highlight_date_end"] = "2020-01-01 00:00";
$demo_content["realty_22"]["fe_construction_status_1"] = 1;
$demo_content["realty_22"]["fe_live_square_1"] = 1100;
$demo_content["realty_22"]["fe_bd_rooms_1"] = 1;
$demo_content["realty_22"]["fe_bth_rooms_1"] = 1;
$demo_content["realty_22"]["fe_garages_1"] = 0;
$demo_content["realty_22"]["fe_year_1"] = 2010;
$demo_content["realty_22"]["fe_foundation_1"] = 2;
$demo_content["realty_22"]["fe_floor_number_1"] = 7;
$demo_content["realty_22"]["fe_total_floors_1"] = 9;
$demo_content["realty_22"]["fe_distance_to_subway_1"] = 5;
$demo_content["realty_22"]["fe_furniture_1"] = 1;
$demo_content["realty_22"]["fe_accomodation_1_option_1"] = 1;
$demo_content["realty_22"]["fe_accomodation_1_option_2"] = 1;
$demo_content["realty_22"]["fe_accomodation_1_option_8"] = 1;
$demo_content["realty_22"]["fe_accomodation_1_option_10"] = 1;
$demo_content["realty_22"]["fe_accomodation_1_option_19"] = 1;
$demo_content["realty_22"]["fe_appliances_1_option_1"] = 1;
$demo_content["realty_22"]["fe_appliances_1_option_3"] = 1;
$demo_content["realty_22"]["fe_appliances_1_option_6"] = 1;
$demo_content["realty_22"]["fe_appliances_1_option_9"] = 1;
$demo_content["realty_22"]["fe_appliances_1_option_12"] = 1;
$demo_content["realty_22"]["fe_amenities_1_option_1"] = 1;
$demo_content["realty_22"]["fe_amenities_1_option_2"] = 1;
$demo_content["realty_22"]["fe_amenities_1_option_3"] = 1;
$demo_content["realty_22"]["fe_amenities_1_option_4"] = 1;
$demo_content["realty_22"]["fe_amenities_1_option_5"] = 1;
$demo_content["realty_22"]["fe_environment_1_option_1"] = 1;
$demo_content["realty_22"]["fe_environment_1_option_2"] = 1;
$demo_content["realty_22"]["fe_environment_1_option_3"] = 1;
$demo_content["realty_22"]["fe_environment_1_option_4"] = 1;
$demo_content["realty_22"]["fe_environment_1_option_5"] = 1;
$demo_content["realty_22"]["headline"] = "Outstanding Vacant Double Lot Waterfront Island Acreage Featuring A Virgin Forest On 413 Feet Of Typical Muskoka Granite Shoreline.";
$demo_content["realty_22"]["fe_comments_1"] = "Affording A Dream Cottage Design. Building, Septic And Dock Permits Are Available From The Township Of Georgian Bay. Current Survey Available.";

$demo_content["realty_23"]["id"] = 23;
$demo_content["realty_23"]["id_user"] = "Frankie Firouznia";
$demo_content["realty_23"]["id_type"] = "sale";
$demo_content["realty_23"]["id_category"] = "Commercial";
$demo_content["realty_23"]["property_type"] = "Manufactured";
$demo_content["realty_23"]["status"] = 1;
$demo_content["realty_23"]["sold"] = 0;
$demo_content["realty_23"]["date_available"] = "2013-09-17 00:00:00";
$demo_content["realty_23"]["date_open"] = "2013-06-17 00:00:00";
$demo_content["realty_23"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_23"]["price"] = 84000;
$demo_content["realty_23"]["gid_currency"] = "USD";
$demo_content["realty_23"]["square"] = 1350;
$demo_content["realty_23"]["square_unit"] = "sqft";
$demo_content["realty_23"]["address"] = "24 Grosvenor Square";
$demo_content["realty_23"]["id_country"] = "United Kingdom";
$demo_content["realty_23"]["id_region"] = "Greater London";
$demo_content["realty_23"]["id_city"] = "London";
$demo_content["realty_23"]["zip"] = "W1K 6AH";
$demo_content["realty_23"]["lat"] = "51.5117508";
$demo_content["realty_23"]["lon"] = "-0.1529341";
$demo_content["realty_23"]["logo_image"] = "353d5a2d0e.jpg";
$demo_content["realty_23"]["slider_image"] = "353d5a2d0e.jpg";
$demo_content["realty_23"]["featured_date_end"] = '2020-01-01 00:00:00'; 
$demo_content["realty_23"]["fe_construction_status_2"] = 1;
$demo_content["realty_23"]["fe_year_2"] = 1995;
$demo_content["realty_23"]["fe_foundation_2"] = 1;
$demo_content["realty_23"]["fe_revenue_2"] = 75;
$demo_content["realty_23"]["fe_cash_flow_2"] = 450000;
$demo_content["realty_23"]["fe_years_established_2"] = 9;
$demo_content["realty_23"]["fe_employees_2"] = 65;
$demo_content["realty_23"]["fe_reason_for_selling_2"] = "";
$demo_content["realty_23"]["headline"] = "Big Solid Brick Corner Unit Home With Formal Living, Formal Dining & Formal Family Room. Original Owners Kept In Pristine Condition.";
$demo_content["realty_23"]["fe_comments_2"] = "This eight-bedroom home features a fantastic new gunite pool, gracious formal rooms, new state-of-the-art kitchen, three fireplaces, exceptional architectural details, beautiful stone work and gleaming hardwood floors throughout.";

$demo_content["realty_24"]["id"] = 24;
$demo_content["realty_24"]["id_user"] = "Frankie Firouznia";
$demo_content["realty_24"]["id_type"] = "sale";
$demo_content["realty_24"]["id_category"] = "Lots/lands";
$demo_content["realty_24"]["property_type"] = "Timberland";
$demo_content["realty_24"]["status"] = 1;
$demo_content["realty_24"]["sold"] = 0;
$demo_content["realty_24"]["date_open"] = "2013-08-21 00:00:00";
$demo_content["realty_24"]["date_available"] = "2013-07-06 00:00:00";
$demo_content["realty_24"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_24"]["price"] = 175000;
$demo_content["realty_24"]["gid_currency"] = "USD";
$demo_content["realty_24"]["square"] = 2100;
$demo_content["realty_24"]["square_unit"] = "sqft";
$demo_content["realty_24"]["address"] = "9, Boulevard de La Madeleine";
$demo_content["realty_24"]["id_country"] = "France";
$demo_content["realty_24"]["id_region"] = "Région Île-de-France";
$demo_content["realty_24"]["id_city"] = "Paris";
$demo_content["realty_24"]["zip"] = "75001";
$demo_content["realty_24"]["lat"] = "48.8694991";
$demo_content["realty_24"]["lon"] = "2.3266096";
$demo_content["realty_24"]["logo_image"] = "7cf3e26e0f.jpg";
$demo_content["realty_24"]["slider_image"] = "7cf3e26e0f.jpg";
$demo_content["realty_24"]["lift_up_date_end"] = "2020-01-01 00:00";
$demo_content["realty_24"]["fe_irrigated_3"] = 1;
$demo_content["realty_24"]["fe_residence_3"] = 1;
$demo_content["realty_24"]["headline"] = "Welcome To Spectacular 4 Bedroom 3 Bathroom In Prime Bradford! This 2413 S/F Sunderland Model Less Than 1 Year New!";
$demo_content["realty_24"]["fe_comments_3"] = "Great Space! Great Value! The \"newer\" list: windows, siding, furnace, and A/C, roof, fence, hot water heater! \"cabinets in eat-in kitchen with large deck right off it for bar-b-ques! Updated baths!";

$demo_content["realty_25"]["id"] = 25;
$demo_content["realty_25"]["id_user"] = "Deborah Black";
$demo_content["realty_25"]["id_type"] = "sale";
$demo_content["realty_25"]["id_category"] = "Residential";
$demo_content["realty_25"]["property_type"] = "Other";
$demo_content["realty_25"]["status"] = 1;
$demo_content["realty_25"]["sold"] = 0;
$demo_content["realty_25"]["date_open"] = "2013-04-25 00:00:00";
$demo_content["realty_25"]["date_available"] = "2013-07-17 00:00:00";
$demo_content["realty_25"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_25"]["price"] = 96000;
$demo_content["realty_25"]["gid_currency"] = "USD";
$demo_content["realty_25"]["square"] = 1270;
$demo_content["realty_25"]["square_unit"] = "sqft";
$demo_content["realty_25"]["address"] = "Friedrich 99";
$demo_content["realty_25"]["id_country"] = "Germany";
$demo_content["realty_25"]["id_region"] = "Land Berlin";
$demo_content["realty_25"]["id_city"] = "Berlin";
$demo_content["realty_25"]["zip"] = "10117";
$demo_content["realty_25"]["lat"] = "52.5886748";
$demo_content["realty_25"]["lon"] = "13.3880120";
$demo_content["realty_25"]["logo_image"] = "1f043c4445.jpg";
$demo_content["realty_25"]["slider_image"] = "1f043c4445.jpg";
$demo_content["realty_25"]["id_wish_lists"] = array(4);
$demo_content["realty_25"]["featured_date_end"] = '2020-01-01 00:00:00'; 
$demo_content["realty_25"]["fe_construction_status_1"] = 2;
$demo_content["realty_25"]["fe_live_square_1"] = 1100;
$demo_content["realty_25"]["fe_bd_rooms_1"] = 2;
$demo_content["realty_25"]["fe_bth_rooms_1"] = 1;
$demo_content["realty_25"]["fe_garages_1"] = 0;
$demo_content["realty_25"]["fe_year_1"] = 2005;
$demo_content["realty_25"]["fe_foundation_1"] = 1;
$demo_content["realty_25"]["fe_floor_number_1"] = 3;
$demo_content["realty_25"]["fe_total_floors_1"] = 12;
$demo_content["realty_25"]["fe_distance_to_subway_1"] = 5;
$demo_content["realty_25"]["fe_furniture_1"] = 1;
$demo_content["realty_25"]["fe_accomodation_1_option_1"] = 1;
$demo_content["realty_25"]["fe_accomodation_1_option_2"] = 1;
$demo_content["realty_25"]["fe_accomodation_1_option_4"] = 1;
$demo_content["realty_25"]["fe_accomodation_1_option_15"] = 1;
$demo_content["realty_25"]["fe_accomodation_1_option_19"] = 1;
$demo_content["realty_25"]["fe_appliances_1_option_1"] = 1;
$demo_content["realty_25"]["fe_appliances_1_option_2"] = 1;
$demo_content["realty_25"]["fe_appliances_1_option_4"] = 1;
$demo_content["realty_25"]["fe_appliances_1_option_7"] = 1;
$demo_content["realty_25"]["fe_appliances_1_option_9"] = 1;
$demo_content["realty_25"]["fe_amenities_1_option_1"] = 1;
$demo_content["realty_25"]["fe_amenities_1_option_3"] = 1;
$demo_content["realty_25"]["fe_amenities_1_option_8"] = 1;
$demo_content["realty_25"]["fe_amenities_1_option_14"] = 1;
$demo_content["realty_25"]["fe_amenities_1_option_15"] = 1;
$demo_content["realty_25"]["fe_environment_1_option_1"] = 1;
$demo_content["realty_25"]["fe_environment_1_option_5"] = 1;
$demo_content["realty_25"]["fe_environment_1_option_8"] = 1;
$demo_content["realty_25"]["fe_environment_1_option_14"] = 1;
$demo_content["realty_25"]["fe_environment_1_option_15"] = 1;
$demo_content["realty_25"]["headline"] = "Perfect For First Time Home Buyer Or Investor. 2 Bdrm Bath, 2 Story Semi-Detached Home On A Large Lot With Potential For Severance.";
$demo_content["realty_25"]["fe_comments_1"] = "Beautiful home in prime location! Gorgeous curb appeal, wood floors, open floor plan, recessed lighting, cozy fireplace and more.";

$demo_content["realty_26"]["id"] = 26;
$demo_content["realty_26"]["id_user"] = "Cornell Commons";
$demo_content["realty_26"]["id_type"] = "sale";
$demo_content["realty_26"]["id_category"] = "Commercial";
$demo_content["realty_26"]["property_type"] = "Other";
$demo_content["realty_26"]["status"] = 0;
$demo_content["realty_26"]["sold"] = 0;
$demo_content["realty_26"]["date_open"] = "2020-01-01 00:00:00";
$demo_content["realty_26"]["date_available"] = "2013-11-08 00:00:00";
$demo_content["realty_26"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_26"]["price"] = 130000;
$demo_content["realty_26"]["price_reduced"] = 129000;
$demo_content["realty_26"]["gid_currency"] = "USD";
$demo_content["realty_26"]["square"] = 1400;
$demo_content["realty_26"]["square_unit"] = "sqft";
$demo_content["realty_26"]["address"] = "Paseo de la Castellana 259-B";
$demo_content["realty_26"]["id_country"] = "Spain";
$demo_content["realty_26"]["id_region"] = "Comunidad de Madrid";
$demo_content["realty_26"]["id_city"] = "Madrid";
$demo_content["realty_26"]["zip"] = "28046";
$demo_content["realty_26"]["lat"] = "40.4357790";
$demo_content["realty_26"]["lon"] = "-3.6897196";
$demo_content["realty_26"]["logo_image"] = "7d16c9dff4.jpg";
$demo_content["realty_26"]["slider_image"] = "7d16c9dff4.jpg";
$demo_content["realty_26"]["listing_file"] = "ae6f3bf295.docx";
$demo_content["realty_26"]["listing_file_name"] = "ae6f3bf295.docx";
$demo_content["realty_26"]["listing_file_comment"] = "In architecture and building engineering, a floor plan otherwise known as a scottish plan is a drawing to scale, showing a view from above, of the relationships between rooms, spaces and other physical features at one level of a structure. Dimensions are usually drawn between the walls to specify room sizes and wall lengths. Floor plans may also include details of fixtures like sinks, water heaters, furnaces, etc. Floor plans may include notes for construction to specify finishes, construction methods, or symbols for electrical items.";
$demo_content["realty_26"]["listing_file_date"] = "2013-08-05 05:51:28";
$demo_content["realty_26"]["listing_video"] = "101_01c0be62.flv";
$demo_content["realty_26"]["listing_video_image"] = "101_01c0be62.jpg";
$demo_content["realty_26"]["listing_video_data"] = array("data"=>array("file_name"=>"101_01c0be62.avi","file_path"=>"/home/svinokurov/public_html/pg_realestate2/uploads/video/listing-video/13/26/","file_data"=>array("name"=>"Spain, Madrid, Paseo de la Castellana 259-B, 28046","description"=>"","ext"=>"avi","type"=>"video/x-msvideo"),"status"=>"start","id_object"=>"26","video_upload_gid"=>"listing-video","video"=>"101_01c0be62.flv","upload_type"=>"local", "image"=>"101_01c0be62.jpg"),"status"=>"end","errors"=>array());
$demo_content["realty_26"]["featured_date_end"] = '2020-01-01 00:00:00';
$demo_content["realty_26"]["lift_up_date_end"] = "2020-01-01 00:00";
$demo_content["realty_26"]["fe_construction_status_2"] = 2;
$demo_content["realty_26"]["fe_year_2"] = 1983;
$demo_content["realty_26"]["fe_foundation_2"] = 4;
$demo_content["realty_26"]["fe_revenue_2"] = 35;
$demo_content["realty_26"]["fe_cash_flow_2"] = 54000;
$demo_content["realty_26"]["fe_years_established_2"] = 13;
$demo_content["realty_26"]["fe_employees_2"] = 10;
$demo_content["realty_26"]["fe_reason_for_selling_2"] = "";  
$demo_content["realty_26"]["headline"] = "Absolutely Gorgeous, Picture Perfect, Recently Renovated 4 Bdrm/3 Bath Family Home. Bright, Upgraded Kitchen With Breakfast Area.";
$demo_content["realty_26"]["fe_comments_2"] = "Wonderful apartment for a great price! Great layout with upgraded floors, bathrooms & more.";

$demo_content["realty_27"]["id"] = 27;
$demo_content["realty_27"]["id_user"] = "Cornell Commons";
$demo_content["realty_27"]["id_type"] = "buy";
$demo_content["realty_27"]["id_category"] = "Lots/lands";
$demo_content["realty_27"]["property_type"] = "Undeveloped land";
$demo_content["realty_27"]["status"] = 1;
$demo_content["realty_27"]["sold"] = 0;
$demo_content["realty_27"]["date_open"] = "2013-03-04 00:00:00";
$demo_content["realty_27"]["date_available"] = "2014-02-01 00:00:00";
$demo_content["realty_27"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_27"]["price"] = 50000;
$demo_content["realty_27"]["price_max"] = 70000;
$demo_content["realty_27"]["gid_currency"] = "USD";
$demo_content["realty_27"]["square"] = 1000;
$demo_content["realty_27"]["square_max"] = 1200;
$demo_content["realty_27"]["square_unit"] = "sqft";
$demo_content["realty_27"]["id_country"] = "Canada";
$demo_content["realty_27"]["id_region"] = "Ontario";
$demo_content["realty_27"]["id_city"] = "Toronto";
$demo_content["realty_27"]["zip"] = "M4Y 1X7";
$demo_content["realty_27"]["lat"] = "43.6624348";
$demo_content["realty_27"]["lon"] = "-79.3833503";
$demo_content["realty_27"]["id_wish_lists"] = array(3);
$demo_content["realty_27"]["featured_date_end"] = '2020-01-01 00:00:00';
$demo_content["realty_27"]["fe_irrigated_3"] = 0;
$demo_content["realty_27"]["fe_residence_3"] = 1; 
$demo_content["realty_27"]["headline"] = "Must See, Large Spacious Home,Huge Livingroom/ Diningroom/ Kitchen Combination On Main Floor, Walkout To Covered Porch From Kitchen, Hardwood Floors Throughout, Wood Trimmed Windows & Doors, Crown Mldg.";
$demo_content["realty_27"]["fe_comments_3"] = "Living room with beamed ceiling, warm brick fireplace and patio access. Spacious kitchen has patio access and plenty of cabinets.";

$demo_content["realty_28"]["id"] = 28;
$demo_content["realty_28"]["id_user"] = "Cornell Commons";
$demo_content["realty_28"]["id_type"] = "buy";
$demo_content["realty_28"]["id_category"] = "Residential";
$demo_content["realty_28"]["property_type"] = "Apartment";
$demo_content["realty_28"]["status"] = 1;
$demo_content["realty_28"]["sold"] = 0;
$demo_content["realty_28"]["date_open"] = "2013-05-09 00:00:00";
$demo_content["realty_28"]["date_available"] = "2013-08-01 00:00:00";
$demo_content["realty_28"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_28"]["price"] = 150000;
$demo_content["realty_28"]["price_max"] = 180000;
$demo_content["realty_28"]["gid_currency"] = "USD";
$demo_content["realty_28"]["square"] = 1800;
$demo_content["realty_28"]["square_max"] = 2000;
$demo_content["realty_28"]["square_unit"] = "sqft";
$demo_content["realty_28"]["id_country"] = "Russia";
$demo_content["realty_28"]["id_region"] = "Moscow";
$demo_content["realty_28"]["id_city"] = "Moscow";
$demo_content["realty_28"]["zip"] = "109444";
$demo_content["realty_28"]["lat"] = "55.7076184";
$demo_content["realty_28"]["lon"] = "37.7988859";
$demo_content["realty_28"]["fe_construction_status_1"] = 1;
$demo_content["realty_28"]["fe_live_square_1"] = 1600;
$demo_content["realty_28"]["fe_bd_rooms_1"] = 2;
$demo_content["realty_28"]["fe_bth_rooms_1"] = 1;
$demo_content["realty_28"]["fe_garages_1"] = 0;
$demo_content["realty_28"]["fe_year_1"] = 2009;
$demo_content["realty_28"]["fe_foundation_1"] = 2;
$demo_content["realty_28"]["fe_floor_number_1"] = 6;
$demo_content["realty_28"]["fe_total_floors_1"] = 14;
$demo_content["realty_28"]["fe_distance_to_subway_1"] = 15;
$demo_content["realty_28"]["fe_furniture_1"] = 1;
$demo_content["realty_28"]["fe_accomodation_1_option_1"] = 1;
$demo_content["realty_28"]["fe_accomodation_1_option_4"] = 1;
$demo_content["realty_28"]["fe_accomodation_1_option_6"] = 1;
$demo_content["realty_28"]["fe_accomodation_1_option_7"] = 1;
$demo_content["realty_28"]["fe_accomodation_1_option_8"] = 1;
$demo_content["realty_28"]["fe_appliances_1_option_1"] = 1;
$demo_content["realty_28"]["fe_appliances_1_option_5"] = 1;
$demo_content["realty_28"]["fe_appliances_1_option_6"] = 1;
$demo_content["realty_28"]["fe_appliances_1_option_8"] = 1;
$demo_content["realty_28"]["fe_appliances_1_option_9"] = 1;
$demo_content["realty_28"]["fe_amenities_1_option_1"] = 1;
$demo_content["realty_28"]["fe_amenities_1_option_6"] = 1;
$demo_content["realty_28"]["fe_amenities_1_option_7"] = 1;
$demo_content["realty_28"]["fe_amenities_1_option_14"] = 1;
$demo_content["realty_28"]["fe_amenities_1_option_15"] = 1;
$demo_content["realty_28"]["fe_environment_1_option_1"] = 1;
$demo_content["realty_28"]["fe_environment_1_option_8"] = 1;
$demo_content["realty_28"]["fe_environment_1_option_9"] = 1;
$demo_content["realty_28"]["fe_environment_1_option_17"] = 1;
$demo_content["realty_28"]["fe_environment_1_option_18"] = 1;
$demo_content["realty_28"]["headline"] = "Absolutely Beautiful,Gorgeous Minto 4 Bedroom&4 Bathroom Detached Home Located In A Fast Growing Community Of Estates Of Credit Ridge.";
$demo_content["realty_28"]["fe_comments_1"] = "Double Door Entry With Open To Above. Oak Round Stairs With Iron Pickets. Garage Door Entrance, All Electric Light Fixtures, Close To All Amenities.";

$demo_content["realty_29"]["id"] = 29;
$demo_content["realty_29"]["id_user"] = "Mark Louis";
$demo_content["realty_29"]["id_type"] = "buy";
$demo_content["realty_29"]["id_category"] = "Commercial";
$demo_content["realty_29"]["property_type"] = "Café";
$demo_content["realty_29"]["status"] = 1;
$demo_content["realty_29"]["sold"] = 0;
$demo_content["realty_29"]["date_open"] = "2013-04-07 00:00:00";
$demo_content["realty_29"]["date_available"] = "2013-10-10 00:00:00";
$demo_content["realty_29"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_29"]["price"] = 210000;
$demo_content["realty_29"]["price_max"] = 250000;
$demo_content["realty_29"]["gid_currency"] = "USD";
$demo_content["realty_29"]["square"] = 2100;
$demo_content["realty_29"]["square_max"] = 2300;
$demo_content["realty_29"]["square_unit"] = "sqft";
$demo_content["realty_29"]["id_country"] = "United States";
$demo_content["realty_29"]["id_region"] = "New York";
$demo_content["realty_29"]["id_city"] = "New York City";
$demo_content["realty_29"]["zip"] = "10012"; 
$demo_content["realty_29"]["lat"] = "40.7302281";
$demo_content["realty_29"]["lon"] = "-74.0004194";
$demo_content["realty_29"]["highlight_date_end"] = "2020-01-01 00:00";
$demo_content["realty_29"]["fe_construction_status_2"] = 1;
$demo_content["realty_29"]["fe_year_2"] = 1994;
$demo_content["realty_29"]["fe_foundation_2"] = 3;
$demo_content["realty_29"]["fe_revenue_2"] = 65;
$demo_content["realty_29"]["fe_cash_flow_2"] = 87000;
$demo_content["realty_29"]["fe_years_established_2"] = 10;
$demo_content["realty_29"]["fe_employees_2"] = 25;
$demo_content["realty_29"]["fe_reason_for_selling_2"] = "";
$demo_content["realty_29"]["headline"] = "Absolutely Beautiful 5 Bedroom Home In The Heart Of Brampton In A Quiet Neighbourhood. Hardwood Throughout Main Floor With Finished Basement.";
$demo_content["realty_29"]["fe_comments_2"] = "All Appliances-Stove,Fridge,Dishwasher,Washer And Dryer. All Light Fixtures And Window Coverings.";

$demo_content["realty_30"]["id"] = 30;
$demo_content["realty_30"]["id_user"] = "Pilot Group";
$demo_content["realty_30"]["id_type"] = "buy";
$demo_content["realty_30"]["id_category"] = "Lots/lands";
$demo_content["realty_30"]["property_type"] = "Other";
$demo_content["realty_30"]["status"] = 1;
$demo_content["realty_30"]["sold"] = 0;
$demo_content["realty_30"]["date_open"] = "2013-07-31 00:00:00";
$demo_content["realty_30"]["date_available"] = "2013-10-12 00:00:00";
$demo_content["realty_30"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_30"]["price"] = 300000;
$demo_content["realty_30"]["price_max"] = 310000;
$demo_content["realty_30"]["gid_currency"] = "USD";
$demo_content["realty_30"]["square"] = 2200;
$demo_content["realty_30"]["square_max"] = 2300;
$demo_content["realty_30"]["square_unit"] = "sqft";
$demo_content["realty_30"]["id_country"] = "United Kingdom";
$demo_content["realty_30"]["id_region"] = "Greater London";
$demo_content["realty_30"]["id_city"] = "London";
$demo_content["realty_30"]["zip"] = "SW17 0QT";
$demo_content["realty_30"]["lat"] = "51.4448273";
$demo_content["realty_30"]["lon"] = "-0.2159221";
$demo_content["realty_30"]["fe_irrigated_3"] = 0;
$demo_content["realty_30"]["fe_residence_3"] = 0;
$demo_content["realty_30"]["headline"] = "Prime Bathurst Manor Location! Bright And Spacious Bungalow On A Quiet Street With Income Opportunity.";
$demo_content["realty_30"]["fe_comments_3"] = "All Electrical Light Fixtures, Fridge, Freezer, Washer/Dryer, Window Coverings/ Exclude Dining Room Chandelier And Master Bedroom Light Fixture. Near Parks, Recreation Center. Shopping, Ttc, Subway And Places Of Worship.";

$demo_content["realty_31"]["id"] = 31;
$demo_content["realty_31"]["id_user"] = "The Gouglas Realty";
$demo_content["realty_31"]["id_type"] = "buy";
$demo_content["realty_31"]["id_category"] = "Residential";
$demo_content["realty_31"]["property_type"] = "Room";
$demo_content["realty_31"]["status"] = 1;
$demo_content["realty_31"]["sold"] = 0;
$demo_content["realty_31"]["date_open"] = "2013-04-30 00:00:00";
$demo_content["realty_31"]["date_available"] = "2013-11-05 00:00:00";
$demo_content["realty_31"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_31"]["price"] = 70000;
$demo_content["realty_31"]["price_max"] = 90000;
$demo_content["realty_31"]["gid_currency"] = "USD";
$demo_content["realty_31"]["square"] = 1700;
$demo_content["realty_31"]["square_max"] = 1800;
$demo_content["realty_31"]["square_unit"] = "sqft";
$demo_content["realty_31"]["id_country"] = "France";
$demo_content["realty_31"]["id_region"] = "Région Île-de-France";
$demo_content["realty_31"]["id_city"] = "Paris";
$demo_content["realty_31"]["zip"] = "75001";
$demo_content["realty_31"]["lat"] = "48.8617981";
$demo_content["realty_31"]["lon"] = "2.3406522";
$demo_content["realty_31"]["fe_construction_status_1"] = 2;
$demo_content["realty_31"]["fe_live_square_1"] = 1560;
$demo_content["realty_31"]["fe_bd_rooms_1"] = 2;
$demo_content["realty_31"]["fe_bth_rooms_1"] = 1;
$demo_content["realty_31"]["fe_garages_1"] = 0;
$demo_content["realty_31"]["fe_year_1"] = 2011;
$demo_content["realty_31"]["fe_foundation_1"] = 4;
$demo_content["realty_31"]["fe_floor_number_1"] = 5;
$demo_content["realty_31"]["fe_total_floors_1"] = 9;
$demo_content["realty_31"]["fe_distance_to_subway_1"] = 0;
$demo_content["realty_31"]["fe_furniture_1"] = 1;
$demo_content["realty_31"]["fe_accomodation_1_option_1"] = 1;
$demo_content["realty_31"]["fe_accomodation_1_option_4"] = 1;
$demo_content["realty_31"]["fe_accomodation_1_option_8"] = 1;
$demo_content["realty_31"]["fe_accomodation_1_option_9"] = 1;
$demo_content["realty_31"]["fe_accomodation_1_option_12"] = 1;
$demo_content["realty_31"]["fe_appliances_1_option_4"] = 1;
$demo_content["realty_31"]["fe_appliances_1_option_5"] = 1;
$demo_content["realty_31"]["fe_appliances_1_option_7"] = 1;
$demo_content["realty_31"]["fe_appliances_1_option_9"] = 1;
$demo_content["realty_31"]["fe_appliances_1_option_10"] = 1;
$demo_content["realty_31"]["fe_amenities_1_option_11"] = 1;
$demo_content["realty_31"]["fe_amenities_1_option_12"] = 1;
$demo_content["realty_31"]["fe_amenities_1_option_13"] = 1;
$demo_content["realty_31"]["fe_amenities_1_option_14"] = 1;
$demo_content["realty_31"]["fe_amenities_1_option_15"] = 1;
$demo_content["realty_31"]["fe_environment_1_option_3"] = 1;
$demo_content["realty_31"]["fe_environment_1_option_4"] = 1;
$demo_content["realty_31"]["fe_environment_1_option_6"] = 1;
$demo_content["realty_31"]["fe_environment_1_option_8"] = 1;
$demo_content["realty_31"]["fe_environment_1_option_9"] = 1;
$demo_content["realty_31"]["headline"] = "Beautiful Bungalow Near Yonge & Sheppard, Steps To Transit Subway, Great School District, Shopping, Restaurants & More.";
$demo_content["realty_31"]["fe_comments_1"] = "Fridge, Stove, B/I Dw, Washer & Dryer, Freezer In Garage, 2 Garage Dr Open & Remotes, Garage Dr Security System, All E.L.Fs, All Wind Cover's.";

/*$demo_content["realty_32"]["id"] = 32;
$demo_content["realty_32"]["id_user"] = "Andrew Cooper";
$demo_content["realty_32"]["id_type"] = "buy";
$demo_content["realty_32"]["id_category"] = "Commercial";
$demo_content["realty_32"]["property_type"] = "Office";
$demo_content["realty_32"]["status"] = 1;
$demo_content["realty_32"]["sold"] = 0;
$demo_content["realty_32"]["date_open"] = "2013-03-11 00:00:00";
$demo_content["realty_32"]["date_available"] = "2013-08-03 00:00:00";
$demo_content["realty_32"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_32"]["price"] = 250000;
$demo_content["realty_32"]["price_max"] = 280000;
$demo_content["realty_32"]["gid_currency"] = "USD";
$demo_content["realty_32"]["square"] = 2400;
$demo_content["realty_32"]["square"] = 2500;
$demo_content["realty_32"]["square_unit"] = "sqft";
$demo_content["realty_32"]["id_country"] = "Germany";
$demo_content["realty_32"]["id_region"] = "Land Berlin";
$demo_content["realty_32"]["id_city"] = "Berlin";
$demo_content["realty_32"]["zip"] = "10178";
$demo_content["realty_32"]["lat"] = "52.5191710";
$demo_content["realty_32"]["lon"] = "13.4060912";
$demo_content["realty_32"]["fe_construction_status_2"] = 2;
$demo_content["realty_32"]["fe_year_2"] = 1999;
$demo_content["realty_32"]["fe_foundation_2"] = 2;
$demo_content["realty_32"]["fe_revenue_2"] = 75;
$demo_content["realty_32"]["fe_cash_flow_2"] = 23000;
$demo_content["realty_32"]["fe_years_established_2"] = 15;
$demo_content["realty_32"]["fe_employees_2"] = 8;
$demo_content["realty_32"]["fe_reason_for_selling_2"] = "";
$demo_content["realty_32"]["headline"] = "Pride Of Ownership Shines Through This ' Move In Ready', Unique 1870'S Century Home.";
$demo_content["realty_32"]["fe_comments_2"] = "Separate Entrance To Home Office/Great Rm W/Walk Out. Washer/Gas Dryer, Elf's, Ceiling Fans, Blinds/Drapes, Fridge, Gas Stove, B/I D/W, Electronic Air Cleaner.";

$demo_content["realty_33"]["id"] = 33;
$demo_content["realty_33"]["id_user"] = "Kevin Johnson";
$demo_content["realty_33"]["id_type"] = "buy";
$demo_content["realty_33"]["id_category"] = "Lots/lands";
$demo_content["realty_33"]["property_type"] = "Farm";
$demo_content["realty_33"]["status"] = 1;
$demo_content["realty_33"]["sold"] = 0;
$demo_content["realty_33"]["date_open"] = "2013-05-25 00:00:00";
$demo_content["realty_33"]["date_available"] = "2013-12-22 00:00:00";
$demo_content["realty_33"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_33"]["price"] = 400000;
$demo_content["realty_33"]["price_max"] = 450000;
$demo_content["realty_33"]["gid_currency"] = "USD";
$demo_content["realty_33"]["square"] = 3000;
$demo_content["realty_33"]["square_max"] = 3200;
$demo_content["realty_33"]["square_unit"] = "sqft";
$demo_content["realty_33"]["id_country"] = "Spain";
$demo_content["realty_33"]["id_region"] = "Comunidad de Madrid";
$demo_content["realty_33"]["id_city"] = "Madrid";
$demo_content["realty_33"]["zip"] = "28006"; 
$demo_content["realty_33"]["lat"] = "40.4406477";
$demo_content["realty_33"]["lon"] = "-3.6821142";
$demo_content["realty_33"]["fe_irrigated_3"] = 0;
$demo_content["realty_33"]["fe_residence_3"] = 0;
$demo_content["realty_33"]["headline"] = "Serene & Picturesque 10 Acre Country Oasis! Fabulous Location! Mins From Newmarket! Newly Reno?d Gourmet Kit & Baths. Prof Landscaped.";
$demo_content["realty_33"]["fe_comments_3"] = "Designer Double Front Door, All Window Coverings, Light Fixtures, Kitchen W/Granite Cntrtops. All Appls! New Washer& Dryer. Professionally Finished Basement Apt W/Walk Up &Roughed In Kitchen.";

$demo_content["realty_34"]["id"] = 34;
$demo_content["realty_34"]["id_user"] = "Kevin Johnson";
$demo_content["realty_34"]["id_type"] = "buy";
$demo_content["realty_34"]["id_category"] = "Residential";
$demo_content["realty_34"]["property_type"] = "Duplex";
$demo_content["realty_34"]["status"] = 1;
$demo_content["realty_34"]["sold"] = 0;
$demo_content["realty_34"]["date_open"] = "2013-03-03 00:00:00";
$demo_content["realty_34"]["date_available"] = "2013-08-17 00:00:00";
$demo_content["realty_34"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_34"]["price"] = 200000;
$demo_content["realty_34"]["price_max"] = 230000;
$demo_content["realty_34"]["gid_currency"] = "USD";
$demo_content["realty_34"]["square"] = 2300;
$demo_content["realty_34"]["square_max"] = 2500;
$demo_content["realty_34"]["square_unit"] = "sqft";
$demo_content["realty_34"]["id_country"] = "Canada";
$demo_content["realty_34"]["id_region"] = "Ontario";
$demo_content["realty_34"]["id_city"] = "Toronto";
$demo_content["realty_34"]["zip"] = "M6K 1X1";
$demo_content["realty_34"]["lat"] = "43.6782743";
$demo_content["realty_34"]["lon"] = "-79.3435956";
$demo_content["realty_34"]["highlight_date_end"] = "2020-01-01 00:00";
$demo_content["realty_34"]["fe_construction_status_1"] = 1;
$demo_content["realty_34"]["fe_live_square_1"] = 2100;
$demo_content["realty_34"]["fe_bd_rooms_1"] = 3;
$demo_content["realty_34"]["fe_bth_rooms_1"] = 2;
$demo_content["realty_34"]["fe_garages_1"] = 0;
$demo_content["realty_34"]["fe_year_1"] = 2005;
$demo_content["realty_34"]["fe_foundation_1"] = 2;
$demo_content["realty_34"]["fe_floor_number_1"] = 10;
$demo_content["realty_34"]["fe_total_floors_1"] = 20;
$demo_content["realty_34"]["fe_distance_to_subway_1"] = 5;
$demo_content["realty_34"]["fe_furniture_1"] = 1;
$demo_content["realty_34"]["fe_accomodation_1_option_1"] = 1;
$demo_content["realty_34"]["fe_accomodation_1_option_2"] = 1;
$demo_content["realty_34"]["fe_accomodation_1_option_3"] = 1;
$demo_content["realty_34"]["fe_accomodation_1_option_4"] = 1;
$demo_content["realty_34"]["fe_accomodation_1_option_5"] = 1;
$demo_content["realty_34"]["fe_appliances_1_option_1"] = 1;
$demo_content["realty_34"]["fe_appliances_1_option_2"] = 1;
$demo_content["realty_34"]["fe_appliances_1_option_3"] = 1;
$demo_content["realty_34"]["fe_appliances_1_option_5"] = 1;
$demo_content["realty_34"]["fe_appliances_1_option_8"] = 1;
$demo_content["realty_34"]["fe_amenities_1_option_1"] = 1;
$demo_content["realty_34"]["fe_amenities_1_option_2"] = 1;
$demo_content["realty_34"]["fe_amenities_1_option_3"] = 1;
$demo_content["realty_34"]["fe_amenities_1_option_4"] = 1;
$demo_content["realty_34"]["fe_amenities_1_option_5"] = 1;
$demo_content["realty_34"]["fe_environment_1_option_1"] = 1;
$demo_content["realty_34"]["fe_environment_1_option_2"] = 1;
$demo_content["realty_34"]["fe_environment_1_option_3"] = 1;
$demo_content["realty_34"]["fe_environment_1_option_4"] = 1;
$demo_content["realty_34"]["fe_environment_1_option_5"] = 1;
$demo_content["realty_34"]["headline"] = "Rare Offering In Miller's Creek. This Tormina Built Brick Home Is Ideal For 1st Or 2nd Time Buyers. Open Concept, Sun Filled, Clean And Well Maintained.";
$demo_content["realty_34"]["fe_comments_1"] = "All Electrical Light Fixtures, All Window Coverings, Central Air, S/S Stove, S/S Fridge, S/S B/I Dishwasher, S/S Microwave";

$demo_content["realty_35"]["id"] = 35;
$demo_content["realty_35"]["id_user"] = "Pilot Group";
$demo_content["realty_35"]["id_type"] = "buy";
$demo_content["realty_35"]["id_category"] = "Commercial";
$demo_content["realty_35"]["property_type"] = "Shop";
$demo_content["realty_35"]["status"] = 1;
$demo_content["realty_35"]["sold"] = 0;
$demo_content["realty_35"]["date_open"] = "2013-02-23 00:00:00";
$demo_content["realty_35"]["date_available"] = "2013-09-12 00:00:00";
$demo_content["realty_35"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_35"]["price"] = 550000;
$demo_content["realty_35"]["price_max"] = 570000;
$demo_content["realty_35"]["gid_currency"] = "USD";
$demo_content["realty_35"]["square"] = 3000;
$demo_content["realty_35"]["square_max"] = 3500;
$demo_content["realty_35"]["square_unit"] = "sqft";
$demo_content["realty_35"]["id_country"] = "Russia";
$demo_content["realty_35"]["id_region"] = "Moscow";
$demo_content["realty_35"]["id_city"] = "Moscow";
$demo_content["realty_35"]["zip"] = "129223";
$demo_content["realty_35"]["lat"] = "55.8119629";
$demo_content["realty_35"]["lon"] = "37.6377724";
$demo_content["realty_35"]["fe_construction_status_2"] = 1;
$demo_content["realty_35"]["fe_year_2"] = 2008;
$demo_content["realty_35"]["fe_foundation_2"] = 1;
$demo_content["realty_35"]["fe_revenue_2"] = 45;
$demo_content["realty_35"]["fe_cash_flow_2"] = 100000;
$demo_content["realty_35"]["fe_years_established_2"] = 10;
$demo_content["realty_35"]["fe_employees_2"] = 15;
$demo_content["realty_35"]["fe_reason_for_selling_2"] = "";
$demo_content["realty_35"]["headline"] = "Designer Inspired Home E Of Ave Rd In Highly Desired Cricket Club! Lrg Bright Windows Are Signature Of This Beautiful Open Concept Home.";
$demo_content["realty_35"]["fe_comments_2"] = "Fam Rm Features Flr To Ceiling White Stone Wall W Modern S/S Fp & Tv Mounted Above. Master Bdrm Retreat W Custom Wall-Wall B/I Cabinets, Vault Ceiling & Spa-Like 5Pc Ensuite W Elegant Freestanding Tub & Designer Marble/Glass Shower Stall.";

$demo_content["realty_36"]["id"] = 36;
$demo_content["realty_36"]["id_user"] = "Lucy Price";
$demo_content["realty_36"]["id_type"] = "buy";
$demo_content["realty_36"]["id_category"] = "Lots/lands";
$demo_content["realty_36"]["property_type"] = "Pasture";
$demo_content["realty_36"]["status"] = 1;
$demo_content["realty_36"]["sold"] = 0;
$demo_content["realty_36"]["date_open"] = "2013-06-08 00:00:00";
$demo_content["realty_36"]["date_available"] = "2013-12-05 00:00:00";
$demo_content["realty_36"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_36"]["price"] = 250000;
$demo_content["realty_36"]["price_max"] = 260000;
$demo_content["realty_36"]["gid_currency"] = "USD";
$demo_content["realty_36"]["square"] = 2200;
$demo_content["realty_36"]["square_max"] = 2300;
$demo_content["realty_36"]["square_unit"] = "sqft";
$demo_content["realty_36"]["id_country"] = "United States";
$demo_content["realty_36"]["id_region"] = "New York";
$demo_content["realty_36"]["id_city"] = "New York City";
$demo_content["realty_36"]["zip"] = "10024";
$demo_content["realty_36"]["lat"] = "40.7859409";
$demo_content["realty_36"]["lon"] = "-73.9726396";
$demo_content["realty_36"]["fe_irrigated_3"] = 1;
$demo_content["realty_36"]["fe_residence_3"] = 0; 
$demo_content["realty_36"]["headline"] = "Design-Inspired Custom Home With A Flowing & Open Design & Outstanding Attention To Detail & Finish. Ideal For Great Family Living & Entertaining!";
$demo_content["realty_36"]["fe_comments_3"] = "Slab Granite Countertops,Island W Eating Bar,Eating Area Surrounded By Bright Windows,W/O To The Entertainment-Sized Deck.Fam Rm Open To Kitchen W B/I Wall-To-Wall Custom Cabinets Fireplace,Big Windows,Cornice Mldg,Hardwood,B/I Speakers.";

$demo_content["realty_37"]["id"] = 37;
$demo_content["realty_37"]["id_user"] = "Pilot Group";
$demo_content["realty_37"]["id_type"] = "buy";
$demo_content["realty_37"]["id_category"] = "Residential";
$demo_content["realty_37"]["property_type"] = "Single Family Home";
$demo_content["realty_37"]["status"] = 1;
$demo_content["realty_37"]["sold"] = 0;
$demo_content["realty_37"]["date_open"] = "2013-07-22 00:00:00";
$demo_content["realty_37"]["date_available"] = "2013-07-07 00:00:00";
$demo_content["realty_37"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_37"]["price"] = 300000;
$demo_content["realty_37"]["price_max"] = 340000;
$demo_content["realty_37"]["gid_currency"] = "USD";
$demo_content["realty_37"]["square"] = 2500;
$demo_content["realty_37"]["square_max"] = 2700;
$demo_content["realty_37"]["square_unit"] = "sqft";
$demo_content["realty_37"]["id_country"] = "United Kingdom";
$demo_content["realty_37"]["id_region"] = "Greater London";
$demo_content["realty_37"]["id_city"] = "London";
$demo_content["realty_37"]["zip"] = "SW1X 7RJ"; 
$demo_content["realty_37"]["lat"] = "51.5017097";
$demo_content["realty_37"]["lon"] = "-0.1657169";
$demo_content["realty_37"]["fe_construction_status_1"] = 1;
$demo_content["realty_37"]["fe_live_square_1"] = 2700;
$demo_content["realty_37"]["fe_bd_rooms_1"] = 3;
$demo_content["realty_37"]["fe_bth_rooms_1"] = 2;
$demo_content["realty_37"]["fe_garages_1"] = 1;
$demo_content["realty_37"]["fe_year_1"] = 2013;
$demo_content["realty_37"]["fe_foundation_1"] = 3;
$demo_content["realty_37"]["fe_floor_number_1"] = 8;
$demo_content["realty_37"]["fe_total_floors_1"] = 9;
$demo_content["realty_37"]["fe_distance_to_subway_1"] = 5;
$demo_content["realty_37"]["fe_furniture_1"] = 1;
$demo_content["realty_37"]["fe_accomodation_1_option_1"] = 1;
$demo_content["realty_37"]["fe_accomodation_1_option_2"] = 1;
$demo_content["realty_37"]["fe_accomodation_1_option_3"] = 1;
$demo_content["realty_37"]["fe_accomodation_1_option_4"] = 1;
$demo_content["realty_37"]["fe_accomodation_1_option_5"] = 1;
$demo_content["realty_37"]["fe_appliances_1_option_1"] = 1;
$demo_content["realty_37"]["fe_appliances_1_option_2"] = 1;
$demo_content["realty_37"]["fe_appliances_1_option_3"] = 1;
$demo_content["realty_37"]["fe_appliances_1_option_4"] = 1;
$demo_content["realty_37"]["fe_appliances_1_option_5"] = 1;
$demo_content["realty_37"]["fe_amenities_1_option_1"] = 1;
$demo_content["realty_37"]["fe_amenities_1_option_2"] = 1;
$demo_content["realty_37"]["fe_amenities_1_option_3"] = 1;
$demo_content["realty_37"]["fe_amenities_1_option_4"] = 1;
$demo_content["realty_37"]["fe_amenities_1_option_5"] = 1;
$demo_content["realty_37"]["fe_environment_1_option_1"] = 1;
$demo_content["realty_37"]["fe_environment_1_option_2"] = 1;
$demo_content["realty_37"]["fe_environment_1_option_3"] = 1;
$demo_content["realty_37"]["fe_environment_1_option_4"] = 1;
$demo_content["realty_37"]["fe_environment_1_option_5"] = 1;
$demo_content["realty_37"]["headline"] = "One Of This Neighbourhood 'S Best Homes - 3500 Sq Ft + - Situated In A Prime Location Close To Avenue Rd! This Home Has No Furnace!";
$demo_content["realty_37"]["fe_comments_1"] = "Made With The Finest Materials & Systems Including Vaulted Ceilings In All Bedrooms On 2nd Floor, A Walk-Thru Pantry With A 2nd Fridge Plus 2 Built-In Fridge Drawers & A Built-In Miele Coffee Maker, Large Marble";

$demo_content["realty_38"]["id"] = 38;
$demo_content["realty_38"]["id_user"] = "Tracy Dorhan";
$demo_content["realty_38"]["id_type"] = "buy";
$demo_content["realty_38"]["id_category"] = "Commercial";
$demo_content["realty_38"]["property_type"] = "Restaurant";
$demo_content["realty_38"]["status"] = 1;
$demo_content["realty_38"]["sold"] = 0;
$demo_content["realty_38"]["date_open"] = "2013-05-18 00:00:00";
$demo_content["realty_38"]["date_available"] = "2013-05-24 00:00:00";
$demo_content["realty_38"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_38"]["price"] = 240000;
$demo_content["realty_38"]["price_max"] = 250000;
$demo_content["realty_38"]["gid_currency"] = "USD";
$demo_content["realty_38"]["square"] = 2000;
$demo_content["realty_38"]["square_max"] = 2100;
$demo_content["realty_38"]["square_unit"] = "sqft";
$demo_content["realty_38"]["id_country"] = "France";
$demo_content["realty_38"]["id_region"] = "Région Île-de-France";
$demo_content["realty_38"]["id_city"] = "Paris";
$demo_content["realty_38"]["zip"] = "65301";
$demo_content["realty_38"]["lat"] = "48.8804610";
$demo_content["realty_38"]["lon"] = "2.2996525";
$demo_content["realty_38"]["fe_construction_status_2"] = 1;
$demo_content["realty_38"]["fe_year_2"] = 1978;
$demo_content["realty_38"]["fe_foundation_2"] = 5;
$demo_content["realty_38"]["fe_revenue_2"] = 75;
$demo_content["realty_38"]["fe_cash_flow_2"] = 450000;
$demo_content["realty_38"]["fe_years_established_2"] = 12;
$demo_content["realty_38"]["fe_employees_2"] = 120;
$demo_content["realty_38"]["fe_reason_for_selling_2"] = "";
$demo_content["realty_38"]["headline"] = "Gorgeous, Modern Totally Upgraded Family Home In A Prestigious Lakeside Community!";
$demo_content["realty_38"]["fe_comments_2"] = "Include All Kitchen Appliances Fridge, Stove, B/I Dishwasher,Washer, Dryer, All Electric Light Fixtures";

$demo_content["realty_39"]["id"] = 39;
$demo_content["realty_39"]["id_user"] = "Barry Lindon";
$demo_content["realty_39"]["id_type"] = "buy";
$demo_content["realty_39"]["id_category"] = "Lots/lands";
$demo_content["realty_39"]["property_type"] = "Timberland";
$demo_content["realty_39"]["status"] = 1;
$demo_content["realty_39"]["sold"] = 0;
$demo_content["realty_39"]["date_open"] = "2013-03-08 00:00:00";
$demo_content["realty_39"]["date_available"] = "2013-07-01 00:00:00";
$demo_content["realty_39"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_39"]["price"] = 310000;
$demo_content["realty_39"]["price_max"] = 315000;
$demo_content["realty_39"]["gid_currency"] = "USD";
$demo_content["realty_39"]["square"] = 2400;
$demo_content["realty_39"]["square_max"] = 2500;
$demo_content["realty_39"]["square_unit"] = "sqft";
$demo_content["realty_39"]["id_country"] = "Germany";
$demo_content["realty_39"]["id_region"] = "Land Berlin";
$demo_content["realty_39"]["id_city"] = "Berlin";
$demo_content["realty_39"]["zip"] = "06037";
$demo_content["realty_39"]["lat"] = "52.5191710";
$demo_content["realty_39"]["lon"] = "13.4060912";
$demo_content["realty_39"]["fe_irrigated_3"] = 1;
$demo_content["realty_39"]["fe_residence_3"] = 1;
$demo_content["realty_39"]["headline"] = "Open House Sunday 2-4!! Need A Separate Space For Older Children? This Home Has A Great Lower Level For All Their Needs!";
$demo_content["realty_39"]["fe_comments_3"] = "Hardwood Thru, Eat-In Kitchen Overlooking The Family Room Featuring A Gas Stove & French Doors Parking For 6 Cars In Drive! 1 Min Walk To St. James Church And Sep. School";

$demo_content["realty_40"]["id"] = 40;
$demo_content["realty_40"]["id_user"] = "Barry Lindon";
$demo_content["realty_40"]["id_type"] = "buy";
$demo_content["realty_40"]["id_category"] = "Residential";
$demo_content["realty_40"]["property_type"] = "Multi Family Home";
$demo_content["realty_40"]["status"] = 1;
$demo_content["realty_40"]["sold"] = 0;
$demo_content["realty_40"]["date_open"] = "2013-04-05 00:00:00";
$demo_content["realty_40"]["date_available"] = "2013-11-24 00:00:00";
$demo_content["realty_40"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_40"]["price"] = 600000;
$demo_content["realty_40"]["price_max"] = 700000;
$demo_content["realty_40"]["gid_currency"] = "USD";
$demo_content["realty_40"]["square"] = 3200;
$demo_content["realty_40"]["square_max"] = 3500;
$demo_content["realty_40"]["square_unit"] = "sqft";
$demo_content["realty_40"]["id_country"] = "Spain";
$demo_content["realty_40"]["id_region"] = "Comunidad de Madrid";
$demo_content["realty_40"]["id_city"] = "Madrid";
$demo_content["realty_40"]["zip"] = "28042";
$demo_content["realty_40"]["lat"] = "40.4641807";
$demo_content["realty_40"]["lon"] = "-3.6128114";
$demo_content["realty_40"]["highlight_date_end"] = "2020-01-01 00:00";
$demo_content["realty_40"]["fe_construction_status_1"] = 1;
$demo_content["realty_40"]["fe_live_square_1"] = 3400;
$demo_content["realty_40"]["fe_bd_rooms_1"] = 4;
$demo_content["realty_40"]["fe_bth_rooms_1"] = 2;
$demo_content["realty_40"]["fe_garages_1"] = 1;
$demo_content["realty_40"]["fe_year_1"] = 2013;
$demo_content["realty_40"]["fe_foundation_1"] = 3;
$demo_content["realty_40"]["fe_floor_number_1"] = 5;
$demo_content["realty_40"]["fe_total_floors_1"] = 10;
$demo_content["realty_40"]["fe_distance_to_subway_1"] = 5;
$demo_content["realty_40"]["fe_furniture_1"] = 1;
$demo_content["realty_40"]["fe_accomodation_1_option_1"] = 1;
$demo_content["realty_40"]["fe_accomodation_1_option_2"] = 1;
$demo_content["realty_40"]["fe_accomodation_1_option_3"] = 1;
$demo_content["realty_40"]["fe_accomodation_1_option_4"] = 1;
$demo_content["realty_40"]["fe_accomodation_1_option_5"] = 1;
$demo_content["realty_40"]["fe_appliances_1_option_1"] = 1;
$demo_content["realty_40"]["fe_appliances_1_option_2"] = 1;
$demo_content["realty_40"]["fe_appliances_1_option_3"] = 1;
$demo_content["realty_40"]["fe_appliances_1_option_4"] = 1;
$demo_content["realty_40"]["fe_appliances_1_option_5"] = 1;
$demo_content["realty_40"]["fe_amenities_1_option_1"] = 1;
$demo_content["realty_40"]["fe_amenities_1_option_2"] = 1;
$demo_content["realty_40"]["fe_amenities_1_option_3"] = 1;
$demo_content["realty_40"]["fe_amenities_1_option_4"] = 1;
$demo_content["realty_40"]["fe_amenities_1_option_5"] = 1;
$demo_content["realty_40"]["fe_environment_1_option_1"] = 1;
$demo_content["realty_40"]["fe_environment_1_option_2"] = 1;
$demo_content["realty_40"]["fe_environment_1_option_3"] = 1;
$demo_content["realty_40"]["fe_environment_1_option_4"] = 1;
$demo_content["realty_40"]["fe_environment_1_option_5"] = 1;
$demo_content["realty_40"]["headline"] = "Great 'Alderwood' Starter Home On A Quiet Cul-De-Sac Surrounded By Newly Developed Homes.";
$demo_content["realty_40"]["fe_comments_1"] = "All Light Fixtures,S/S Samsung Fridge,S/S Induction Cooktop,S/S Bosh D/W,Sharp B/I Micro W/Pullout Drawer, Cvac+Equip,Gb&E(Hi-Eff),Cac,Hwt(R).Bsmt Fridge&Stove&Micro,B/I Speakers On Mn,R/I For Alarm/Cam,Washer,Dryer.Gas Bbq Hookup At Rear.";

$demo_content["realty_41"]["id"] = 41;
$demo_content["realty_41"]["id_user"] = "Lucy Price";
$demo_content["realty_41"]["id_type"] = "buy";
$demo_content["realty_41"]["id_category"] = "Commercial";
$demo_content["realty_41"]["property_type"] = "Hotel";
$demo_content["realty_41"]["status"] = 1;
$demo_content["realty_41"]["sold"] = 0;
$demo_content["realty_41"]["date_open"] = "2020-01-01 00:00:00";
$demo_content["realty_41"]["date_available"] = "2013-08-16 00:00:00";
$demo_content["realty_41"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_41"]["price"] = 50000;
$demo_content["realty_41"]["price_max"] = 60000;
$demo_content["realty_41"]["gid_currency"] = "USD";
$demo_content["realty_41"]["square"] = 1000;
$demo_content["realty_41"]["square_max"] = 1200;
$demo_content["realty_41"]["square_unit"] = "sqft";
$demo_content["realty_41"]["id_country"] = "Canada";
$demo_content["realty_41"]["id_region"] = "Ontario";
$demo_content["realty_41"]["id_city"] = "Toronto";
$demo_content["realty_41"]["zip"] = "M6R 3A9";
$demo_content["realty_41"]["lat"] = "43.6542179";
$demo_content["realty_41"]["lon"] = "-79.4517318";
$demo_content["realty_41"]["fe_construction_status_2"] = 1;
$demo_content["realty_41"]["fe_year_2"] = 1985;
$demo_content["realty_41"]["fe_foundation_2"] = 1;
$demo_content["realty_41"]["fe_revenue_2"] = 58;
$demo_content["realty_41"]["fe_cash_flow_2"] = 62000;
$demo_content["realty_41"]["fe_years_established_2"] = 7;
$demo_content["realty_41"]["fe_employees_2"] = 15;
$demo_content["realty_41"]["fe_reason_for_selling_2"] = "";
$demo_content["realty_41"]["headline"] = "A Rare Find At Spring Water!Special One Of 2.5 Acres Forest Retreat!Tucked Away All Brick Bungalow On Lotsa Land.";
$demo_content["realty_41"]["fe_comments_2"] = "All Elfs,S/S Fridge,S/S Stove,S/S B/I Dishwasher,2 Washers& Dryer(One Stackable Set),Bsmnt Fridge,Stove,Dishwasher.Custom Blinds&California Shutters,Numerous Pot Lights.200 Amps.Cac, Gb&E,2 Jacuzzis.Gdo+2Remotes.Large Det.Stucco Garage!A10+";

$demo_content["realty_42"]["id"] = 42;
$demo_content["realty_42"]["id_user"] = "Lucy Price";
$demo_content["realty_42"]["id_type"] = "buy";
$demo_content["realty_42"]["id_category"] = "Lots/lands";
$demo_content["realty_42"]["property_type"] = "Undeveloped land";
$demo_content["realty_42"]["status"] = 1;
$demo_content["realty_42"]["sold"] = 0;
$demo_content["realty_42"]["date_open"] = "2013-02-23 00:00:00";
$demo_content["realty_42"]["date_available"] = "2013-12-08 00:00:00";
$demo_content["realty_42"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_42"]["price"] = 100000;
$demo_content["realty_42"]["price_max"] = 120000;
$demo_content["realty_42"]["gid_currency"] = "USD";
$demo_content["realty_42"]["square"] = 1800;
$demo_content["realty_42"]["square_max"] = 1900;
$demo_content["realty_42"]["square_unit"] = "sqft";
$demo_content["realty_42"]["id_country"] = "Russia";
$demo_content["realty_42"]["id_region"] = "Moscow";
$demo_content["realty_42"]["id_city"] = "Moscow";
$demo_content["realty_42"]["zip"] = "127083";
$demo_content["realty_42"]["lat"] = "55.7512419";
$demo_content["realty_42"]["lon"] = "37.6184217";
$demo_content["realty_42"]["fe_irrigated_3"] = 1;
$demo_content["realty_42"]["fe_residence_3"] = 0;
$demo_content["realty_42"]["headline"] = "Solid 2 Br. Bungalow With Seperate Side Entrance To 2 Br. In-Law Apt. Private Drive And Large Detached Garage.";
$demo_content["realty_42"]["fe_comments_3"] = "All Existing Electric Light Fixtures, 2 Fridges, 2 Stoves, Washer, Dryer, Gb&E (00), 37Cac, Hwt(O), Main Roof (01), Garge Roof (07), Updated Electrical Panel (06). Gdo+Remote Newer Side Door. Exclude Existing Sheers And Curtains.";

$demo_content["realty_43"]["id"] = 43;
$demo_content["realty_43"]["id_user"] = "Barbara Close";
$demo_content["realty_43"]["id_type"] = "buy";
$demo_content["realty_43"]["id_category"] = "Residential";
$demo_content["realty_43"]["property_type"] = "Condo";
$demo_content["realty_43"]["status"] = 1;
$demo_content["realty_43"]["sold"] = 0;
$demo_content["realty_43"]["date_open"] = "2013-11-01 00:00:00";
$demo_content["realty_43"]["date_available"] = "2013-09-13 00:00:00";
$demo_content["realty_43"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_43"]["price"] = 210000;
$demo_content["realty_43"]["price_max"] = 220000;
$demo_content["realty_43"]["gid_currency"] = "USD";
$demo_content["realty_43"]["square"] = 2000;
$demo_content["realty_43"]["square_max"] = 2500;
$demo_content["realty_43"]["square_unit"] = "sqft";
$demo_content["realty_43"]["id_country"] = "United States";
$demo_content["realty_43"]["id_region"] = "New York";
$demo_content["realty_43"]["id_city"] = "New York City";
$demo_content["realty_43"]["zip"] = "10003";
$demo_content["realty_43"]["lat"] = "40.7267917";
$demo_content["realty_43"]["lon"] = "-73.9887597";
$demo_content["realty_43"]["fe_construction_status_1"] = 1;
$demo_content["realty_43"]["fe_live_square_1"] = 2500;
$demo_content["realty_43"]["fe_bd_rooms_1"] = 3;
$demo_content["realty_43"]["fe_bth_rooms_1"] = 1;
$demo_content["realty_43"]["fe_garages_1"] = 1;
$demo_content["realty_43"]["fe_year_1"] = 2010;
$demo_content["realty_43"]["fe_foundation_1"] = 5;
$demo_content["realty_43"]["fe_floor_number_1"] = 3;
$demo_content["realty_43"]["fe_total_floors_1"] = 5;
$demo_content["realty_43"]["fe_distance_to_subway_1"] = 5;
$demo_content["realty_43"]["fe_furniture_1"] = 1;
$demo_content["realty_43"]["fe_accomodation_1_option_1"] = 1;
$demo_content["realty_43"]["fe_accomodation_1_option_2"] = 1;
$demo_content["realty_43"]["fe_accomodation_1_option_3"] = 1;
$demo_content["realty_43"]["fe_accomodation_1_option_4"] = 1;
$demo_content["realty_43"]["fe_accomodation_1_option_5"] = 1;
$demo_content["realty_43"]["fe_appliances_1_option_1"] = 1;
$demo_content["realty_43"]["fe_appliances_1_option_2"] = 1;
$demo_content["realty_43"]["fe_appliances_1_option_3"] = 1;
$demo_content["realty_43"]["fe_appliances_1_option_4"] = 1;
$demo_content["realty_43"]["fe_appliances_1_option_5"] = 1;
$demo_content["realty_43"]["fe_amenities_1_option_1"] = 1;
$demo_content["realty_43"]["fe_amenities_1_option_2"] = 1;
$demo_content["realty_43"]["fe_amenities_1_option_3"] = 1;
$demo_content["realty_43"]["fe_amenities_1_option_4"] = 1;
$demo_content["realty_43"]["fe_amenities_1_option_5"] = 1;
$demo_content["realty_43"]["fe_environment_1_option_1"] = 1;
$demo_content["realty_43"]["fe_environment_1_option_2"] = 1;
$demo_content["realty_43"]["fe_environment_1_option_3"] = 1;
$demo_content["realty_43"]["fe_environment_1_option_4"] = 1;
$demo_content["realty_43"]["fe_environment_1_option_5"] = 1;
$demo_content["realty_43"]["headline"] = "This Is It! Your Search Stops Here! Brand New With Over $65K Upgrades From Builder. Smooth Ceiling And Hardwood Floor Throughout.";
$demo_content["realty_43"]["fe_comments_1"] = "Front Porch,Double Garage,Oak Staircase.Walk To Park.Beautifully Landscaped Property,Private Yard With Fruite Trees.Very Well-Kept.Shows 10+. All Elf's Washer/Dryer, Stove, Dishwasher, Fridge,Blinds, Hwt(R),Gdo& 2Remotes,Electronic Air Cleaner,Cac,Cvac.";

$demo_content["realty_44"]["id"] = 44;
$demo_content["realty_44"]["id_user"] = "Coldwell Banker Dolphin Realty";
$demo_content["realty_44"]["id_type"] = "buy";
$demo_content["realty_44"]["id_category"] = "Commercial";
$demo_content["realty_44"]["property_type"] = "Sports facility";
$demo_content["realty_44"]["status"] = 1;
$demo_content["realty_44"]["sold"] = 0;
$demo_content["realty_44"]["date_open"] = "2013-02-04 00:00:00";
$demo_content["realty_44"]["date_available"] = "2013-04-14 00:00:00";
$demo_content["realty_44"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_44"]["price"] = 100000;
$demo_content["realty_44"]["price_max"] = 300000;
$demo_content["realty_44"]["gid_currency"] = "USD";
$demo_content["realty_44"]["square"] = 1500;
$demo_content["realty_44"]["square_max"] = 1700;
$demo_content["realty_44"]["square_unit"] = "sqft";
$demo_content["realty_44"]["id_country"] = "United Kingdom";
$demo_content["realty_44"]["id_region"] = "Greater London";
$demo_content["realty_44"]["id_city"] = "London";
$demo_content["realty_44"]["zip"] = "N20 0JB";
$demo_content["realty_44"]["lat"] = "51.6040656";
$demo_content["realty_44"]["lon"] = "-0.1794804";
$demo_content["realty_44"]["fe_construction_status_2"] = 1;
$demo_content["realty_44"]["fe_year_2"] = 2205;
$demo_content["realty_44"]["fe_foundation_2"] = 3;
$demo_content["realty_44"]["fe_revenue_2"] = 65;
$demo_content["realty_44"]["fe_cash_flow_2"] = 34000;
$demo_content["realty_44"]["fe_years_established_2"] = 6;
$demo_content["realty_44"]["fe_employees_2"] = 4;
$demo_content["realty_44"]["fe_reason_for_selling_2"] = "";
$demo_content["realty_44"]["headline"] = "Stunning,Immaculate!4 Bdrm Home In Sonoma Heights!Huge Kitchen Open Concept To Family Rm.";
$demo_content["realty_44"]["fe_comments_2"] = "New Central Vac, New Central Air, Appliance To Be Neg., Incl New Garage Door Opener, New Garage Doors, Everything Is Brand New. Basement All Drywalled Ready To Finish.";

$demo_content["realty_45"]["id"] = 45;
$demo_content["realty_45"]["id_user"] = "Ann Fox";
$demo_content["realty_45"]["id_type"] = "buy";
$demo_content["realty_45"]["id_category"] = "Lots/lands";
$demo_content["realty_45"]["property_type"] = "Other";
$demo_content["realty_45"]["status"] = 1;
$demo_content["realty_45"]["sold"] = 0;
$demo_content["realty_45"]["date_open"] = "2020-01-01 00:00:00";
$demo_content["realty_45"]["date_available"] = "2013-08-16 00:00:00";
$demo_content["realty_45"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_45"]["price"] = 400000;
$demo_content["realty_45"]["price_max"] = 450000;
$demo_content["realty_45"]["gid_currency"] = "USD";
$demo_content["realty_45"]["square"] = 3000;
$demo_content["realty_45"]["square_max"] = 3100;
$demo_content["realty_45"]["square_unit"] = "sqft";
$demo_content["realty_45"]["id_country"] = "France";
$demo_content["realty_45"]["id_region"] = "Région Île-de-France";
$demo_content["realty_45"]["id_city"] = "Paris";
$demo_content["realty_45"]["zip"] = "75007";
$demo_content["realty_45"]["lat"] = "48.8569775";
$demo_content["realty_45"]["lon"] = "2.3296994";
$demo_content["realty_45"]["fe_irrigated_3"] = 1;
$demo_content["realty_45"]["fe_residence_3"] = 1;
$demo_content["realty_45"]["headline"] = "A Beautiful Approx 2500Sqft 4+2 Bedroom, Detached All Brick House With Seperate Side Entrance To Legal 2 Br Spacious Basement Apartment, Freshly Painted.";
$demo_content["realty_45"]["fe_comments_3"] = "All Window Coverings, All Elfs (Exl: Ceiling Fan In Hall)Fridge, Stove, D/W, Washer, Dryer, Custom Sliding Glass Dr Btwn Kitchen/Dining Room, Gas Bbq, Garage Dr Openers; Cac, Cvac. Exl: Small Washer In Laundry Rm; Microwave. Shed Under Deck.";

$demo_content["realty_46"]["id"] = 46;
$demo_content["realty_46"]["id_user"] = "Ann Fox";
$demo_content["realty_46"]["id_type"] = "buy";
$demo_content["realty_46"]["id_category"] = "Residential";
$demo_content["realty_46"]["property_type"] = "Townhouse";
$demo_content["realty_46"]["status"] = 1;
$demo_content["realty_46"]["sold"] = 0;
$demo_content["realty_46"]["date_open"] = "2013-01-07 00:00:00";
$demo_content["realty_46"]["date_available"] = "2013-09-08 00:00:00";
$demo_content["realty_46"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_46"]["price"] = 200000;
$demo_content["realty_46"]["price_max"] = 230000;
$demo_content["realty_46"]["gid_currency"] = "USD";
$demo_content["realty_46"]["square"] = 1900;
$demo_content["realty_46"]["square_max"] = 2300;
$demo_content["realty_46"]["square_unit"] = "sqft";
$demo_content["realty_46"]["id_country"] = "Germany";
$demo_content["realty_46"]["id_region"] = "Land Berlin";
$demo_content["realty_46"]["id_city"] = "Berlin";
$demo_content["realty_46"]["zip"] = "13353";
$demo_content["realty_46"]["lat"] = "52.6392527";
$demo_content["realty_46"]["lon"] = "13.3666079";
$demo_content["realty_46"]["fe_construction_status_1"] = 1;
$demo_content["realty_46"]["fe_live_square_1"] = 2000;
$demo_content["realty_46"]["fe_bd_rooms_1"] = 2;
$demo_content["realty_46"]["fe_bth_rooms_1"] = 1;
$demo_content["realty_46"]["fe_garages_1"] = 0;
$demo_content["realty_46"]["fe_year_1"] = 2010;
$demo_content["realty_46"]["fe_foundation_1"] = 5;
$demo_content["realty_46"]["fe_floor_number_1"] = 5;
$demo_content["realty_46"]["fe_total_floors_1"] = 9;
$demo_content["realty_46"]["fe_distance_to_subway_1"] = 5;
$demo_content["realty_46"]["fe_furniture_1"] = 1;
$demo_content["realty_46"]["fe_accomodation_1_option_1"] = 1;
$demo_content["realty_46"]["fe_accomodation_1_option_2"] = 1;
$demo_content["realty_46"]["fe_accomodation_1_option_3"] = 1;
$demo_content["realty_46"]["fe_accomodation_1_option_4"] = 1;
$demo_content["realty_46"]["fe_accomodation_1_option_5"] = 1;
$demo_content["realty_46"]["fe_appliances_1_option_1"] = 1;
$demo_content["realty_46"]["fe_appliances_1_option_2"] = 1;
$demo_content["realty_46"]["fe_appliances_1_option_3"] = 1;
$demo_content["realty_46"]["fe_appliances_1_option_4"] = 1;
$demo_content["realty_46"]["fe_appliances_1_option_5"] = 1;
$demo_content["realty_46"]["fe_amenities_1_option_1"] = 1;
$demo_content["realty_46"]["fe_amenities_1_option_2"] = 1;
$demo_content["realty_46"]["fe_amenities_1_option_3"] = 1;
$demo_content["realty_46"]["fe_amenities_1_option_4"] = 1;
$demo_content["realty_46"]["fe_amenities_1_option_5"] = 1;
$demo_content["realty_46"]["fe_environment_1_option_1"] = 1;
$demo_content["realty_46"]["fe_environment_1_option_2"] = 1;
$demo_content["realty_46"]["fe_environment_1_option_3"] = 1;
$demo_content["realty_46"]["fe_environment_1_option_4"] = 1;
$demo_content["realty_46"]["fe_environment_1_option_5"] = 1;
$demo_content["realty_46"]["headline"] = "Beautiful 3 Bdrm Detached Home Located On A Quiet Court In Heart Lake Community!";
$demo_content["realty_46"]["fe_comments_1"] = "Fridge, Stove, Dishwasher, B/I Microwave, Front Load Washer, Front Load Dryer, All Elf's, Blinds, Garage Door Opener With Remote And Cac";

$demo_content["realty_47"]["id"] = 47;
$demo_content["realty_47"]["id_user"] = "The Gouglas Realty";
$demo_content["realty_47"]["id_type"] = "buy";
$demo_content["realty_47"]["id_category"] = "Commercial";
$demo_content["realty_47"]["property_type"] = "Warehouse";
$demo_content["realty_47"]["status"] = 1;
$demo_content["realty_47"]["sold"] = 0;
$demo_content["realty_47"]["date_open"] = "2013-04-15 00:00:00";
$demo_content["realty_47"]["date_available"] = "2013-11-04 00:00:00";
$demo_content["realty_47"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_47"]["price"] = 1000000;
$demo_content["realty_47"]["price_max"] = 1500000;
$demo_content["realty_47"]["gid_currency"] = "USD";
$demo_content["realty_47"]["square"] = 4000;
$demo_content["realty_47"]["square_max"] = 4200;
$demo_content["realty_47"]["square_unit"] = "sqft";
$demo_content["realty_47"]["id_country"] = "Spain";
$demo_content["realty_47"]["id_region"] = "Comunidad de Madrid";
$demo_content["realty_47"]["id_city"] = "Madrid";
$demo_content["realty_47"]["zip"] = "28014";
$demo_content["realty_47"]["lat"] = "40.4157127";
$demo_content["realty_47"]["lon"] = "-3.6926108";
$demo_content["realty_47"]["fe_construction_status_2"] = 1;
$demo_content["realty_47"]["fe_year_2"] = 2001;
$demo_content["realty_47"]["fe_foundation_2"] = 2;
$demo_content["realty_47"]["fe_revenue_2"] = 95;
$demo_content["realty_47"]["fe_cash_flow_2"] = 67000;
$demo_content["realty_47"]["fe_years_established_2"] = 10;
$demo_content["realty_47"]["fe_employees_2"] = 7;
$demo_content["realty_47"]["fe_reason_for_selling_2"] = "";
$demo_content["realty_47"]["headline"] = "Little Gem On Large Corner Lot In High Demand West Brampton. Near Downtown, Transit, Gage Park And More.";
$demo_content["realty_47"]["fe_comments_2"] = "All Appliances, Elf's, Existing Washer/Dryer/Fridge/Stove. New Front Roof, Newer Broadloom, New Hardwood On Main Floor And Newer 2nd Floor Bath And New Sewer Pipes Complete This Family Home.";

$demo_content["realty_48"]["id"] = 48;
$demo_content["realty_48"]["id_user"] = "The Gouglas Realty";
$demo_content["realty_48"]["id_type"] = "buy";
$demo_content["realty_48"]["id_category"] = "Lots/lands";
$demo_content["realty_48"]["property_type"] = "Farm";
$demo_content["realty_48"]["status"] = 1;
$demo_content["realty_48"]["sold"] = 0;
$demo_content["realty_48"]["date_open"] = "2013-08-01 00:00:00";
$demo_content["realty_48"]["date_available"] = "2014-01-14 00:00:00";
$demo_content["realty_48"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_48"]["price"] = 300000;
$demo_content["realty_48"]["price_max"] = 310000;
$demo_content["realty_48"]["gid_currency"] = "USD";
$demo_content["realty_48"]["square"] = 2300;
$demo_content["realty_48"]["square_max"] = 2500;
$demo_content["realty_48"]["square_unit"] = "sqft";
$demo_content["realty_48"]["id_country"] = "United States";
$demo_content["realty_48"]["id_region"] = "New York";
$demo_content["realty_48"]["id_city"] = "New York City";
$demo_content["realty_48"]["zip"] = "10281";
$demo_content["realty_48"]["lat"] = "40.7113747";
$demo_content["realty_48"]["lon"] = "-74.0156618"; 
$demo_content["realty_48"]["fe_irrigated_3"] = 0;
$demo_content["realty_48"]["fe_residence_3"] = 0;
$demo_content["realty_48"]["headline"] = "Unique &Amazing Property In Huntsville,Ontario's Hottest Cottage Country Vacation Spot";
$demo_content["realty_48"]["fe_comments_3"] = "Over 100 Pics-See Virtual Tour**Value Not Only In Cap Rate But In High Quality Buildings W/Unique Lot&Location.";

$demo_content["realty_49"]["id"] = 49;
$demo_content["realty_49"]["id_user"] = "Cornell Commons";
$demo_content["realty_49"]["id_type"] = "buy";
$demo_content["realty_49"]["id_category"] = "Residential";
$demo_content["realty_49"]["property_type"] = "Mobile home";
$demo_content["realty_49"]["status"] = 1;
$demo_content["realty_49"]["sold"] = 0;
$demo_content["realty_49"]["date_open"] = "2013-08-02 00:00:00";
$demo_content["realty_49"]["date_available"] = "2013-08-18 00:00:00";
$demo_content["realty_49"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_49"]["price"] = 80000;
$demo_content["realty_49"]["price_max"] = 85000;
$demo_content["realty_49"]["gid_currency"] = "USD";
$demo_content["realty_49"]["square"] = 900;
$demo_content["realty_49"]["square_max"] = 1000;
$demo_content["realty_49"]["square_unit"] = "sqft";
$demo_content["realty_49"]["id_country"] = "Canada";
$demo_content["realty_49"]["id_region"] = "Ontario";
$demo_content["realty_49"]["id_city"] = "Toronto";
$demo_content["realty_49"]["zip"] = "M6K 3S1";
$demo_content["realty_49"]["lat"] = "43.6399519";
$demo_content["realty_49"]["lon"] = "-79.4149071"; 
$demo_content["realty_49"]["fe_construction_status_1"] = 1;
$demo_content["realty_49"]["fe_live_square_1"] = 900;
$demo_content["realty_49"]["fe_bd_rooms_1"] = 1;
$demo_content["realty_49"]["fe_bth_rooms_1"] = 1;
$demo_content["realty_49"]["fe_garages_1"] = 0;
$demo_content["realty_49"]["fe_year_1"] = 2002;
$demo_content["realty_49"]["fe_foundation_1"] = 5;
$demo_content["realty_49"]["fe_floor_number_1"] = 5;
$demo_content["realty_49"]["fe_total_floors_1"] = 9;
$demo_content["realty_49"]["fe_distance_to_subway_1"] = 5;
$demo_content["realty_49"]["fe_furniture_1"] = 1;
$demo_content["realty_49"]["fe_accomodation_1_option_1"] = 1;
$demo_content["realty_49"]["fe_accomodation_1_option_2"] = 1;
$demo_content["realty_49"]["fe_accomodation_1_option_3"] = 1;
$demo_content["realty_49"]["fe_accomodation_1_option_4"] = 1;
$demo_content["realty_49"]["fe_accomodation_1_option_5"] = 1;
$demo_content["realty_49"]["fe_appliances_1_option_1"] = 1;
$demo_content["realty_49"]["fe_appliances_1_option_2"] = 1;
$demo_content["realty_49"]["fe_appliances_1_option_3"] = 1;
$demo_content["realty_49"]["fe_appliances_1_option_4"] = 1;
$demo_content["realty_49"]["fe_appliances_1_option_5"] = 1;
$demo_content["realty_49"]["fe_amenities_1_option_1"] = 1;
$demo_content["realty_49"]["fe_amenities_1_option_2"] = 1;
$demo_content["realty_49"]["fe_amenities_1_option_3"] = 1;
$demo_content["realty_49"]["fe_amenities_1_option_4"] = 1;
$demo_content["realty_49"]["fe_amenities_1_option_5"] = 1;
$demo_content["realty_49"]["fe_environment_1_option_1"] = 1;
$demo_content["realty_49"]["fe_environment_1_option_2"] = 1;
$demo_content["realty_49"]["fe_environment_1_option_3"] = 1;
$demo_content["realty_49"]["fe_environment_1_option_4"] = 1;
$demo_content["realty_49"]["fe_environment_1_option_5"] = 1;
$demo_content["realty_49"]["headline"] = "Environment With Carefully Selected Building Materials & Finishes, For Owners With Health Concerns, Ie., Asthma Or Allergies.";
$demo_content["realty_49"]["fe_comments_1"] = "Elf's, Custom Window Cvrgs, C/Air,Fridge, Two Oven Stove, Washer & Dryer, Gdr Opnr, 'Centralux' Vac Traps 99.97% Of Contaminants, R/I Bath, Hwt R.";

$demo_content["realty_50"]["id"] = 50;
$demo_content["realty_50"]["id_user"] = "Cornell Commons";
$demo_content["realty_50"]["id_type"] = "buy";
$demo_content["realty_50"]["id_category"] = "Commercial";
$demo_content["realty_50"]["property_type"] = "Manufactured";
$demo_content["realty_50"]["status"] = 1;
$demo_content["realty_50"]["sold"] = 0;
$demo_content["realty_50"]["date_open"] = "2013-07-11 00:00:00";
$demo_content["realty_50"]["date_available"] = "2013-12-04 00:00:00";
$demo_content["realty_50"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_50"]["price"] = 150000;
$demo_content["realty_50"]["price_max"] = 160000;
$demo_content["realty_50"]["gid_currency"] = "USD";
$demo_content["realty_50"]["square"] = 1200;
$demo_content["realty_50"]["square_max"] = 1500;
$demo_content["realty_50"]["square_unit"] = "sqft";
$demo_content["realty_50"]["id_country"] = "Russia";
$demo_content["realty_50"]["id_region"] = "Moscow";
$demo_content["realty_50"]["id_city"] = "Moscow";
$demo_content["realty_50"]["zip"] = "103132";
$demo_content["realty_50"]["lat"] = "55.7564857";
$demo_content["realty_50"]["lon"] = "37.6294582"; 
$demo_content["realty_50"]["fe_construction_status_2"] = 2;
$demo_content["realty_50"]["fe_year_2"] = 1996;
$demo_content["realty_50"]["fe_foundation_2"] = 1;
$demo_content["realty_50"]["fe_revenue_2"] = 45;
$demo_content["realty_50"]["fe_cash_flow_2"] = 70000;
$demo_content["realty_50"]["fe_years_established_2"] = 14;
$demo_content["realty_50"]["fe_employees_2"] = 20;
$demo_content["realty_50"]["fe_reason_for_selling_2"] = "";
$demo_content["realty_50"]["headline"] = "This Beauty Nestled Beside A Park Is Centrally Located On A Quiet Crescent And Near All Amenities Including University/College!";
$demo_content["realty_50"]["fe_comments_2"] = "Fridge, Gas Stove, B/I Dishwasher, L.G Washer (1.5 Years Old) Whirlpool Dryer, Furnace 2010 (Duct Cleaning 2013), Roof (New), Pool Filter, Gdo, Elf, Gas Fireplace, Ceiling Fans, Cvac + Equip(Excl:Curtains In Master & 2nd Bdrm)";*/

$demo_content["realty_51"]["id"] = 51;
$demo_content["realty_51"]["id_user"] = "Pilot Group";
$demo_content["realty_51"]["id_type"] = "rent";
$demo_content["realty_51"]["id_category"] = "Lots/lands";
$demo_content["realty_51"]["property_type"] = "Pasture";
$demo_content["realty_51"]["status"] = 1;
$demo_content["realty_51"]["sold"] = 0;
$demo_content["realty_51"]["date_open"] = "2015-08-21 00:00:00";
$demo_content["realty_51"]["date_available"] = "2015-11-08 00:00:00";
$demo_content["realty_51"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_51"]["price"] = 15;
$demo_content["realty_51"]["gid_currency"] = "USD";
$demo_content["realty_51"]["price_period"] = 1;
$demo_content["realty_51"]["price_type"] = 1;
$demo_content["realty_51"]["square"] = 1200;
$demo_content["realty_51"]["square_unit"] = "sqft";
$demo_content["realty_51"]["address"] = "1 New Orchard Road Armonk";
$demo_content["realty_51"]["id_country"] = "United States";
$demo_content["realty_51"]["id_region"] = "New York";
$demo_content["realty_51"]["id_city"] = "New York City";
$demo_content["realty_51"]["zip"] = "10504";
$demo_content["realty_51"]["lat"] = "41.1071760";
$demo_content["realty_51"]["lon"] = "-73.7214850"; 
$demo_content["realty_51"]["logo_image"] = "c01fb8ab6f.jpg";
$demo_content["realty_51"]["fe_irrigated_6"] = 1;
$demo_content["realty_51"]["fe_residence_6"] = 0;
$demo_content["realty_51"]["headline"] = "Do Not Miss Out! Immaculate Semi On Cres In Highly Sought After Neighborhood. Open Concepts. Oak Stairs Main Floor Parque.";
$demo_content["realty_51"]["fe_comments_6"] = "Stove , Fridge, B/I Dishwasher,B/I Microwave Gdo,Washer And Dryer,Cac,All Elf's,All Window Coverings.Basement Appliances.";

$demo_content["realty_52"]["id"] = 52;
$demo_content["realty_52"]["id_user"] = "Pilot Group";
$demo_content["realty_52"]["id_type"] = "rent";
$demo_content["realty_52"]["id_category"] = "Residential";
$demo_content["realty_52"]["property_type"] = "Other";
$demo_content["realty_52"]["status"] = 1;
$demo_content["realty_52"]["sold"] = 0;
$demo_content["realty_52"]["date_open"] = "2016-01-01 00:00:00";
$demo_content["realty_52"]["date_available"] = "2016-01-01 00:00:00";
$demo_content["realty_52"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_52"]["price"] = 130;
$demo_content["realty_52"]["gid_currency"] = "USD";
$demo_content["realty_52"]["price_period"] = 2;
$demo_content["realty_52"]["price_type"] = 1;
$demo_content["realty_52"]["square"] = 600;
$demo_content["realty_52"]["square_unit"] = "sqft";
$demo_content["realty_52"]["address"] = "8, Society Building";
$demo_content["realty_52"]["id_country"] = "United Kingdom";
$demo_content["realty_52"]["id_region"] = "Greater London";
$demo_content["realty_52"]["id_city"] = "London";
$demo_content["realty_52"]["zip"] = "N1 9RL";
$demo_content["realty_52"]["lat"] = "51.5078343";
$demo_content["realty_52"]["lon"] = "-0.1254040"; 
$demo_content["realty_52"]["logo_image"] = "1fd4c1b3a4.jpg";
$demo_content["realty_52"]["fe_check_in_4"] = "14:00";
$demo_content["realty_52"]["fe_check_out_4"] = "10:00";
$demo_content["realty_52"]["fe_room_type_4"] = "Single room";
$demo_content["realty_52"]["fe_guests_4"] = "2";
$demo_content["realty_52"]["fe_placement_4"] = "Отдельная комната";
$demo_content["realty_52"]["fe_live_square_4"] = 3600;
$demo_content["realty_52"]["fe_bd_rooms_4"] = 3;
$demo_content["realty_52"]["fe_bth_rooms_4"] = 1;
$demo_content["realty_52"]["fe_garages_4"] = 1;
$demo_content["realty_52"]["fe_year_4"] = 1980;
$demo_content["realty_52"]["fe_floor_number_4"] = 2;
$demo_content["realty_52"]["fe_total_floors_4"] = 9;
$demo_content["realty_52"]["fe_distance_to_subway_4"] = 5;
$demo_content["realty_52"]["fe_furniture_4"] = 1;
$demo_content["realty_52"]["fe_accomodation_4_option_1"] = 1;
$demo_content["realty_52"]["fe_accomodation_4_option_2"] = 1;
$demo_content["realty_52"]["fe_accomodation_4_option_3"] = 1;
$demo_content["realty_52"]["fe_accomodation_4_option_5"] = 1;
$demo_content["realty_52"]["fe_accomodation_4_option_9"] = 1;
$demo_content["realty_52"]["fe_appliances_4_option_1"] = 1;
$demo_content["realty_52"]["fe_appliances_4_option_2"] = 1;
$demo_content["realty_52"]["fe_appliances_4_option_3"] = 1;
$demo_content["realty_52"]["fe_appliances_4_option_5"] = 1;
$demo_content["realty_52"]["fe_appliances_4_option_9"] = 1;
$demo_content["realty_52"]["fe_amenities_4_option_1"] = 1;
$demo_content["realty_52"]["fe_amenities_4_option_2"] = 1;
$demo_content["realty_52"]["fe_amenities_4_option_3"] = 1;
$demo_content["realty_52"]["fe_amenities_4_option_4"] = 1;
$demo_content["realty_52"]["fe_amenities_4_option_5"] = 1;
$demo_content["realty_52"]["fe_environment_4_option_1"] = 1;
$demo_content["realty_52"]["fe_environment_4_option_2"] = 1;
$demo_content["realty_52"]["fe_environment_4_option_3"] = 1;
$demo_content["realty_52"]["fe_environment_4_option_4"] = 1;
$demo_content["realty_52"]["fe_environment_4_option_5"] = 1;
$demo_content["realty_52"]["headline"] = "Brand New Remington Built Home Located In One Of The Most Sought After Neighborhoods In South Georgetown.";
$demo_content["realty_52"]["fe_comments_4"] = "2 Masters Bedroom One With Huge 5 Piece Ensuite, Soaker Tub Plus 3 Nice Size Bedrooms, Elf's, Stainless Steel Fridge,B/I Oven,Stove,B/I Dishwasher,Washer And Dryer.";

$demo_content["realty_53"]["id"] = 53;
$demo_content["realty_53"]["id_user"] = "Pilot Group";
$demo_content["realty_53"]["id_type"] = "rent";
$demo_content["realty_53"]["id_category"] = "Commercial";
$demo_content["realty_53"]["property_type"] = "Other";
$demo_content["realty_53"]["status"] = 1;
$demo_content["realty_53"]["sold"] = 0;
$demo_content["realty_53"]["date_open"] = "2014-05-08 00:00:00";
$demo_content["realty_53"]["date_available"] = "2014-06-01 00:00:00";
$demo_content["realty_53"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_53"]["price"] = 20;
$demo_content["realty_53"]["price_reduced"] = 18;
$demo_content["realty_53"]["gid_currency"] = "USD";
$demo_content["realty_53"]["price_period"] = 1;
$demo_content["realty_53"]["price_type"] = 2;
$demo_content["realty_53"]["square"] = 600;
$demo_content["realty_53"]["square_unit"] = "sqft";
$demo_content["realty_53"]["address"] = "4, rue Jean Rey";
$demo_content["realty_53"]["id_country"] = "France";
$demo_content["realty_53"]["id_region"] = "Région Île-de-France";
$demo_content["realty_53"]["id_city"] = "Paris";
$demo_content["realty_53"]["zip"] = "75724";
$demo_content["realty_53"]["lat"] = "48.8553125";
$demo_content["realty_53"]["lon"] = "2.2899172"; 
$demo_content["realty_53"]["logo_image"] = "dfb5e2cda2.jpg";
$demo_content["realty_53"]["fe_year_5"] = 1994;
$demo_content["realty_53"]["fe_revenue_5"] = 65;
$demo_content["realty_53"]["fe_cash_flow_5"] = 10000;
$demo_content["realty_53"]["fe_years_established_5"] = 10;
$demo_content["realty_53"]["fe_employees_5"] = 5;
$demo_content["realty_53"]["fe_reason_for_selling_5"] = "";
$demo_content["realty_53"]["headline"] = "Markland Wood Two Storey 5+1 Bdrm/4Bath On Golf Valley Lane Overlooking 3rd. Fairway. 60X 144' Lot.";
$demo_content["realty_53"]["fe_comments_5"] = "Existing :Fridge, Stove, Washer, Dryer, B/I Dishwasher, All Light Fixtures, All Windows Covering, Central Air Conditioning, Great Sized Backyard And No Neighbours Behind. This Home Is A Must See.";

$demo_content["realty_54"]["id"] = 54;
$demo_content["realty_54"]["id_user"] = "Pilot Group";
$demo_content["realty_54"]["id_type"] = "rent";
$demo_content["realty_54"]["id_category"] = "Lots/lands";
$demo_content["realty_54"]["property_type"] = "Timberland";
$demo_content["realty_54"]["status"] = 1;
$demo_content["realty_54"]["sold"] = 0;
$demo_content["realty_54"]["date_open"] = "2014-08-02 00:00:00";
$demo_content["realty_54"]["date_available"] = "2014-01-01 00:00:00";
$demo_content["realty_54"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_54"]["price"] = 30;
$demo_content["realty_54"]["gid_currency"] = "USD";
$demo_content["realty_54"]["price_period"] = 1;
$demo_content["realty_54"]["price_type"] = 2;
$demo_content["realty_54"]["square"] = 1000;
$demo_content["realty_54"]["square_unit"] = "sqft";
$demo_content["realty_54"]["address"] = "25 Budapesterstrasse";
$demo_content["realty_54"]["id_country"] = "Germany";
$demo_content["realty_54"]["id_region"] = "Land Berlin";
$demo_content["realty_54"]["id_city"] = "Berlin";
$demo_content["realty_54"]["zip"] = "10787";
$demo_content["realty_54"]["lat"] = "52.6349305";
$demo_content["realty_54"]["lon"] = "13.3308838"; 
$demo_content["realty_54"]["logo_image"] = "f984651af3.jpg";
$demo_content["realty_54"]["fe_irrigated_6"] = 1;
$demo_content["realty_54"]["fe_residence_6"] = 1;
$demo_content["realty_54"]["headline"] = "Located In A Great Neighbourhood In Erin Mills. This Home Was Recently Renovated With New Flooring, Doors, Baseboards.";
$demo_content["realty_54"]["fe_comments_6"] = "Gas Stove, Fridge, Dishwasher, Washer, Dryer, Central Air And All Window Covering.";

$demo_content["realty_55"]["id"] = 55;
$demo_content["realty_55"]["id_user"] = "Pilot Group";
$demo_content["realty_55"]["id_type"] = "rent";
$demo_content["realty_55"]["id_category"] = "Residential";
$demo_content["realty_55"]["property_type"] = "Apartment";
$demo_content["realty_55"]["status"] = 1;
$demo_content["realty_55"]["sold"] = 0;
$demo_content["realty_55"]["date_open"] = "2014-08-02 00:00:00";
$demo_content["realty_55"]["date_available"] = "2014-01-01 00:00:00";
$demo_content["realty_55"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_55"]["price"] = 30;
$demo_content["realty_55"]["gid_currency"] = "USD";
$demo_content["realty_55"]["price_period"] = 1;
$demo_content["realty_55"]["price_type"] = 2;
$demo_content["realty_55"]["square"] = 1000;
$demo_content["realty_55"]["square_unit"] = "sqft";
$demo_content["realty_55"]["address"] = "80 Apartado";
$demo_content["realty_55"]["id_country"] = "Spain";
$demo_content["realty_55"]["id_region"] = "Comunidad de Madrid";
$demo_content["realty_55"]["id_city"] = "Madrid";
$demo_content["realty_55"]["zip"] = "28080";
$demo_content["realty_55"]["lat"] = "40.4167754";
$demo_content["realty_55"]["lon"] = "-3.7037902"; 
$demo_content["realty_55"]["logo_image"] = "01366086a8.jpg";
$demo_content["realty_55"]["fe_check_in_4"] = "14:00";
$demo_content["realty_55"]["fe_check_out_4"] = "10:00";
$demo_content["realty_55"]["fe_room_type_4"] = "Single room";
$demo_content["realty_55"]["fe_guests_4"] = "3";
$demo_content["realty_55"]["fe_placement_4"] = 3;
$demo_content["realty_55"]["fe_live_square_4"] = 1600;
$demo_content["realty_55"]["fe_bd_rooms_4"] = 2;
$demo_content["realty_55"]["fe_bth_rooms_4"] = 1;
$demo_content["realty_55"]["fe_garages_4"] = 0;
$demo_content["realty_55"]["fe_year_4"] = 1964;
$demo_content["realty_55"]["fe_floor_number_4"] = 4;
$demo_content["realty_55"]["fe_total_floors_4"] = 5;
$demo_content["realty_55"]["fe_distance_to_subway_4"] = 5;
$demo_content["realty_55"]["fe_furniture_4"] = 1;
$demo_content["realty_55"]["fe_accomodation_4_option_1"] = 1;
$demo_content["realty_55"]["fe_accomodation_4_option_8"] = 1;
$demo_content["realty_55"]["fe_accomodation_4_option_15"] = 1;
$demo_content["realty_55"]["fe_accomodation_4_option_19"] = 1;
$demo_content["realty_55"]["fe_accomodation_4_option_24"] = 1;
$demo_content["realty_55"]["fe_appliances_4_option_1"] = 1;
$demo_content["realty_55"]["fe_appliances_4_option_7"] = 1;
$demo_content["realty_55"]["fe_appliances_4_option_11"] = 1;
$demo_content["realty_55"]["fe_appliances_4_option_12"] = 1;
$demo_content["realty_55"]["fe_appliances_4_option_17"] = 1;
$demo_content["realty_55"]["fe_amenities_4_option_1"] = 1;
$demo_content["realty_55"]["fe_amenities_4_option_2"] = 1;
$demo_content["realty_55"]["fe_amenities_4_option_3"] = 1;
$demo_content["realty_55"]["fe_amenities_4_option_4"] = 1;
$demo_content["realty_55"]["fe_amenities_4_option_5"] = 1;
$demo_content["realty_55"]["fe_environment_4_option_1"] = 1;
$demo_content["realty_55"]["fe_environment_4_option_2"] = 1;
$demo_content["realty_55"]["fe_environment_4_option_3"] = 1;
$demo_content["realty_55"]["fe_environment_4_option_4"] = 1;
$demo_content["realty_55"]["fe_environment_4_option_5"] = 1;
$demo_content["realty_55"]["headline"] = "Grab Your Checklist! Move In Ready Townhouse And Only 1.5 Years New. Stainless Appliances.";
$demo_content["realty_55"]["fe_comments_4"] = "Great Neighborhood, Walk 2 Shopping, Parks, Family Oriented Area, 3+1 Good Size Bedrooms, Updated Bathrooms, Fridge, Stove.";

$demo_content["realty_56"]["id"] = 56;
$demo_content["realty_56"]["id_user"] = "Carolina Mountain Realty, Inc.";
$demo_content["realty_56"]["id_type"] = "rent";
$demo_content["realty_56"]["id_category"] = "Commercial";
$demo_content["realty_56"]["property_type"] = "Café";
$demo_content["realty_56"]["status"] = 1;
$demo_content["realty_56"]["sold"] = 0;
$demo_content["realty_56"]["date_open"] = "2014-11-03 00:00:00";
$demo_content["realty_56"]["date_available"] = "2014-01-01 00:00:00";
$demo_content["realty_56"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_56"]["price"] = 1200;
$demo_content["realty_56"]["gid_currency"] = "USD";
$demo_content["realty_56"]["price_period"] = 2;
$demo_content["realty_56"]["price_type"] = 1;
$demo_content["realty_56"]["square"] = 1000;
$demo_content["realty_56"]["square_unit"] = "sqft";
$demo_content["realty_56"]["address"] = "483 Bay Street North Tower";
$demo_content["realty_56"]["id_country"] = "Canada";
$demo_content["realty_56"]["id_region"] = "Ontario";
$demo_content["realty_56"]["id_city"] = "Toronto";
$demo_content["realty_56"]["zip"] = "M5G 2P5";
$demo_content["realty_56"]["lat"] = "43.6593745";
$demo_content["realty_56"]["lon"] = "-79.3853872"; 
$demo_content["realty_56"]["logo_image"] = "633be864b0.jpg";
$demo_content["realty_56"]["fe_year_5"] = 2004;
$demo_content["realty_56"]["fe_revenue_5"] = 78;
$demo_content["realty_56"]["fe_cash_flow_5"] = 30000;
$demo_content["realty_56"]["fe_years_established_5"] = 8;
$demo_content["realty_56"]["fe_employees_5"] = 7;
$demo_content["realty_56"]["fe_reason_for_selling_5"] = "";
$demo_content["realty_56"]["headline"] = "Absolutely Stunning 4 Bedrooms, Detached House, New Paint, Upgrade Kitchen Cabinet Granite Countertop, No Carpet In The House.";
$demo_content["realty_56"]["fe_comments_5"] = "Fridge, Stove, Washer, Dryer, Dishwasher, Upgraded Light Fixtures. Show With Confidence.";

$demo_content["realty_57"]["id"] = 57;
$demo_content["realty_57"]["id_user"] = "Coldwell Banker Dolphin Realty";
$demo_content["realty_57"]["id_type"] = "rent";
$demo_content["realty_57"]["id_category"] = "Lots/lands";
$demo_content["realty_57"]["property_type"] = "Undeveloped land";
$demo_content["realty_57"]["status"] = 1;
$demo_content["realty_57"]["sold"] = 0;
$demo_content["realty_57"]["date_open"] = "2014-05-22 00:00:00";
$demo_content["realty_57"]["date_available"] = "2014-01-01 00:00:00";
$demo_content["realty_57"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_57"]["price"] = 1200;
$demo_content["realty_57"]["gid_currency"] = "USD";
$demo_content["realty_57"]["price_period"] = 2;
$demo_content["realty_57"]["price_type"] = 1;
$demo_content["realty_57"]["square"] = 15000;
$demo_content["realty_57"]["square_unit"] = "sqft";
$demo_content["realty_57"]["address"] = "2 Paveletskaya sq.";
$demo_content["realty_57"]["id_country"] = "Russia";
$demo_content["realty_57"]["id_region"] = "Moscow";
$demo_content["realty_57"]["id_city"] = "Moscow";
$demo_content["realty_57"]["zip"] = "115054";
$demo_content["realty_57"]["lat"] = "55.7153340";
$demo_content["realty_57"]["lon"] = "37.6458340"; 
$demo_content["realty_57"]["logo_image"] = "25f47c619e.jpg";
$demo_content["realty_57"]["fe_irrigated_6"] = 1;
$demo_content["realty_57"]["fe_residence_6"] = 0;
$demo_content["realty_57"]["headline"] = "Well Maintained Royal Pine Home On An Oversized Lot. Exceptionally Maintained Throughout.";
$demo_content["realty_57"]["fe_comments_6"] = "Refrigerator, Stove, Dishwasher, Washer, Dryer & Freezer. Central Vacuum & Attachments, Plantation Shutters & Window Treatments, Light Fixtures, Pot Lights.";

$demo_content["realty_58"]["id"] = 58;
$demo_content["realty_58"]["id_user"] = "Marina Group Ltd";
$demo_content["realty_58"]["id_type"] = "rent";
$demo_content["realty_58"]["id_category"] = "Residential";
$demo_content["realty_58"]["property_type"] = "Room";
$demo_content["realty_58"]["status"] = 1;
$demo_content["realty_58"]["sold"] = 0;
$demo_content["realty_58"]["date_open"] = "2014-06-13 00:00:00";
$demo_content["realty_58"]["date_available"] = "2014-08-01 00:00:00";
$demo_content["realty_58"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_58"]["price"] = 20;
$demo_content["realty_58"]["gid_currency"] = "USD";
$demo_content["realty_58"]["price_period"] = 2;
$demo_content["realty_58"]["price_type"] = 1;
$demo_content["realty_58"]["square"] = 900;
$demo_content["realty_58"]["square_unit"] = "sqft";
$demo_content["realty_58"]["address"] = "80 Spring Street";
$demo_content["realty_58"]["id_country"] = "United States";
$demo_content["realty_58"]["id_region"] = "New York";
$demo_content["realty_58"]["id_city"] = "New York City";
$demo_content["realty_58"]["zip"] = "10012";
$demo_content["realty_58"]["lat"] = "40.7226590";
$demo_content["realty_58"]["lon"] = "-73.9981820"; 
$demo_content["realty_58"]["logo_image"] = "c0924d5bff.jpg";
$demo_content["realty_58"]["fe_check_in_4"] = "14:00";
$demo_content["realty_58"]["fe_check_out_4"] = "10:00";
$demo_content["realty_58"]["fe_room_type_4"] = "Premium room";
$demo_content["realty_58"]["fe_guests_4"] = "1";
$demo_content["realty_58"]["fe_placement_4"] = "Общая комната";
$demo_content["realty_58"]["fe_live_square_4"] = 2400;
$demo_content["realty_58"]["fe_bd_rooms_4"] = 3;
$demo_content["realty_58"]["fe_bth_rooms_4"] = 1;
$demo_content["realty_58"]["fe_garages_4"] = 2;
$demo_content["realty_58"]["fe_year_4"] = 1997;
$demo_content["realty_58"]["fe_floor_number_4"] = 1;
$demo_content["realty_58"]["fe_total_floors_4"] = 1;
$demo_content["realty_58"]["fe_distance_to_subway_4"] = 15;
$demo_content["realty_58"]["fe_furniture_4"] = 1;
$demo_content["realty_58"]["fe_accomodation_4_option_1"] = 1;
$demo_content["realty_58"]["fe_accomodation_4_option_5"] = 1;
$demo_content["realty_58"]["fe_accomodation_4_option_6"] = 1;
$demo_content["realty_58"]["fe_accomodation_4_option_8"] = 1;
$demo_content["realty_58"]["fe_accomodation_4_option_11"] = 1;
$demo_content["realty_58"]["fe_appliances_4_option_1"] = 1;
$demo_content["realty_58"]["fe_appliances_4_option_4"] = 1;
$demo_content["realty_58"]["fe_appliances_4_option_5"] = 1;
$demo_content["realty_58"]["fe_appliances_4_option_12"] = 1;
$demo_content["realty_58"]["fe_appliances_4_option_14"] = 1;
$demo_content["realty_58"]["fe_amenities_4_option_1"] = 1;
$demo_content["realty_58"]["fe_amenities_4_option_8"] = 1;
$demo_content["realty_58"]["fe_amenities_4_option_9"] = 1;
$demo_content["realty_58"]["fe_amenities_4_option_11"] = 1;
$demo_content["realty_58"]["fe_amenities_4_option_14"] = 1;
$demo_content["realty_58"]["fe_environment_4_option_5"] = 1;
$demo_content["realty_58"]["fe_environment_4_option_7"] = 1;
$demo_content["realty_58"]["fe_environment_4_option_11"] = 1;
$demo_content["realty_58"]["fe_environment_4_option_14"] = 1;
$demo_content["realty_58"]["fe_environment_4_option_15"] = 1;
$demo_content["realty_58"]["headline"] = "Congratulations For Finding This Listing!!! Elegant, Modern Home On A Quiet Crescent. One Of The Larger Models On The Crescent.";
$demo_content["realty_58"]["fe_comments_4"] = "Huge Master Bedroom With W/I Closet And Spacious 4 Pc Bathroom. Finished Basement With Large Play Area And Additional Washroom.";

$demo_content["realty_59"]["id"] = 59;
$demo_content["realty_59"]["id_user"] = "Centurey Wolf's";
$demo_content["realty_59"]["id_type"] = "rent";
$demo_content["realty_59"]["id_category"] = "Commercial";
$demo_content["realty_59"]["property_type"] = "Office";
$demo_content["realty_59"]["status"] = 1;
$demo_content["realty_59"]["sold"] = 0;
$demo_content["realty_59"]["date_open"] = "2014-08-24 00:00:00";
$demo_content["realty_59"]["date_available"] = "2014-09-11 00:00:00";
$demo_content["realty_59"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_59"]["price"] = 2000;
$demo_content["realty_59"]["price_reduced"] = 1000;
$demo_content["realty_59"]["gid_currency"] = "USD";
$demo_content["realty_59"]["price_period"] = 1;
$demo_content["realty_59"]["price_type"] = 1;
$demo_content["realty_59"]["square"] = 600;
$demo_content["realty_59"]["square_unit"] = "sqft";
$demo_content["realty_59"]["address"] = "125 Knightsbridge";
$demo_content["realty_59"]["id_country"] = "United Kingdom";
$demo_content["realty_59"]["id_region"] = "Greater London";
$demo_content["realty_59"]["id_city"] = "London";
$demo_content["realty_59"]["zip"] = "SW1X 7RJ";
$demo_content["realty_59"]["lat"] = "51.5017097";
$demo_content["realty_59"]["lon"] = "-0.1657169";
$demo_content["realty_59"]["logo_image"] = "a1a3c8aede.jpg"; 
$demo_content["realty_59"]["fe_year_5"] = 1988;
$demo_content["realty_59"]["fe_revenue_5"] = 53;
$demo_content["realty_59"]["fe_cash_flow_5"] = 230000;
$demo_content["realty_59"]["fe_years_established_5"] = 12;
$demo_content["realty_59"]["fe_employees_5"] = 50;
$demo_content["realty_59"]["fe_reason_for_selling_5"] = "";
$demo_content["realty_59"]["headline"] = "5 Bedroom Green Park Built Home. Thousands Spent On Upgrades. 3 Newly Renovated Washrooms. Glass Shower Stall In Master.";
$demo_content["realty_59"]["fe_comments_5"] = "Tankless Hotwater System(Owned), Central Vac (Rough-In). Fridge, Stove, Dishwasher, Washer & Dryer, Central Air,Garage Door Opener.";

$demo_content["realty_60"]["id"] = 60;
$demo_content["realty_60"]["id_user"] = "Centurey Wolf's";
$demo_content["realty_60"]["id_type"] = "rent";
$demo_content["realty_60"]["id_category"] = "Lots/lands";
$demo_content["realty_60"]["property_type"] = "Other";
$demo_content["realty_60"]["status"] = 1;
$demo_content["realty_60"]["sold"] = 0;
$demo_content["realty_60"]["date_open"] = "2014-08-24 00:00:00";
$demo_content["realty_60"]["date_available"] = "2014-09-11 00:00:00";
$demo_content["realty_60"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_60"]["price"] = 2000;
$demo_content["realty_60"]["gid_currency"] = "USD";
$demo_content["realty_60"]["price_period"] = 1;
$demo_content["realty_60"]["price_type"] = 1;
$demo_content["realty_60"]["square"] = 600;
$demo_content["realty_60"]["square_unit"] = "sqft";
$demo_content["realty_60"]["address"] = "25 rue de Marignan";
$demo_content["realty_60"]["id_country"] = "France";
$demo_content["realty_60"]["id_region"] = "Région Île-de-France";
$demo_content["realty_60"]["id_city"] = "Paris";
$demo_content["realty_60"]["zip"] = "75008";
$demo_content["realty_60"]["lat"] = "48.8693567";
$demo_content["realty_60"]["lon"] = "2.3064055"; 
$demo_content["realty_60"]["logo_image"] = "184281c915.jpg"; 
$demo_content["realty_60"]["fe_irrigated_6"] = 0;
$demo_content["realty_60"]["fe_residence_6"] = 1;
$demo_content["realty_60"]["headline"] = "Must Seen House In Great Loction On Steeles,Lot Is Back 53.77 F & 144.79 F As Per Survey, Close To Yonge.";
$demo_content["realty_60"]["fe_comments_6"] = "Existing Fridge, Built-In Dishwasher, Washer, Dryer, Kitchen Island. All Existing Window Coverings And Fireplace (As Is).";

$demo_content["realty_61"]["id"] = 61;
$demo_content["realty_61"]["id_user"] = "Centurey Wolf's";
$demo_content["realty_61"]["id_type"] = "rent";
$demo_content["realty_61"]["id_category"] = "Residential";
$demo_content["realty_61"]["property_type"] = "Duplex";
$demo_content["realty_61"]["status"] = 1;
$demo_content["realty_61"]["sold"] = 0;
$demo_content["realty_61"]["date_open"] = "2014-09-01 00:00:00";
$demo_content["realty_61"]["date_available"] = "2014-10-26 00:00:00";
$demo_content["realty_61"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_61"]["price"] = 1400;
$demo_content["realty_61"]["gid_currency"] = "USD";
$demo_content["realty_61"]["price_period"] = 1;
$demo_content["realty_61"]["price_type"] = 2;
$demo_content["realty_61"]["square"] = 50;
$demo_content["realty_61"]["square_unit"] = "sqft";
$demo_content["realty_61"]["address"] = "143 Daimlerstrabe";
$demo_content["realty_61"]["id_country"] = "Germany";
$demo_content["realty_61"]["id_region"] = "Land Berlin";
$demo_content["realty_61"]["id_city"] = "Berlin";
$demo_content["realty_61"]["zip"] = "12277";
$demo_content["realty_61"]["lat"] = "52.5191710";
$demo_content["realty_61"]["lon"] = "13.4060912"; 
$demo_content["realty_61"]["logo_image"] = "b3bf1a1d68.jpg";
$demo_content["realty_61"]["fe_check_in_4"] = "14:00";
$demo_content["realty_61"]["fe_check_out_4"] = "10:00";
$demo_content["realty_61"]["fe_room_type_4"] = "Single room";
$demo_content["realty_61"]["fe_guests_4"] = "4"; 
$demo_content["realty_61"]["fe_placement_4"] = "Отдельная комната";
$demo_content["realty_61"]["fe_live_square_4"] = 1800;
$demo_content["realty_61"]["fe_bd_rooms_4"] = 2;
$demo_content["realty_61"]["fe_bth_rooms_4"] = 1;
$demo_content["realty_61"]["fe_garages_4"] = 1;
$demo_content["realty_61"]["fe_year_4"] = 2002;
$demo_content["realty_61"]["fe_floor_number_4"] = 12;
$demo_content["realty_61"]["fe_total_floors_4"] = 24;
$demo_content["realty_61"]["fe_distance_to_subway_4"] = 5;
$demo_content["realty_61"]["fe_furniture_4"] = 2;
$demo_content["realty_61"]["fe_accomodation_4_option_4"] = 1;
$demo_content["realty_61"]["fe_accomodation_4_option_5"] = 1;
$demo_content["realty_61"]["fe_accomodation_4_option_6"] = 1;
$demo_content["realty_61"]["fe_accomodation_4_option_7"] = 1;
$demo_content["realty_61"]["fe_accomodation_4_option_8"] = 1;
$demo_content["realty_61"]["fe_appliances_4_option_2"] = 1;
$demo_content["realty_61"]["fe_appliances_4_option_8"] = 1;
$demo_content["realty_61"]["fe_appliances_4_option_12"] = 1;
$demo_content["realty_61"]["fe_appliances_4_option_14"] = 1;
$demo_content["realty_61"]["fe_appliances_4_option_16"] = 1;
$demo_content["realty_61"]["fe_amenities_4_option_1"] = 1;
$demo_content["realty_61"]["fe_amenities_4_option_3"] = 1;
$demo_content["realty_61"]["fe_amenities_4_option_5"] = 1;
$demo_content["realty_61"]["fe_amenities_4_option_7"] = 1;
$demo_content["realty_61"]["fe_amenities_4_option_9"] = 1;
$demo_content["realty_61"]["fe_environment_4_option_1"] = 1;
$demo_content["realty_61"]["fe_environment_4_option_2"] = 1;
$demo_content["realty_61"]["fe_environment_4_option_3"] = 1;
$demo_content["realty_61"]["fe_environment_4_option_4"] = 1;
$demo_content["realty_61"]["fe_environment_4_option_12"] = 1;
$demo_content["realty_61"]["headline"] = "Southampton-Looking For An Idyllic Location To Custom Design And Build Your Dream Home? Then Look No Further!";
$demo_content["realty_61"]["fe_comments_4"] = "Don't Miss This Great Opportunity To Build On One Of The Last Vacant Lots Available In This Prime Area. Local Builders And Trades Available.";

$demo_content["realty_62"]["id"] = 62;
$demo_content["realty_62"]["id_user"] = "Amanda O'Connor";
$demo_content["realty_62"]["id_type"] = "rent";
$demo_content["realty_62"]["id_category"] = "Commercial";
$demo_content["realty_62"]["property_type"] = "Shop";
$demo_content["realty_62"]["status"] = 1;
$demo_content["realty_62"]["sold"] = 0;
$demo_content["realty_62"]["date_open"] = "2014-08-21 00:00:00";
$demo_content["realty_62"]["date_available"] = "2014-10-24 00:00:00";
$demo_content["realty_62"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_62"]["price"] = 2500;
$demo_content["realty_62"]["gid_currency"] = "USD";
$demo_content["realty_62"]["price_period"] = 1;
$demo_content["realty_62"]["price_type"] = 1;
$demo_content["realty_62"]["square"] = 1800;
$demo_content["realty_62"]["square_unit"] = "sqft";
$demo_content["realty_62"]["address"] = "40, Almagro";
$demo_content["realty_62"]["id_country"] = "Spain";
$demo_content["realty_62"]["id_region"] = "Comunidad de Madrid";
$demo_content["realty_62"]["id_city"] = "Madrid";
$demo_content["realty_62"]["zip"] = "E-28010";
$demo_content["realty_62"]["lat"] = "40.4145954";
$demo_content["realty_62"]["lon"] = "-3.6926639"; 
$demo_content["realty_62"]["logo_image"] = "ea1160db0b.jpg"; 
$demo_content["realty_62"]["fe_year_5"] = 1991;
$demo_content["realty_62"]["fe_revenue_5"] = 85;
$demo_content["realty_62"]["fe_cash_flow_5"] = 50000;
$demo_content["realty_62"]["fe_years_established_5"] = 7;
$demo_content["realty_62"]["fe_employees_5"] = 12;
$demo_content["realty_62"]["fe_reason_for_selling_5"] = "";
$demo_content["realty_62"]["headline"] = "Main Floor Only --- Stunning 4 Bedroom Home In Great Upper Thornhill Estates. Approx. 3100 Sq-Ft, 9 Ft Ceilings, Oversize Front Double Door.";
$demo_content["realty_62"]["fe_comments_5"] = "S/S Fridge, Stove, Microwave, B/I Dishwasher. Washer & Dryer, Upgraded Light Fixtures & Window Coverings, Cac With Humidifier, Garage Door Opener With Remote, Central Vacuum.";

$demo_content["realty_63"]["id"] = 63;
$demo_content["realty_63"]["id_user"] = "Amanda O'Connor";
$demo_content["realty_63"]["id_type"] = "rent";
$demo_content["realty_63"]["id_category"] = "Lots/lands";
$demo_content["realty_63"]["property_type"] = "Farm";
$demo_content["realty_63"]["status"] = 1;
$demo_content["realty_63"]["sold"] = 0;
$demo_content["realty_63"]["date_open"] = "2014-11-01 00:00:00";
$demo_content["realty_63"]["date_available"] = "2014-11-20 00:00:00";
$demo_content["realty_63"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_63"]["price"] = 3500;
$demo_content["realty_63"]["gid_currency"] = "USD";
$demo_content["realty_63"]["price_period"] = 1;
$demo_content["realty_63"]["price_type"] = 1;
$demo_content["realty_63"]["square"] = 20000;
$demo_content["realty_63"]["square_unit"] = "sqft";
$demo_content["realty_63"]["address"] = "27 King's College Circle";
$demo_content["realty_63"]["id_country"] = "Canada";
$demo_content["realty_63"]["id_region"] = "Ontario";
$demo_content["realty_63"]["id_city"] = "Toronto";
$demo_content["realty_63"]["zip"] = "M5S 1A1";
$demo_content["realty_63"]["lat"] = "43.6611024";
$demo_content["realty_63"]["lon"] = "-79.3959291"; 
$demo_content["realty_63"]["logo_image"] = "c708fc30fc.jpg";
$demo_content["realty_63"]["fe_irrigated_6"] = 0;
$demo_content["realty_63"]["fe_residence_6"] = 0; 
$demo_content["realty_63"]["headline"] = "An Excellent Property To Invest. Close To All Amenities, School, Ttc, Shopping Malls, Recreation Centre, Easy To Rent Location.";
$demo_content["realty_63"]["fe_comments_6"] = "All Chattels Belong To Vendor; Fridge, Stove, Gas-Burn Furnace & Equipment.";

$demo_content["realty_64"]["id"] = 64;
$demo_content["realty_64"]["id_user"] = "Deborah Black";
$demo_content["realty_64"]["id_type"] = "rent";
$demo_content["realty_64"]["id_category"] = "Residential";
$demo_content["realty_64"]["property_type"] = "Single Family Home";
$demo_content["realty_64"]["status"] = 1;
$demo_content["realty_64"]["sold"] = 0;
$demo_content["realty_64"]["date_open"] = "2014-05-08 00:00:00";
$demo_content["realty_64"]["date_available"] = "2014-08-19 00:00:00";
$demo_content["realty_64"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_64"]["price"] = 25;
$demo_content["realty_64"]["gid_currency"] = "USD";
$demo_content["realty_64"]["price_period"] = 2;
$demo_content["realty_64"]["price_type"] = 2;
$demo_content["realty_64"]["square"] = 300;
$demo_content["realty_64"]["square_unit"] = "sqft";
$demo_content["realty_64"]["address"] = "71 Izmailovsky highway";
$demo_content["realty_64"]["id_country"] = "Russia";
$demo_content["realty_64"]["id_region"] = "Moscow";
$demo_content["realty_64"]["id_city"] = "Moscow";
$demo_content["realty_64"]["zip"] = "105187";
$demo_content["realty_64"]["lat"] = "40.6722028";
$demo_content["realty_64"]["lon"] = "-74.0111417";
$demo_content["realty_64"]["logo_image"] = "0dbd0beb31.jpg";  
$demo_content["realty_64"]["fe_check_in_4"] = "14:00";
$demo_content["realty_64"]["fe_check_out_4"] = "10:00";
$demo_content["realty_64"]["fe_room_type_4"] = "Double room";
$demo_content["realty_64"]["fe_guests_4"] = "3"; 
$demo_content["realty_64"]["fe_placement_4"] = "Дом/квартира целиком";
$demo_content["realty_64"]["fe_live_square_4"] = 70;
$demo_content["realty_64"]["fe_bd_rooms_4"] = 3;
$demo_content["realty_64"]["fe_bth_rooms_4"] = 1;
$demo_content["realty_64"]["fe_garages_4"] = 0;
$demo_content["realty_64"]["fe_year_4"] = 2009;
$demo_content["realty_64"]["fe_floor_number_4"] = 3;
$demo_content["realty_64"]["fe_total_floors_4"] = 9;
$demo_content["realty_64"]["fe_distance_to_subway_4"] = 0;
$demo_content["realty_64"]["fe_furniture_4"] = 1;
$demo_content["realty_64"]["fe_accomodation_4_option_1"] = 1;
$demo_content["realty_64"]["fe_accomodation_4_option_2"] = 1;
$demo_content["realty_64"]["fe_accomodation_4_option_3"] = 1;
$demo_content["realty_64"]["fe_accomodation_4_option_5"] = 1;
$demo_content["realty_64"]["fe_accomodation_4_option_6"] = 1;
$demo_content["realty_64"]["fe_appliances_4_option_1"] = 1;
$demo_content["realty_64"]["fe_appliances_4_option_2"] = 1;
$demo_content["realty_64"]["fe_appliances_4_option_3"] = 1;
$demo_content["realty_64"]["fe_appliances_4_option_5"] = 1;
$demo_content["realty_64"]["fe_appliances_4_option_6"] = 1;
$demo_content["realty_64"]["fe_amenities_4_option_1"] = 1;
$demo_content["realty_64"]["fe_amenities_4_option_2"] = 1;
$demo_content["realty_64"]["fe_amenities_4_option_3"] = 1;
$demo_content["realty_64"]["fe_amenities_4_option_4"] = 1;
$demo_content["realty_64"]["fe_amenities_4_option_5"] = 1;
$demo_content["realty_64"]["fe_environment_4_option_1"] = 1;
$demo_content["realty_64"]["fe_environment_4_option_2"] = 1;
$demo_content["realty_64"]["fe_environment_4_option_3"] = 1;
$demo_content["realty_64"]["fe_environment_4_option_4"] = 1;
$demo_content["realty_64"]["fe_environment_4_option_5"] = 1;
$demo_content["realty_64"]["headline"] = "One Of A Kind Home Centrally Located On Lakefront Promenade Way Featuring 4 Floors Fully Furnished & Finished Beach Inspired Spaces W/Lake Views.";
$demo_content["realty_64"]["fe_comments_4"] = "Stainless Steel Appl Incl: Fridge, Stove, Washer, Dryer, Built-In Dishwasher & Microwave. Above Ground Pool, Garden Shed.";

$demo_content["realty_65"]["id"] = 65;
$demo_content["realty_65"]["id_user"] = "Deborah Black";
$demo_content["realty_65"]["id_type"] = "rent";
$demo_content["realty_65"]["id_category"] = "Commercial";
$demo_content["realty_65"]["property_type"] = "Restaurant";
$demo_content["realty_65"]["status"] = 1;
$demo_content["realty_65"]["sold"] = 0;
$demo_content["realty_65"]["date_open"] = "2014-08-15 00:00:00";
$demo_content["realty_65"]["date_available"] = "2014-11-12 00:00:00";
$demo_content["realty_65"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_65"]["price"] = 25000;
$demo_content["realty_65"]["gid_currency"] = "USD";
$demo_content["realty_65"]["price_period"] = 1;
$demo_content["realty_65"]["price_type"] = 2;
$demo_content["realty_65"]["square"] = 25000;
$demo_content["realty_65"]["square_unit"] = "sqft";
$demo_content["realty_65"]["address"] = "10 Smolenskaya Naberezhnaya";
$demo_content["realty_65"]["id_country"] = "United States";
$demo_content["realty_65"]["id_region"] = "New York";
$demo_content["realty_65"]["id_city"] = "New York City";
$demo_content["realty_65"]["zip"] = "121099";
$demo_content["realty_65"]["lat"] = "40.7143528";
$demo_content["realty_65"]["lon"] = "-74.0059731"; 
$demo_content["realty_65"]["logo_image"] = "112f838bcc.jpg";
$demo_content["realty_65"]["fe_year_5"] = 2002;
$demo_content["realty_65"]["fe_revenue_5"] = 45;
$demo_content["realty_65"]["fe_cash_flow_5"] = 45000;
$demo_content["realty_65"]["fe_years_established_5"] = 15;
$demo_content["realty_65"]["fe_employees_5"] = 6;
$demo_content["realty_65"]["fe_reason_for_selling_5"] = "";   
$demo_content["realty_65"]["headline"] = "Truly Impressive 5 + 1 Bedroom Greenpark Home. Magnificent Upgraded Kitchen With Granite Countertop, Center Island, Backsplash, Pantry And More. ";
$demo_content["realty_65"]["fe_comments_5"] = "3 Level Spiral Oak Staircase. Finished Lower Level Games Room With B/I Bar. 1Bdrm In Law Suite Complete With Sep Entrance, Kitchen & Liv Rm W/ Full W/O To Backyard.";

$demo_content["realty_66"]["id"] = 66;
$demo_content["realty_66"]["id_user"] = "Forsaleby2007 Tester";
$demo_content["realty_66"]["id_type"] = "rent";
$demo_content["realty_66"]["id_category"] = "Lots/lands";
$demo_content["realty_66"]["property_type"] = "Pasture";
$demo_content["realty_66"]["status"] = 1;
$demo_content["realty_66"]["sold"] = 0;
$demo_content["realty_66"]["date_open"] = "2015-06-01 00:00:00";
$demo_content["realty_66"]["date_available"] = "2015-12-12 00:00:00";
$demo_content["realty_66"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_66"]["price"] = 40000;
$demo_content["realty_66"]["price_reduced"] = 37000;
$demo_content["realty_66"]["gid_currency"] = "USD";
$demo_content["realty_66"]["price_period"] = 1;
$demo_content["realty_66"]["price_type"] = 1;
$demo_content["realty_66"]["square"] = 25000;
$demo_content["realty_66"]["square_unit"] = "sqft";
$demo_content["realty_66"]["address"] = "17 Grosvenor Place";
$demo_content["realty_66"]["id_country"] = "United Kingdom";
$demo_content["realty_66"]["id_region"] = "Greater London";
$demo_content["realty_66"]["id_city"] = "London";
$demo_content["realty_66"]["zip"] = "SW1X 7HR";
$demo_content["realty_66"]["lat"] = "51.5002884";
$demo_content["realty_66"]["lon"] = "-0.1492095"; 
$demo_content["realty_66"]["logo_image"] = "70a7b4654b.jpg";  
$demo_content["realty_66"]["fe_irrigated_6"] = 1;
$demo_content["realty_66"]["fe_residence_6"] = 0;
$demo_content["realty_66"]["headline"] = "Truly Spectacular 3 Bdrm Exec. Bungalow Tucked Away In Rolling Hills Of Erin,5.3 Beautiful Acres,No Expenses Spared In Const.";
$demo_content["realty_66"]["fe_comments_6"] = "Brand New B/I Appls, Cvac, Cntrl Air, Xtra Heavy Granite Counters,Xtra Lrg 2Car Attached Grge W/ Cell Phone Activating Openers, B/I Dining Hutch, Timber+Stone Grand Entry, Set Well Back Behind Wall Of Trees.";

$demo_content["realty_67"]["id"] = 67;
$demo_content["realty_67"]["id_user"] = "Forsaleby2007 Tester";
$demo_content["realty_67"]["id_type"] = "rent";
$demo_content["realty_67"]["id_category"] = "Residential";
$demo_content["realty_67"]["property_type"] = "Multi Family Home";
$demo_content["realty_67"]["status"] = 1;
$demo_content["realty_67"]["sold"] = 0;
$demo_content["realty_67"]["date_open"] = "2015-02-07 00:00:00";
$demo_content["realty_67"]["date_available"] = "2015-06-01 00:00:00";
$demo_content["realty_67"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_67"]["price"] = 40;
$demo_content["realty_67"]["gid_currency"] = "USD";
$demo_content["realty_67"]["price_period"] = 1;
$demo_content["realty_67"]["price_type"] = 2;
$demo_content["realty_67"]["square"] = 450;
$demo_content["realty_67"]["square_unit"] = "sqft";
$demo_content["realty_67"]["address"] = "64, bd Haussmann";
$demo_content["realty_67"]["id_country"] = "France";
$demo_content["realty_67"]["id_region"] = "Région Île-de-France";
$demo_content["realty_67"]["id_city"] = "Paris";
$demo_content["realty_67"]["zip"] = "75009";
$demo_content["realty_67"]["lat"] = "48.8737703";
$demo_content["realty_67"]["lon"] = "2.3280824"; 
$demo_content["realty_67"]["logo_image"] = "6d87eea7f2.jpg";  
$demo_content["realty_67"]["fe_check_in_4"] = "14:00";
$demo_content["realty_67"]["fe_check_out_4"] = "10:00";
$demo_content["realty_67"]["fe_room_type_4"] = "Single room";
$demo_content["realty_67"]["fe_guests_4"] = "2"; 
$demo_content["realty_67"]["fe_placement_4"] = "Общая комната";
$demo_content["realty_67"]["fe_live_square_4"] = 1350;
$demo_content["realty_67"]["fe_bd_rooms_4"] = 2;
$demo_content["realty_67"]["fe_bth_rooms_4"] = 1;
$demo_content["realty_67"]["fe_garages_4"] = 0;
$demo_content["realty_67"]["fe_year_4"] = 1998;
$demo_content["realty_67"]["fe_floor_number_4"] = 3;
$demo_content["realty_67"]["fe_total_floors_4"] = 5;
$demo_content["realty_67"]["fe_distance_to_subway_4"] = 0;
$demo_content["realty_67"]["fe_furniture_4"] = 1;
$demo_content["realty_67"]["fe_accomodation_4_option_1"] = 1;
$demo_content["realty_67"]["fe_accomodation_4_option_2"] = 1;
$demo_content["realty_67"]["fe_accomodation_4_option_3"] = 1;
$demo_content["realty_67"]["fe_accomodation_4_option_4"] = 1;
$demo_content["realty_67"]["fe_accomodation_4_option_5"] = 1;
$demo_content["realty_67"]["fe_accomodation_4_option_6"] = 1;
$demo_content["realty_67"]["fe_accomodation_4_option_7"] = 1;
$demo_content["realty_67"]["fe_accomodation_4_option_8"] = 1;
$demo_content["realty_67"]["fe_accomodation_4_option_9"] = 1;
$demo_content["realty_67"]["fe_accomodation_4_option_10"] = 1;
$demo_content["realty_67"]["fe_appliances_4_option_1"] = 1;
$demo_content["realty_67"]["fe_appliances_4_option_3"] = 1;
$demo_content["realty_67"]["fe_appliances_4_option_8"] = 1;
$demo_content["realty_67"]["fe_appliances_4_option_15"] = 1;
$demo_content["realty_67"]["fe_appliances_4_option_19"] = 1;
$demo_content["realty_67"]["fe_amenities_4_option_4"] = 1;
$demo_content["realty_67"]["fe_amenities_4_option_6"] = 1;
$demo_content["realty_67"]["fe_amenities_4_option_9"] = 1;
$demo_content["realty_67"]["fe_amenities_4_option_12"] = 1;
$demo_content["realty_67"]["fe_amenities_4_option_15"] = 1;
$demo_content["realty_67"]["fe_environment_4_option_10"] = 1;
$demo_content["realty_67"]["fe_environment_4_option_11"] = 1;
$demo_content["realty_67"]["fe_environment_4_option_12"] = 1;
$demo_content["realty_67"]["fe_environment_4_option_13"] = 1;
$demo_content["realty_67"]["fe_environment_4_option_14"] = 1;
$demo_content["realty_67"]["headline"] = "Absolutely Beautiful 4 Bedroom Detached Home In The Great Neighbourhood. Spacious Interior.";
$demo_content["realty_67"]["fe_comments_4"] = "2 Decks, Open Concept Living, Dining & Kitchen Area. Lower Level Rec Room With Separate Entrance 2 Basement With Fully Functional In Law Suite.";

$demo_content["realty_68"]["id"] = 68;
$demo_content["realty_68"]["id_user"] = "Randy Ingraham";
$demo_content["realty_68"]["id_type"] = "rent";
$demo_content["realty_68"]["id_category"] = "Commercial";
$demo_content["realty_68"]["property_type"] = "Hotel";
$demo_content["realty_68"]["status"] = 1;
$demo_content["realty_68"]["sold"] = 0;
$demo_content["realty_68"]["date_open"] = "2015-08-14 00:00:00";
$demo_content["realty_68"]["date_available"] = "2015-08-14 00:00:00";
$demo_content["realty_68"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_68"]["price"] = 10000;
$demo_content["realty_68"]["gid_currency"] = "USD";
$demo_content["realty_68"]["price_period"] = 2;
$demo_content["realty_68"]["price_type"] = 1;
$demo_content["realty_68"]["square"] = 45000;
$demo_content["realty_68"]["square_unit"] = "sqft";
$demo_content["realty_68"]["address"] = "55 Behrenstrabe";
$demo_content["realty_68"]["id_country"] = "Germany";
$demo_content["realty_68"]["id_region"] = "Land Berlin";
$demo_content["realty_68"]["id_city"] = "Berlin";
$demo_content["realty_68"]["zip"] = "10117";
$demo_content["realty_68"]["lat"] = "52.5191710";
$demo_content["realty_68"]["lon"] = "13.4060912"; 
$demo_content["realty_68"]["logo_image"] = "2d42f0a1ad.jpg";
$demo_content["realty_68"]["fe_year_5"] = 1964;
$demo_content["realty_68"]["fe_revenue_5"] = 25;
$demo_content["realty_68"]["fe_cash_flow_5"] = 80000;
$demo_content["realty_68"]["fe_years_established_5"] = 9;
$demo_content["realty_68"]["fe_employees_5"] = 15;
$demo_content["realty_68"]["fe_reason_for_selling_5"] = "";  
$demo_content["realty_68"]["headline"] = "Stunning, Renovated, Spacious Five (5) Bedroom Southview Home Backing Onto Toronto Ladies Golf Club.";
$demo_content["realty_68"]["fe_comments_5"] = "All Window Blinds, All Electrical Light Fixtures, Built-In S.S Kitchen Appliances(Gas Stove, Fridge, Dishwasher, Microwave), Lg Drum Washer And Dryer, Garage Door Opener With Remote, Furnace, Air Conditioner.";

$demo_content["realty_69"]["id"] = 69;
$demo_content["realty_69"]["id_user"] = "Rhonda Lindsay";
$demo_content["realty_69"]["id_type"] = "rent";
$demo_content["realty_69"]["id_category"] = "Lots/lands";
$demo_content["realty_69"]["property_type"] = "Timberland";
$demo_content["realty_69"]["status"] = 1;
$demo_content["realty_69"]["sold"] = 0;
$demo_content["realty_69"]["date_open"] = "2015-09-22 00:00:00";
$demo_content["realty_69"]["date_available"] = "2015-09-22 00:00:00";
$demo_content["realty_69"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_69"]["price"] = 1000;
$demo_content["realty_69"]["price_reduced"] = 900;
$demo_content["realty_69"]["gid_currency"] = "USD";
$demo_content["realty_69"]["price_period"] = 2;
$demo_content["realty_69"]["price_type"] = 1;
$demo_content["realty_69"]["square"] = 5000;
$demo_content["realty_69"]["square_unit"] = "sqft";
$demo_content["realty_69"]["address"] = "79 Claudio Coello";
$demo_content["realty_69"]["id_country"] = "Spain";
$demo_content["realty_69"]["id_region"] = "Comunidad de Madrid";
$demo_content["realty_69"]["id_city"] = "Madrid";
$demo_content["realty_69"]["zip"] = "28001";
$demo_content["realty_69"]["lat"] = "40.4295887";
$demo_content["realty_69"]["lon"] = "-3.6860727"; 
$demo_content["realty_69"]["logo_image"] = "697c142772.jpg";  
$demo_content["realty_69"]["fe_irrigated_6"] = 1;
$demo_content["realty_69"]["fe_residence_6"] = 1;
$demo_content["realty_69"]["headline"] = "Newly Renovated And Affordable; This Is A Great Opportunity For First Time Buyers Or Savvy Investors!";
$demo_content["realty_69"]["fe_comments_6"] = "Schools, Parks And Shopping. Tenant Pays Utilities. Fridge, Stove, Washer, Dryer, All Elf's.";

$demo_content["realty_70"]["id"] = 70;
$demo_content["realty_70"]["id_user"] = "Rhonda Lindsay";
$demo_content["realty_70"]["id_type"] = "rent";
$demo_content["realty_70"]["id_category"] = "Residential";
$demo_content["realty_70"]["property_type"] = "Condo";
$demo_content["realty_70"]["status"] = 1;
$demo_content["realty_70"]["sold"] = 0;
$demo_content["realty_70"]["date_open"] = "2015-03-02 00:00:00";
$demo_content["realty_70"]["date_available"] = "2015-06-01 00:00:00";
$demo_content["realty_70"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_70"]["price"] = 10;
$demo_content["realty_70"]["gid_currency"] = "USD";
$demo_content["realty_70"]["price_period"] = 2;
$demo_content["realty_70"]["price_type"] = 1;
$demo_content["realty_70"]["square"] = 80;
$demo_content["realty_70"]["square_unit"] = "sqft";
$demo_content["realty_70"]["address"] = "279 Yonge Street";
$demo_content["realty_70"]["id_country"] = "Canada";
$demo_content["realty_70"]["id_region"] = "Ontario";
$demo_content["realty_70"]["id_city"] = "Toronto";
$demo_content["realty_70"]["zip"] = "M5B 1N8";
$demo_content["realty_70"]["lat"] = "43.6556329";
$demo_content["realty_70"]["lon"] = "-79.3804719"; 
$demo_content["realty_70"]["logo_image"] = "1e337f08a0.jpg";
$demo_content["realty_70"]["fe_check_in_4"] = "14:00";
$demo_content["realty_70"]["fe_check_out_4"] = "10:00";
$demo_content["realty_70"]["fe_room_type_4"] = "Single room";
$demo_content["realty_70"]["fe_guests_4"] = "1"; 
$demo_content["realty_70"]["fe_placement_4"] = "Дом/квартира целиком";  
$demo_content["realty_70"]["fe_live_square_4"] = 3000;
$demo_content["realty_70"]["fe_bd_rooms_4"] = 4;
$demo_content["realty_70"]["fe_bth_rooms_4"] = 2;
$demo_content["realty_70"]["fe_garages_4"] = 1;
$demo_content["realty_70"]["fe_year_4"] = 2002;
$demo_content["realty_70"]["fe_floor_number_4"] = 5;
$demo_content["realty_70"]["fe_total_floors_4"] = 10;
$demo_content["realty_70"]["fe_distance_to_subway_4"] = 0;
$demo_content["realty_70"]["fe_furniture_4"] = 1;
$demo_content["realty_70"]["fe_accomodation_4_option_1"] = 1;
$demo_content["realty_70"]["fe_accomodation_4_option_2"] = 1;
$demo_content["realty_70"]["fe_accomodation_4_option_3"] = 1;
$demo_content["realty_70"]["fe_accomodation_4_option_5"] = 1;
$demo_content["realty_70"]["fe_accomodation_4_option_6"] = 1;
$demo_content["realty_70"]["fe_accomodation_4_option_7"] = 1;
$demo_content["realty_70"]["fe_accomodation_4_option_10"] = 1;
$demo_content["realty_70"]["fe_appliances_4_option_1"] = 1;
$demo_content["realty_70"]["fe_appliances_4_option_3"] = 1;
$demo_content["realty_70"]["fe_appliances_4_option_8"] = 1;
$demo_content["realty_70"]["fe_appliances_4_option_15"] = 1;
$demo_content["realty_70"]["fe_appliances_4_option_19"] = 1;
$demo_content["realty_70"]["fe_amenities_4_option_4"] = 1;
$demo_content["realty_70"]["fe_amenities_4_option_6"] = 1;
$demo_content["realty_70"]["fe_amenities_4_option_9"] = 1;
$demo_content["realty_70"]["fe_amenities_4_option_12"] = 1;
$demo_content["realty_70"]["fe_amenities_4_option_15"] = 1;
$demo_content["realty_70"]["fe_environment_4_option_10"] = 1;
$demo_content["realty_70"]["fe_environment_4_option_11"] = 1;
$demo_content["realty_70"]["fe_environment_4_option_12"] = 1;
$demo_content["realty_70"]["fe_environment_4_option_13"] = 1;
$demo_content["realty_70"]["fe_environment_4_option_14"] = 1;
$demo_content["realty_70"]["headline"] = "Bright And Spacious Brand New Apartment Building In The Heart Of Richmond Hill.";
$demo_content["realty_70"]["fe_comments_4"] = "Impeccable Two Bedroom Unit, Approximately 800 Square Feet. Features Brand New Stainless Steel Appliances, Hardwood Floors Throughout And Locker. Don't Miss Out On This Great Opportunity!";

$demo_content["realty_71"]["id"] = 71;
$demo_content["realty_71"]["id_user"] = "Rhonda Lindsay";
$demo_content["realty_71"]["id_type"] = "rent";
$demo_content["realty_71"]["id_category"] = "Commercial";
$demo_content["realty_71"]["property_type"] = "Sports facility";
$demo_content["realty_71"]["status"] = 1;
$demo_content["realty_71"]["sold"] = 0;
$demo_content["realty_71"]["date_open"] = "2015-05-17 00:00:00";
$demo_content["realty_71"]["date_available"] = "2015-08-25 00:00:00";
$demo_content["realty_71"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_71"]["price"] = 2000;
$demo_content["realty_71"]["gid_currency"] = "USD";
$demo_content["realty_71"]["price_period"] = 2;
$demo_content["realty_71"]["price_type"] = 1;
$demo_content["realty_71"]["square"] = 2980;
$demo_content["realty_71"]["square_unit"] = "sqft";
$demo_content["realty_71"]["address"] = "35, Ul. Myasnitskaya";
$demo_content["realty_71"]["id_country"] = "Russia";
$demo_content["realty_71"]["id_region"] = "Moscow";
$demo_content["realty_71"]["id_city"] = "Moscow";
$demo_content["realty_71"]["zip"] = "101000";
$demo_content["realty_71"]["lat"] = "55.7660560";
$demo_content["realty_71"]["lon"] = "37.6381050"; 
$demo_content["realty_71"]["logo_image"] = "f816fe1a97.jpg"; 
$demo_content["realty_71"]["fe_year_5"] = 1998;
$demo_content["realty_71"]["fe_revenue_5"] = 85;
$demo_content["realty_71"]["fe_cash_flow_5"] = 20000;
$demo_content["realty_71"]["fe_years_established_5"] = 18;
$demo_content["realty_71"]["fe_employees_5"] = 3;
$demo_content["realty_71"]["fe_reason_for_selling_5"] = ""; 
$demo_content["realty_71"]["headline"] = "2 Houses On This 96 Acre Farm With Amazing Hill Top Views Overlooking Lake Simcoe.";
$demo_content["realty_71"]["fe_comments_5"] = "All Eletrical Light Fixtures, All Window Coverings, Electric Fireplace, Jacuzzi Tub. Note Natural Gas Available At Road.";

$demo_content["realty_72"]["id"] = 72;
$demo_content["realty_72"]["id_user"] = "Marina Group Ltd";
$demo_content["realty_72"]["id_type"] = "rent";
$demo_content["realty_72"]["id_category"] = "Lots/lands";
$demo_content["realty_72"]["property_type"] = "Undeveloped land";
$demo_content["realty_72"]["status"] = 1;
$demo_content["realty_72"]["sold"] = 0;
$demo_content["realty_72"]["date_open"] = "2015-05-17 00:00:00";
$demo_content["realty_72"]["date_available"] = "2015-08-25 00:00:00";
$demo_content["realty_72"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_72"]["price"] = 1200;
$demo_content["realty_72"]["price_reduced"] = 1000;
$demo_content["realty_72"]["gid_currency"] = "USD";
$demo_content["realty_72"]["price_period"] = 2;
$demo_content["realty_72"]["price_type"] = 1;
$demo_content["realty_72"]["square"] = 900;
$demo_content["realty_72"]["square_unit"] = "sqft";
$demo_content["realty_72"]["address"] = "77 Water Street";
$demo_content["realty_72"]["id_country"] = "United States";
$demo_content["realty_72"]["id_region"] = "New York";
$demo_content["realty_72"]["id_city"] = "New York City";
$demo_content["realty_72"]["zip"] = "10005";
$demo_content["realty_72"]["lat"] = "40.7042130";
$demo_content["realty_72"]["lon"] = "-74.0082089"; 
$demo_content["realty_72"]["logo_image"] = "3112d8ffe0.jpg";  
$demo_content["realty_72"]["fe_irrigated_6"] = 1;
$demo_content["realty_72"]["fe_residence_6"] = 0;
$demo_content["realty_72"]["headline"] = "Brand New Custom Built Luxury Home! Unique 3,100 Sqft. 4 Bedroom, 5 Bath In Desired Willowdale E Location.";
$demo_content["realty_72"]["fe_comments_6"] = "Custom Kitchen With Beverage Fridge. Stainless Steel Appliances. Convenient 2nd Floor Laundry Room. Skylight On 2nd Level. Power Garage Door With Code Access.";

$demo_content["realty_73"]["id"] = 73;
$demo_content["realty_73"]["id_user"] = "Deborah Black";
$demo_content["realty_73"]["id_type"] = "rent";
$demo_content["realty_73"]["id_category"] = "Residential";
$demo_content["realty_73"]["property_type"] = "Townhouse";
$demo_content["realty_73"]["status"] = 1;
$demo_content["realty_73"]["sold"] = 0;
$demo_content["realty_73"]["date_open"] = "2015-03-14 00:00:00";
$demo_content["realty_73"]["date_available"] = "2015-05-15 00:00:00";
$demo_content["realty_73"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_73"]["price"] = 15;
$demo_content["realty_73"]["gid_currency"] = "USD";
$demo_content["realty_73"]["price_period"] = 1;
$demo_content["realty_73"]["price_type"] = 1;
$demo_content["realty_73"]["square"] = 300;
$demo_content["realty_73"]["square_unit"] = "sqft";
$demo_content["realty_73"]["address"] = "17 Watling Street";
$demo_content["realty_73"]["id_country"] = "United Kingdom";
$demo_content["realty_73"]["id_region"] = "Greater London";
$demo_content["realty_73"]["id_city"] = "London";
$demo_content["realty_73"]["zip"] = "EC4M 9BB";
$demo_content["realty_73"]["lat"] = "51.5128291";
$demo_content["realty_73"]["lon"] = "-0.0947577"; 
$demo_content["realty_73"]["logo_image"] = "cd75624017.jpg";  
$demo_content["realty_73"]["fe_check_in_4"] = "14:00";
$demo_content["realty_73"]["fe_check_out_4"] = "10:00";
$demo_content["realty_73"]["fe_room_type_4"] = "Triple room";
$demo_content["realty_73"]["fe_guests_4"] = "4"; 
$demo_content["realty_73"]["fe_placement_4"] = "Отдельная комната";
$demo_content["realty_73"]["fe_live_square_4"] = 1100;
$demo_content["realty_73"]["fe_bd_rooms_4"] = 1;
$demo_content["realty_73"]["fe_bth_rooms_4"] = 1;
$demo_content["realty_73"]["fe_garages_4"] = 0;
$demo_content["realty_73"]["fe_year_4"] = 2010;
$demo_content["realty_73"]["fe_floor_number_4"] = 7;
$demo_content["realty_73"]["fe_total_floors_4"] = 9;
$demo_content["realty_73"]["fe_distance_to_subway_4"] = 5;
$demo_content["realty_73"]["fe_furniture_4"] = 1;
$demo_content["realty_73"]["fe_accomodation_4_option_1"] = 1;
$demo_content["realty_73"]["fe_accomodation_4_option_2"] = 1;
$demo_content["realty_73"]["fe_accomodation_4_option_8"] = 1;
$demo_content["realty_73"]["fe_accomodation_4_option_10"] = 1;
$demo_content["realty_73"]["fe_accomodation_4_option_19"] = 1;
$demo_content["realty_73"]["fe_appliances_4_option_1"] = 1;
$demo_content["realty_73"]["fe_appliances_4_option_3"] = 1;
$demo_content["realty_73"]["fe_appliances_4_option_6"] = 1;
$demo_content["realty_73"]["fe_appliances_4_option_9"] = 1;
$demo_content["realty_73"]["fe_appliances_4_option_12"] = 1;
$demo_content["realty_73"]["fe_amenities_4_option_1"] = 1;
$demo_content["realty_73"]["fe_amenities_4_option_2"] = 1;
$demo_content["realty_73"]["fe_amenities_4_option_3"] = 1;
$demo_content["realty_73"]["fe_amenities_4_option_4"] = 1;
$demo_content["realty_73"]["fe_amenities_4_option_5"] = 1;
$demo_content["realty_73"]["fe_environment_4_option_1"] = 1;
$demo_content["realty_73"]["fe_environment_4_option_2"] = 1;
$demo_content["realty_73"]["fe_environment_4_option_3"] = 1;
$demo_content["realty_73"]["fe_environment_4_option_4"] = 1;
$demo_content["realty_73"]["fe_environment_4_option_5"] = 1;
$demo_content["realty_73"]["headline"] = "Beautiful All Brick Detach Home In Mississauga.. Great Location Centrally Located Home With A Huge Lot.";
$demo_content["realty_73"]["fe_comments_4"] = "Basement Apartment, Home Kept In Pristine Condition A Must See Beauty.. Priced For Quick Action! Fridge, Stove, Washer & Dryer, All Elfs & Window Coverings";

$demo_content["realty_74"]["id"] = 74;
$demo_content["realty_74"]["id_user"] = "Frankie Firouznia";
$demo_content["realty_74"]["id_type"] = "rent";
$demo_content["realty_74"]["id_category"] = "Commercial";
$demo_content["realty_74"]["property_type"] = "Warehouse";
$demo_content["realty_74"]["status"] = 1;
$demo_content["realty_74"]["sold"] = 0;
$demo_content["realty_74"]["date_open"] = "2015-03-14 00:00:00";
$demo_content["realty_74"]["date_available"] = "2015-05-15 00:00:00";
$demo_content["realty_74"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_74"]["price"] = 250;
$demo_content["realty_74"]["gid_currency"] = "USD";
$demo_content["realty_74"]["price_period"] = 2;
$demo_content["realty_74"]["price_type"] = 1;
$demo_content["realty_74"]["square"] = 800;
$demo_content["realty_74"]["square_unit"] = "sqft";
$demo_content["realty_74"]["address"] = "Versailles";
$demo_content["realty_74"]["id_country"] = "France";
$demo_content["realty_74"]["id_region"] = "Région Île-de-France";
$demo_content["realty_74"]["id_city"] = "Paris";
$demo_content["realty_74"]["zip"] = "78000";
$demo_content["realty_74"]["lat"] = "48.7993695";
$demo_content["realty_74"]["lon"] = "2.1421879"; 
$demo_content["realty_74"]["logo_image"] = "85cfcc0d39.jpg";  
$demo_content["realty_74"]["fe_year_5"] = 1995;
$demo_content["realty_74"]["fe_revenue_5"] = 75;
$demo_content["realty_74"]["fe_cash_flow_5"] = 450000;
$demo_content["realty_74"]["fe_years_established_5"] = 9;
$demo_content["realty_74"]["fe_employees_5"] = 65;
$demo_content["realty_74"]["fe_reason_for_selling_5"] = "";
$demo_content["realty_74"]["headline"] = "Beautiful Family Home On Quiet St In Central Brampton. Very Spacious 2138 Sq Ft As Per Sellers.";
$demo_content["realty_74"]["fe_comments_5"] = "Soak-Er Tub And Walk In Closet; 2nd Master Bedroom W/ En-Suite, Other 2 Good Size Bedrooms With Jack And Jill. Just Need Excellent Tenants; A Must See! Call Today!";

$demo_content["realty_75"]["id"] = 75;
$demo_content["realty_75"]["id_user"] = "Mark Louis";
$demo_content["realty_75"]["id_type"] = "rent";
$demo_content["realty_75"]["id_category"] = "Lots/lands";
$demo_content["realty_75"]["property_type"] = "Other";
$demo_content["realty_75"]["status"] = 1;
$demo_content["realty_75"]["sold"] = 0;
$demo_content["realty_75"]["date_open"] = "2015-06-21 00:00:00";
$demo_content["realty_75"]["date_available"] = "2015-08-27 00:00:00";
$demo_content["realty_75"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_75"]["price"] = 150;
$demo_content["realty_75"]["gid_currency"] = "USD";
$demo_content["realty_75"]["price_period"] = 2;
$demo_content["realty_75"]["price_type"] = 1;
$demo_content["realty_75"]["square"] = 600;
$demo_content["realty_75"]["square_unit"] = "sqft";
$demo_content["realty_75"]["address"] = "2a Voltaire Strabe";
$demo_content["realty_75"]["id_country"] = "Germany";
$demo_content["realty_75"]["id_region"] = "Land Berlin";
$demo_content["realty_75"]["id_city"] = "Berlin";
$demo_content["realty_75"]["zip"] = "10179";
$demo_content["realty_75"]["lat"] = "52.5191710";
$demo_content["realty_75"]["lon"] = "13.4060912"; 
$demo_content["realty_75"]["logo_image"] = "5af5bdc4c3.jpg";  
$demo_content["realty_75"]["fe_irrigated_6"] = 1;
$demo_content["realty_75"]["fe_residence_6"] = 1;
$demo_content["realty_75"]["headline"] = "4 Bed, 4 Wash, Finished Basement. Premium Corner Lot With Sprinkler System.";
$demo_content["realty_75"]["fe_comments_6"] = "2 Fridges, 2 Stove, Washer/Dryer. All Electrical Light Fixtures, All Window Coverings/Blinds. Except The Chandelier In The Family Room.";

$demo_content["realty_76"]["id"] = 76;
$demo_content["realty_76"]["id_user"] = "Pilot Group";
$demo_content["realty_76"]["id_type"] = "lease";
$demo_content["realty_76"]["id_category"] = "Residential";
$demo_content["realty_76"]["property_type"] = "Mobile home";
$demo_content["realty_76"]["status"] = 1;
$demo_content["realty_76"]["sold"] = 0;
$demo_content["realty_76"]["date_open"] = "2014-02-01 00:00:00";
$demo_content["realty_76"]["date_available"] = "2014-02-01 00:00:00";
$demo_content["realty_76"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_76"]["price"] = 15;
$demo_content["realty_76"]["price_max"] = 25;
$demo_content["realty_76"]["gid_currency"] = "USD";
$demo_content["realty_76"]["price_period"] = 1;
$demo_content["realty_76"]["price_type"] = 1;
$demo_content["realty_76"]["square"] = 80;
$demo_content["realty_76"]["square_max"] = 90;
$demo_content["realty_76"]["square_unit"] = "sqft";
$demo_content["realty_76"]["id_country"] = "Spain";
$demo_content["realty_76"]["id_region"] = "Comunidad de Madrid";
$demo_content["realty_76"]["id_city"] = "Madrid";
$demo_content["realty_76"]["zip"] = "28036";
$demo_content["realty_76"]["lat"] = "40.4532371";
$demo_content["realty_76"]["lon"] = "-3.6893934"; 
$demo_content["realty_76"]["fe_check_in_4"] = "14:00";
$demo_content["realty_76"]["fe_check_out_4"] = "10:00";
$demo_content["realty_76"]["fe_room_type_4"] = "Single room";
$demo_content["realty_76"]["fe_guests_4"] = "1"; 
$demo_content["realty_76"]["fe_placement_4"] = "Дом/квартира целиком";
$demo_content["realty_76"]["fe_live_square_4"] = 1100;
$demo_content["realty_76"]["fe_bd_rooms_4"] = 2;
$demo_content["realty_76"]["fe_bth_rooms_4"] = 1;
$demo_content["realty_76"]["fe_garages_4"] = 0;
$demo_content["realty_76"]["fe_year_4"] = 2005;
$demo_content["realty_76"]["fe_floor_number_4"] = 3;
$demo_content["realty_76"]["fe_total_floors_4"] = 12;
$demo_content["realty_76"]["fe_distance_to_subway_4"] = 5;
$demo_content["realty_76"]["fe_furniture_4"] = 1;
$demo_content["realty_76"]["fe_accomodation_4_option_1"] = 1;
$demo_content["realty_76"]["fe_accomodation_4_option_2"] = 1;
$demo_content["realty_76"]["fe_accomodation_4_option_4"] = 1;
$demo_content["realty_76"]["fe_accomodation_4_option_15"] = 1;
$demo_content["realty_76"]["fe_accomodation_4_option_19"] = 1;
$demo_content["realty_76"]["fe_appliances_4_option_1"] = 1;
$demo_content["realty_76"]["fe_appliances_4_option_2"] = 1;
$demo_content["realty_76"]["fe_appliances_4_option_4"] = 1;
$demo_content["realty_76"]["fe_appliances_4_option_7"] = 1;
$demo_content["realty_76"]["fe_appliances_4_option_9"] = 1;
$demo_content["realty_76"]["fe_amenities_4_option_1"] = 1;
$demo_content["realty_76"]["fe_amenities_4_option_3"] = 1;
$demo_content["realty_76"]["fe_amenities_4_option_8"] = 1;
$demo_content["realty_76"]["fe_amenities_4_option_14"] = 1;
$demo_content["realty_76"]["fe_amenities_4_option_15"] = 1;
$demo_content["realty_76"]["fe_environment_4_option_1"] = 1;
$demo_content["realty_76"]["fe_environment_4_option_5"] = 1;
$demo_content["realty_76"]["fe_environment_4_option_8"] = 1;
$demo_content["realty_76"]["fe_environment_4_option_14"] = 1;
$demo_content["realty_76"]["fe_environment_4_option_15"] = 1;
$demo_content["realty_76"]["headline"] = "Original Owners! Fabulous 4 Bdrm Home With Exceptional Landscaping, Beautiful Backyard!";
$demo_content["realty_76"]["fe_comments_4"] = "Windows Replaced (2009), Roof Shingles (2008), Cvac Equipment. Exclude: Chandeliers In Foyer & Dining Rm (To Be Replaced), Wood Stove (At Doors), Water Softener And Distiller Amazing Value! Seller Says Sell!";

$demo_content["realty_77"]["id"] = 77;
$demo_content["realty_77"]["id_user"] = "Pilot Group";
$demo_content["realty_77"]["id_type"] = "lease";
$demo_content["realty_77"]["id_category"] = "Commercial";
$demo_content["realty_77"]["property_type"] = "Manufactured";
$demo_content["realty_77"]["status"] = 1;
$demo_content["realty_77"]["sold"] = 0;
$demo_content["realty_77"]["date_open"] = "2014-05-08 00:00:00";
$demo_content["realty_77"]["date_available"] = "2014-06-01 00:00:00";
$demo_content["realty_77"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_77"]["price"] = 180;
$demo_content["realty_77"]["price_max"] = 240;
$demo_content["realty_77"]["gid_currency"] = "USD";
$demo_content["realty_77"]["price_period"] = 2;
$demo_content["realty_77"]["price_type"] = 1;
$demo_content["realty_77"]["square"] = 400;
$demo_content["realty_77"]["square_max"] = 420;
$demo_content["realty_77"]["square_unit"] = "sqft";
$demo_content["realty_77"]["id_country"] = "Canada";
$demo_content["realty_77"]["id_region"] = "Ontario";
$demo_content["realty_77"]["id_city"] = "Toronto";
$demo_content["realty_77"]["zip"] = "M4W 1A8";
$demo_content["realty_77"]["lat"] = "43.6706237";
$demo_content["realty_77"]["lon"] = "-79.3863045"; 
$demo_content["realty_77"]["fe_year_5"] = 1983;
$demo_content["realty_77"]["fe_revenue_5"] = 35;
$demo_content["realty_77"]["fe_cash_flow_5"] = 54000;
$demo_content["realty_77"]["fe_years_established_5"] = 13;
$demo_content["realty_77"]["fe_employees_5"] = 10;
$demo_content["realty_77"]["fe_reason_for_selling_5"] = ""; 
$demo_content["realty_77"]["headline"] = "Desirable Streetsville Glen Gorgeous Designer Finish Amazing 4 Bedrooms With Extensive Renovations Incl.";
$demo_content["realty_77"]["fe_comments_5"] = "Top Of The Line Appliances- Brand New Dishwasher, Water Softener Air Cleaner Fridge, Front Load Washer/Dryer, Central Vac, A/C,Garage Openers, Cvac Alarm.";

$demo_content["realty_78"]["id"] = 78;
$demo_content["realty_78"]["id_user"] = "Pilot Group";
$demo_content["realty_78"]["id_type"] = "lease";
$demo_content["realty_78"]["id_category"] = "Lots/lands";
$demo_content["realty_78"]["property_type"] = "Farm";
$demo_content["realty_78"]["status"] = 1;
$demo_content["realty_78"]["sold"] = 0;
$demo_content["realty_78"]["date_open"] = "2014-05-08 00:00:00";
$demo_content["realty_78"]["date_available"] = "2014-06-01 00:00:00";
$demo_content["realty_78"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_78"]["price"] = 500;
$demo_content["realty_78"]["price_max"] = 1000;
$demo_content["realty_78"]["gid_currency"] = "USD";
$demo_content["realty_78"]["price_period"] = 2;
$demo_content["realty_78"]["price_type"] = 1;
$demo_content["realty_78"]["square"] = 1200;
$demo_content["realty_78"]["square_max"] = 1250;
$demo_content["realty_78"]["square_unit"] = "sqft";
$demo_content["realty_78"]["id_country"] = "Russia";
$demo_content["realty_78"]["id_region"] = "Moscow";
$demo_content["realty_78"]["id_city"] = "Moscow";
$demo_content["realty_78"]["zip"] = "121248";
$demo_content["realty_78"]["lat"] = "55.7514536";
$demo_content["realty_78"]["lon"] = "37.5701199"; 
$demo_content["realty_78"]["fe_irrigated_6"] = 0;
$demo_content["realty_78"]["fe_residence_6"] = 1; 
$demo_content["realty_78"]["headline"] = "Located In The Heart Of Mississauga & Just Steps From Square One.";
$demo_content["realty_78"]["fe_comments_6"] = "All Electrical Fixtures, Window Coverings, California Shutters, Fridge, Stove, Dishwasher, Washer/Dryer, Move In Condition, Easy Access To All Hwys, Square One, Ymca, Library & Schools.";

$demo_content["realty_79"]["id"] = 79;
$demo_content["realty_79"]["id_user"] = "Pilot Group";
$demo_content["realty_79"]["id_type"] = "lease";
$demo_content["realty_79"]["id_category"] = "Residential";
$demo_content["realty_79"]["property_type"] = "Other";
$demo_content["realty_79"]["status"] = 1;
$demo_content["realty_79"]["sold"] = 0;
$demo_content["realty_79"]["date_open"] = "2014-09-18 00:00:00";
$demo_content["realty_79"]["date_available"] = "2014-09-18 00:00:00";
$demo_content["realty_79"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_79"]["price"] = 50;
$demo_content["realty_79"]["price_max"] = 100;
$demo_content["realty_79"]["gid_currency"] = "USD";
$demo_content["realty_79"]["price_period"] = 1;
$demo_content["realty_79"]["price_type"] = 1;
$demo_content["realty_79"]["square"] = 1000;
$demo_content["realty_79"]["square_max"] = 1100;
$demo_content["realty_79"]["square_unit"] = "sqft";
$demo_content["realty_79"]["id_country"] = "United States";
$demo_content["realty_79"]["id_region"] = "New York";
$demo_content["realty_79"]["id_city"] = "New York City";
$demo_content["realty_79"]["zip"] = "11205";
$demo_content["realty_79"]["lat"] = "40.6921700";
$demo_content["realty_79"]["lon"] = "-73.9630320";
$demo_content["realty_76"]["fe_check_in_4"] = "14:00";
$demo_content["realty_76"]["fe_check_out_4"] = "10:00";
$demo_content["realty_76"]["fe_room_type_4"] = "Single room";
$demo_content["realty_76"]["fe_guests_4"] = "3"; 
$demo_content["realty_79"]["fe_placement_4"] = "Отдельная комната";
$demo_content["realty_79"]["fe_live_square_4"] = 1600;
$demo_content["realty_79"]["fe_bd_rooms_4"] = 2;
$demo_content["realty_79"]["fe_bth_rooms_4"] = 1;
$demo_content["realty_79"]["fe_garages_4"] = 0;
$demo_content["realty_79"]["fe_year_4"] = 2009;
$demo_content["realty_79"]["fe_floor_number_4"] = 6;
$demo_content["realty_79"]["fe_total_floors_4"] = 14;
$demo_content["realty_79"]["fe_distance_to_subway_4"] = 15;
$demo_content["realty_79"]["fe_furniture_4"] = 1;
$demo_content["realty_79"]["fe_accomodation_4_option_1"] = 1;
$demo_content["realty_79"]["fe_accomodation_4_option_4"] = 1;
$demo_content["realty_79"]["fe_accomodation_4_option_6"] = 1;
$demo_content["realty_79"]["fe_accomodation_4_option_7"] = 1;
$demo_content["realty_79"]["fe_accomodation_4_option_8"] = 1;
$demo_content["realty_79"]["fe_appliances_4_option_1"] = 1;
$demo_content["realty_79"]["fe_appliances_4_option_5"] = 1;
$demo_content["realty_79"]["fe_appliances_4_option_6"] = 1;
$demo_content["realty_79"]["fe_appliances_4_option_8"] = 1;
$demo_content["realty_79"]["fe_appliances_4_option_9"] = 1;
$demo_content["realty_79"]["fe_amenities_4_option_1"] = 1;
$demo_content["realty_79"]["fe_amenities_4_option_6"] = 1;
$demo_content["realty_79"]["fe_amenities_4_option_7"] = 1;
$demo_content["realty_79"]["fe_amenities_4_option_14"] = 1;
$demo_content["realty_79"]["fe_amenities_4_option_15"] = 1;
$demo_content["realty_79"]["fe_environment_4_option_1"] = 1;
$demo_content["realty_79"]["fe_environment_4_option_8"] = 1;
$demo_content["realty_79"]["fe_environment_4_option_9"] = 1;
$demo_content["realty_79"]["fe_environment_4_option_17"] = 1;
$demo_content["realty_79"]["fe_environment_4_option_18"] = 1; 
$demo_content["realty_79"]["headline"] = "Beautiful Well Maintained All Brick Semi In A Fantastic Location!";
$demo_content["realty_79"]["fe_comments_4"] = "All Elfs, Cac, Window Coverings, Appliances - Fridge, Stove, Dishwasher, Laundry Washer, Dryer, Central Vac Rough In.";

/*$demo_content["realty_80"]["id"] = 80;
$demo_content["realty_80"]["id_user"] = "Pilot Group";
$demo_content["realty_80"]["id_type"] = "lease";
$demo_content["realty_80"]["id_category"] = "Commercial";
$demo_content["realty_80"]["property_type"] = "Other";
$demo_content["realty_80"]["status"] = 1;
$demo_content["realty_80"]["sold"] = 0;
$demo_content["realty_80"]["date_open"] = "2014-09-18 00:00:00";
$demo_content["realty_80"]["date_available"] = "2014-09-18 00:00:00";
$demo_content["realty_80"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_80"]["price"] = 750;
$demo_content["realty_80"]["price_max"] = 900;
$demo_content["realty_80"]["gid_currency"] = "USD";
$demo_content["realty_80"]["price_period"] = 2;
$demo_content["realty_80"]["price_type"] = 1;
$demo_content["realty_80"]["square"] = 1300;
$demo_content["realty_80"]["square_max"] = 1400;
$demo_content["realty_80"]["square_unit"] = "sqft";
$demo_content["realty_80"]["id_country"] = "United Kingdom";
$demo_content["realty_80"]["id_region"] = "Greater London";
$demo_content["realty_80"]["id_city"] = "London";
$demo_content["realty_80"]["zip"] = "W1B 2HW";
$demo_content["realty_80"]["lat"] = "51.5169720";
$demo_content["realty_80"]["lon"] = "-0.1428562"; 
$demo_content["realty_80"]["fe_year_5"] = 1994;
$demo_content["realty_80"]["fe_revenue_5"] = 65;
$demo_content["realty_80"]["fe_cash_flow_5"] = 87000;
$demo_content["realty_80"]["fe_years_established_5"] = 10;
$demo_content["realty_80"]["fe_employees_5"] = 25;
$demo_content["realty_80"]["fe_reason_for_selling_5"] = "";
$demo_content["realty_80"]["headline"] = "Executive Home In Castlemore Area, Stone & Stucco Elevation.";
$demo_content["realty_80"]["fe_comments_5"] = "All Window Coverings & Blinds, Central A/C, Garage Door Opener With Remote.";

$demo_content["realty_81"]["id"] = 81;
$demo_content["realty_81"]["id_user"] = "Centurey Wolf's";
$demo_content["realty_81"]["id_type"] = "lease";
$demo_content["realty_81"]["id_category"] = "Lots/lands";
$demo_content["realty_81"]["property_type"] = "Pasture";
$demo_content["realty_81"]["status"] = 1;
$demo_content["realty_81"]["sold"] = 0;
$demo_content["realty_81"]["date_open"] = "2014-09-18 00:00:00";
$demo_content["realty_81"]["date_available"] = "2014-09-18 00:00:00";
$demo_content["realty_81"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_81"]["price"] = 350;
$demo_content["realty_81"]["price_max"] = 400;
$demo_content["realty_81"]["gid_currency"] = "USD";
$demo_content["realty_81"]["price_period"] = 2;
$demo_content["realty_81"]["price_type"] = 1;
$demo_content["realty_81"]["square"] = 800;
$demo_content["realty_81"]["square_max"] = 900;
$demo_content["realty_81"]["square_unit"] = "sqft";
$demo_content["realty_81"]["id_country"] = "France";
$demo_content["realty_81"]["id_region"] = "Région Île-de-France";
$demo_content["realty_81"]["id_city"] = "Paris";
$demo_content["realty_81"]["zip"] = "75002";
$demo_content["realty_81"]["lat"] = "48.8698619";
$demo_content["realty_81"]["lon"] = "2.3395384"; 
$demo_content["realty_81"]["fe_irrigated_6"] = 0;
$demo_content["realty_81"]["fe_residence_6"] = 0;
$demo_content["realty_81"]["headline"] = "In Town Lot Available. 1.36 Acres With 382.63 Ft Of Highway Frontage.";
$demo_content["realty_81"]["fe_comments_6"] = "B/I Dishwasher, Stove, Fridge, Washer, Dryer, Microwave, Water Softener, Security System (Assume Contract) All Custom Window Treatments, All Elf's, All Furnishing On Property Is Negotiable.";

$demo_content["realty_82"]["id"] = 82;
$demo_content["realty_82"]["id_user"] = "Centurey Wolf's";
$demo_content["realty_82"]["id_type"] = "lease";
$demo_content["realty_82"]["id_category"] = "Residential";
$demo_content["realty_82"]["property_type"] = "Apartment";
$demo_content["realty_82"]["status"] = 1;
$demo_content["realty_82"]["sold"] = 0;
$demo_content["realty_82"]["date_open"] = "2014-10-10 00:00:00";
$demo_content["realty_82"]["date_available"] = "2014-10-10 00:00:00";
$demo_content["realty_82"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_82"]["price"] = 25;
$demo_content["realty_82"]["price_max"] = 40;
$demo_content["realty_82"]["gid_currency"] = "USD";
$demo_content["realty_82"]["price_period"] = 1;
$demo_content["realty_82"]["price_type"] = 1;
$demo_content["realty_82"]["square"] = 1200;
$demo_content["realty_82"]["square_max"] = 1290;
$demo_content["realty_82"]["square_unit"] = "sqft";
$demo_content["realty_82"]["id_country"] = "Germany";
$demo_content["realty_82"]["id_region"] = "Land Berlin";
$demo_content["realty_82"]["id_city"] = "Berlin";
$demo_content["realty_82"]["zip"] = "10719";
$demo_content["realty_82"]["lat"] = "52.5005160";
$demo_content["realty_82"]["lon"] = "13.3140213"; 
$demo_content["realty_82"]["fe_check_in_4"] = "14:00";
$demo_content["realty_82"]["fe_check_out_4"] = "10:00";
$demo_content["realty_82"]["fe_room_type_4"] = "Premium room";
$demo_content["realty_82"]["fe_guests_4"] = "3"; 
$demo_content["realty_82"]["fe_placement_4"] = "Дом/квартира целиком";
$demo_content["realty_82"]["fe_live_square_4"] = 1560;
$demo_content["realty_82"]["fe_bd_rooms_4"] = 2;
$demo_content["realty_82"]["fe_bth_rooms_4"] = 1;
$demo_content["realty_82"]["fe_garages_4"] = 0;
$demo_content["realty_82"]["fe_year_4"] = 2011;
$demo_content["realty_82"]["fe_floor_number_4"] = 5;
$demo_content["realty_82"]["fe_total_floors_4"] = 9;
$demo_content["realty_82"]["fe_distance_to_subway_4"] = 0;
$demo_content["realty_82"]["fe_furniture_4"] = 1;
$demo_content["realty_82"]["fe_accomodation_4_option_1"] = 1;
$demo_content["realty_82"]["fe_accomodation_4_option_4"] = 1;
$demo_content["realty_82"]["fe_accomodation_4_option_8"] = 1;
$demo_content["realty_82"]["fe_accomodation_4_option_9"] = 1;
$demo_content["realty_82"]["fe_accomodation_4_option_12"] = 1;
$demo_content["realty_82"]["fe_appliances_4_option_4"] = 1;
$demo_content["realty_82"]["fe_appliances_4_option_5"] = 1;
$demo_content["realty_82"]["fe_appliances_4_option_7"] = 1;
$demo_content["realty_82"]["fe_appliances_4_option_9"] = 1;
$demo_content["realty_82"]["fe_appliances_4_option_10"] = 1;
$demo_content["realty_82"]["fe_amenities_4_option_11"] = 1;
$demo_content["realty_82"]["fe_amenities_4_option_12"] = 1;
$demo_content["realty_82"]["fe_amenities_4_option_13"] = 1;
$demo_content["realty_82"]["fe_amenities_4_option_14"] = 1;
$demo_content["realty_82"]["fe_amenities_4_option_15"] = 1;
$demo_content["realty_82"]["fe_environment_4_option_3"] = 1;
$demo_content["realty_82"]["fe_environment_4_option_4"] = 1;
$demo_content["realty_82"]["fe_environment_4_option_6"] = 1;
$demo_content["realty_82"]["fe_environment_4_option_8"] = 1;
$demo_content["realty_82"]["fe_environment_4_option_9"] = 1;
$demo_content["realty_82"]["headline"] = "Stunning Executive (Kylemore Built) Home In Wonderful Lakeside Community.";
$demo_content["realty_82"]["fe_comments_4"] = "Add'l Study/Playroom & 3Pc Bath. 10Ft Vaulted Great Room Ceiling. Hunter Douglas Blinds On All Windows, 8Ft Doors To Backyard Deck, Interlock Driveway, Security System. Incl All Elf's, B/I Dishwasher, Intercom System. Spacious Bright Home.";

$demo_content["realty_83"]["id"] = 83;
$demo_content["realty_83"]["id_user"] = "Centurey Wolf's";
$demo_content["realty_83"]["id_type"] = "lease";
$demo_content["realty_83"]["id_category"] = "Commercial";
$demo_content["realty_83"]["property_type"] = "Café";
$demo_content["realty_83"]["status"] = 1;
$demo_content["realty_83"]["sold"] = 0;
$demo_content["realty_83"]["date_open"] = "2014-12-05 00:00:00";
$demo_content["realty_83"]["date_available"] = "2014-12-31 00:00:00";
$demo_content["realty_83"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_83"]["price"] = 250;
$demo_content["realty_83"]["price_max"] = 300;
$demo_content["realty_83"]["gid_currency"] = "USD";
$demo_content["realty_83"]["price_period"] = 1;
$demo_content["realty_83"]["price_type"] = 1;
$demo_content["realty_83"]["square"] = 1500;
$demo_content["realty_83"]["square_max"] = 1700;
$demo_content["realty_83"]["square_unit"] = "sqft";
$demo_content["realty_83"]["id_country"] = "Spain";
$demo_content["realty_83"]["id_region"] = "Comunidad de Madrid";
$demo_content["realty_83"]["id_city"] = "Madrid";
$demo_content["realty_83"]["zip"] = "28108";
$demo_content["realty_83"]["lat"] = "40.4370994";
$demo_content["realty_83"]["lon"] = "-3.6641573"; 
$demo_content["realty_83"]["fe_year_5"] = 1999;
$demo_content["realty_83"]["fe_revenue_5"] = 75;
$demo_content["realty_83"]["fe_cash_flow_5"] = 23000;
$demo_content["realty_83"]["fe_years_established_5"] = 15;
$demo_content["realty_83"]["fe_employees_5"] = 8;
$demo_content["realty_83"]["fe_reason_for_selling_5"] = "";
$demo_content["realty_83"]["headline"] = "Beautiful 2122 Sq.Ft. Baywood Built 4-Bedroom Home On A Quiet Cul-De-Sac.";
$demo_content["realty_83"]["fe_comments_5"] = "Covered Front Porch And Large Double-Wide Driveway For Lots Of Parking. Located Close To All Amenities Including Park, Schools, Hospital And Library.";

$demo_content["realty_84"]["id"] = 84;
$demo_content["realty_84"]["id_user"] = "The Gouglas Realty";
$demo_content["realty_84"]["id_type"] = "lease";
$demo_content["realty_84"]["id_category"] = "Lots/lands";
$demo_content["realty_84"]["property_type"] = "Timberland";
$demo_content["realty_84"]["status"] = 1;
$demo_content["realty_84"]["sold"] = 0;
$demo_content["realty_84"]["date_open"] = "2015-02-13 00:00:00";
$demo_content["realty_84"]["date_available"] = "2015-02-13 00:00:00";
$demo_content["realty_84"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_84"]["price"] = 150;
$demo_content["realty_84"]["price_max"] = 180;
$demo_content["realty_84"]["gid_currency"] = "USD";
$demo_content["realty_84"]["price_period"] = 1;
$demo_content["realty_84"]["price_type"] = 1;
$demo_content["realty_84"]["square"] = 1000;
$demo_content["realty_84"]["square_max"] = 1500;
$demo_content["realty_84"]["square_unit"] = "sqft";
$demo_content["realty_84"]["id_country"] = "Canada";
$demo_content["realty_84"]["id_region"] = "Ontario";
$demo_content["realty_84"]["id_city"] = "Toronto";
$demo_content["realty_84"]["zip"] = "M5B 2A2";
$demo_content["realty_84"]["lat"] = "43.6605244";
$demo_content["realty_84"]["lon"] = "-79.3789268";
$demo_content["realty_84"]["fe_irrigated_6"] = 0;
$demo_content["realty_84"]["fe_residence_6"] = 0; 
$demo_content["realty_84"]["headline"] = "Spectacular Open Concept Family Home On A Premium Lot Backing On To The Oak Ridges Conservation Area.";
$demo_content["realty_84"]["fe_comments_6"] = "Stainless Steel Fridge, Gas Stove, Dishwasher, Washer/Dryer, Over-The-Hood Microwave, Electrical Light Fixtures (Excl Chandeliers), All Window Coverings, Garage Door Opener With Remote.";

$demo_content["realty_85"]["id"] = 85;
$demo_content["realty_85"]["id_user"] = "Coldwell Banker Dolphin Realty";
$demo_content["realty_85"]["id_type"] = "lease";
$demo_content["realty_85"]["id_category"] = "Residential";
$demo_content["realty_85"]["property_type"] = "Room";
$demo_content["realty_85"]["status"] = 1;
$demo_content["realty_85"]["sold"] = 0;
$demo_content["realty_85"]["date_open"] = "2015-04-02 00:00:00";
$demo_content["realty_85"]["date_available"] = "2015-04-02 00:00:00";
$demo_content["realty_85"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_85"]["price"] = 6;
$demo_content["realty_85"]["price_max"] = 15;
$demo_content["realty_85"]["gid_currency"] = "USD";
$demo_content["realty_85"]["price_period"] = 1;
$demo_content["realty_85"]["price_type"] = 1;
$demo_content["realty_85"]["square"] = 50;
$demo_content["realty_85"]["square_max"] = 70;
$demo_content["realty_85"]["square_unit"] = "sqft";
$demo_content["realty_85"]["id_country"] = "Russia";
$demo_content["realty_85"]["id_region"] = "Moscow";
$demo_content["realty_85"]["id_city"] = "Moscow";
$demo_content["realty_85"]["zip"] = "119034";
$demo_content["realty_85"]["lat"] = "55.7372740";
$demo_content["realty_85"]["lon"] = "37.6023540"; 
$demo_content["realty_85"]["fe_check_in_4"] = "14:00";
$demo_content["realty_85"]["fe_check_out_4"] = "10:00";
$demo_content["realty_85"]["fe_room_type_4"] = "Single room";
$demo_content["realty_85"]["fe_guests_4"] = "3";
$demo_content["realty_85"]["fe_placement_4"] = "Отдельная комната";
$demo_content["realty_85"]["fe_live_square_4"] = 2100;
$demo_content["realty_85"]["fe_bd_rooms_4"] = 3;
$demo_content["realty_85"]["fe_bth_rooms_4"] = 2;
$demo_content["realty_85"]["fe_garages_4"] = 0;
$demo_content["realty_85"]["fe_year_4"] = 2005;
$demo_content["realty_85"]["fe_floor_number_4"] = 10;
$demo_content["realty_85"]["fe_total_floors_4"] = 20;
$demo_content["realty_85"]["fe_distance_to_subway_4"] = 5;
$demo_content["realty_85"]["fe_furniture_4"] = 1;
$demo_content["realty_85"]["fe_accomodation_4_option_1"] = 1;
$demo_content["realty_85"]["fe_accomodation_4_option_2"] = 1;
$demo_content["realty_85"]["fe_accomodation_4_option_3"] = 1;
$demo_content["realty_85"]["fe_accomodation_4_option_4"] = 1;
$demo_content["realty_85"]["fe_accomodation_4_option_5"] = 1;
$demo_content["realty_85"]["fe_appliances_4_option_1"] = 1;
$demo_content["realty_85"]["fe_appliances_4_option_2"] = 1;
$demo_content["realty_85"]["fe_appliances_4_option_3"] = 1;
$demo_content["realty_85"]["fe_appliances_4_option_5"] = 1;
$demo_content["realty_85"]["fe_appliances_4_option_8"] = 1;
$demo_content["realty_85"]["fe_amenities_4_option_1"] = 1;
$demo_content["realty_85"]["fe_amenities_4_option_2"] = 1;
$demo_content["realty_85"]["fe_amenities_4_option_3"] = 1;
$demo_content["realty_85"]["fe_amenities_4_option_4"] = 1;
$demo_content["realty_85"]["fe_amenities_4_option_5"] = 1;
$demo_content["realty_85"]["fe_environment_4_option_1"] = 1;
$demo_content["realty_85"]["fe_environment_4_option_2"] = 1;
$demo_content["realty_85"]["fe_environment_4_option_3"] = 1;
$demo_content["realty_85"]["fe_environment_4_option_4"] = 1;
$demo_content["realty_85"]["fe_environment_4_option_5"] = 1;
$demo_content["realty_85"]["headline"] = "Gracious 3 Storey In The Park Area. Hardwood Floors Throughout. Large Principal Rooms, High Ceilings, Main Floor Addition Provides For Spacious Kitchen With Adjacent Sitting Area/Breakfast Room With Walkout To Yard & Pool.";
$demo_content["realty_85"]["fe_comments_4"] = "Include: Electric Light Fixtures, All Pool Equipment (Pool Is Chlorine), Built-In Dishwasher. Hot Water Tank(Rental).";

$demo_content["realty_86"]["id"] = 86;
$demo_content["realty_86"]["id_user"] = "Marina Group Ltd";
$demo_content["realty_86"]["id_type"] = "lease";
$demo_content["realty_86"]["id_category"] = "Commercial";
$demo_content["realty_86"]["property_type"] = "Office";
$demo_content["realty_86"]["status"] = 1;
$demo_content["realty_86"]["sold"] = 0;
$demo_content["realty_86"]["date_open"] = "2015-07-12 00:00:00";
$demo_content["realty_86"]["date_available"] = "2015-07-12 00:00:00";
$demo_content["realty_86"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_86"]["price"] = 200;
$demo_content["realty_86"]["price_max"] = 250;
$demo_content["realty_86"]["gid_currency"] = "USD";
$demo_content["realty_86"]["price_period"] = 2;
$demo_content["realty_86"]["price_type"] = 1;
$demo_content["realty_86"]["square"] = 250;
$demo_content["realty_86"]["square_max"] = 300;
$demo_content["realty_86"]["square_unit"] = "sqft";
$demo_content["realty_86"]["id_country"] = "United States";
$demo_content["realty_86"]["id_region"] = "New York";
$demo_content["realty_86"]["id_city"] = "New York City";
$demo_content["realty_86"]["zip"] = "10016";
$demo_content["realty_86"]["lat"] = "40.7518153";
$demo_content["realty_86"]["lon"] = "-73.9804736"; 
$demo_content["realty_86"]["fe_year_5"] = 2008;
$demo_content["realty_86"]["fe_revenue_5"] = 45;
$demo_content["realty_86"]["fe_cash_flow_5"] = 100000;
$demo_content["realty_86"]["fe_years_established_5"] = 10;
$demo_content["realty_86"]["fe_employees_5"] = 15;
$demo_content["realty_86"]["fe_reason_for_selling_5"] = "";
$demo_content["realty_86"]["headline"] = "Rarely Offered 4 Bedroom Semi-Detached In John Boddy Community.";
$demo_content["realty_86"]["fe_comments_5"] = "Fridge,Stove, B/I Dishwasher, Washer, Dryer, All Exsisting Light Fixtures All Window Coverings";

$demo_content["realty_87"]["id"] = 87;
$demo_content["realty_87"]["id_user"] = "Marina Group Ltd";
$demo_content["realty_87"]["id_type"] = "lease";
$demo_content["realty_87"]["id_category"] = "Lots/lands";
$demo_content["realty_87"]["property_type"] = "Undeveloped land";
$demo_content["realty_87"]["status"] = 1;
$demo_content["realty_87"]["sold"] = 0;
$demo_content["realty_87"]["date_open"] = "2015-09-02 00:00:00";
$demo_content["realty_87"]["date_available"] = "2015-09-02 00:00:00";
$demo_content["realty_87"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_87"]["price"] = 100;
$demo_content["realty_87"]["price_max"] = 150;
$demo_content["realty_87"]["gid_currency"] = "USD";
$demo_content["realty_87"]["price_period"] = 2;
$demo_content["realty_87"]["price_type"] = 1;
$demo_content["realty_87"]["square"] = 650;
$demo_content["realty_87"]["square_max"] = 1000;
$demo_content["realty_87"]["square_unit"] = "sqft";
$demo_content["realty_87"]["id_country"] = "United Kingdom";
$demo_content["realty_87"]["id_region"] = "Greater London";
$demo_content["realty_87"]["id_city"] = "London";
$demo_content["realty_87"]["zip"] = "SW7 1PT";
$demo_content["realty_87"]["lat"] = "51.5016583";
$demo_content["realty_87"]["lon"] = "-0.1698242"; 
$demo_content["realty_87"]["fe_irrigated_6"] = 1;
$demo_content["realty_87"]["fe_residence_6"] = 0; 
$demo_content["realty_87"]["headline"] = "Own Use Or Invest. Spacious Solid-Brick Detach 2-Storey.";
$demo_content["realty_87"]["fe_comments_6"] = "2 Stoves, 1 Fridge, Gas-Boiler. New Survey (Nov.2010). Electrical Light Fixtures.";

$demo_content["realty_88"]["id"] = 88;
$demo_content["realty_88"]["id_user"] = "Ann Fox";
$demo_content["realty_88"]["id_type"] = "lease";
$demo_content["realty_88"]["id_category"] = "Residential";
$demo_content["realty_88"]["property_type"] = "Duplex";
$demo_content["realty_88"]["status"] = 1;
$demo_content["realty_88"]["sold"] = 0;
$demo_content["realty_88"]["date_open"] = "2015-10-24 00:00:00";
$demo_content["realty_88"]["date_available"] = "2015-10-24 00:00:00";
$demo_content["realty_88"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_88"]["price"] = 50;
$demo_content["realty_88"]["price_max"] = 70;
$demo_content["realty_88"]["gid_currency"] = "USD";
$demo_content["realty_88"]["price_period"] = 1;
$demo_content["realty_88"]["price_type"] = 1;
$demo_content["realty_88"]["square"] = 350;
$demo_content["realty_88"]["square_max"] = 400;
$demo_content["realty_88"]["square_unit"] = "sqft";
$demo_content["realty_88"]["id_country"] = "France";
$demo_content["realty_88"]["id_region"] = "Région Île-de-France";
$demo_content["realty_88"]["id_city"] = "Paris";
$demo_content["realty_88"]["zip"] = "75007";
$demo_content["realty_88"]["lat"] = "48.8569775";
$demo_content["realty_88"]["lon"] = "2.3296994"; 
$demo_content["realty_88"]["fe_check_in_4"] = "14:00";
$demo_content["realty_88"]["fe_check_out_4"] = "10:00";
$demo_content["realty_88"]["fe_room_type_4"] = "Single room";
$demo_content["realty_88"]["fe_guests_4"] = "2";
$demo_content["realty_88"]["fe_placement_4"] = "Отдельная комната";
$demo_content["realty_88"]["fe_live_square_4"] = 2700;
$demo_content["realty_88"]["fe_bd_rooms_4"] = 3;
$demo_content["realty_88"]["fe_bth_rooms_4"] = 2;
$demo_content["realty_88"]["fe_garages_4"] = 1;
$demo_content["realty_88"]["fe_year_4"] = 2013;
$demo_content["realty_88"]["fe_floor_number_4"] = 8;
$demo_content["realty_88"]["fe_total_floors_4"] = 9;
$demo_content["realty_88"]["fe_distance_to_subway_4"] = 5;
$demo_content["realty_88"]["fe_furniture_4"] = 1;
$demo_content["realty_88"]["fe_accomodation_4_option_1"] = 1;
$demo_content["realty_88"]["fe_accomodation_4_option_2"] = 1;
$demo_content["realty_88"]["fe_accomodation_4_option_3"] = 1;
$demo_content["realty_88"]["fe_accomodation_4_option_4"] = 1;
$demo_content["realty_88"]["fe_accomodation_4_option_5"] = 1;
$demo_content["realty_88"]["fe_appliances_4_option_1"] = 1;
$demo_content["realty_88"]["fe_appliances_4_option_2"] = 1;
$demo_content["realty_88"]["fe_appliances_4_option_3"] = 1;
$demo_content["realty_88"]["fe_appliances_4_option_4"] = 1;
$demo_content["realty_88"]["fe_appliances_4_option_5"] = 1;
$demo_content["realty_88"]["fe_amenities_4_option_1"] = 1;
$demo_content["realty_88"]["fe_amenities_4_option_2"] = 1;
$demo_content["realty_88"]["fe_amenities_4_option_3"] = 1;
$demo_content["realty_88"]["fe_amenities_4_option_4"] = 1;
$demo_content["realty_88"]["fe_amenities_4_option_5"] = 1;
$demo_content["realty_88"]["fe_environment_4_option_1"] = 1;
$demo_content["realty_88"]["fe_environment_4_option_2"] = 1;
$demo_content["realty_88"]["fe_environment_4_option_3"] = 1;
$demo_content["realty_88"]["fe_environment_4_option_4"] = 1;
$demo_content["realty_88"]["fe_environment_4_option_5"] = 1;
$demo_content["realty_88"]["headline"] = "Beautiful, Updates Bungalow In The Heart Of Acton.";
$demo_content["realty_88"]["fe_comments_4"] = "Inclusions: Fridge, Stove, Micro, Dw, Washer/Dryer, Gdo, Cvac, Elf's (Except D/R & 3rd Bdrm), Window Coverings Exclusions: Chandelier In Dining Room & Chandelier In 3rd Bedroom With Porch. Stained Glass In Dining Room Window.";

$demo_content["realty_89"]["id"] = 89;
$demo_content["realty_89"]["id_user"] = "Ann Fox";
$demo_content["realty_89"]["id_type"] = "lease";
$demo_content["realty_89"]["id_category"] = "Commercial";
$demo_content["realty_89"]["property_type"] = "Shop";
$demo_content["realty_89"]["status"] = 1;
$demo_content["realty_89"]["sold"] = 0;
$demo_content["realty_89"]["date_open"] = "2015-10-12 00:00:00";
$demo_content["realty_89"]["date_available"] = "2015-10-12 00:00:00";
$demo_content["realty_89"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_89"]["price"] = 350;
$demo_content["realty_89"]["price_max"] = 500;
$demo_content["realty_89"]["gid_currency"] = "USD";
$demo_content["realty_89"]["price_period"] = 1;
$demo_content["realty_89"]["price_type"] = 1;
$demo_content["realty_89"]["square"] = 2350;
$demo_content["realty_89"]["square_max"] = 2450;
$demo_content["realty_89"]["square_unit"] = "sqft";
$demo_content["realty_89"]["id_country"] = "Germany";
$demo_content["realty_89"]["id_region"] = "Land Berlin";
$demo_content["realty_89"]["id_city"] = "Berlin";;
$demo_content["realty_89"]["zip"] = "10179";
$demo_content["realty_89"]["lat"] = "52.5191710";
$demo_content["realty_89"]["lon"] = "13.4060912"; 
$demo_content["realty_89"]["fe_year_5"] = 1978;
$demo_content["realty_89"]["fe_revenue_5"] = 75;
$demo_content["realty_89"]["fe_cash_flow_5"] = 450000;
$demo_content["realty_89"]["fe_years_established_5"] = 12;
$demo_content["realty_89"]["fe_employees_5"] = 120;
$demo_content["realty_89"]["fe_reason_for_selling_5"] = "";
$demo_content["realty_89"]["headline"] = "An In Town Lot Approx 1.5 Acres In Size In The Un-Incorporated Community Of Port Loring.";
$demo_content["realty_89"]["fe_comments_5"] = "Will Be Granted By Seller. All Offers Must Be Conditional Upon Severance Approval.";

$demo_content["realty_90"]["id"] = 90;
$demo_content["realty_90"]["id_user"] = "Barbara Close";
$demo_content["realty_90"]["id_type"] = "lease";
$demo_content["realty_90"]["id_category"] = "Lots/lands";
$demo_content["realty_90"]["property_type"] = "Other";
$demo_content["realty_90"]["status"] = 1;
$demo_content["realty_90"]["sold"] = 0;
$demo_content["realty_90"]["date_open"] = "2015-12-01 00:00:00";
$demo_content["realty_90"]["date_available"] = "2015-12-01 00:00:00";
$demo_content["realty_90"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_90"]["price"] = 250;
$demo_content["realty_90"]["price_max"] = 280;
$demo_content["realty_90"]["gid_currency"] = "USD";
$demo_content["realty_90"]["price_period"] = 1;
$demo_content["realty_90"]["price_type"] = 1;
$demo_content["realty_90"]["square"] = 1250;
$demo_content["realty_90"]["square_max"] = 1290;
$demo_content["realty_90"]["square_unit"] = "sqft";
$demo_content["realty_90"]["id_country"] = "Spain";
$demo_content["realty_90"]["id_region"] = "Comunidad de Madrid";
$demo_content["realty_90"]["id_city"] = "Madrid";
$demo_content["realty_90"]["zip"] = "28014";
$demo_content["realty_90"]["lat"] = "40.4069322";
$demo_content["realty_90"]["lon"] = "-3.6831952";
$demo_content["realty_90"]["fe_irrigated_6"] = 1;
$demo_content["realty_90"]["fe_residence_6"] = 1; 
$demo_content["realty_90"]["headline"] = "Executive Fernbrook Built Home Siding On Lush Ravine.";
$demo_content["realty_90"]["fe_comments_6"] = "Soaker Tub & Separate Shower Ensuite.3 Other Good Sized Bedrooms.Gorgeous Nanny/In-Law Suite In The Lower Level.";

$demo_content["realty_91"]["id"] = 91;
$demo_content["realty_91"]["id_user"] = "Forsaleby2007 Tester";
$demo_content["realty_91"]["id_type"] = "lease";
$demo_content["realty_91"]["id_category"] = "Residential";
$demo_content["realty_91"]["property_type"] = "Single Family Home";
$demo_content["realty_91"]["status"] = 1;
$demo_content["realty_91"]["sold"] = 0;
$demo_content["realty_91"]["date_open"] = "2015-12-01 00:00:00";
$demo_content["realty_91"]["date_available"] = "2015-12-01 00:00:00";
$demo_content["realty_91"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_91"]["price"] = 50;
$demo_content["realty_91"]["price_max"] = 70;
$demo_content["realty_91"]["gid_currency"] = "USD";
$demo_content["realty_91"]["price_period"] = 1;
$demo_content["realty_91"]["price_type"] = 2;
$demo_content["realty_91"]["square"] = 850;
$demo_content["realty_91"]["square_max"] = 1250;
$demo_content["realty_91"]["square_unit"] = "sqft";
$demo_content["realty_91"]["id_country"] = "Canada";
$demo_content["realty_91"]["id_region"] = "Ontario";
$demo_content["realty_91"]["id_city"] = "Toronto";
$demo_content["realty_91"]["zip"] = "M5H 2S8";
$demo_content["realty_91"]["lat"] = "43.6500840";
$demo_content["realty_91"]["lon"] = "-79.3810854"; 
$demo_content["realty_91"]["fe_check_in_4"] = "14:00";
$demo_content["realty_91"]["fe_check_out_4"] = "10:00";
$demo_content["realty_91"]["fe_room_type_4"] = "Single room";
$demo_content["realty_91"]["fe_guests_4"] = "1";
$demo_content["realty_91"]["fe_placement_4"] = "Дом/квартира целиком";
$demo_content["realty_91"]["fe_live_square_4"] = 3400;
$demo_content["realty_91"]["fe_bd_rooms_4"] = 4;
$demo_content["realty_91"]["fe_bth_rooms_4"] = 2;
$demo_content["realty_91"]["fe_garages_4"] = 1;
$demo_content["realty_91"]["fe_year_4"] = 2013;
$demo_content["realty_91"]["fe_floor_number_4"] = 5;
$demo_content["realty_91"]["fe_total_floors_4"] = 10;
$demo_content["realty_91"]["fe_distance_to_subway_4"] = 5;
$demo_content["realty_91"]["fe_furniture_4"] = 1;
$demo_content["realty_91"]["fe_accomodation_4_option_1"] = 1;
$demo_content["realty_91"]["fe_accomodation_4_option_2"] = 1;
$demo_content["realty_91"]["fe_accomodation_4_option_3"] = 1;
$demo_content["realty_91"]["fe_accomodation_4_option_4"] = 1;
$demo_content["realty_91"]["fe_accomodation_4_option_5"] = 1;
$demo_content["realty_91"]["fe_appliances_4_option_1"] = 1;
$demo_content["realty_91"]["fe_appliances_4_option_2"] = 1;
$demo_content["realty_91"]["fe_appliances_4_option_3"] = 1;
$demo_content["realty_91"]["fe_appliances_4_option_4"] = 1;
$demo_content["realty_91"]["fe_appliances_4_option_5"] = 1;
$demo_content["realty_91"]["fe_amenities_4_option_1"] = 1;
$demo_content["realty_91"]["fe_amenities_4_option_2"] = 1;
$demo_content["realty_91"]["fe_amenities_4_option_3"] = 1;
$demo_content["realty_91"]["fe_amenities_4_option_4"] = 1;
$demo_content["realty_91"]["fe_amenities_4_option_5"] = 1;
$demo_content["realty_91"]["fe_environment_4_option_1"] = 1;
$demo_content["realty_91"]["fe_environment_4_option_2"] = 1;
$demo_content["realty_91"]["fe_environment_4_option_3"] = 1;
$demo_content["realty_91"]["fe_environment_4_option_4"] = 1;
$demo_content["realty_91"]["fe_environment_4_option_5"] = 1;
$demo_content["realty_91"]["headline"] = "Beautiful Semi-Detached With Upgraded Kitchens.";
$demo_content["realty_91"]["fe_comments_4"] = "All Elf's, Fridge, Stove, Washer & Dryer Included. Shed In The Backyard. Property That Shows Great & Rented Basement. Property Shows Well. Good House For First Time Home Buyers.... Why To Pay Rent When You Can Buy This Beautiful House.";

$demo_content["realty_92"]["id"] = 92;
$demo_content["realty_92"]["id_user"] = "Mark Louis";
$demo_content["realty_92"]["id_type"] = "lease";
$demo_content["realty_92"]["id_category"] = "Commercial";
$demo_content["realty_92"]["property_type"] = "Restaurant";
$demo_content["realty_92"]["status"] = 1;
$demo_content["realty_92"]["sold"] = 0;
$demo_content["realty_92"]["date_open"] = "2016-01-23 00:00:00";
$demo_content["realty_92"]["date_available"] = "2016-01-23 00:00:00";
$demo_content["realty_92"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_92"]["price"] = 130;
$demo_content["realty_92"]["price_max"] = 150;
$demo_content["realty_92"]["gid_currency"] = "USD";
$demo_content["realty_92"]["price_period"] = 2;
$demo_content["realty_92"]["price_type"] = 1;
$demo_content["realty_92"]["square"] = 1350;
$demo_content["realty_92"]["square_max"] = 1500;
$demo_content["realty_92"]["square_unit"] = "sqft";
$demo_content["realty_92"]["id_country"] = "Russia";
$demo_content["realty_92"]["id_region"] = "Moscow";
$demo_content["realty_92"]["id_city"] = "Moscow";
$demo_content["realty_92"]["zip"] = "143900";
$demo_content["realty_92"]["lat"] = "55.9056699";
$demo_content["realty_92"]["lon"] = "37.5654899"; 
$demo_content["realty_92"]["fe_year_5"] = 1985;
$demo_content["realty_92"]["fe_revenue_5"] = 58;
$demo_content["realty_92"]["fe_cash_flow_5"] = 62000;
$demo_content["realty_92"]["fe_years_established_5"] = 7;
$demo_content["realty_92"]["fe_employees_5"] = 15;
$demo_content["realty_92"]["fe_reason_for_selling_5"] = "";
$demo_content["realty_92"]["headline"] = "It's Sitting Pretty! This 3+2 Bdrm & 2 Bath Detached Bungalow Is Located On A Serene 1/2 Acre Country Lot!";
$demo_content["realty_92"]["fe_comments_5"] = "Bsmt Including Separate Entrance - Ideal For Nanny Suite. Rec Rm With Ceramic Tile, Pot Lights, Large Windows With California Shutters, W/O To Patio Area, 2 Extra Bdrms & 3 Pc Bath. Enjoy Privacy Of Country Living But Mins Away From Town!";

$demo_content["realty_93"]["id"] = 93;
$demo_content["realty_93"]["id_user"] = "Mark Louis";
$demo_content["realty_93"]["id_type"] = "lease";
$demo_content["realty_93"]["id_category"] = "Lots/lands";
$demo_content["realty_93"]["property_type"] = "Farm";
$demo_content["realty_93"]["status"] = 1;
$demo_content["realty_93"]["sold"] = 0;
$demo_content["realty_93"]["date_open"] = "2016-01-01 00:00:00";
$demo_content["realty_93"]["date_available"] = "2016-01-01 00:00:00";
$demo_content["realty_93"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_93"]["price"] = 100;
$demo_content["realty_93"]["price_max"] = 120;
$demo_content["realty_93"]["gid_currency"] = "USD";
$demo_content["realty_93"]["price_period"] = 2;
$demo_content["realty_93"]["price_type"] = 1;
$demo_content["realty_93"]["square"] = 2350;
$demo_content["realty_93"]["square_max"] = 2500;
$demo_content["realty_93"]["square_unit"] = "sqft";
$demo_content["realty_93"]["id_country"] = "United States";
$demo_content["realty_93"]["id_region"] = "New York";
$demo_content["realty_93"]["id_city"] = "New York City";
$demo_content["realty_93"]["zip"] = "11710";
$demo_content["realty_93"]["lat"] = "40.7147600";
$demo_content["realty_93"]["lon"] = "-73.9805000"; 
$demo_content["realty_93"]["fe_irrigated_6"] = 1;
$demo_content["realty_93"]["fe_residence_6"] = 0;
$demo_content["realty_93"]["headline"] = "Family Matters? Then You'll Want To See This Immaculate 4 Bdrm, 2.5 Bath Detached Home.";
$demo_content["realty_93"]["fe_comments_6"] = "Central Air Conditioning, Central Vacuum And Equipment, Window Shutters & Drapes Except As Noted, Garage Door Opener And 1 Remote, Heat Recovery System, Security System, Built-In Dishwasher.";

$demo_content["realty_94"]["id"] = 94;
$demo_content["realty_94"]["id_user"] = "Cornell Commons";
$demo_content["realty_94"]["id_type"] = "lease";
$demo_content["realty_94"]["id_category"] = "Residential";
$demo_content["realty_94"]["property_type"] = "Multi Family Home";
$demo_content["realty_94"]["status"] = 1;
$demo_content["realty_94"]["sold"] = 0;
$demo_content["realty_94"]["date_open"] = "2016-04-22 00:00:00";
$demo_content["realty_94"]["date_available"] = "2016-04-22 00:00:00";
$demo_content["realty_94"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_94"]["price"] = 300;
$demo_content["realty_94"]["price_max"] = 320;
$demo_content["realty_94"]["gid_currency"] = "USD";
$demo_content["realty_94"]["price_period"] = 2;
$demo_content["realty_94"]["price_type"] = 1;
$demo_content["realty_94"]["square"] = 950;
$demo_content["realty_94"]["square_max"] = 1000;
$demo_content["realty_94"]["square_unit"] = "sqft";
$demo_content["realty_94"]["id_country"] = "United Kingdom";
$demo_content["realty_94"]["id_region"] = "Greater London";
$demo_content["realty_94"]["id_city"] = "London";
$demo_content["realty_94"]["zip"] = "E14 5HS";
$demo_content["realty_94"]["lat"] = "51.5058040";
$demo_content["realty_94"]["lon"] = "-0.0201590"; 
$demo_content["realty_94"]["fe_check_in_4"] = "14:00";
$demo_content["realty_94"]["fe_check_out_4"] = "10:00";
$demo_content["realty_94"]["fe_room_type_4"] = "Double room";
$demo_content["realty_94"]["fe_guests_4"] = "4";
$demo_content["realty_94"]["fe_placement_4"] = "Дом/квартира целиком";
$demo_content["realty_94"]["fe_live_square_4"] = 2500;
$demo_content["realty_94"]["fe_bd_rooms_4"] = 3;
$demo_content["realty_94"]["fe_bth_rooms_4"] = 1;
$demo_content["realty_94"]["fe_garages_4"] = 1;
$demo_content["realty_94"]["fe_year_4"] = 2010;
$demo_content["realty_94"]["fe_floor_number_4"] = 3;
$demo_content["realty_94"]["fe_total_floors_4"] = 5;
$demo_content["realty_94"]["fe_distance_to_subway_4"] = 5;
$demo_content["realty_94"]["fe_furniture_4"] = 1;
$demo_content["realty_94"]["fe_accomodation_4_option_1"] = 1;
$demo_content["realty_94"]["fe_accomodation_4_option_2"] = 1;
$demo_content["realty_94"]["fe_accomodation_4_option_3"] = 1;
$demo_content["realty_94"]["fe_accomodation_4_option_4"] = 1;
$demo_content["realty_94"]["fe_accomodation_4_option_5"] = 1;
$demo_content["realty_94"]["fe_appliances_4_option_1"] = 1;
$demo_content["realty_94"]["fe_appliances_4_option_2"] = 1;
$demo_content["realty_94"]["fe_appliances_4_option_3"] = 1;
$demo_content["realty_94"]["fe_appliances_4_option_4"] = 1;
$demo_content["realty_94"]["fe_appliances_4_option_5"] = 1;
$demo_content["realty_94"]["fe_amenities_4_option_1"] = 1;
$demo_content["realty_94"]["fe_amenities_4_option_2"] = 1;
$demo_content["realty_94"]["fe_amenities_4_option_3"] = 1;
$demo_content["realty_94"]["fe_amenities_4_option_4"] = 1;
$demo_content["realty_94"]["fe_amenities_4_option_5"] = 1;
$demo_content["realty_94"]["fe_environment_4_option_1"] = 1;
$demo_content["realty_94"]["fe_environment_4_option_2"] = 1;
$demo_content["realty_94"]["fe_environment_4_option_3"] = 1;
$demo_content["realty_94"]["fe_environment_4_option_4"] = 1;
$demo_content["realty_94"]["fe_environment_4_option_5"] = 1;
$demo_content["realty_94"]["headline"] = "Centrally Located 1 Bedroom Loft Apartment. Spiral Staircase Up To Loft Bedroom With Walk In Closet And Vintage Bathtub.";
$demo_content["realty_94"]["fe_comments_4"] = "Layout With State Of The Art Finishes. Open Concept, Walk-Out Deck, Walk-Out Basement, Nanny Suite, Centre Island With Calacatta Marble Counter & Bar Seating.";

$demo_content["realty_95"]["id"] = 95;
$demo_content["realty_95"]["id_user"] = "Barry Lindon";
$demo_content["realty_95"]["id_type"] = "lease";
$demo_content["realty_95"]["id_category"] = "Commercial";
$demo_content["realty_95"]["property_type"] = "Hotel";
$demo_content["realty_95"]["status"] = 1;
$demo_content["realty_95"]["sold"] = 0;
$demo_content["realty_95"]["date_open"] = "2016-06-08 00:00:00";
$demo_content["realty_95"]["date_available"] = "2016-06-08 00:00:00";
$demo_content["realty_95"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_95"]["price"] = 200;
$demo_content["realty_95"]["price_max"] = 220;
$demo_content["realty_95"]["gid_currency"] = "USD";
$demo_content["realty_95"]["price_period"] = 2;
$demo_content["realty_95"]["price_type"] = 1;
$demo_content["realty_95"]["square"] = 1400;
$demo_content["realty_95"]["square_max"] = 1500;
$demo_content["realty_95"]["square_unit"] = "sqft";
$demo_content["realty_95"]["id_country"] = "France";
$demo_content["realty_95"]["id_region"] = "Région Île-de-France";
$demo_content["realty_95"]["id_city"] = "Paris";
$demo_content["realty_95"]["zip"] = "75001";
$demo_content["realty_95"]["lat"] = "48.8633024";
$demo_content["realty_95"]["lon"] = "2.3488814"; 
$demo_content["realty_95"]["fe_year_5"] = 2205;
$demo_content["realty_95"]["fe_revenue_5"] = 65;
$demo_content["realty_95"]["fe_cash_flow_5"] = 34000;
$demo_content["realty_95"]["fe_years_established_5"] = 6;
$demo_content["realty_95"]["fe_employees_5"] = 4;
$demo_content["realty_95"]["fe_reason_for_selling_5"] = "";
$demo_content["realty_95"]["headline"] = "Beautiful And Spacious 4 Bedroom Country Home, With A Modern Touch.";
$demo_content["realty_95"]["fe_comments_5"] = "10 Minutes To Town, Large Bright Windows In The Basement Rec Room. Great Potential For In Law Suite. 2 Wood Stoves And An Indoor Bbq! Move In Ready With Quick Closing Available! Please See Virtual Tour!";

$demo_content["realty_96"]["id"] = 96;
$demo_content["realty_96"]["id_user"] = "Barry Lindon";
$demo_content["realty_96"]["id_type"] = "lease";
$demo_content["realty_96"]["id_category"] = "Lots/lands";
$demo_content["realty_96"]["property_type"] = "Pasture";
$demo_content["realty_96"]["status"] = 1;
$demo_content["realty_96"]["sold"] = 0;
$demo_content["realty_96"]["date_open"] = "2016-07-21 00:00:00";
$demo_content["realty_96"]["date_available"] = "2016-07-21 00:00:00";
$demo_content["realty_96"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_96"]["price"] = 100;
$demo_content["realty_96"]["price_max"] = 130;
$demo_content["realty_96"]["gid_currency"] = "USD";
$demo_content["realty_96"]["price_period"] = 2;
$demo_content["realty_96"]["price_type"] = 1;
$demo_content["realty_96"]["square"] = 1200;
$demo_content["realty_96"]["square_max"] = 1500;
$demo_content["realty_96"]["square_unit"] = "sqft";
$demo_content["realty_96"]["id_country"] = "Germany";
$demo_content["realty_96"]["id_region"] = "Land Berlin";
$demo_content["realty_96"]["id_city"] = "Berlin";
$demo_content["realty_96"]["zip"] = "17316";
$demo_content["realty_96"]["lat"] = "52.5191710";
$demo_content["realty_96"]["lon"] = "13.4060912";
$demo_content["realty_96"]["fe_irrigated_6"] = 1;
$demo_content["realty_96"]["fe_residence_6"] = 1; 
$demo_content["realty_96"]["headline"] = "Great Value!!! Stunning Newly Built. Just 8 Months Old In High Demand Vellore Village.";
$demo_content["realty_96"]["fe_comments_6"] = "S/S (Fridge,Gas Stove, B/I Dishwasher), Dryer & Washer. All Electric Light Fixtures, Rough In For C-Vac (Excl D/R, Breakfast Elf's & Kids Rooms Window Coverings). Assume Security Alarm (25+Hst/Per Month), Cac.";

$demo_content["realty_97"]["id"] = 97;
$demo_content["realty_97"]["id_user"] = "Andrew Cooper";
$demo_content["realty_97"]["id_type"] = "lease";
$demo_content["realty_97"]["id_category"] = "Residential";
$demo_content["realty_97"]["property_type"] = "Condo";
$demo_content["realty_97"]["status"] = 1;
$demo_content["realty_97"]["sold"] = 0;
$demo_content["realty_97"]["date_open"] = "2016-07-17 00:00:00";
$demo_content["realty_97"]["date_available"] = "2016-07-17 00:00:00";
$demo_content["realty_97"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_97"]["price"] = 20;
$demo_content["realty_97"]["price_max"] = 30;
$demo_content["realty_97"]["gid_currency"] = "USD";
$demo_content["realty_97"]["price_period"] = 1;
$demo_content["realty_97"]["price_type"] = 2;
$demo_content["realty_97"]["square"] = 1200;
$demo_content["realty_97"]["square_max"] = 1500;
$demo_content["realty_97"]["square_unit"] = "sqft";
$demo_content["realty_97"]["id_country"] = "Spain";
$demo_content["realty_97"]["id_region"] = "Comunidad de Madrid";
$demo_content["realty_97"]["id_city"] = "Madrid";
$demo_content["realty_97"]["zip"] = "28014";
$demo_content["realty_97"]["lat"] = "40.4156078";
$demo_content["realty_97"]["lon"] = "-3.6970363"; 
$demo_content["realty_97"]["fe_check_in_4"] = "14:00";
$demo_content["realty_97"]["fe_check_out_4"] = "10:00";
$demo_content["realty_97"]["fe_room_type_4"] = "Single room";
$demo_content["realty_97"]["fe_guests_4"] = "2";
$demo_content["realty_97"]["fe_placement_4"] = "Дом/квартира целиком";
$demo_content["realty_97"]["fe_live_square_4"] = 2000;
$demo_content["realty_97"]["fe_bd_rooms_4"] = 2;
$demo_content["realty_97"]["fe_bth_rooms_4"] = 1;
$demo_content["realty_97"]["fe_garages_4"] = 0;
$demo_content["realty_97"]["fe_year_4"] = 2010;
$demo_content["realty_97"]["fe_floor_number_4"] = 5;
$demo_content["realty_97"]["fe_total_floors_4"] = 9;
$demo_content["realty_97"]["fe_distance_to_subway_4"] = 5;
$demo_content["realty_97"]["fe_furniture_4"] = 1;
$demo_content["realty_97"]["fe_accomodation_4_option_1"] = 1;
$demo_content["realty_97"]["fe_accomodation_4_option_2"] = 1;
$demo_content["realty_97"]["fe_accomodation_4_option_3"] = 1;
$demo_content["realty_97"]["fe_accomodation_4_option_4"] = 1;
$demo_content["realty_97"]["fe_accomodation_4_option_5"] = 1;
$demo_content["realty_97"]["fe_appliances_4_option_1"] = 1;
$demo_content["realty_97"]["fe_appliances_4_option_2"] = 1;
$demo_content["realty_97"]["fe_appliances_4_option_3"] = 1;
$demo_content["realty_97"]["fe_appliances_4_option_4"] = 1;
$demo_content["realty_97"]["fe_appliances_4_option_5"] = 1;
$demo_content["realty_97"]["fe_amenities_4_option_1"] = 1;
$demo_content["realty_97"]["fe_amenities_4_option_2"] = 1;
$demo_content["realty_97"]["fe_amenities_4_option_3"] = 1;
$demo_content["realty_97"]["fe_amenities_4_option_4"] = 1;
$demo_content["realty_97"]["fe_amenities_4_option_5"] = 1;
$demo_content["realty_97"]["fe_environment_4_option_1"] = 1;
$demo_content["realty_97"]["fe_environment_4_option_2"] = 1;
$demo_content["realty_97"]["fe_environment_4_option_3"] = 1;
$demo_content["realty_97"]["fe_environment_4_option_4"] = 1;
$demo_content["realty_97"]["fe_environment_4_option_5"] = 1;
$demo_content["realty_97"]["headline"] = "Highly Sought After Open Concept Pinehurst Model Located In The Ballantrae Golf Club Adult Lifestyle Community.";
$demo_content["realty_97"]["fe_comments_4"] = "Extras: Fridge, Stove, Dishwasher, Washer, Dryer, Water Softener. All Window Coverings, Motorized Awning, Elfs, Cac, Grdo & Remote.";

$demo_content["realty_98"]["id"] = 98;
$demo_content["realty_98"]["id_user"] = "Andrew Cooper";
$demo_content["realty_98"]["id_type"] = "lease";
$demo_content["realty_98"]["id_category"] = "Commercial";
$demo_content["realty_98"]["property_type"] = "Sports facility";
$demo_content["realty_98"]["status"] = 1;
$demo_content["realty_98"]["sold"] = 0;
$demo_content["realty_98"]["date_open"] = "2016-08-12 00:00:00";
$demo_content["realty_98"]["date_available"] = "2016-08-12 00:00:00";
$demo_content["realty_98"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_98"]["price"] = 200;
$demo_content["realty_98"]["price_max"] = 230;
$demo_content["realty_98"]["gid_currency"] = "USD";
$demo_content["realty_98"]["price_period"] = 2;
$demo_content["realty_98"]["price_type"] = 1;
$demo_content["realty_98"]["square"] = 2300;
$demo_content["realty_98"]["square_max"] = 2500;
$demo_content["realty_98"]["square_unit"] = "sqft";
$demo_content["realty_98"]["id_country"] = "Canada";
$demo_content["realty_98"]["id_region"] = "Ontario";
$demo_content["realty_98"]["id_city"] = "Toronto";
$demo_content["realty_98"]["zip"] = "M5J 1B7";
$demo_content["realty_98"]["lat"] = "43.6417163";
$demo_content["realty_98"]["lon"] = "-79.3786465";
$demo_content["realty_98"]["fe_year_5"] = 2001;
$demo_content["realty_98"]["fe_revenue_5"] = 95;
$demo_content["realty_98"]["fe_cash_flow_5"] = 67000;
$demo_content["realty_98"]["fe_years_established_5"] = 10;
$demo_content["realty_98"]["fe_employees_5"] = 7;
$demo_content["realty_98"]["fe_reason_for_selling_5"] = ""; 
$demo_content["realty_98"]["headline"] = "Wow! Huge Bungalow,3+1 Bdrms,2 Kitchens,2Washrms,Walkout Bsmt. ";
$demo_content["realty_98"]["fe_comments_5"] = "Trees, Flowers & Plants - Vegetables Garden. Room In Backyard Self Contained Unit With Light Fixture,Water Connection & Wood Fire Place. S/S Appliances On Main Floor, Fridge, Stove, Washer, Dryer At Lower Level.";

$demo_content["realty_99"]["id"] = 99;
$demo_content["realty_99"]["id_user"] = "Tracy Dorhan";
$demo_content["realty_99"]["id_type"] = "lease";
$demo_content["realty_99"]["id_category"] = "Lots/lands";
$demo_content["realty_99"]["property_type"] = "Timberland";
$demo_content["realty_99"]["status"] = 1;
$demo_content["realty_99"]["sold"] = 0;
$demo_content["realty_99"]["date_open"] = "2016-09-21 00:00:00";
$demo_content["realty_99"]["date_available"] = "2016-09-21 00:00:00";
$demo_content["realty_99"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_99"]["price"] = 100;
$demo_content["realty_99"]["price_max"] = 140;
$demo_content["realty_99"]["gid_currency"] = "USD";
$demo_content["realty_99"]["price_period"] = 2;
$demo_content["realty_99"]["price_type"] = 1;
$demo_content["realty_99"]["square"] = 1700;
$demo_content["realty_99"]["square_max"] = 2000;
$demo_content["realty_99"]["square_unit"] = "sqft";
$demo_content["realty_99"]["id_country"] = "Russia";
$demo_content["realty_99"]["id_region"] = "Moscow";
$demo_content["realty_99"]["id_city"] = "Moscow";
$demo_content["realty_99"]["zip"] = "117485";
$demo_content["realty_99"]["lat"] = "55.7558260";
$demo_content["realty_99"]["lon"] = "37.6173000"; 
$demo_content["realty_99"]["fe_irrigated_6"] = 0;
$demo_content["realty_99"]["fe_residence_6"] = 0;
$demo_content["realty_99"]["headline"] = "Build Your Dream Home In Prestigious Ballantrae Community.";
$demo_content["realty_99"]["fe_comments_6"] = "Stainless Steel Fridge, Stove, Washer, Dryer, B/I Dishwasher, B/I Microwave, Elf's, Central Air.";

$demo_content["realty_100"]["id"] = 100;
$demo_content["realty_100"]["id_user"] = "Tracy Dorhan";
$demo_content["realty_100"]["id_type"] = "lease";
$demo_content["realty_100"]["id_category"] = "Residential";
$demo_content["realty_100"]["property_type"] = "Townhouse";
$demo_content["realty_100"]["status"] = 1;
$demo_content["realty_100"]["sold"] = 0;
$demo_content["realty_100"]["date_open"] = "2016-10-14 00:00:00";
$demo_content["realty_100"]["date_available"] = "2016-10-14 00:00:00";
$demo_content["realty_100"]["date_activity"] = "2013-03-01 00:00:00";
$demo_content["realty_100"]["price"] = 40;
$demo_content["realty_100"]["price_max"] = 60;
$demo_content["realty_100"]["gid_currency"] = "USD";
$demo_content["realty_100"]["price_period"] = 1;
$demo_content["realty_100"]["price_type"] = 2;
$demo_content["realty_100"]["square"] = 1800;
$demo_content["realty_100"]["square_max"] = 2000;
$demo_content["realty_100"]["square_unit"] = "sqft";
$demo_content["realty_100"]["id_country"] = "United Kingdom";
$demo_content["realty_100"]["id_region"] = "Greater London";
$demo_content["realty_100"]["id_city"] = "London";
$demo_content["realty_100"]["zip"] = "N1 9GU";
$demo_content["realty_100"]["lat"] = "51.5346490";
$demo_content["realty_100"]["lon"] = "-0.1221380";
$demo_content["realty_100"]["fe_check_in_4"] = "14:00";
$demo_content["realty_100"]["fe_check_out_4"] = "10:00";
$demo_content["realty_100"]["fe_room_type_4"] = "Single room";
$demo_content["realty_100"]["fe_guests_4"] = "1";
$demo_content["realty_100"]["fe_placement_4"] = "Отдельная комната"; 
$demo_content["realty_100"]["fe_live_square_4"] = 900;
$demo_content["realty_100"]["fe_bd_rooms_4"] = 1;
$demo_content["realty_100"]["fe_bth_rooms_4"] = 1;
$demo_content["realty_100"]["fe_garages_4"] = 0;
$demo_content["realty_100"]["fe_year_4"] = 2002;
$demo_content["realty_100"]["fe_floor_number_4"] = 5;
$demo_content["realty_100"]["fe_total_floors_4"] = 9;
$demo_content["realty_100"]["fe_distance_to_subway_4"] = 5;
$demo_content["realty_100"]["fe_furniture_4"] = 1;
$demo_content["realty_100"]["fe_accomodation_4_option_1"] = 1;
$demo_content["realty_100"]["fe_accomodation_4_option_2"] = 1;
$demo_content["realty_100"]["fe_accomodation_4_option_3"] = 1;
$demo_content["realty_100"]["fe_accomodation_4_option_4"] = 1;
$demo_content["realty_100"]["fe_accomodation_4_option_5"] = 1;
$demo_content["realty_100"]["fe_appliances_4_option_1"] = 1;
$demo_content["realty_100"]["fe_appliances_4_option_2"] = 1;
$demo_content["realty_100"]["fe_appliances_4_option_3"] = 1;
$demo_content["realty_100"]["fe_appliances_4_option_4"] = 1;
$demo_content["realty_100"]["fe_appliances_4_option_5"] = 1;
$demo_content["realty_100"]["fe_amenities_4_option_1"] = 1;
$demo_content["realty_100"]["fe_amenities_4_option_2"] = 1;
$demo_content["realty_100"]["fe_amenities_4_option_3"] = 1;
$demo_content["realty_100"]["fe_amenities_4_option_4"] = 1;
$demo_content["realty_100"]["fe_amenities_4_option_5"] = 1;
$demo_content["realty_100"]["fe_environment_4_option_1"] = 1;
$demo_content["realty_100"]["fe_environment_4_option_2"] = 1;
$demo_content["realty_100"]["fe_environment_4_option_3"] = 1;
$demo_content["realty_100"]["fe_environment_4_option_4"] = 1;
$demo_content["realty_100"]["fe_environment_4_option_5"] = 1;
$demo_content["realty_100"]["headline"] = "Wow! Don't Miss This Normerica-Designed Stunningly Reno'd Private Paradise.";
$demo_content["realty_100"]["fe_comments_4"] = "S/S Stove, Hood-Vent, S/S Fridge, S/S Dishwasher, Custom Built-Ins, Front-End Loading, High-End Washer & Dryer, All Elf's, All Window Coverings, Hi-Eff Furnace, Cac, Water Softener, Iron Filter, 200 Amp Breaker, 2 Gdo & Remotes.";*/

$uploads = array(
	array("id"=>1, "object_id"=>1, "file_name"=>"3b8a8cc7ae.jpg", "settings"=>array('width'=>1500, 'height'=>1004, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>27, "object_id"=>1, "file_name"=>"577540990a.jpg", "settings"=>array('width'=>650, 'height'=>488, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>28, "object_id"=>1, "file_name"=>"ae00497160.jpg", "settings"=>array('width'=>675, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>2, "object_id"=>2, "file_name"=>"14e7bc2e86.jpg", "settings"=>array('width'=>1936, 'height'=>1296, 'use_for_search'=>1, 'use_for_slide_show'=>1)),	
	array("id"=>29, "object_id"=>2, "file_name"=>"7269dada0a.jpg", "settings"=>array('width'=>675, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),	
	array("id"=>30, "object_id"=>2, "file_name"=>"e80ae7bd00.jpg", "settings"=>array('width'=>650, 'height'=>650, 'use_for_search'=>0, 'use_for_slide_show'=>1)),	
	array("id"=>3, "object_id"=>3, "file_name"=>"3624eaddd6.jpg", "settings"=>array('width'=>1728, 'height'=>1152, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>31, "object_id"=>3, "file_name"=>"56a0866c17.jpg", "settings"=>array('width'=>675, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>32, "object_id"=>3, "file_name"=>"f0b5ec814e.jpg", "settings"=>array('width'=>650, 'height'=>472, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>4, "object_id"=>4, "file_name"=>"dab30740c3.jpg", "settings"=>array('width'=>1248, 'height'=>832, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>33, "object_id"=>4, "file_name"=>"f8ce4ccb4e.jpg", "settings"=>array('width'=>675, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>34, "object_id"=>4, "file_name"=>"b177ad5b22.jpg", "settings"=>array('width'=>762, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>5, "object_id"=>5, "file_name"=>"101e5e1a8e.jpg", "settings"=>array('width'=>1451, 'height'=>1001, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>35, "object_id"=>5, "file_name"=>"f999cd63e1.jpg", "settings"=>array('width'=>675, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>36, "object_id"=>5, "file_name"=>"37982df381.jpg", "settings"=>array('width'=>727, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>6, "object_id"=>6, "file_name"=>"96d3b91ebb.jpg", "settings"=>array('width'=>1750, 'height'=>1166, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>37, "object_id"=>6, "file_name"=>"91b489c38f.jpg", "settings"=>array('width'=>650, 'height'=>455, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>38, "object_id"=>6, "file_name"=>"58ab14f9f4.jpg", "settings"=>array('width'=>677, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>7, "object_id"=>7, "file_name"=>"431cd453de.jpg", "settings"=>array('width'=>1456, 'height'=>971, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>39, "object_id"=>7, "file_name"=>"cd98862d36.jpg", "settings"=>array('width'=>921, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>40, "object_id"=>7, "file_name"=>"96dfdbce89.jpg", "settings"=>array('width'=>650, 'height'=>501, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>8, "object_id"=>8, "file_name"=>"1491c4d873.jpg", "settings"=>array('width'=>1818, 'height'=>1230, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>41, "object_id"=>8, "file_name"=>"5b4ed9cd8a.jpg", "settings"=>array('width'=>729, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>42, "object_id"=>8, "file_name"=>"458d079138.jpg", "settings"=>array('width'=>650, 'height'=>884, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>9, "object_id"=>9, "file_name"=>"b98a116a25.jpg", "settings"=>array('width'=>1400, 'height'=>1400, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>43, "object_id"=>9, "file_name"=>"9507ad69c3.jpg", "settings"=>array('width'=>655, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>44, "object_id"=>9, "file_name"=>"8238b43092.jpg", "settings"=>array('width'=>650, 'height'=>650, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>10, "object_id"=>10, "file_name"=>"7efc8e9d77.jpg", "settings"=>array('width'=>1500, 'height'=>1021, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>45, "object_id"=>10, "file_name"=>"9e32f1d210.jpg", "settings"=>array('width'=>658, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>46, "object_id"=>10, "file_name"=>"4eea528489.jpg", "settings"=>array('width'=>650, 'height'=>455, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>11, "object_id"=>11, "file_name"=>"a2e463ceab.jpg", "settings"=>array('width'=>1250, 'height'=>912, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>47, "object_id"=>11, "file_name"=>"71771b6786.jpg", "settings"=>array('width'=>650, 'height'=>812, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>12, "object_id"=>12, "file_name"=>"bde59e34b9.jpg", "settings"=>array('width'=>1944, 'height'=>1296, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>48, "object_id"=>12, "file_name"=>"9c83a3acfd.jpg", "settings"=>array('width'=>678, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>49, "object_id"=>12, "file_name"=>"d075fa409c.jpg", "settings"=>array('width'=>675, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>13, "object_id"=>13, "file_name"=>"f7868998ab.jpg", "settings"=>array('width'=>1404, 'height'=>710, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>50, "object_id"=>13, "file_name"=>"e44cc21498.jpg", "settings"=>array('width'=>654, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>51, "object_id"=>13, "file_name"=>"f150ccddb4.jpg", "settings"=>array('width'=>650, 'height'=>459, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>14, "object_id"=>14, "file_name"=>"e18784d6a7.jpg", "settings"=>array('width'=>1871, 'height'=>1253, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>52, "object_id"=>14, "file_name"=>"f601d85cd4.jpg", "settings"=>array('width'=>675, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>53, "object_id"=>14, "file_name"=>"67ed940a2c.jpg", "settings"=>array('width'=>675, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>15, "object_id"=>15, "file_name"=>"85ca5aeb4d.jpg", "settings"=>array('width'=>2048, 'height'=>1365, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>54, "object_id"=>15, "file_name"=>"a68419b1d2.jpg", "settings"=>array('width'=>650, 'height'=>488, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>55, "object_id"=>15, "file_name"=>"c53ff1eb3d.jpg", "settings"=>array('width'=>675, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>56, "object_id"=>15, "file_name"=>"c6fd6f02b4.jpg", "settings"=>array('width'=>675, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>0)),
	array("id"=>16, "object_id"=>16, "file_name"=>"f076d70829.jpg", "settings"=>array('width'=>1347, 'height'=>907, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>57, "object_id"=>16, "file_name"=>"deb8017de7.jpg", "settings"=>array('width'=>701, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>58, "object_id"=>16, "file_name"=>"29bb3b37be.jpg", "settings"=>array('width'=>675, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>17, "object_id"=>17, "file_name"=>"95a301dc26.jpg", "settings"=>array('width'=>1404, 'height'=>936, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>59, "object_id"=>17, "file_name"=>"705ceff944.jpg", "settings"=>array('width'=>660, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>60, "object_id"=>17, "file_name"=>"e35bbb4970.jpg", "settings"=>array('width'=>675, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>61, "object_id"=>17, "file_name"=>"74e6b1e16b.jpg", "settings"=>array('width'=>675, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>0)),
	array("id"=>62, "object_id"=>17, "file_name"=>"6ea9a76e48.jpg", "settings"=>array('width'=>675, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>0)),
	array("id"=>18, "object_id"=>18, "file_name"=>"3a6fd38758.jpg", "settings"=>array('width'=>1374, 'height'=>924, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>63, "object_id"=>18, "file_name"=>"65bd885633.jpg", "settings"=>array('width'=>675, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>64, "object_id"=>18, "file_name"=>"367658d15e.jpg", "settings"=>array('width'=>675, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>19, "object_id"=>19, "file_name"=>"677eb70de8.jpg", "settings"=>array('width'=>1512, 'height'=>1008, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>65, "object_id"=>19, "file_name"=>"339c2a07cc.jpg", "settings"=>array('width'=>675, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>66, "object_id"=>19, "file_name"=>"aa68554250.jpg", "settings"=>array('width'=>675, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>20, "object_id"=>20, "file_name"=>"56dbb354c1.jpg", "settings"=>array('width'=>2136, 'height'=>1424, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>67, "object_id"=>20, "file_name"=>"b3623b3524.jpg", "settings"=>array('width'=>650, 'height'=>484, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>68, "object_id"=>20, "file_name"=>"1805d37423.jpg", "settings"=>array('width'=>670, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>69, "object_id"=>20, "file_name"=>"b8290b639a.jpg", "settings"=>array('width'=>650, 'height'=>495, 'use_for_search'=>0, 'use_for_slide_show'=>0)),
	array("id"=>21, "object_id"=>21, "file_name"=>"0fb307fa1d.jpg", "settings"=>array('width'=>1248, 'height'=>832, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>70, "object_id"=>21, "file_name"=>"9678e8e248.jpg", "settings"=>array('width'=>650, 'height'=>453, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>71, "object_id"=>21, "file_name"=>"fe8171b6ba.jpg", "settings"=>array('width'=>622, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>22, "object_id"=>22, "file_name"=>"7153ec9cc0.jpg", "settings"=>array('width'=>1404, 'height'=>936, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>72, "object_id"=>22, "file_name"=>"b47562259a.jpg", "settings"=>array('width'=>663, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>23, "object_id"=>23, "file_name"=>"103e2da6e2.jpg", "settings"=>array('width'=>2274, 'height'=>1530, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>73, "object_id"=>23, "file_name"=>"2e00ff7ef9.jpg", "settings"=>array('width'=>650, 'height'=>487, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>24, "object_id"=>24, "file_name"=>"af381c4b23.jpg", "settings"=>array('width'=>1400, 'height'=>1025, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>74, "object_id"=>24, "file_name"=>"7205340053.jpg", "settings"=>array('width'=>675, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>75, "object_id"=>24, "file_name"=>"16375d507f.jpg", "settings"=>array('width'=>672, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>25, "object_id"=>25, "file_name"=>"6008f2c1b5.jpg", "settings"=>array('width'=>2098, 'height'=>1386, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>76, "object_id"=>25, "file_name"=>"e41aaec40e.jpg", "settings"=>array('width'=>650, 'height'=>488, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>77, "object_id"=>25, "file_name"=>"8c94d1ef29.jpg", "settings"=>array('width'=>676, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>26, "object_id"=>26, "file_name"=>"0466d4b8a2.jpg", "settings"=>array('width'=>1250, 'height'=>853, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>78, "object_id"=>26, "file_name"=>"589c5dd4ed.jpg", "settings"=>array('width'=>650, 'height'=>454, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>79, "object_id"=>26, "file_name"=>"8e7ef0f138.jpg", "settings"=>array('width'=>643, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>80, "object_id"=>26, "file_name"=>"edb1a1700f.jpg", "settings"=>array('width'=>620, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>88, "object_id"=>51, "file_name"=>"d4657d7249.jpg", "settings"=>array('width'=>650, 'height'=>488, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>89, "object_id"=>51, "file_name"=>"3c7d130c31.jpg", "settings"=>array('width'=>675, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>90, "object_id"=>51, "file_name"=>"319038f00e.jpg", "settings"=>array('width'=>672, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>91, "object_id"=>52, "file_name"=>"f0b53161a3.jpg", "settings"=>array('width'=>675, 'height'=>450, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>92, "object_id"=>52, "file_name"=>"db350ec9a6.jpg", "settings"=>array('width'=>650, 'height'=>650, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>93, "object_id"=>52, "file_name"=>"e8d7395c10.jpg", "settings"=>array('width'=>672, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>94, "object_id"=>53, "file_name"=>"a23f58379c.jpg", "settings"=>array('width'=>675, 'height'=>450, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>95, "object_id"=>53, "file_name"=>"227c61a792.jpg", "settings"=>array('width'=>650, 'height'=>488, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>96, "object_id"=>53, "file_name"=>"6768d65675.jpg", "settings"=>array('width'=>650, 'height'=>472, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>97, "object_id"=>53, "file_name"=>"6c6f60da06.jpg", "settings"=>array('width'=>675, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>0)),
	array("id"=>98, "object_id"=>54, "file_name"=>"2a8dfbff7f.jpg", "settings"=>array('width'=>675, 'height'=>450, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>99, "object_id"=>54, "file_name"=>"0a665edb1e.jpg", "settings"=>array('width'=>762, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>100, "object_id"=>54, "file_name"=>"0e7ac33104.jpg", "settings"=>array('width'=>675, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>101, "object_id"=>55, "file_name"=>"0b2d1dba6f.jpg", "settings"=>array('width'=>675, 'height'=>450, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>102, "object_id"=>55, "file_name"=>"fe5cb3ba1d.jpg", "settings"=>array('width'=>727, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>103, "object_id"=>55, "file_name"=>"e34912143d.jpg", "settings"=>array('width'=>652, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>104, "object_id"=>56, "file_name"=>"6176f16137.jpg", "settings"=>array('width'=>650, 'height'=>455, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>105, "object_id"=>56, "file_name"=>"85b36b11d8.jpg", "settings"=>array('width'=>677, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>106, "object_id"=>56, "file_name"=>"a856c3690f.jpg", "settings"=>array('width'=>675, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>107, "object_id"=>57, "file_name"=>"2526b662fb.jpg", "settings"=>array('width'=>921, 'height'=>450, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>108, "object_id"=>57, "file_name"=>"1f24bce2ef.jpg", "settings"=>array('width'=>650, 'height'=>501, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>109, "object_id"=>57, "file_name"=>"fd74e96cc2.jpg", "settings"=>array('width'=>675, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>110, "object_id"=>58, "file_name"=>"05300a8ec8.jpg", "settings"=>array('width'=>729, 'height'=>450, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>111, "object_id"=>58, "file_name"=>"e0f06182d7.jpg", "settings"=>array('width'=>650, 'height'=>884, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>112, "object_id"=>58, "file_name"=>"d520c64f0b.jpg", "settings"=>array('width'=>665, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>113, "object_id"=>59, "file_name"=>"15ed76a147.jpg", "settings"=>array('width'=>655, 'height'=>450, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>114, "object_id"=>59, "file_name"=>"fc082afa34.jpg", "settings"=>array('width'=>650, 'height'=>650, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>115, "object_id"=>59, "file_name"=>"a4e15850fc.jpg", "settings"=>array('width'=>650, 'height'=>650, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>116, "object_id"=>60, "file_name"=>"49f713042a.jpg", "settings"=>array('width'=>658, 'height'=>450, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>117, "object_id"=>60, "file_name"=>"a955c671e4.jpg", "settings"=>array('width'=>650, 'height'=>455, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>118, "object_id"=>60, "file_name"=>"0b7b944808.jpg", "settings"=>array('width'=>661, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>119, "object_id"=>61, "file_name"=>"d417918a1a.jpg", "settings"=>array('width'=>650, 'height'=>812, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>120, "object_id"=>61, "file_name"=>"487e052bce.jpg", "settings"=>array('width'=>650, 'height'=>474, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>121, "object_id"=>62, "file_name"=>"715974d900.jpg", "settings"=>array('width'=>678, 'height'=>450, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>122, "object_id"=>62, "file_name"=>"acd3141de9.jpg", "settings"=>array('width'=>675, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>123, "object_id"=>62, "file_name"=>"063885fba0.jpg", "settings"=>array('width'=>675, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>124, "object_id"=>63, "file_name"=>"8f9fcbf837.jpg", "settings"=>array('width'=>654, 'height'=>450, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>125, "object_id"=>63, "file_name"=>"8c17e50c05.jpg", "settings"=>array('width'=>650, 'height'=>459, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>126, "object_id"=>63, "file_name"=>"e8aa66d46c.jpg", "settings"=>array('width'=>890, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>127, "object_id"=>64, "file_name"=>"b1e8f53d37.jpg", "settings"=>array('width'=>675, 'height'=>450, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>128, "object_id"=>64, "file_name"=>"6450e218f8.jpg", "settings"=>array('width'=>675, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>129, "object_id"=>64, "file_name"=>"a9b0f6eddd.jpg", "settings"=>array('width'=>672, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>130, "object_id"=>65, "file_name"=>"df1e133d0f.jpg", "settings"=>array('width'=>675, 'height'=>450, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>131, "object_id"=>65, "file_name"=>"5a19d9dfdd.jpg", "settings"=>array('width'=>675, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>132, "object_id"=>65, "file_name"=>"cb95060703.jpg", "settings"=>array('width'=>650, 'height'=>488, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>133, "object_id"=>65, "file_name"=>"dbad308b5f.jpg", "settings"=>array('width'=>675, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>0)),
	array("id"=>134, "object_id"=>66, "file_name"=>"5928066174.jpg", "settings"=>array('width'=>701, 'height'=>450, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>135, "object_id"=>66, "file_name"=>"98998693d6.jpg", "settings"=>array('width'=>675, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>136, "object_id"=>66, "file_name"=>"6f045a46de.jpg", "settings"=>array('width'=>668, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>137, "object_id"=>67, "file_name"=>"58f619e7d4.jpg", "settings"=>array('width'=>675, 'height'=>450, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>138, "object_id"=>67, "file_name"=>"4e3711283e.jpg", "settings"=>array('width'=>675, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>139, "object_id"=>67, "file_name"=>"c425afcff4.jpg", "settings"=>array('width'=>675, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>140, "object_id"=>68, "file_name"=>"670ec04f72.jpg", "settings"=>array('width'=>675, 'height'=>450, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>141, "object_id"=>68, "file_name"=>"feeb14577c.jpg", "settings"=>array('width'=>675, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>142, "object_id"=>68, "file_name"=>"c7932320f0.jpg", "settings"=>array('width'=>669, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>143, "object_id"=>69, "file_name"=>"3892c003a0.jpg", "settings"=>array('width'=>675, 'height'=>450, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>144, "object_id"=>69, "file_name"=>"4304a45a37.jpg", "settings"=>array('width'=>675, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>145, "object_id"=>69, "file_name"=>"329e00b33f.jpg", "settings"=>array('width'=>675, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>146, "object_id"=>70, "file_name"=>"d8de0ef5de.jpg", "settings"=>array('width'=>670, 'height'=>450, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>147, "object_id"=>70, "file_name"=>"be3a724554.jpg", "settings"=>array('width'=>650, 'height'=>484, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>148, "object_id"=>71, "file_name"=>"5a4b3d7fee.jpg", "settings"=>array('width'=>622, 'height'=>450, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>149, "object_id"=>71, "file_name"=>"44e376e0f3.jpg", "settings"=>array('width'=>650, 'height'=>453, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>150, "object_id"=>72, "file_name"=>"3fa978adb7.jpg", "settings"=>array('width'=>663, 'height'=>450, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>151, "object_id"=>73, "file_name"=>"763f9cc61a.jpg", "settings"=>array('width'=>650, 'height'=>487, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>152, "object_id"=>74, "file_name"=>"decad06abd.jpg", "settings"=>array('width'=>675, 'height'=>450, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>153, "object_id"=>74, "file_name"=>"e6be6e7196.jpg", "settings"=>array('width'=>672, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),
	array("id"=>154, "object_id"=>75, "file_name"=>"865ad0ae3d.jpg", "settings"=>array('width'=>650, 'height'=>488, 'use_for_search'=>1, 'use_for_slide_show'=>1)),
	array("id"=>155, "object_id"=>75, "file_name"=>"980cffc9fd.jpg", "settings"=>array('width'=>676, 'height'=>450, 'use_for_search'=>0, 'use_for_slide_show'=>1)),	
);

$vtours = array(
	array("id"=>81, "object_id"=>4, "file_name"=>"7fa252c222.jpg", "settings"=>array('width'=>819, 'height'=>400)),
	array("id"=>82, "object_id"=>7, "file_name"=>"45b0e525c5.jpg", "settings"=>array('width'=>1205, 'height'=>400)),
	array("id"=>83, "object_id"=>24, "file_name"=>"19c2e0a4b6.jpg", "settings"=>array('width'=>638, 'height'=>400)),
	array("id"=>84, "object_id"=>25, "file_name"=>"6d5db8ad59.jpg", "settings"=>array('width'=>936, 'height'=>400)),
	array("id"=>85, "object_id"=>25, "file_name"=>"6d5db8ad59.jpg", "settings"=>array('width'=>1004, 'height'=>400)),
	array("id"=>86, "object_id"=>26, "file_name"=>"911eea8247.jpg", "settings"=>array('width'=>933, 'height'=>400)),
	array("id"=>87, "object_id"=>26, "file_name"=>"911eea8247.jpg", "settings"=>array('width'=>1148, 'height'=>400)),
);

$wish_lists = array(
	array('id'=>1, 'name'=>'Luxury interiors', 'name_en'=>'Luxury interiors' , 'name_ru'=>'Роскошные интерьеры'),
	array('id'=>2, 'name'=>'For large families', 'name_en'=>'For large families' , 'name_ru'=>'Дом для большой семьи'),
	array('id'=>3, 'name'=>'Peace & quiet', 'name_en'=>'Peace & quiet' , 'name_ru'=>'Спокойный уголок'),
	array('id'=>4, 'name'=>'For nature-lovers', 'name_en'=>'For nature-lovers' , 'name_ru'=>'Для любителей природы'),
);

$reviews = array(
	array(
		'gid_type' => 'listings_object',
		'id_object' => 11,
		'id_poster' => 1,
		'id_responder' => 2,
		'rating_data' => array("main"=>"5", "dop1"=>"5", "dop2"=>"4"),
		'message' => 'You will never regret staying at this house! It is truly a unique experience. It was the perfect trip...this house is located in the south end of a tiny island off of Cancun, you rent a golf cart and get around very easily, the house is right on the water, it has two huge bedrooms and 3 bathrooms, we had our own private pool, kitchen, living room etc. My friends still can\'t believe we stayed there, it was such an unbelievable place. The owner lives next door and is really nice, she stays out of your way but is there if you need her. Michelle was also good in providing information and answering all of our questions.',
		'date_add' => '2013-08-04 06:25:50',
		'answer' => '',
	),
	array(
		'gid_type' => 'listings_object',
		'id_object' => 14,
		'id_poster' => 1,
		'id_responder' => 7,
		'rating_data' => array("main"=>"5", "dop1"=>"4", "dop2"=>"5"),
		'message' => 'You will never regret staying at this house! It is truly a unique experience. It was the perfect trip...this house is located in the south end of a tiny island off of Cancun, you rent a golf cart and get around very easily, the house is right on the water, it has two huge bedrooms and 3 bathrooms, we had our own private pool, kitchen, living room etc. My friends still can\'t believe we stayed there, it was such an unbelievable place. The owner lives next door and is really nice, she stays out of your way but is there if you need her. Michelle was also good in providing information and answering all of our questions.',
		'date_add' => '2013-08-04 06:25:50',
		'answer' => '',
	),
);
