<?php

$_permissions["admin_listings"]["activate"]	= "3";
$_permissions["admin_listings"]["activate_wish_list"] = "3";
$_permissions["admin_listings"]["active"] = "3";
$_permissions["admin_listings"]["add_to_wish_list"] = "3";
$_permissions["admin_listings"]["ajax_delete_from_wish_list"] = "3";
$_permissions["admin_listings"]["ajax_delete_panorama"] = "3";
$_permissions["admin_listings"]["ajax_delete_photo"] = "3";
$_permissions["admin_listings"]["ajax_get_booking_price"] = "3";
$_permissions["admin_listings"]["ajax_get_calendar"] = "3";
$_permissions["admin_listings"]["ajax_get_export_extend_form"] = "3";
$_permissions["admin_listings"]["ajax_order_delete"] = "3";
$_permissions["admin_listings"]["ajax_order_form"] = "3";
$_permissions["admin_listings"]["ajax_panorama_form"] = "3";
$_permissions["admin_listings"]["ajax_period_delete"] = "3";
$_permissions["admin_listings"]["ajax_period_form"] = "3";
$_permissions["admin_listings"]["ajax_photo_form"] = "3";
$_permissions["admin_listings"]["ajax_reload_panorama"] = "3";
$_permissions["admin_listings"]["ajax_reload_panorama_block"] = "3";
$_permissions["admin_listings"]["ajax_reload_photo_block"] = "3";
$_permissions["admin_listings"]["ajax_reload_wish_list_content"] = "3";
$_permissions["admin_listings"]["ajax_save_order"] = "3";
$_permissions["admin_listings"]["ajax_save_panorama_data"] = "3";
$_permissions["admin_listings"]["ajax_save_panorama_sorting"] = "3";
$_permissions["admin_listings"]["ajax_save_period"] = "3";
$_permissions["admin_listings"]["ajax_save_photo_data"] = "3";
$_permissions["admin_listings"]["ajax_save_photo_sorting"] = "3";
$_permissions["admin_listings"]["ajax_save_wish_list_sorting"] = "3";
$_permissions["admin_listings"]["ajax_set_map"] = "3";
$_permissions["admin_listings"]["all"] = "3";
$_permissions["admin_listings"]["buy"] = "3";
$_permissions["admin_listings"]["delete"] = "3";
$_permissions["admin_listings"]["delete_from_wish_list"] = "3";
$_permissions["admin_listings"]["delete_wish_list"] = "3";
$_permissions["admin_listings"]["edit"] = "3";
$_permissions["admin_listings"]["edit_wish_list"] = "3";
$_permissions["admin_listings"]["index"] = "3";
$_permissions["admin_listings"]["moderation"] = "3";
$_permissions["admin_listings"]["moderation_edit"] = "3";
$_permissions["admin_listings"]["not_active"] = "3";
$_permissions["admin_listings"]["order_approve"] = "3";
$_permissions["admin_listings"]["order_decline"] = "3";
$_permissions["admin_listings"]["order_delete"] = "3";
$_permissions["admin_listings"]["order_edit"] = "3";
$_permissions["admin_listings"]["orders"] = "3";
$_permissions["admin_listings"]["panorama_upload"] = "3";
$_permissions["admin_listings"]["period_delete"] = "3";
$_permissions["admin_listings"]["photo_upload"] = "3";
$_permissions["admin_listings"]["sale"] = "3";
$_permissions["admin_listings"]["services"] = "3";
$_permissions["admin_listings"]["settings"] = "3";
$_permissions["admin_listings"]["show"] = "3";
$_permissions["admin_listings"]["wish_list_content"] = "3";
$_permissions["admin_listings"]["wish_lists"] = "3";
$_permissions["listings"]["activity"] = "2";
$_permissions["listings"]["agents"] = "1";
$_permissions["listings"]["ajax_alert"] = "2";
$_permissions["listings"]["ajax_close_alert"] = "2";
$_permissions["listings"]["ajax_delete_panorama"] = "2";
$_permissions["listings"]["ajax_delete_photo"] = "2";
$_permissions["listings"]["ajax_delete_saved"] = "2";
$_permissions["listings"]["ajax_delete_saved_search"] = "2";
$_permissions["listings"]["ajax_delete_search_criteria"] = "1";
$_permissions["listings"]["ajax_get_booking_price"] = "1";
$_permissions["listings"]["ajax_get_calendar"] = "1";
$_permissions["listings"]["ajax_get_export_extend_form"] = "1";
$_permissions["listings"]["ajax_get_listings"] = "1";
$_permissions["listings"]["ajax_get_listings_form"] = "1";
$_permissions["listings"]["ajax_get_main_search_form"] = "1";
$_permissions["listings"]["ajax_get_property_type_data"] = "1";
$_permissions["listings"]["ajax_get_property_type_form"] = "1";
$_permissions["listings"]["ajax_get_rss_link"] = "1";
$_permissions["listings"]["ajax_get_search_advanced_form"] = "1";
$_permissions["listings"]["ajax_get_search_link"] = "1";
$_permissions["listings"]["ajax_get_search_quick_form"] = "1";
$_permissions["listings"]["ajax_get_section"] = "1";
$_permissions["listings"]["ajax_get_selected_listings"] = "1";
$_permissions["listings"]["ajax_get_selected_wish_lists"] = "1";
$_permissions["listings"]["ajax_get_wish_lists"] = "1";
$_permissions["listings"]["ajax_get_wish_lists_form"] = "1";
$_permissions["listings"]["ajax_index"] = "1";
$_permissions["listings"]["ajax_mortgage_calc"] = "1";
$_permissions["listings"]["ajax_my"] = "2";
$_permissions["listings"]["ajax_order_approve"] = "2";
$_permissions["listings"]["ajax_order_decline"] = "2";
$_permissions["listings"]["ajax_order_delete"] = "2";
$_permissions["listings"]["ajax_order_form"] = "2";
$_permissions["listings"]["ajax_order_approve_form"] = "2";
$_permissions["listings"]["ajax_orders"] = "2";
$_permissions["listings"]["ajax_panorama_form"] = "2";
$_permissions["listings"]["ajax_period_delete"] = "2";
$_permissions["listings"]["ajax_period_form"] = "2";
$_permissions["listings"]["ajax_photo_form"] = "2";
$_permissions["listings"]["ajax_reload_panorama"] = "2";
$_permissions["listings"]["ajax_reload_panorama_block"] = "2";
$_permissions["listings"]["ajax_reload_photo_block"] = "2";
$_permissions["listings"]["ajax_request_delete"] = "2";
$_permissions["listings"]["ajax_requests"] = "2";
$_permissions["listings"]["ajax_save"] = "2";
$_permissions["listings"]["ajax_save_order"] = "2";
$_permissions["listings"]["ajax_save_panorama_data"] = "2";
$_permissions["listings"]["ajax_save_panorama_sorting"] = "2";
$_permissions["listings"]["ajax_save_period"] = "2";
$_permissions["listings"]["ajax_save_photo_data"] = "2";
$_permissions["listings"]["ajax_save_photo_sorting"] = "2";
$_permissions["listings"]["ajax_save_search"] = "2";
$_permissions["listings"]["ajax_saved"] = "2";
$_permissions["listings"]["ajax_search"] = "1";
$_permissions["listings"]["ajax_search_counts"] = "1";
$_permissions["listings"]["ajax_search_form"] = "1";
$_permissions["listings"]["ajax_set_map"] = "2";
$_permissions["listings"]["ajax_share_form"] = "1";
$_permissions["listings"]["ajax_share_mail"] = "1";
$_permissions["listings"]["ajax_slider_form"] = "1";
$_permissions["listings"]["ajax_user"] = "1";
$_permissions["listings"]["ajax_visitors"] = "2";
$_permissions["listings"]["ajax_visits"] = "2";
$_permissions["listings"]["ajax_wish_list"] = "1";
$_permissions["listings"]["alert"] = "2";
$_permissions["listings"]["apply_service"] = "2";
$_permissions["listings"]["category"] = "1";
$_permissions["listings"]["delete"] = "2";
$_permissions["listings"]["delete_saved"] = "2";
$_permissions["listings"]["delete_saved_search"] = "2";
$_permissions["listings"]["delete_search_criteria"] = "1";
$_permissions["listings"]["discount"] = "1";
$_permissions["listings"]["edit"] = "2";
$_permissions["listings"]["index"] = "1";
$_permissions["listings"]["load_saved_search"] = "2";
$_permissions["listings"]["location"] = "1";
$_permissions["listings"]["my"] = "2";
$_permissions["listings"]["open_house"] = "1";
$_permissions["listings"]["order_approve"] = "2";
$_permissions["listings"]["order_decline"] = "2";
$_permissions["listings"]["order_delete"] = "2";
$_permissions["listings"]["orders"] = "2";
$_permissions["listings"]["panorama_upload"] = "2";
$_permissions["listings"]["pdf"] = "1";
$_permissions["listings"]["period_delete"] = "2";
$_permissions["listings"]["photo_upload"] = "2";
$_permissions["listings"]["preferences"] = "2";
$_permissions["listings"]["privates"] = "1";
$_permissions["listings"]["request_delete"] = "2";
$_permissions["listings"]["requests"] = "2";
$_permissions["listings"]["rss"] = "1";
$_permissions["listings"]["save"] = "2";
$_permissions["listings"]["save_search"] = "2";
$_permissions["listings"]["saved"] = "2";
$_permissions["listings"]["search"] = "1";
$_permissions["listings"]["services"] = "2";
$_permissions["listings"]["set_view_mode"] = "1";
$_permissions["listings"]["share_form"] = "1";
$_permissions["listings"]["user"] = "1";
$_permissions["listings"]["similar_listing"] = "1";
$_permissions["listings"]["view"] = "1";
$_permissions["listings"]["visitors"] = "2";
$_permissions["listings"]["visits"] = "2";
$_permissions["listings"]["wish_list"] = "1";
$_permissions["listings"]["wish_lists"] = "1";
$_permissions["api_listings"]["create"] = "2";
$_permissions["api_listings"]["delete"] = "2";
$_permissions["api_listings"]["delete_saved"] = "2";
$_permissions["api_listings"]["file_delete"] = "2";
$_permissions["api_listings"]["file_upload"] = "2";
$_permissions["api_listings"]["get"] = "1";
$_permissions["api_listings"]["get_booking_price"] = "1";
$_permissions["api_listings"]["get_photos"] = "1";
$_permissions["api_listings"]["get_virtual_tours"] = "1";
$_permissions["api_listings"]["my"] = "2";
$_permissions["api_listings"]["order_approve"] = "2";
$_permissions["api_listings"]["order_decline"] = "2";
$_permissions["api_listings"]["order_delete"] = "2";
$_permissions["api_listings"]["orders"] = "2";
$_permissions["api_listings"]["panorama_delete"] = "2";
$_permissions["api_listings"]["panorama_upload"] = "2";
$_permissions["api_listings"]["period_delete"] = "2";
$_permissions["api_listings"]["photo_delete"] = "2";
$_permissions["api_listings"]["photo_upload"] = "2";
$_permissions["api_listings"]["request_delete"] = "2";
$_permissions["api_listings"]["requests"] = "2";
$_permissions["api_listings"]["save"] = "2";
$_permissions["api_listings"]["save_order"] = "2";
$_permissions["api_listings"]["save_period"] = "2";
$_permissions["api_listings"]["saved"] = "2";
$_permissions["api_listings"]["search"] = "1";
$_permissions["api_listings"]["set"] = "2";
$_permissions["api_listings"]["video_delete"] = "2";
$_permissions["api_listings"]["video_upload"] = "2";
