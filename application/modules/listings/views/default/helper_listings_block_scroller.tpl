{if $listings}
{js file='jcarousellite.min.js'}
{js file='init_carousel_controls.js'}
<script>{literal}
$(function(){
	var rtl = 'rtl' === '{/literal}{$_LANG.rtl}{literal}';
	var idPrev, idNext;
	if(!rtl) {
		idNext = '#directionright{/literal}{$listings_page_data.rand}{literal}';
		idPrev = '#directionleft{/literal}{$listings_page_data.rand}{literal}';
	} else {
		idNext = '#directionleft{/literal}{$listings_page_data.rand}{literal}';
		idPrev = '#directionright{/literal}{$listings_page_data.rand}{literal}';
	};
	$('.carousel_block{/literal}{$listings_page_data.rand}{literal}').jCarouselLite({
		rtl: rtl,
		visible: {/literal}{$listings_page_data.visible}{literal},
		btnNext: idNext,
		btnPrev: idPrev,
		circular:false,
		afterEnd: function(a) {
			var index = $(a[0]).index();
			carousel_controls{/literal}{$listings_page_data.rand}{literal}.update_controls(index);
		}
	});

	carousel_controls{/literal}{$listings_page_data.rand}{literal} = new init_carousel_controls({
		rtl: rtl,
		carousel_images_count: {/literal}{$listings_page_data.visible}{literal},
		carousel_total_images:{/literal}{$listings_page_data.count}{literal},
		btnNext: idNext,
		btnPrev: idPrev
	});
});
</script>{/literal}
<h2>{l i='header_'+$type+'_listings' gid='listings'}</h2>
<div class="{$type|escape}_listings_block carousel {$photo_size|escape}">
	<div id="directionleft{$listings_page_data.rand}" class="directionleft">
		<div class="with-icon i-larr w" id="l_hover"></div>
	</div>
	<div class="carousel_block carousel_block{$listings_page_data.rand} item_{$listings_page_data.visible}_info">
	<ul>
		{foreach item=item key=key from=$listings}
		<li>
			<div class="listing {$photo_size}">
				<a href="{seolink module='listings' method='view' data=$item}">
					<img src="{$item.media.photo.thumbs[$photo_size]}" alt="{$item.output_name|escape}" title="{$item.output_name|escape}">
					{if $item.photo_count || $item.is_vtour}
					<div class="photo-info">
						<div class="panel">
							{if $item.photo_count}<span class="btn-link"><ins class="with-icon-small w i-photo"></ins> {$item.photo_count}</span>{/if}
							{if $item.is_vtour}<span class="btn-link"><ins class="with-icon-small w i-vtour"></ins> 360&deg;</span>{/if}
						</div>
						<div class="background"></div>
					</div>
					{/if}
				</a>
				<a href="{seolink module='listings' method='view' data=$item}">{if $photo_size eq 'big'}{$item.output_name|truncate:35}{else}{$item.output_name|truncate:30}{/if}</a>
				<span>{$item.property_type_str} {$item.operation_type_str}</span>
				<span>{block name=listing_price_block module='listings' data=$item template='small'}</span>
			</div>
		</li>
		{/foreach}
	</ul>
	</div>
	<div id="directionright{$listings_page_data.rand}" class="directionright">
		<div class="with-icon i-rarr w" id="r_hover"></div>
	</div>
	<div class="clr"></div>
</div>
{/if}
