{include file="header.tpl" load_type='ui'}
{if $listing.price}
	<input type="hidden" value ="{$listing.price}" id="listing_price" />
{/if}

{if $bank}
	<input type="hidden" value ="{$bank}" id="bank" />
{/if}

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>
{literal}
$(document).ready(function(){
function numberWithCommas(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}
	//serves as a timer delay functions
	var delay = (function(){
	  var timer = 0;
	  return function(callback, ms){
		clearTimeout (timer);
		timer = setTimeout(callback, ms);
	  };
	})();
	function loader(){
		$('#tabletest').append(
		"<thead>"+
		"<tr>"+
			"<th align='center' colspan='2'><img src='{/literal}{$base_url}{literal}/application/modules/listings/views/default/css/loader.gif' style='border:none;'/></th>"+
		"</tr>"+
		"</thead>"
		);
		
	}
	function kpr_table(){
		var propertiID=$('#properti').val();
		var promosiID=$('#promosi').val();
		var bank=$('#bank').val();
		$('#tabletest').empty();
		loader();
		$.post("listings-kpr_get_bank",{ propertiID: propertiID, promosiID: promosiID, bank: bank}, function(json) {
			$('#tabletest').empty();
			var data = JSON.parse(json);
			i=0;
			$('#tabletest').append(
			"<thead>"+
			"<tr>"+
				"<th style='background-color:#3a5a98; color:white;'>Masa Promosi</td>"+
				"<th style='background-color:#3a5a98; color:white;'>Masa pinjaman</td>"+
				"<th style='background-color:#3a5a98; color:white;'>Bunga</td>"+				
				"<th style='background-color:#3a5a98; color:white;'>Angsuran per Bulan</td>"+
			"</tr>"+
			"</thead>"
			);
			if(data)
			{
			$.each(data, function(){
				var pinjaman_get = $('#sisapinjaman').val();
				var pinjaman_comma = parseFloat(pinjaman_get.replace(/,/g, ''))
				var pinjaman_pokok = pinjaman_comma;
				var lama_pinjaman = $('#masa_pinjaman').val();
				/*
				var cicilan_pokok = pinjaman_pokok/(lama_pinjaman*12);
				var cicilan_pokok_rounded = (cicilan_pokok).toFixed(2);
				var bunga_bulan = (pinjaman_pokok*data[i].bunga/12/100).toFixed(2);
				var cicilan_bulan = (parseFloat(bunga_bulan)+parseFloat(cicilan_pokok_rounded)).toFixed(2);
				*/
				var tenor = lama_pinjaman*12;
				var bunga_bulan = data[i].bunga/12/100;
				var step1 = 1+bunga_bulan;
				var step2 = Math.pow(step1, -tenor);
				var step3 = 1-step2;
				var step4 = bunga_bulan/step3;
				var step5 = pinjaman_pokok*step4;
				var cicilan_bulan = parseFloat(step5).toFixed(2);
				
				var image_check =  data[i].logo;
				if(i%2==0){
					if(image_check!='')
					{
			//$base_url only works if it's placed outside of the literal tags
						$('#tabletest').append(
						"<tr>"+
							"<td style='background-color:#ebedfa; text-align:center;'><font style='font-size:24px; font-style:italic;'>"+data[i].periode+"</font> Tahun</td>"+
							"<td style='background-color:#ebedfa; text-align:center;'><font style='font-size:24px; font-style:italic;'>"+lama_pinjaman+"</font> Tahun</td>"+
							"<td id='bungabank"+i+"' style='background-color:#ebedfa; text-align:center;'><font style='font-size:24px; color:red; font-style:italic;'>"+data[i].bunga+"</font> %</td>"+							
							"<td style='background-color:#ebedfa; text-align:center;'><font style='font-size:24px; color:#1C51FF; font-style:italic;'>Rp. "+numberWithCommas(cicilan_bulan)+"</font></td>"+
						"</tr>"
						);
					}
					else
					{
						$('#tabletest').append(
						"<tr>"+
							"<td style='background-color:#ebedfa; text-align:center;'><font style='font-size:24px; font-style:italic;'>"+data[i].periode+"</font> Tahun</td>"+
							"<td style='background-color:#ebedfa; text-align:center;'><font style='font-size:24px; font-style:italic;'>"+lama_pinjaman+"</font> Tahun</td>"+
							"<td id='bungabank"+i+"' style='background-color:#ebedfa; text-align:center;'><font style='font-size:24px; color:red; font-style:italic;'>"+data[i].bunga+"</font> %</td>"+							
							"<td style='background-color:#ebedfa; text-align:center;'><font style='font-size:24px; color:#1C51FF; font-style:italic;'>Rp. "+numberWithCommas(cicilan_bulan)+"</font></td>"+
						"</tr>"
						);
					}
				i++;
				}
				else
				{
					if(image_check!='')
					{
						$('#tabletest').append(
						"<tr>"+
							"<td style='background-color:white; text-align:center;'><font style='font-size:24px; font-style:italic;'>"+data[i].periode+"</font> Tahun</td>"+
							"<td style='background-color:white; text-align:center;'><font style='font-size:24px; font-style:italic;'>"+lama_pinjaman+"</font> Tahun</td>"+
							"<td id='bungabank"+i+"' style='background-color:white; text-align:center;'><font style='font-size:24px; color:red; font-style:italic;'>"+data[i].bunga+"</font> %</td>"+							
							"<td style='background-color:white; text-align:center;'><font style='font-size:24px; color:#1C51FF; font-style:italic;'>Rp. "+numberWithCommas(cicilan_bulan)+"</font></td>"+
						"</tr>"
						);
					}
					else
					{
						$('#tabletest').append(
						"<tr>"+
							"<td style='background-color:white; text-align:center;'><font style='font-size:24px; font-style:italic;'>"+data[i].periode+"</font> Tahun</td>"+
							"<td style='background-color:white; text-align:center;'><font style='font-size:24px; font-style:italic;'>"+lama_pinjaman+"</font> Tahun</td>"+
							"<td id='bungabank"+i+"' style='background-color:white; text-align:center;'><font style='font-size:24px; color:red; font-style:italic;'>"+data[i].bunga+"</font> %</td>"+							
							"<td style='background-color:white; text-align:center;'><font style='font-size:24px; color:#1C51FF; font-style:italic;'>Rp. "+numberWithCommas(cicilan_bulan)+"</font></td>"+
						"</tr>"
						);
					}
				i++;
				}
			});
			}
			else
			{
					$('#tabletest').append(
					"<tr>"+
						"<td align='center' style='background-color:#ebedfa;' colspan='4'><font style='font-size:18px; color:#1C51FF; font-style:italic;'>For special rate, please contact our representative.</font></td>"+
					"</tr>"
					);
			}			
		});
	
			
	}
	// RUN BY DEFAULT
	//kpr_table();
	// START OPTIONAL FUNCTIONS
	$('#properti').change(function(){
		kpr_table();
	});
	$('#promosi').change(function(){
		kpr_table();
	});
	
	$('#masa_pinjaman').change(function(){
		kpr_table();		
	});
	
	//added mod support to be able to directly output KPR calculation based on listings from Infoproperti
	if($('#listing_price').val()!='')
	{
		$('#pinjaman').val($('#listing_price').val());
		$('#check_pinjaman').hide();
		$('#pinjaman_check').empty();
		var check_dp = $('#dp').val();
		var check_pinjaman = $('#pinjaman').val();
		//$('#pinjaman_check').val(print_pinjaman);
			if(($.isNumeric(check_dp)==true) && ($.isNumeric(check_pinjaman)==true)){
				//append commas to a span below the input box
				var pinjaman_comma = numberWithCommas(check_pinjaman);
				var print_pinjaman = 'Rp. '+pinjaman_comma;
				$('#pinjaman_check').append(print_pinjaman);
				
				
				var pinjaman = $('#pinjaman').val();
				var dp = $('#dp').val();
				var sisapinjaman = pinjaman-(pinjaman*dp/100);
				var test_sisapinjaman = numberWithCommas(sisapinjaman)
				$('#sisapinjaman').val(test_sisapinjaman);	
					
				$('#tabletest').empty();
				delay(function(){
					kpr_table();
				}, 2000);
				$('#pinjaman').val(pinjaman_comma);
			}
			else if($.isNumeric(check_pinjaman)==false&&check_pinjaman!=''){
				$('#check_pinjaman').show();
			}
	}

	$('#pinjaman').keyup(function(){
	$('#check_pinjaman').hide();
	$('#pinjaman_check').empty();
	var check_dp = $('#dp').val();
	var check_pinjaman = $('#pinjaman').val();
	//$('#pinjaman_check').val(print_pinjaman);
		if(($.isNumeric(check_dp)==true) && ($.isNumeric(check_pinjaman)==true)){
			//append commas to a span below the input box
			var pinjaman_comma = numberWithCommas(check_pinjaman);
			var print_pinjaman = 'Rp. '+pinjaman_comma;
			$('#pinjaman_check').append(print_pinjaman);
			
			
			var pinjaman = $('#pinjaman').val();
			var dp = $('#dp').val();
			var sisapinjaman = pinjaman-(pinjaman*dp/100);
			var test_sisapinjaman = numberWithCommas(sisapinjaman)
			$('#sisapinjaman').val(test_sisapinjaman);	
				
			$('#tabletest').empty();
			delay(function(){
				kpr_table();
			}, 2000);
		}
		else if($.isNumeric(check_pinjaman)==false&&check_pinjaman!=''){
		 	$('#check_pinjaman').show();
		}
	});
	
	$('#pinjaman').blur(function(){
		var pinjaman_check = $('#pinjaman').val();
		var pinjaman_comma = numberWithCommas(pinjaman_check);
		$('#pinjaman').val(pinjaman_comma);
	});
	
	$('#pinjaman').focus(function(){
		var pinjaman_check = $('#pinjaman').val();
		var pinjaman_int = parseFloat(pinjaman_check.replace(/,/g, ''))
		if(pinjaman_check=='')
		{
			$('#pinjaman').val('');
		}
		else
		{
			$('#pinjaman').val(pinjaman_int);
		}
	});
	
	$('#dp').keyup(function(){
	$('#check_dp').hide();	
	var check_dp = $('#dp').val();
	var check_pinjaman = $('#pinjaman').val();
	var pinjaman = parseFloat(check_pinjaman.replace(/,/g, ''))
		if(($.isNumeric(check_dp)==true) && ($.isNumeric(pinjaman)==true)){
			
			//var pinjaman = $('#pinjaman').val();
			var dp = $('#dp').val();
			var sisapinjaman = pinjaman-(pinjaman*dp/100);
			var sisapinjaman_comma = numberWithCommas(sisapinjaman);
			$('#sisapinjaman').val(sisapinjaman_comma);	
				
			$('#tabletest').empty();
			delay(function(){
				kpr_table();
			}, 1000);
		}
		else if($.isNumeric(check_dp)==false){
		 	$('#check_dp').show();
		}
		
	});
});
{/literal}
</script>
<div class="kpr_wrapper">

<form>
<div class="kpr_header">Kalkulasi Cicilan KPR</div>
<div style="position:relative;width:250px; float:left;"></div>
<div class="kpr_input" style="position:relative; float:left;">

	<table style="width:980px;"  cellpadding="5" cellspacing="0" >
    	<tr>
        	<td colspan="2">
            	KPR Bank {$bank_detail.name}
            </td>
            <td rowspan="6" valign="top">
		<div class="listing-block {if $listing.is_highlight}highlight{/if}" >
			<div id="item-block-{$listing.id}" class="item listing {if $listing.is_highlight}highlight{/if}">
				<h3 class="listing-heading search-result-entry-heading">
                <a href="{seolink module='listings' method='view' data=$listing}" title="{$listing.output_name|escape}">{$listing.output_name|truncate:50}</a>
                <div style="position:relative; float:right; margin-top:-5px; display:block;">
                {if $listing.review_sorter neq 0}                
                {depends module=reviews}
                    {block name=get_rate_block module=reviews rating_data_main=$listing.review_sorter type_gid='listings_object' template='normal' read_only='true'}
				{/depends}
                {/if}
                </div>
				</h3>
                <!-- #MOD FOR REVIEW STARS ON LISTINGS LIST -->
                <!-- #END OF MOD# -->
				<div class="image">
					<a href="{seolink module='listings' method='view' data=$listing}">
						{if $animate_available && $listing.is_slide_show}
						<img src="{$listing.media.photo.thumbs.middle_anim}" title="{$listing.output_name|escape}">
						{else}
						<img src="{$listing.media.photo.thumbs.middle}" title="{$listing.output_name|escape}">
						{/if}
						{if $listing.photo_count || $listing.is_vtour}
						<div class="photo-info">
							<div class="panel">
								{if $listing.photo_count}<span class="btn-link"><ins class="with-icon-small w i-photo"></ins> {$listing.photo_count}</span>{/if}
								{if $listing.is_vtour}<span class="btn-link"><ins class="with-icon-small w i-vtour"></ins> 360&deg;</span>{/if}
							</div>
							<div class="background"></div>
						</div>
						{/if}
					</a>
				</div>
		
				{capture assign='status_info'}{strip}
					{if $listing.is_lift_up || 
						$location_filter eq 'country' && $listing.is_lift_up_country || 
						$location_filter eq 'region' && ($listing.is_lift_up_country || $listing.is_lift_up_region) || 
						$location_filter eq 'city' && ($listing.is_lift_up_country || $listing.is_lift_up_region || $listing.is_lift_up_city) ||
						$sort_data.order ne 'default' && ($listing.is_lift_up ||$listing.is_lift_up_country || $listing.is_lift_up_region || $listing.is_lift_up_city)}
						{l i='status_lift_up' gid='listings'}
					{elseif $listing.is_featured}
						{l i='status_featured' gid='listings'}
					{/if}
				{/strip}{/capture}
				
				<div class="body clearfix">
					<div class="prop-info">
						<h3 class="price-highlight">{block name=listing_price_block module='listings' data=$listing}</h3>
						<div class="t-1">
							{$listing.property_type_str} {$listing.operation_type_str}<br />
							{l i='field_square' gid='listings'} : {$listing.square_output|truncate: 30}
							<ul class="icon-stats">
							{foreach item=residen from=$residential}

								{if $listing.id == $residen.id_listing}
									
									{if $residen.fe_bd_rooms_1 != '0'}	 
										<li class="listing-icon bedroom"> : {$residen.fe_bd_rooms_1}</li>
									{/if}
									
									{if $residen.fe_bth_rooms_1 != '0' }
										<li class="listing-icon bathroom"> : {$residen.fe_bth_rooms_1}</li>
									{/if}
									
									{if $residen.fe_garages_1 != '0' }
										<li class="listing-icon garages"> : {$residen.fe_garages_1}</li>
									{/if}
								{/if}
							{/foreach}
							
							{foreach item=commer from=$commercial}
								{if $listing.id == $commer.id_listing}
									{if $commer.fe_year_2 != '0'}	 
										<li class="listing-icon year">Year : {$commer.fe_year_2}</li>
									{/if}
									
									{if $commer.fe_foundation_2 != '0' }
										<li class="listing-icon foundation">Foundation : {$commer.fe_foundation_2}</li>
									{/if}
								{/if}
							{/foreach}
							</ul>
				{if $listing.headline}<p class="headline" title="{$listing.headline}">{$listing.headline|truncate:100}</p>
					{else}<p class="headline">&nbsp;</p>
				{/if}
                            
						</div>
					</div>
				</div>
				<div class="clr"></div>
			</div>
	</div>
            </td>
        </tr>
    	<tr>
        	<td colspan="2">
            	<img src="{$base_url}ext/kpr_cms/images/logo/{$bank_detail.logo}" style="height:100px; border:0px;" />
            </td>
        </tr>
    	<tr>
        	<td>
            Harga Properti <font color="red"><span id="check_pinjaman" style="display:none;">Must be Numeric!</span></font><br/>
            <input name="pinjaman" id="pinjaman" type="text" style="width:100%;" value="0" fcsa-number/>
            </td>
            <td>
            Down Payment (Uang Muka) <font color="red"><span id="check_dp" style="display:none;">Must be Numeric!</span></font><br/>
            <input name="dp" id="dp" type="text" value="30" size="2" />%
            </td>
        </tr>
        <tr>
        	<td><font color="#ae8d6a"><span id="pinjaman_check"></span></font></td>
            <td></td>
        </tr>
        <tr>
        	<td>
            Pinjaman<br/>
            <input name="sisapinjaman" id="sisapinjaman" value="0"  type="text" disabled="disabled" style="width:100%; background-color:#E6E6E6; cursor:not-allowed;"/>
            </td>
            <td></td>
        </tr>
        <tr>
        	<td colspan="2">
            <table cellspacing="10">
                <tr>
                    <td>Tipe Properti</td>
                    <td>Lama Pinjaman</td>
            	{if !$bank}
                    <td>Masa Promosi</td>
                {/if}
                </tr>
                <tr>
                    <td>
                    <select name="properti" id="properti">
                    {foreach item=item from=$properti}
                    <option value="{$item->propertiID}" {if $item->propertiID==$propertiID}selected="selected"{/if}>{$item->namaproperti}</option>
                    {/foreach}
                    </select>
                    </td>
                    <td>
                    <select name="masa_pinjaman" id="masa_pinjaman" >
                    {foreach item=item from=$pinjaman}
                    <option value="{$item->lamapinjaman}">{$item->lamapinjaman} Tahun</option>
                    {/foreach}
                    </select>
                    </td>
                <!-- show this option on normal KPR page, hide this when it's the special kpr page -->
            	{if !$bank}
                    <td>
                    <select name="promosi" id="promosi">
                    {foreach item=item from=$promosi}
                    <option value="{$item->promosiID}">{$item->periode} Tahun</option>
                    {/foreach}
                    </select>
                    </td>
               	{/if}
                </tr>
            
            </table>
            </td>
            
        </tr>
    </table>
</div>
	<!-- top part form -->
    <br/><br/>
</form>      
    <table id="tabletest" align="center" style="width:100%; margin-bottom:25px; margin-top:25px; border:1px black solid;"  cellpadding="10" cellspacing="0">
    
    </table>
    

</div>


{if $bank}
	{include file="footer.tpl" load_type='ui'}
{/if}