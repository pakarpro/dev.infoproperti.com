{switch from=$template}
	{case value='button'}
		{assign var='popup_form' value=1}
		<div class="b outside"><input type="submit" name="booking_btn" value="{l i='btn_order' gid='listings' type='button'}" id="booking_btn{$rand}"></div>
	{case value='icon'}
		{assign var='popup_form' value=1}
		<a href="#" id="booking_btn{$rand}" class="btn-link link-r-margin" title="{l i='link_booking_listing' gid='listings' type='button'}"><ins class="with-icon i-calendar"></ins></a>
	{case value='link'}
		{assign var='popup_form' value=1}
		<a href="#" id="booking_btn{$rand}">{l i='link_booking_listing' gid='listings'}</a>	
	{case value='form'}
		{assign var='popup_form' value=0}
		{assign var='full_form' value=1}
		{include file="order_form.tpl" module="listings" theme="default"}
	{case}
		{assign var='popup_form' value=0}
		{assign var='full_form' value=0}
		{include file="order_form.tpl" module="listings" theme="default"}
{/switch}
{$separator}
{js file='booking-form.js' module='listings'}
<script>{literal}
	$(function(){
		new bookingForm({
			siteUrl: '{/literal}{$site_url}{literal}',
			bookingBtn: 'booking_btn{/literal}{$rand}{literal}',
			isPopup: {/literal}{$popup_form}{literal},
			{/literal}{if $show_link}showCalendarLink: true,{/if}{literal}
			cFormId: '{/literal}{if $popup_form}order_form{else}period_form{$rand}{/if}{literal}',
			listingId: '{/literal}{$booking_listing.id}{literal}',
			priceType: '{/literal}{$booking_listing.price_period}{literal}',
			{/literal}{if $is_guest}displayLogin: true,{/if}{literal}
			{/literal}{if $show_price}showPrice: true,{/if}{literal}
			{/literal}{if $noSavePeriod}isSavePeriod: false,{/if}{literal}
		});
	});
{/literal}</script>
