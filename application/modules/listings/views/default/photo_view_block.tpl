	<div class="photo-info-area">
		<div class="photo-area">
			<img src="{$photo.media.thumbs.200_200}" hspace="3" id="listing_photo_{$photo.id}" />
			<div class="photo-info">
				<div class="panel">
					{if $photo.status}
					<font class="stat-active">{l i='photo_active' gid='listings'}</font>
					{else}
					<font class="stat-moder">{l i='photo_moderate' gid='listings'}</font>
					{/if}
					{if $photo.comment}<br>{$photo.comment|truncate:50:'...':true}{/if}
				</div>
				<div class="background"></div>
			</div>
		</div>
		<div class="action" onclick="javascript: gUpload.open_edit_form({$photo.id});">
			<div class="btn-link"><ins class="with-icon w i-edit no-hover"></ins></div>
			<div class="background"></div>
		</div>
		<div class="canvas hide"></div>
		<div class="clr"></div>
	</div>
