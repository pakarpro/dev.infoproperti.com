{include file="header.tpl" load_type='ui'}

<!-- search result google map display -->
{capture assign='right_block'}
	{if $use_map_in_search}{block name=show_default_map gid='listing_search' module=geomap markers=$markers settings=$map_settings width='230' height='300' map_id='listings_map_container'}{/if}
	{helper func_name=show_banner_place module=banners func_param='right-banner'}
	{helper func_name='show_mortgage_calc' module='listings'}
	{helper func_name=show_banner_place module=banners func_param='right-banner-2'}

{/capture}

{capture assign='content_block'}
<!-- Search Result listing  -->
<div class="rc">
	<div class ="inside clearfix">
		<?php /* disable temporarily <h1>{l i='header_listings_result' gid='listings'} - <span id="total_rows">{$page_data.total_rows}</span> {l i='header_listings_found' gid='listings'}</h1> */ ?>
        
		<div class="tabs tab-size-15 noPrint">
			{if $use_rss}
				<span id="rss_link" class="btn-link fright" target="_blank" title="{l i='link_search_results_rss' gid='listings' type='button'}"><ins class="with-icon i-rss"></ins></span>
			{/if}
			<ul id="search_listings_sections">
				{foreach item=tgid from=$operation_types}
				<li id="m_{$tgid}" sgid="{$tgid}" class="{if $current_operation_type eq $tgid}active{/if}"><a href="{$menu_action_link|replace:'[operation_type]':$tgid}/default/DESC/1">{l i='operation_search_'+$tgid gid='listings'}</a></li>
				{/foreach}
			</ul>
			{helper func_name=show_map_view module=geomap func_param=$site_url+'listings/set_view_mode/map'}
		</div>
		
		{capture assign='main_block'}
			<div class="content-block">
				<div id="listings_block">{$block}</div>
				{js module=listings file='listings-list.js'}
				<script>{literal}
					$(function(){
						new listingsList({
							siteUrl: '{/literal}{$site_url}{literal}',
							listAjaxUrl: '{/literal}listings/ajax_index{literal}',
							sectionId: 'search_listings_sections',
							{/literal}{if $use_rss}useRss: true,{/if}{literal}
							operationType: '{/literal}{$current_operation_type}{literal}',
							order: '{/literal}{$order}{literal}',
							orderDirection: '{/literal}{$order_direction}{literal}',
							page: {/literal}{$page_data.cur_page}{literal},
							tIds: ['pages_block_1', 'pages_block_2', 'sorter_block'],
						});
					});
				{/literal}</script>
			</div>
		{/capture}
		
		{if $right_block|trim}
			<div class="rc-1">{$main_block}</div>
			<div class="rc-2">{$right_block}</div>
		{else}
			{$main_block}
		{/if}
	</div>
</div>

<div class="lc">
	<div class="inside account_menu">
		{block name=listings_search_block module=listings}
		{if $use_poll_in_search}{block name=show_poll_place_block module=polls one_poll_place=0}{/if}
		{helper func_name=show_banner_place module=banners func_param='big-left-banner'}
		{helper func_name=show_banner_place module=banners func_param='left-banner'}
	</div>
</div>

<div class="clr"></div>
{/capture}

{if $right_block|trim}
<div class="rc_wrapper with-toolbar">
<div class="panel">
<div class="inside">
	{$content_block}
</div>
</div>
</div>
{else}
	{$content_block}
{/if}

{include file="footer.tpl"}
