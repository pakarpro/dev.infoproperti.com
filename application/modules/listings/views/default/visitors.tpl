{include file="header.tpl" load_type='ui'}
{include file="left_panel.tpl" module="start"}
<div class="rc">
	<div class="content-block">
		<h1>{l i='text_listings_visitors' gid='listings'} ({$listings_count})</h1>
		<div id="listings_block">{$block}</div>
		{js module=listings file='listings-list.js'}
		<script>{literal}
		$(function(){
			new listingsList({
				siteUrl: '{/literal}{$site_url}{literal}',
				listAjaxUrl: '{/literal}listings/ajax_visitors{literal}',
				order: '{/literal}{$order}{literal}',
				orderDirection: '{/literal}{$order_direction}{literal}',
				page: {/literal}{$page}{literal},
				tIds: ['pages_block_1', 'pages_block_2', 'sorter_block'],
			});
		});
		{/literal}</script>
	</div>
</div>
<div class="clr"></div>
{include file="footer.tpl"}
