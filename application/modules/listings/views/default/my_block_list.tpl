	
	<!-- my_block_list -->
	{if $listings}
	<div class="sorter line" id="sorter_block">
		{sorter links=$sort_data.links order=$sort_data.order direction=$sort_data.direction url=$sort_data.url}
		<div class="fright" id="pages_block_1">{pagination data=$page_data type='cute'}</div>
	</div>
	
	<div id="table-my-list">
		<?php /* DISABLE FOR NOW
		<table class="list-heading">
			<tr id="sorter_block">
				<th class="w100">{l i='field_photo' gid='listings'}</th>		
				<th><a href="{$sort_links.name}" class="link-sorter">{l i='field_name' gid='listings'}{if $page_data.order eq 'name'}<ins class="fright i-sorter with-icon-small {$page_data.order_direction|lower}"></ins>{/if}</a></th>		
				<th><a href="{$sort_links.date_modified}" class="link-sorter">{l i='field_date_modified' gid='listings'}{if $page_data.order eq 'date_modified'}<ins class="fright i-sorter with-icon-small {$page_data.order_direction|lower}"></ins>{/if}</a></th>		
				<th class="w30">{l i='field_views' gid='listings'}</th>
				<th class="w100">{l i='field_search_status' gid='listings'}</th>		
			</tr>
		</table>
		*/ ?>
		
		{foreach item=item from=$listings}
		{capture assign='property_output'}{$item.property_type_str} {$item.operation_type_str}{/capture}
		{capture assign='price_output'}	
			{block name=listing_price_block module='listings' data=$item template='small'}
		{/capture}
		<div class="my-list-content clearfix">
			<!-- image / photo -->
			<a class="photo-link" href="{seolink module='listings' method='view' data=$item}" title="{l i='btn_preview' gid='start' type='button'}">
				<img src="{$item.media.photo.thumbs.small}" alt="{$item.output_name|truncate:30|escape}" title="{$item.output_name|escape}">
			</a>
			<div class="prop-detail clearfix">
				<!-- location and details -->
				<p>{$item.output_name|truncate:40}<br/>{$property_output|truncate:40}<br />{$price_output|truncate:70}<br /><br />
					<!-- list updated -->
					<span class="list-updated">{l i='field_date_modified' gid='listings'}:&nbsp;{$item.date_modified|date_format}</span>&nbsp;|&nbsp;
					<!-- number of view -->
					<span class="number-view">{l i='field_views' gid='listings'}:&nbsp;{$item.views}</span>
				</p>
				
				<!-- list status -->
				<div class="list-status">
					{if $item.status}
						<span class="status active"><ins class="btn-link"></ins>{l i='active_listing' gid='listings'}</span><br>
					{else}
						<span class="status inactive"><ins class="btn-link"></ins>{l i='inactive_listing' gid='listings'}</span><br>
					{/if}
					
					{if $item.moderation_status eq 'default' && $item.initial_moderation}<span class="status"><ins class="btn-link ins"></ins>{l i='listing_status_default' gid='listings'}</span>
					{elseif $item.moderation_status eq 'decline'}<span class="status decline"><ins class="btn-link"></ins>{l i='listing_status_decline' gid='listings'}</span>
					{elseif $item.moderation_status eq 'approved' || $item.moderation_status eq 'default' && !$item.initial_moderation}<span class="status approved"><ins class="btn-link"></ins>{l i='listing_status_approved' gid='listings'}</span>
					{elseif $item.moderation_status eq 'wait'}<span class="status wait"><ins class="btn-link"></ins>{l i='listing_status_wait' gid='listings'}</span>
					{/if}
				</div>
			</div>
			<!-- button act -->
			<div class="my-list-act clearfix">
				<a href="{seolink module='listings' method='view' data=$item}" class="btn-link act-icon i-eye" title="{l i='btn_preview' gid='start' type='button'}"><ins class="with-icon i-eye"></ins></a>
				<a href="{$site_url}listings/edit/{$item.id}" class="btn-link act-icon i-edit" title="{l i='btn_edit' gid='start' type='button'}"><ins class="with-icon i-edit"></ins></a>
				{if $item.operation_type eq 'rent'}<a href="{$site_url}listings/edit/{$item.id}/calendar/2" class="btn-link act-icon i-calendar" title="{l i='link_calendar_view' gid='listings' type='button'}"><ins class="with-icon i-calendar"></ins></a>{/if}
				{depends module=services}{if $item.operation_type ne 'buy' && $item.operation_type ne 'lease'  && $item.status}<a href="{$site_url}listings/services/{$item.id}" class="btn-link act-icon i-dollar" title="{l i='link_services' gid='listings' type='button'}"><ins class="with-icon i-dollar"></ins></a>{/if}{/depends}
				<a href="{$site_url}listings/delete/{$item.id}" onclick="javascript: if(!confirm('{l i='note_delete_listing' gid='listings' type='js'}')) return false;" class="btn-link act-icon i-delete" title="{l i='btn_delete' gid='start' type='button'}"><ins class="with-icon i-delete"></ins></a>
			</div>
		</div>
		
		{/foreach}
	</div>
	<div id="pages_block_2">{include file="pagination.tpl"}</div>
	{else}
	<div class="item empty">{l i='no_listings' gid='listings'}</div>
	{/if}
	
	<script>{literal}
	$(function(){
		$('#total_rows').html('{/literal}{$page_data.total_rows}{literal}');
	});
	{/literal}</script>
