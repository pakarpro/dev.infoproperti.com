	{if $main_search_form_data}
		{foreach item=item from=$main_search_form_data}
		{if $item.type eq 'section'}
			{foreach item=field key=key from=$item.section.fields}
			<div class="search-field custom {$field.field.type} {$field.settings.search_type}">
				<p>{if $field.field.type ne 'checkbox'}{$field.field_content.name}{else}&nbsp;{/if}</p>
				{include file="helper_main_search_field_block.tpl" module="listings" field=$field}
				<input type="hidden" name="short_name" value="fe{$item.field_content.id}">
			</div>
			{/foreach}
		{else}
			<div class="search-field custom {$item.field.type} {$item.settings.search_type}">
				<p>{if $item.field.type ne 'checkbox'}{$item.field_content.name}{else}&nbsp;{/if}</p>
				{include file="helper_main_search_field_block.tpl" module="listings" field=$item field_gid=$item.gid}
				<input type="hidden" name="short_name" value="fe{$item.field_content.id}">
			</div>
		{/if}
		{/foreach}
	{/if}
