{include file="header.tpl" load_type='ui'}
<div class="content-block">
	<div class="edit_block">
		<h1>{if $data.id}{l i='header_order_edit' gid='listings'}{else}{l i='header_order_add' gid='listings'}{/if}</h1>
		<form method="post" action="{$data.action|escape}" name="save_form" id="save_form" enctype="multipart/form-data">
			{if $data.price_period}
				{assign var='price_period' value=$data.price_period}
			{else}
				{assign var='price_period' value=1}
			{/if}
			<div class="r">
				<div class="f">{l i='field_booking_date_start' gid='listings'}:</div>
				<div class="v">
					{switch from=$price_period}
						{case value='1'}
							<input type="text" name="period[date_start]" value="{if $data.booking_period.date_start|strtotime>0}{$data.booking_period.date_start|date_format:$page_data.date_format|escape}{/if}" id="date_start" class="middle">
							<input type="hidden" name="date_start_alt" value="" id="alt_date_start">
							<script>{literal}
								$(function(){
									$('#date_start').datepicker({dateFormat: '{/literal}{$page_data.datepicker_date_format}{literal}', altFormat: '{/literal}{$page_data.datepicker_alt_format}{literal}', altField: '#alt_date_start'});
								});
							{/literal}</script>
						{case value='2'}
							{ld i='month-names' gid='start' assign='month_names'}
							<select name="date_start_month" class="middle">
								<option value="">{$month_names.header}</value>
								{foreach item=item key=key from=$month_names.option}
								<option value="{$key}" {if $key eq $data.booking_period.date_start}selected{/if}>{$item}</option>
								{/foreach}
							</select>
							{assign var='cyear' value=('now'|date:'Y')}
							<select name="date_start_year" class="short">
								<option value="">{l i='text_year' gid='listings'}</option>
								{for start=0 stop=10 value=i}
								{math equation='x + y' x=$cyear y=$i assign='year'}
								<option value="{$year}">{$year}</option>
								{/for}
							</select>
					{/switch}
				</div>
			</div>
			<div class="r">
				<div class="f">{l i='field_booking_date_end' gid='listings'}:</div>
				<div class="v">
					{switch from=$price_period}
						{case value='1'}
							<input type="text" name="period[date_end]" value="{if $data.booking_period.date_end|strtotime>0}{$data.booking_period.date_end|date_format:$page_data.date_format|escape}{/if}" id="date_end" class="middle">
							<input type="hidden" name="date_end_alt" value="" id="alt_date_end">
							<script>{literal}
								$(function(){
									$('#date_end').datepicker({dateFormat: '{/literal}{$page_data.datepicker_date_format}{literal}', altFormat: '{/literal}{$page_data.datepicker_alt_format}{literal}', altField: '#alt_date_end'});
								});
							{/literal}</script>
						{case value='2'}
							{ld i='month-names' gid='start' assign='month_names'}
							<select name="date_end_month" class="middle">
								<option value="">{$month_names.header}</value>
								{foreach item=item key=key from=$month_names.option}
								<option value="{$key}" {if $key eq $data.booking_period.date_end}selected{/if}>{$item}</option>
								{/foreach}
							</select>
							{assign var='cyear' value=('now'|date:'Y')}
							<select name="date_end_year" class="short">
								<option value="">{l i='text_year' gid='listings'}</option>
								{for start=0 stop=10 value=i}
								{math equation='x + y' x=$cyear y=$i assign='year'}
								<option value="{$year}">{$year}</option>
								{/for}
							</select>
					{/switch}
				</div>
			</div>
			<div class="r">
				<div class="f">{l i='field_booking_guests' gid='listings'}:</div>
				<div class="v">
					{*<input type="text" name="period[guests]" value="{$data.booking_period.guests|escape}" class="middle">*}
					{ld i='booking_guests' gid='listings' assign='booking_guests'}
					<select name="period[guests]" class="middle">
						<option value="">{$booking_guests.header}</option>
						{foreach item=item key=key from=$booking_guests.option}
						<option value="{$key}" {if $key eq $data.booking_period.guests}{/if}>{$item}</option>
						{/foreach}
					</select>
				</div>
			</div>
			<div class="r">
				<div class="f">{l i='field_booking_comment' gid='listings'}:</div>
				<div class="v" id="period_comment">
					<textarea name="period_comment" rows="10" cols="80">{$data.booking_period|escape}</textarea>
				</div>
			</div>
			<div class="b">
				<input type="submit" name="btn_save" value="{l i='btn_save' gid='start' type='button'}" class="btn">
				<a href="{$site_url}listings/orders" class="btn-link"><ins class="with-icon i-larr"></ins>{l i='link_back_to_my_orders' gid='listings'}</a>
			</div>
			<div class="clr"></div>
		</form>
	</div>
</div>
<div class="clr"></div>
{include file="footer.tpl"}
