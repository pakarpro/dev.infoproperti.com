<h2>{l i='header_'+$type+'_regions_search_block' gid='listings'}</h2>
<div class="listing-cat-block"><div class="inside">
	<ul>
		{foreach item=item from=$listings_regions key=key}
		<li {if $item.empty}class="empty"{/if}>
			{if $item.statistics.listing_active}
			<a href="{seolink module='listings' method='location' data=$item.data}">{$item.name} ({$item.statistics.listing_active})</a>
			{else}
			{$item.name}
			{/if}
		</li>
		{/foreach}
	</ul>
</div></div>
