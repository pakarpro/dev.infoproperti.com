	{js file='jcarousellite.min.js'}
	{js file='init_carousel_controls.js'}
	<script>{literal}
		$(function(){
			var rtl = 'rtl' === '{/literal}{$_LANG.rtl}{literal}';
			var idPrev, idNext;
			if(!rtl){
				idNext = '#directionright{/literal}{$similar_rand}{literal}';
				idPrev = '#directionleft{/literal}{$similar_rand}{literal}';
			}else{
				idNext = '#directionleft{/literal}{$similar_rand}{literal}';
				idPrev = '#directionright{/literal}{$similar_rand}{literal}';
			};
			$('.carousel_block{/literal}{$similar_rand}{literal}').jCarouselLite({
				rtl: rtl,
				visible: {/literal}{$similar_visible}{literal},
				btnNext: idNext,
				btnPrev: idPrev,
				circular: false,
				afterEnd: function(a){
					var index = $(a[0]).index();
					carousel_controls{/literal}{$similar_rand}{literal}.update_controls(index);
				}
			});

			carousel_controls{/literal}{$similar_rand}{literal} = new init_carousel_controls({
				rtl: rtl,
				carousel_images_count: {/literal}{$similar_visible}{literal},
				carousel_total_images: {/literal}{$similar_total}{literal},
				btnNext: idNext,
				btnPrev: idPrev
			});
		});
	</script>{/literal}
	<div id="similar_listings">
		<h2>{l i='header_similar_listings' gid='listings'}</h2>
		<div class="carousel {if $similar_total <= $similar_visible}visible{/if}">
			<div id="directionleft{$similar_rand}" class="directionleft">
				<div class="with-icon i-larr w" id="l_hover"></div>
			</div>
			<div class="carousel_block carousel_block{$similar_rand} item_{$similar_visible}_info">
				<ul>
					{foreach item=item key=key from=$similar_listings}
					<li>
						<div class="listing">
							<a href="{seolink module='listings' method='view' data=$item}"><img src="{$item.media.photo.thumbs.middle}" alt="{$item.output_name|truncate:30|escape}" title="{$item.output_name|escape}" /></a>
							<a href="{seolink module='listings' method='view' data=$item}">{$item.output_name|truncate:30}</a>
							<span>{$item.property_type_str} {$item.operation_type_str}</span>
							<span>{block name=listing_price_block module='listings' data=$item template='small'}</span>
						</div>
					</li>
					{/foreach}
				</ul>
			</div>
			<div id="directionright{$similar_rand}" class="directionright">
				<div class="with-icon i-rarr w" id="r_hover"></div>
			</div>
			<div class="clr"></div>
		</div>
	</div>
	
