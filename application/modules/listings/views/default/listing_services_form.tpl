{include file="header.tpl"}

	<div class="content-block">		
		<div class="edit_block">
			<h1>{l i='header_listing_edit' gid='listings'}</h1>

			{include file="my_listings_menu.tpl" module="listings"}

			{js module=listings file='listings-edit-steps.js'}
			<script>{literal}
				$(function(){
					new listingsEditSteps();
				});
			{/literal}</script>
			<div class="rollup-box">
			
			<form method="post" action="{$data.action|escape}" name="save_form" id="save_form" enctype="multipart/form-data">
			<table class="list" id="services_list">
			<tr>
				<th class="w200">{l i='service_name' gid='listings'}</th>		
				<th class="w50">{l i='service_price' gid='listings'}, {block name=currency_output module=start cur_gid=$base_currency.gid}</th>
				<th class="w150">{if $one_param}{$param_name}{else}{l i='service_params' gid='listings'}{/if}</th>
				<th class="w100">&nbsp;</th>		
			</tr>
			{if $page_data.use_services && $page_data.listings_services}
			{foreach item=item from=$page_data.listings_services}
			<tr>
				<td>{$item.name}</td>
				<td>{block name='currency_format_output' module='start' value=$item.price cur_gid=$base_currency.gid disable_abbr=1}</td>
				<td>
					{if $one_param}
						{$item.data_admin[$param_gid]}<br>
					{else}
						{foreach item=item2 key=key2 from=$item.data_admin}
						{$item.template.data_admin_array[$key2].name}: {$item.data_admin[$key2]}<br>
						{/foreach}
					{/if}
				</td>
				<td class="center">
					<a class="btn-link" href="{$site_url}listings/apply_service/{$data.id}/{$item.gid}" target="blank">
						<ins class="with-icon i-rarr"></ins>
						{l i='btn_pay' gid='listings'}
					</a>	
				</tr>
			</tr>
			{/foreach}
			{else}
			<tr><td colspan="4" class="center">{l i='no_services' gid='listings'}</td></tr>
			{/if}
			</table>
			</div>
			
			<div class="b outside">
				<input type="button" name="btn_previous" value="{l i='nav_prev' gid='start' type='button'}" class="btn hide" id="edit_listings_prev" />
				<input type="button" name="btn_next" value="{l i='nav_next' gid='start' type='button'}" class="btn hide" id="edit_listings_next" />
				<a href="{$site_url}listings/my" class="btn-link"><ins class="with-icon i-larr"></ins>{l i='link_back_to_my_listings' gid='listings'}</a>
			</div>
			</form>	
		</div>	
	</div>

<div class="clr"></div>
{include file="footer.tpl"}
