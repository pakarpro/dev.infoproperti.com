	{if $form_data}
		{foreach item=item from=$form_data}
		{if $item.type eq 'section'}
			{foreach item=field key=key from=$item.section.fields}
			<div class="search-field custom {$field.field.type} {$field.settings.search_type}">
				<p>{$field.field_content.name}</p>
				{include file="helper_search_field_block.tpl" module="listings" field=$field}
			</div>
			{/foreach}
		{else}
			<div class="r custom {$item.field.type} {$item.settings.search_type}">
				<div class="f">{$item.field_content.name}</div>
				<div class="v">{include file="helper_search_field_block.tpl" module="listings" field=$item}</div>
			</div>
		{/if}
		{/foreach}
	{/if}

