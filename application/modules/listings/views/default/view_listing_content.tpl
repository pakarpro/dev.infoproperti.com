{l i='no_information' gid='start' assign='no_info_str'}
<!--#MOD FOR KPR-->
{if $section_gid == 'kpr'}
	{$kpr}
{/if}

{if $section_gid == 'overview'}

	<?php /* start additional */ ?>
	<div class="view">
		<div class="t-0">
        
        	<?php /* lines below for displaying modified listing by date */ ?>
            {capture assign='date_modified'}{strip}
            {$data.date_modified|date_format:$date_format} <?php /* replaced the $listing.date_modified with $item.date_modified */ ?>
            {/strip}{/capture} 
            
			<h2>{l i='field_id' gid='listings'} {$data.id}</h2>
			<h2 class="price-highlight">{block name=listing_price_block module='listings' data=$data}</h2>
			
			{$data.property_type_str} {$data.operation_type_str}<br>
			{if $data.sold}<span class="status_text">{l i='text_sold' gid='listings'}</span><br>{/if}
			{if $data.is_lift_up || $data.is_lift_up_country || $data.is_lift_up_region || $data.is_lift_up_city}
				<span class="status_text">{l i='status_lift_up' gid='listings'}</span>
			{elseif $data.is_featured}
				<span class="status_text">{l i='status_featured' gid='listings'}</span>
			{/if}
			
			<div class="noPrint">
            	<span>{l i='field_date_modified' gid='listings'}: {if $date_modified}{$date_modified}{else}{l i='inactive_listing' gid='listings'}{/if}</span><br />
				<span>{l i='field_views' gid='listings'}:</span> {$data.views}<br />
			</div>
		</div>
		
		<div class="t-1">&nbsp;</div>
		
		<div class="qr-code">
			<img alt="{l i='text_qrcode' gid='listings' type='button'}" title="{l i='text_qrcode' gid='listings' type='button'}" src="{$data.qr_code}" id="qr_code_img" onerror="this.src='uploads/qr/qr-default.png'">
		</div>
	
	</div>
    <?php /* end additional */ ?>
		
	{if $data.operation_type eq 'rent' || $data.operation_type eq 'lease'}
	<div class="r">
		<div class="f">{l i='field_price_type' gid='listings'} :</div>
		<div class="v">
			{ld_option i='price_type' gid='listings' option=$data.price_type}
		</div>
	</div>
	{/if}
	
	<div class="r">
		<div class="f">{l i='field_square' gid='listings'} :</div>
		<div class="v">{$data.square_output}</div>
	</div>
	
	{if $data.operation_type eq 'sale' || $data.operation_type eq 'rent'}
    	{if $data.price_auction} {*MOD hide if negotiated NO*}
	<div class="r">
		<div class="f">{l i='field_price_auction' gid='listings'} :</div>
		<div class="v">
			{if $data.price_auction}
			{l i='option_checkbox_yes' gid='start'}
			{else}
			{l i='option_checkbox_no' gid='start'}
			{/if}
		</div>
	</div>
    	{/if} {*MOD hide if negotiated NO*}
	{/if}
	{if $data.date_available|strtotime > 0}
	<div class="r">
		<div class="f">{l i='field_date_available' gid='listings'} :</div>
		<div class="v">{$data.date_available|date_format:$date_format}</div>
	</div>
	{/if}
	{if $data.date_open|strtotime > 0}
	<div class="r">
		<div class="f">{l i='field_date_open' gid='listings'}:</div> 
		<div class="v">
			{$data.date_open|date_format:$date_format} 
			{if $data.date_open_begin}{ld_option i='dayhour-names' gid='start' option=$data.date_open_begin}{/if}
			{if $data.date_open_end}{if $data.date_open_begin} - {/if}{ld_option i='dayhour-names' gid='start' option=$data.date_open_end}{/if}
		</div>
	</div>
	{/if}
	{foreach item=item from=$data.field_editor}
	{if ($item.value|is_array and count($item.value) or $item.value|trim or $item.value_original|trim)}
	<div class="r">
		<div class="f">{$item.name} :</div>
		<div class="v">{if $item.value_str}{$item.value_str}{elseif $item.value_original}{$item.value_original}{else}{$item.value}{/if}
        
        {if $item.name =='Luas Tanah' || $item.name =='Luas Bangunan' }m<span class="superscript">2</span>{elseif $item.name =='Building Area' || $item.name =='Land Area'}sq.m{/if}
                
        </div>
	</div>
	{/if}
	{/foreach}
    
    <div class="t-3 headline">{$data.headline}</div>

{/if}
{if $section_gid == 'gallery'}
	<div class="photo-view">
		{if $data.photo_count}
		{js file='dualslider/jquery.dualSlider.0.3.min.js'}
		{js file='dualslider/jquery.timers-1.2.js'}
		<div class="slider">
			<div class="carousel">	
				<div class="backgrounds">
					{foreach item=item key=key from=$data.photos}
						<div class="item item_{$key}" style="background: url('{$item.media.thumbs.620_400}') no-repeat;"></div>
					{/foreach}
				</div>
				
				{if $data.photo_count > 1}
				<div class="paging_wrapper">
					<div class="paging_wrapper2">
						<div class="paging">
							<a id="previous_item" class="previous hide" alt="{l i='nav_prev' gid='start' type='button'}" title="{l i='nav_prev' gid='start' type='button'}">{l i='nav_prev' gid='start'}</a>
							<a id="next_item" class="next hide" alt="{l i='nav_next' gid='start' type='button'}" title="{l i='nav_next' gid='start' type='button'}">{l i='nav_next' gid='start'}</a>
							<span id="numbers" class="hide"><a href="#" rel=""></a></span>
						</div>
					</div>
				</div>	
				{/if}
				
				<div class="panel">
					<div class="details_wrapper">
						<div class="details">
							{foreach item=item key=key from=$fata.photos}
							<div class="detail">
								{if $item.comment}
								<div class="photo-comment">
									<div class="comment-panel">{$item.comment|truncate:255}</div>
									<div class="background"></div>
								</div>
								{/if}
							</div>
							{/foreach}
						</div>
					</div>
					
				</div>
			</div>
		</div>
		<script>{literal}
			$(function(){
				$(".slider .carousel").dualSlider({
					auto: {/literal}{if $page_data.slider_auto}true{else}false{/if}{literal},
					autoDelay: {/literal}{if $page_data.slider_auto}{$page_data.rotation*1000}{else}false{/if}{literal},
					easingCarousel: "swing",
					easingDetails: "swing",
					durationCarousel: 700,
					durationDetails: 300,
					widthsliderimage: $(".slider.carousel .backgrounds .item").width(),
					{/literal}{if $_LANG.rtl eq 'rtl'}rtl: true,{/if}{literal}
				});
				$('.slider .backgrounds .item').bind('click', function(){
					var next = $('#next_item');
					if(next.css('display') != 'none') next.trigger('click');
				});
			});
		{/literal}</script>
		{else}
		<img src="{$data.photo_default.media.thumbs.620_400}" alt="{$data.output_name|truncate:100|escape}" title="{$data.output_name|escape}">
		{/if}
	</div>
	
	{if $data.photo_count}
	{if $data.photo_count > $page_data.visible}
	{js file='jcarousellite.min.js'}
	{js file='init_carousel_controls.js'}
	<script>{literal}
		$(function(){
			var rtl = 'rtl' === '{/literal}{$_LANG.rtl}{literal}';
			var idPrev, idNext;
			if(!rtl) {
				idNext = '#directionright{/literal}{$page_data.rand}{literal}';
				idPrev = '#directionleft{/literal}{$page_data.rand}{literal}';
			} else {
				idNext = '#directionleft{/literal}{$page_data.rand}{literal}';
				idPrev = '#directionright{/literal}{$page_data.rand}{literal}';
			};
			$('#listings_carousel .carousel_block{/literal}{$page_data.rand}{literal}').jCarouselLite({
				rtl: rtl,
				visible: {/literal}{$page_data.visible}{literal},
				btnNext: idNext,
				btnPrev: idPrev,
				circular: false,
				afterEnd: function(a) {
					var index = $(a[0]).index();
					carousel_controls{/literal}{$page_data.rand}{literal}.update_controls(index);
				}
			});
			carousel_controls{/literal}{$page_data.rand}{literal} = new init_carousel_controls({
				rtl: rtl,
				carousel_images_count: {/literal}{$page_data.visible}{literal},
				carousel_total_images: {/literal}{$data.photo_count}{literal},
				btnNext: idNext,
				btnPrev: idPrev
			});
		});
	{/literal}</script>	
	{/if}	
	<script>{literal}
		$(function(){
			$('#previous_item').bind('click', function(){
				$('#listings_carousel li.active').removeClass('active').prev().addClass('active');
			});
			$('#next_item').bind('click', function(){
				$('#listings_carousel li.active').removeClass('active').next().addClass('active');
			});
			$('#listings_carousel img').bind('click', function(){
				var control = $('#numbers a');
				if(control.length){
					control.attr('rel', $(this).attr('rel')); control.trigger('click');
					$('#listings_carousel li').removeClass('active');
					$(this).parent().addClass('active');
				}
			});
		});
	{/literal}</script>	
	<div id="listings_carousel" class="carousel {if $data.photo_count <= $page_data.visible}visible{/if}">
		<div id="directionleft{$page_data.rand}" class="directionleft {if $data.photo_count <= $page_data.visible}hide{/if}">
			<div class="with-icon i-larr w" id="l_hover"></div>
		</div>
		<div class="carousel_block carousel_block{$page_data.rand} item_{$page_data.visible}_info">
			<ul>
				{counter print=false assign=counter start=0}
				{foreach item=item key=key from=$data.photos}
				{counter print=false assign=counter}
				<li {if $counter eq 1}class="active"{/if}><img src="{$item.media.thumbs.60_60}" id="listing_photo{$key}" rel="{$counter}"></li>
				{/foreach}
			</ul>
		</div>
		<div id="directionright{$page_data.rand}" class="directionright">
			<div class="with-icon i-rarr w" id="r_hover"></div>
		</div>
		<div class="clr"></div>		
	</div>
	<div class="clr"></div>
	{/if}
{/if}
{if $section_gid == 'print'}
	{if $data.photos > 0}
	<div class="tabs tab-size-15">
		<ul>
			<li>{l i='filter_section_gallery' gid='listings'}</li>
		</ul>
	</div>
	<div class="photos">
		<ul>
			{foreach item=item key=key from=$data.photos}
			<li><img src="{$item.media.thumbs.200_200}"></li>
			{/foreach}
		</ul>
		<div class="clr"></div>
	</div>
	{/if}
{/if}
{if $section_gid == 'virtual_tour'}
	{if $data.virtual_tour_count}
	<div id="panorama_block">{block name=virtual_tour_block module=listings data=$data.virtual_tour.0}</div>
	
	{js file='jcarousellite.min.js'}
	{js file='init_carousel_controls.js'}
	<script>{literal}
		$(function(){
			var rtl = 'rtl' === '{/literal}{$_LANG.rtl}{literal}';
			var idVtourPrev, idVtourNext;
			if(!rtl){
				idVtourNext = '#directionright_vtour_{/literal}{$page_data.rand}{literal}';
				idVtourPrev = '#directionleft_vtour_{/literal}{$page_data.rand}{literal}';
			} else {
				idVtourNext = '#directionleft_vtour_{/literal}{$page_data.rand}{literal}';
				idVtourPrev = '#directionright_vtour_{/literal}{$page_data.rand}{literal}';
			};
			$('#listings_vtour_carousel .carousel_block_vtour_{/literal}{$page_data.rand}{literal}').jCarouselLite({
				rtl: rtl,
				visible: {/literal}{$page_data.visible}{literal},
				btnNext: idVtourNext,
				btnPrev: idVtourPrev,
				circular: false,
				afterEnd: function(a) {
					var index = $(a[0]).index();
					carousel_controls_vtour_{/literal}{$page_data.rand}{literal}.update_controls(index);
				}
			});
			carousel_controls_vtour_{/literal}{$page_data.rand}{literal} = new init_carousel_controls({
				rtl: rtl,
				carousel_images_count: {/literal}{$page_data.visible}{literal},
				carousel_total_images: {/literal}{$data.virtual_tour_count}{literal},
				btnNext: idVtourNext,
				btnPrev: idVtourPrev,
			});
			
			$('#listings_vtour_carousel img').bind('click', function(){
				$('#listings_vtour_carousel li').removeClass('active');
				$(this).parent().addClass('active');
			});
		});
	{/literal}</script>
	<div id="listings_vtour_carousel" class="carousel {if $data.virtual_tour_count <= $page_data.visible}visible{/if}">
		<div id="directionleft_vtour_{$page_data.rand}" class="directionleft">
			<div class="with-icon i-larr w" id="l_hover"></div>
		</div>
		<div class="carousel_block carousel_block_vtour_{$page_data.rand} item_{$page_data.visible}_info">
			<ul>
				{counter print=false assign=counter start=0}
				{foreach item=item key=key from=$data.virtual_tour}
				{counter print=false assign=counter}
				<li {if $counter eq 1}class="active"{/if}><img src="{$item.media.thumbs.60_60}" id="listing_panorama{$key}" rel="{$counter}" class="panorama" data-url="{$item.media.url}" data-file="{$item.media.url|str_replace:'':$item.media.thumbs.620_400}" data-width="{$item.settings.width}" data-height="{$item.settings.height}" data-comment="{$item.comment|escape}"></li>
				{/foreach}
			</ul>
		</div>
		<div id="directionright_vtour_{$page_data.rand}" class="directionright">
			<div class="with-icon i-rarr w" id="r_hover"></div>
		</div>
		<div class="clr"></div>		
	</div>
	<div class="clr"></div>
	{/if}
{/if}
{if $section_gid == 'map'}
	{block name=show_default_map module=geomap id_user=$user_id object_id=$data.id gid='listing_view' markers=$markers settings=$map_settings width='630' height='400' only_load_scripts=$map_only_load_scripts only_load_content=$map_only_load_content}
{/if}
{if $section_gid == 'panorama'}
	{if !$panorama_only_load_scripts}<div id="pano_container" class="pano_container"></div>{/if}
	{block name=show_default_map module=geomap id_user=$data.id_user gid='listing_view' markers=$markers settings=$map_settings width='630' height='400' only_load_scripts=$panorama_only_load_scripts only_load_content=$panorama_only_load_content}	
{/if}
{if $section_gid == 'reviews'}
	{block name=get_reviews_block module=reviews object_id=$data.id type_gid='listings_object'}
{/if}
{if $section_gid == 'video'}
	<div class="r">{$data.listing_video_content.embed}</div>
{/if}
{if $section_gid == 'file'}
	<h3>{$data.listing_file_name}</h3>
	{if $data.listing_file_comment}{$data.listing_file_comment}<br><br>{/if}
	
	<div class="download-bar">
		<a class="btn-link" title="{l i='field_file_download' gid='listings' type='button'}" href="{$data.listing_file_content.file_url}" target="blank"><ins class="with-icon i-download"></ins></a>
		{$data.listing_file_content.file_name}
	</div>	
{/if}
{if $section_gid == 'calendar'}
	{block name='listings_calendar_block' module='listings' listing=$data template='view' count=2}
	{block name='listings_booking_block' module='listings' listing=$data template='form' no_save=1}
{/if}
{if $field_editor_section}
	{foreach item=section_item  key=section_key from=$sections_data}
	<h2>{$section_item.name}</h2>	
		{foreach item=item from=$data.field_editor}
		{if $item.section_gid eq $section_item.gid}
		<div class="r">
			<div class="f">{$item.name}: </div>
			<div class="v">{$item.value}</div>
		</div>
		{/if}
		{/foreach}
	{/foreach}
{/if}

