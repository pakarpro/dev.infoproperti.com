	<div id="booking_{if $full_form}full_{/if}block" class="{if !$full_form && $listing.price_period eq 2}edit_block{/if} noPrint">
		<h2>{l i='header_order_add' gid='listings'}</h2>
		<form method="post" action="" name="save_form" id="period_form{$rand}" enctype="multipart/form-data">
			{capture assign='booking_price'}
				{if $current_price > 0}
					{block name='currency_format_output' module='start' value=$current_price cur_gid=$listing.gid_currency}
				{else}
					{l i='text_negotiated_price_rent' gid='listings'}
				{/if}
			{/capture}
			<div class="r {if !$full_form && $listing.price_period eq 1}fleft{/if}">
				<div class="f">{l i='field_booking_date_start' gid='listings'}:</div>
				<div class="v periodbox">
					{switch from=$listing.price_period}
						{case value='1'}
							<input type="text" name="period[date_start]" value="{$current_date_start|date_format:$date_format|escape}" id="date_start{$rand}" class="middle">
							<input type="hidden" name="date_start_alt" value="" id="alt_date_start{$rand}">
							<script>{literal}
								$(function(){
									$('#date_start{/literal}{$rand}{literal}').datepicker({dateFormat: '{/literal}{$datepicker_date_format}{literal}', altFormat: '{/literal}{$datepicker_alt_format}{literal}', altField: '#alt_date_start{/literal}{$rand}{literal}', showOn: 'both'});
								});
							{/literal}</script>
						{case value='2'}
							{ld i='month-names' gid='start' assign='month_names'}
							<select name="date_start_month" class="middle">
								<option value="">{$month_names.header}</option>
								{foreach item=item key=key from=$month_names.option}
								<option value="{$key}" {if $key eq $current_month_start}selected{/if}>{$item}</option>
								{/foreach}
							</select>
							{assign var='cyear' value=('now'|date:'Y')}
							<select name="date_start_year" class="short">
								<option value="">{l i='text_year' gid='listings'}</option>
								{for start=0 stop=10 value=i}
								{math equation='x + y' x=$cyear y=$i assign='year'}
								<option value="{$year}" {if $year eq $current_year_start}selected{/if}>{$year}</option>
								{/for}
							</select>
					{/switch}
				</div>
			</div>
			{if !$full_form && $listing.price_period eq 1}<div class="r fleft">&nbsp;</div>{/if}
			<div class="r {if !$full_form && $listing.price_period eq 1}fleft{/if}">
				<div class="f">{l i='field_booking_date_end' gid='listings'}:</div>
				<div class="v periodbox">
					{switch from=$listing.price_period}
						{case value='1'}
							<input type="text" name="period[date_end]" value="{$current_date_end|date_format:$date_format|escape}" id="date_end{$rand}" class="middle">
							<input type="hidden" name="date_end_alt" value="" id="alt_date_end{$rand}">
							<script>{literal}
								$(function(){
									$('#date_end{/literal}{$rand}{literal}').datepicker({dateFormat: '{/literal}{$datepicker_date_format}{literal}', altFormat: '{/literal}{$datepicker_alt_format}{literal}', altField: '#alt_date_end{/literal}{$rand}{literal}', showOn: 'both'});
								});
							{/literal}</script>
						{case value='2'}
							{ld i='month-names' gid='start' assign='month_names'}
							<select name="date_end_month" class="middle">
								<option value="">{$month_names.header}</option>
								{foreach item=item key=key from=$month_names.option}
								<option value="{$key}" {if $key eq $current_month_end}selected{/if}>{$item}</option>
								{/foreach}
							</select>
							{assign var='cyear' value=('now'|date:'Y')}
							<select name="date_end_year" class="short">
								<option value="">{l i='text_year' gid='listings'}</option>
								{for start=0 stop=10 value=i}
								{math equation='x + y' x=$cyear y=$i assign='year'}
								<option value="{$year}" {if $year eq $current_year_end}selected{/if}>{$year}</option>
								{/for}
							</select>
					{/switch}
				</div>
			</div>
			{if !$full_form && $listing.price_period eq 1}<div class="r fleft">&nbsp;</div>{/if}
			<div class="r {if !$full_form && $listing.price_period eq 1}fleft{/if}">
				<div class="f">{l i='field_booking_guests' gid='listings'}:</div>
				<div class="v">
					{ld i='booking_guests' gid='listings' assign='booking_guests'}
					<select name="period[guests]" {if $full_form}class="middle"{/if}>
						{foreach item=item key=key from=$booking_guests.option}
						<option value="{$key}" {if $key eq $data.calendar_guests_min}selected{/if}>{$item}</option>
						{/foreach}
					</select>
				</div>
			</div>
			{if $full_form}
			<div class="r">
				<div class="f">{l i='field_booking_comment' gid='listings'}:</div>
				<div class="v"><textarea name="period[comment]" rows="10" cols="80"></textarea></div>
			</div>
			<div class="r">
				<div class="f">{l i='field_booking_price' gid='listings'}</div>
				<div class="v booking_price booking_price_value">{$booking_price}</div>
			</div>
			{else}
			<div class="clr r booking_price">{l i='text_price_total' gid='listings'}: <span class="booking_price_value">{$booking_price}</span></div>
			{/if}
			<div class="b clr">
				<input type="submit" name="btn_booking" value="{l i='btn_order' gid='listings' type='button'}">
			</div>
			<div class="clr"></div>
		</form>
	</div>
