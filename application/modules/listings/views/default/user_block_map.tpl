{block name=update_default_map module=geomap markers=$markers map_id='listings_map_container'}

{capture assign='sorter_block'}{strip}
	{sorter links=$sort_data.links order=$sort_data.order direction=$sort_data.direction url=$sort_data.url}
	{if $page_data.total_rows}<div class="fright">{pagination data=$page_data type='cute'}</div>{/if}
{/strip}{/capture}
{capture assign='paginator_bottom_block'}{pagination data=$page_data type='full'}{/capture}

{capture assign='list_link'}{strip}
	<a href="{$site_url}listings/user/{$user.id}/{$operation_type}/list" class="btn-link fright" title="{l i='link_view_list' gid='listings' type='button'}"><ins class="with-icon i-list"></ins></a>
{/strip}{/capture}

<script>{literal}
	$(function(){
		$('#total_rows').html('{/literal}{$page_data.total_rows}{literal}');
		$('#sorter_block').html(decodeURIComponent('{/literal}{$sorter_block|rawurlencode}{literal}'));
		$('#pages_block_2').html('{/literal}{strip}{$paginator_bottom_block|addslashes}{/strip}{literal}');
		$('#list_link').html('{/literal}{$list_link}{literal}');
	});
{/literal}</script>

