{include file="header.tpl" load_type='ui'}
{helper func_name='listings_pagination_block' module='listings' func_param=$listing}	
{block name=show_social_networks_like module=social_networking func_param=true}

<div class="rc_wrapper">
<div class="panel">
<div class="inside">

{capture assign="review_callback"}{literal}
	function(data){
		window.location.href = '{/literal}{seolink module='listings' method='view' data=$listing section='reviews'}#m_reviews{literal}';
	}
{/literal}{/capture}

<div class="lc-1" id="view_listing">
	<h1>{seotag tag='header_text'}</h1>
		<div class="actions noPrint clearfix">
			<div class="act-icon add-to-fav">{block name='save_listing_block' module='listings' listing=$listing template='icon'}</div>
			<div class="act-icon pdf-view"><a href="{seolink method='view' module='listings' data=$listing pdf='yes'}" id="pdf_btn" class="btn-link link-r-margin" title="{l i='link_pdf' gid='listings' type='button'}"><ins class="with-icon i-pdf"></ins></a></div>
			<div class="act-icon print-view"><a href="javascript:void(0);" id="print_btn" class="btn-link link-r-margin" title="{l i='link_print' gid='listings' type='button'}" onclick="javascript: window.print(); return false;"><ins class="with-icon i-print"></ins></a></div>
			<div class="act-icon share-listing"><a href="{$site_url}listings/share/{$listing.id}" id="share_btn" data-id="{$listing.id}" class="btn-link link-r-margin" title="{l i='link_share' gid='listings' type='button'}"><ins class="with-icon i-share"></ins></a></div>
			<div class="act-icon send-review">{block name='send_review_block' module='reviews' object_id=$listing.id type_gid='listings_object' responder_id=$listing.id_user success=$review_callback is_owner=$is_listing_owner}</div>
			<div class="act-icon report-spam">{block name='mark_as_spam_block' module='spam' object_id=$listing.id type_gid='listings_object' is_owner=$is_listing_owner}</div>
		
		</div>
	<div class="clr"></div>
	
	{if $section_gid|array_key_exists:$display_sections.top}
		{assign var='top_section_gid' value=$section_gid}
	{else}
		{assign var='top_section_gid' value='gallery'}
	{/if}

	{if $section_gid|array_key_exists:$display_sections.bottom}
		{assign var='bottom_section_gid' value=$section_gid}
	{else}
		{assign var='bottom_section_gid' value='overview'}
	{/if}
		
	{js module=listings file='listings-menu.js'}
	{js file='jquery.waitforimages.min.js'}
	{js file='listings-email-alert.js' module='listings'}
	{js file='listings-share.js' module='listings'}
	{js file='listings-print.js' module='listings'}
	<script>{literal}
		var tMenu;
		var bMenu;
		$(function(){
			tMenu = new listingsMenu({
				siteUrl: '{/literal}{$site_url}{literal}',
				idListing: '{/literal}{$listing.id}{literal}',
				listBlockId: 'top_listing_block',
				sectionId: 'top_listing_sections',
				currentSection: '{/literal}m_{$top_section_gid}{literal}',
			});
			bMenu = new listingsMenu({
				siteUrl: '{/literal}{$site_url}{literal}',
				idListing: '{/literal}{$listing.id}{literal}',
				currentSection: '{/literal}m_{$bottom_section_gid}{literal}',
			});
			new listingsEmailAlert({
				siteUrl: '{/literal}{$site_url}{literal}',
				idListing: '{/literal}{$listing.id}{literal}',
			});
			new listingsShare({
				siteUrl: '{/literal}{$site_url}{literal}',
				listingId: '{/literal}{$listing.id}{literal}',
			});
			
			new listingsPrint({
				siteUrl: '{/literal}{$site_url}{literal}',
				idListing: '{/literal}{$listing.id}{literal}',
			});
		});
	{/literal}</script>
	
	<div class="edit_block noPrint">
		{include file="view_listing_menu_top.tpl" module="listings" theme="default"}
		<div id="top_listing_block">
			<div id="content_m_gallery" class="view-section {if $top_section_gid ne 'gallery'}hide{/if} noPrint">{$listing_content.gallery}</div>
			<div id="content_m_map" class="view-section{if $top_section_gid ne 'map'} hide{/if} noPrint">{$listing_content.map}</div>
			<div id="content_m_panorama" class="view-section{if $top_section_gid ne 'panorama'} hide{/if} noPrint">{$listing_content.panorama}</div>
			<div id="content_m_virtual_tour" class="view-section{if $top_section_gid ne 'virtual_tour'} hide{/if} noPrint">{$listing_content.virtual_tour}</div>
			<div id="content_m_video" class="view-section{if $top_section_gid ne 'video'} hide{/if} noPrint">{$listing_content.video}</div>
		</div>
	</div>
	<div class="xview">
		<?php /* <h2>{block name=listing_price_block module='listings' data=$listing}</h2>
		<div class="t-1">
		{$listing.property_type_str} {$listing.operation_type_str}<br>
		{if $listing.sold}<span class="status_text">{l i='text_sold' gid='listings'}</span><br>{/if}
		{if $listing.is_lift_up || $listing.is_lift_up_country || $listing.is_lift_up_region || $listing.is_lift_up_city}
			<span class="status_text">{l i='status_lift_up' gid='listings'}</span><br>
		{elseif $listing.is_featured}
			<span class="status_text">{l i='status_featured' gid='listings'}</span><br>
		{/if}
		</div> */ ?>
		
		{capture assign='date_modified'}{strip}
			{$listing.date_modified|date_format:$page_data.date_format}
		{/strip}{/capture} 
		
		<div class="listing-update-date">
			<?php /* <h2>{l i='field_id' gid='listings'} {$listing.id}</h2> */ ?>
			<div class="noPrint">	
				<?php /* Disable the original code -
					<span>{if $listing.date_modified|strtotime - $listing.date_created|strtotime > 120}{l i='field_date_modified' gid='listings'}{else}{l i='field_date_created' gid='listings'}{/if}:</span> {if $date_modified}{$date_modified}{else}{l i='inactive_listing' gid='listings'}{/if}<br> 
				*/?>
				
				<?php /* The code below to display the Modified date for the listing */ ?>
				<?php /*<span>{l i='field_date_modified' gid='listings'}: {if $date_modified}{$date_modified}{else}{l i='inactive_listing' gid='listings'}{/if}</span><br> */ ?>
				<?php /* <span>{l i='field_views' gid='listings'}:</span> {$listing.views}<br> */ ?>
			</div>
		</div>
	</div>
	<div class="edit_block">
		{include file="view_listing_menu_bottom.tpl" module="listings" theme="default"}
		<div id="listing_block">
			<div id="content_m_overview" class="view-section{if $bottom_section_gid ne 'overview'} hide{/if} print_block">{$listing_content.overview}</div>
			<div id="content_m_reviews" class="view-section{if $bottom_section_gid ne 'reviews'} hide{/if} noPrint">{$listing_content.reviews}</div>			
			<div id="content_m_file" class="view-section{if $bottom_section_gid ne 'file'} hide{/if} noPrint">{$listing_content.file}</div>
			<div id="content_m_calendar" class="view-section{if $bottom_section_gid ne 'calendar'} hide{/if}">{$listing_content.calendar}</div>
			<div id="content_m_kpr" class="view-section{if $bottom_section_gid ne 'kpr'} hide{/if}">{$listing_content.kpr}</div>
		</div>
		<div class="clr"></div>
		<div id="content_m_print" class="hide print_block">{$listing_content.print}</div>
	</div>
	{helper func_name='similar_listings_block' module='listings' func_param=$listing}	
</div>
<div class="rc-2 right-panel">
	{block name='listings_booking_block' module='listings' listing=$listing no_save=1}
	{assign var=show_all_listings value=true}
	{block name="user_info" module="users" user=$listing.user all_listing=true}
	{helper func_name='show_banner_place' module='banners' func_param='right-banner'}
	{helper func_name='show_mortgage_calc' module='listings'}
</div>

<div class="clr"></div>

</div>
</div>
</div>

{include file="footer.tpl"}
