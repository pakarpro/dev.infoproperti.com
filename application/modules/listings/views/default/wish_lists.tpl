{include file="header.tpl"}

<h1>{seotag tag='header_text'}</h1>

{foreach item=item from=$wish_lists}
	<div class="listing-block">
		<div id="item-block-{$item.id}" class="item wish_list">
			<h3><a href="{$site_url}listings/wish_list/{$item.id}">{$item.output_name|truncate:50}</a></h3>
		</div>
	</div>
{foreachelse}
	<div class="item empty">{l i='no_wish_lists' gid='listings'}</div>
{/foreach}
{if $listings}<div id="pages_block_2">{pagination data=$page_data type='full'}</div>{/if}

{include file="footer.tpl"}
