{js module=listings file='mortgage-calc.js'}
<script>
{literal}
    function onChangeRupiah(value)
    {
        var propertiVal = document.getElementById("properti_value").value;
        var dpPercent = document.getElementById("deposit_value_percentage").value;
        var dpRupiah = document.getElementById("deposit_value_rp");
        var principal = document.getElementById("principal");
        
        dpPercent = dpPercent / 100;
    
        if(propertiVal.length >= 1 || propertiVal.length != "")
        {
            dpRupiah.value = propertiVal * dpPercent;
			principal.value = (propertiVal - dpRupiah.value);
        }
        else if(propertiVal == 0 || propertiVal == "")
            {
                dpRupiah.value = 0 * dpPercent;
				principal.value = (propertiVal - dpRupiah.value);
            }
        
    }
    
    function onChangePercent(value)
    {
        var propertiVal = document.getElementById("properti_value").value;
        var dpPercent = document.getElementById("deposit_value_percentage");
        var dpRupiah = document.getElementById("deposit_value_rp").value;
        var principal = document.getElementById("principal");
    
        dpPercent.value = (dpRupiah/propertiVal) * 100;
        principal.value = (propertiVal - dpRupiah);
    }
{/literal}
</script>
<div class="mortgage_calc_block edit_form">
	<div class="inside">
		<h2>{l i='mortgage_calc_title' gid='listings'}</h2>
		<form method="POST">
			<div class="r">
				<label>1.Harga properti *</label>
				<input type="text" name="properti_value" size="15" id="properti_value" onKeyUp="onChangeRupiah(this.value)">
			</div>
			<div class="r">
				<label>2.Uang muka *</label>
				<input class="deposit_value_percentage" type="text" name="deposit_value_percentage" size="5" value="30" id="deposit_value_percentage" onKeyUp="onChangeRupiah(this.value)">&nbsp;%<br />
				<input class="deposit_value_rp" type="text" name="deposit_value_rp" size="15" id="deposit_value_rp" onKeyUp="onChangePercent(this.value)">
			</div>			
			<div class="r">
				<label>3.&nbsp;{l i='mortgage_calc_query1' gid='listings'}&nbsp;*</label>
				<input type="text" name="principal" size="15" id="principal">
			</div>
			<div class="r">
				<label>4.&nbsp;{l i='mortgage_calc_query2' gid='listings'}&nbsp;*</label>
				<input type="text" name="intRate" size="15" value="10">
			</div>
			<div class="r">
				<label>5.&nbsp;{l i='mortgage_calc_query3' gid='listings'}&nbsp;*</label>
				<input type="text" name="numYears" size="15" value="10">
			</div>
			<div class="r">
				<input type="button" value="{l i='mortgage_calc_compute' gid='listings' type='button'}" id="calc_btn">
				<input type="button" value="{l i='mortgage_calc_reset' gid='listings' type='button'}" id="empty_btn">
			</div>
			<div class="r">
				<label>6.&nbsp;{l i='mortgage_calc_query4' gid='listings'}&nbsp;*</label>
				<input type="text" name="moPmt" size="15" dir="ltr">
				<input type="hidden" name="HmoPmt" value=0 size="15">
			</div>
			<p>{l i='mortgage_calc_create' gid='listings'}</p>
			<p><input type="button" value="{l i='mortgage_calc_createbutton' gid='listings' type='button'}" id="results_btn"></p>
			<p class="subtext">{l i='mortgage_calc_content2' gid='listings' type='button'}</p>
		</form>
	</div>
</div>
{capture assign='currency'}{strip}
	{block name='currency_output' module='start' value='%s'}
{/strip}{/capture}
<script>{literal}
	$(function(){
		new mortgageCalc({
			siteUrl: '{/literal}{$site_url}{literal}',
			selectedCurrAbbr: '{/literal}{$currency|strip_tags}{literal}',
		});
	});
{/literal}</script>