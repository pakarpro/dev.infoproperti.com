<div class="tabs tab-size-15">
	<ul id="top_listing_sections">
		{foreach item=item key=sgid from=$display_sections.top}
		{if $item}<li id="m_{$sgid}" sgid="{$sgid}" class="{if $sgid eq $top_section_gid}active{else}noPrint{/if}"><a href="{$site_url}listings/view/{$listing.id}/{$sgid}">{l i='filter_section_'+$sgid gid='listings'}</a></li>{/if}
		{/foreach}
	</ul>
</div>
