<div class="tabs tab-size-15">
	<ul id="listing_sections">
		{foreach item=item key=sgid from=$display_sections.bottom}
		{if $item}<li id="m_{$sgid}" sgid="{$sgid}" class="{if $bottom_section_gid eq $sgid}active{/if} {if $sgid ne 'overview'}noPrint{/if}"><a href="{$site_url}listings/view/{$listing.id}/{$sgid}">{l i='filter_section_'+$sgid gid='listings'} {if $sgid eq 'reviews'}({$listing.review_count}){/if}</a></li>{/if}
		{/foreach}
	</ul>
</div>
