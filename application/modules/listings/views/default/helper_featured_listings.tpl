{if $listings}
<h2>{l i='header_featured_listings' gid='listings'}</h2>
<div class="featured_listings_block">
	{foreach item=item key=key from=$listings}
	<div class="listing{if $key is div by 8} first{/if}">
		<a href="{$site_url}listings/view/{$item.id}"><img src="{$item.media.photo.thumbs.middle}" alt="{$item.output_name|escape}" title="{$item.output_name|escape}"></a><br>{$item.output_name|truncate:30}
	</div>
	{/foreach}
</div>
{/if}
