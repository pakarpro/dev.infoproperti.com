{include file="header.tpl" load_type='ui'}
{if $listing.price_reduced neq 0}
	<input type="hidden" value ="{$listing.price_reduced}" id="listing_price" />
{else}
	<input type="hidden" value ="{$listing.price}" id="listing_price" />
{/if}
<!-- this might cause conflict with id -->
{if $propertiID}
	<input type="hidden" value ="{$propertiID}" id="properti" />
{/if}
{if $bank}
	<input type="hidden" value ="{$bank}" id="bank" />
{/if}

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>
{literal}
$(document).ready(function(){
function numberWithCommas(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}
	//serves as a timer delay functions
	var delay = (function(){
	  var timer = 0;
	  return function(callback, ms){
		clearTimeout (timer);
		timer = setTimeout(callback, ms);
	  };
	})();
	
	function kpr_table(){
		var propertiID=$('#properti').val();
		var promosiID=$('#promosi').val();
		var bank=$('#bank').val();
		$('#tabletest').empty();
		$.post("listings-kpr_get_bank",{ propertiID: propertiID, promosiID: promosiID, bank: bank}, function(json) {
			var data = JSON.parse(json);
			i=0;
			$('#tabletest').append(
			"<thead>"+
			"<tr>"+
				"<th style='background-color:#3a5a98; color:white;'>Bunga</td>"+
				"<th style='background-color:#3a5a98; color:white;'>Masa Promosi</td>"+
				"<th style='background-color:#3a5a98; color:white;'>Masa pinjaman</td>"+
				"<th style='background-color:#3a5a98; color:white;'>Cicilan Pokok</td>"+
				"<th style='background-color:#3a5a98; color:white;'>Bunga per Bulan</td>"+
				"<th style='background-color:#3a5a98; color:white;'>Angsuran per Bulan</td>"+
			"</tr>"+
			"</thead>"
			);
			$.each(data, function(){
				var pinjaman_get = $('#sisapinjaman').val();
				var pinjaman_comma = parseFloat(pinjaman_get.replace(/,/g, ''))
				var pinjaman_pokok = pinjaman_comma;
				var lama_pinjaman = $('#masa_pinjaman').val();
				var cicilan_pokok = pinjaman_pokok/(lama_pinjaman*12);
				var cicilan_pokok_rounded = (cicilan_pokok).toFixed(2);
				var bunga_bulan = (pinjaman_pokok*data[i].bunga/12/100).toFixed(2);
				var cicilan_bulan = (parseFloat(bunga_bulan)+parseFloat(cicilan_pokok_rounded)).toFixed(2);
				var image_check =  data[i].logo;
				if(i%2==0){
					if(image_check!='')
					{
			//$base_url only works if it's placed outside of the literal tags
						$('#tabletest').append(
						"<tr>"+
							"<td id='bungabank"+i+"' style='background-color:#ebedfa;'>"+data[i].bunga+"%</td>"+
							"<td style='background-color:#ebedfa;'>"+data[i].periode+" Tahun</td>"+
							"<td style='background-color:#ebedfa;'>"+lama_pinjaman+" Tahun</td>"+
							"<td style='background-color:#ebedfa;'> Rp. "+numberWithCommas(cicilan_pokok_rounded)+"</td>"+
							"<td style='background-color:#ebedfa;'> Rp. "+numberWithCommas(bunga_bulan)+"</td>"+
							"<td style='background-color:#ebedfa;'> Rp. "+numberWithCommas(cicilan_bulan)+"</td>"+
						"</tr>"
						);
					}
					else
					{
						$('#tabletest').append(
						"<tr>"+
							"<td id='bungabank"+i+"' style='background-color:#ebedfa;'>"+data[i].bunga+"%</td>"+
							"<td style='background-color:#ebedfa;'>"+data[i].periode+" Tahun</td>"+
							"<td style='background-color:#ebedfa;'>"+lama_pinjaman+" Tahun</td>"+
							"<td style='background-color:#ebedfa;'> Rp. "+numberWithCommas(cicilan_pokok_rounded)+"</td>"+
							"<td style='background-color:#ebedfa;'> Rp. "+numberWithCommas(bunga_bulan)+"</td>"+
							"<td style='background-color:#ebedfa;'> Rp. "+numberWithCommas(cicilan_bulan)+"</td>"+
						"</tr>"
						);
					}
				i++;
				}
				else
				{
					if(image_check!='')
					{
						$('#tabletest').append(
						"<tr>"+
							"<td id='bungabank"+i+"' style='background-color:white;'>"+data[i].bunga+"%</td>"+
							"<td style='background-color:white;'>"+data[i].periode+" Tahun</td>"+
							"<td style='background-color:white;'>"+lama_pinjaman+" Tahun</td>"+
							"<td style='background-color:white;'> Rp. "+numberWithCommas(cicilan_pokok_rounded)+"</td>"+
							"<td style='background-color:white;'> Rp. "+numberWithCommas(bunga_bulan)+"</td>"+
							"<td style='background-color:white;'> Rp. "+numberWithCommas(cicilan_bulan)+"</td>"+
						"</tr>"
						);
					}
					else
					{
						$('#tabletest').append(
						"<tr>"+
							"<td id='bungabank"+i+"' style='background-color:white;'>"+data[i].bunga+"%</td>"+
							"<td style='background-color:white;'>"+data[i].periode+" Tahun</td>"+
							"<td style='background-color:white;'>"+lama_pinjaman+" Tahun</td>"+
							"<td style='background-color:white;'> Rp. "+numberWithCommas(cicilan_pokok_rounded)+"</td>"+
							"<td style='background-color:white;'> Rp. "+numberWithCommas(bunga_bulan)+"</td>"+
							"<td style='background-color:white;'> Rp. "+numberWithCommas(cicilan_bulan)+"</td>"+
						"</tr>"
						);
					}
				i++;
				}
			});
		
		});
	
			
	}
	// RUN BY DEFAULT
	//kpr_table();
	// START OPTIONAL FUNCTIONS
	$('#properti').change(function(){
		kpr_table();
	});
	$('#promosi').change(function(){
		kpr_table();
	});
	
	$('#masa_pinjaman').change(function(){
		kpr_table();		
	});
	
	//added mod support to be able to directly output KPR calculation based on listings from Infoproperti
	if($('#listing_price').val()!='')
	{
		$('#pinjaman').val($('#listing_price').val());
		$('#check_pinjaman').hide();
		$('#pinjaman_check').empty();
		var check_dp = $('#dp').val();
		var check_pinjaman = $('#pinjaman').val();
		//$('#pinjaman_check').val(print_pinjaman);
			if(($.isNumeric(check_dp)==true) && ($.isNumeric(check_pinjaman)==true)){
				//append commas to a span below the input box
				var pinjaman_comma = numberWithCommas(check_pinjaman);
				var print_pinjaman = 'Rp. '+pinjaman_comma;
				$('#pinjaman_check').append(print_pinjaman);
				
				
				var pinjaman = $('#pinjaman').val();
				var dp = $('#dp').val();
				var sisapinjaman = pinjaman-(pinjaman*dp/100);
				var test_sisapinjaman = numberWithCommas(sisapinjaman)
				$('#sisapinjaman').val(test_sisapinjaman);	
					
				$('#tabletest').empty();
				delay(function(){
					kpr_table();
				}, 2000);
				$('#pinjaman').val(pinjaman_comma);
			}
			else if($.isNumeric(check_pinjaman)==false&&check_pinjaman!=''){
				$('#check_pinjaman').show();
			}
	}

	$('#pinjaman').keyup(function(){
	$('#check_pinjaman').hide();
	$('#pinjaman_check').empty();
	var check_dp = $('#dp').val();
	var check_pinjaman = $('#pinjaman').val();
	//$('#pinjaman_check').val(print_pinjaman);
		if(($.isNumeric(check_dp)==true) && ($.isNumeric(check_pinjaman)==true)){
			//append commas to a span below the input box
			var pinjaman_comma = numberWithCommas(check_pinjaman);
			var print_pinjaman = 'Rp. '+pinjaman_comma;
			$('#pinjaman_check').append(print_pinjaman);
			
			
			var pinjaman = $('#pinjaman').val();
			var dp = $('#dp').val();
			var sisapinjaman = pinjaman-(pinjaman*dp/100);
			var test_sisapinjaman = numberWithCommas(sisapinjaman)
			$('#sisapinjaman').val(test_sisapinjaman);	
				
			$('#tabletest').empty();
			delay(function(){
				kpr_table();
			}, 2000);
		}
		else if($.isNumeric(check_pinjaman)==false&&check_pinjaman!=''){
		 	$('#check_pinjaman').show();
		}
	});
	
	$('#pinjaman').blur(function(){
		var pinjaman_check = $('#pinjaman').val();
		var pinjaman_comma = numberWithCommas(pinjaman_check);
		$('#pinjaman').val(pinjaman_comma);
	});
	
	$('#pinjaman').focus(function(){
		var pinjaman_check = $('#pinjaman').val();
		var pinjaman_int = parseFloat(pinjaman_check.replace(/,/g, ''))
		if(pinjaman_check=='')
		{
			$('#pinjaman').val('');
		}
		else
		{
			$('#pinjaman').val(pinjaman_int);
		}
	});
	
	$('#dp').keyup(function(){
	$('#check_dp').hide();	
	var check_dp = $('#dp').val();
	var check_pinjaman = $('#pinjaman').val();
	var pinjaman = parseFloat(check_pinjaman.replace(/,/g, ''))
		if(($.isNumeric(check_dp)==true) && ($.isNumeric(pinjaman)==true)){
			
			//var pinjaman = $('#pinjaman').val();
			var dp = $('#dp').val();
			var sisapinjaman = pinjaman-(pinjaman*dp/100);
			var sisapinjaman_comma = numberWithCommas(sisapinjaman);
			$('#sisapinjaman').val(sisapinjaman_comma);	
				
			$('#tabletest').empty();
			delay(function(){
				kpr_table();
			}, 1000);
		}
		else if($.isNumeric(check_dp)==false){
		 	$('#check_dp').show();
		}
		
	});
});
{/literal}
</script>
<div class="kpr_wrapper">

<form>
<div class="kpr_header">Kalkulasi Cicilan KPR</div>
<div style="position:relative;width:250px; float:left;"></div>
<div class="kpr_input" style="position:relative; float:left;">
	<table style="width:850px;"  cellpadding="5" cellspacing="0">
    	<tr>
        	<td colspan="2">
            	KPR {$bank_detail.name}
            </td>
            <td style="text-align:right;">
            	<a href="listing-{$listing.property}-in-{$listing.city}-id-{$listing.id}-{$listing.section}-operation-{$listing.operation_type}-{$listing.pdf}">{$listing.gid}</a>
            </td>
        </tr>
    	<tr>
        	<td colspan="2">
            	<!-- edited, source is from kpr cms -->
                <img src="{$base_url}ext/kpr_cms/images/logo/{$bank_detail.logo}" style="height:100px; border:0px;" />
            	<!--ORIGINAL ONE, LOCAL TO PAKARPRO<img src="{$base_url}application/views/default/img/logo/{$bank_detail.logo}" style="height:100px; border:0px;" />-->
            </td>
            <td align="right">
            	<img src="{$listing.media.photo.thumbs.big}" style="height:100px; border:0px;"/>
            </td>
        </tr>
    	<tr>
        	<td>
            Harga Properti <font color="red"><span id="check_pinjaman" style="display:none;">Must be Numeric!</span></font><br/>
            <input name="pinjaman" id="pinjaman" type="text" style="width:100%;" value="0" fcsa-number/>
            </td>
            <td>
            Down Payment (Uang Muka) <font color="red"><span id="check_dp" style="display:none;">Must be Numeric!</span></font><br/>
            <input name="dp" id="dp" type="text" value="30" size="2" />%
            </td>
            <td align="right"style="padding-left:140px;">
            	Luas Tanah : {$listing.square_output}
            </td>
        </tr>
        <tr>
        	<td><font color="#ae8d6a"><span id="pinjaman_check"></span></font></td>
            <td></td>
            <td align="right" style="padding-left:140px;">
            	Luas Bangunan : {$land_area} sq.m.
            </td>
        </tr>
        <tr>
        	<td>
            Pinjaman<br/>
            <input name="sisapinjaman" id="sisapinjaman" value="0"  type="text" disabled="disabled" style="width:100%; background-color:#E6E6E6; cursor:not-allowed;"/>
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr>
        	<td colspan="2">
            <table cellspacing="10">
                <tr>
                    <td>Tipe Properti</td>
                    <td>Lama Pinjaman</td>
            	{if !$bank}
                    <td>Masa Promosi</td>
                {/if}
                </tr>
                <tr>
                    <td>
                    <select name="properti" id="properti">
                    {foreach item=item from=$properti}
                    <option value="{$item->propertiID}" {if $item->propertiID==$propertiID}selected="selected"{/if}>{$item->namaproperti}</option>
                    {/foreach}
                    </select>
                    </td>
                    <td>
                    <select name="masa_pinjaman" id="masa_pinjaman" >
                    {foreach item=item from=$pinjaman}
                    <option value="{$item->lamapinjaman}">{$item->lamapinjaman} Tahun</option>
                    {/foreach}
                    </select>
                    </td>
                <!-- show this option on normal KPR page, hide this when it's the special kpr page -->
            	{if !$bank}
                    <td>
                    <select name="promosi" id="promosi">
                    {foreach item=item from=$promosi}
                    <option value="{$item->promosiID}">{$item->periode} Tahun</option>
                    {/foreach}
                    </select>
                    </td>
               	{/if}
                </tr>
            
            </table>
            </td>
            <td></td>
            
        </tr>
    </table>
</div>
	<!-- top part form -->
    <br/><br/>
</form>      
    <table id="tabletest" align="center" style="width:100%; margin-bottom:25px; margin-top:25px; border:1px black solid;"  cellpadding="10" cellspacing="0">
    
    </table>
    

</div>


{if $bank}
	{include file="footer.tpl" load_type='ui'}
{/if}