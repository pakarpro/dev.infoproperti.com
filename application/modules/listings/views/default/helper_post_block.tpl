<li>
	<a href="{$site_url}listings/edit" id="post_listing_btn" class="post-btn">{l i='link_add_listing' gid='listings'}&nbsp;<span class="plus">+</span></a>
	{if $is_guest}
	<script>{literal}
		$(function(){
			$('#post_listing_btn').bind('click', function(){
				$('html, body').animate({
					scrollTop: $("#ajax_login_link").offset().top
				}, 2000);
				$("#ajax_login_link").click();
				return false;
			});
		});
	{/literal}</script>
	{/if}
</li>
