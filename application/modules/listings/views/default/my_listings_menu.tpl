	{if $data.id}
	<div class="tabs tab-size-15" id="edit_listings_menu">
		<ul>
			<li class="{if $section_gid eq 'overview'}active{/if}"><a href="{$site_url}listings/edit/{$data.id}/overview">{l i='filter_section_overview' gid='listings'}</a></li>
			<li class="{if $section_gid eq 'description'}active{/if}"><a href="{$site_url}listings/edit/{$data.id}/description">{l i='filter_section_description' gid='listings'}</a></li>
			{if $data.operation_type eq 'sale' || $data.operation_type eq 'rent'}<li class="{if $section_gid eq 'gallery'}active{/if}"><a href="{$site_url}listings/edit/{$data.id}/gallery">{l i='filter_section_gallery' gid='listings'}</a></li>{/if}
			{if $data.operation_type eq 'rent'}<li class="{if $section_gid eq 'calendar'}active{/if}"><a href="{$site_url}listings/edit/{$data.id}/calendar">{l i='filter_section_calendar' gid='listings'}</a></li>{/if}
			{depends module=geomap}<li class="{if $section_gid eq 'map'}active{/if}"><a href="{$site_url}listings/edit/{$data.id}/map">{l i='filter_section_map' gid='listings'}</a></li>{/depends}
			<li class="{if $section_gid eq 'activity'}active{/if}"><a href="{$site_url}listings/activity/{$data.id}">{l i='filter_section_activity' gid='listings'}</a></li>
			{depends module=services}{if $data.operation_type ne 'buy' && $data.operation_type ne 'lease' && $data.status}<li class="{if $section_gid eq 'services'}active{/if}"><a href="{$site_url}listings/services/{$data.id}">{l i='filter_section_services' gid='listings'}</a></li>{/if}{/depends}
		</ul>
		&nbsp;
	</div>	
	{/if}
