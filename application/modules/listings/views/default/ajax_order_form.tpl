<div class="content-block load_content">
	<h1>{l i='header_order_add' gid='listings'}</h1>
	<div class="inside edit_block">
		<form method="post" action="{$lisitng.action|escape}" name="save_form" id="order_form" enctype="multipart/form-data">
			<div class="r fleft">
				<div class="f">{l i='field_booking_date_start' gid='listings'}:</div>
				<div class="v periodbox">
					{switch from=$listing.price_period}
						{case value='1'}
							<input type="text" name="period[date_start]" value="{$current_date_start|strtotime|date_format:$page_data.date_format|escape}" id="date_start{$rand}" class="middle"> <a href="#" id="date_start_open_btn" title="{l i='link_calendar_open' gid='listings' type='button'}"><ins class="with-icon i-calendar"></ins></a>
							<input type="hidden" name="date_start_alt" value="" id="alt_date_start{$rand}">
							<script>{literal}
								$(function(){
									$('#date_start{/literal}{$rand}{literal}').datepicker({dateFormat: '{/literal}{$page_data.datepicker_date_format}{literal}', altFormat: '{/literal}{$page_data.datepicker_alt_format}{literal}', altField: '#alt_date_start{/literal}{$rand}{literal}', showOn: 'both'});
								});
							{/literal}</script>
						{case value='2'}
							{ld i='month-names' gid='start' assign='month_names'}
							<select name="date_start_month" class="middle">
								<option value="">{$month_names.header}</value>
								{foreach item=item key=key from=$month_names.option}
								<option value="{$key}" {if $key eq $current_month_start}selected{/if}>{$item}</option>
								{/foreach}
							</select>
							{assign var='cyear' value=('now'|date:'Y')}
							<select name="date_start_year" class="short">
								<option value="">{l i='text_year' gid='listings'}</option>
								{for start=0 stop=10 value=i}
								{math equation='x + y' x=$cyear y=$i assign='year'}
								<option value="{$year}" {if $year eq $current_year_start}selected{/if}>{$year}</option>
								{/for}
							</select>
					{/switch}
				</div>
			</div>
			<div class="r fleft">&nbsp;</div>
			<div class="r fleft">
				<div class="f">{l i='field_booking_date_end' gid='listings'}:</div>
				<div class="v periodbox">
					{switch from=$listing.price_period}
						{case value='1'}
							<input type="text" name="period[date_end]" value="{$current_date_end|strtotime|date_format:$page_data.date_format|escape}" id="date_end{$rand}" class="middle"> <a href="#" id="date_end_open_btn" title="{l i='link_calendar_open' gid='listings' type='button'}"><ins class="with-icon i-calendar"></ins></a>
							<input type="hidden" name="date_end_alt" value="" id="alt_date_end{$rand}">
							<script>{literal}
								$(function(){
									$('#date_end{/literal}{$rand}{literal}').datepicker({dateFormat: '{/literal}{$page_data.datepicker_date_format}{literal}', altFormat: '{/literal}{$page_data.datepicker_alt_format}{literal}', altField: '#alt_date_end{/literal}{$rand}{literal}', showOn: 'both'});
								});
							{/literal}</script>
						{case value='2'}
							{ld i='month-names' gid='start' assign='month_names'}
							<select name="date_end_month" class="middle">
								<option value="">{$month_names.header}</value>
								{foreach item=item key=key from=$month_names.option}
								<option value="{$key}" {if $key eq $current_month_end}selected{/if}>{$item}</option>
								{/foreach}
							</select>
							{assign var='cyear' value=('now'|date:'Y')}
							<select name="date_end_year" class="short">
								<option value="">{l i='text_year' gid='listings'}</option>
								{for start=0 stop=10 value=i}
								{math equation='x + y' x=$cyear y=$i assign='year'}
								<option value="{$year}" {if $year eq $current_year_end}selected{/if}>{$year}</option>
								{/for}
							</select>
					{/switch}
				</div>
			</div>
			<div class="r clr">
				<div class="f">{l i='field_booking_guests' gid='listings'}:</div>
				<div class="v">
					{ld i='booking_guests' gid='listings' assign='booking_guests'}
					<select name="period[guests]" class="middle">
						{foreach item=item key=key from=$booking_guests.option}
						<option value="{$key}" {if $key eq $order.guests}selected{/if}>{$item}</option>
						{/foreach}
					</select>
				</div>
			</div>
			<div class="r">
				<div class="f">{l i='field_booking_comment' gid='listings'}:</div>
				<div class="v" id="period_comment">
					<textarea name="period[comment]" rows="10" cols="80">{$order.comment|escape}</textarea>
				</div>
			</div>
		
			{if $show_price}
			{capture assign='booking_price'}
				{if $current_price > 0}
					{block name='currency_format_output' module='start' value=$current_price cur_gid=$listing.gid_currency}
				{else}
					{l i='text_negotiated_price_rent' gid='listings'}
				{/if}
			{/capture}
			<div class="r booking_price">{l i='text_price_total' gid='listings'}: <span class="booking_price_value">{$booking_price}</span></div>
			{/if}
			
			<div class="calendar_link hide fright">
				<a href="{seolink module='listings' method='view' data=$listing section='calendar'}#m_calendar">{l i='link_calendar_view' gid='listings'}</a>
			</div>

			<div class="b">
				<input type="submit" name="btn_save" value="{l i='btn_save' gid='start' type='button'}" id="close_btn">
			</div>
		</form>
	</div>
</div>
