{include file="header.tpl"}

{include file="left_panel.tpl" module="start"}

<div class="rc">
	<div class="content-block">
		<h1>{l i='header_my_listings' gid='listings'} ({$listings_count_sum})</h1>

		<div class="search-links">
			{l i='link_search_in_my_listings' gid='listings'}
			<div class="edit_block">			
				{if !$noshow_add_button}<a class="btn-link fright my-list-add-listing" href="{$site_url}listings/edit"><ins class="with-icon i-expand"></ins><span>{l i='link_add_listing' gid='listings'}</span></a>{/if}			
				<form action="" method="post" enctype="multipart/form-data" id="listings_search_form">
				<div class="r">
					<input type="text" name="data[keyword]" value="{$page_data.keyword|escape}" id="listings_search" autocomplete="off" />
				</div>
				</form>
			</div>
		</div>

		<div class="tabs tab-size-15 noPrint">
			<ul id="my_listings_sections">
				{foreach item=tgid from=$operation_types}
				<li id="m_{$tgid}" sgid="{$tgid}" class="{if $current_operation_type eq $tgid}active{/if}"><a href="{$site_url}listings/my/{$tgid}">{l i='operation_search_'+$tgid gid='listings'} ({$listings_count[$tgid]})</a></li>
				{/foreach}
			</ul>
		</div>

		<div id="listings_block">{$block}</div>
		
		{js module=users_services file='available_view.js'}
		{js module=listings file='listings-list.js'}
		<script>{literal}
		$(function(){
			new listingsList({
				siteUrl: '{/literal}{$site_url}{literal}',
				listAjaxUrl: '{/literal}listings/ajax_my{literal}',
				sectionId: 'my_listings_sections',
				operationType: '{/literal}{$current_operation_type}{literal}',
				order: '{/literal}{$order}{literal}',
				orderDirection: '{/literal}{$order_direction}{literal}',
				page: {/literal}{$page}{literal},
				tIds: ['pages_block_1', 'pages_block_2', 'sorter_block'],
			});
		});
		{/literal}</script>
	</div>
</div>
<div class="clr"></div>
{include file="footer.tpl"}
