<h2>{l i='header_'+$type+'_categories_search_block' gid='listings'}</h2>
<div class="listing-property_types-block"><div class="inside">
	{foreach item=categories from=$listings_categories }
	<ul>
		{foreach item=item from=$categories key=key}
		<li class="{if $item.group}group{/if} {if $item.empty}empty{/if}">
			{if $item.group}
			<h3>{$item.name}</h3>
			{elseif $item.statistics.listings_active}
			<a href="{seolink module='listings' method='category' data=$item.data}">{$item.name} ({$item.statistics.listings_active})</a>
			{else}
			{$item.name}	
			{/if}
		</li>
		{/foreach}
	</ul>
	{/foreach}
</div></div>
