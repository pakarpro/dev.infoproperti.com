<div class="edit_block load_content">
	<h1>{if $data.id}{l i='header_edit_panorama' gid='listings'}{else}{l i='header_add_panorama' gid='listings'}{/if}</h1>
	<div class="inside edit_block">
		<div class="tabs tab-size-15 noPrint">
			<ul id="photo_edit_sections">
				<li id="m_view" sgid="view" class="active"><a href="{$site_url}upload_gallery/view/{$data.id}">{l i='link_photo_view' gid='listings'}</a></li>
				<li id="m_comment" sgid="comment"><a href="{$site_url}upload_gallery/comment/{$data.id}">{l i='link_photo_comment' gid='listings'}</a></li>
				<li id="m_rotate" sgid="rotate"><a href="{$site_url}upload_gallery/rotate/{$data.id}/90">{l i='link_photo_rotate' gid='listings'}</a></li>
				<li id="m_recrop" sgid="recrop"><a href="{$site_url}listings/recrop/{$data.id}/small">{l i='link_photo_recrop' gid='listings'}</a></li>
			</ul>
		</div>
		<div id="photo_edit_block">
			<div id="content_m_view" class="view-section">
				<div id="photo_edit">
					<div class="source_box">
						<div id="photo_source_view_box" class="photo_source_box">
							<div class="imagebox">
								<img src="{$data.media.file_url}?{''|time}" {if 344*$data.settings.width > 500*$data.settings.height}width="{$data.settings.width|min:500}"{else}height="{$data.settings.height|min:344}"{/if} id="photo_source_view">
								<div class="statusbar {if !$data.comment}hide{/if}">
									<div class="panel">{$data.comment|truncate:255}</div>
									<div class="background"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="b outside" id="actionbar_basic">
					{if $data.media.file_name && $data.id}<input type="button" class="btn" value="{l i='btn_delete' gid='start' type='button'}" name="btn_delete" onclick="javascript: vtourUpload.delete_photo({$data.id});">{/if}
				</div>				
				<div class="clr"></div>
			</div>
			<div id="content_m_comment" class="hide view-section">
				<form action="" method="post" enctype="multipart/form-data">
					<div class="r">
						<div class="f">{l i='field_photo_comment' gid='listings'}</div>
						<div class="v"><textarea name="comment" rows="10" cols="80" id="panorama_comment">{$data.comment|escape}</textarea></div>
					</div>
					<div class="b outside" id="actionbar_basic">
						{if $data.media.file_name && $data.id}<input type="button" class="btn" value="{l i='btn_save' gid='start' type='button'}" name="btn_comment" onclick="javascript: vtourUpload.save_photo_data({$data.id});">{/if}
					</div>	
				</form>
				<div class="clr"></div>
			</div>
			<div id="content_m_rotate" class="hide view-section">
				<div id="photo_edit">
					<div class="source_box">
						<div id="photo_source_rotate_box" class="photo_source_box">
							<img src="{$data.media.file_url}?{''|time}" {if 4*$data.settings.width > 5*$data.settings.height}width="{$data.settings.width|min:500}"{else}height="{$data.settings.height|min:400}"{/if} id="photo_source_rotate">
						</div>
						<div class="statusbar">
							<div class="panel">
								<a href="javascript:void(0);" class="btn-link" id="photo_rotate_left"><ins class="with-icon i-rotate-left w"></ins></a>
								<a href="javascript:void(0);" class="btn-link" id="photo_rotate_right"><ins class="with-icon i-rotate-right w"></ins></a>
							</div>
							<div class="background"></div>
						</div>
					</div>
				</div>
			</div>
			<div id="content_m_recrop" class="hide view-section">
				<form action="" method="post" enctype="multipart/form-data">
					<div id="photo_edit">
						<div class="source_box">
							<ul id="photo_sizes">
								{foreach item=item key=key from=$selections}
								{counter print=false assign=counter}
								{if $counter ne 1}<li>&rarr;</li>{/if}
								<li id="photo_size_{$key}" class="{if $counter eq 1}active{/if} photo_size"><span>{l i='photo_size_'+$key gid='listings'}</span></li>								
								{/foreach}
							</ul>
							<div id="photo_source_recrop_box" class="photo_source_box">
								<img src="{$data.media.file_url}?{''|time}" {if 344*$data.settings.width > 500*$data.settings.height}width="{$data.settings.width|min:500}"{else}height="{$data.settings.height|min:344}"{/if} id="photo_source_recrop">
							</div>
							<div class="statusbar">
								<div class="panel">
									<a href="javascript:void(0);" class="btn-link" id="recrop_btn"><ins class="with-icon i-crop w"></ins></a>
								</div>
								<div class="background"></div>
							</div>
						</div>
					</div>
					<div class="clr"></div>
				</form>
			</div>
		</div>
	</div>
</div>
{if $data.id}
<script>{literal}	
	var vEdit;
	$(function(){
		vEdit = new photoEdit({
			siteUrl: '{/literal}{$site_url}{literal}',
			imageWidth: {/literal}{$data.settings.width}{literal},
			imageHeight: {/literal}{$data.settings.height}{literal},
			idPhoto: {/literal}{$data.id}{literal},
			parentImage: '{/literal}listing_photo_{$data.id}{literal}',
			parentThumb: 'small',
		});
		
		{/literal}
		{foreach item=item key=key from=$selections}
		vEdit.add_selection('{$key}', 0, 0, {$item.width|min:$data.settings.width}, {$item.height|min:$data.settings.height}, {$item.width}, {$item.height});
		{/foreach}
		{literal}
	});	
{/literal}</script>	
{/if}
