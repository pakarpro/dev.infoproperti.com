{include file="header.tpl"}

{include file="left_panel.tpl" module="start"}

<div class="rc">
	<div class="content-block">
		<h1>{l i='header_my_orders' gid='listings'} ({$orders_count_sum})</h1>

		<div class="search-links">
			<div class="edit_block">
				<form action="" method="post" enctype="multipart/form-data" id="orders_search_form">
					<div class="r">
						{capture assign='listings_select_callback'}{literal}
							function(data){
								ordersList.search({data: {listings: data}});
							}
						{/literal}{/capture}
						{block name='listing_select' module='listings' selected=$selected max=1 var_name='id_listing' id_user=$user_id operation_type='rent' callback=$listings_select_callback}
					</div>
				</form>
			</div>
		</div>
		
		<div class="tabs tab-size-15 noPrint">
			<ul id="orders_sections">
				<li id="m_wait" sgid="wait" class="{if $status eq 'wait'}active{/if}"><a href="{$site_url}listings/orders/wait">{l i='booking_status_wait' gid='listings'} (<span id="section_wait">{$orders_count.wait}</span>)</a></li>
				<li id="m_approved" sgid="approve" class="{if $status eq 'approve'}active{/if}"><a href="{$site_url}listings/orders/approve">{l i='booking_status_approved' gid='listings'} (<span id="section_approve">{$orders_count.approve}</span>)</a></li>
				<li id="m_declined" sgid="decline" class="{if $status eq 'decline'}active{/if}"><a href="{$site_url}listings/orders/decline">{l i='booking_status_declined' gid='listings'} (<span id="section_decline">{$orders_count.decline}</span>)</a></li>
			</ul>
		</div>

		<div id="orders_block">{$block}</div>
		
		{js module=listings file='listings-order.js'}
		{js module=listings file='listings-list.js'}
		<script>{literal}
			var ordersList;
			var orders;
			$(function(){
				ordersList = new listingsList({
					siteUrl: '{/literal}{$site_url}{literal}',
					listAjaxUrl: '{/literal}listings/ajax_orders{literal}',
					sectionId: 'orders_sections',
					listBlockId: 'orders_block',
					sFormId: 'orders_search_form',
					operationType: '{/literal}{$status}{literal}',
					order: '{/literal}{$order}{literal}',
					orderDirection: '{/literal}{$order_direction}{literal}',
					page: {/literal}{$page_data.cur_page}{literal},
					tIds: ['pages_block_1', 'pages_block_2', 'sorter_block'],
				});
				orders = new listingsOrder({
					siteUrl: '{/literal}{$site_url}{literal}',
					sectionId: 'orders_sections',
					waitCnt: {/literal}{$orders_count.wait}{literal},
					approveCnt: {/literal}{$orders_count.approve}{literal},
					declineCnt: {/literal}{$orders_count.decline}{literal},
				});
			});
		{/literal}</script>
	</div>
</div>
<div class="clr"></div>
{include file="footer.tpl"}
