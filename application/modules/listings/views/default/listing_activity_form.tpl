{include file="header.tpl"}

	<div class="content-block">
		<div class="edit_block">

			<h1>{if $data.id}{l i='header_listing_edit' gid='listings'}{else}{l i='header_add_listing' gid='listings'}{/if}</h1>

			{include file="my_listings_menu.tpl" module="listings"}

			{capture assign="service_block"}
				{if $page_data.use_services}
					{if $page_data.user_services}
					<select name="id_user_service">
					{foreach item=item from=$page_data.user_services}
					{assign var='service_name' value=$item.service_name}
					<option value="{$item.id}">{l i="service_string_name_$service_name" gid='listings'} ({l i='listing_service_post_left' gid='listings'}: {$item.post_count}/{$item.post_period} {l i='listing_service_days' gid='listings'})</option>
					{/foreach}
					</select><br><br>
					<input type="submit" name="btn_activate" value="{l i='btn_activate' gid='listings' type='button'}">
					{else}
					<a href="{$site_url}users_services/index/post" target="blank">{l i='listing_service_link_buy' gid='listings'}</a>
					{/if}
				{else}
				<p>{l i='listing_service_active_period' gid='listings'}: <b>{$page_data.default_activation_period} {l i='listing_service_days' gid='listings'}</b>. </p>
				<input type="submit" name="btn_activate" value="{l i='btn_activate' gid='listings' type='button'}">
				{/if}
			{/capture}
			{js module=listings file='listings-edit-steps.js'}
			<script>{literal}
			$(function(){
				new listingsEditSteps();
			});
			{/literal}</script>

			<form method="post" action="{$data.action|escape}" name="save_form" id="save_form" enctype="multipart/form-data">
			
			<div class="rollup-box">
				<div class="title">
					<div class="btn-link"><ins class="with-icon i-collapse"></ins></div>
					<h2>{l i='header_activation_listing' gid='listings'}</h2>
				</div>
				<div class="cont">

				{if $data.status eq '1'}
					<p>
						{l i='listing_active_up_to' gid='listings'}: <u>{$data.date_expire|date_format:$page_data.date_format}</u>. <br>
						{l i='deactivation_text_1' gid='listings'}
					</p>
					<input type="submit" name="btn_deactivate" value="{l i='btn_deactivate' gid='listings' type='button'}">
					{if $data.operation_type eq 'sale'}
						{if $data.sold}
						<input type="submit" name="btn_delete_sold" value="{l i='btn_delete_sold' gid='listings' type='button'}">
						{else}
						<input type="submit" name="btn_make_sold" value="{l i='btn_make_sold' gid='listings' type='button'}">
						{/if}
					{/if}
					<br>
					{if $page_data.use_services}
					<p>{l i='activation_text_0' gid='listings'}</p>
					{$service_block}
					{/if}
				{elseif $data.status eq '0' && $page_data.can_reactivate}
					<p>{l i='activation_text_1' gid='listings'}</p>
					<input type="submit" name="btn_reactivate" value="{l i='btn_reactivate' gid='listings' type='button'}"><br>
				{elseif $data.status eq '0' && $data.initial_moderation eq '1'}
					<p>{l i='activation_text_1' gid='listings'}</p>
					{$service_block}
				{elseif $data.status eq '0' && $data.initial_moderation eq '0' && $data.initial_activity eq '0'}
					<p>{l i='activation_text_2' gid='listings'}</p>
					{$service_block}
				{else}
					<p>{l i='activation_text_3' gid='listings'}</p>
				{/if}
				</div>
			</div>
			<div class="b outside">
				<input type="button" name="btn_previous" value="{l i='nav_prev' gid='start' type='button'}" class="btn hide" id="edit_listings_prev" />
				<input type="button" name="btn_next" value="{l i='nav_next' gid='start' type='button'}" class="btn hide" id="edit_listings_next" />
				<a href="{$site_url}listings/my" class="btn-link"><ins class="with-icon i-larr"></ins>{l i='link_back_to_my_listings' gid='listings'}</a>
			</div>
			</form>
			<div class="clr"></div>
		</div>
	</div>

<div class="clr"></div>
{include file="footer.tpl"}
