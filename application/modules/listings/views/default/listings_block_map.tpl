{block name=update_default_map module=geomap markers=$markers map_id='listings_map_full_container'}

{if $update_operation_type}
<script>{literal}
	$(function(){
		var operation_type = $('#operation_type');
		if(operation_type.val() != '{/literal}{$update_operation_type}{literal}')
			$('#operation_type').val('{/literal}{$update_operation_type}{literal}').trigger('change');
	});
{/literal}</script>
{/if}

{capture assign='sorter_block'}{strip}
	{sorter links=$sort_data.links order=$sort_data.order direction=$sort_data.direction url=$sort_data.url}
	{if $page_data.total_rows}<div class="fright" id="pages_block_1">{pagination data=$page_data type='cute'}</div>{/if}
{/strip}{/capture}
{capture assign='paginator_bottom_block'}{if $page_data.total_rows}{pagination data=$page_data type='full'}{/if}{/capture}

<script>{literal}
	$(function(){
		{/literal}{if $update_search_block}$('#search_filter_block').html('{strip}{$search_filters_block}{/strip}');{/if}{literal}
		$('#total_rows').html('{/literal}{$page_data.total_rows}{literal}');
		$('#sorter_block').html(decodeURIComponent('{/literal}{$sorter_block|rawurlencode}{literal}'));
		$('#pages_block_2').html('{/literal}{strip}{$paginator_bottom_block|addslashes}{/strip}{literal}');		
	});
{/literal}</script>
