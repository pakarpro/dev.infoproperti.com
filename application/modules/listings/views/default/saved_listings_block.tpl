	{if $listings}
	<div class="sorter line" id="sorter_block">
		{sorter links=$sort_data.links order=$sort_data.order direction=$sort_data.direction url=$sort_data.url}
		<div class="fright" id="pages_block_1">{pagination data=$page_data type='cute'}</div>
	</div>
	{/if}

	<div class="listing-block">
	{foreach item=item from=$listings}
		<div id="item-block-{$item.id}" class="item listing">
			<h3><a href="{seolink module='listings' method='view' data=$item}">{$item.output_name|truncate:100}</a></h3>
			<div class="image">
				<a href="{seolink module='listings' method='view' data=$item}">
					<img src="{$item.media.photo.thumbs.middle}" title="{$item.output_name|escape}">
					{if $item.photo_count || $item.is_vtour}
					<div class="photo-info">
						<div class="panel">
							{if $item.photo_count}<span class="btn-link"><ins class="with-icon-small w i-photo"></ins> {$item.photo_count}</span>{/if}
							{if $item.is_vtour}<span class="btn-link"><ins class="with-icon-small w i-vtour"></ins> 360&deg;</span>{/if}
						</div>
						<div class="background"></div>
					</div>
					{/if}
				</a>
			</div>
			{capture assign='status_info'}{strip}
				{if $item.is_lift_up || 
					$location_filter eq 'country' && $item.is_lift_up_country || 
					$location_filter eq 'region' && ($item.is_lift_up_country || $item.is_lift_up_region) || 
					$location_filter eq 'city' && ($item.is_lift_up_country || $item.is_lift_up_region || $item.is_lift_up_city) ||
					$sort_data.order ne 'default' && ($item.is_lift_up ||$item.is_lift_up_country || $item.is_lift_up_region || $item.is_lift_up_city)}
					{l i='status_lift_up' gid='listings'}
				{elseif $item.is_featured}
					{l i='status_featured' gid='listings'}
				{/if}
			{/strip}{/capture}
			<div class="body">
				{l i='no_information' gid='start' assign='no_info_str'}
				<h3>{block name=listing_price_block module='listings' data=$item}</h3>
				<div class="t-1">
					{$item.property_type_str} {$item.operation_type_str}
					<br>{$item.square_output|truncate:30}
					{if $item.date_open|strtotime >= $tstamp}<br>{l i='text_date_open_status' gid='listings'}{/if}
					{if $item.sold}<br><span class="status_text">{l i='text_sold' gid='listings'}</span>{/if}
					{if $status_info}<br><span class="status_text">{$status_info}</span>{/if}
				</div>
				<div class="t-2">
				</div>
				<div class="t-3">
					{$item.user.output_name|truncate:50}
				</div>
				<div class="t-4">
					{capture assign='user_logo'}
						{if $item.user.is_show_logo}
						<img src="{$item.user.media.user_logo.thumbs.small}" title="{$item.user.output_name}">
						{else}
						<img src="{$item.user.media.default_logo.thumbs.small}" title="{$item.user.output_name}">
						{/if}
					{/capture}
					{if $item.user.status}
					<a href="{seolink module='users' method='view' data=$item.user}">{$user_logo}</a>
					{else}
					{$user_logo}
					{/if}
				</div>
			</div>
			<div class="clr"></div>
			{if $item.headline}<p class="headline" title="{$item.headline}">{$item.headline|truncate:100}</p>{/if}
			<div class="actions">
				<a href="{$site_url}listings/delete_saved/{$item.id}" onclick="javascript: if(!confirm('{l i='note_delete_listing' gid='listings' type='js'}')) return false;">{l i='link_delete_saved' gid='listings'}</a> |
				<a href="{seolink module='listings' method='view' data=$item}">{l i='link_details' gid='listings'}</a>
			</div>
		</div>
	{foreachelse}
		<div class="item empty">{l i='no_listings' gid='listings'}</div>
	{/foreach}
	</div>
	{if $listings}<div id="pages_block_2">{pagination data=$page_data type='full'}</div>{/if}
	{js file='change_link_action.js'}
	<script>{literal}
		var rMenu;
		$(function(){
			link_action = new change_link_action({
				siteUrl: '{/literal}{$site_url}{literal}',
				viewUrl: '{/literal}{$site_url}{literal}listings/delete_saved/{/literal}{$listing_id}{literal}',
				viewAjaxUrl: '{/literal}{$site_url}{literal}listings/ajax_delete_saved/{/literal}{$listing_id}{literal}',
				listBlockParam: '.del_saved',
				successCallBack: function(obj){
					$.get('{/literal}{$site_url}{literal}listings/ajax_saved', {}, function(data){
						$('#listings_block').html(data);
					});
				}
			});
		});
	{/literal}</script>
