	{if $orders}
	<div class="sorter line" id="sorter_block">
		{sorter links=$sort_data.links order=$sort_data.order direction=$sort_data.direction url=$sort_data.url}
		<div class="fright" id="pages_block_1">{pagination data=$page_data type='cute'}</div>
	</div>
	{/if}
	
	<div>	
		{foreach item=item from=$orders}
		<div class="listing-block">
			<div id="item-block-{$item.id}" class="item listing">
				<div class="image">
					<a href="{seolink module='listings' method='view' data=$item.listing}">
						<img src="{$item.listing.media.photo.thumbs.small}" title="{$item.output_name|escape}">
					</a>
				</div>
				<div class="body">
					<h3><a href="{seolink module='listings' method='view' data=$item.listing}" title="{$item.listing.output_name|escape}">{$item.listing.output_name|truncate:50}</a></h3>
					<div class="t-1">
						{switch from=$item.listing.price_period}
							{case value='1'}
								{$item.date_start|date_format:$page_data.date_format} - {$item.date_end|date_format:$page_data.date_format}
							{case value='2'}
								{ld_option i='month-names' gid='start' option=$item.date_start|date_format:'%m'}
								{$item.date_start|date_format:'%Y'}
								&mdash; 
								{ld_option i='month-names' gid='start' option=$item.date_end|date_format:'%m'}
								{$item.date_end|date_format:'%Y'}
						{/switch}<br>
						{$item.guests} {l i='text_booking_guests' gid='listings'}<br>
						{block name='currency_format_output' module='start' value=$item.price cur_gid=$item.gid_currency}<br>
						{if $item.comment}<span title="{$item.comment|escape}">{$item.comment|truncate:70}</span><br>{/if}
						{if $item.answer}<span class="order_answer" title="{$item.answer|escape}">{l i='text_order_answer' gid='listings'} {$item.answer|truncate:70}</span><br>{/if}
					</div>
					<div class="t-2">
						<span>{$item.date_created}</span><br>
						{$item.user.output_name|truncate:50}<br>
						<a href="{seolink module='users' method='view' data=$item.user}" title="{$item.user.output_name|escape}">
							<img src="{$item.user.media.user_logo.thumbs.small}" title="{$item.user.output_name|escape}">
						</a>
					</div>
				</div>
				<div class="clr"></div>
				{if $item.status eq 'wait'}
				<a href="{$site_url}listings/order_approve/{$item.id}" data-id="{$item.id}" class="btn-link order_approve" title="{l i='btn_approve' gid='start' type='button'}"><ins class="with-icon i-approve"></ins></a><span class="btn-text link-r-margin">{l i='btn_approve' gid='start'}</span>
				<a href="{$site_url}listings/order_decline/{$item.id}" data-id="{$item.id}" class="btn-link order_decline" title="{l i='btn_approve' gid='start' type='button'}"><ins class="with-icon i-decline"></ins></a><span class="btn-text link-r-margin">{l i='btn_decline' gid='start'}</span>
				{/if}
				{block name='button_contact' module='mailbox' user_id=$item.user.id user_type=$item.user.user_type template='button'}
				<a href="{$site_url}listings/edit/{$item.listing.id}/calendar/2" class="btn-link" title="{l i='link_booking_listing' gid='listings' type='button'}"><ins class="with-icon i-calendar"></ins></a><span class="btn-text link-r-margin">{l i='btn_calendar' gid='listings'}</span>
				<div class="clr"></div>
			</div>
		</div>
		{foreachelse}
		<div class="item empty">{l i='no_orders' gid='listings'}</div>
		{/foreach}
		
	</div>
	
	{if $orders}<div id="pages_block_2">{pagination data=$page_data type='full'}</div>{/if}
