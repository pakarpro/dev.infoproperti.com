{include file="header.tpl" load_type='ui'}
{block name=show_social_networks_like module=social_networking func_param=true}


<script type="text/javascript">
{literal}
	function format(n, currency)
	{
		return currency + " " + n.toFixed(0).replace(/./g, function(c, i, a) {
			return i > 0 && c !== "," && (a.length - i) % 3 === 0 ? "." + c : c;
		});
	}
{/literal}
</script>


<div id="compare-container" class="clearfix">
<div id="comp-title">
	<span class="td-grey comp-td">Tipe Properti</span>
	<span class="comp-td">Harga</span>
	<span class="td-grey comp-td">Harga/m2</span>	
	<span class="comp-td">Komplek</span>
	<span class="td-grey comp-td">Luas Bangunan</span>
	<span class="comp-td">Luas Tanah</span>
	<span class="td-grey comp-td">Daya Listrik</span>	
	<span class="comp-td">Kamar Tidur</span>
	<span class="td-grey comp-td">Kamar Mandi</span>
	<span class="comp-td">Garasi</span>
	<span class="td-grey comp-td">Interior</span>
	<span class="comp-td">Pengembang</span>
</div>


{foreach item=listing from=$listings}
	<div class="inner-compare-box">
		<div class="a-link clearfix">
			<a href="{$site_url}listings/save/{$listing.id}" class="w-icon-fav">Save to favorite</a>
		</div>
	
    <img class="comp-thumb" src="{$base_url}uploads/listing-search-photo/{$listing.id}/middle-{$listing.logo_image}" title="{$listing.address}">
    
	<div class="comp-detail"> 
		<!-- Tipe Properti :-->
		<span class="td-grey comp-td">
		{if $listing.id_category == 1 && $listing.property_type == 1}
			Apartemen
			{elseif $listing.id_category == 1 && $listing.property_type == 4 }
			Rumah
			{elseif $listing.id_category == 1 && $listing.property_type == 10}
			Tanah
			{elseif $listing.id_category == 1 && $listing.property_type == 2}
			Kamar
			{elseif $listing.id_category == 1 && $listing.property_type == 9}
			Lain-lain
		{/if}
		</span>
		
		<!-- Harga :-->
		<span class="comp-td">{literal} <script> document.write(format({$listing.price}, "Rp. ") +",-"); </script> {literal}</span>
		
		<!-- Harga/m2 :-->
		<span class="td-grey comp-td">{literal} <script> document.write(format({$listing.price}, "Rp. ") +",-"); </script> {literal}</span>
		
		<!-- Lokasi :-->
		<span class="comp-td comp-loc">
			{if $listing.address == ''}
				-
			{else}
				{$listing.address}
			{/if}
		</span>
		
		{if $listing.id_type == 1} 
            <!-- MOD : Foreach from residential with type sale -->
			{foreach item=residen from=$residential}
				{if $listing.id == $residen.id_listing}
					<!--Luas Bangunan :-->
					<span class="td-grey comp-td">
						{if $residen.fe_field71 == ''}
							-
						{else}
							{$residen.fe_field71} sq.m
						{/if}
					</span>
					
					<!--Luas Tanah :-->
					<span class="comp-td">
						{if $residen.fe_field74 == ''}
							-
						{else}
							{$residen.fe_field74} sq.m
						{/if}
					</span>
					
					<!--Daya Listrik :-->
					<span class="td-grey comp-td">
						{if $residen.fe_field73 eq 0}
							-
						{elseif $residen.fe_field73 eq 1}
							1300 Watt
						{elseif $residen.fe_field73 eq 2}
							2200 Watt
						{elseif $residen.fe_field73 eq 3}
							3500 Watt
						{elseif $residen.fe_field73 eq 4}
							4400 Watt
						{elseif $residen.fe_field73 eq 5}
							5500 Watt
						{elseif $residen.fe_field73 eq 6}
							6600 Watt
						{else}
							> 6600 Watt <!-- original is Lainnya -->
						{/if}
					</span>
					
					<!--bedroom-->
					<div class="clearfix">
                        {if $residen.fe_bd_rooms_1 == 0}
                            <span class="comp-td float-left">-</span>
                        {else}
                            <span class="comp-td float-left">{$residen.fe_bd_rooms_1}</span>
                            <img class="comp-icon float-left" src="{$base_url}application/views/default/img/inner-icons/icon-bedroom.png" />
                        {/if}
					</div>
					
					<!--bathroom :-->
					<div class="clearfix td-grey">
                        {if $residen.fe_bth_rooms_1 == 0}
                            <span class="td-grey comp-td float-left">-</span>
                        {else}
                            <span class="td-grey comp-td float-left">{$residen.fe_bth_rooms_1}</span>
                            <img class="comp-icon float-left" src="{$base_url}application/views/default/img/inner-icons/icon-bathroom.png" />
                        {/if}
					</div>
					
					<!--garage :-->
					<div class="clearfix">
                        {if $residen.fe_garages_1 == 0}
                            <span class="comp-td float-left">-</span>
                        {else}
                            <span class="comp-td float-left">{$residen.fe_garages_1}</span>
                            <img class="comp-icon float-left" src="{$base_url}application/views/default/img/inner-icons/icon-garage.png" />
                        {/if}
					</div>
					
					<!--Interior :-->
					<span class="td-grey comp-td">
                        {if $residen.fe_furniture_1 eq 1}
                            Furnished
                        {elseif $residen.fe_furniture_1 eq 2}
                            Semi-Furnished 
                        {elseif $residen.fe_furniture_1 eq 3}
                            Unfurnished 
                        {else}
                            -
                        {/if}
					</span>
				{/if}
			{/foreach}
            <!-- End MOD : Foreach from residential with type sale -->
			
            <!-- MOD : Foreach from commercial with type sale -->
			{foreach item=commer from=$commercial}
			   {if $listing.id == $commer.id_listing}
					<!--Luas Bangunan :-->
					<span class="td-grey comp-td">
						{if $commer.fe_field71 == ''}
							-
						{else}
							{$commer.fe_field71} sq.m
						{/if}
					</span>
					
					<!--Luas Tanah :-->
					<span class="comp-td">
						{if $commer.fe_field74 == ''}
							-
						{else}
							{$commer.fe_field74} sq.m
						{/if}
					</span>
					
					<!--Daya Listrik :-->
					<span class="td-grey comp-td">
						{if $commer.fe_field73 eq 0}
							-
						{elseif $commer.fe_field73 eq 1}
							1300 Watt
						{elseif $commer.fe_field73 eq 2}
							2200 Watt
						{elseif $commer.fe_field73 eq 3}
							3500 Watt
						{elseif $commer.fe_field73 eq 4}
							4400 Watt
						{elseif $commer.fe_field73 eq 5}
							5500 Watt
						{elseif $commer.fe_field73 eq 6}
							6600 Watt
						{else}
							> 6600 Watt <!-- original is Lainnya -->
						{/if}
					</span>
					
					<!--bedroom-->
					<div class="clearfix">
                        {if $commer.fe_bd_rooms_1 == 0}
                            <span class="comp-td float-left">-</span>
                        {else}
                            <span class="comp-td float-left">{$commer.fe_bd_rooms_1}</span>
                            <img class="comp-icon float-left" src="{$base_url}application/views/default/img/inner-icons/icon-bedroom.png" />
                        {/if}
					</div>
					
					<!--bathroom :-->
					<div class="clearfix td-grey">
                        {if $commer.fe_bth_rooms_1 == 0}
                            <span class="td-grey comp-td float-left">-</span>
                        {else}
                            <span class="td-grey comp-td float-left">{$commer.fe_bth_rooms_1}</span>
                            <img class="comp-icon float-left" src="{$base_url}application/views/default/img/inner-icons/icon-bathroom.png" />
                        {/if}
					</div>
					
					<!--garage :-->
					<div class="clearfix">
                        {if $commer.fe_garages_1 == 0}
                            <span class="comp-td float-left">-</span>
                        {else}
                            <span class="comp-td float-left">{$commer.fe_garages_1}</span>
                            <img class="comp-icon float-left" src="{$base_url}application/views/default/img/inner-icons/icon-garage.png" />
                        {/if}
					</div>
                    
					<!--Interior :-->
					<span class="td-grey comp-td">
                        {if $commer.fe_furniture_1 eq 1}
                            Furnished
                        {elseif $commer.fe_furniture_1 eq 2}
                            Semi-Furnished 
                        {elseif $commer.fe_furniture_1 eq 3}
                            Unfurnished 
                        {else}
                            -
                        {/if}
					</span>
                    
				<!-- MOD : Originally coded by Teguh, temporarily disabled
				    <div class="clearfix">
						<span class="comp-td float-left">{$commer.fe_bd_rooms_1}</span><img class="comp-icon float-left" src="{$base_url}application/views/default/img/inner-icons/icon-bedroom.png" />
					</div>
					<div class="clearfix td-grey">
						<span class="td-grey comp-td float-left">{$commer.fe_bth_rooms_1}</span><img class="comp-icon float-left" src="{$base_url}application/views/default/img/inner-icons/icon-bathroom.png" />
					</div>
					<div class="clearfix">
						<span class="comp-td float-left">{$commer.fe_garages_1}</span><img class="comp-icon float-left" src="{$base_url}application/views/default/img/inner-icons/icon-garage.png" />
					</div>

					<!--Luas Bangunan :
					<span class="td-grey comp-td">{$commer.fe_field71} sq.m </span>
					
					<!--Luas Tanah :
					<span class="comp-td">{$commer.fe_field74} sq.m </span>
					
					<!--Daya Listrik :
					<span class="td-grey comp-td">{$commer.fe_field73} </span>
					
					<!--Interior :
					<span class="comp-td">
						{if $commer.fe_furniture_1 == 1}
							Furnished
						{elseif $commer.fe_furniture_1 == 2}
							Semi-Furnished
						{elseif $commer.fe_furniture_1 == 3}
							Unfurnished
						 {else}
							-
						{/if}
					</span>
                -->
				{/if}
			{/foreach}
            <!-- End MOD : Foreach from commercial with type sale -->

		{elseif $listing.id_type == 3}
		
			{foreach item=residen from=$residential}
				{if $listing.id == $residen.id_listing}
				   
					<div class="clearfix td-grey">
						<span class="td-grey comp-td float-left">{$residen.fe_bd_rooms_4}</span><img class="comp-icon float-left" src="{$base_url}application/views/default/img/inner-icons/icon-bedroom.png" />
					</div>
					<div class="clearfix">
						<span class="comp-td float-left">{$residen.fe_bth_rooms_4}</span><img class="comp-icon float-left" src="{$base_url}application/views/default/img/inner-icons/icon-bathroom.png" />
					</div>
					<div class="clearfix td-grey">
						<span class="td-grey comp-td float-left">{$residen.fe_garages_4}</span><img class="comp-icon float-left" src="{$base_url}application/views/default/img/inner-icons/icon-garage.png" />
					</div>
					
					<!--Luas Bangunan :-->
					<span class="comp-td">{$listing.square} sq.m</span>
					
					<!--Interior :--> 
					<span class="td-grey comp-td">
					{if $residen.fe_furniture_4 == 1}
						Furnished
					{elseif $residen.fe_furniture_4 == 2}
						Semi-Furnished
					{elseif $residen.fe_furniture_4 == 3}
						Unfurnished
					{else}
						-
					{/if}
					</span>
				{/if}
			{/foreach}
			
			{foreach item=commer from=$commercial}
			   {if $listing.id == $commer.id_listing}
				
				<div class="clearfix">
					<span class="comp-td float-left">{$commer.fe_bd_rooms_4}</span><img class="comp-icon float-left" src="{$base_url}application/views/default/img/inner-icons/icon-bedroom.png" />
				</div>
				
				<div class="clearfix td-grey">
					<span class="td-grey comp-td float-left">{$commer.fe_bth_rooms_4}</span><img class="comp-icon float-left" src="{$base_url}application/views/default/img/inner-icons/icon-bathroom.png" />
				</div>
				
				<div class="clearfix">
					<span class="comp-td float-left">{$commer.fe_garages_4}</span><img class="comp-icon float-left" src="{$base_url}application/views/default/img/inner-icons/icon-garage.png" />
				</div>
				
				<!--Luas Bangunan :-->
				<span class="td-grey comp-td">{$listing.square} sq.m</span>
				
				<!--Interior :-->
				<span class="comp-td">
					{if $commer.fe_furniture_4 == 1}
						Furnished
						{elseif $commer.fe_furniture_4 == 2}
						Semi-Furnished
						{elseif $commer.fe_furniture_4 == 3}
						Unfurnished
						{else}
						-
					{/if}
				</span>
				{/if}
			{/foreach}
		{/if}    
		
		<!--Pengembang :-->
		{foreach item=developer from=$develop}
			{if $developer.id == $listing.id_user}
				<span class="comp-td">
					{if $developer.company_name != ""}
						{$developer.company_name}
					 {else}
						-
					{/if}
				</span>
			{/if}
		 {/foreach}

		<div class="a-link clearfix">
			<a class="" href="{$site_url}listing-Propertidijual-in-Indonesia-id-{$listing.id}-overview-operation-sale-no">View Detail</a> <br /> <br /> <br /> <br />
		</a>
	</div><!-- END .comp-detail -->
	
	<!-- Others Listing -->
	<div class="comp-detail">
		<h1>Listing serupa</h1>

		{foreach item=simlisting from=$similar}
			{if $simlisting.id_user == $listing.id_user}
				<img src="{$base_url}uploads/listing-search-photo/{$simlisting.id}/middle-{$simlisting.logo_image}" title="{$simlisting.address}"> <br /> <br />
					{if $simlisting.id_category == 1 && $simlisting.property_type == 1}
							Apartment Dijual<br />
						{elseif $simlisting.id_category == 1 && $simlisting.property_type == 4 }
							Rumah Dijual<br />
						{elseif $simlisting.id_category == 1 && $simlisting.property_type == 10}
							Tanah Dijual<br />
						{elseif $simlisting.id_category == 1 && $simlisting.property_type == 2}
							Kamar Dijual<br />
						{elseif $simlisting.id_category == 1 && $simlisting.property_type == 9}
							Lain-lain<br />
						{/if} 
					Harga : {literal} <script> document.write(format({$simlisting.price}, "Rp. ") +",-"); </script> {literal}<br />
					Lokasi : {$simlisting.address}<br />
					
					{if $simlisting.id_type == 1} 
					
						{foreach item=residen from=$residential}
									
							{if $simlisting.id == $residen.id_simlisting}
							   
								{$residen.fe_bd_rooms_1} KT <br />
								{$residen.fe_bth_rooms_1}KM <br />
								LB {$residen.fe_field71} sq.m <br />
								LT {$residen.fe_field74} sq.m <br />
										  
							{/if}
							
						{/foreach}
						
						{foreach item=commer from=$commercial}
									
						   {if $simlisting.id == $commer.id_simlisting}
							   <ul class="feat-icon">
									<li class="simlisting-icon bedroom"> : {$commer.fe_bd_rooms_1}</li>
									<li class="simlisting-icon bathroom"> : {$commer.fe_bth_rooms_1}</li>
									<li class="simlisting-icon garages"> : {$commer.fe_garages_1}</li>
								</ul>
									LB {$commer.fe_field71} sq.m <br />
									LT {$commer.fe_field74} sq.m <br />
							  
							{/if}
							
						{/foreach}
						
					{elseif $simlisting.id_type == 3}
						{foreach item=residen from=$residential}
							{if $simlisting.id == $residen.id_simlisting}
								<ul class="feat-icon">									
									<li class="simlisting-icon bedroom"> : {$residen.fe_bd_rooms_4}</li>
									<li class="simlisting-icon bathroom"> : {$residen.fe_bth_rooms_4}</li>
									<li class="simlisting-icon garages"> : {$residen.fe_garages_4}</li>
								</ul>
									LB {$simlisting.square} sq.m <br /><br/>
							{/if}
						{/foreach}
						{foreach item=commer from=$commercial}
						   {if $simlisting.id == $commer.id_simlisting}
								<ul class="feat-icon">
									<li class="simlisting-icon bedroom"> : {$commer.fe_bd_rooms_4}</li>
									<li class="simlisting-icon bathroom"> : {$commer.fe_bth_rooms_4}</li>
									<li class="simlisting-icon garages"> : {$commer.fe_garages_4}</li>
								</ul>
									LB {$simlisting.square} sq.m <br /><br/>
							{/if}
						{/foreach}
					{/if}    
					<a href="{$site_url}simlisting-Propertidijual-in-Indonesia-id-{$simlisting.id}-overview-operation-sale-no">View Detail</a> <br /> <br /> <br /> <br />
				 {/if}         
			{/foreach}
		</div>
	</div>
</div><!-- end inner compare box -->
    
{/foreach}
</div><!-- end #compare-container -->
	
{include file="footer.tpl"}