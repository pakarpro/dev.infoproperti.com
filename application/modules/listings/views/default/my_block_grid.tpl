	<div>

		<table class="list">
		<tr id="sorter_block">
			<th><a href="{$sort_links.name}" class="link-sorter">{l i='field_name' gid='listings'}{if $page_data.order eq 'name'}<ins class="fright i-sorter with-icon-small {$page_data.order_direction|lower}"></ins>{/if}</a></th>		
			<th><a href="{$sort_links.date_created}" class="link-sorter">{l i='field_date_created' gid='listings'}{if $page_data.order eq 'date_created'}<ins class="fright i-sorter with-icon-small {$page_data.order_direction|lower}"></ins>{/if}</a></th>		
			<th><a href="{$sort_links.date_modified}" class="link-sorter">{l i='field_date_modified' gid='listings'}{if $page_data.order eq 'date_modified'}<ins class="fright i-sorter with-icon-small {$page_data.order_direction|lower}"></ins>{/if}</a></th>		
			<th class="w100">{l i='field_search_status' gid='listings'}</th>		
			<th class="w100">{l i='field_approval_status' gid='listings'}</th>		
			<th class="w125">&nbsp;</th>		
		</tr>
		{foreach item=item from=$listings}
		<tr>
			<td>{$item.name}</td>
			<td>{$item.date_created|strtotime|date_format:$page_data.date_format}</td>
			<td>{$item.date_modified|strtotime|date_format:$page_data.date_format}</td>
			<td>
				<span class="status lift-up"{if $item.is_lift_up} title="{l i='listing_lift_up_till' gid='listings' type='button'}: {$item.lift_up_date_end|strtotime|date_format:$page_data.date_format|escape}"{/if}>
				{if $item.status}{if $item.is_lift_up}<ins></ins>{/if}{l i='active_listing' gid='listings'}
				{else}{l i='inactive_listing' gid='listings'}
				{/if}
				</span>
			</td>
			<td>
				{if $item.moderation_status eq 'default' && $item.initial_moderation}<span class="status"><ins></ins>{l i='listing_status_default' gid='listings'}</span>
				{elseif $item.moderation_status eq 'decline' || $item.moderation_status eq 'default' && !$item.initial_moderation}<span class="status decline"><ins></ins>{l i='listing_status_decline' gid='listings'}</span>
				{elseif $item.moderation_status eq 'approved'}<span class="status approved"><ins></ins>{l i='listing_status_approved' gid='listings'}</span>
				{elseif $item.moderation_status eq 'wait'}<span class="status wait"><ins></ins>{l i='listing_status_wait' gid='listings'}</span>
				{/if}
			</td>
			<td>
				<a href="{$site_url}listings/delete/{$item.id}" onclick="javascript: if(!confirm('{l i='note_delete_listing' gid='listings' type='js'}')) return false;" class="btn-link fright"><ins class="with-icon i-delete"></ins></a>
				<a href="{$site_url}listings/edit/{$item.id}" class="btn-link fright"><ins class="with-icon i-edit"></ins></a>
				{if $listings.status && $lift_up_services_active}
				<a href="{$site_url}listings/apply_service/{$item.id}/listing_lift_up_services" class="btn-link fright"><ins class="with-icon i-buy"></ins></a>
				{/if}
			</td>
		</tr>
		{/foreach}
		</table>	

	</div>
	<div id="pages_block_2">{include file="pagination.tpl"}</div>

	<script>{literal}
	$(function(){
		$('#total_rows').html('{/literal}{$page_data.total_rows}{literal}');
	});
	{/literal}</script>
