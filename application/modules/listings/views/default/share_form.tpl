<div class="content-block load_content">
	{capture assign='facebook_share'}{strip}
		{block name=show_social_networks_share module=social_networking}
	{/strip}{/capture}
	<h1>{l i='header_share' gid='listings'}</h1>
	<div class="inside edit_block">
		<div class="tabs tab-size-15 noPrint">
			<ul id="share_sections">
				<li id="m_send_email" sgid="send_email" class="active"><a href="#">{l i='link_send_mail_friend' gid='listings'}</a></li>
				{if $facebook_share}<li id="m_send_facebook" sgid="send_facebook"><a href="#">{l i='link_share_facebook' gid='listings'}</a></li>{/if}
				<li id="m_send_twitter" sgid="send_twitter"><a href="#">{l i='link_share_twitter' gid='listings'}</a></li>
			</ul>
		</div>
		<div class="clr"></div>
		<div id="share_block" class="view edit-form">
			<div id="content_m_send_email" class="view-section">
				
				<form action="" method="post" id="share_form">
					<div class="popup">
					<div class="r">
						<div class="h">{l i='send_mail_email' gid='listings'}:&nbsp;*</div>
						<div class="v"><input type="text" id="email" name="email"></div>
					</div>
					<div class="r">
						<div class="h">{l i='send_mail_user' gid='listings'}:&nbsp;*</div>
						<div class="v"><input type="text" id="user" name="user"></div>
					</div>
					<div class="r">
						<div class="h">{l i='send_mail_text' gid='listings'}:&nbsp;*</div>
						<div class="v"><textarea id="message" name="message" rows="3"></textarea></div>
					</div>
					</div>
					<div class="r">
						<input type="submit" value="{l i='btn_send' gid='start' type='button'}">
					</div>
				</form>
	
			</div>
			{if $facebook_share}
			<div id="content_m_send_facebook" class="hide view-section">	
				<div class="popup">
				<div class="listing">
					<h3><a href="{seolink module='listings' method='view' data=$data}" title="{$data.output_name|escape}">{$data.output_name|truncate:50}</a></h3>
					<div class="image">
						<a href="{seolink module='listings' method='view' data=$data}">
							<img src="{$data.media.photo.thumbs.middle}" title="{$data.output_name|escape}">
							{if $data.photo_count || $data.is_vtour}
							<div class="photo-info">
								<div class="panel">
									{if $data.photo_count}<span class="btn-link"><ins class="with-icon-small w i-photo"></ins> {$data.photo_count}</span>{/if}
									{if $data.is_vtour}<span class="btn-link"><ins class="with-icon-small w i-vtour"></ins> 360&deg;</span>{/if}
								</div>
								<div class="background"></div>
							</div>
							{/if}
						</a>
					</div>
					
					{capture assign='status_info'}{strip}
					{if $data.is_lift_up || $data.is_lift_up_country || $data.is_lift_up_region || $data.is_lift_up_city}
						{l i='status_lift_up' gid='listings'}
					{elseif $data.is_featured}
						{l i='status_featured' gid='listings'}
					{/if}
					{/strip}{/capture}
			
					<div class="body">
						<h3>{block name=listing_price_block module='listings' data=$data}</h3>
						<div class="t-1">
							{$data.property_type_str} {$data.operation_type_str}
							<br>{$data.square_output|truncate:30}
							{if $data.date_open|strtotime >= $tstamp}<br>{l i='text_date_open_status' gid='listings'}{/if}
							{if $data.sold}<br><span class="status_text">{l i='text_sold' gid='listings'}</span>{/if}
							{if $status_info}<br><span class="status_text">{$status_info}</span>{/if}
						</div>
						<div class="t-2">
						</div>
						{if $data.user.status}
						<div class="t-3">
							{$data.user.output_name|truncate:50}
						</div>
						<div class="t-4">
							<a href="{seolink module='users' method='view' data=$data.user}"><img src="{$data.user.media.user_logo.thumbs.small}" title="{$data.user.output_name|truncate}"></a>
						</div>
						{/if}
					</div>
					<div class="clr"></div>
				</div>
				{if $data.headline}<p class="headline" title="{$data.headline}">{$data.headline|truncate:50}</p>{/if}
				</div>
				{capture assign='description'}{strip}
					{block name='currency_format_output' module='start' value=$data.price cur_gid=$data.gid_currency}
				{/strip}{/capture}
				{capture assign='facebook_ui'}{strip}{literal}
					FB.ui({
						method: 'feed',
						redirect_uri: '{/literal}{seolink module='listings' method='view' data=$data}{literal}',
						link: 'https://developers.facebook.com/docs/reference/dialogs/',
						picture: '{/literal}{$data.media.photo.thumbs.small}{literal}',
						name: '{/literal}{$data.output_name|escape:'quotes'}{literal}',
						caption: '{/literal}{$data.property_type_str|escape:'quotes'} {$data.operation_type_str|escape:'quotes'}{literal}',
						description: '{/literal}{$description|strip_tags|escape:'quotes'}{literal}'
					}, function(response){})
				{/literal}{/strip}{/capture}	
				<input type="button" onclick="javascript:{$facebook_ui};" value="{l i='btn_share' gid='listings' type='button'}">
			</div>
			{/if}
			<div id="content_m_send_twitter" class="hide view-section">
				<div class="popup">
				<div class="r">{seolink module='listings' method='view' data=$data}</div>
				</div>
				<div class="r"><input type="button" onclick="javascript:window.open('http://twitter.com/home?status={seolink module='listings' method='view' data=$data}');" value="{l i='btn_share' gid='listings' type='button'}" class="btn_small"></div>
			</div>
		</div>
	</div>
</div>
<script>{literal}
	var pMenu;
	$(function(){
		pMenu = new listingsMenu({
			siteUrl: '{/literal}{$site_url}{literal}',
			idListing: '{/literal}{$data.id}{literal}',
			listBlockId: 'share_block',
			sectionId: 'share_sections',
			currentSection: 'm_send_email',
		});
	});
{/literal}</script>	
