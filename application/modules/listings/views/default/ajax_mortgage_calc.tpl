<html>
	<head>
		<title>{l i='mortgage_calc_mes1' gid='listings'}</title>
		<style>{literal}
			*,html,body{background-color:#fff}
			table{border-collapse:collapse;}
			td{border:1px solid #000;}
		{/literal}</style>
	</head>
	<body>
		<center><font size="+3">{l i='mortgage_calc_mes1' gid='listings'}</font></center>
		<center> 
			<table border="0" cellpadding="4" cellspacing="0">
				<tr>
					<td colspan="5">
						<b>{l i='mortgage_calc_mes2' gid='listings'} {$loanDate}<br>
						{l i='mortgage_calc_mes3' gid='listings'}: {$selected_curr_abbr} _self.RightDig(form.principal.value)<br>
						# {l i='mortgage_calc_mes4' gid='listings'} aNPer<br>
						{l i='mortgage_calc_mes5' gid='listings'} _self.RightDig(aIntRate * 12 * 100)%<br>
						{l i='mortgage_calc_mes6' gid='listings'} {$selected_curr_abbr} _self.RightDig(form.HmoPmt.value)</b>
					</td>
				</tr>
				<tr>
					<td colspan="5">
						<center><font size="+2">{l i='mortgage_calc_mes7' gid='listings'}</font><br>
								<font size="-1">{l i='mortgage_calc_mes8' gid='listings'}</font></center></td> 
				</tr>
				<tr>
					<td><b>{l i='mortgage_calc_mes9' gid='listings'} #</b></td>
					<td><b>{l i='mortgage_calc_mes13' gid='listings'}</b></td>
					<td><b>{l i='mortgage_calc_mes3' gid='listings'}</b></td>
					<td><b>{l i='mortgage_calc_mes10' gid='listings'}</b></td>
					<td><b>{l i='mortgage_calc_mes11' gid='listings'}</b></td>
				</tr>
				aPmtRow +
				<tr>
					<td></td>
					<td></td>
					<td align="{$default_right}"><b>RightDig(aAccumPrin)</b></td>
					<td><b>RightDig(aAccumInt)</b></td>
					<td></td>
				</tr>
			</table>
		</center>
	</body>
</html>