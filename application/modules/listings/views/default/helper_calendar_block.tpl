<div id="calendar" class="cal-type-{$listing.price_period} noPrint">
	<div class="calendar-nav">
		<div class="fleft">
			{switch from=$listing.price_period}
				{case value='1'}
					<a href="#" class="btn-link" id="prev_month" title="{l i='link_month_prev' gid='listings' type='button'}"><ins class="with-icon i-larr"></ins></a>
				{case value='2'}
					<a href="#" class="btn-link" id="prev_year" title="{l i='link_year_prev' gid='listings' type='button'}"><ins class="with-icon i-larr"></ins></a> 
			{/switch}
		</div>
		<div class="fright">
			{switch from=$listing.price_period}
				{case value='1'}
					<a href="#" class="btn-link fright" id="next_month" title="{l i='link_month_next' gid='listings' type='button'}"><ins class="with-icon i-rarr"></ins></a>
				{case value='2'}
					<a href="#" class="btn-link fright" id="next_year" title="{l i='link_year_next' gid='listings' type='button'}"><ins class="with-icon i-rarr"></ins></a>
			{/switch}
		</div>
		<div class="clr"></div>
	</div>
	
	<div id="calendar_block">
		{include file="calendar_block.tpl" module="listings" theme="default"}
	</div>
	
	<div class="statusbar">
		{ld i='booking_status' gid='listings' assign='booking_status'}
		{foreach item=item2 key=key2 from=$booking_status.option}
		{if $key2 eq 'open' || $key2 eq 'book'}
		<span class="{$key2}"></span> {$item2}
		{/if}
		{/foreach}
	</div>
</div>
{js file='booking-form.js' module='listings'}
{js file='listings-calendar.js' module='listings'}
<script>{literal}
	var listings_calendar{/literal}{$rand}{literal};
	$(function(){
		listings_calendar{/literal}{$rand}{literal} = new listingsCalendar({
			siteUrl: '{/literal}{$site_url}{literal}',
			listingId: {/literal}{$listing.id}{literal},
			count: {/literal}{$count}{literal},
			month: {/literal}{$m}{literal},
			year: {/literal}{$y}{literal},
		});
	});
{/literal}</script>

{capture assign='calendar_periods'}
<div class="calendar_periods edit_block">
	<div class="rollup-box">
	<h2>{l i='header_calendar_periods' gid='listings'}</h2>
	<table class="list" id="listing_periods">
		<tr>
			<th class="w200">{l i='field_booking_period' gid='listings'}</th>
			<th class="w30">{l i='field_booking_guests' gid='listings'}</th>
			<th class="w70">{l i='field_booking_price' gid='listings'}, {$current_price_currency.abbr} {ld_option i='price_period' gid='listings' option=$data.price_period}</th>
			<th class="w100">{l i='field_booking_comment' gid='listings'}</th>
			{if $edit}<th class="w100">&nbsp;</th>{/if}
		</tr>
		{assign var='is_opened' value=0}
		{foreach item=item from=$data.booking.periods}
		{if $item.id_user eq $item.id_owner && $item.status eq 'open'}
		{assign var='is_opened' value=1}
		{assign var='period' value=$item}
		{include file="calendar_period.tpl" module="listings" themes="default"}
		{/if}
		{/foreach}
		{if !$is_opened}<tr><td colspan="{if $edit}5{else}4{/if}" class="center gray_italic">{l i='no_periods' gid='listings'}</td></tr>{/if}
	</table>
	</div>
	
	{if $edit}	
	<div class="b outside noPrint">
		<input type="button" value="{l i='btn_add' gid='start' type='button'}" name="btn_period" id="btn_period">
	</div>
	<script>{literal}
		$(function(){
			new bookingForm({
				siteUrl: '{/literal}{$site_url}{literal}',
				listingId: '{/literal}{$data.id}{literal}',
				bookingBtn: 'btn_period',
				cFormId: 'period_form',
				isSavePeriod: false,
				urlGetForm: 'listings/ajax_period_form/',
				urlSaveForm: 'listings/ajax_save_period/',
				calendar: listings_calendar{/literal}{$rand}{literal},
				successCallback: function(id, data, calendar){
					var periods = $('#listing_periods');
					var row = periods.find('tr:nth-child(2)').first();
					if(row.children().length == 1) row.remove();
					periods.find('tr:first-child').after(data);
					window['period_'+id].set_calendar(calendar);
				},
			});
		});
	{/literal}</script>
	{/if}
	
	{if $edit}	
	<div class="rollup-box noPrint">
	
	<h2>{l i='header_calendar_booked' gid='listings'}</h2>
	<table class="list" id="listing_booked">
		<tr>
			<th class="w200">{l i='field_booking_period' gid='listings'}</th>
			<th class="w30">{l i='field_booking_guests' gid='listings'}</th>
			<th class="w70">&nbsp;</th>
			<th class="w100">{l i='field_booking_comment' gid='listings'}</th>
			<th class="w100">&nbsp;</th>
		</tr>
		{assign var='is_booked' value=0}
		{foreach item=item from=$data.booking.periods}
		{if $item.id_user eq $item.id_owner && $item.status eq 'book'}
		{assign var='is_booked' value=1}
		{assign var='period' value=$item}
		{include file="calendar_period.tpl" module="listings" themes="default"}
		{/if}
		{/foreach}
		{if !$is_booked}<tr><td colspan="{if $edit}5{else}4{/if}" class="center gray_italic">{l i='no_periods' gid='listings'}</td></tr>{/if}	
	</table>
	
	</div>
					
	<div class="b outside noPrint">
		<input type="button" value="{l i='btn_add' gid='start' type='button'}" name="btn_order" id="btn_order">
	</div>
	<script>{literal}
		$(function(){
			new bookingForm({
				siteUrl: '{/literal}{$site_url}{literal}',
				listingId: '{/literal}{$data.id}{literal}',
				bookingBtn: 'btn_order',
				isSavePeriod: false,
				urlGetForm: 'listings/ajax_order_form/',
				urlSaveForm: 'listings/ajax_save_order/',
				urlGetPrice: 'listings/ajax_get_booking_price/',
				calendar: listings_calendar{/literal}{$rand}{literal},
				successCallback: function(id, data, calendar){
					var periods = $('#listing_booked');
					var row = periods.find('tr:nth-child(2)').first();
					if(row.children().length == 1) row.remove();
					periods.find('tr:first-child').after(data);
					window['period_'+id].set_calendar(calendar);
				},
			});
		});
	{/literal}</script>
	{/if}
</div>
{/capture}
{if $edit || $is_opened}{$calendar_periods}{/if}
