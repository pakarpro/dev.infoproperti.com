<table cellpadding="0" cellspacing="0">
	<tr>
		<td rowspan="2">
			<a href="{seolink module='listings' method='view' data=$listing}"><img src="{$listing.media.photo.thumbs.middle}" title="{$listing.output_name|escape}"></a>
		</td>
		{capture assign='status_info'}{strip}
			{if $listing.is_lift_up || 
				$location_filter eq 'country' && $listing.is_lift_up_country || 
				$location_filter eq 'region' && ($listing.is_lift_up_country || $listing.is_lift_up_region) || 
				$location_filter eq 'city' && ($listing.is_lift_up_country || $listing.is_lift_up_region || $listing.is_lift_up_city) ||
				$sort_data.order ne 'default' && ($listing.is_lift_up ||$listing.is_lift_up_country || $listing.is_lift_up_region || $listing.is_lift_up_city)}
				{l i='status_lift_up' gid='listings'}
			{elseif $listing.is_featured}
				{l i='status_featured' gid='listings'}
			{/if}
		{/strip}{/capture}
		<td colspan="2">
			<h3>{block name=listing_price_block module='listings' data=$listing}</h3>
			{$listing.property_type_str} {$listing.operation_type_str}
			<br>{$listing.square_output|truncate:50}
			{if $listing.date_open|strtotime >= $tstamp}<br>{l i='text_date_open_status' gid='listings'}{/if}
			{if $listing.sold}<br><span class="status_text">{l i='text_sold' gid='listings'}</span>{/if}
			{if $status_info}<br><span class="status_text">{$status_info}</span>{/if}
		</td>
	</tr>
	{if $listing.user.status}
	<tr>
		<td>{$listing.user.output_name|truncate:50}</td>
		<td>
			{capture assign='user_logo'}
				<img src="{$listing.user.media.user_logo.thumbs.small}" title="{$listing.user.output_name|escape}">
			{/capture}
			{if $listing.user.status}
			<a href="{seolink module='users' method='view' data=$listing.user}">{$user_logo}</a>
			{else}
			{$user_logo}
			{/if}
		</td>
	</tr>
	{/if}
	{if $listing.headline}
	<tr>
		<td colspan="3">{$listing.headline|truncate:100}</td>
	</tr>	
	{/if}
</table>
