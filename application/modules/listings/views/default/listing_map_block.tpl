	{if $view_mode eq 'map'}
		<div class="listing-block">
			<div class="item no-hover no-border listing">
				<h3><a href="{seolink module='listings' method='view' data=$listing}" title="{$listing.output_name|escape}">{$listing.output_name|truncate:50}</a></h3>
				<div class="image">
					<a href="{seolink module='listings' method='view' data=$listing}">
						{if $listing.is_slide_show}
							<img src="{$listing.media.photo.thumbs.middle_anim}" title="{$listing.output_name|escape}">
						{else}
							<img src="{$listing.media.photo.thumbs.middle}" title="{$listing.output_name|escape}">
						{/if}
						{if $listing.photo_count || $listing.is_vtour}
						<div class="photo-info">
							<div class="panel">
								{if $listing.photo_count}<span class="btn-link"><ins class="with-icon-small w i-photo"></ins> {$listing.photo_count}</span>{/if}
								{if $listing.is_vtour}<span class="btn-link"><ins class="with-icon-small w i-vtour"></ins> 360&deg;</span>{/if}
							</div>
							<div class="background"></div>
						</div>
						{/if}
					</a>
				</div>
					
				{capture assign='status_info'}{strip}
					{if $listing.is_lift_up || 
						$location_filter eq 'country' && $listing.is_lift_up_country || 
						$location_filter eq 'region' && ($listing.is_lift_up_country || $listing.is_lift_up_region) || 
						$location_filter eq 'city' && ($listing.is_lift_up_country || $listing.is_lift_up_region || $listing.is_lift_up_city) ||
						$sort_data.order ne 'default' && ($listing.is_lift_up ||$listing.is_lift_up_country || $listing.is_lift_up_region || $listing.is_lift_up_city)}
						{l i='status_lift_up' gid='listings'}
					{elseif $listing.is_featured}
						{l i='status_featured' gid='listings'}
					{/if}
				{/strip}{/capture}
				
				<div class="body">
					{l i='no_information' gid='start' assign='no_info_str'}
					<h3>{block name=listing_price_block module='listings' data=$listing}</h3>
					<div class="t-1">
						{$listing.property_type_str} {$listing.operation_type_str}
						<br>{$listing.square_output|truncate:30}
						{if $listing.date_open|strtotime >= $tstamp}<br>{l i='text_date_open_status' gid='listings'}{/if}
						{if $listing.sold}<br><span class="status_text">{l i='text_sold' gid='listings'}</span>{/if}
						{if $status_info}<br><span class="status_text">{$status_info}</span>{/if}
					</div>
					<div class="t-2">		
					</div>
					{if $listing.user.status}
					<div class="t-3">
						{$listing.user.output_name|truncate:50}
					</div>
					<div class="t-4">
						{capture assign='user_logo'}
							{if $show_logo || $listing.user.is_show_logo}
							<img src="{$listing.user.media.user_logo.thumbs.small}" title="{$listing.user.output_name|escape}">
							{else}
							<img src="{$listing.user.media.default_logo.thumbs.small}" title="{$listing.user.output_name|escape}">
							{/if}
						{/capture}
						{if $listing.user.status}
						<a href="{seolink module='users' method='view' data=$listing.user}">{$user_logo}</a>
						{else}
							{$user_logo}
						{/if}
					</div>
					{/if}
				</div>
				<div class="clr"></div>
				{if $listing.headline}<p class="headline" title="{$listing.headline}">{$listing.headline|truncate:125}</p>{/if}
			</div>
		</div>
	{else}
		<a href="{seolink module='listings' method='view' data=$listing}">{$listing.output_name|truncate:50}</a>
	{/if}
