<div id="pages_block_1">
	<div class="pages">
		<div class="inside">		
			<ins class="prev{if $prev_listing.id eq $listing.id} gray{/if}"><a href="{seolink module='listings' method='view' data=$prev_listing}" title="{l i='nav_prev' gid='start' type='button'}">&nbsp;</a></ins>
			<ins class="current">{$current_page} {l i='text_of' gid='start'} {$total_pages}</ins>
			<ins class="next{if $next_listing.id eq $listing.id} gray{/if}"><a href="{seolink module='listings' method='view' data=$next_listing}" title="{l i='nav_next' gid='start' type='button'}">&nbsp;</a></ins>
		</div>
	</div>
	<div class="clr"></div>
</div>
