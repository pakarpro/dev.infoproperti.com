{if $price}
	<input type="hidden" value ="{$price}" id="listing_price" />
{/if}
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>
{literal}
var jq = $.noConflict(true);
jq(document).ready(function(){
function numberWithCommas(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}
	//serves as a timer delay functions
	var delay = (function(){
	  var timer = 0;
	  return function(callback, ms){
		clearTimeout (timer);
		timer = setTimeout(callback, ms);
	  };
	})();
	function loader(){
		jq('#tabletest').append(
		"<thead>"+
		"<tr>"+
			"<th align='center' colspan='2'><img src='{/literal}{$base_url}{literal}/application/modules/listings/views/default/css/loader.gif' style='border:none;'/></th>"+
		"</tr>"+
		"</thead>"
		);
		
	}
	function kpr_table(){
		var propertiID=jq('#properti').val();
		var promosiID=jq('#promosi').val();
		jq('#tabletest').empty();
		loader();
		jq.post("listings-kpr_get_bank",{ propertiID: propertiID, promosiID: promosiID }, function(json) {
			jq('#tabletest').empty();
			var data = JSON.parse(json);
			i=0;
			jq('#tabletest').append(
			"<thead>"+
			"<tr>"+
				"<th align='left' colspan='2' style='background-color:#3a5a98; color:white;'>KPR List</th>"+
			"</tr>"+
			"</thead>"
			);
			if(data)
			{
			jq.each(data, function(){
				var pinjaman_get = jq('#sisapinjaman').val();
				var pinjaman_comma = parseFloat(pinjaman_get.replace(/,/g, ''))
				var pinjaman_pokok = pinjaman_comma;
				var lama_pinjaman = jq('#masa_pinjaman').val();
				
				var tenor = lama_pinjaman*12;
				var bunga_bulan = data[i].bunga/12/100;
				var step1 = 1+bunga_bulan;
				var step2 = Math.pow(step1, -tenor);
				var step3 = 1-step2;
				var step4 = bunga_bulan/step3;
				var step5 = pinjaman_pokok*step4;
				var cicilan_bulan = parseFloat(step5).toFixed(2);
				/*
				var cicilan_pokok = pinjaman_pokok/(lama_pinjaman*12);
				var cicilan_pokok_rounded = (cicilan_pokok).toFixed(2);
				var bunga_bulan = (pinjaman_pokok*data[i].bunga/12/100).toFixed(2);
				var cicilan_bulan = (parseFloat(bunga_bulan)+parseFloat(cicilan_pokok_rounded)).toFixed(2);
				*/
				var image_check =  data[i].logo;
				if(i%2==0){
					if(image_check!='')
					{
						jq('#tabletest').append(
						"<tr>"+
							"<td align='center' style='background-color:#ebedfa;'><img src='{/literal}{$base_url}ext/kpr_cms/images/logo/{literal}"+data[i].logo+"' style='width:200px; border:0px;'/><br/>"+data[i].name+"<br/></td>"+
							"<td align='center' style='background-color:#ebedfa;'><font style='font-size:18px; color:#1C51FF; font-style:italic;'>Rp. "+numberWithCommas(cicilan_bulan)+" /</font> <font style='font-size:12px; color:grey;'>bulan</font><br/>Bunga fixed: "+data[i].bunga+"% ("+data[i].periode+" tahun)<br/></td>"+
						"</tr>"
						);
					}
					else
					{
						jq('#tabletest').append(
						"<tr>"+
							"<td align='center' style='background-color:#ebedfa;'>"+data[i].name+"<br/></td>"+
							"<td align='center' style='background-color:#ebedfa;'><font style='font-size:20px; color:#1C51FF; font-style:italic;'>Rp. "+numberWithCommas(cicilan_bulan)+" /</font> <font style='font-size:15px; color:grey;'>bulan</font><br/>Bunga fixed: "+data[i].bunga+"% ("+data[i].periode+" tahun)<br/></td>"+
						"</tr>"
						);
					}
				i++;
				}
				else
				{
					if(image_check!='')
					{
						jq('#tabletest').append(
						"<tr>"+
							"<td align='center' style='background-color:white;'><img src='{/literal}{$base_url}ext/kpr_cms/images/logo/{literal}"+data[i].logo+"' style='width:200px; border:0px;'/><br/>"+data[i].name+"<br/></td>"+
							"<td align='center' style='background-color:white;'><font style='font-size:20px; color:#1C51FF; font-style:italic;'>Rp. "+numberWithCommas(cicilan_bulan)+" /</font> <font style='font-size:15px; color:grey;'>bulan</font><br/>Bunga fixed: "+data[i].bunga+"% ("+data[i].periode+" tahun)<br/></td>"+
						"</tr>"
						);
					}
					else
					{
						jq('#tabletest').append(
						"<tr>"+
							"<td align='center' align='center' style='background-color:white;'>"+data[i].name+"<br/></td>"+
							"<td align='center' style='background-color:white;'><font style='font-size:20px; color:#1C51FF; font-style:italic;'>Rp. "+numberWithCommas(cicilan_bulan)+" /</font> <font style='font-size:15px; color:grey;'>bulan</font><br/>Bunga fixed: "+data[i].bunga+"% ("+data[i].periode+" tahun)<br/></td>"+
						"</tr>"
						);
					}
				i++;
				}
			});
			}
			else
			{
				jq('#tabletest').append(
				"<tr>"+
					"<td align='center' style='background-color:#ebedfa;'><font style='font-size:18px; color:#1C51FF; font-style:italic;'>For special rate, please contact our representative.</font></td>"+
				"</tr>"
				);
			}
		
		});
	
			
	}
	// RUN BY DEFAULT
	//kpr_table();
	// START OPTIONAL FUNCTIONS
	jq('#properti').change(function(){
		kpr_table();
	});
	jq('#promosi').change(function(){
		kpr_table();
	});
	jq('#masa_pinjaman').change(function(){
		kpr_table();
	});
	
	//added mod support to be able to directly output KPR calculation based on listings from Infoproperti
	if(jq('#listing_price').val()!='')
	{
		jq('#pinjaman').val(jq('#listing_price').val());
		jq('#check_pinjaman').hide();
		jq('#pinjaman_check').empty();
		var check_dp = jq('#dp').val();
		var check_pinjaman = jq('#pinjaman').val();
		//jq('#pinjaman_check').val(print_pinjaman);
			if((jq.isNumeric(check_dp)==true) && (jq.isNumeric(check_pinjaman)==true)){
				//append commas to a span below the input box
				var pinjaman_comma = numberWithCommas(check_pinjaman);
				var print_pinjaman = 'Rp. '+pinjaman_comma;
				jq('#pinjaman_check').append(print_pinjaman);
				
				
				var pinjaman = jq('#pinjaman').val();
				var dp = jq('#dp').val();
				var sisapinjaman = pinjaman-(pinjaman*dp/100);
				var test_sisapinjaman = numberWithCommas(sisapinjaman)
				jq('#sisapinjaman').val(test_sisapinjaman);	
					
				jq('#tabletest').empty();
				delay(function(){
					kpr_table();
				}, 2000);
				jq('#pinjaman').val(pinjaman_comma);				
			}
			else if(jq.isNumeric(check_pinjaman)==false&&check_pinjaman!=''){
				jq('#check_pinjaman').show();
			}
	}

	jq('#pinjaman').keyup(function(){
	jq('#check_pinjaman').hide();
	jq('#pinjaman_check').empty();
	var check_dp = jq('#dp').val();
	var check_pinjaman = jq('#pinjaman').val();
	//jq('#pinjaman_check').val(print_pinjaman);
		if((jq.isNumeric(check_dp)==true) && (jq.isNumeric(check_pinjaman)==true)){
			//append commas to a span below the input box
			var pinjaman_comma = numberWithCommas(check_pinjaman);
			var print_pinjaman = 'Rp. '+pinjaman_comma;
			jq('#pinjaman_check').append(print_pinjaman);
			
			
			var pinjaman = jq('#pinjaman').val();
			var dp = jq('#dp').val();
			var sisapinjaman = pinjaman-(pinjaman*dp/100);
			var test_sisapinjaman = numberWithCommas(sisapinjaman)
			jq('#sisapinjaman').val(test_sisapinjaman);	
				
			jq('#tabletest').empty();
			delay(function(){
				kpr_table();
			}, 2000);
		}
		else if(jq.isNumeric(check_pinjaman)==false&&check_pinjaman!=''){
		 	jq('#check_pinjaman').show();
		}
	});
	
	jq('#pinjaman').blur(function(){
		var pinjaman_check = jq('#pinjaman').val();
		var pinjaman_comma = numberWithCommas(pinjaman_check);
		jq('#pinjaman').val(pinjaman_comma);
	});
	
	jq('#pinjaman').focus(function(){
		var pinjaman_check = jq('#pinjaman').val();
		var pinjaman_int = parseFloat(pinjaman_check.replace(/,/g, ''))
		if(pinjaman_check=='')
		{
			jq('#pinjaman').val('');
		}
		else
		{
			jq('#pinjaman').val(pinjaman_int);
		}
	});
	
	jq('#dp').keyup(function(){
	jq('#check_dp').hide();	
	var check_dp = jq('#dp').val();
	var check_pinjaman = jq('#pinjaman').val();
	var pinjaman = parseFloat(check_pinjaman.replace(/,/g, ''))
		if((jq.isNumeric(check_dp)==true) && (jq.isNumeric(pinjaman)==true)){
			
			//var pinjaman = jq('#pinjaman').val();
			var dp = jq('#dp').val();
			var sisapinjaman = pinjaman-(pinjaman*dp/100);
			var sisapinjaman_comma = numberWithCommas(sisapinjaman);
			jq('#sisapinjaman').val(sisapinjaman_comma);	
				
			jq('#tabletest').empty();
			delay(function(){
				kpr_table();
			}, 1000);
		}
		else if(jq.isNumeric(check_dp)==false){
		 	jq('#check_dp').show();
		}
		
	});
});
{/literal}
</script>
<div class="kpr_wrapper">

<form>
<div class="kpr_header">Kalkulasi Cicilan KPR</div>
<div style="position:relative;width:250px; float:left;"></div>
<div class="kpr_input" style="position:relative; float:left;">
	<table style="width:850px;"  cellpadding="5" cellspacing="0">
    	<tr>
        	<td>
            Harga Properti <font color="red"><span id="check_pinjaman" style="display:none;">Must be Numeric!</span></font><br/>
            <input name="pinjaman" id="pinjaman" type="text" style="width:100%;" value="0" fcsa-number/>
            </td>
            <td>
            Down Payment (Uang Muka) <font color="red"><span id="check_dp" style="display:none;">Must be Numeric!</span></font><br/>
            <input name="dp" id="dp" type="text" value="30" size="2" />%
            </td>
            <td></td>
        </tr>
        <tr>
        	<td><font color="#ae8d6a"><span id="pinjaman_check"></span></font></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
        	<td>
            Pinjaman<br/>
            <input name="sisapinjaman" id="sisapinjaman" value="0"  type="text" disabled="disabled" style="width:100%; background-color:#E6E6E6; cursor:not-allowed;"/>
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr>
        	<td colspan="2">
            <table cellspacing="10">
                <tr>
                    <td>Tipe Properti</td>
                    <td>Lama Pinjaman</td>
                    <td>Masa Promosi</td>
                </tr>
                <tr>
                    <td>
                    <!--<select name="properti" id="properti" {if $propertiID}disabled="disabled" style="background-color:#E6E6E6; cursor:not-allowed;"{/if}>-->
                    <select name="properti" id="properti" >
                    {foreach item=item from=$properti}
                    <option value="{$item->propertiID}" {if $propertiID==$item->propertiID}selected="selected"{/if}>{$item->namaproperti}</option>
                    {/foreach}
                    </select>
                    </td>
                    <td>
                    <select name="masa_pinjaman" id="masa_pinjaman">
                    {foreach item=item from=$pinjaman}
                    <option value="{$item->lamapinjaman}">{$item->lamapinjaman} Tahun</option>
                    {/foreach}
                    </select>
                    </td>
                    <td>
                    <select name="promosi" id="promosi">
                    {foreach item=item from=$promosi}
                    <option value="{$item->promosiID}">{$item->periode} Tahun</option>
                    {/foreach}
                    </select>
                    </td>
                </tr>
            
            </table>
            </td>
            <td></td>
            
        </tr>
    </table>
</div>
	<!-- top part form -->
    <br/>
</form>      
	<!-- loader animation -->
    <!-- end of loader animation -->
    <table id="tabletest" align="center" style="width:100%; margin-bottom:25px; margin-top:25px; border:1px black solid;"  cellpadding="10" cellspacing="0">
    
    </table>
    

</div>
