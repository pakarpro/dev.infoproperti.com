	<table cellspacing="0" cellpadding="0" class="data" width="100%">
	<tr>
		<th class="first" colspan="2">{l i='stat_header_listings' gid='listings'}</th>
	</tr>
	{if $stat_listings.index_method}
	<tr>
		<td class="first"><a href="{$site_url}admin/listings/index/">{l i='stat_header_all' gid='listings'}</a></td>
		<td class="w50 center"><a href="{$site_url}admin/listings/index/">{$stat_listings.all}</a></td>
	</tr>
	<tr class="zebra">
		<td class="first"><a href="{$site_url}admin/listings/active/">{l i='stat_header_active' gid='listings'}</a></td>
		<td class="w50 center"><a href="{$site_url}admin/listings/active/">{$stat_listings.active}</a></td>
	</tr>
	<tr>
		<td class="first"><a href="{$site_url}admin/listings/not_active/">{l i='stat_header_inactive' gid='listings'}</a></td>
		<td class="w50 center"><a href="{$site_url}admin/listings/not_active/">{$stat_listings.inactive}</a></td>
	</tr>
	{/if}
	<tr class="zebra">
		<td class="first"><a href="{$site_url}admin/listings/moderation/">{l i='stat_header_moderate' gid='listings'}</a></td>
		<td class="w50 center"><a href="{$site_url}admin/listings/moderation/">{$stat_listings.moderate}</a></td>
	</tr>
	</table>
