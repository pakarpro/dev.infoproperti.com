<div class="load_content_controller">
	<h1>{l i='header_order_add' gid='listings'}</h1>
	<div class="inside">
		<form method="post" class="edit-form n150" name="save_form" id="order_form" enctype="multipart/form-data">
			<div class="row">
				<div class="h">{l i='field_booking_date_start' gid='listings'}:</div>
				<div class="v">
					{switch from=$listing.price_period}
						{case value='1'}
							<input type="text" name="period[date_start]" value="{if $order.date_start|strtotime>0}{$order.date_start|date_format:$page_data.date_format|escape}{/if}" id="date_start{$rand}" class="middle"> <a href="#" id="date_start_open_btn" title="{l i='link_calendar_open' gid='listings' type='button'}"><ins class="with-icon i-calendar"></ins></a>
							<input type="hidden" name="date_start_alt" value="" id="alt_date_start{$rand}">
							<script>{literal}
								$(function(){
									$('#date_start{/literal}{$rand}{literal}').datepicker({dateFormat: '{/literal}{$page_data.datepicker_date_format}{literal}', altFormat: '{/literal}{$page_data.datepicker_alt_format}{literal}', altField: '#alt_date_start{/literal}{$rand}{literal}', showOn: 'both'});
								});
							{/literal}</script>
						{case value='2'}
							{ld i='month-names' gid='start' assign='month_names'}
							<select name="date_start_month" class="middle">
								<option value="">{$month_names.header}</value>
								{foreach item=item key=key from=$month_names.option}
								<option value="{$key}" {if $key eq $order.date_start|date_format:'%m'}selected{/if}>{$item}</option>
								{/foreach}
							</select>
							{assign var='cyear' value=('now'|date:'Y')}
							<select name="date_start_year" class="middle">
								<option value="">{l i='text_year' gid='listings'}</option>
								{for start=0 stop=10 value=i}
								{math equation='x + y' x=$cyear y=$i assign='year'}
								<option value="{$year}" {if $year eq $order.date_start|date_format:'%Y'}selected{/if}>{$year}</option>
								{/for}
							</select>
					{/switch}
				</div>
			</div>
			<div class="row">
				<div class="h">{l i='field_booking_date_end' gid='listings'}:</div>
				<div class="v">
					{switch from=$listing.price_period}
						{case value='1'}
							<input type="text" name="period[date_end]" value="{if $order.date_end|strtotime>0}{$order.date_end|date_format:$page_data.date_format|escape}{/if}" id="date_end{$rand}" class="middle"> <a href="#" id="date_end_open_btn" title="{l i='link_calendar_open' gid='listings' type='button'}"><ins class="with-icon i-calendar"></ins></a>
							<input type="hidden" name="date_end_alt" value="" id="alt_date_end{$rand}">
							<script>{literal}
								$(function(){
									$('#date_end{/literal}{$rand}{literal}').datepicker({dateFormat: '{/literal}{$page_data.datepicker_date_format}{literal}', altFormat: '{/literal}{$page_data.datepicker_alt_format}{literal}', altField: '#alt_date_end{/literal}{$rand}{literal}', showOn: 'both'});
								});
							{/literal}</script>
						{case value='2'}
							{ld i='month-names' gid='start' assign='month_names'}
							<select name="date_end_month" class="middle">
								<option value="">{$month_names.header}</value>
								{foreach item=item key=key from=$month_names.option}
								<option value="{$key}" {if $key eq $order.date_end|date_format:'%m'}selected{/if}>{$item}</option>
								{/foreach}
							</select>
							{assign var='cyear' value=('now'|date:'Y')}
							<select name="date_end_year" class="middle">
								<option value="">{l i='text_year' gid='listings'}</option>
								{for start=0 stop=10 value=i}
								{math equation='x + y' x=$cyear y=$i assign='year'}
								<option value="{$year}" {if $year eq $order.date_end|date_format:'%Y'}selected{/if}>{$year}</option>
								{/for}
							</select>
					{/switch}
				</div>
			</div>
			<div class="row">
				<div class="h">{l i='field_booking_guests' gid='listings'}:</div>
				<div class="v">
					{ld i='booking_guests' gid='listings' assign='booking_guests'}
					<select name="period[guests]" class="middle">
						{foreach item=item key=key from=$booking_guests.option}
						<option value="{$key}" {if $key eq $order.guests}{/if}>{$item}</option>
						{/foreach}
					</select>
				</div>
			</div>
			<div class="row">
				<div class="h">{l i='field_booking_comment' gid='listings'}:</div>
				<div class="v" id="period_comment">
					<textarea name="period[comment]" rows="5" cols="80">{$order.comment|escape}</textarea>
				</div>
			</div>
			<div class="btn"><div class="l"><input type="submit" name="btn_save" value="{l i='btn_save' gid='start' type='button'}" id="close_btn"></div></div>
		</form>
	</div>
</div>
<script>{literal}
$(function(){
	$("#order_form div.row:odd").addClass("zebra");
});
{/literal}</script>
