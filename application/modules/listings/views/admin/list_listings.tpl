{include file="header.tpl"}
{helper func_name=get_admin_level1_menu helper_name=menu func_param='admin_listings_menu'}
<div class="actions">
	<ul>
		{if !$is_operation_types_disabled}<li><div class="l"><a href="{$site_url}admin/listings/edit">{l i='link_add_listing' gid='listings'}</a></div></li>{/if}
	</ul>
	&nbsp;
</div>

<div class="menu-level3">
	<ul>
		<li class="{if $filter eq 'all'}active{/if}{if !$filter_data.all} hide{/if}"><a href="{$site_url}admin/listings/all">{l i='filter_all_listings' gid='listings'} ({$filter_data.all})</a></li>
		<li class="{if $filter eq 'not_active'}active{/if}{if !$filter_data.not_active} hide{/if}"><a href="{$site_url}admin/listings/not_active">{l i='filter_not_active_listings' gid='listings'} ({$filter_data.not_active})</a></li>
		<li class="{if $filter eq 'active'}active{/if}{if !$filter_data.active} hide{/if}"><a href="{$site_url}admin/listings/active">{l i='filter_active_listings' gid='listings'} ({$filter_data.active})</a></li>
	</ul>
	&nbsp;
</div>
	
<form method="post" action="" name="save_form" enctype="multipart/form-data">
<div class="filter-form">
	<div class="form">
			<div class="row">
				<div class="h">{l i='field_listing_type' gid='listings'}:</div>
				<div class="v">
					<select name="type" id="type" class="long">
						<option value="">...</option>
						{foreach item=item from=$operation_types}
						<option value="{$item|escape}" {if $item eq $page_data.filter.type}selected{/if}>{l i='operation_search_'+$item gid='listings'}</option>
						{/foreach}
					</select>
				</div>
			</div>

			{if $page_data.filter.property_type}
				{assign var='category' value=$page_data.filter.id_category+'_'+$page_data.filter.property_type}
			{else}
				{assign var='category' value=$page_data.filter.id_category}
			{/if}
			<div class="row">
				<div class="h">{l i='field_category' gid='listings'}:</div>
				<div class="v">{block name='properties_select' module='properties' var_name='category' js_var_name='category' selected=$category cat_select=true}</div>
			</div>
		
			<div class="row">
				<div class="h">{l i='field_country' gid='listings'}:</div>
				<div class="v">{country_select select_type='city' id_country=$page_data.filter.id_country id_region=$page_data.filter.id_region id_city=$page_data.filter.id_city}</div>
			</div>
			<div class="row">
				<div class="h">{l i='field_owner' gid='listings'}:</div>
				<div class="v">{user_select selected=$page_data.filter.id_user max=1 var_name='id_user'}</div>
			</div>
			<div class="row">
				<div class="h">
					<input type="submit" name="filter-submit" value="{l i='header_filters' gid='listings' type='button'}">
					<input type="submit" name="filter-reset" value="{l i='header_reset' gid='listings' type='button'}">
				</div>
			</div>
	</div>
</div>
</form>

<form id="listings_form" action="" method="post">
<table cellspacing="0" cellpadding="0" class="data" width="100%">
<tr>
	<th>{l i='field_photo' gid='listings'}</th>
	<th class="w200">{l i='field_description' gid='listings'}</th>
	<th class="w150">{l i='field_user_data' gid='listings'}</th>
	{depends module=reviews}
	<th class="w30"><a href="{$sort_links.reviews}"{if $order eq 'reviews'} class="{$order_direction|lower}"{/if}>{l i='field_reviews_count' gid='listings'}</a></th>
	{/depends}
	<th class="w150"><a href="{$sort_links.date_created}"{if $order eq 'date_created'} class="{$order_direction|lower}"{/if}>{l i='field_date_created' gid='listings'}</a></th>
	<th class="w150">&nbsp;</th>
</tr>
{foreach item=item from=$listings}
{counter print=false assign=counter}
{capture assign='property_output'}
	{$item.property_type_str} {$item.operation_type_str}
{/capture}
{capture assign='price_output'}
	{block name=listing_price_block module='listings' data=$item template='small'}
{/capture}
<tr{if $counter is div by 2} class="zebra"{/if}>
	<td><img src="{$item.media.photo.thumbs.small}" alt="{$item.output_name|truncate:20|escape}"></td>
	<td>{$item.output_name|truncate:23}<br>{$property_output|truncate:23}<br>{$price_output}</td>
	<td>{$item.user.output_name|truncate:30}</td>
	{depends module=reviews}
	<td class="center"><a href="{$site_url}admin/reviews/index/listings_object/{$item.id}">{$item.review_count}</a></td>
	{/depends}
	<td class="center">{$item.date_created|date_format:$page_data.date_format}</td>
	<td class="icons">
		{if $item.status}
		<a href="{$site_url}admin/listings/activate/{$item.id}/0"><img src="{$site_root}{$img_folder}icon-full.png" width="16" height="16" border="0" alt="{l i='link_deactivate_listing' gid='listings' type='button'}" title="{l i='link_deactivate_listing' gid='listings' type='button'}"></a>
		{else}
		<a href="{$site_url}admin/listings/activate/{$item.id}/1"><img src="{$site_root}{$img_folder}icon-empty.png" width="16" height="16" border="0" alt="{l i='link_activate_listing' gid='listings' type='button'}" title="{l i='link_activate_listing' gid='listings' type='button'}"></a>
		{/if}
		<a href="{$site_url}admin/listings/edit/{$item.id}"><img src="{$site_root}{$img_folder}icon-edit.png" width="16" height="16" border="0" alt="{l i='link_edit_listing' gid='listings' type='button'}" title="{l i='link_edit_listing' gid='listings' type='button'}"></a>
		<a href="{$site_url}admin/listings/delete/{$item.id}" onclick="javascript: if(!confirm('{l i='note_delete_listing' gid='listings' type='js'}')) return false;"><img src="{$site_root}{$img_folder}icon-delete.png" width="16" height="16" border="0" alt="{l i='link_delete_listing' gid='listings' type='button'}" title="{l i='link_delete_listing' gid='listings' type='button'}"></a>
	</td>
</tr>
{foreachelse}
{assign var='colspan' value=6}
{depends module=reviews}{assign var='colspan' value=$colspan+1}{/depends}
<tr><td colspan="{$colspan}" class="center">{l i='no_listings' gid='listings'}</td></tr>
{/foreach}
</table>
</form>
{include file="pagination.tpl"}

{js file='easyTooltip.min.js'}
<script>{literal}
var reload_link = "{/literal}{$site_url}admin/listings{literal}";
var order = '{/literal}{$order}{literal}';
var loading_content;
var order_direction = '{/literal}{$order_direction}{literal}';
$(function(){
	$(".tooltip").each(function(){
		$(this).easyTooltip({
			useElement: 'span_'+$(this).attr('id')
		});
	});
});
function reload_this_page(value){
	var link = reload_link + '/' + value + '/' + order + '/' + order_direction;
	location.href=link;
}

{/literal}</script>

{include file="footer.tpl"}
