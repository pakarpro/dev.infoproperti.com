		{switch from=$listing.price_period}
			{case value='1'}
				<p class="calendar-year">{$years}</p>
				<div class="calendar-cont">
					{foreach item=month from=$months}
					<div class="calendar">
						<p class="calendar-month">{$month.name}</p>
						<table cellpadding="0" cellspacing="0">
							<thead>
								<tr>							
									{foreach item=item key=key from=$weeks}
									<th>{$item}</th>
									{/foreach}
								</tr>
							</thead>
							<tbody>
								{foreach item=week key=key from=$month.weeks}
								<tr>							
									{foreach item=item from=$week}
									<td class="{if $item.start}period-start{/if} {if $item.end}period-end{/if}">
										<div class="{$item.status}" title="{ld_option i='booking_status' gid='listings' option=$item.status type='button'}">{$item.day}</div>
										{if $item.status eq 'book'}
										{if $item.comment}
										<span class="period-comment" title="{$item.comment|escape}">i</span>
										{else}
										<span>&nbsp;</span>
										{/if}
										{else}
										<span>{$item.price}</span>
										{/if}
									</td>
									{/foreach}
								</tr>
								{/foreach}
							</tbody>
						</table>
					</div>
					{/foreach}
					<div class="clr"></div>
				</div>
			{case value='2'}
				<div class="calendar-cont">
					{foreach item=year key=key from=$years}
					<div class="calendar">
						<p class="calendar-months-year">{$key}</p>
						<table cellpadding="0" cellspacing="0">
							<tr>
								{foreach item=item from=$year.0}
								<td>
									<div class="{$item.status}">{$item.month}</div>
									{if $item.status eq 'book'}
									{if $item.comment}
									<span class="period-comment" title="{$item.comment|escape}">i</span>
									{else}
									<span>&nbsp;</span>
									{/if}
									{else}
									{$item.price}
									{/if}
								</td>
								{/foreach}
							</tr>	
							<tr>
								{foreach item=item from=$year.1}
								<td>
									<div class="{$item.status}">{$item.month}</div>
									{if $item.status eq 'book'}
									{if $item.comment}
									<span class="period-comment" title="{$item.comment|escape}">i</span>
									{else}
									<span>&nbsp;</span>
									{/if}
									{else}
									{$item.price}
									{/if}
								</td>
								{/foreach}
							</tr>
							<tr>
								{foreach item=item from=$year.2}
								<td>
									<div class="{$item.status}">{$item.month}</div>
									{if $item.status eq 'book'}
									{if $item.comment}
									<span class="period-comment" title="{$item.comment|escape}">i</span>
									{else}
									<span>&nbsp;</span>
									{/if}
									{else}
									{$item.price}
									{/if}
								</td>
								{/foreach}
							</tr>
						</table>
					</div>
					{/foreach}
				</div>
		{/switch}
