{include file="header.tpl" load_type='ui'}

{include file="edit_listings_menu.tpl" module="listings"}

<form method="post" action="{$data.action|escape}" name="save_form" id="save_form" enctype="multipart/form-data">
	<div class="edit-form n150">
		<div class="row header">{l i='admin_header_listings_services' gid='listings'}</div>
		<div class="rollup-box">
			{if $page_data.use_services && $page_data.listings_services}
			{foreach item=item from=$page_data.listings_services}
			<div class="content-block">
				<p><b>{$item.name}</b></p>
				{foreach item=item2 key=key2 from=$item.data_admin}
				<p>{$item.template.data_admin_array[$key2].name}: {$item.data_admin[$key2]}</p>
				{/foreach}
				<a class="btn-link" href="{$site_url}admin/listings/apply_service/{$data.id}/{$item.gid}">
				{l i='btn_apply' gid='start'}
				</a>
				<br>
			</div>
			{/foreach}
			{/if}
		</div>
	</div>
	<a class="cancel" href="{$site_url}admin/listings">{l i='btn_cancel' gid='start'}</a>
</form>
<div class="clr"></div>

<script>{literal}
	var on_moderation = '{/literal}{$on_moderation}{literal}';
	$(function(){
		$("div.row:odd").addClass("zebra");
		if(on_moderation == 1){
			$('#save_form input, #save_form textarea, #save_form select').attr('disabled', 'disabled');
		}
	});
{/literal}</script>

{include file="footer.tpl"}
