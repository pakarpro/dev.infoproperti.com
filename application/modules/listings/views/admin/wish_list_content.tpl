{include file="header.tpl" load_type='ui'}
{js module=listings file='wish-list.js'}
<div class="actions">
	<ul>
		<li><div class="l"><a href="{$site_url}admin/listings/delete_from_wish_list/{$wish_list.id}" id="delete_from_wish_list">{l i='link_delete_from_wish_list' gid='listings'}</a></div></li>
	</ul>
	&nbsp;
</div>

<div id="wish_list_content">
{if $listings|count}
<ol class="blocks" id="sortList">
	{foreach item=item from=$listings}
	<li id="pItem{$item.listing.id}">
	<div class="item listing">
		<div class="fright"><a href="{$site_url}admin/listings/delete_from_wish_list/{$wish_list.id}/{$item.listing.id}" onclick="javascript: if(!confirm('{l i='note_delete_from_wish_list' gid='listings' type='js'}')) return false; wishList.delete_from_wish_list({$item.listing.id}); return false;"><img src="{$site_root}{$img_folder}icon-delete.png" width="16" height="16" border="0" alt="{l i='link_delete_from_wish_list' gid='listings' type='button'}" title="{l i='link_delete_from_wish_list' gid='listings' type='button'}"></a></div>
		<h3><a href="{$site_url}admin/listings/edit/{$item.listing.id}">{$item.listing.output_name|truncate:45}</a></h3>
		<div class="image">
			<a href="{$site_url}admin/listings/edit/{$item.listing.id}"><img src="{$item.listing.media.photo.thumbs.small}" title="{$item.listing.output_name}"></a>
		</div>
		<div class="body">
			{l i='no_information' gid='start' assign='no_info_str'}
			<h3>{block name=listing_price_block module='listings' data=$item.listing}</h3>
			<div class="t-1">
				{$item.listing.property_type_str} {$item.listing.operation_type_str}
				<br>{$item.listing.square_output|truncate:20}
				{if $item.listing.date_open|strtotime >= $tstamp}<br>{l i='text_date_open_status' gid='listings'}{/if}
			</div>
			<div class="t-2">
				
			</div>
			<div class="t-3">
				{$item.listing.user.output_name|truncate:50}
			</div>
		</div>
		<div class="clr"></div>
	</div>
	</li>
	{/foreach}
</ol>
{/if}
<p {if $listings|count > 0}class="hide"{/if} id="empty_content">{l i='no_listings' gid='listings'}</p>
<div class="clr"></div>
</div>

<script>{literal}
	var wishList;
	$(function(){
		wishList = new wishList({
			siteUrl: '{/literal}{$site_url}{literal}',
			saveSortingUrl: 'admin/listings/ajax_save_wish_list_sorting/{/literal}{$wish_list.id}{literal}',
			deleteFromWishListUrl: 'admin/listings/ajax_delete_from_wish_list/{/literal}{$wish_list.id}{literal}/',
			reloadBlockUrl: 'admin/listings/ajax_reload_wish_list_content/{/literal}{$wish_list.id}{literal}/'
		});
	});
{/literal}</script>
	
{include file="footer.tpl"}
