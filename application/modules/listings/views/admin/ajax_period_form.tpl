<div class="load_content_controller">
	<h1>{if $data.id}{l i='admin_header_calendar_period_add' gid='listings'}{else}{l i='admin_header_listings_add' gid='listings'}{/if}</h1>
	<div class="inside">
		<form name="period_edit" class="edit-form n150" id="period_form">
			{if $data.price_period}
				{assign var='price_period' value=$data.price_period}
			{else}
				{assign var='price_period' value=1}
			{/if}
			<div class="row">
				<div class="h">{l i='field_booking_date_start' gid='listings'}: </div>
				<div class="v">
					{switch from=$price_period}
						{case value='1'}
							<input type="text" name="period[date_start]" value="{if $period.date_start|strtotime>0}{$period.date_start|date_format:$page_data.date_format|escape}{/if}" id="date_start{$rand}" class="middle">
							<input type="hidden" name="date_start_alt{$rand}" value="" id="alt_date_start">
							<script>{literal}
								$(function(){
									$('#date_start{/literal}{$rand}{literal}').datepicker({dateFormat: '{/literal}{$page_data.datepicker_date_format}{literal}', altFormat: '{/literal}{$page_data.datepicker_alt_format}{literal}', altField: '#alt_date_start{/literal}{$rand}{literal}'});
								});
							{/literal}</script>
						{case value='2'}
							{ld i='month-names' gid='start' assign='month_names'}
							<select name="date_start_month" class="middle">
								<option value="">{$month_names.header}</value>
								{foreach item=item key=key from=$month_names.option}
								<option value="{$key}" {if $key eq $period.date_start|date_format:'%m'}selected{/if}>{$item}</option>
								{/foreach}
							</select>
							{assign var='cyear' value=('now'|date:'Y')}
							<select name="date_start_year" class="middle">
								<option value="">{l i='text_year' gid='listings'}</option>
								{for start=0 stop=10 value=i}
								{math equation='x + y' x=$cyear y=$i assign='year'}
								<option value="{$year}" {if $year eq $period.date_start|date_format:'%Y'}selected{/if}>{$year}</option>
								{/for}
							</select>
					{/switch}
				</div>
			</div>
			<div class="row">
				<div class="h">{l i='field_booking_date_end' gid='listings'}:</div>
				<div class="v">
					{switch from=$price_period}
						{case value='1'}
							<input type="text" name="period[date_end]" value="{if $period.date_end|strtotime>0}{$period.date_end|date_format:$page_data.date_format|escape}{/if}" id="date_end{$rand}" class="middle">
							<input type="hidden" name="date_end_alt{$rand}" value="" id="alt_date_end">
							<script>{literal}
								$(function(){
									$('#date_end{/literal}{$rand}{literal}').datepicker({dateFormat: '{/literal}{$page_data.datepicker_date_format}{literal}', altFormat: '{/literal}{$page_data.datepicker_alt_format}{literal}', altField: '#alt_date_end{/literal}{$rand}{literal}'});
								});
							{/literal}</script>
						{case value='2'}
							{ld i='month-names' gid='start' assign='month_names'}
							<select name="date_end_month" class="middle">
								<option value="">{$month_names.header}</value>
								{foreach item=item key=key from=$month_names.option}
								<option value="{$key}" {if $key eq $period.date_end|date_format:'%m'}selected{/if}>{$item}</option>
								{/foreach}
							</select>
							{assign var='cyear' value=('now'|date:'Y')}
							<select name="date_end_year" class="middle">
								<option value="">{l i='text_year' gid='listings'}</option>
								{for start=0 stop=10 value=i}
								{math equation='x + y' x=$cyear y=$i assign='year'}
								<option value="{$year}" {if $year eq $period.date_end|date_format:'%Y'}selected{/if}>{$year}</option>
								{/for}
							</select>
					{/switch}
				</div>
			</div>
			{*<div class="row">
				<div class="h">{l i='field_booking_status' gid='listings'}:</div>
				<div class="v">
					{ld i='booking_status' gid='listings' assign='booking_status'}
					<select name="period[status]" class="middle">
						<option value="">{$booking_status.header}</option>
						{foreach item=item key=key from=$booking_status.option}
						<option value="{$key}" {if $key eq $period.status}{/if}>{$item}</option>
						{/foreach}
					</select>
				</div>
			</div>*}
			<div class="row">
				<div class="h">{l i='field_booking_guests' gid='listings'}:</div>
				<div class="v">
					{ld i='booking_guests' gid='listings' assign='booking_guests'}
					<select name="period[guests]" class="middle">
						<option value="">{$booking_guests.header}</option>
						{foreach item=item key=key from=$booking_guests.option}
						<option value="{$key}" {if $key eq $period.guests}selected{/if}>{$item}</option>
						{/foreach}
					</select>
				</div>
			</div>
			<div class="row">
				<div class="h">{l i='field_booking_price' gid='listings'}:</div>
				<div class="v">
					<input type="text" name="period[price]" value="{$period.price|escape}" class="middle"> {$current_price_currency.abbr} {ld_option i='price_period' gid='listings' option=$price_period}
				</div>
			</div>
			<div class="row">
				<div class="h">{l i='field_booking_comment' gid='listings'}:</div>
				<div class="v">
					<textarea name="period[comment]" rows="5" cols="80">{$period.comment|escape}</textarea>
				</div>
			</div>
			<div class="btn"><div class="l"><input type="button" value="{l i='btn_save' gid='start' type='button'}" name="btn_period" id="close_btn"></div></div>
			<input type="hidden" name="period[status]" value="open">
		</form>
	</div>
</div>
<script>{literal}
$(function(){
	$("#period_form div.row:odd").addClass("zebra");
});
{/literal}</script>
