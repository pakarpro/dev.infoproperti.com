{include file="header.tpl" load_type='ui'}
{if $data.id}
<div class="menu-level3">
	<ul>
		<li class="{if $section_gid eq 'overview'}active{/if}"><a href="{$site_url}admin/listings/moderation_edit/{$data.id}/overview">{l i='filter_section_overview' gid='listings'}{if $section_gid ne 'overview' && $compare_data.sections.overview eq '1'} ({l i='mod_data_chaged' gid='listings'}){/if}</a></li>
		{if $fields_data|count}<li class="{if $section_gid eq 'description'}active{/if}"><a href="{$site_url}admin/listings/moderation_edit/{$data.id}/description">{l i='filter_section_description' gid='listings'}{if $section_gid ne 'description' && $compare_data.sections.description eq '1'} ({l i='mod_data_chaged' gid='listings'}){/if}</a></li>{/if}
		{if $data.operation_type eq 'sale'}<li class="{if $section_gid eq 'gallery'}active{/if}"><a href="{$site_url}admin/listings/moderation_edit/{$data.id}/gallery">{l i='filter_section_gallery' gid='listings'}{if $section_gid ne 'gallery' && $compare_data.sections.gallery eq '1'} ({l i='mod_data_chaged' gid='listings'}){/if}</a></li>{/if}
	</ul>
	&nbsp;
</div>
{/if}
<form method="post" action="{$data.action}" name="save_form" enctype="multipart/form-data">
	<div class="edit-form n150">
		<div class="row header">{l i='admin_header_listings_moderation_edit' gid='listings'}</div>
{if $section_gid eq 'overview'}
		<div class="row{if $compare_data.fields.address eq '1'} mark{/if}">
			<div class="h">{l i='field_address' gid='listings'}: </div>
			<div class="v"><input type="text" name="data[address]" value="{$data.address|escape}"></div>
		</div>
		<div class="row{if $compare_data.fields.zip eq '1'} mark{/if}">
			<div class="h">{l i='field_zip' gid='listings'}: </div>
			<div class="v"><input type="text" name="data[zip]" value="{$data.zip|escape}"></div>
		</div>				
{elseif $section_gid eq 'description'}
		{assign var='compare_fields' value=$compare_data.fields}
		{foreach item=item from=$fields_data}
		{assign var='field_name' value=$item.field_name}
		<div class="row{if $compare_data.fields[$field_name] eq '1'} mark{/if}">
			<div class="h">{$item.name}: </div>
			<div class="v">
			{if $item.field_type eq 'select'}
				{if $item.settings_data_array.view_type eq 'select'}
				<select name="{$item.field_name}">
				{if $item.settings_data_array.empty_option}<option value="0"{if $value eq 0} selected{/if}>...</option>{/if}
				{foreach item=option key=value from=$item.options.option}<option value="{$value|escape}" {if $value eq $item.value}selected{/if}>{$option}</option>{/foreach}
				</select>
				{else}
				{if $item.settings_data_array.empty_option}<input type="radio" name="{$item.field_name}" value="0" {if $value eq 0}checked{/if} id="{$item.field_name}_0"><label for="{$item.field_name}_0">No select</label><br>{/if}
				{foreach item=option key=value from=$item.options.option}<input type="radio" name="{$item.field_name}" value="{$value|escape}" {if $value eq $item.value} checked{/if} id="{$item.field_name}_{$value|escape}"><label for="{$item.field_name}_{$value|escape}">{$option}</label><br>{/foreach}
				{/if}
			{elseif $item.field_type eq 'text'}
				<input type="text" name="{$item.field_name}" value="{$item.value|escape}" maxlength="{$item.settings_data_array.max_char}" {if $item.settings_data_array.max_char < 11}class="short"{elseif $item.settings_data_array.max_char > 1100}class="long"{/if}>
			{elseif $item.field_type eq 'textarea'}
				<textarea name="{$item.field_name}">{$item.value|escape}</textarea>
			{elseif $item.field_type eq 'checkbox'}
				<input type="checkbox" name="{$item.field_name}" value="1" {if $item.value eq '1'}checked{/if}>
			{/if}
			&nbsp;
			</div>
		</div>
		{foreachelse}
		<p>{l i='no_fields_for_moderate'}</p>
		{/foreach}
{elseif $data.operation_type eq 'sale' && $section_gid eq 'gallery'}
		<div class="row{if $compare_data.fields.listing_file eq '1'} mark{/if}">
			<div class="h">{l i='field_listing_file' gid='listings'}: </div>
			<div class="v">
				{if $data.listing_file}
				<br><a href="{$data.listing_file_content.file_url}" target="blank">{l i='field_file_download' gid='listings'}</a>
				<br><input type="checkbox" name="listing_file_delete" value="1" id="uichb"><label for="uichb">{l i='field_file_delete' gid='listings'}</label><br>
				{else}
				<i>{l i='no_file_uploaded' gid='listings'}</i>
				{/if}
			</div>
		</div>
		<div class="row{if $compare_data.fields.listing_file_name eq '1'} mark{/if}">
			<div class="h">{l i='field_listing_file_name' gid='listings'}: </div>
			<div class="v">
				<input type="text" name="data[listing_file_name]" value="{$data.listing_file_name|escape}">
			</div>
		</div>
		<div class="row {if $compare_data.fields.listing_file_comment eq '1'}mark{/if}">
			<div class="h">{l i='field_listing_file_comment' gid='listings'}: </div>
			<div class="v">
				<textarea name="data[listing_file_comment]">{$data.listing_file_comment|escape}</textarea>
			</div>
		</div>
		<div class="row{if $compare_data.fields.listing_video eq '1'} mark{/if}">
			<div class="h">{l i='field_video' gid='listings'}: </div>
			<div class="v">
				{if $data.listing_video}
					<br>{l i='field_video_status' gid='listings'}: 
					{if $data.listing_video_data.status eq 'end' && $data.listing_video_data.errors}
					<font color="red">{foreach item=item from=$data.listing_video_data.errors}{$item}<br>{/foreach}</font>
					{elseif $data.listing_video_data.status eq 'end'}
					<font color="green">{l i='field_video_status_end' gid='listings'}</font><br>
					{elseif $data.listing_video_data.status eq 'images'}
					<font color="yellow">{l i='field_video_status_images' gid='listings'}</font><br>
					{elseif $data.listing_video_data.status eq 'waiting'}
					<font color="yellow">{l i='field_video_status_waiting' gid='listings'}</font><br>
					{elseif $data.listing_video_data.status eq 'start'}
					<font color="yellow">{l i='field_video_status_start' gid='listings'}</font><br>
					{/if}
					{if $data.listing_video_content.embed}<br>{$data.listing_video_content.embed}{/if}
					<br><input type="checkbox" name="listing_video_delete" value="1" id="uvchb"><label for="uvchb">{l i='field_video_delete' gid='listings'}</label>
				{else}
				<i>{l i='no_video_uploaded' gid='listings'}</i>
				{/if}
			</div>
		</div>
{/if}
		<br>
		<div class="row header">{l i='admin_header_moderation_alert' gid='listings'}</div>
		<div class="row">
			<div class="h">{l i='field_admin_alert' gid='listings'}: </div>
			<div class="v">
				<textarea name="admin_alert">{$data.admin_alert|escape}</textarea>
			</div>
		</div>
	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="{l i='btn_only_save' gid='listings' type='button'}"></div></div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save_approve" value="{l i='btn_save_and_approve' gid='listings' type='button'}"></div></div>
	<a class="cancel" href="{$site_url}admin/listings/moderation">{l i='btn_cancel' gid='start'}</a>
	<div class="btn fright"><div class="l"><input type="submit" name="btn_decline" value="{l i='btn_decline' gid='start' type='button'}"></div></div>
</form>
<div class="clr"></div>
<script>{literal}
$(function(){
	$("div.row:odd").addClass("zebra");
});
{/literal}</script>

{include file="footer.tpl"}
