{if $data.id}
<div class="menu-level3">
	<ul>
		<li class="{if $section_gid eq 'overview'}active{/if}"><a href="{$site_url}admin/listings/edit/{$data.id}/overview">{l i='filter_section_overview' gid='listings'}</a></li>
		<li class="{if $section_gid eq 'description'}active{/if}"><a href="{$site_url}admin/listings/edit/{$data.id}/description">{l i='filter_section_description' gid='listings'}</a></li>
		{if $data.operation_type eq 'sale' || $data.operation_type eq 'rent'}<li class="{if $section_gid eq 'gallery'}active{/if}"><a href="{$site_url}admin/listings/edit/{$data.id}/gallery">{l i='filter_section_gallery' gid='listings'}</a></li>{/if}
		{if $data.operation_type eq 'rent'}<li class="{if $section_gid eq 'calendar'}active{/if}"><a href="{$site_url}admin/listings/edit/{$data.id}/calendar">{l i='filter_section_calendar' gid='listings'}</a></li>{/if}
		{depends module=geomap}<li class="{if $section_gid eq 'map'}active{/if}"><a href="{$site_url}admin/listings/edit/{$data.id}/map">{l i='filter_section_map' gid='listings'}</a></li>{/depends}
	</ul>
	&nbsp;
</div>
{/if}
