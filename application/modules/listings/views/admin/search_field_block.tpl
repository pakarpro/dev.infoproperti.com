		{if $field.field.type eq 'select'}
			{assign var="field_name" value=$field.field_content.field_name}
			{l i='select_default' gid='start' assign='default_select_lang'}
			{switch from=$field.settings.search_type}
				{case value='one'}
					{switch from=$field.settings.view_type}
						{case value='select'}
							<select name="data[{$field.field.gid}]">
								{*if $field.field_content.settings_data_array.empty_option*}<option value="">...</option>{*/if*}
								{foreach item=item key=key from=$field.field_content.options.option}
								<option value="{$key|escape}" {if $key eq $data[$field_name]}selected{/if}>{$item}</option>
								{/foreach}
							</select>
						{case value='radio'}
							{foreach item=item key=key from=$field.field_content.options.option}
							<input type="radio" name="data[{$field.field.gid}]" value="{$key|escape}" id="{$field.field.gid}_select_{$key|escape}{$form_rand}" {if $data[$field_name]|is_array && $key|in_array:$data[$field_name]}checked{/if} /> <label for="{$field.field.gid}_select_{$key|escape}{$form_rand}">{$item}</label><br>
							{/foreach}
					{/switch}
				{case value='many'}
					{switch from=$field.settings.view_type}
						{case value='select'}
							<select name="data[{$field.field.gid}]" multiple>
								{*if $field.field_content.settings_data_array.empty_option*}<option value="">...</option>{*/if*}
								{foreach item=item key=key from=$field.field_content.options.option}
								<option value="{$key|escape}" {if $key eq $data[$field_name]}selected{/if}>{$item}</option>
								{/foreach}
							</select>
						{case value='radio'}
							<input type="checkbox" id="{$field.field.gid}_all{$form_rand}"> <label for="{$field.field.gid}_all{$form_rand}">{l i='select_all' gid='start'}</label><br>
							<input type="hidden" name="data[{$field.field.gid}][]" value="0">
							{foreach item=item key=key from=$field.field_content.options.option}
							<input type="checkbox" name="data[{$field.field.gid}][]" value="{$key|escape}" id="{$field.field.gid}_select_{$key|escape}{$form_rand}" {if $data[$field_name]|is_array && $key|in_array:$data[$field_name]}checked{/if} /> <label for="{$field.field.gid}_select_{$key|escape}{$form_rand}">{$item}</label><br>
							{/foreach}
							<script>{literal}
							$(function(){
								$('#{/literal}{$field.field.gid}_all{$form_rand}{literal}').bind('change', function(){
									if(this.checked){
										$(this).parent().find('input[name^=data]').attr('checked', 'checked');
									}else{
										$(this).parent().find('input[name^=data]').removeAttr('checked');
									}	
								});
							});
							{/literal}</script>
						{case value='slider'}
							{assign var='field_gid' value=$field.field.gid}
							<link rel="stylesheet" href="{$site_root}{$js_folder}jquery-ui/jquery-ui.custom.css" type="text/css" />
							<link rel="stylesheet" href="{$site_root}{$js_folder}slider/css/ui.slider.extras.css" type="text/css" />
							{js file='slider/jquery.ui.slider-'+$_LANG.rtl+'.js'}
							{js file='slider/selectToUISlider.jQuery.js'}
							<script>{literal}
								$(function(){				
									$({/literal}'select#{$field_gid}_min{$form_rand}, select#{$field_gid}_max{$form_rand}'{literal}).selectToUISlider({
										labels : 2,
										tooltip: false,
										tooltipSrc : 'text',
										labelSrc: 'text',
										isRTL: {/literal}{if $_LANG.rtl == 'rtl'}true{else}false{/if}{literal},
									});
								});
							{/literal}</script>
					
							{if $field.field_content.settings_data_array.empty_option}
								{assign var=min_value value=0}
							{else}
								{foreach item=item key=key from=$field.field_content.options.option}
									{assign var=min_value value=$key}
									{php}break;{/php}
								{/foreach}
							{/if}
					
							{foreach item=item key=key from=$field.field_content.options.option}
								{assign var=max_value value=$key}
							{/foreach}
					
							{if $data[$field_name].range.min}
								{assign var=current_min_value value=$data[$field_name].range.min}
							{else}
								{assign var=current_min_value value=$min_value}
							{/if}
					
							{if $data[$field_name].range.max}
								{assign var=current_max_value value=$data[$field_name].range.max}
							{else}
								{assign var=current_max_value value=$max_value}
							{/if}
					
							<div class="select-slider">
								<select id="{$field_gid}_min{$form_rand}" name="data[{$field_gid}][range][min]" class="hide">	
									{if $field.field_content.settings_data_array.empty_option}<option value="0">0</option>{/if}
									{foreach item=item key=key from=$field.field_content.options.option}
									<option value="{$key|escape}" {if $key eq $current_min_value}selected{/if}>{$item}</option>
									{/foreach}
								</select>
									
								<select id="{$field_gid}_max{$form_rand}" name="data[{$field_gid}][range][max]" class="hide">
									{if $field.field_content.settings_data.empty_option}<option value="0">0</option>{/if}
									{foreach item=item key=key from=$field.field_content.options.option}
									<option value="{$key|escape}" {if $key eq $current_max_value}selected{/if}>{$item}</option>
									{/foreach}
								</select>
				
								<div class="vals">
									<div id="{$field_gid}_min{$form_rand}_selected_val" class="fleft">{if $field.field_content.options.option[$current_min_value]}{$field.field_content.options.option[$current_min_value]}{else}0{/if}</div>
									<div id="{$field_gid}_max{$form_rand}_selected_val" class="fright">{if $field.field_content.options.option[$current_max_value]}{$field.field_content.options.option[$current_max_value]}{else}0{/if}</div>
								</div>
								<div class="clr"></div>
							</div>
					
							<input type="hidden" name="field_editor_range[{$field_gid}][min]" value="{$min_value|escape}">
							<input type="hidden" name="field_editor_range[{$field_gid}][max]" value="{$max_value|escape}">
					{/switch}
			{/switch}
		{elseif $field.field.type eq 'multiselect'}
			{assign var="field_name" value=$field.field_content.field_name}
			{l i='select_default' gid='start' assign='default_select_lang'}
			{foreach item=item key=key from=$field.field_content.options.option}
			<input type="checkbox" name="data[{$field.field.gid}][]" value="{$key|escape}" id="{$field.field.gid}_select_{$key|escape}{$form_rand}" {if $data[$field_name]|is_array && $key|in_array:$data[$field_name]}checked{/if} /> <label for="{$field.field.gid}_select_{$key|escape}{$form_rand}">{$item}</label>
			{/foreach}		
		{elseif $field.field.type eq 'text'}
			{assign var="field_name" value=$field.field_content.field_name}
			{if $field.settings.search_type eq 'number' && $field.settings.view_type eq 'range'}
				<input type="text" name="data[{$field.field.gid}][range][min]" class="short" value="{$data[$field_name].range.min|escape}"> &nbsp;-&nbsp;
				<input type="text" name="data[{$field.field.gid}][range][max]" class="short" value="{$data[$field_name].range.max|escape}">
			{elseif $field.settings.search_type eq 'number'}
				<input type="text" name="data[{$field.field.gid}]" class="short" value="{$data[$field_name]|escape}">
			{else}
				<input type="text" name="data[{$field.field.gid}]" value="{$data[$field_name]|escape}">
			{/if}
		{elseif $field.field.type eq 'textarea'}
			{assign var="field_name" value=$field.field_content.field_name}
			<input type="text" name="data[{$field.field.gid}]" value="{$data[$field_name]|escape}">
		{elseif $field.field.type eq 'checkbox'}	
			{assign var="field_name" value=$field.field_content.field_name}
			<input type="checkbox" name="data[{$field.field.gid}]" value="1" {if $data[$field_name]}checked{/if}>
		{/if}	
