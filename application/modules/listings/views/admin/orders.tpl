{include file="header.tpl" load_type='ui'}
{helper func_name=get_admin_level1_menu helper_name=menu func_param='admin_listings_menu'}
<div class="actions">
	&nbsp;
</div>

<div class="menu-level3">
	<ul>
		<li class="{if $status eq 'wait'}active{/if}{if !$status_data.wait} hide{/if}"><a href="{$site_url}admin/listings/orders/wait">{l i='booking_status_wait' gid='listings'} ({$status_data.wait})</a></li>
		<li class="{if $status eq 'approve'}active{/if}{if !$status_data.approve} hide{/if}"><a href="{$site_url}admin/listings/orders/approve">{l i='booking_status_approved' gid='listings'} ({$status_data.approve})</a></li>
		<li class="{if $status eq 'decline'}active{/if}{if !$status_data.decline} hide{/if}"><a href="{$site_url}admin/listings/orders/decline">{l i='booking_status_declined' gid='listings'} ({$status_data.decline})</a></li>		
	</ul>
	&nbsp;
</div>


<form method="post" action="" name="save_form" enctype="multipart/form-data">
<div class="filter-form">
	<div class="form">
			<div class="row">
				<div class="h">{l i='field_booking_listing' gid='listings'}:</div>
				<div class="v">{block name='listing_select' module='listings' selected=$filters.listings max=10 var_name='id_listing'}</div>
			</div>
			<div class="row">
				<div class="h">{l i='field_owner' gid='listings'}:</div>
				<div class="v">{user_select selected=$filters.owners max=1 var_name='id_owner'}</div>
			</div>
			<div class="row">
				<div class="h">{l i='field_booking_user' gid='listings'}:</div>
				<div class="v">{user_select selected=$filters.users max=1 var_name='id_user'}</div>
			</div>
			<div class="row">
				<div class="h">
					<input type="submit" name="filter-submit" value="{l i='header_filters' gid='listings' type='button'}">
					<input type="submit" name="filter-reset" value="{l i='header_reset' gid='listings' type='button'}">
				</div>
			</div>
	</div>
</div>
</form>


<form id="listings_form" action="" method="post">
<table cellspacing="0" cellpadding="0" class="data" width="100%">
<tr>
	<th class="w200">{l i='field_booking_listing' gid='listings'}</th>
	<th class="w200">{l i='field_booking_user' gid='listings'}</th>
	<th class="w100">{l i='field_booking_date_start' gid='listings'}</th>
	<th class="w100">{l i='field_booking_date_end' gid='listings'}</th>
	<th class="w150">&nbsp;</th>
</tr>
{foreach item=item from=$orders}
{counter print=false assign=counter}
<tr>
	<td>{$item.listing.output_name|truncate:100}</td>
	<td>{$item.user.output_name|truncate:100}</td>
	<td class="center">
		{switch from=$item.listing.price_period}
			{case value='1'}
				{$item.date_start|date_format:$page_data.date_format}
			{case value='2'}
				{ld_option i='month-names' gid='start' option=$item.date_start|date_format:'%m'}
				{$item.date_start|date_format:'%Y'}
		{/switch}
	</td>
	<td class="center">
		{switch from=$item.listing.price_period}
			{case value='1'}
				{$item.date_end|date_format:$page_data.date_format}
			{case value='2'}
				{ld_option i='month-names' gid='start' option=$item.date_end|date_format:'%m'}
				{$item.date_end|date_format:'%Y'}
		{/switch}
	</td>
	<td class="icons">
		{if $item.status eq 'wait'}
		<a href="{$site_url}admin/listings/order_approve/{$item.id}"><img src="{$site_root}{$img_folder}icon-approve.png" width="16" height="16" border="0" alt="{l i='link_order_approve' gid='listings' type='button'}" title="{l i='link_order_approve' gid='listings' type='button'}"></a>
		<a href="{$site_url}admin/listings/order_decline/{$item.id}"><img src="{$site_root}{$img_folder}icon-decline.png" width="16" height="16" border="0" alt="{l i='link_order_decline' gid='listings' type='button'}" title="{l i='link_order_decline' gid='listings' type='button'}"></a>
		{/if}
		<a href="{$site_url}admin/listings/order_delete/{$item.id}" onclick="javascript: if(!confirm('{l i='note_delete_listing' gid='listings' type='js'}')) return false;"><img src="{$site_root}{$img_folder}icon-delete.png" width="16" height="16" border="0" alt="{l i='link_delete_listing' gid='listings' type='button'}" title="{l i='link_delete_listing' gid='listings' type='button'}"></a>
	</td>
</tr>
{foreachelse}
<tr><td colspan="6" class="center">{l i='no_listings' gid='listings'}</td></tr>
{/foreach}
</table>
</form>
{include file="pagination.tpl"}

{js file='easyTooltip.min.js'}
<script>{literal}
var reload_link = "{/literal}{$site_url}admin/listings{literal}";
var order = '{/literal}{$order}{literal}';
var loading_content;
var order_direction = '{/literal}{$order_direction}{literal}';
$(function(){
	$(".tooltip").each(function(){
		$(this).easyTooltip({
			useElement: 'span_'+$(this).attr('id')
		});
	});
});
function reload_this_page(value){
	var link = reload_link + '/' + value + '/' + order + '/' + order_direction;
	location.href=link;
}

{/literal}</script>

{include file="footer.tpl"}
