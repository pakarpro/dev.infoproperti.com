	<div class="panorama-info-area">
		<div class="panorama-area">
			<img src="{$panorama.media.thumbs.200_200}" hspace="3" class="panorama" data-url="{$panorama.media.url}" data-file="{$panorama.media.url|str_replace:'':$panorama.media.thumbs.620_400}" data-width="{$panorama.settings.width}" data-height="{$panorama.settings.height}" data-comment="{$panorama.comment|escape}"><br>
			<b>{l i='panorama_status' gid='listings'}: {if $panorama.status}<font class="stat-active">{l i='panorama_active' gid='listings'}</font>{else}<font class="stat-moder">{l i='panorama_moderate' gid='listings'}</font>{/if}</b><br>
			{if $panorama.comment}{$panorama.comment|truncate:50:'...':true}<br>{/if}
		</div>
		<a href="#" onclick="javascript: vtourUpload.open_edit_form({$panorama.id}); return false;"><img src="{$site_root}{$img_folder}icon-edit.png" width="16" height="16" border="0" alt="{l i='btn_edit' gid='start' type='button'}" title="{l i='btn_edit' gid='start' type='button'}"></a>
		<a href="#" onclick="javascript: vtourUpload.delete_photo({$panorama.id}); return false;"><img src="{$site_root}{$img_folder}icon-delete.png" width="16" height="16" border="0" alt="{l i='btn_delete' gid='start' type='button'}" title="{l i='btn_delete' gid='start' type='button'}"></a>
		<div class="clr"></div>
	</div>
