	<div class="row">
		<div class="h">{l i='field_listing_type' gid='listings'}</div>
		<div class="v">
			<select name="filters[type]" id="operation_type">
				<option value="">...</option>
				{foreach item=item from=$operation_types}
				<option value="{$item|escape}" {if $item eq $data.type}selected{/if}>{l i='operation_search_'+$item gid='listings'}</option>
				{/foreach}
			</select>
		</div>
	</div>
	<div class="row">
		<div class="h">{l i='field_location' gid='listings'}:</div>
		<div class="v">{country_select select_type='city' id_country=$data.id_country id_region=$data.id_region id_city=$data.id_city var_country='filters[id_country]' var_region='filters[id_region]' var_city='filters[id_city]'}</div>	
	</div>	
	<div  class="row">
		<div class="h">{l i='field_category' gid='listings'}</div>
		<div class="v">
		{if $data.property_type > 0}{assign var='selected_category' value=$data.id_category+'_'+$data.property_type}{else}{assign var='selected_category' value=$data.id_category}{/if}
		{block name='properties_select' module='properties' var_name='filters[id_category]' js_var_name='id_category' selected=$selected_category cat_select=true}
		</div>
	</div>
	<div id="export_search_form">
		{$extend_search_form}
	</div>
	<div class="row">
		<div class="h">{l i='field_keyword' gid='listings'}:</div>
		<div class="v"><input type="text" name="filters[keyword]" value="{$data.keyword|escape}"></div>
	</div>
	<div class="row">
		<div class="h"><label for="status">{l i='field_status' gid='listings'}</label>:</div>
		<div class="v"><input type="checkbox" name="filters[status]" value="1" id="status" {if $data.status}checked=""{/if}></div>
	</div>
	<input type="hidden" name="form" value="admin_export_form">
		
	<script>{literal}
		$(function(){
			function get_extend_form(type, id_category){
				if(type && id_category != 0){
					$.ajax({
						url: '{/literal}{$site_url}admin/listings/ajax_get_export_extend_form/{$data.id}{literal}',
						data: {type: type, id_category: id_category},
						cache: false,
						type: 'post',
						success: function(data){
							$('#export_search_form').html(data);
							$('div.row').removeClass('zebra');
							$('div.row:odd').addClass('zebra');
						}
					});
				}else{
					$('#export_search_form').html('');
				}
			}
			$('#operation_type').bind('change', function(){
				var type = $(this).val();
				var id_category = $('#id_category').val();
				get_extend_form(type, id_category)
			});
			$('#id_category').bind('change', function(){
				var id_category = $(this).val();
				var type = $('#operation_type').val();
				get_extend_form(type, id_category)
			});
			$('div.row:odd').addClass('zebra');
		});
	{/literal}</script>
	
