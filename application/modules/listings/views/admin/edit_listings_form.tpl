{include file="header.tpl" load_type='ui'}
	
{include file="edit_listings_menu.tpl" module="listings"}

<form method="post" action="{$data.action}" name="save_form" id="save_form" enctype="multipart/form-data">
{* Overview section *}		
{if $section_gid eq 'overview'}
<div class="edit-form n150">
	<div class="row header">{if $data.id}{l i='admin_header_listings_edit' gid='listings'}{else}{l i='admin_header_listings_add' gid='listings'}{/if}</div>
	<div class="row">
		<div class="h">{l i='field_listing_type' gid='listings'}:{if !$data.id}&nbsp;*{/if}</div>
		<div class="v">
			{if $data.id}
				{$data.operation_type_str}
			{elseif $operation_types_count eq 1}
				{foreach item=item from=$operation_types}
				<input type="text" name="type" value="{$item|escape}">
				{$item}
				{/foreach}
			{else}
				{foreach item=item from=$operation_types}
				<input type="radio" name="type" value="{$item|escape}" id="for_{$item}" {if $data.operation_type eq $item}checked{/if}>
				<label for="for_{$item}">{l i='operation_type_'+$item gid='listings'}</label>
				{/foreach}
				<script>{literal}
					$(function(){
						$('input[name=type]').bind('change', function(){
							switch(this.value){
								case 'sale':
									$('#price_max').val('').hide();
									$('#square_max').val('').hide();
									$('#price_reduced_box').show();
									$('#sold_box').show();
									$('#price_negotiated_sale').show();
									$('#price_negotiated_buy').hide();
									$('#address_box').show();
									$('#services').show();
									$('#booking_period').hide();
									$('#price_period').val('');
									$('#price_type').val('');
								break;
								case 'buy':
									$('#price_max').show();
									$('#square_max').show();
									$('#price_reduced_box').hide();
									$('#price_reduced').val('');
									$('#sold_box').hide();
									$('#sold').removeAttr('checked');
									$('#price_negotiated_sale').hide();
									$('#price_negotiated_buy').show();
									$('#address_box').show();
									$('#address').val('');
									$('#services').hide().find('input[type=checkbox]').removeAttr('checked');
									$('#booking_period').hide();
									$('#price_period').val('');
									$('#price_type').val('');
								break;
								case 'rent':
									$('#price_max').val('').hide();
									$('#square_max').val('').hide();
									$('#price_reduced_box').show();
									$('#price_period').show();
									$('#price_type').show();
									$('#sold_box').hide();
									$('#sold').removeAttr('checked');
									$('#price_negotiated_sale').show();
									$('#price_negotiated_buy').hide();
									$('#address_box').show();
									$('#services').show();
									$('#booking_period').show();
								break;
								case 'lease':
									$('#price_max').val('').show();
									$('#square_max').val('').show();
									$('#price_reduced_box').hide();
									$('#price_reduced').val('');
									$('#price_period').show();
									$('#price_type').show();
									$('#sold_box').hide();
									$('#sold').removeAttr('checked');
									$('#price_negotiated_sale').hide();
									$('#price_negotiated_buy').show();
									$('#address_box').hide();
									$('#address').val('');
									$('#services').hide().find('input[type=checkbox]').removeAttr('checked');
									$('#booking_period').show();
								break;
							}
							$('div.row:visible').removeClass('zebra').parent().find('div.row:visible:odd').addClass('zebra');
						});
					});
				{/literal}</script>
			{/if}
		</div>
	</div>
	<div class="row">
		<div class="h">{l i='field_owner' gid='listings'}:&nbsp;*</div>
		<div class="v">{user_select selected=$data.id_user max=1 var_name='id_user'}&nbsp;</div>
	</div>
	<div class="row">
		<div class="h">{l i='field_location' gid='listings'}:&nbsp;* </div>
		<div class="v">{country_select select_type='city' id_country=$data.id_country id_region=$data.id_region id_city=$data.id_city}</div>
	</div>	
	<div class="row" id="address_box">
	<!-- <div class="row {if $data.operation_type ne 'sale' && $data.operation_type ne 'rent'}hide{/if}" id="address_box"> -->
		<div class="h">{l i='field_address' gid='listings'}: </div>
		<div class="v"><input type="text" name="data[address]" value="{$data.address|escape}" id="address"></div>
	</div>
	<div class="row">
		<div class="h">{l i='field_postal_code' gid='listings'}: </div>
		<div class="v"><input type="text" name="data[zip]" value="{$data.zip|escape}" /></div>
	</div>
	<div class="row">
		<div class="h">{l i='field_category' gid='listings'}:&nbsp;* </div>
		<div class="v">
			{assign var='selected_category' value=$data.id_category+'_'+$data.property_type}
			{if $data.id}
				{$data.category_str}, {$data.property_type_str}
				<div>
				    {block name='properties_select' module='properties' var_name='data[category]' selected=$selected_category id_category=$data.id_category category_str=$data.category_str}
				</div>
			{else}
				{assign var='selected_category' value=$data.id_category+'_'+$data.property_type}
				{block name='properties_select' module='properties' var_name='data[category]' selected=$selected_category cat_select=false js_var_name='category'}
			{/if}
		</div>
	</div>
	<div class="row {if $data.operation_type ne 'sale'}hide{/if}" id="sold_box">
		<div class="h">{l i='field_sold' gid='listings'}: </div>
		<div class="v">
			<input type="hidden" name="data[sold]" value="0">
			<input type="checkbox" name="data[sold]" value="1" {if $data.sold}checked{/if} id="sold">
		</div>
	</div>
	
	<div class="row {if $data.operation_type ne 'rent' && $data.operation_type ne 'lease'}hide{/if}" id="booking_period">
		<div class="h">{l i='field_booking_period' gid='listings'}:&nbsp;* </div>
		<div class="v">
			<!-- #MOD# -->
			
			{ld i='price_period' gid='listings' assign='price_period'}
			<select name="data[price_period]" class="middle {if $data.operation_type ne 'rent' && $data.operation_type ne 'lease'}hide{/if}" id="price_period">
			<option value="0">{$price_period.header}</option>
			{foreach item=item key=key from=$price_period.option}
			<option value="{$key}" {if $key eq $data.price_period}selected{/if}>{$item}</option>
			{/foreach}
			</select>
			
			{ld i='price_type' gid='listings' assign='price_type'}
			<select name="data[price_type]" class="middle {if $data.operation_type ne 'rent' && $data.operation_type ne 'lease'}hide{/if}" id="price_type">
			<option value="0">{$price_type.header}</option>
			{foreach item=item key=key from=$price_type.option}
			<option value="{$key}" {if $key eq $data.price_type}selected{/if}>{$item}</option>
			{/foreach}
			</select>
			
			<!-- #MOD# -->
		</div>
	</div>
	
	<div class="row">
		<div class="h">
			<span id="price_negotiated_buy"{if $data.operation_type ne 'buy'}class="hide"{/if}>{l i='field_price_negotiated_buy' gid='listings'}</span>
			<span id="price_negotiated_sale"{if $data.operation_type eq 'buy'}class="hide"{/if}>{l i='field_price_negotiated_sale' gid='listings'}</span>: </div>
		<div class="v">
			<input type="hidden" name="data[price_negotiated]" value="0">
			<input type="checkbox" name="data[price_negotiated]" value="1" id="price_negotiated" {if $data.price_negotiated}checked{/if}> 
		</div>
	</div>
	<div class="row">
		<div class="h">{l i='field_price' gid='listings'}:&nbsp;* </div>
		<div class="v">
			<input type="text" name="data[price]" value="{$data.price|escape}" id="price" class="middle" {if $data.price_negotiated}disabled{/if}> 
			<input type="text" name="data[price_max]" value="{$data.price_max|escape}" id="price_max" class="middle {if $data.operation_type ne 'buy'}hide{/if}" {if $data.price_negotiated}disabled{/if} /> 
			{depends module=payments}
			{if $data.id && ($data.operation_type eq 'rent' || $data.operation_type eq 'lease')}
				{$current_price_currency.abbr}
			{else}
			<select name="data[gid_currency]" id="price_unit" class="short" {if $data.price_negotiated}disabled{/if}>
				{foreach item=item from=$currencies}
				<option value="{$item.gid|escape}" {if $item.gid eq $data.gid_currency}selected{/if}>{$item.abbr}</option>
				{/foreach}
			</select>
			<script>{literal}
				$(function(){
					$('#price_unit').bind('click', function(){
						var text = $(this).find(':selected').html();
						$('#price_reduced_unit').html(text);
					});
				});
			{/literal}</script>
			{/if}
			{/depends}
		</div>
	</div>
	<div class="row {if $data.operation_type ne 'sale' && $data.operation_type ne 'rent'}hide{/if}" id="price_reduced_box">
		<div class="h">{l i='field_price_reduced' gid='listings'}: </div>
		<div class="v">
			<input type="text" name="data[price_reduced]" value="{$data.price_reduced|escape}" id="price_reduced" class="middle" {if $data.price_negotiated}disabled{/if}> 
			<span id="price_reduced_unit">{$current_price_currency.abbr}</span>
		</div>
	</div>
	<div class="row">
		<div class="h">{l i='field_price_auction' gid='listings'}: </div>
		<div class="v">
			<input type="hidden" name="data[price_auction]" value="0">
			<input type="checkbox" name="data[price_auction]" value="1" {if $data.price_auction}checked{/if}>
		</div>
	</div>
	<div class="row">
		<div class="h">{l i='field_square' gid='listings'}:&nbsp;* </div>
		<div class="v">
			<input type="text" name="data[square]" value="{$data.square|escape}" class="middle">
			<input type="text" name="data[square_max]" value="{$data.square_max|escape}" id="square_max" class="middle {if $data.operation_type ne 'buy'}hide{/if}">
			{ld i='square_units' gid='listings' assign='square_units'}
			<select name="data[square_unit]" class="short">
				{foreach item=item key=key from=$square_units.option}
				<option value="{$key|escape}" {if $key eq $data.square_unit}selected{/if}>{$item}</option>
				{foreachelse}
				<option value="">...</option>
				{/foreach}
			</select>
		</div>
	</div>
	<div class="row">
		<div class="h">{l i='field_date_open' gid='listings'}: </div>
		<div class="v">
			<input type="text" name="data[date_open]" value="{if $data.date_open|strtotime>0}{$data.date_open|date_format:$page_data.date_format|escape}{/if}" id="date_open">
			<input type="hidden" name="date_open_alt" value="" id="alt_date_open">
			{ld i='dayhour-names' gid='start' assign='dayhours'}
			{l i='text_from' gid='listings'}:
			<select name="data[date_open_begin]" class="middle"> 
				<option value="" {if !$data.date_open_begin}selected{/if}>{$dayhours.header}</option>
				{foreach item=item key=key from=$dayhours.option}
				<option value="{$key|escape}" {if $data.date_open_begin eq $key}selected{/if}>{$item}</option>
				{/foreach}
			</select>
			{l i='text_till' gid='listings'}:
			<select name="data[date_open_end]" class="middle"> 
				<option value="" {if !$data.date_open_end}selected{/if}>{$dayhours.header}</option>
				{foreach item=item key=key from=$dayhours.option}
				<option value="{$key|escape}" {if $data.date_open_end eq $key}selected{/if}>{$item}</option>
				{/foreach}
			</select>			
		</div>
	</div>
	<div class="row">
		<div class="h">{l i='field_date_available' gid='listings'}: </div>
		<div class="v">
			<input type="text" name="data[date_available]" value="{if $data.date_available|strtotime>0}{$data.date_available|date_format:$page_data.date_format|escape}{/if}" id="date_available"> 
			<input type="hidden" name="date_available_alt" value="" id="alt_date_available">
		</div>
	</div>
	<div class="row">
		<div class="h">{l i='field_wish_list' gid='listings'}: </div>
		<div class="v">{wish_list_select selected=$data.id_wish_lists var_name='id_wish_lists'}</div>
	</div>
	{if $use_services && $data.operation_type ne 'buy'}
	<div class="row" id="services">
		<div class="h">
			{l i='field_services' gid='listings'}: <br><br><br>
			<div class="select_actions">
				<a href="javascript:void(0)" id="services_select_all">{l i='select_all' gid='start'}</a>
				<a href="javascript:void(0)" id="services_unselect_all">{l i='unselect_all' gid='start'}</a>
			</div>
		</div>
		<div class="v">
			<div class="fleft">
			{if $use_listings_featured_service}
			<input type="hidden" name="services[featured]" value="0">
			<input type="checkbox" name="services[featured]" value="1" id="service_featured" {if $data.is_featured}checked{/if} class="{if $data.operation_type eq 'buy'}hide{/if} width6">
			<label for="service_featured" class="{if $data.operation_type eq 'buy'}hide{/if} width6" id="service_featured_label">{l i='field_services_featured' gid='listings'}</label>
			{/if}
		
			{if $use_listings_lift_up_service}
			<input type="hidden" name="services[lift_up]" value="0">
			<input type="checkbox" name="services[lift_up]" value="1" id="service_list_up" {if $data.is_lift_up}checked{/if} class="width6">
			<label for="service_lift_up" class="width6">{l i='field_services_lift_up' gid='listings'}</label>
			{/if}
			
			{if $use_listings_lift_up_country_service}
			<input type="hidden" name="services[lift_up_country]" value="0">
			<input type="checkbox" name="services[lift_up_country]" value="1" id="service_lift_up_country" {if $data.is_lift_up_country}checked{/if} class="width6">
			<label for="service_lift_up_country" class="width6">{l i='field_services_lift_up_country' gid='listings'}</label>
			{/if}
			
			{if $use_listings_lift_up_region_service}
			<input type="hidden" name="services[lift_up_region]" value="0">
			<input type="checkbox" name="services[lift_up_region]" value="1" id="service_lift_up_region" {if $data.is_lift_up_region}checked{/if} class="width6">
			<label for="service_lift_up_region" class="width6">{l i='field_services_lift_up_region' gid='listings'}</label>
			{/if}
			
			{if $use_listings_lift_up_city_service}
			<input type="hidden" name="services[lift_up_city]" value="0">
			<input type="checkbox" name="services[lift_up_city]" value="1" id="service_lift_up_city" {if $data.is_lift_up_city}checked{/if} class="width6">
			<label for="service_lift_up_city" class="width6">{l i='field_services_lift_up_city' gid='listings'}</label>
			{/if}
			
			{if $use_listings_highlight_service}
			<input type="hidden" name="services[highlight]" value="0">
			<input type="checkbox" name="services[highlight]" value="1" id="service_highlight" {if $data.is_highlight}checked{/if} class="width6">
			<label for="service_highlight" class="width6">{l i='field_services_highlight' gid='listings'}</label>
			{/if}
			
			{if $use_listings_slide_show_service}
			<input type="hidden" name="services[slide_show]" value="0">
			<input type="checkbox" name="services[slide_show]" value="1" id="service_slide_show" {if $data.is_slide_show}checked{/if} class="{if $data.operation_type eq 'buy'}hide{/if} width6">
			<label for="service_slide_show" class="{if $data.operation_type eq 'buy'}hide{/if} width6" id="service_slide_show_label">{l i='field_services_slide_show' gid='listings'}</label>
			{/if}
			</div>
			<script>{literal}
			$(function(){
				$('#services_select_all').bind('click', function(){
					$('input[name^=services]').attr('checked', 'checked');
				});
				$('#services_unselect_all').bind('click', function(){
					$('input[name^=services]').removeAttr('checked');
				});
			});
			{/literal}</script>
		</div>
	</div>	
	<div class="row">
		<div class="h">{l i='field_headline' gid='listings'}: </div>
		<div class="v">
			<textarea name="data[headline]" rows="10" cols="80">{$data.headline|escape}</textarea>
		</div>
	</div>
	{/if}
</div>
<div class="btn"><div class="l"><input type="submit" name="btn_save_overview" value="{l i='btn_save' gid='start' type='button'}"></div></div>
<a class="cancel" href="{$site_url}admin/listings">{l i='btn_cancel' gid='start'}</a>
<input type="hidden" name="data[lat]" value="{$data.lat|escape}" id="lat">
<input type="hidden" name="data[lon]" value="{$data.lon|escape}" id="lon">
{block name=geomap_load_geocoder module='geomap'}
<script>{literal}
	function update_coordinates(country, region, city, address, postal_code){
		if(typeof(geocoder) != 'undefined'){
			var location = geocoder.getLocationFromAddress(country, region, city, address, postal_code);
			geocoder.geocodeLocation(location, function(latitude, longitude){
				$('#lat').val(latitude);
				$('#lon').val(longitude);
			});	
		}
	}
	$(function(){
		var location_change_wait = 0;
		var country_old = '{/literal}{$data.id_country}{literal}';
		var region_old = '{/literal}{$data.id_region}{literal}';
		var city_old = '{/literal}{$data.id_city}{literal}';
		var address_old = '{/literal}{'\''|str_replace:'\\\'':$data.address}{literal}';
		var postal_code_old = '{/literal}{"'"|str_replace:"\'":$data.zip}{literal}';
		
		$('input[name=id_city]').bind('change', function(){
			var city = $(this).val();
			if(city == 0) return;
			location_change_wait++;
			check_address_updated();
		});
		
		$('input[name=data\\[address\\]], input[name=data\\[zip\\]]').bind('keypress', function(){
			location_change_wait++;
			setTimeout(check_address_updated, 1000);
		});
		
		function check_address_updated(){
			location_change_wait--;
			if(location_change_wait) return;
			var country = $('input[name=id_country]').val();
			var region = $('input[name=id_region]').val();
			var city = $('input[name=id_city]').val();
			var address = $('input[name=data\\[address\\]]').val();
			var postal_code = $('input[name=data\\[zip\\]]').val();
			if(country == country_old && region == region_old && 
				city == city_old && address == address_old && postal_code == postal_code_old) return;
			country_old = country;
			region_old = region;
			city_old = city;
			address_old = address;
			postal_code_old = postal_code;
			var country_name = $('input[name=id_country]').attr('data-name');
			var region_name = $('input[name=id_region]').attr('data-name');
			var city_name = $('input[name=id_city]').attr('data-name');
			update_coordinates(country_name, region_name, city_name, address, postal_code);
		}
		
		$('#price_negotiated').bind('click', function(){
			if(this.checked){
				$('#price').attr('disabled', 'disabled');
				$('#price_max').attr('disabled', 'disabled');
				$('#price_reduced').attr('disabled', 'disabled');
				$('#price_unit').attr('disabled', 'disabled');
			}else{
				$('#price').removeAttr('disabled');
				$('#price_max').removeAttr('disabled');
				$('#price_reduced').removeAttr('disabled');
				$('#price_unit').removeAttr('disabled');
			}
		});
		$('#date_open').datepicker({dateFormat: '{/literal}{$page_data.datepicker_date_format}{literal}', altFormat: '{/literal}{$page_data.datepicker_alt_format}{literal}', altField: '#alt_date_open'});
		$('#date_available').datepicker({dateFormat: '{/literal}{$page_data.datepicker_date_format}{literal}', altFormat: '{/literal}{$page_data.datepicker_alt_format}{literal}', altField: '#alt_date_available'});
	});
{/literal}</script>
{* Description section *}		
{elseif $section_gid eq 'description'}
<div class="edit-form n150">
	<div class="row header">{if $data.id}{l i='admin_header_listings_edit' gid='listings'}{else}{l i='admin_header_listings_add' gid='listings'}{/if}</div>
	{foreach item=section_item  key=section_key from=$sections_data}
	{foreach item=item from=$fields_data}
	{if $item.section_gid eq $section_item.gid}
	<div class="row">
		<div class="h">
			{$item.name}: {if $item.settings_data_array.min_char > 0}{l i='text_min_char' gid='listings'}&nbsp;<b>{$item.settings_data_array.min_char}</b>{/if} {if $item.settings_data_array.max_char > 0}{l i='text_max_char' gid='listings'}&nbsp;<b>{$item.settings_data_array.max_char}</b>{/if}
			{if $item.field_type eq 'multiselect'}<br><br>
			<div class="select_actions">
				<a href="javascript:void(0)" id="{$item.field_name}_select_all">{l i='select_all' gid='start'}</a> |
				<a href="javascript:void(0)" id="{$item.field_name}_unselect_all">{l i='unselect_all' gid='start'}</a>
			</div>
			{/if}
		</div>
		<div class="v">
		{if $item.field_type eq 'select'}
			{if $item.settings_data_array.view_type eq 'select'}
			<select name="{$item.field_name}">
			{if $item.settings_data_array.empty_option}<option value="0"{if $value eq 0} selected{/if}>...</option>{/if}
			{foreach item=option key=value from=$item.options.option}<option value="{$value|escape}" {if $value eq $item.value}selected{/if}>{$option}</option>{/foreach}
			</select>
			{else}
			{if $item.settings_data_array.empty_option}<input type="radio" name="{$item.field_name}" value="0" {if $value eq 0}checked{/if} id="{$item.field_name}_0"><label for="{$item.field_name}_0">No select</label><br>{/if}
			{foreach item=option key=value from=$item.options.option}<input type="radio" name="{$item.field_name}" value="{$value|escape}" {if $value eq $item.value}checked{/if} id="{$item.field_name}_{$value|escape}"><label for="{$item.field_name}_{$value|escape}">{$option}</label><br>{/foreach}
			{/if}
		{elseif $item.field_type eq 'multiselect'}
			<div class="fleft">
			{foreach item=option key=value from=$item.options.option}
			<input type="checkbox" name="{$item.field_name}[]" value="{$value|escape}" {if $value|in_array:$item.value}checked{/if} id="{$item.field_name}_{$value|escape}" class="width4"><label for="{$item.field_name}_{$value|escape}" class="width4">{$option}</label>
			{/foreach}
			<input type="hidden" name="{$item.field_name}[]" value="0">
			</div>
			<script>{literal}
			$(function(){
				$('#{/literal}{$item.field_name}_select_all{literal}').bind('click', function(){
					$('input[name^={/literal}{$item.field_name}{literal}]').attr('checked', 'checked');
				});
				$('#{/literal}{$item.field_name}_unselect_all{literal}').bind('click', function(){
					$('input[name^={/literal}{$item.field_name}{literal}]').removeAttr('checked');
				});
			});
			{/literal}</script>
		{elseif $item.field_type eq 'text'}
			<input type="text" name="{$item.field_name}" value="{$item.value|escape}" maxlength="{$item.settings_data_array.max_char|escape}" {if $item.settings_data_array.max_char < 11}class="short"{elseif $item.settings_data_array.max_char > 1100}class="long"{/if}>
		{elseif $item.field_type eq 'textarea'}
			<textarea name="{$item.field_name}" rows="10" cols="80">{$item.value|escape}</textarea>
		{elseif $item.field_type eq 'checkbox'}
			<input type="checkbox" name="{$item.field_name}" value="1" {if $item.value eq '1'}checked{/if}>
		{/if}
		&nbsp;
			{if $item.comment}<p class="help">{$item.comment}</p>{/if}
		</div>
	</div>
	{/if}
	{/foreach}
	{/foreach}
</div>
<div class="btn"><div class="l"><input type="submit" name="btn_save_description" value="{l i='btn_save' gid='start' type='button'}"></div></div>
<a class="cancel" href="{$site_url}admin/listings">{l i='btn_cancel' gid='start'}</a>
{* Gallery section *}
{elseif $section_gid eq 'gallery' && ($data.operation_type eq 'sale' || $data.operation_type eq 'rent')}
<div class="edit-form n150">
	<div class="row header">{if $data.id}{l i='admin_header_listings_edit' gid='listings'}{else}{l i='admin_header_listings_add' gid='listings'}{/if}</div>
	{js file='ajaxfileupload.min.js'}
	{js file='gallery-uploads.js'}
	<script>{literal}
		var gUpload;
		$(function(){
			gUpload = new galleryUploads({
				siteUrl: '{/literal}{$site_url}{literal}',
				objectId: '{/literal}{$data.id}{literal}',
				maxPhotos: '{/literal}{$data.max_photos}{literal}',
				maxPhotoError: '{/literal}{l i='error_max_photos_reached' gid='listings' type='js'}{literal}',
				uploadPhotoUrl: 'admin/listings/photo_upload/{/literal}{$data.id}{literal}/',
				savePhotoDataUrl: 'admin/listings/ajax_save_photo_data/{/literal}{$data.id}{literal}/',
				saveSortingUrl: 'admin/listings/ajax_save_photo_sorting/{/literal}{$data.id}{literal}',
				deletePhotoUrl: 'admin/listings/ajax_delete_photo/{/literal}{$data.id}{literal}/',
				formPhotoUrl: 'admin/listings/ajax_photo_form/{/literal}{$data.id}{literal}/',
				reloadBlockUrl: 'admin/listings/ajax_reload_photo_block/{/literal}{$data.id}{literal}/'
			});
		});
	{/literal}
	</script>
	<div class="row">
		<div class="upload_block">
			<p>
				{if $gallery_settings.max_items_count}{l i='max_photo_header' gid='listings'}: <b>{$gallery_settings.max_items_count}</b><br>{/if}
				{if $gallery_settings.upload_settings.max_size_str}{l i='max_photo_size_header' gid='listings'}: <b>{$gallery_settings.upload_settings.max_size_str}</b><br>{/if}
				{if $gallery_settings.upload_settings.min_width && $gallery_settings.upload_settings.min_height}{l i='min_photo_width_and_height_header' gid='listings'}: <b>{$gallery_settings.upload_settings.min_width}x{$gallery_settings.upload_settings.min_height} {l i='photo_width_and_height_unit' gid='listings'}</b><br>{/if}
				{if $gallery_settings.upload_settings.file_formats}{l i='text_accepted_file_types' gid='listings'}: <b>{', '|implode:$gallery_settings.upload_settings.file_formats}</b><br>{/if}
			</p>
		
			<p>{l i='photo_upload_header' gid='listings'}</p>
					
			{capture assign="uploader_callback"}{literal}
				function(name, data){
					if(typeof(data.id) != 'undefined' && data.id > 0){
						gUpload.save_photo_data(data.id);
					}
				}
			{/literal}{/capture}
			{block name='uploader_block' module='uploads' field_name='photo_file' form_id='save_form' url='admin/listings/photo_upload/'+$data.id callback=$uploader_callback}
					
			<div class="clr"></div>
		</div>
			
		<ol class="blocks" id="sortList">
			{foreach item=photo from=$data.photos}
			<li id="pItem{$photo.id}">
			{include file="photo_view_block.tpl" module="listings"}
			</li>
			{/foreach}
		</ol>
		<div class="clr"></div>			
	</div>
</div>	
<div class="btn"><div class="l"><input type="submit" name="btn_save_photo" value="{l i='btn_save' gid='start' type='button'}"></div></div>
<a class="cancel" href="{$site_url}admin/listings">{l i='btn_cancel' gid='start'}</a>
<div class="clr"></div>
<div class="edit-form n150">
	<div class="row header">{l i='header_listing_virtual_tour' gid='listings'}</div>
	<div class="row">
		<div class="upload_block">
			<p>
				{if $vtour_settings.max_items_count}{l i='max_panorama_header' gid='listings'}: <b>{$vtour_settings.max_items_count}</b><br>{/if}
				{if $vtour_settings.upload_settings.max_size_str}{l i='max_panorama_size_header' gid='listings'}: <b>{$vtour_settings.upload_settings.max_size_str}</b><br>{/if}
				{if $vtour_settings.upload_settings.min_width && $vtour_settings.upload_settings.min_height}{l i='min_panorama_width_and_height_header' gid='listings'}: <b>{$vtour_settings.upload_settings.min_width}x{$vtour_settings.upload_settings.min_height} {l i='photo_width_and_height_unit' gid='listings'}</b><br>{/if}
				{if $vtour_settings.upload_settings.file_formats}{l i='text_accepted_file_types' gid='listings'}: <b>{', '|implode:$vtour_settings.upload_settings.file_formats}</b><br>{/if}
			</p>
		
			<p>{l i='panorama_upload_header' gid='listings'}</p>
					
			{capture assign="uploader_callback"}{literal}
				function(name, data){
					if(typeof(data.id) != 'undefined' && data.id > 0){
					vtourUpload.save_photo_data(data.id);
					$.get('{/literal}{$site_url}{literal}admin/listings/ajax_reload_panorama/{/literal}{$data.id}{literal}/'+data.id, {}, function(data){
						$('#panorama_block').html(data);
					});
				}
			}
			{/literal}{/capture}
			{block name='uploader_block' module='uploads' field_name='panorama_file' form_id='save_form' url='admin/listings/panorama_upload/'+$data.id callback=$uploader_callback}
					
			<div class="clr"></div>
		</div>
		
		<ol class="blocks" id="vtourSortList">
			{foreach item=panorama from=$data.virtual_tour}
			<li id="pItem{$panorama.id}">
			{include file="panorama_view_block.tpl" module="listings"}
			</li>
			{/foreach}
		</ol>
		<div class="clr"></div>		
		<div id="panorama_block">{if $data.virtual_tour_count}{block name=virtual_tour_block module=listings data=$data.virtual_tour.0}{/if}</div>
		<script>{literal}
			var vtourUpload;
			$(function(){
				vtourUpload = new galleryUploads({
					siteUrl: '{/literal}{$site_url}{literal}',
					objectId: '{/literal}{$data.id}{literal}',
					fileElementId: 'panorama_file',
					commentElementId: 'panorama_comment',
					maxPhotos: '{/literal}{$data.max_panorama}{literal}',
					maxPhotoError: '{/literal}{l i='error_max_panorama_reached' gid='listings' type='js'}{literal}',
					uploadPhotoUrl: 'admin/listings/panorama_upload/{/literal}{$data.id}{literal}/',
					savePhotoDataUrl: 'admin/listings/ajax_save_panorama_data/{/literal}{$data.id}{literal}/',
					saveSortingUrl: 'admin/listings/ajax_save_panorama_sorting/{/literal}{$data.id}{literal}',
					deletePhotoUrl: 'admin/listings/ajax_delete_panorama/{/literal}{$data.id}{literal}/',
					formPhotoUrl: 'admin/listings/ajax_panorama_form/{/literal}{$data.id}{literal}/',
					reloadBlockUrl: 'admin/listings/ajax_reload_panorama_block/{/literal}{$data.id}{literal}/',
					listItemID: 'vtourSortList',
					reloadCallback: function(id){
						$.get('{/literal}{$site_url}{literal}admin/listings/ajax_reload_panorama/{/literal}{$data.id}{literal}/'+id, {}, function(data){
							$('#panorama_block').html(data);
						});
					},
					deleteCallback: function(id){
						$('#panorama_block').html('');
					},
				});
			});
		{/literal}</script>
	</div>		
</div>
<div class="btn"><div class="l"><input type="submit" name="btn_save_virtual_tour" value="{l i='btn_save' gid='start' type='button'}"></div></div>
<a class="cancel" href="{$site_url}admin/listings">{l i='btn_cancel' gid='start'}</a>
<div class="clr"></div>
{depends module=file_uploads}
<div class="edit-form n150">
	<div class="row header">{l i='admin_header_upload_file' gid='listings'}</div>
	<div class="row">
		<div class="h">{l i='field_listing_file' gid='listings'}:&nbsp;* </div>
		<div class="v">
			<p>
				{l i='max_file_header' gid='listings'}: <b>1</b><br>
				{if $file_settings.max_size_str}{l i='max_file_size_header' gid='listings'}: <b>{$file_settings.max_size_str}</b><br>{/if}
				{if $file_settings.file_formats}{l i='text_accepted_file_types' gid='listings'}: <b>{', '|implode:$file_settings.file_formats}</b><br>{/if}
			</p>
			<input type="file" name="listing_file">
			{if $data.listing_file}
			<br><br><a href="{$data.listing_file_content.file_url|escape}" target="blank">{l i='field_file_download' gid='listings'}</a>
			<br><br><input type="checkbox" name="listing_file_delete" value="1" id="uichb"><label for="uichb">{l i='field_file_delete' gid='listings'}</label><br>
			{/if}
		</div>
	</div>
	<div class="row">
		<div class="h">{l i='field_listing_file_name' gid='listings'}:&nbsp;* </div>
		<div class="v">
			<input type="text" name="data[listing_file_name]" value="{$data.listing_file_name|escape}">
		</div>
	</div>
	<div class="row">
		<div class="h">{l i='field_listing_file_comment' gid='listings'}: </div>
		<div class="v">
			<textarea name="data[listing_file_comment]" rows="10" cols="80">{$data.listing_file_comment|escape}</textarea>
		</div>
	</div>
</div>
<div class="btn"><div class="l"><input type="submit" name="btn_save_file" value="{l i='btn_save' gid='start' type='button'}"></div></div>
<a class="cancel" href="{$site_url}admin/listings">{l i='btn_cancel' gid='start'}</a>
<div class="clr"></div>
{/depends}
{depends module=video_uploads}
<div class="edit-form n150">
	<div class="row header">{l i='admin_header_upload_video' gid='listings'}</div>
	<div class="row">
		<div class="h">{l i='field_video' gid='listings'}:&nbsp;* </div>
		<div class="v">
			<p>
				{l i='max_video_header' gid='listings'}: <b>1</b><br>
				{if $video_settings.max_size_str}{l i='max_video_size_header' gid='listings'}: <b>{$video_settings.max_size_str}</b><br>{/if}
				{if $video_settings.file_formats}{l i='text_accepted_file_types' gid='listings'}: <b>{', '|implode:$video_settings.file_formats}</b><br>{/if}
			</p>
			<input type="file" name="listing_video">
			{if $data.listing_video}
				<br><br>{l i='field_video_status' gid='listings'}:
				{if $data.listing_video_data.status eq 'end' && $data.listing_video_data.errors}	<font color="red">{foreach item=item from=$data.listing_video_data.errors}{$item}<br>{/foreach}</font>
				{elseif $data.listing_video_data.status eq 'end'}	<font color="green">{l i='field_video_status_end' gid='listings'}</font><br>
				{elseif $data.listing_video_data.status eq 'images'} <font color="yellow">{l i='field_video_status_images' gid='listings'}</font><br>
				{elseif $data.listing_video_data.status eq 'waiting'} <font color="yellow">{l i='field_video_status_waiting' gid='listings'}</font><br>
				{elseif $data.listing_video_data.status eq 'start'} <font color="yellow">{l i='field_video_status_start' gid='listings'}</font><br>
				{/if}
				{if $data.listing_video_content.embed}
				<br>{$data.listing_video_content.embed}
				{/if}
				<br><input type="checkbox" name="listing_video_delete" value="1" id="uvchb"><label for="uvchb">{l i='field_video_delete' gid='listings'}</label>
			{/if}
		</div>
	</div>
</div>
<div class="btn"><div class="l"><input type="submit" name="btn_save_video" value="{l i='btn_save' gid='start' type='button'}"></div></div>
<a class="cancel" href="{$site_url}admin/listings">{l i='btn_cancel' gid='start'}</a>
{/depends}
{* Map section *}		
{elseif $section_gid eq 'map'}
{depends module=geomap}
<script>{literal}
	if(typeof(get_listing_type_data) == 'undefined'){
		function get_listing_type_data(type){
			$('#map_type').val(type);
		}
	}
	if(typeof(get_listing_zoom_data) == 'undefined'){
		function get_listing_zoom_data(zoom){
			$('#map_zoom').val(zoom);
		}
	}
	if(typeof(get_listing_drag_data) == 'undefined'){
		function get_listing_drag_data(point_gid, lat, lon){
			$('#lat').val(lat);
			$('#lon').val(lon);
		}
	}
{/literal}</script>
<div class="edit-form n150">
	<div class="row header">{if $data.id}{l i='admin_header_listings_edit' gid='listings'}{else}{l i='admin_header_listings_add' gid='listings'}{/if}</div>
	<div class="row">{block name=show_default_map module=geomap gid='listing_view' object_id=$data.id markers=$markers settings=$map_settings width='630' height='400'}</div>
</div>
<div class="btn"><div class="l"><input type="submit" name="btn_save" value="{l i='btn_save' gid='start' type='button'}"></div></div>
<a class="cancel" href="{$site_url}admin/listings">{l i='btn_cancel' gid='start'}</a>
<input type="hidden" name="map[view_type]" value="{$listing_map_settings.view_type}" id="map_type">
<input type="hidden" name="map[zoom]" value="{$listing_map_settings.zoom}" id="map_zoom">
<input type="hidden" name="data[lat]" value="{$data.lat|escape}" id="lat">
<input type="hidden" name="data[lon]" value="{$data.lon|escape}" id="lon">
{/depends}
{* Calendar section *}
{elseif $section_gid eq 'calendar' && $data.operation_type eq 'rent'}
<div class="edit-form n150">
	<div class="row header">{if $data.id}{l i='admin_header_listings_edit' gid='listings'}{else}{l i='admin_header_listings_add' gid='listings'}{/if}</div>		
	<div class="row">
		<div class="h">{l i='field_calendar_enabled' gid='listings'}:</div>
		<div class="v">
			<input type="hidden" name="data[use_calendar]" value="0">
			<input type="checkbox" name="data[use_calendar]" {if $data.use_calendar}checked{/if}>
		</div>
	</div>
	<div class="row">
		<div class="h">{l i='field_calendar_period_min' gid='listings'}:</div>
		<div class="v">
			<input type="text" name="data[calendar_period_min]" value="{$data.calendar_period_min|escape}" class="middle">
		</div>
	</div>
	<div class="row">
		<div class="h">{l i='field_calendar_period_max' gid='listings'}:</div>
		<div class="v">
			<input type="text" name="data[calendar_period_max]" value="{$data.calendar_period_max|escape}" class="middle">
		</div>
	</div>	
</div>
<div class="btn"><div class="l"><input type="submit" name="btn_save_file" value="{l i='btn_save' gid='start' type='button'}"></div></div>
<a class="cancel" href="{$site_url}admin/listings">{l i='btn_cancel' gid='start'}</a>
<div class="clr"></div>
{block name='listings_calendar_block' module='listings' listing=$data template='edit' count=2}
{/if}
<input type="hidden" name="btn_save" value="1">
</form>
<div class="clr"></div>
<script>{literal}
var on_moderation = '{/literal}{$on_moderation}{literal}';
$(function(){
	$("div.row:visible:odd").addClass("zebra");
	if(on_moderation == 1){
		$('#save_form input, #save_form textarea, #save_form select').attr('disabled', 'disabled');
	}
});
{/literal}</script>

{include file="footer.tpl"}
