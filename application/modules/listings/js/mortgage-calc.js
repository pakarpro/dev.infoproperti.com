function mortgageCalc(optionArr)
{	
	this.properties = {
		siteUrl: '',
		loadUrl: 'listings/ajax_mortgage_calc', <!-- ajax_mortgage_calc is a function in listings controller that displays some messages (include information, error msg, etc)
		defaultRight: 0,
		selectedCurrAbbr: '',
		langs: {},
		contentObj: new loadingContent({loadBlockWidth: '584px', loadBlockID: 'mortgage_calc', closeBtnClass: 'load_content_close', closeBtnPadding: 5}),
		errorObj: new Errors,		
	};

	var _self = this;
	
<!------Giving function to these button ------------------------------------------------------------------------------------------------------------------------->
	this.Init = function(options)
	{
		_self.properties = $.extend(_self.properties, options);
		
		var url = _self.properties.siteUrl + _self.properties.loadUrl;
		$.getJSON(url, {}, function(data)
		{
			for(var i in data)
			{
				_self.properties[i] = data[i];
			}
			
		<!--Function of Calculate Button ---------------------------------------------------------------------------->
			$('#calc_btn').bind('click', function()
			{
				_self.calc(this.form);
			});
		
		<!--Function of Reset Button -------------------------------------------------------------------------------->
			$('#empty_btn').bind('click', function()
			{
				_self.empty(this.form);
			});
		
		<!--Function of Print Button -------------------------------------------------------------------------------->
			$('#results_btn').bind('click', function()
			{
				_self.results(this.form);
			});
		});
	}
	
<!------Get and Showing results -------------------------------------------------------------------------------------------------------------------------------->
	this.results = function(form)
	{
		var aPrin = form.principal.value; 			// Property Price
		var aIntRate = form.intRate.value; 			// Interest, eg. 10
		var depositRp = form.deposit_value_rp.value;				// DP, by default 30%
		var depositPercent = form.deposit_value_percentage.value;	// DP, in Rupiah. User can change the value
		var propertiVal = form.properti_value.value;				// Real Property price
		if(aIntRate > 1) aIntRate = aIntRate/100; 	<!-- If the interest is more than 1, the interest will be divided 100
		aIntRate = aIntRate / 12; 					<!-- Then, the result will be divided 12
		var aNPer = form.numYears.value *12; 		// Payments (in Month), eg. 10 years = 120 months
		if(form.HmoPmt.value == 0 || form.HmoPmt.value == '')
		{
		<!--Alert 1 : Please compute the payment before creating the schedule --------------------------------------->
			_self.properties.errorObj.show_error_block(_self.properties.alert1, 'error'); 
		}
		else
		{
			
		<!--Declaration of variables -------------------------------------------------------------------------------->
			var aPmt = form.HmoPmt.value;			// Used to get payment amount result after calculating the KPR simulation
			var aIntPort = 0;						// Used for Interest Value in Payments Schedule
			var aAccumInt = 0;						// Used for Accumulation of Interest Value in Payments Schedule
			var aPrinPort = 0;						// Used for Principal Value in Payments Schedule
			var aAccumPrin = 0;						// Used for Accumulation of Principal Value in Payments Schedule
			var aCount = 0;							// Used for Counting total month to Pay the Payment, by default it should not be larger than 600
			var aPmtRow = "";						// Used to Display Detail Information of Payments Schedule
			var aPmtNum = 0; 						// Used for Pmt# in Payments Schedule (Numbering)
			
		<!--Date and time are here ---------------------------------------------------------------------------------->
			var today = new Date();					// Thu Nov 06 2014 16:05:16 GMT+0700 (SE Asia Standard Time)
			var dayFactor = today.getTime();		// Result eg. 14152647-51369, 14152647-61245
			var pmtDay = today.getDate();			// The Result current day, eg. 6
			var loanMM = today.getMonth() + 1;		// The result, current month + 1, eg. Oct = 10 + 1, will be 11 (means Nov)
			var loanYY = today.getFullYear();		// The result, current year, eg. 2014
			var loanDate = (pmtDay + "/" + loanMM + "/" + loanYY); // 6/11/2014

			// 86400000 means 1 day
			// 30,4 means average of days for each month in 1 year
			var monthMS = 86400000 * 30.4;			// The result is 2626560000
			var pmtDate = 0;

		<!--Loop and display the Schedule of Payment ---------------------------------------------------------------->
			while(aCount < aNPer) // ( 0 < 120 )
			{
				aIntPort = aPrin * aIntRate;						// Property Price * Interest, eg. 5000000000 * 0.008333333
				aAccumInt = eval(aAccumInt) + eval(aIntPort);		// Accum. of Interest + Interest Value, eg. 0 + 41666666,66
				aPrinPort = eval(aPmt) - eval(aIntPort);			// Principal Value (should be paid for each month), eg. 66075368.44 - 41666666,66
				
				if(aPrinPort > 1000000000001)						// if the Principal Value more than 1 Trillion, display error msg
				{
					_self.properties.errorObj.show_error_block(_self.properties.alert3, 'error');
					focus(); return false;
				}
				
				aAccumPrin = eval(aAccumPrin) + eval(aPrinPort);	// Accum. of Principal + Principal Value, eg. 0 + 24408701,77
				aPrin = eval(aPrin) - eval(aPrinPort);				// Property Price - Principal Value, eg. 5000000000 - 24408701,77
				aCount = eval(aCount) + eval(1);					// eg. 0 + 1
				aPmtNum = eval(aPmtNum) + eval(1); 					// Numbering, eg. 0 + 1
				dayFactor = eval(dayFactor) + eval(monthMS);		// 
				
		<!--Variables to be used in Schedule of Payments ------------------------------------------------------------>
				pmtDate = new Date(dayFactor); 							// Get the complete format date
				pmtMonth = pmtDate.getMonth();							// Split date and get the month, if now is November, the result should be 10 (means October)
				pmtMonth = pmtMonth + 1;								// So, we have to increase 1 month
				pmtYear = pmtDate.getFullYear();						// split date and Get the year
				pmtString = (pmtDay + '/' + pmtMonth + '/' + pmtYear);	// Format Date => day-month-year
				
		<!--Detail Values of Payment Schedule ----------------------------------------------------------------------->
				aPmtRow = (aPmtRow + 
							'<tr>' +
								'<td align="'+_self.properties.defaultRight+'">' + aPmtNum + '</td>' + 						// Pmt # or numbering
								'<td align="'+_self.properties.defaultRight+'">' + pmtString + '</td>' +  					// Loan Date
								'<td align="'+_self.properties.defaultRight+'">' + _self.RightDig(aPrinPort) + '</td>' +	// Principal
								'<td align="'+_self.properties.defaultRight+'">' + _self.RightDig(aIntPort) + '</td>' +		// Interest
								'<td align="'+_self.properties.defaultRight+'">' + _self.RightDig(aPrin) + '</td>' +		// Balance
							'</tr>'
						  );
					
				if(aCount > 600)
				{
		<!--Alert 2 : Using your current entries you will never pay off this loan ----------------------------------->
					_self.properties.errorObj.show_error_block(_self.properties.alert2, 'error'); 
					break; 
				}
				else
				{
					continue; 
				}
					
			} // End of while
			
		<!--Declaration schedule variable for creating Total Popup dialog ------------------------------------------->
			var schedule = (
				
		<!--Title of Popup Dialog ----------------------------------------------------------------------------------->
				'<div class="load_content">' +
					'<h1>'+_self.properties.mes1+'</h1>' +	// This is the title named 'Total'
					'<div class="inside">' +
						'<TABLE BORDER=0 CELLPADDING=4 CELLSPACING=0>' +
						
		<!--Detail Information from KPR Simulation ------------------------------------------------------------------>
						'<TR><TD COLSPAN=5>' +
							'<B>' +
							_self.properties.mes2 + ': ' + loanDate + '<BR>' +																		// Loan Date: 6/11/2014
							'Harga Properti : ' + _self.properties.selectedCurrAbbr.replace('%s', _self.RightDig(form.properti_value.value)) +		// Property Price
				  '<BR> ' + 'Uang Muka : ' + _self.RightDig(depositPercent) + '%' +																	// DP : 30,00%
							' (' + _self.properties.selectedCurrAbbr.replace('%s', _self.RightDig(form.deposit_value_rp.value)) + ') ' +			// Rincian DP : Rp .....
				  '<BR> ' + _self.properties.mes3 + ': ' + _self.properties.selectedCurrAbbr.replace('%s', _self.RightDig(form.principal.value)) +	// Principal: Rp.5.000.000.000,00
				 '<BR># ' + _self.properties.mes4 + ': ' + aNPer + '<BR>' +																			// # of Payments: 120
							_self.properties.mes5 + ': ' + _self.RightDig(aIntRate * 12 * 100) + '%<BR>' +												// Interest Rate: 10,00%
							_self.properties.mes6 + ': ' + _self.properties.selectedCurrAbbr.replace('%s', _self.RightDig(form.HmoPmt.value)) +
							'</B>' +
						'</TD></TR>' +

		<!--Sub Title ----------------------------------------------------------------------------------------------->
						'<TR><TD COLSPAN=5><CENTER>' +
							'<FONT SIZE=+2>' + _self.properties.mes7 + '</FONT><BR>' +	// Schedule of Payments
							'<FONT SIZE=-1>' + _self.properties.mes8 + '</FONT>' +		// Please allow for slight rounding differences
						'</CENTER></TD></TR>' +
						
		<!--Fields from each Payment detail ------------------------------------------------------------------------->
						'<TR>' +
							'<TD><B>' + _self.properties.mes9 + ' #</B></TD>' +			// Pmt # or numbering
							'<TD><B>' + _self.properties.mes13 + '</B></TD>' +			// Date
							'<TD><B>' + _self.properties.mes3 + '</B></TD>' +			// Principal
							'<TD><B>' + _self.properties.mes10 + '</B></TD>' +			// Interest
							'<TD><B>' + _self.properties.mes11 + '</B></TD>' +			// Balance
						'</TR>' +
						
		<!--Detail Values of Payment Schedule ----------------------------------------------------------------------->
						aPmtRow +			
						
		<!--Baris penjumlahan dari detail rencana pembayaran -------------------------------------------------------->
						'<TR><TD>'/*+'<B>' + _self.properties.mes12 + '</B>'*/ + '</TD>' +
						'<TD></TD>' + 
						'<TD ALIGN='+_self.properties.defaultRight+'><B>' + _self.RightDig(aAccumPrin) + '</B></TD>' +	// Total of Principal Accumulation
						'<TD><B>' + _self.RightDig(aAccumInt) + '</B></TD>' +											// Total of Interest Accumulation
						'<TD></TD>' +
						'</TR></TABLE>' +
					'</div>' + 
				'</div>'
			);

		<!--Showing the Popup Dialog inlcude of its contents -------------------------------------------------------->
			_self.properties.contentObj.show_load_block(schedule);
		}	// End of else
	} 		// End of this.results = function(form)

<!------Clear the forms ---------------------------------------------------------------------------------------------------------------------------------------->
	this.empty = function(form)
	{
		form.properti_value.value = form.deposit_value_rp.value = form.deposit_value_percentage.value =	form.principal.value = form.intRate.value = form.numYears.value = form.moPmt.value = form.HmoPmt.value = '';
	}

<!------Formatting the numbers --------------------------------------------------------------------------------------------------------------------------------->
	this.RightDig = function(Vnum)
	{ 
		if(Vnum > 1000000000001)
		{
		<!--Alert 3 : Mortgage calculator doesn't generate numbers larger than 1 Trillion --------------------------->
			_self.properties.errorObj.show_error_block(_self.properties.alert3, 'error');
			focus();
		}
		else
		{
			//100 billions
			var V100billion = parseInt(Vnum / 100000000000);
			
			//10 billions
			var V10billion = (Vnum % 100000000000) / 10000000000;
			if(V10billion / 10000000000 == 1){
				V10billion = 1;
			}else if(V10billion < 1){
				V10billion = '0';
			}else{
				V10billion = parseInt(V10billion,10);
			}
			
			//1 billion
			var V1billion = (Vnum % 10000000000) / 1000000000;
			if(V1billion / 1000000000 == 1){
				V1billion = 1; 
			}else if(V1billion < 1){
				V1billion = '0'; 
			}else{
				V1billion = parseInt(V1billion,10); 
			}
			
			//100 millions
			var V100million = (Vnum % 1000000000) / 100000000;
			if(V100million / 100000000 == 1){
				V100million = 1;
			}else if(V100million < 1){
				V100million = '0'; 
			}else{
				V100million = parseInt(V100million,10);
			}
			// ********************************************************* //
			
			//10 millions
			var V10million = (Vnum % 100000000) / 10000000;
			if(V10million / 10000000 == 1){
				V10million = 1;
			}else if(V10million < 1){
				V10million = '0'; 
			}else{
				V10million = parseInt(V10million,10);
			}
			
			//1 million
			var V1million = (Vnum % 10000000)  / 1000000;
			if(V1million / 1000000 == 1){
				V1million = 1;
			}else if(V1million < 1){
				V1million = '0';
			}else{
				V1million = parseInt(V1million,10); 
			}
			
			//100 thousands
			var V100thousand = (Vnum % 1000000) / 100000;
			if(V100thousand / 100000 == 1){
				V100thousand = 1;
			}else if(V100thousand < 1){
				V100thousand = '0'; 
			}else{
				V100thousand = parseInt(V100thousand,10);
			}
			
			//10 thousands
			var V10thousand = (Vnum % 100000)  / 10000;
			if(V10thousand / 10000 == 1){
				V10thousand = 1;
			}else if(V10thousand < 1){
				V10thousand = '0';
			}else{
				V10thousand = parseInt(V10thousand,10);
			}
			
			//1 thousands
			var V1thousand = (Vnum % 10000) / 1000;
			if(V1thousand / 1000 == 1){
				V1thousand = 1; 
			}else if(V1thousand < 1){
				V1thousand = '0'; 
			}else{
				V1thousand = parseInt(V1thousand,10); 
			}
			var Vhundreds = (Vnum % 1000)  / 100;
			if(Vhundreds / 100 == 1){
				Vhundreds = 1;
			}else if(Vhundreds < 1){
				Vhundreds = '0'; 
			}else{
				Vhundreds = parseInt(Vhundreds,10);
			}
			var Vtens = (Vnum % 100)  / 10;
			if(Vtens / 10 == 1){
				Vtens = 1; 
			}else if(Vtens < 1){
				Vtens = '0'; 
			}else{
				Vtens = parseInt(Vtens,10);
			}
			var Vones = (Vnum % 10)  / 1;
			if(Vones / 1 == 1){
				Vones = 1;
			}else if(Vones < 1){
				Vones = '0';
			}else{
				Vones = parseInt(Vones,10);
			}

		<!--Showing the Popup Dialog inlcude of its contents -------------------------------------------------------->
			var Vcents = 0;
			if(Vnum % 1 * 100 < 1){
				Vcents = 0; 
			}else{
				Vcents = parseInt(((eval(Vnum % 1) * 100)),10);
			}
			if(Vcents < 1){
				Vcents = '00'; 
			}else if(Vcents % 10 == 0){
				Vcents = Vcents + '0'; 
			}else if(Vcents % 10 == Vcents){
				Vcents = '0' + Vcents; 
			}else{
				Vcents = Vcents;
			}
			if(Vcents == '900'){
				Vcents = '90'; 
			}else if(Vcents == '800'){
				Vcents = '80'; 
			}else if(Vcents == '700'){
				Vcents = '70';
			}else if(Vcents == '600'){
				Vcents = '60'; 
			}else if(Vcents == '500'){
				Vcents = '50'; 
			}else if(Vcents == '400'){
				Vcents = "40"; 
			}else if(Vcents == '300'){
				Vcents = '30'; 
			}else if(Vcents == '200'){
				Vcents = '20'; 
			}else if(Vcents == '100'){
				Vcents = '10'; 
			}else{
				Vcents = Vcents; 
			}

		<!--Showing the Popup Dialog inlcude of its contents -------------------------------------------------------->
			var Vformat = '';
			if(Vnum >= 100000000000){ //100 billion
				Vformat = (
					V100billion + '' + V10billion + '' +  V1billion + '.' + V100million + '' + V10million + '' + 
					V1million + '.' + V100thousand + '' + V10thousand + '' + V1thousand + '.' + Vhundreds + '' + 
					Vtens + '' + Vones + ',' + Vcents
				); 
			}else if(Vnum >= 10000000000){ //10 billion
				Vformat = (
					V10billion + '' +  V1billion + '.' + V100million + '' + V10million + '' + V1million + '.' + 
					V100thousand + '' + V10thousand + '' + V1thousand + '.' + Vhundreds + '' + 
					Vtens + '' + Vones + ',' + Vcents
				); 
			}else if(Vnum >= 1000000000){ //1 billion
				Vformat = (
					V1billion + '.' + V100million + '' + V10million + '' + V1million + '.' + V100thousand + '' +
					V10thousand + '' + V1thousand + '.' + Vhundreds + '' + 
					Vtens + '' + Vones + ',' + Vcents
				); 
			}else if(Vnum >= 100000000){ //100 million
				Vformat = (
					V100million + '' + V10million + '' + V1million + '.' + V100thousand + '' +
					V10thousand + '' + V1thousand + '.' + Vhundreds + '' + 
					Vtens + '' + Vones + ',' + Vcents
				); 
			}else if(Vnum >= 10000000){ //10 million
				Vformat = (
					V10million + '' + V1million + '.' + V100thousand + '' +
					V10thousand + '' + V1thousand + '.' + Vhundreds + '' + 
					Vtens + '' + Vones + ',' + Vcents
				); 
			}else if(Vnum >= 1000000){
				Vformat = (
					V1million + '.' + V100thousand + '' + V10thousand + '' + 
					V1thousand + '.' + Vhundreds + '' + Vtens + '' + Vones + ',' + Vcents
				); 
			}else if(Vnum >= 100000){
				Vformat = (
					V100thousand + '' + V10thousand + '' + V1thousand + '.' + 
					Vhundreds + '' + Vtens + '' + Vones + ',' + Vcents
				); 
			}else if(Vnum >= 10000){
				Vformat = (V10thousand + '' + V1thousand + '.' + Vhundreds + '' + Vtens + '' + Vones + ',' + Vcents); 
			}else if(Vnum >= 1000){
				Vformat = (V1thousand + '.' + Vhundreds + '' + Vtens + '' + Vones + ',' + Vcents); 
			}else if(Vnum >= 100){
				Vformat = (Vhundreds + '' + Vtens + '' + Vones + ',' + Vcents); 
			}else if(Vnum >= 10){
				Vformat = (Vtens + '' + Vones + ',' + Vcents); 
			}else if(Vnum >= 1){
				Vformat = (Vones + ',' + Vcents); 
			}else{
				Vformat = ('0,' + Vcents); 
			}
			return Vformat;
		}	// End of else
	}		// End of this.RightDig = function(Vnum)

<!------Calculating the number  -------------------------------------------------------------------------------------------------------------------------------->
	this.calc = function(form)
	{
/*		if(form.principal.value == '' || form.principal.value == 0)
		{
			_self.properties.errorObj.show_error_block(_self.properties.alert4+' #1.', 'error'); 
			form.principal.focus(); 
		}
*/		
		<!--Additional Variables to Calculate New KPR Simulation ---------------------------------------------------->
		if(form.properti_value.value == '' || form.properti_value.value == 0)
		{
			_self.properties.errorObj.show_error_block(_self.properties.alert4+' #1.', 'error'); 
			form.properti_value.focus(); 
		}
		else if(form.deposit_value_percentage.value == '' || form.deposit_value_percentage.value == 0)
		{
			_self.properties.errorObj.show_error_block(_self.properties.alert4+' #2.', 'error'); 
			form.deposit_value_percentage.focus(); 
		}
		else if(form.deposit_value_rp.value == '' || form.deposit_value_rp.value == 0)
		{
			_self.properties.errorObj.show_error_block(_self.properties.alert4+' #2.', 'error'); 
			form.deposit_value_rp.focus(); 
		}
		<!----------------------------------------------------------------------------------------------------------->
		
		else if(form.intRate.value == '' || form.intRate.value == 0)
		{
			_self.properties.errorObj.show_error_block(_self.properties.alert4+' #4.', 'error'); 
			form.intRate.focus(); 
		}
		else if(form.numYears.value == '' || form.numYears.value == 0)
		{
			_self.properties.errorObj.show_error_block(_self.properties.alert4+' #5.', 'error');
			form.numYears.focus(); 
		}
		else
		{
			var Vprincipal = form.principal.value;		// Result after Real Price - DP
			var intRate = form.intRate.value;			// Interest, defined as 10
/*
		<!--Additional Variables to Calculate New KPR Simulation ---------------------------------------------------->
			var depositRp = form.deposit_value_rp.value;				// DP, by default 30%
			var depositPercent = form.deposit_value_percentage.value;	// DP, in Rupiah. User can change the value
			var propertiVal = form.properti_value.value;				// Real Property price
		<!--If Deposit Percentage is more than 1 -------------------------------------------------------------------->
			if(depositPercent > 1.0)
			{
				depositPercent = depositPercent / 100.0;
			}
			depositRp = propertiVal * depositPercent;	// Deposit Rp = Properti Value * Deposit Percentage
		<!--If Deposit Rupiah is more than 1 ------------------------------------------------------------------------>
			if(depositRp > 1.0)
			{
				depositPercent = (depositRp/propertiVal);	<!-- Deposit Percentage = Deposit Rp : Properti Value
			}
			depositPercent = depositPercent * 100;			// Deposit Percentage = Deposit Percentage * 100
			
			Vprincipal = propertiVal - depositRp;
		<!----------------------------------------------------------------------------------------------------------->
*/
			if(intRate > 1.0)
			{
				intRate = intRate / 100.0; 				<!-- Interest divided by 100 -->
			}
			intRate = intRate / 12;						<!-- Last Result of Interest divided by 12 -->
			var numMonths = form.numYears.value * 12;	// Year * 12, eg. 10 * 12
			var factor = 1;
			for (var j = 0; j < numMonths; j++)
			{
				factor = factor * (eval(1) + eval(intRate));
			}
			var moPmt = (Vprincipal * factor * intRate) / (eval(factor) - eval(1));		<!-- 5000000000 *  * 0.0083 -->
			form.moPmt.value = _self.properties.selectedCurrAbbr.replace('%s', _self.RightDig(moPmt));
			form.HmoPmt.value = moPmt;
		}	// End of else
	}		// End of this.calc = function(form)

	_self.Init(optionArr);
}