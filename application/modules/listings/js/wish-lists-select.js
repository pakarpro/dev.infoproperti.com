function wishListsSelect(optionArr){
	this.properties = {
		siteUrl: '',
		rand: '',
		id_items: 'wish_list_select_items',
		id_selected_items: 'wish_list_selected_items',
		id_search: 'wish_list_search',
		id_page: 'wish_list_page',
		id_close: 'wish_list_close_link',
		selected_items:[],
		max: '',
		var_name: '', 
		template: 'default',
		params: {},		
		load_form: 'listings/ajax_get_wish_lists_form/',
		load_wish_list_link: 'listings/ajax_get_wish_lists/',
		load_selected_data_link: 'listings/ajax_get_selected_wish_lists',		
		contentObj: new loadingContent({loadBlockWidth: '680px', closeBtnClass: 'load_content_controller_close', closeBtnPadding: 15})
	}
	var _self = this;
	
	var id_main = '';
	var id_span = '';
	var id_manage_link = '';

	this.errors = {
	}

	this.Init = function(options){
		_self.properties = $.extend(_self.properties, options);
		_self.id_main = 'wish_list_select_'+_self.properties.rand;
		_self.id_span = 'wish_list_text_'+_self.properties.rand;
		_self.id_manage_link = 'wish_list_link_'+_self.properties.rand;
		_self.properties.max = parseInt(_self.properties.max);

		$('#'+_self.id_manage_link).bind('click', function(){
			_self.open_form();
			return false;
		});
	}
	
	this.open_form = function(){
		var url =  _self.properties.siteUrl+_self.properties.load_form+_self.properties.max+'/'+_self.properties.template;

		$.ajax({
			url: url, 
			type: 'POST',
			data: {selected: _self.properties.selected_items},
			cache: false,
			success: function(data){
				_self.load_wish_lists('', 1);
				$('#'+_self.properties.id_search).unbind().bind('keyup', function(){
					_self.load_wish_lists($(this).val(), 1);
				});				
				if(_self.properties.max != 1){
					$('#'+_self.properties.id_selected_items+ ' input:checkbox').bind('click', function(){
						_self.unset_wish_list($(this).val());
					});
				}
				_self.properties.contentObj.show_load_block(data);				
				
				$('#'+_self.properties.id_close).bind('click', function(){
					_self.properties.contentObj.hide_load_block();
					return false;
				});
			}
		});
	}
	
	this.load_wish_lists = function(search, page){
		if(search != ''){
			var send_data = {search: search, selected: _self.properties.selected_items};
		}else{
			var send_data = {selected: _self.properties.selected_items};
		}
		$.ajax({
			url: _self.properties.siteUrl+_self.properties.load_wish_list_link + page,
			dataType: 'json',
			type: 'POST',
			data: $.extend(send_data, _self.properties.params),
			cache: false,
			success: function(data){
				$('#'+_self.properties.id_items+' option').unbind();
				$('#'+_self.properties.id_items).empty();
				for(var id in data.items ){
					$('#'+_self.properties.id_items).append('<li index="'+data.items[id].id+'">'+data.items[id].output_name+'</li>');
				}
				_self.generate_wish_list_pages(data.pages, data.current_page, search);
				$('#'+_self.properties.id_items+' li').bind('click', function(){
					_self.set_wish_list($(this).attr('index'));
				});
			}
		});
	}

	this.generate_wish_list_pages = function(pages, current_page, search){
		$('#'+_self.properties.id_page+' a').unbind();
		$('#'+_self.properties.id_page).empty();
		if(pages > 1){
			for(var i=1; i<=pages; i++){
				if(i == current_page){
					$('#'+_self.properties.id_page).append('<strong>'+i+'</strong>');
				}else{
					$('#'+_self.properties.id_page).append('<a href="#">'+i+'</a>');
				}
			}
			$('#'+_self.properties.id_page+' a').bind('click', function(){
				_self.load_wish_lists(search, $(this).text());
				return false;
			});
		}
	}


	this.set_wish_list = function(id){

		var in_selected = false;
		var i=0;
		for( i in _self.properties.selected_items){
			if(_self.properties.selected_items[i] == id){
				in_selected = true;
			}
		}

		if(_self.properties.max>1 && _self.properties.selected_items.length >= _self.properties.max){
			_self.properties.selected_items = _self.properties.selected_items.splice(0, _self.properties.max);
			_self.load_selected();
			return;
		}
		if(_self.properties.max == 1 && _self.properties.selected_items.length > 0){
			_self.properties.selected_items = [];
		}

		if(!in_selected){
			i = parseInt(i)+1;
			if(!_self.properties.selected_items.length) i=0;
			_self.properties.selected_items[i] = id;
			if(_self.properties.max == 1){
				_self.properties.contentObj.hide_load_block();
			}else{
				_self.remove_option(id);
			}
			_self.load_selected();
		}
	}

	this.load_selected = function(){
		$.ajax({
			url: _self.properties.siteUrl+_self.properties.load_selected_data_link,
			dataType: 'json',
			type: 'POST',
			data: {selected: _self.properties.selected_items},
			cache: false,
			success: function(data){
				_self.reload_span(data);
				if(_self.properties.max != 1){
					_self.reload_selected(data);
				}
			}
		});
	}

	this.unset_wish_list = function(id){
		var in_selected = false;
		for(var i in _self.properties.selected_items){
			if(_self.properties.selected_items[i] == id){
				in_selected = true;
				_self.properties.selected_items.splice(i,1);
			}
		}

		if(in_selected){
			$.ajax({
				url: _self.properties.siteUrl+_self.properties.load_selected_data_link,
				dataType: 'json',
				type: 'POST',
				data: {selected: _self.properties.selected_items},
				cache: false,
				success: function(data){
					_self.reload_span(data);
					if(_self.properties.max != 1){
						_self.reload_selected(data);
					}
					_self.load_wish_lists('', 1);
				}
			});
		}
	}

	this.remove_option = function(id){
		$('#'+_self.properties.id_items+' li[index='+id+']').remove();
	}

	this.reload_span = function(data){
		$('#'+_self.id_span).empty();
		for(var i in data){
			if(_self.properties.max != 1){
				$('#'+_self.id_span).append(data[i].output_name+'<br><input type="hidden" name="'+_self.properties.var_name+'[]" value="'+data[i].id+'">');
			}else{
				$('#'+_self.id_span).append(data[i].output_name+'<input type="hidden" name="'+_self.properties.var_name+'" value="'+data[i].id+'">');
			}
		}
	}
	this.reload_selected = function(data){
		$('#'+_self.properties.id_selected_items).empty();
		for(var i in data){
			$('#'+_self.properties.id_selected_items).append('<li><div class="wish-list-block"><input type="checkbox" name="remove_wish_lists[]" value="'+data[i].id+'" checked>'+data[i].output_name+'</div></li>');
		}
		$('#'+_self.properties.id_selected_items+ ' input:checkbox').bind('click', function(){
			_self.unset_wish_list($(this).val());
		});
	}

	_self.Init(optionArr);

}
