function wishList(optionArr){
	this.properties = {
		siteUrl: '',
		saveSortingUrl: '',
		deleteFromWishListUrl: '',
		reloadBlockUrl: '',
		listItemPrefix: 'pItem',
		listItemID: 'sortList',
		errorObj: new Errors,
		contentObj: new loadingContent({loadBlockWidth: 'no'})
	}

	var _self = this;

	this.Init = function(options){
		_self.properties = $.extend(_self.properties, options);
		_self.set_sorting();
	}

	this.set_sorting = function(){
		$("#"+_self.properties.listItemID).sortable({
			items: 'li',
			scroll: true,
			forcePlaceholderSize: true,
			placeholder: 'limiter',
			revert: true,
			stop: function(event, ui) { 
//				_self.update_closers();
			},
			update: function(event, ui) { 
				_self.update_sorting();
			}
		});
	}

	this.update_sorting = function(){
		var data = new Object;
		$("#"+_self.properties.listItemID+" > li").each(function(i){
			data[$(this).attr('id')] = i+1;
		});
		$.ajax({
			url: _self.properties.siteUrl + _self.properties.saveSortingUrl, 
			type: 'POST',
			data: ({sorter: data}), 
			dataType: 'json',
			cache: false,
			success: function(data){
				_self.properties.errorObj.show_error_block(data.success, 'success');
			}
		});
	}
	
	this.refresh_block_item = function(id){
		if(!$("#"+_self.properties.listItemPrefix + id).length){
			$("#"+_self.properties.listItemID).append('<li id="'+_self.properties.listItemPrefix+id+'"></li>');
		}
		var url =  _self.properties.siteUrl+_self.properties.reloadBlockUrl+id;
		$.ajax({
			url: url, 
			cache: false,
			success: function(data){
				$("#"+_self.properties.listItemPrefix + id).html(data);
			}
		});

	}

	this.delete_block_item = function(id){
		$("#"+_self.properties.listItemPrefix + id).remove();
		_self.properties.contentObj.hide_load_block();
		var childs = $('#'+_self.properties.listItemID).children();
		if(!childs.length){
			$('#'+_self.properties.listItemID).remove();
			$('#empty_content').show();
		}
	}
	
	this.delete_from_wish_list = function(id){
		$.ajax({
			url: _self.properties.siteUrl + _self.properties.deleteFromWishListUrl+id, 
			type: 'GET',
			dataType: 'json',
			cache: false,
			success: function(data){
				if(typeof(data.error) != 'undefined' && data.error != ''){
					_self.properties.errorObj.show_error_block(data.error, 'error');
				}else{
					_self.properties.errorObj.show_error_block(data.success, 'success');
					_self.delete_block_item(id);
				}
			}
		});
	}

	_self.Init(optionArr);
}
