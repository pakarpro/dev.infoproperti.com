function listingsSaved(optionArr){
	this.properties = {
		siteUrl: '',
		saveBtn: 'save_btn',
		saveLink: 'save_link',
		saveText: 'save_text',
		saveUrl: 'listings/ajax_save/',
		listingId: 0,
		isSaved: false,
		savedTitle: '',
		displayLogin: false,
		errorObj: new Errors,		
	};

	var _self = this;
	
	this.Init = function(options){
		_self.properties = $.extend(_self.properties, options);
		
		$('#'+_self.properties.saveBtn).bind('click', function(){
			if(_self.properties.isSaved) return false;
			if(_self.properties.displayLogin){
				$('html, body').animate({
					scrollTop: $("#ajax_login_link").offset().top
				}, 2000);
				$("#ajax_login_link").click();
			}else{
				_self.save(1);
			}
			return false;
		});
		
		$('#'+_self.properties.saveLink).bind('click', function(){
			if(_self.properties.isSaved) return false;
			if(_self.properties.displayLogin){
				$('html, body').animate({
					scrollTop: $("#ajax_login_link").offset().top
				}, 2000);
				$("#ajax_login_link").click();
			}else{
				_self.save(2);
			}
			return false;
		});
	}
	
	this.save = function(type){
		$.ajax({
			url: _self.properties.siteUrl +  _self.properties.saveUrl +  _self.properties.listingId, 
			type: 'GET',
			dataType: 'json',
			cache: false,
			success: function(data){
				if(typeof(data.error) != 'undefined' && data.error != ''){
					_self.properties.errorObj.show_error_block(data.message, 'error');
				}else{
					_self.properties.errorObj.show_error_block(data.message, 'success');
					switch(type){
						case 1:
							$('#'+_self.properties.saveBtn).attr('title', _self.properties.savedTitle)
														   .unbind('click').bind('click', function(){
								return false;
							}).find('.i-favorite').addClass('g');
						break;
						case 2:
							$('#'+_self.properties.saveLink).hide();
							$('#'+_self.properties.saveText).show();
						break;
					}
				}
			},
		});
			
	}
	
	_self.Init(optionArr);
}
