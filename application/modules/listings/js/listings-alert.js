function listingsAlert(optionArr){
	this.properties = {
		siteUrl: '',
		closeAlertUrl: 'listings/ajax_close_alert/',
		listingId: 0,
		closeClass: 'close'
	}

	var _self = this;

	this.Init = function(options){
		_self.properties = $.extend(_self.properties, options);
		_self.init_alerts();
	}
	
	this.init_alerts = function(){
		$('#approve_message .close').bind('click', function(){
			_self.close_alert('approve');
		});
		$('#decline_message .close').bind('click', function(){
			_self.close_alert('decline');
		});
	}
	
	this.close_alert = function(close_type){
		$.ajax({
			url: _self.properties.siteUrl + _self.properties.closeAlertUrl + _self.properties.listingId, 
			type: 'GET',
			cache: false,
			dataType: 'json',			
			success: function(data){
				if(data.success == '1'){
					$('#'+close_type+'_message').slideUp();
				}
			}
		});
	}
	_self.Init(optionArr);	
}
