function listingsEmailAlert(optionArr){
	this.properties = {
		siteUrl: '',
		alertBtn: 'alert_btn',
		cFormId: 'alert_form',
		urlAlertForm: 'listings/ajax_alert/',
		id_close: 'close_btn',
		contentObj: new loadingContent({loadBlockWidth: '544px', closeBtnClass: 'load_content_close', closeBtnPadding: 5}),
		errorObj: new Errors,
	}

	var _self = this;

	this.Init = function(options){
		_self.properties = $.extend(_self.properties, options);
		$('#'+_self.properties.alertBtn).bind('click', function(){
			var id = $(this).attr('data-id');			
			_self.get_form(id);
			return false;
		}).show();
	}
	
	
	this.get_form = function(id){
		$.ajax({
			url: _self.properties.siteUrl + _self.properties.urlAlertForm + id,
			type: 'GET',
			dataType: 'json',
			cache: false,
			error: function(jqXHR, textStatus, errorThrown){
				var ajax_login_link = $("#ajax_login_link");
				if(ajax_login_link.length){
					$('html, body').animate({
						scrollTop: ajax_login_link.offset().top
					}, 2000);
					ajax_login_link.click();
				}
			},
			success: function(data){
				_self.properties.contentObj.show_load_block(data.content);
				$('#'+_self.properties.id_close).unbind().bind('click', function(){
					_self.clearBox();
					return false;
				});
			}
		});		
		return false;
	}
	
	this.clearBox = function(){
		
	}
	
	_self.Init(optionArr);	
}
