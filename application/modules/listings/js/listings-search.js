function listingSearch(optionArr){
	this.properties = {
		siteUrl: '',
		viewAjaxUrl: 'listings/ajax_search/',
		loadQuickForm: 'listings/ajax_get_search_quick_form/',
		loadAdvancedForm: 'listings/ajax_get_search_advanced_form/',
		saveSearchUrl: 'listings/ajax_save_search/',
		sFromId: 'quick_search_form',
		rFromId: 'advanced_search_form',
		listBlockId: 'listings_block',
		mapBlockId: 'listings_map',
		saveBlockId: 'save_search_block',
		quickFormId: 'quick_search_extend_form',
		advancedFormId: 'advanced_search_extend_form',
		sectionId: 'search_listings_sections',
		saveSearchUrl: 'listings/ajax_save_search',
		deleteSearchUrl: 'listings/delete_saved_search', 
		deleteSearchAjaxUrl: 'listings/ajax_delete_saved_search',
		searchesBlockId: 'saved_searches_block',
		deleteFilterUrl: 'listings/delete_search_criteria', 
		deleteFilterAjaxUrl: 'listings/ajax_delete_search_criteria',
		filtersBlockId: 'search_filter_block',
		tIds: [],
		errorObj: new Errors(),
	};
	
	var _self = this;

	this.Init = function(options){
		_self.properties = $.extend(_self.properties, options);
		var sFormId = $('#'+_self.properties.sFromId);
		
		sFormId.bind('submit', function(){
			_self.search();
			return false;
		}); 
		
		sFormId.find('select[name=filters\\[type\\]]').bind('change', function(){
			var category = $('#'+_self.properties.sFromId).find('select[name=filters\\[category\\]]').val();
			_self.load_extend_form($(this).val(), category);
		});
		
		sFormId.find('select[name=filters\\[category\\]]').bind('change', function(){
			var type = $('#'+_self.properties.sFromId).find('select[name=filters\\[type\\]]').val();
			_self.load_extend_form(type, $(this).val());
		});
			
		$('#'+_self.properties.rFromId).bind('submit', function(){
			_self.refine();
			return false;
		});
		$('#'+_self.properties.saveBlockId).bind('submit', function(){
			_self.save_search();
			return false;
		});	
		
		if(_self.properties.tIds.length){
			for(var index in _self.properties.tIds){
				var id = _self.properties.tIds[index];
				$('#'+id+' a').live('click', function(){
					var new_url = $(this).attr('href').replace(_self.properties.deleteSearchUrl, _self.properties.deleteSearchAjaxUrl);
					if(new_url != $(this).attr('href')){
						_self.delete_search(new_url);
						return false;
					}
					
					new_url = $(this).attr('href').replace(_self.properties.deleteFilterUrl, _self.properties.deleteFilterAjaxUrl);
					if(new_url != $(this).attr('href')){
						_self.delete_filter(new_url);
						return false;
					}
				});
			}
		}
	}
	
	this.search = function(){
		var operation_type = $('#'+_self.properties.sFromId+' select:first').val();
		$('#'+_self.properties.sectionId+' li').removeClass('active');
		$('#'+_self.properties.sectionId+' li[sgid='+operation_type+']').addClass('active');
		var send_data = $('#'+_self.properties.sFromId).serialize();
		$.ajax({
			url: _self.properties.siteUrl + _self.properties.viewAjaxUrl + operation_type,
			type: 'POST',
			cache: false,
			data: send_data,
			success: function(data){
				$('#'+_self.properties.sectionId+' [sgid='+operation_type+']').trigger('click');
			}
		});
	}
	
	this.refine = function(){
		var operation_type = $('#'+_self.properties.sFromId+' select:first').val();
		$('#'+_self.properties.sectionId+' li').removeClass('active');
		$('#'+_self.properties.sectionId+' li[sgid='+operation_type+']').addClass('active');
		var send_data = $('#'+_self.properties.rFromId).serialize();
		$.ajax({
			url: _self.properties.siteUrl + _self.properties.viewAjaxUrl + operation_type,
			type: 'POST',
			cache: false,
			data: send_data,
			success: function(data){
				$('#'+_self.properties.sectionId+' [sgid='+operation_type+']').trigger('click');
			}
		});
	}
	
	this.save_search = function(){
		$.ajax({
			url: _self.properties.siteUrl + _self.properties.saveSearchUrl, 
			type: 'GET',
			cache: false,
			dataType: 'json',
			success: function(data){
				if(data.success){
					_self.properties.errorObj.show_error_block(data.success, 'success');
					$('#'+_self.properties.searchesBlockId).html(data.data);
				}else{
					_self.properties.errorObj.show_error_block(data.error, 'error');
				}
				return false;
			}
		});
	}	
	
	this.load_extend_form = function(type, category_id){
		$.ajax({
			url: _self.properties.siteUrl + _self.properties.loadQuickForm, 
			type: 'POST',
			cache: false,
			data: {type: type, id_category: category_id},
			dataType: 'html',
			success: function(data){
				$('#'+_self.properties.quickFormId+' .ui-slider').empty();
				$('#'+_self.properties.quickFormId).html(data);	
			}
		});
		
		$.ajax({
			url: _self.properties.siteUrl + _self.properties.loadAdvancedForm, 
			type: 'POST',
			cache: false,
			data: {type: type, id_category: category_id},
			dataType: 'html',
			success: function(data){
				$('#'+_self.properties.advancedFormId).html(data);				
			}
		});
	}
	
	this.delete_search = function(url){
		$.ajax({
			url: url, 
			type: 'GET',
			cache: false,
			dataType: 'json',
			success: function(data){
				if(data.error){
					_self.properties.errorObj.show_error_block(data.error, 'error');
				}else{
					$('#'+_self.properties.searchesBlockId).html(data.data);
					_self.properties.errorObj.show_error_block(data.success, 'success');
				}
				return false;
			}
		});
	}	
	
	this.delete_filter = function(url){
		$.ajax({
			url: url, 
			type: 'GET',
			cache: false,
			dataType: 'json',
			success: function(data){
				if(data.error){
					_self.properties.errorObj.show_error_block(data.error, 'error');
				}else{
					$('#'+_self.properties.filtersBlockId).html(data.data);
					_self.properties.errorObj.show_error_block(data.success, 'success');
					$('#m_'+data.type+' a').trigger('click');
				}
				return false;
			}
		});
	}
	
	_self.Init(optionArr);
}
