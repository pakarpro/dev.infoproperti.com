function listingsCalendar(optionArr){
	this.properties = {
		siteUrl: '',
		month: 0,
		year: 0,
		count: 1,
		scroll: 1,
		listingId: 0,
		blockId: 'calendar_block',
		ajaxCalendarUrl: 'listings/ajax_get_calendar',
	}

	var _self = this;

	this.Init = function(options){
		_self.properties = $.extend(_self.properties, options);
			
		$('#prev_month').bind('click', function(){
			_self.properties.month -= _self.properties.scroll;
			if(_self.properties.month < 1){
				_self.properties.month = Math.abs(_self.properties.month);
				_self.properties.year -= Math.floor(_self.properties.month/12) + 1;
				_self.properties.month = 12 - _self.properties.month % 12;
			}
			_self.load_calendar();
			return false;
		});
		
		$('#next_month').bind('click', function(){
			_self.properties.month += _self.properties.scroll -1;
			_self.properties.year += Math.floor(_self.properties.month/12);
			_self.properties.month = _self.properties.month % 12 +1;
			_self.load_calendar();
			return false;
		});
		
		$('#prev_year').bind('click', function(){
			_self.properties.year -= _self.properties.scroll;
			_self.load_calendar();
			return false;
		});
		
		$('#next_year').bind('click', function(){
			_self.properties.year += _self.properties.scroll;
			_self.load_calendar();
			return false;
		});
	}
	
	this.load_calendar = function(){
		var url =  _self.properties.siteUrl + _self.properties.ajaxCalendarUrl + '/' + 
				   _self.properties.listingId + '/' + _self.properties.count + '/' +
				   _self.properties.year;
		if(_self.properties.month) url += '/' + _self.properties.month;
		$.ajax({
			url: url,			
			type: 'get',
			cache: false,
			success: function(data){
				$('#'+_self.properties.blockId).html(data);
			}
		});
	}
	
	_self.Init(optionArr);	
}
