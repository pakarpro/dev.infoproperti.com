<?php
/**
* Listings Export Model
* 
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/

class Listings_export_model extends Model{
	
	/**
	 * link to CodeIgniter object
	 * @var object
	 */
	private $CI;

	/**
	 * Field editor admin export form
	 * @var string
	 */
	public $admin_export_form_gid = 'admin_export_form';
	
	/**
	 * Field editor admin export form for Residential
	 * @var string
	 */
	public $admin_export_form_residential_sale_gid = 'admin_export_form_1';
	
	/**
	 * Field editor admin export form for Commercial
	 * @var string
	 */
	public $admin_export_form_commercial_sale_gid = 'admin_export_form_2';
	
	/**
	 * Field editor admin export form for Lot and land
	 * @var string
	 */
	public $admin_export_form_lot_and_land_sale_gid = 'admin_export_form_3';
	
	/**
	 * Field editor admin export form for Residential (rental)
	 * @var string
	 */
	public $admin_export_form_residential_rent_gid = 'admin_export_form_4';
	
	/**
	 * Field editor admin export form for Commercial (rental)
	 * @var string
	 */
	public $admin_export_form_commercial_rent_gid = 'admin_export_form_5';
	
	/**
	 * Field editor admin export form for Lot and land (rental)
	 * @var string
	 */
	public $admin_export_form_lot_and_land_rent_gid = 'admin_export_form_6';
	
	/**
	 * Field editor user export form
	 * @var string
	 */
	public $user_export_form_gid = 'user_export_form';
	
	/**
	 * Field editor user export form for Residential
	 * @var string
	 */
	public $user_export_form_residential_sale_gid = 'user_export_form_1';
	
	/**
	 * Field editor user export form for Commercial
	 * @var string
	 */
	public $user_export_form_commercial_sale_gid = 'user_export_form_2';
	
	/**
	 * Field editor user export form for Lot and land
	 * @var string
	 */
	public $user_export_form_lot_and_land_sale_gid = 'user_export_form_3';
	
	/**
	 * Field editor user export form for Residential (rental)
	 * @var string
	 */
	public $user_export_form_residential_rent_gid = 'user_export_form_4';
	
	/**
	 * Field editor user export form for Commercial (rental)
	 * @var string
	 */
	public $user_export_form_commercial_rent_gid = 'user_export_form_5';
	
	/**
	 * Field editor user export form for Lot and land (rental)
	 * @var string
	 */
	public $user_export_form_lot_and_land_rent_gid = 'user_export_form_6';
	
	/**
	 * Transfer configuration
	 */
	private $fields_for_transfer = array();
	
	/**
	 * Constructor
	 *
	 * return Listings_export_model object
	 */
	function __construct()
	{
		parent::Model();
		$this->CI = & get_instance();
		$this->CI->load->model('Listings_model');
		
		$this->fields_for_transfer = array(
			array('field_name'=>'id', 'field_type'=>'int', 'name'=>l('field_id', 'listings'), 'required'=>false),
			array('fk'=>'user.output_name', 'field_name'=>'id_user', 'field_type'=>'select', 'options'=>array('header'=>l('field_owner', 'listings')), 'required'=>true),
			array('fk'=>'user.contact_email', 'field_name'=>'user_email', 'field_type'=>'text', 'name'=>l('field_owner_email', 'listings'), 'required'=>false),
			array('fk'=>'user.contact_phone', 'field_name'=>'user_phone', 'field_type'=>'text', 'name'=>l('field_owner_phone', 'listings'), 'required'=>false),
			array('field_name'=>'user_url', 'field_type'=>'text', 'name'=>l('field_owner_url', 'listings'), 'required'=>false),
			array('fk'=>'operation_type_str', 'field_name'=>'id_type', 'field_type'=>'select', 'options'=>array('header'=>l('field_listing_type', 'listings')), 'required'=>true),
			array('fk'=>'category_str', 'field_name'=>'id_category', 'field_type'=>'select', 'options'=>array('header'=>l('field_category', 'listings')), 'required'=>true),
			array('fk'=>'property_type_str', 'field_name'=>'property_type', 'field_type'=>'select', 'options'=>array('header'=>l('field_property_type', 'listings')), 'required'=>true),
			array('fk'=>'status_output', 'field_name'=>'status', 'field_type'=>'select', 'options'=>array('header'=>l('field_status', 'listings')), 'required'=>false),
			array('fk'=>'country', 'field_name'=>'id_country', 'field_type'=>'select', 'options'=>array('header'=>l('field_country', 'listings')), 'required'=>true),
			array('fk'=>'region', 'field_name'=>'id_region', 'field_type'=>'select', 'options'=>array('header'=>l('field_region', 'listings')), 'required'=>true),
			array('fk'=>'city', 'field_name' => 'id_city', 'field_type'=>'select', 'options'=>array('header'=>l('field_city', 'listings')), 'required'=>true),
			array('field_name'=>'address', 'field_type' => 'text', 'name' => l('field_address', 'listings'), 'required'=>false),
			array('field_name'=>'zip', 'field_type'=>'text', 'name'=>l('field_zip', 'listings'), 'required'=>false),
			array('field_name'=>'sold', 'field_type'=>'int', 'name'=>l('field_sold', 'listings'), 'required'=>false),
			array('field_name'=>'price', 'field_type'=>'int', 'name'=>l('field_price', 'listings'), 'required'=>true),
			array('field_name'=>'price_max', 'field_type'=>'int', 'name'=>l('field_price_max', 'listings'), 'required'=>false),
			array('field_name'=>'price_reduced', 'field_type'=>'int', 'name'=>l('field_price_reduced', 'listings'), 'required'=>false),
			array('fk'=>'price_period_str', 'field_name'=>'price_period', 'field_type'=>'select', 'options'=>array('header'=>l('field_price_period', 'listings')), 'required'=>false),
			array('fk'=>'price_type_str', 'field_name'=>'price_type', 'field_type'=>'select', 'options'=>array('header'=>l('field_price_type', 'listings')), 'required'=>false),
			array('field_name'=>'gid_currency', 'field_type'=>'text', 'name'=>l('field_currency', 'listings'), 'required'=>false),
			array('field_name'=>'price_negotiated', 'field_type'=>'int', 'name'=>l('field_price_negotiated', 'listings'), 'required'=>false),
			array('field_name'=>'price_auction', 'field_type'=>'int', 'name'=>l('field_price_auction', 'listings'), 'required'=>false),
			array('field_name'=>'square', 'field_type'=>'int', 'name'=>l('field_square', 'listings'), 'required'=>false),
			array('field_name'=>'square_max', 'field_type'=>'int', 'name'=>l('field_square_max', 'listings'), 'required'=>false),
			array('field_name'=>'square_unit', 'field_type'=>'text', 'name'=>l('field_unit', 'listings'), 'required'=>false),
			array('field_name'=>'headline', 'field_type' => 'text', 'name' => l('field_headline', 'listings'), 'required'=>false),
			array('field_name'=>'date_created', 'field_type'=>'text', 'name'=>l('field_date_created', 'listings'), 'required'=>false),
			array('field_name'=>'date_modified', 'field_type'=>'text', 'name'=>l('field_date_modified', 'listings'), 'required'=>false),
			array('field_name'=>'date_activity', 'field_type'=>'text', 'name'=>l('field_date_publish', 'listings'), 'required'=>false),
			array('field_name'=>'date_expire', 'field_type'=>'text', 'name'=>l('field_date_expire', 'listings'), 'required'=>false),
			array('field_name'=>'date_open', 'field_type'=>'text', 'name'=>l('field_date_open', 'listings'), 'required'=>false),
			array('fk'=>'date_open_begin_text', 'field_name'=>'date_open_begin', 'field_type'=>'select', 'options'=>array('header'=>l('field_date_open_begin', 'listings')), 'required'=>false),
			array('fk'=>'date_open_end_text', 'field_name'=>'date_open_end', 'field_type'=>'select', 'options'=>array('header'=>l('field_date_open_end', 'listings')), 'required'=>false),
			array('field_name'=>'date_available', 'field_type'=>'text', 'name'=>l('field_date_available', 'listings'), 'required'=>false),
			array('field_name'=>'lat', 'field_type'=>'text', 'name'=>l('field_latitude', 'listings'), 'required'=>false),
			array('field_name'=>'lon', 'field_type'=>'text', 'name'=>l('field_longitude', 'listings'), 'required'=>false),
			array('field_name'=>'output_name', 'field_type'=>'text', 'name'=>l('field_title', 'listings'), 'required'=>false),
			array('field_name'=>'use_calendar', 'field_type'=>'int', 'name'=>l('field_calendar_enabled', 'listings'), 'required'=>false),
			array('field_name'=>'calendar_period_min', 'field_type'=>'int', 'name'=>l('field_calendar_period_min', 'listings'), 'required'=>false),
			array('field_name'=>'calendar_period_max', 'field_type'=>'int', 'name'=>l('field_calendar_period_max', 'listings'), 'required'=>false),
			array('field_name'=>'seolink', 'field_type'=>'text', 'name'=>l('field_seolink', 'listings'), 'required'=>false),
			array('field_name'=>'featured_date_end', 'field_type'=>'text', 'name'=>l('field_featured_date_end', 'listings'), 'required'=>false),
			array('field_name'=>'lift_up_date_end', 'field_type'=>'text', 'name'=>l('field_lift_up_date_end', 'listings'), 'required'=>false),
			array('field_name'=>'lift_up_country_date_end', 'field_type'=>'text', 'name'=>l('field_lift_up_country_date_end', 'listings'), 'required'=>false),
			array('field_name'=>'lift_up_region_date_end', 'field_type'=>'text', 'name'=>l('field_lift_up_region_date_end', 'listings'), 'required'=>false),
			array('field_name'=>'lift_up_city_date_end', 'field_type'=>'text', 'name'=>l('field_lift_up_city_date_end', 'listings'), 'required'=>false),
			array('field_name'=>'highlight_date_end', 'field_type'=>'text', 'name'=>l('field_highlight_date_end', 'listings'), 'required'=>false),
			array('field_name'=>'slide_show_date_end', 'field_type'=>'text', 'name'=>l('field_slide_show_date_end', 'listings'), 'required'=>false),
			array('field_name'=>'uploads', 'field_type'=>'file', 'name'=>l('field_uploads', 'listings'), 'required'=>false),
			array('field_name'=>'vtours', 'field_type'=>'file', 'name'=>l('field_panorama', 'listings'), 'required'=>false),
			array('field_name'=>'files', 'field_type'=>'file', 'name'=>l('field_listing_file_source', 'listings'), 'required'=>false),
			array('field_name'=>'videos', 'field_type'=>'file', 'name'=>l('field_video_source', 'listings'), 'required'=>false),
		);
	}
	
	/**
	 * Callback get custom fields for module export
	 * @param boolean $is_export fields for export
	 * @return array
	 */
	public function callback_get_fields($is_export=true){
		$this->CI->load->model('Field_editor_model');
		
		$field_editor_types = $this->CI->Listings_model->get_field_editor_types();		
		foreach($field_editor_types as $property_type_gid){
			$this->CI->Field_editor_model->initialize($property_type_gid);
	
			// Sections fields
			$property_types = $this->CI->Field_editor_model->get_section_list();
			if(!empty($property_types)){
				$where['where_in']['section_gid'] = array_keys($property_types);
				$where['where']['editor_type_gid'] = $property_type_gid;
				$fields = $this->CI->Field_editor_model->get_fields_list($where);
				foreach($fields as $field_name=>$field_data){
					if(!isset($property_types[$field_data['section_gid']])) continue;
					$section_prefix = $property_types[$field_data['section_gid']]['name'].' - ';
					switch($field_data['field_type']){
						case 'select':
							$fields[$field_name]['options']['header'] = $section_prefix.$fields[$field_name]['options']['header'];
						break;
						default:
							$fields[$field_name]['name'] = $section_prefix.$fields[$field_name]['name'];
						break;
					}
				}
				$this->fields_for_transfer = array_merge($this->fields_for_transfer, $fields);
			}
		}
	
		foreach($this->fields_for_transfer as $field){
			switch($field['field_type']){
				case 'select':
					switch($field['field_name']){
						case 'id_type':
							$custom_fields[] = array('name' => 'type_code', 'type'=>'text', 'label'=>$field['options']['header'].' (text, code)');
							$custom_fields[] = array('name' => $field['field_name'], 'type'=>'int', 'label'=>$field['options']['header'].' (int, id)');
						break;
						case 'id_category':
							$custom_fields[] = array('name' => 'category_code', 'type'=>'text', 'label'=>$field['options']['header'].' (text, code)');
							$custom_fields[] = array('name' => $field['field_name'], 'type'=>'int', 'label'=>$field['options']['header'].' (int, id)');
						break;
						case 'id_country':
							$custom_fields[] = array('name' => 'country_code', 'type'=>'text', 'label'=>$field['options']['header'].' (text, code)');
						break;
						case 'id_region':
							$custom_fields[] = array('name' => 'region_code', 'type'=>'text', 'label'=>$field['options']['header'].' (text, code)');
							$custom_fields[] = array('name' => $field['field_name'], 'type'=>'int', 'label'=>$field['options']['header'].' (int, id)');
						break;
						default:
							$custom_fields[] = array('name' => $field['field_name'], 'type'=>'int', 'label'=>$field['options']['header'].' (int, id)');
						break;
					}
					if(isset($field['fk'])){
						$custom_fields[] = array('fk'=>$field['fk'], 'name' => $field['field_name'], 'type'=>'text', 'label'=>$field['options']['header'].' (text)');
					}else{
						$custom_fields[] = array('fk'=>'field_editor_'.$field['field_name'].'_output', 'name' => $field['field_name'], 'type'=>'text', 'label'=>$field['options']['header'].' (text)');
					}
				break;
				case 'textarea':
					$custom_fields[] = array('name' => $field['field_name'], 'type'=>'text', 'label'=>$field['name'].' (text)');
				break;
				case 'file':
					$custom_fields[] = array('name' => $field['field_name'], 'type'=>'text', 'label'=>$field['name'].' (url)');
					$custom_fields[] = array('name' => $field['field_name'], 'type'=>'file', 'label'=>$field['name'].' (file)');
				break;
				case 'checkbox':
					$custom_fields[] = array('name' => 'field_editor_'.$field['gid'], 'type'=>'int', 'label'=>$field['name'].' (int)', 'default'=>'0');
					$custom_fields[] = array('name' => 'field_editor_'.$field['gid'], 'type'=>'text', 'label'=>$field['name'].' (text)', 'default'=>l('no_str', 'start'));
				break;
				case 'multiselect':
					foreach($field['options']['option'] as $value => $name){
						$custom_fields[] = array('name' => 'field_editor_'.$field['gid'].'_option_'.$value, 'type'=>'int', 'label'=>$field['name'].' - '.$name.' (int)', 'default'=>'0');
						$custom_fields[] = array('name' => 'field_editor_'.$field['gid'].'_option_'.$value, 'type'=>'text', 'label'=>$field['name'].' - '.$name.' (text)', 'default'=>l('no_str', 'start'));
					}
				break;
				default:
					$custom_fields[] = array('name'=>$field['field_name'], 'type'=>$field['field_type'], 'label'=>$field['name'].' ('.$field['field_type'].')');
				break;
			}
		}
	
		return $custom_fields;		
	}

	/**
	 * Callback get admin form for module export
	 * @param array $data selected values
	 * @return string
	 */
	public function callback_get_admin_form($data){
		$operation_types = $this->CI->Listings_model->get_operation_types(true);
		$this->template_lite->assign('operation_types', $operation_types);
		
		$property_type_gid = $this->Listings_model->get_field_editor_type($data);
		if(isset($data['field_editor_data']['admin_export_form'])){
			if(is_array($data['field_editor_data']['admin_export_form'])){
				$this->CI->load->model('Field_editor_model');
				$this->CI->Field_editor_model->initialize($property_type_gid);
	
				$params = array();
				$fields_data = $this->CI->Field_editor_model->get_fields_list($params);		
				foreach($fields_data as $field_data){
					if(isset($data['field_editor_data']['admin_export_form'][$field_data['gid']])){
						$data[$field_data['field_name']] = $data['field_editor_data']['admin_export_form'][$field_data['gid']];
					}
					if(isset($data['field_editor_data']['admin_export_form'][$field_data['gid'].'_min'])){
						$data[$field_data['field_name'].'_min'] = $data['field_editor_data']['admin_export_form'][$field_data['gid'].'_min'];
					}
					if(isset($data['field_editor_data']['admin_export_form'][$field_data['gid'].'_max'])){
						$data[$field_data['field_name'].'_max'] = $data['field_editor_data']['admin_export_form'][$field_data['gid'].'_max'];
					}
				}
			}
			unset($data['field_editor_data']);
		}
		$this->CI->template_lite->assign('data', $data);

		if(isset($data['type']) && !empty($data['type']) && 
		   isset($data['id_category']) && !empty($data['id_category'])){
			$extend_search_form = $this->get_export_extend_form($property_type_gid, 'admin_export_form_gid');
			$this->CI->template_lite->assign('extend_search_form', $extend_search_form);
		}	

		return $this->CI->template_lite->fetch('search_form', 'admin', 'listings');
	}
	
	/**
	 * Callback get user form for module export
	 * @param array $data selected values
	 * @return string
	 */
	public function callback_get_user_form($data){
		$operation_types = $this->CI->Listings_model->get_operation_types(true);
		$this->template_lite->assign('operation_types', $operation_types);
		
		$property_type_gid = $this->Listings_model->get_field_editor_type($data);
		if(isset($data['field_editor_data']['user_export_form'])){
			if(is_array($data['field_editor_data']['user_export_form'])){
				$this->CI->load->model('Field_editor_model');
				$this->CI->Field_editor_model->initialize($property_type_gid);
	
				$params = array();
				$fields_data = $this->CI->Field_editor_model->get_fields_list($params);
				foreach($fields_data as $field_data){
					if(isset($data['field_editor_data']['user_export_form'][$field_data['gid']])){
						$data[$field_data['field_name']] = $data['field_editor_data']['user_export_form'][$field_data['gid']];
					}
					if(isset($data['field_editor_data']['user_export_form'][$field_data['gid'].'_min'])){
						$data[$field_data['field_name'].'_min'] = $data['field_editor_data']['user_export_form'][$field_data['gid'].'_min'];
					}
					if(isset($data['field_editor_data']['user_export_form'][$field_data['gid'].'_max'])){
						$data[$field_data['field_name'].'_max'] = $data['field_editor_data']['user_export_form'][$field_data['gid'].'_max'];
					}
				}
			}
			unset($data['field_editor_data']);
		}
		$this->CI->template_lite->assign('data', $data);

		if(isset($data['type']) && !empty($data['type']) && 
		   isset($data['id_category']) && !empty($data['id_category'])){
			$extend_search_form = $this->get_export_extend_form($property_type_gid, 'user_export_form_gid', false);
			$this->CI->template_lite->assign('extend_search_form', $extend_search_form);	
		}
		
		return $this->CI->template_lite->fetch('export_form', 'user', 'listings');
	}
	
	/**
	 * Callback process export form for module export
	 * @param boolean $admin_mode admin mode
	 * @return string
	 */
	public function callback_process_form($admin_mode=true){
		$post_data = (array)$this->input->post('filters', true);

		if(isset($post_data['id_category']) && !empty($post_data['id_category'])){
			$pos = strpos($post_data['id_category'], '_');
			if($pos !== false){
				$post_data['property_type'] = substr($post_data['id_category'], $pos + 1);			
				$post_data['id_category'] = substr($post_data['id_category'], 0, $pos);
			}
		}
		
		//field editor
		$form = $this->input->post('form', true);
		if($form){
			$post_data['field_editor_data'][$form] = $this->input->post('data', true);
			if(!is_array($post_data['field_editor_data'][$form])){
				$post_data['field_editor_data'][$form] = array();
			}
			$field_editor_range = $this->input->post('field_editor_range', true);
			if(!is_array($field_editor_range)){
				$field_editor_range = array();
			}
			$short_value = $this->input->post('short_value', true);
		
			foreach($post_data['field_editor_data'][$form] as $field_gid=>$field_value){
				if(isset($field_value['range']['min'])){
					if(isset($field_editor_range[$field_gid]['min'])){
						$min_value = $field_editor_range[$field_gid]['min'];
					}else{
						$min_value = 0;
					}
					if($field_value['range']['min'] <= $min_value){
						unset($post_data['field_editor_data'][$form][$field_gid]['range']['min']);
					}
				}
				if(isset($field_value['range']['max'])){
					if(isset($field_editor_range[$field_gid]['max'])){
						$max_value = $field_editor_range[$field_gid]['max'];
						if($field_value['range']['max'] >= $max_value){
							unset($post_data['field_editor_data'][$form][$field_gid]['range']['max']);
						}
					}elseif(empty($field_value['range']['max'])){
						unset($post_data['field_editor_data'][$form][$field_gid]['range']['max']);
					}
				}
				if(isset($post_data['field_editor_data'][$form][$field_gid]['range']) && 
				   empty($post_data['field_editor_data'][$form][$field_gid]['range'])){
					unset($post_data['field_editor_data'][$form][$field_gid]['range']);
				}
			}
		
			foreach($post_data['field_editor_data'][$form] as $field_gid=>$field_data){
				if(empty($field_data)){
					unset($post_data['field_editor_data'][$form][$field_gid]);
				}elseif(is_array($field_data)){
					$empty = true;
					foreach($field_data as $value){
						if(!empty($value)){
							$empty = false;
							break;
						}
					}
					if($empty){
						unset($post_data['field_editor_data'][$form][$field_gid]);
					}
				}
			}
			if(empty($post_data['field_editor_data'][$form])) unset($post_data['field_editor_data'][$form]);
		}
		
		foreach($post_data as $key => $value){
			if(empty($value)) unset($post_data[$key]);
		}

		return $post_data;
	}
	
	/**
	 * Callback process export form for module export
	 * @param array $filter_data search data
	 * @param array $relations relations data
	 * @param integer $user_id user identifier
	 * @return array
	 */
	public function callback_export_data($filter_data, $relations=array(), $user_id=0){
		$this->CI->load->model('Field_editor_model');
		
		$field_editor_names = array();
		
		if(isset($filter_data['type']) && !empty($filter_data['type']) && 
		   isset($filter_data['id_category']) && !empty($filter_data['id_category'])){
			$property_types = array($this->Listings_model->get_field_editor_type($filter_data));
		}else{
			$property_types = $this->Listings_model->get_field_editor_types();
		}

		foreach($property_types as $property_type_gid){
			$this->CI->Field_editor_model->initialize($property_type_gid);
	
			// Sections fields
			$where['where']['editor_type_gid'] = $property_type_gid;
			$fields = $this->CI->Field_editor_model->get_fields_list($where);
			foreach($fields as $i=>$field){
				switch($field['field_type']){
					case 'select':
						$field_editor_names[] = $field['field_name'];
						$fields[$i]['fk'] = 'field_editor_'.$field['gid'].'_output';
						$fields[$i]['default'] = '';
					break;
					case 'checkbox':
						$field_editor_names[] = 'field_editor_'.$field['gid'];
						$fields[] = array('fk'=>'field_editor_'.$field['gid'].'_text', 'field_name'=>'field_editor_'.$field['gid'], 'default'=>l('no_str', 'start'));
						unset($fields[$i]);
					break;
					case 'multiselect':
						foreach($field['options']['option'] as $value => $name){
							$field_editor_names[] = 'field_editor_'.$field['gid'].'_option_'.$value;
							$fields[] = array('fk'=>'field_editor_'.$field['gid'].'_option_'.$value.'_text', 'field_name'=>'field_editor_'.$field['gid'].'_option_'.$value, 'default'=>l('no_str', 'start'));
						}
						unset($fields[$i]);
					break;
					default:
						$field_editor_names[] = $field['field_name'];
					break;
				}
			}
		
			$this->fields_for_transfer = array_merge($this->fields_for_transfer, $fields);
		}
		
		reset($property_types);
		$property_type_gid = current($property_types);
	
		// Field editor forms
		if(!empty($filter_data['field_editor_data'])){
			$field_editor_data = $filter_data['field_editor_data'];
			unset($filter_data['field_editor_data']);
			
			if(isset($filter_data['type']) && !empty($filter_data['type']) &&
			   isset($filter_data['id_category']) && !empty($filter_data['id_category'])){
				$property_type_gid = $this->CI->Listings_model->get_field_editor_type($filter_data);
			}else{
				$property_type_gid = $this->CI->Listings_model->get_field_editor_type_default();
				$filter_data['id_category'] = $this->CI->Listings_model->get_category_default();
			}

			$this->CI->load->model('field_editor/models/Field_editor_forms_model');
			$fe_criteria = array();
			foreach($field_editor_data as $form_name=>$form_data){
				foreach($form_data as $field_gid=>$value){
					if(isset($value['range']) && is_array($value['range'])){
						if(isset($value['range']['min'])) $form_data[$field_gid.'_min'] = $value['range']['min'];
						if(isset($value['range']['max'])) $form_data[$field_gid.'_max'] = $value['range']['max'];
						unset($form_data[$field_gid]);
					}
				}
				$form_gid = $this->{$form_name.'_'.$property_type_gid.'_gid'};
				$temp = $this->CI->Field_editor_forms_model->get_search_criteria($form_gid, $form_data, $property_type_gid);
				if(!empty($temp)){
					$fe_criteria = array_merge($fe_criteria, $temp);
				}
			}
			if(!empty($fe_criteria)){
				if(isset($filter_data['field_editor'])){
					if(!in_array($property_type_gid, $filter_data['field_editor'][0]))
						$filter_data['field_editor'][0][] = $property_type_gid;
					$filter_data['field_editor'][1] = array_merge($filter_data['field_editor'][0], $fe_criteria);
				}else{
					$filter_data['field_editor'] = array(array($property_type_gid), $fe_criteria);
				}
			}
		}

		if($user_id) $filter_data['user'] = $user_id;			
		
		$relation_names = array();
		
		foreach($relations as $relation){
			switch($relation['link']){
				case 'uploads':
					$this->CI->Listings_model->set_format_settings('get_photos_all', true);
				break;
				case 'vtours':
					$this->CI->Listings_model->set_format_settings('get_virtual_tours_all', true);
				break;
				case 'files':
					$this->CI->Listings_model->set_format_settings('get_file', true);
				break;
				case 'video': 
					$this->CI->Listings_model->set_format_settings('get_video', true);
				break;
				default:
					$relation_names[] = $relation['link'];
				break;
			}
		}

		if(count(array_intersect($relation_names, $field_editor_names))){
			$this->CI->Listings_model->set_format_settings('get_description', true);
		}
	
		$data = $this->CI->Listings_model->get_listings_list($filter_data);

		$this->CI->Listings_model->set_format_settings(array(
			'get_description' => false, 
			'get_photos_all' => false,
			'get_virtual_tours_all' => false,
			'get_video' => false, 
			'get_file' => false,
		));

		$return = array('data'=>$data);
	
		foreach($relations as $i=>$relation){
			if($relation['link'] == 'type_code'){
				foreach($data as $j=>$row){
					$return['data'][$j]['type_code'] = $row['operation_type'];
				}
			}elseif($relation['link'] == 'category_code'){
				foreach($data as $j=>$row){
					$return['data'][$j]['category_code'] = substr($row['field_editor_type'], 0, strrpos($row['field_editor_type'], '_'));
				}
			}elseif($relation['link'] == 'country_code'){
				foreach($data as $j=>$row){
					$return['data'][$j]['country_code'] = $row['id_country'];
				}
			}elseif($relation['link'] == 'region_code'){
				foreach($data as $j=>$row){
					$return['data'][$j]['region_code'] = $row['region_code'];
				}
			}elseif($relation['link'] == 'seolink'){
				$this->CI->load->helper('seo');
				foreach($data as $j=>$row){
					$return['data'][$j]['seolink'] = rewrite_link('listings', 'view', $row);
				}
			}elseif($relation['link'] == 'user_url'){
				$this->CI->load->helper('seo');
				foreach($data as $j=>$row){
					$return['data'][$j]['user_url'] = rewrite_link('users', 'view', $row['user']);
				}
			}elseif($relation['link'] == 'uploads'){
				foreach($data as $j=>$row){
					$return['data'][$j]['uploads'] = array();
					foreach($row['photos'] as $photo){
						$return['data'][$j]['uploads'][] = $photo['media']['file_url'];
					}
				}
			}elseif($relation['link'] == 'vtours'){
				foreach($data as $j=>$row){
					$return['data'][$j]['vtours'] = array();
					foreach($row['virtual_tour'] as $vtour){
						$return['data'][$j]['vtours'][] = $vtour['media']['file_url'];
					}
				}
			}elseif($relation['link'] == 'files'){
				foreach($data as $j=>$row){
					$return['data'][$j]['files'] = array();
					if($row['is_file']){
						$return['data'][$j]['files'][] = $row['listing_file_content']['file_url'];
					}
				}
			}elseif($relation['link'] == 'videos'){
				foreach($data as $j=>$row){
					$return['data'][$j]['videos'] = array();
					if($row['is_video']){
						$return['data'][$j]['videos'][] = preg_replace('#^'.preg_quote(FRONTEND_PATH, '#').'#i', FRONTEND_URL, $row['listing_video_data']['data']['file_path']).$row['listing_video'];
					}
				}
			}elseif($relation['type'] == 'text'){
				foreach($this->fields_for_transfer as $field){
					if($relation['link'] == $field['field_name']){
						if(isset($field['fk'])){
							foreach($data as $j=>$row){
								if(strpos($field['fk'], '.')){
									$chunks = explode('.', $field['fk']);
									$return['fk'][$j][$relation['link']] = isset($row[$chunks[0]][$chunks[1]]) ? $row[$chunks[0]][$chunks[1]] : $field['default'];
								}else{
									$return['fk'][$j][$relation['link']] = isset($row[$field['fk']]) ? $row[$field['fk']] : $field['default'];
								}
							}
						}
						break;
					}			
				}
			}
		}

		return $return;
	}
	
	/**
	 * Return export form from field editor
	 * @param string $property_type_gid field editor type GUID
	 * @param string $form_gid form GUID
	 * @param boolean $admin_mode admin mode
	 */
	public function get_export_extend_form($property_type_gid, $form_gid, $admin_mode=true){
		$this->CI->load->model('Field_editor_model');
	
		$this->CI->Field_editor_model->initialize($property_type_gid);

		$form_gid = $this->{$this->{$form_gid}.'_'.$property_type_gid.'_gid'}; 

		$this->CI->load->model('field_editor/models/Field_editor_forms_model');
		$form = $this->CI->Field_editor_forms_model->get_form_by_gid($form_gid, $property_type_gid);
		$form = $this->CI->Field_editor_forms_model->format_output_form($form);
		if(count($form['field_data'])) $this->CI->template_lite->assign('form_data', $form['field_data']);
		
		if($admin_mode){
			$mode = 'admin';
			$template_name = 'search_form_block';
		}else{
			$mode = 'user';
			$template_name = 'helper_search_form_block';
		}
		
		$this->CI->template_lite->assign('form_rand', rand(100000, 999999));
		
		return $this->CI->template_lite->fetch($template_name, $mode, 'listings');
	}
}
