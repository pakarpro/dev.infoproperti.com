<?php
/**
* Listings Import Model
* 
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/

class Listings_import_model extends Model{
	
	/**
	 * link to CodeIgniter object
	 * @var object
	 */
	private $CI;

	/**
	 * Transfer configuration
	 */
	private $fields_for_transfer = array();
	
	/**
	 * Constructor
	 *
	 * return Listings_import_model object
	 */
	function __construct(){
		parent::Model();
		$this->CI = & get_instance();
		$this->CI->load->model('Listings_model');
		
		$this->fields_for_transfer = array(
			array('field_name'=>'id', 'field_type'=>'int', 'name'=>l('field_id', 'listings'), 'required'=>false),
			array('fk'=>'user.output_name', 'field_name'=>'id_user', 'field_type'=>'select', 'options'=>array('header'=>l('field_owner', 'listings')), 'required'=>true),
			array('fk'=>'type_output', 'field_name'=>'id_type', 'field_type'=>'select', 'options'=>array('header'=>l('field_listing_type', 'listings')), 'required'=>true),
			array('fk'=>'category_output', 'field_name'=>'id_category', 'field_type'=>'select', 'options'=>array('header'=>l('field_category', 'listings')), 'required'=>true),
			array('fk'=>'property_type_output', 'field_name'=>'property_type', 'field_type'=>'select', 'options'=>array('header'=>l('field_property_type', 'listings')), 'required'=>true),
			array('fk'=>'status_output', 'field_name'=>'status', 'field_type'=>'select', 'options'=>array('header'=>l('field_status', 'listings')), 'required'=>false),
			array('fk'=>'country', 'field_name'=>'id_country', 'field_type'=>'select', 'options'=>array('header'=>l('field_country', 'listings')), 'required'=>true),
			array('fk'=>'region', 'field_name'=>'id_region', 'field_type'=>'select', 'options'=>array('header'=>l('field_region', 'listings')), 'required'=>true),
			array('fk'=>'city', 'field_name'=>'id_city', 'field_type'=>'select', 'options'=>array('header'=>l('field_city', 'listings')), 'required'=>true),
			array('field_name'=>'address', 'field_type'=>'text', 'name'=>l('field_address', 'listings'), 'required'=>false),
			array('field_name'=>'zip', 'field_type'=>'text', 'name'=>l('field_zip', 'listings'), 'required'=>false),
			array('field_name'=>'sold', 'field_type'=>'int', 'name'=>l('field_sold', 'listings'), 'required'=>false),
			array('field_name'=>'price', 'field_type'=>'int', 'name'=>l('field_price', 'listings'), 'required'=>true),
			array('field_name'=>'price_max', 'field_type'=>'int', 'name'=>l('field_price_max', 'listings'), 'required'=>false),
			array('field_name'=>'price_reduced', 'field_type'=>'int', 'name'=>l('field_price_reduced', 'listings'), 'required'=>false),
			array('fk'=>'price_period_str', 'field_name'=>'price_period', 'field_type'=>'select', 'options'=>array('header'=>l('field_price_period', 'listings')), 'required'=>false),
			array('fk'=>'price_type_str', 'field_name'=>'price_type', 'field_type'=>'select', 'options'=>array('header'=>l('field_price_type', 'listings')), 'required'=>false),
			array('field_name'=>'gid_currency', 'field_type'=>'text', 'name'=>l('field_currency', 'listings'), 'required'=>false),
			array('field_name'=>'square', 'field_type'=>'int', 'name'=>l('field_square', 'listings'), 'required'=>false),
			array('field_name'=>'square_max', 'field_type'=>'int', 'name'=>l('field_square_max', 'listings'), 'required'=>false),
			array('field_name'=>'square_unit', 'field_type'=>'text', 'name'=>l('field_unit', 'listings'), 'required'=>false),
			array('field_name'=>'headline', 'field_type'=>'text', 'name'=>l('field_headline', 'listings'), 'required'=>false),
			array('field_name'=>'date_created', 'field_type'=>'text', 'name'=>l('field_date_created', 'listings'), 'required'=>false),
			array('field_name'=>'date_modified', 'field_type'=>'text', 'name'=>l('field_date_modified', 'listings'), 'required'=>false),
			array('field_name'=>'date_activity', 'field_type'=>'text', 'name'=>l('field_date_publish', 'listings'), 'required'=>false),
			array('field_name'=>'date_expire', 'field_type'=>'text', 'name'=>l('field_date_expire', 'listings'), 'required'=>false),
			array('field_name'=>'date_open', 'field_type'=>'text', 'name'=>l('field_date_open', 'listings'), 'required'=>false),
			array('fk'=>'date_open_begin_text', 'field_name'=>'date_open_begin', 'field_type'=>'select', 'options'=>array('header'=>l('field_date_open_begin', 'listings')), 'required'=>false),
			array('fk'=>'date_open_end_text', 'field_name'=>'date_open_end', 'field_type'=>'select', 'options'=>array('header'=>l('field_date_open_end', 'listings')), 'required'=>false),
			array('field_name'=>'date_available', 'field_type'=>'text', 'name'=>l('field_date_available', 'listings'), 'required'=>false),
			array('field_name'=>'lat', 'field_type'=>'text', 'name' => l('field_latitude', 'listings'), 'required'=>false),
			array('field_name'=>'lon', 'field_type'=>'text', 'name' => l('field_longitude', 'listings'), 'required'=>false),
			array('field_name'=>'use_calendar', 'field_type'=>'int', 'name'=>l('field_calendar_enabled', 'listings'), 'required'=>false),
			array('field_name'=>'calendar_period_min', 'field_type'=>'int', 'name'=>l('field_calendar_period_min', 'listings'), 'required'=>false),
			array('field_name'=>'calendar_period_max', 'field_type'=>'int', 'name'=>l('field_calendar_period_max', 'listings'), 'required'=>false),
			array('field_name'=>'featured_date_end', 'field_type'=>'text', 'name'=>l('field_featured_date_end', 'listings'), 'required'=>false),
			array('field_name'=>'lift_up_date_end', 'field_type'=>'text', 'name'=>l('field_lift_up_date_end', 'listings'), 'required'=>false),
			array('field_name'=>'lift_up_country_date_end', 'field_type'=>'text', 'name'=>l('field_lift_up_country_date_end', 'listings'), 'required'=>false),
			array('field_name'=>'lift_up_region_date_end', 'field_type'=>'text', 'name'=>l('field_lift_up_region_date_end', 'listings'), 'required'=>false),
			array('field_name'=>'lift_up_city_date_end', 'field_type'=>'text', 'name'=>l('field_lift_up_city_date_end', 'listings'), 'required'=>false),
			array('field_name'=>'highlight_date_end', 'field_type'=>'text', 'name'=>l('field_highlight_date_end', 'listings'), 'required'=>false),
			array('field_name'=>'slide_show_date_end', 'field_type'=>'text', 'name'=>l('field_slide_show_date_end', 'listings'), 'required'=>false),
			array('field_name'=>'uploads', 'field_type'=>'file', 'name'=>l('field_uploads', 'listings'), 'required'=>false),
			array('field_name'=>'vtours', 'field_type'=>'file', 'name'=>l('field_panorama', 'listings'), 'required'=>false),
//			array('field_name'=>'files', 'field_type'=>'file', 'name'=>l('field_listing_file_source', 'listings'), 'required'=>false),
//			array('field_name'=>'videos', 'field_type'=>'file', 'name'=>l('field_video_source', 'listings'), 'required'=>false),
		);
	}
	
	/**
	 * Callback get custom fields for module import
	 * @return array
	 */
	public function callback_get_fields(){
		$this->CI->load->model('Field_editor_model');
		
		$field_editor_types = $this->CI->Listings_model->get_field_editor_types();
		foreach($field_editor_types as $property_type_gid){
			$this->CI->Field_editor_model->initialize($property_type_gid);
	
			// Sections fields
			$property_types = $this->CI->Field_editor_model->get_section_list();
			if(!empty($property_types)){
				$where['where_in']['section_gid'] = array_keys($property_types);
				$where['where']['editor_type_gid'] = $property_type_gid;
				$fields = $this->CI->Field_editor_model->get_fields_list($where);
				foreach($fields as $field_name=>$field_data){
					if(!isset($property_types[$field_data['section_gid']])) continue;
					$section_prefix = $property_types[$field_data['section_gid']]['name'].' - ';
					switch($field_data['field_type']){
						case 'select':
							$fields[$field_name]['options']['header'] = $section_prefix.$field_data['options']['header'];
						break;
						default:
							$fields[$field_name]['name'] = $section_prefix.$field_data['name'];
						break;
					}
				}
				$this->fields_for_transfer = array_merge($this->fields_for_transfer, $fields);
			}
		}
		
		foreach($this->fields_for_transfer as $field){
			switch($field['field_type']){
				case 'select':
					switch($field['field_name']){
						case 'id_type':
							$custom_fields[] = array('name' => 'type_code', 'type'=>'text', 'label'=>$field['options']['header'].' (text, code)');
							$custom_fields[] = array('name' => $field['field_name'], 'type'=>'int', 'label'=>$field['options']['header'].' (int, id)');
						break;
						case 'id_category':
							$custom_fields[] = array('name' => 'category_code', 'type'=>'text', 'label'=>$field['options']['header'].' (text, code)');
							$custom_fields[] = array('name' => $field['field_name'], 'type'=>'int', 'label'=>$field['options']['header'].' (int, id)');
						break;
						case 'id_country':
							$custom_fields[] = array('name' => 'country_code', 'type'=>'text', 'label'=>$field['options']['header'].' (text, code)');
						break;
						case 'id_region':
							$custom_fields[] = array('name' => 'region_code', 'type'=>'text', 'label'=>$field['options']['header'].' (text, code)');
							$custom_fields[] = array('name' => $field['field_name'], 'type'=>'int', 'label'=>$field['options']['header'].' (int, id)');
						break;
						default:
							$custom_fields[] = array('name' => $field['field_name'], 'type'=>'int', 'label'=>$field['options']['header'].' (int, id)');
						break;
					}
					if(isset($field['fk'])){
						$custom_fields[] = array('fk'=>$field['fk'], 'name' => $field['field_name'], 'type'=>'text', 'label'=>$field['options']['header'].' (text)');
					}else{
						$custom_fields[] = array('fk'=>'field_editor_'.$field['field_name'].'_output', 'name' => $field['field_name'], 'type'=>'text', 'label'=>$field['options']['header'].' (text)');
					}
				break;
				case 'textarea':
					$custom_fields[] = array('name' => $field['field_name'], 'type'=>'text', 'label'=>$field['name'].' (text)');
				break;
				case 'file':
					$custom_fields[] = array('name' => $field['field_name'], 'type'=>'text', 'label'=>$field['name'].' (url)');
					$custom_fields[] = array('name' => $field['field_name'], 'type'=>'file', 'label'=>$field['name'].' (file)');
				break;
				case 'checkbox':
					$custom_fields[] = array('fk'=>'field_editor_'.$field['field_name'].'_output', 'name' => $field['field_name'], 'type'=>'text', 'label'=>$field['name']);
				break;
				case 'multiselect':
					foreach($field['options']['option'] as $value => $name){
						$custom_fields[] = array('fk'=>'field_editor_'.$field['field_name'].'_output_'.$value, 'name' => $field['field_name'].'_option_'.$value, 'type'=>'text', 'label'=>$field['name'].' - '.$name);
					}
				break;
				default:
					$custom_fields[] = array('name'=>$field['field_name'], 'type'=>$field['field_type'], 'label'=>$field['name'].' ('.$field['field_type'].')');
				break;
			}
		}
	
		return $custom_fields;		
	}

	/**
	 * Callback import data
	 * @param array $filter_data search data
	 * @param array $relations relations between fields
	 * @param integer $user_id user identifier
	 * @param boolean $use_moderation use moderation
	 * @return array
	 */
	public function callback_import_data($data, $relations, $user_id=0, $use_moderate=false){
		$return = array('errors'=>array(), 'data'=>array());
	
		$this->CI->load->model('Field_editor_model');
	
		$field_editor_types = $this->CI->Listings_model->get_field_editor_types();
		foreach($field_editor_types as $property_type_gid){
			$this->CI->Field_editor_model->initialize($property_type_gid);
			$where['where']['editor_type_gid'] = $property_type_gid;
			$fields = $this->CI->Field_editor_model->get_fields_list($where);		
			foreach($fields as $i=>$field){
				switch($field['field_type']){
					case 'select':
					case 'checkbox':
						$fields[$i]['fk'] = 'field_editor.'.$field['gid'].'_output';
					break;
					case 'multiselect':
						foreach($field['options']['option'] as $value=>$name){
							$fields[] = array('fk'=>'field_editor.'.$field['gid'].'_output', 'field_name'=>$field['field_name'].'_option_'.$value);
						}
						unset($fields[$i]);
					break;
				}
			}
			$this->fields_for_transfer = array_merge($this->fields_for_transfer, $fields);
		}
		
		if(isset($data['type_code']) && $data['type_code']){
			$operation_types = array_flip($this->CI->Listings_model->get_operation_types());
			if(in_array($data['type_code'], $operation_types)){
				$data['id_type'] = $operation_types[$data['type_code']];
			}else{
				$data['id_type'] = '';
			}
			unset($data['type_code']);
		}
		
		if(isset($data['category_code']) && $data['category_code']){
			$data['id_category'] =  intval($this->CI->Listings_model->get_category($data['category_code']));
			unset($data['category_code']);
		}
		
		if(isset($data['country_code']) && $data['country_code']){
			$this->CI->load->model('Countries_model');			
			$country = $this->CI->Countries_model->get_country($data['country_code']);
			if($country){
				$data['id_country'] = $data['country_code'];
			}else{
				$data['id_country'] = '';
			}
			unset($data['country_code']);
		}
		
		if(isset($data['region_code']) && $data['region_code']){
			$this->CI->load->model('Countries_model');			
			$region = $this->CI->Countries_model->get_region_by_code($data['region_code'], $data['id_country']);
			if($region){
				if(!$data['id_country']) $data['id_country'] = $region['country_code'];
				$data['id_region'] = $region['id'];
			}else{
				$data['id_region'] = 0;
			}
			unset($data['region_code']);
		}

		foreach($relations as $i=>$relation){
			if($relation['type'] == 'text'){
				foreach($this->fields_for_transfer as $field){
					if($relation['link'] == $field['field_name']){
						if(isset($field['fk']) && isset($data[$field['field_name']])){
							$data = $this->get_fk_values($field['fk'], $field['field_name'], $data);
						}
						break;
					}
				}
			}
		}

		if(isset($data['id_city']) && $data['id_city'] && (!isset($data['id_region']) || !$data['id_region'])){
			$this->CI->load->model('Countries_model');
			$city = $this->CI->Countries_model->get_city($data['id_city']);
			if($city){
				if(!isset($data['id_country']) || !$data['id_country']) $data['id_country'] = $city['country_code'];
				$data['id_region'] = $city['id_region'];
			}
		}
		
		if(isset($data['id_region']) && $data['id_region'] && (!isset($data['id_country']) || !$data['id_country'])){
			$this->CI->load->model('Countries_model');
			$region = $this->CI->Countries_model->get_region($data['id_region']);
			if($region){
				$data['id_country'] = $region['country_code'];
			}
		}
		
		if(!isset($data['status'])) $data['status'] = 1;
		
		if(isset($data['uploads'])){
			$uploads = (array)$data['uploads'];
			unset($data['uploads']);
		}else{
			$uploads = null;
		}
			
		if(isset($data['vtours'])){
			$vtours = (array)$data['vtours'];
			unset($data['vtours']);
		}else{
			$vtours = null;
		}
		
		/*if(isset($data['files'])){
			$files = (array)$data['files'];
			unset($data['files']);
		}else{
			$files = null;
		}
		
		if(isset($data['videos'])){
			$videos = (array)$data['videos'];
			unset($data['videos']);
		}else{
			$videos = null;
		}*/
		
		if($user_id) $data['id_user'] = $user_id;
		
		$listing_id = null;
		if(isset($data['id'])){
			$this->CI->Listings_model->set_format_settings('use_format', false);
			$listing = $this->CI->Listings_model->get_listing_by_id($data['id']);
			$this->CI->Listings_model->set_format_settings('use_format', true);
			if($listing){
				$listing_id = $data['id'];
				unset($data['id']);
			}
		}

		$data = $this->validate_listing($listing_id, $data);

		$validate_data = $this->CI->Listings_model->validate_listing($listing_id, $data);
		if(!empty($validate_data['errors'])){
			$return['errors'] = $validate_data['errors'];
		}else{
			if(!is_null($uploads)){
				if($listing_id) $this->CI->Listings_model->delete_photos($listing_id);
			}else{
				$uploads = array();
			}
			
			if(!is_null($vtours)){
				if($listing_id) $this->CI->Listings_model->delete_panoramas($listing_id);
			}else{
				$vtours = array();
			}
			
			/*if(!is_null($files)){
				if($listing_id) $this->CI->Listings_model->delete_files($listing_id);
			}else{
				$files = array();
			}
			
			if(!is_null($videos)){
				if($listing_id) $this->CI->Listings_model->delete_videos($listing_id);
			}else{
				$videos = array();
			}*/
			
			$listing_id = $this->CI->Listings_model->save_listing($listing_id, $validate_data['data'], $use_moderate);
			
			foreach($uploads as $upload){	
				$result = $this->CI->Listings_model->save_local_photo($listing_id, null, $upload, $use_moderate);			
				if(!empty($result['errors'])) continue;
			}
			
			foreach($vtours as $panorama){	
				$result = $this->CI->Listings_model->save_local_panorama($listing_id, null, $panorama, $use_moderate);			
				if(!empty($result['errors'])) continue;
			}
			
			/*foreach($files as $file){	
				$result = $this->CI->Listings_model->save_local_file($listing_id, null, $file, $use_moderate);			
				if(!empty($result['errors'])) continue;
			}
			
			foreach($videos as $video){	
				$result = $this->CI->Listings_model->save_local_video($listing_id, null, $video, $use_moderate);			
				if(!empty($result['errors'])) continue;
			}*/
			
			$return['data']['id'] = $listing_id;
		}
	
		return $return;
	}
	
	/**
	 * Return foreign key value
	 * @param string $fk_name foreign key name
	 * @param string $field_name field_name
	 * @param array $data import data
	 * @return mixed
	 */
	public function get_fk_values($fk_name, $field_name, $data){
		$this->CI->load->model('Countries_model');

		switch($fk_name){
			case 'country':
				$country = $this->CI->Countries_model->get_country_by_name($data['id_country']);
				if($country){
					$data['id_country'] = $country['code'];
				}else{
					$data['id_country'] = 0;
				}
			break;
			case 'region':
				$region = $this->CI->Countries_model->get_region_by_name($data['id_region']);
				if($region){
					if(!$data['id_country']) $data['id_country'] = $region['country_code'];
					$data['id_region'] = $region['id'];
				}else{
					$data['id_region'] = 0;
				}
			break;
			case 'city':
				$city = $this->CI->Countries_model->get_city_by_name($data['id_city']);
				if($city){
					if(!$data['id_country']) $data['id_country'] = $city['country_code'];
					if(!$data['id_region']) $data['id_region'] = $city['id_region'];
					$data['id_city'] = $city['id'];
				}else{
					$data['id_city'] = 0;
				}
			break;
			case 'type_output':
				$replace = false;
				$default_lang = $this->CI->pg_language->current_lang_id;
				$operation_types = $this->CI->Listings_model->get_operation_types();
				foreach($operation_types as $index => $code){
					if($code == $data['id_type']){
						$data['id_type'] = $index;
						$replace = true;
						break;
					}
				}
		
				if(!$replace){
					foreach($operation_types as $index=>$code){
						if(l('operation_type_'.$code, 'listings') == $data['id_type']){
							$data['id_type'] = $index;
							$replace = true;
							break;
						}
					}
					if(!$replace){
						foreach($this->CI->pg_language->languages as $lid => $lang_data){
							if($lid == $default_lang) continue;
							foreach($operation_types as $index=>$code){
								if(l('operation_type_'.$code, 'listings', $lid) == $data['id_type']){
									$data['id_type'] = $index;
									$replace = true;
									break;
								}
							}
						}
						if($replace) break;
					}
				}
				if(!$replace){
					$operation_type = $this->CI->Listings_model->get_operation_type_gid_default();
					$data['id_type'] = $operation_types[$operation_type];
				}
			break;
			case 'category_output':
				$replace = false;
				$default_lang = $this->CI->pg_language->current_lang_id;
				$category_ids = $this->CI->Listings_model->get_category_ids();	
				foreach($category_ids as $category_id){
					if($type_gid == $data['id_category']){
						$data['id_category'] = $category_id;
						$replace = true;
						break;
					}
				}
				if(!$replace){
					$this->CI->load->model('Properties_model');
					$property_types = $this->CI->Properties_model->get_categories('property_types', $default_lang);
					foreach($property_types as $option_value=>$option_name){
						if($option_name == $data['id_category']){
							$data['id_category'] = $option_value;
							$replace = true;
							break;
						}
					}
					if(!$replace){
						foreach($this->CI->pg_language->languages as $lid => $lang_data){
							if($lid == $default_lang) continue;
							$property_types = $this->CI->Properties_model->get_categories('property_types', $lid);
							foreach($property_types as $option_value=>$option_name){
								if($option_name == $data['id_category']){
									$data['id_category'] = $option_value;
									$replace = true;
									break;
								}
							}
							if($replace) break;
						}
					}
				}
				if(!$replace){
					$data['id_category'] = $this->CI->Listings_model->get_category_default();
				}	
				
				if(!isset($data['property_type'])) break;
				
				$this->CI->load->model('Properties_model');
				
				$replace = false;								
				$property_items = $this->CI->Properties_model->get_properties($data['id_category'], $default_lang);
				if(!empty($property_items)){
					foreach($property_items as $option_value=>$option_name){
						if($option_name == $data['property_type']){
							$data['property_type'] = $option_value;
							$replace = true;
							break;
						}
					}
					if(!$replace){
						foreach($this->CI->pg_language->languages as $lid => $lang_data){
							if($lid == $default_lang) continue;
							$property_items = $this->CI->Properties_model->get_properties($data['id_category'], $lid);
							foreach($property_items as $option_value=>$option_name){
								if($option_name == $data['property_type']){
									$data['property_type'] = $option_value;
									$replace = true;
									break;
								}
							}
							if($replace) break;
						}
					}
				}
				if(!$replace) $data['property_type'] = 0;
			break;
			case 'property_type_output':
				$replace = false;
				$default_lang = $this->CI->pg_language->current_lang_id;
				$category_ids = $this->CI->Listings_model->get_category_ids();
				foreach($category_ids as $category_id){
					$this->CI->load->model('Properties_model');
					$property_items = $this->CI->Properties_model->get_properties($category_id, $default_lang);
					if(!empty($property_items)){
						foreach($property_items as $option_value=>$option_name){
							if($option_name == $data['property_type']){
								$data['id_category'] = $category_id;
								$data['property_type'] = $option_value;
								$replace = true;
								break;
							}
						}
						if(!$replace){
							foreach($this->CI->pg_language->languages as $lid => $lang_data){
								if($lid == $default_lang) continue;
								$property_items = $this->CI->Properties_model->get_properties($category_id, $lid);
								if(empty($property_items)) continue;
								foreach($property_items as $option_value=>$option_name){
									if($option_name == $data['property_type']){
										$data['id_category'] = $category_id;
										$data['property_type'] = $option_value;
										$replace = true;
										break;
									}
								}
								if($replace) break;
							}
						}
					}
				}
				if(!$replace && !isset($data['property_type'])){
					if(!isset($data['id_category'])){
						$data['id_category'] = $this->CI->Listings_model->get_category_default();
					}
					$this->CI->load->model('Properties_model');
					$property_items = $this->CI->Properties_model->get_properties($data['id_category'], $default_lang);
					if(!empty($property_items)){
						$data['property_type'] = key($property_items);
					}
				}
			break;
			case 'user.output_name':
				$this->CI->load->model('Users_model');
				$users = $this->CI->Users_model->get_users_list_by_filters(array('name'=>$data['id_user']), 1, 1);
				if(!empty($users)){
					$data['id_user'] = $users[0]['id'];
				}else{
					$data['id_user'] = 0;
				}
			break;
			case 'price_period_str':
				if(empty($data['price_period'])) break;
				$default_lang = $this->CI->pg_language->current_lang_id;
				$replace = false;
				$price_period = ld('price_period', 'listings');
				foreach($price_period['option'] as $key=>$value){
					if($value == $data['price_period']){
						$data['price_period'] = $key;
						$replace = true;
						break;
					}
				}
				if(!$replace){
					foreach($this->CI->pg_language->languages as $lid => $lang_data){
						if($lid == $default_lang) continue;
						$price_period = ld('price_period', 'listings', $lid);
						foreach($price_period['option'] as $key=>$value){
							if($value == $data['price_period']){
								$data['price_period'] = $key;
								$replace = true;
								break;
							}
						}
						if($replace) break;
					}
				}
				if(!$replace){
					$data['price_period'] = 0;
				}
			break;
			case 'price_type_str':
				if(empty($data['price_type'])) break;
				$default_lang = $this->CI->pg_language->current_lang_id;
				$replace = false;
				$price_type = ld('price_type', 'listings');
				foreach($price_type['option'] as $key=>$value){
					if($value == $data['price_type']){
						$data['price_type'] = $key;
						$replace = true;
						break;
					}
				}
				if(!$replace){
					foreach($this->CI->pg_language->languages as $lid => $lang_data){
						if($lid == $default_lang) continue;
						$price_type = ld('price_type', 'listings', $lid);
						foreach($price_type['option'] as $key=>$value){
							if($value == $data['price_type']){
								$data['price_type'] = $key;
								$replace = true;
								break;
							}
						}
						if($replace) break;
					}
				}
				if(!$replace){
					$data['price_type'] = 0;
				}
			break;
			case 'date_open_begin_text':
				if(empty($data['date_open_begin'])) break;
				$default_lang = $this->CI->pg_language->current_lang_id;
				$replace = false;
				$dayhours = ld('dayhour-names', 'start');
				foreach($dayhours['option'] as $key=>$value){
					if($value == $data['date_open_begin']){
						$data['date_open_begin'] = $key;
						$replace = true;
						break;
					}
				}
				if(!$replace){
					foreach($this->CI->pg_language->languages as $lid => $lang_data){
						if($lid == $default_lang) continue;
						$dayhours = ld('dayhour-names', 'start', $lid);
						foreach($dayhours['option'] as $key=>$value){
							if($value == $data['date_open_begin']){
								$data['date_open_begin'] = $key;
								$replace = true;
								break;
							}
						}
						if($replace) break;
					}
				}
				if(!$replace){
					$data['date_open_begin'] = 0;
				}
			break;
			case 'date_open_end_text':
				if(empty($data['date_open_end'])) break;
				$default_lang = $this->CI->pg_language->current_lang_id;
				$replace = false;
				$dayhours = ld('dayhour-names', 'start');
				foreach($dayhours['option'] as $key=>$value){
					if($value == $data['date_open_end']){
						$data['date_open_end'] = $key;
						$replace = true;
						break;
					}
				}
				if(!$replace){
					foreach($this->CI->pg_language->languages as $lid => $lang_data){
						if($lid == $default_lang) continue;
						$dayhours = ld('dayhour-names', 'start', $lid);
						foreach($dayhours['option'] as $key=>$value){
							if($value == $data['date_open_end']){
								$data['date_open_end'] = $key;
								$replace = true;
								break;
							}
						}
						if($replace) break;
					}
				}
				if(!$replace){
					$data['date_open_end'] = 0;
				}
			break;
			default:
				if(strpos($fk_name, 'field_editor.') === 0){
					$this->CI->load->model('Field_editor_model');
					$replace = false;
					$field_editor_types = $this->CI->Listings_model->get_field_editor_types();
					foreach($field_editor_types as $property_type_gid){
						$this->CI->Field_editor_model->initialize($property_type_gid);
						$field_gid = substr($fk_name, 13, strlen($fk_name)-20);
						$field = $this->CI->Field_editor_model->get_field_by_gid($field_gid);
						if(!empty($field)){
							switch($field['field_type']){
								case 'select':
									foreach($field['options']['option'] as $option_gid=>$option_data){
										if($option_data == $data[$field_name]){
											$data[$field_name] = $option_gid;
											$replace = true;
											break;
										}
									}
								break;
								case 'checkbox':
									if(strval($data[$field_name]) === '1' || strval($data[$field_name]) === '0'){
										$replace = true;
									}else{
										if(l('yes_str', 'start') == $data[$field_name]){
											$data[$field_name] = 1;
											$replace = true;
										}elseif(l('no_str', 'start') == $data[$field_name]){
											$data[$field_name] = 0;
											$replace = true;
										}else{
											$default_lang = $this->CI->pg_language->current_lang_id;
											foreach($this->CI->pg_language->languages as $lid => $lang_data){
												if($lid == $default_lang) continue;
												if(l('yes_str', 'start', $lid) == $data[$field_name]){
													$data[$field_name] = 1;
													$replace = true;
												}elseif(l('no_str', 'start', $lid) == $data[$field_name]){
													$data[$field_name] = 0;
													$replace = true;
												}
											}
										}
									}
								break;
								case 'multiselect':
									if(strval($data[$field_name]) === '1' || $data[$field_name] == l('yes_str', 'start')){
										$chunks = explode('_option_', $field_name);
										if(!isset($data[$chunks[0]])) $data[$chunks[0]] = array();
										$data[$chunks[0]][] = $chunks[1];
									}elseif(strval($data[$field_name]) !== '0' && $data[$field_name] != l('no_str', 'start')){
										$default_lang = $this->CI->pg_language->current_lang_id;
										foreach($this->CI->pg_language->languages as $lid => $lang_data){
											if($lid == $default_lang) continue;
											if($data[$field_name] == l('yes_str', 'start', $lid)){
												$chunks = explode('_option_', $field_name);
												if(!isset($data[$chunks[0]])) $data[$chunks[0]] = array();
												$data[$chunks[0]][] = $chunks[1];
											}
										}
									}
									$replace = true;
									unset($data[$field_name]);
								break;
							}
						}
						if($replace) break;
					}
					if(!$replace){
						$data[$field_name] = 0;
					}
				}				
			break;
		}

		return $data;
	}
	
	/**
	 * Fill empty required fields
	 * @param integer $listing_id listing identifier
	 * @param array $data listing data
	 * @var array
	 */
	public function validate_listing($listing_id, $data){
		if($listing_id) return $data;
	
		$is_default = false;
		
		if(!isset($data['id_type']) || empty($data['id_type'])){
			$operation_types = $this->CI->Listings_model->get_operation_types();
			$operation_types = array_flip($operation_types);
			$operation_type = $this->CI->Listings_model->get_operation_type_gid_default();
			$data['id_type'] = $operation_types[$operation_type];
			$is_default = true;
		}
		
		if(!isset($data['id_category']) || empty($data['id_category'])){
			$data['id_category'] = $this->CI->Listings_model->get_category_default();
			$is_default = true;
		}
		
		if($data['id_category'] && (!isset($data['property_type']) || empty($data['property_type']))){
			$this->CI->load->model('Properties_model');
			$property_items = $this->CI->Properties_model->get_properties($data['id_category'], $this->CI->pg_language->current_lang_id);
			$data['property_type'] = !empty($property_items) ? key($property_items) : 0;
			$is_default = true;
		}

		if($this->CI->pg_module->is_module_installed('payments')){
			if(!isset($data['gid_currency']) || empty($data['gid_currency'])){
				$this->load->model('payments/models/Payment_currency_model');
				$currencies = $this->CI->Payment_currency_model->get_currency_list();
				$data['gid_currency'] = !empty($currencies) ? key($currencies) : '';
				$is_default = true;
			}
		}
			
		if(!isset($data['square_unit']) || empty($data['square_unit'])){
			$square_units = ld('square_units', 'listings');
			$data['square_unit'] = !empty($square_units) ? key($square_units) : '';
			$is_default = true;
		}
		
		if(!isset($data['id_country']) || empty($data['id_country'])){
			$countries = $this->CI->Countries_model->get_countries(null, array(), array(), $this->CI->pg_language->current_lang_id);
			if(!empty($countries)){
				$data['id_country'] = key($countries);
			}else{
				$data['id_country'] = 'US';
			}
			$is_default = true;
		}
		
		if(!isset($data['id_region']) || empty($data['id_region'])){
			$regions = $this->CI->Countries_model->get_regions($data['id_country'], null, array(), array(), $this->CI->pg_language->current_lang_id);
			if(!empty($regions)){
				$data['id_region'] = $regions[0]['id'];
			}else{
				$data['id_region'] = 1;
			}
			$is_default = true;
		}
		
		if(!isset($data['id_city']) || empty($data['id_city'])){
			$cities = $this->CI->Countries_model->get_cities(1, 1, array(), array('id_region'=>$data['id_region'], 'country_code'=>$data['id_country']), array(), $this->CI->pg_language->current_lang_id);
			if(!empty($cities)){
				$data['id_city'] = $cities[0]['id'];
			}else{
				$data['id_city'] = 1;
			}			
			$is_default = true;
		}
			
		if(!isset($data['id_user']) || empty($data['id_user'])){
			$this->CI->load->model('users/models/Users_import_model');
			$data['id_user'] = $this->CI->Users_import_model->get_default_user_id();
			$is_default = true;
		}
		
		if(!isset($data['price']) || empty($data['price'])){
			$data['price'] = 1;
			$is_default = true;
		}
		
		if(!isset($data['square']) || empty($data['square'])){
			$data['square'] = 1;
			$is_default = true;
		}
		
		if($is_default){
			$data['status'] = 0;
			$data['id_wish_lists'] = array();
		}
				
		return $data;
	}
}
