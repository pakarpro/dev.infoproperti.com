<?php
/**
* Listings Wish Lists Model
* 
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/

define('LISTINGS_WISH_LISTS_TABLE', DB_PREFIX.'listings_wish_lists');
define('LISTINGS_WISH_LISTS_ITEMS_TABLE', DB_PREFIX.'listings_wish_lists_items');

class Wish_list_model extends Model{
	
	/**
	 * link to CodeIgniter object
	 * @var object
	 */
	private $CI;

	/**
	 * link to DataBase object
	 * @var object
	 */
	private $DB;

	/**
	 * Table fields
	 * @var array
	 */
	private $_fields = array(
		'id',
		'id_user',
		'status',
		'logo_image',
		'listings_count',
		'date_created',
		'date_modified',
	);
	
	/**
	 * Table fields of content
	 * @var array
	 */
	private $_content_fields = array(
		'id',
		'id_wish_list',
		'id_listing',
		'sorter',
		'date_added',
	);
	
	/**
	 * Format settings
	 * @var array
	 */
	private $format_settings = array(
		'use_format' => true,
		'get_user' => true,
		'get_content' => false,
		'get_listings' => false,
	);
	
	/**
	 * Format content settings
	 * @var array
	 */
	private $format_content_settings = array(
		'get_wish_list' => false,
		'get_listings' => false,
	);
	
	/**
	 * GUID upload image for wish list 
	 * @var string
	 */
	public $upload_config_id = 'listing-wish-list-photo';
	
	/**
	 * Constructor
	 *
	 * return Wish_list_model object
	 */
	function __construct(){
		parent::Model();
		$this->CI = & get_instance();
		$this->DB = &$this->CI->db;
		foreach($this->CI->pg_language->languages as $id=>$value){
			$this->_fields[] = 'name_'.$value['id'];
		}
	}

	/**
	 * Get wish lists by ID
	 * @param inegere $wish_list_id wish list identifier
	 * @param boolean $formatted format results
	 * @return array
	 */
	public function get_wish_list_by_id($wish_list_id, $formatted=false){
		$this->DB->select(implode(', ', $this->_fields));
		$this->DB->from(LISTINGS_WISH_LISTS_TABLE);
		$this->DB->where('id', $wish_list_id);
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			$data = array($results[0]);
			if($formatted) $data = $this->format_wish_list($data);
			return $data[0];
		}
		return array();
	}
	
	/**
	 * Save wish list
	 * @param integer $wish_list_id wish list identifier
	 * @param array $data wish list data
	 */
	public function save_wish_list($wish_list_id, $data){
		if(!$wish_list_id){
			$data['date_created'] = $data['date_modified'] = date('Y-m-d H:i:s');	
			$this->DB->insert(LISTINGS_WISH_LISTS_TABLE, $data);
			$wish_list_id = $this->DB->insert_id();
		}else{
			$data['date_modified'] = date('Y-m-d H:i:s');
			$this->DB->where('id', $wish_list_id);
			$this->DB->update(LISTINGS_WISH_LISTS_TABLE, $data);
		}
		return $wish_list_id;
	}
	
	/**
	 * Remove wish list
	 * @param integer $wish_list_id wish list identifier
	 * @return boolean
	 */
	public function delete_wish_list($wish_list_id){
		$data = $this->get_wish_list_by_id($data);
	
		$this->DB->where('id', $wish_list_id);
		$this->DB->delete(LISTINGS_WISH_LISTS_TABLE);
		
		/// delete uploads
		$this->CI->load->model('Uploads_model');
		if($data['logo_image']){
			$this->CI->Uploads_model->delete_upload($this->upload_config_id, $wish_list_id.'/', $data['logo_image']);
		}
	}
	
	/**
	 * Activate wish list
	 * @param integer $wish_list_id wish list identifier
	 * @return boolean
	 */
	public function activate_wish_list($wish_list_id){
		$wish_list = $this->get_wish_list_by_id($wish_list_id);
		if(!$wish_list['listings_count']) return false;
		$data['status'] = 1;
		$this->DB->where('id', $wish_list_id);
		$this->DB->update(LISTINGS_WISH_LISTS_TABLE, $data);
		return true;
	}
	
	/**
	 * Deactivate wish list
	 * @param integer $wish_list_id wish list identifier
	 * @return boolean
	 */
	public function deactivate_wish_list($wish_list_id){
		$data['status'] = 0;
		$this->DB->where('id', $wish_list_id);
		$this->DB->update(LISTINGS_WISH_LISTS_TABLE, $data);
		return true;
	}
	
	/**
	 * Return search criteria
	 * @param array $filters filters data
	 */
	private function _get_search_criteria($filters){
		$params = array();
	
		$fields = array_flip($this->_fields);
		foreach($filters as $filter_name=>$filter_data){
			if(!$filter_data) continue;
			switch($filter_name){
				// By active:
				case 'active':
					$params = array_merge_recursive($params, array('where'=>array('status'=>1)));	
				break;
				// By ids
				case 'ids':
					if(is_array($filter_data)){
						$params = array_merge_recursive($params, array('where_in'=>array('id'=>$filter_data)));
					}else{
						$params = array_merge_recursive($params, array('where'=>array('id'=>$filter_data)));
					}
				break;
				// By ids
				case 'not_ids':
					if(is_array($filter_data)){
						$params = array_merge_recursive($params, array('where_not_in'=>array('id'=>$filter_data)));
					}else{
						$params = array_merge_recursive($params, array('where'=>array('id'=>'!= '.$filter_data)));
					}
				break;
				// By keywords
				case 'keywords':
					$params = array_merge_recursive($params, array('where_sql'=>array('(address LIKE '.$this->DB->escape('%'.$filter_data.'%').' OR zip LIKE '.$this->DB->escape('%'.$filter_data.'%').')')));
				break;
				// By field
				default:
					if(isset($fields[$filter_name])) 
						$params = array_merge_recursive($params, array('where'=>array($filter_name=>$filter_data)));	
				break;
			}
		}
	
		return $params;
	}
		
	/**
	 * Return wish lists as array
	 * @param integer $page page of results
	 * @param string $limits results limit
	 * @param array $order_by sorting order
	 * @param array $params filter criteria
	 * @param boolean $formatted results formatting
	 * @return array
	 */
	private function _get_wish_lists_list($page=null, $limits=null, $order_by=null, $params=array(), $formatted=true){
		$this->DB->select(implode(', ', $this->_fields));
		$this->DB->from(LISTINGS_WISH_LISTS_TABLE);
		
		if(isset($params['where']) && is_array($params['where']) && count($params['where'])){
			foreach($params['where'] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params['where_in']) && is_array($params['where_in']) && count($params['where_in'])){
			foreach($params['where_in'] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}
		
		if(isset($params['where_not_in']) && is_array($params['where_not_in']) && count($params['where_not_in'])){
			foreach($params['where_not_in'] as $field=>$value){
				$this->DB->where_not_in($field, $value);
			}
		}

		if(isset($params['where_sql']) && is_array($params['where_sql']) && count($params['where_sql'])){
			foreach($params['where_sql'] as $value){
				$this->DB->where($value);
			}
		}

		if(is_array($order_by) && count($order_by) > 0){
			foreach($order_by as $field => $dir){
				if(in_array($field, $this->_fields)){
					$this->DB->order_by($field.' '.$dir);
				}
			}
		}elseif($order_by){
			$this->DB->order_by($order_by);
		}
		
		if(!is_null($page)){
			$page = intval($page)?intval($page):1;
			$this->DB->limit($limits, $limits*($page-1));
		}

		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			foreach($results as $r){
				$data[] = $r;
			}
			if($formatted){
				$data = $this->format_wish_list($data);
			}
			return $data;
		}
		return array();
	}
	
	/**
	 * Return number of wish lists
	 * @param array $params filters criteria
	 * @return integer
	 */
	private function _get_wish_lists_count($params=null){
		$this->DB->select('COUNT(*) AS cnt');
		$this->DB->from(LISTINGS_WISH_LISTS_TABLE);

		if(isset($params['where']) && is_array($params['where']) && count($params['where'])){
			foreach($params['where'] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params['where_in']) && is_array($params['where_in']) && count($params['where_in'])){
			foreach($params['where_in'] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params['where_sql']) && is_array($params['where_sql']) && count($params['where_sql'])){
			foreach($params['where_sql'] as $value){
				$this->DB->where($value);
			}
		}

		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			return intval($results[0]['cnt']);
		}
		return 0;
	}
	
	/**
	 * Return list of filtered wish lists
	 * @param array $filters filter criteria
	 * @param integer $page page of results
	 * @param integer $items_on_page items per page
	 * @param string $order_by sorting order
	 * @param boolean $formatted results formatting
	 */
	public function get_wish_lists_list($filters=array(), $page=null, $items_on_page=null, $order_by=null, $formatted=true){
		$params = $this->_get_search_criteria($filters);		
		return $this->_get_wish_lists_list($page, $items_on_page, $order_by, $params, $formatted);
	}
	
	/**
	 * Return number of filtered wish lists
	 * @param array $filters filter criteria
	 * @return array
	 */
	public function get_wish_lists_count($filters=array()){
		$params = $this->_get_search_criteria($filters);
		return $this->_get_wish_lists_count($params);
	}
	
	/**
	 * Return wish list by key
	 * @param array $filters filter criteria
	 * @param boolean $formatted format results
	 * @param integer $page page of results
	 * @param integer $items_on_page items per page
	 * @param string $order_by sorting order
	 * @param boolean $formatted results formatting
	 */
	public function get_wish_lists_list_by_key($filters=array(), $page=null, $items_on_page=null, $order_by=null, $formatted=true){
		$params = $this->get_search_criteria($filters);		
		$listings_data = $this->_get_wish_lists_list($page, $items_on_page, $order_by, $params, $formatted);
		$return_data = array();
		foreach($listings_data as $data){
			$return_data[$data['id']] = $data;
		}
		return $return_data;
	}
	
	/**
	 * Change format settings
	 * @param string $name parameter name
	 * @param mixed $value parameter value
	 */
	public function set_format_settings($name, $value=false){
		if(!is_array($name)) $name = array($name=>$value);
		if(empty($name)) return;
		foreach($name as $key => $item)	$this->format_settings[$key] = $item;
	}
	
	/**
	 * Change format content settings
	 * @param string $name parameter name
	 * @param mixed $value parameter value
	 */
	public function set_format_content_settings($name, $value=false){
		if(!is_array($name)) $name = array($name=>$value);
		if(empty($name)) return;
		foreach($name as $key => $item)	$this->format_content_settings[$key] = $item;
	}
	
	/**
	 * Validate wish list
	 * @param array $data wish list data
	 * @param integer $wish_list_id wish list identifier
	 * @return array
	 */
	public function validate_wish_list($wish_list_id, $data){
		$return = array('errors'=>array(), 'data'=>array());

		if(isset($data['id'])){
			$return['data']['id'] = intval($data['id']);
			if(empty($return['data']['id'])) unset($return['data']['id']);
		}
		
		if(isset($data['id_user'])){
			$return['data']['id_user'] = intval($data['id_user']);
		}
		
		if(isset($data['status'])){
			$return['data']['status'] = $data['status'] ? 1 : 0;
		}
		
		if(isset($data['logo_image'])){
			$return['data']['logo_image'] = trim(strip_tags($data['logo_image']));
			if(empty($return['data']['logo_image'])) unset($return['data']['logo_image']);
		}
		
		if(isset($data['listings_count'])){
			$return['data']['listings_count'] = intval($data['listings_count']);
		}
		
		if(isset($data['date_created'])){
			$value = strtotime($data['date_created']);
			if($value > 0) $return['data']['date_created'] = date('Y-m-d', $value);
		}
		
		if(isset($data['date_modified'])){
			$value = strtotime($data['date_modified']);
			if($value > 0) $return['data']['date_modified'] = date('Y-m-d', $value);
		}
		
		$default_lang_id = $this->CI->pg_language->current_lang_id;
		if(isset($data['name_'.$default_lang_id])){
			$return['data']['name_'.$default_lang_id] = trim(strip_tags($data['name_'.$default_lang_id]));
			if(empty($return['data']['name_'.$default_lang_id])){
				$return['errors'][] = l('error_empty_wish_list_name', 'listings');
			}else{				
				foreach($this->pg_language->languages as $lid=>$lang_data){
					if($lid == $default_lang_id) continue;
					if(!isset($data['name_'.$lid]) || empty($data['name_'.$lid])){
						$return['data']['name_'.$lid] = $return['data']['name_'.$default_lang_id];
					}else{
						$return['data']['name_'.$lid] = trim(strip_tags($data['name_'.$lid]));
						if(empty($return['data']['name_'.$lid])){
							$return['errors'][] = l('error_empty_wish_list_name', 'listings');
							break;
						}
					}
				}
			}
		}
		
		return $return;
	}
	
	/**
	 * Format wish lists data
	 * @param array $data set of wish lists
	 * @return array
	 */
	public function format_wish_list($data){
		if(!$this->format_settings['use_format']){
			return $data;
		}
		
		$this->CI->load->model('Uploads_model');

		$users_search = array();
		$listings_search = array();
		
		foreach($data as $key=>$wish_list){
			$wish_list['name'] = $wish_list['output_name'] = $wish_list['name_'.$this->pg_language->current_lang_id];
		
			//	get_user
			if($this->format_settings['get_user']){
				$users_search[] = $wish_list['id_user'];
			}
	
			//	get_content
			if($this->format_settings['get_content']){
				$listings_search[] = $wish_list['id'];
			}
			
			// thumbs
			if(!empty($wish_list['logo_image'])){
				$wish_list['media']['photo'] = $this->CI->Uploads_model->format_upload($this->upload_config_id, $wish_list['id'], $wish_list['logo_image']);
			}else{
				$wish_list['media']['photo'] = $this->CI->Uploads_model->format_default_upload($this->upload_config_id);
			}
			
			$data[$key] = $wish_list;
		}
	
		// get user
		if($this->format_settings['get_user'] && !empty($users_search)){
			$this->CI->load->model('Users_model');
			$users_data = $this->CI->Users_model->get_users_list_by_key(null, null, null, array(), $users_search);
			foreach($data as $key=>$wish_list){
				$data[$key]['user'] = (isset($users_data[$wish_list['id_user']])) ? 
					$users_data[$wish_list['id_user']] : $this->CI->Users_model->format_default_user($wish_list['id_user']);
			}
		}

		// get content
		if($this->format_settings['get_content'] && !empty($listings_search)){
			$this->DB->select(implode(',', $this->_content_fields));
			$this->DB->from(LISTINGS_WISH_LISTS_ITEMS_TABLE);
			$this->DB->where_in('id', $listings_search);			
			$results = $this->DB->get()->result_array();
			if(!empty($results) && is_array($results)){
				foreach($results as $r){
					$listings[$r['id_wish_list']][] = $r['id_listing'];
				}
			}
			foreach($data as $key=>$wish_list){
				$data[$key]['listings'] = isset($listings[$key]) ? $listings[$key] : array();
			}
		}
		
		return $data;
	}
	
	/**
	 * Format default wish list
	 * @param integer $wish_list_id wish list identifier
	 * @return array
	 */
	public function format_default_wish_list($wish_list_id){
		$this->CI->load->model('Uploads_model');
		$data['postfix'] = $wish_list_id;
		$data['output_name'] = 'Default wish list';
		$data['media']['logo_image'] = $this->CI->Uploads_model->format_default_upload($this->upload_config_id);
		return $data;
	}
	
	/**
	 * Add listings to wish list
	 * @param integer $wish_list_id wish list identifier
	 * @param array $listing_ids listings identifiers
	 */
	public function add_to_wish_list($wish_list_id, $listing_ids){
		$this->CI->load->model('Listings_model');
	
		$exists_ids = $this->is_exists_wish_list_content($wish_list_id, $listing_ids);
		$listing_ids = array_diff($listing_ids, $exists_ids);
		if(empty($listing_ids)) return false;
		
		$wish_list = $this->get_wish_list_by_id($wish_list_id, true);
		
		$wish_data = array('logo_image'=>$wish_list['logo_image'], 'listings_count'=>$wish_list['listings_count']);
		$item_data = array('id_wish_list'=>$wish_list_id, 'date_added'=>date('Y-m-d H:i:s'));
		
		foreach($listing_ids as $listing_id){
			$item_data['id_listing'] = $listing_id;
			$item_data['sorter'] = ++$wish_data['listings_count'];
			$this->DB->insert(LISTINGS_WISH_LISTS_ITEMS_TABLE, $item_data);
			$this->DB->insert_id();
			
			if($wish_data['logo_image']) continue;
			$listing = $this->CI->Listings_model->get_listing_by_id($listing_id);
			if(!$listing['media']['photo']['file_name']) continue;
				
			$this->CI->load->model('Uploads_model');
			$img_return = $this->CI->Uploads_model->upload_exist($this->upload_config_id, $wish_list_id.'/', $listing['media']['photo']['file_path']);
			if(!empty($img_return['errors'])){
				$return['errors'] = array_merge($return['errors'], $img_return['errors']);
			}else{
				$wish_data['logo_image'] = $img_return['file'];
			}
		}	
		
		$validation_data = $this->validate_wish_list($wish_list_id, $wish_data);
		$this->save_wish_list($wish_list_id, $validation_data['data']);
		return false;
	}
	
	/**
	 * Remove listings to wish list
	 * @param integer $wish_list_id wish list identifier
	 * @param array $listing_ids listing identifier
	 * @param boolean $update_listings update listings after removing
	 */
	public function delete_from_wish_list($wish_list_id, $listing_ids, $update_listings=true){
		$this->CI->load->model('Listings_model');
		
		if(!is_array($listing_ids)) $listing_ids = array($listing_ids);
		
		if($update_listings){
			$this->Listings_model->set_format_settings('use_format', true);
			$listings = $this->CI->Listings_model->get_listings_list(array('ids'=>$listing_ids));

			foreach($listings as $listing){
				$data['id_wish_lists'] = array_flip($listing['id_wish_lists']);
				unset($data['id_wish_lists'][$wish_list_id]);
				$save_data['id_wish_lists'] = array_flip($data['id_wish_lists']);
				$validate_data = $this->CI->Listings_model->validate_listing($listing['id'], $save_data);
				if(!empty($validate_data['errors'])) continue;
				$this->CI->Listings_model->save_listing($listing['id'], $validate_data['data']);
			}
		}

		$this->DB->where('id_wish_list', $wish_list_id);
		$this->DB->where_in('id_listing', $listing_ids);
		$this->DB->delete(LISTINGS_WISH_LISTS_ITEMS_TABLE);
		
		$count = $this->get_wish_list_content_count($wish_list_id);
		if($count){
			$params['where']['id_wish_list'] = $wish_list_id;
			$params['where']['sorter'] = 1;
			$first_item = $this->_get_wish_list_content_count($params);
			if(!$first_item) $this->change_logo($wish_list_id);
		}else{
			$this->clear_logo($wish_list_id);
		}
		
		$this->refresh_content_sorter($wish_list_id);
		
		$data['id'] = $wish_list_id;
		$data['listings_count'] = $count;
		if(!$count) $data['status'] = 0;
		$validate_data = $this->validate_wish_list($wish_list_id, $data);
		$this->save_wish_list($wish_list_id, $validate_data['data']);
	}
	
	/**
	 * Clear wish list
	 * @param integer $wish_list_id wish list identifier
	 */
	public function clear_wish_list($wish_list_id){
		$content = $this->get_wish_list_content_list($wish_list_id);
		foreach($content as $row){
			$this->delete_from_wish_list($wish_list_id, $row['id_listing']);
		}
	}
	
	/**
	 * Return listings in wish list
	 * @param integer $page page of results
	 * @param integer $limits items per page
	 * @param array $order_by order by statement
	 * @param array $params query conditions
	 * @param boolean $formatted format results
	 * @return array
	 */
	public function _get_wish_list_content_list($page=1, $limits=null, $order_by=array('sorter'=>'ASC'), $params=array(), $formatted=true){
		$this->DB->select(implode(', ', $this->_content_fields));
		$this->DB->from(LISTINGS_WISH_LISTS_ITEMS_TABLE);
	
		if(isset($params['where']) && is_array($params['where']) && count($params['where'])){
			foreach($params['where'] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params['where_in']) && is_array($params['where_in']) && count($params['where_in'])){
			foreach($params['where_in'] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params['where_sql']) && is_array($params['where_sql']) && count($params['where_sql'])){
			foreach($params['where_sql'] as $value){
				$this->DB->where($value);
			}
		}

		if(is_array($order_by) && count($order_by) > 0){
			foreach($order_by as $field => $dir){
				if(in_array($field, $this->_content_fields)){
					$this->DB->order_by($field.' '.$dir);
				}
			}
		}elseif($order_by){
			$this->DB->order_by($order_by);
		}
		
		if(!is_null($page)){
			$page = intval($page)?intval($page):1;
			$this->DB->limit($limits, $limits*($page-1));
		}

		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			foreach($results as $r){
				$data[] = $r;
			}
			if($formatted){
				$data = $this->format_content($data);
			}
			return $data;
		}
		return array();
	}
	
	/**
	 * Return count of listings in wish list
	 * @param array $params query conditions
	 * @return integer
	 */
	public function _get_wish_list_content_count($params=array()){
		$this->DB->select('COUNT(*) AS cnt');
		$this->DB->from(LISTINGS_WISH_LISTS_ITEMS_TABLE);
		
		if(isset($params['where']) && is_array($params['where']) && count($params['where'])){
			foreach($params['where'] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params['where_in']) && is_array($params['where_in']) && count($params['where_in'])){
			foreach($params['where_in'] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params['where_sql']) && is_array($params['where_sql']) && count($params['where_sql'])){
			foreach($params['where_sql'] as $value){
				$this->DB->where($value);
			}
		}
		
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			return intval($results[0]['cnt']);
		}
		return 0;
	}
	
	/**
	 * Return listings in wish list
	 * @param integer $wish_list_id wish list identifier
	 * @param integer $page page of results
	 * @param integer $items_on_page items per page
	 * @param array $order_by order by statement
	 * @param boolean $formatted format results
	 * @return array
	 */
	public function get_wish_list_content_list($wish_list_id, $page=1, $items_on_page=null, $order_by=array('sorter'=>'ASC'), $formatted=true){
		$params['where']['id_wish_list'] = $wish_list_id;
		return $this->_get_wish_list_content_list($page, $items_on_page, $order_by, $params, $formatted);
	}
	
	/**
	 * Return count of listings in wish list
	 * @param integer $wish_list_id wish list identifier
	 * @return integer
	 */
	public function get_wish_list_content_count($wish_list_id){
		$params['where']['id_wish_list'] = $wish_list_id;
		return $this->_get_wish_list_content_count($params);
	}
	
	/**
	 * Save content sort order
	 * @param integer $wish_list_id wish list identifier
	 * @param integer $listing_id listing identifier
	 * @param integer $sorter
	 */
	public function save_content_sorter($wish_list_id, $listing_id, $sorter){
		$data['sorter'] = intval($sorter);
		$this->DB->where('id_wish_list', $wish_list_id);
		$this->DB->where('id_listing', $listing_id);
		$this->DB->update(LISTINGS_WISH_LISTS_ITEMS_TABLE, $data);
	}
	
	/**
	 * Refresh content sorter
	 * @param integer $wish_list_id wish list identifier
	 */
	public function refresh_content_sorter($wish_list_id){
		$this->DB->select('id, sorter');
		$this->DB->from(LISTINGS_WISH_LISTS_ITEMS_TABLE);
		$this->DB->where('id_wish_list', $wish_list_id);
		$this->DB->order_by('sorter ASC');
		$results = $this->DB->get()->result_array();
		if(!empty($results)){
			$i = 1;
			foreach($results as $r){
				$data['sorter'] = $i;
				$this->DB->where('id', $r['id']);
				$this->DB->update(LISTINGS_WISH_LISTS_ITEMS_TABLE, $data);
				$i++;
			}
		}
	}
	
	/**
	 * Return exists listings in wish list
	 * @param integer $wish_list_id wish list identifier
	 * @param integer $listing_ids listings identifiers
	 * @return array
	 */
	public function is_exists_wish_list_content($wish_list_id, $listing_ids){
		$return = array();
		$params['where']['id_wish_list'] = $wish_list_id;
		$params['where_in']['id_listing'] = $listing_ids;
		$results = $this->_get_wish_list_content_list(1, null, null, $params, false);
		foreach($results as $r){
			$return[] = $r['id_listing'];
		}
		return $return;
	}
	
	/**
	 * Validate content
	 * @param array $data content data
	 * @return array
	 */
	public function validate_content($data){
		$return = array('errors'=>array(), 'data'=>array());

		foreach($this->_content_fields as $field){
			if(isset($data[$field])){
				$return['data'][$field] = $data[$field];
			}
		}
	
		return $return;
	}
	
	/**
	 * Format content data
	 * @param array $data content data
	 * @return array
	 */
	public function format_content($data){
		$wish_lists_search = array();
		$listings_search = array();
		
		foreach($data as $key=>$listing){
			//	get_wish_list
			if($this->format_content_settings['get_wish_list']){
				$wish_lists_search[] = $wish_list['id_wish_list'];
			}
	
			//	get_content
			if($this->format_content_settings['get_listings']){
				$listings_search[] = $listing['id_listing'];
			}
		}
		
		// get wish lists
		if($this->format_content_settings['get_wish_list'] && !empty($wish_lists_search)){
			$filters['ids'] = $wish_lists_search;
			$wish_lists_data = $this->get_wish_lists_list_by_key($filters);
			foreach($data as $key=>$content){
				$data[$key]['wish_list'] = isset($wish_lists_data[$content['id_wish_list']]) ? 
					$wish_lists_data[$content['id_wish_list']] : $this->Wish_list_model->format_default_wish_list($content['id_wish_list']);
			}
		}
	
		// get listings
		if($this->format_content_settings['get_listings'] && !empty($listings_search)){			
			$this->load->model('Listings_model');
			$filters['ids'] = $listings_search;
			$listings_data = $this->Listings_model->get_listings_list_by_key($filters);
			foreach($data as $key=>$content){
				if(isset($listings_data[$content['id_listing']])){
					$data[$key]['listing'] = $listings_data[$content['id_listing']];
				}else{
					unset($data[$key]);
				}
			}
		}
	
		return $data;
	}
	
	/**
	 * Change wish list logo
	 * @param integer $wish_list_id wish list identifier
	 */
	public function change_logo($wish_list_id){
		$return = array('errors'=>array(), 'data'=>array());
		$item = $this->get_wish_list_content_list($wish_list_id, 1, 1);
		if(empty($item)){$return['errors'][] = ''; return $return;}
		$listing_id = $item[0]['id_listing'];
		
		$this->CI->load->model('Listings_model');
		$this->CI->Listings_model->set_format_settings('get_photos', true);
		$listing = $this->CI->Listings_model->get_listing_by_id($listing_id);
		$this->CI->Listings_model->set_format_settings('get_photos', false);
		if($listing['photo_count']){
			$wish_list = $this->get_wish_list_by_id($wish_list_id);
			$this->CI->load->model('Uploads_model');
			$img_return = $this->CI->Uploads_model->upload_exist($this->upload_config_id, $wish_list_id.'/', $listing['photos'][0]['media']['file_path']);
			if(!empty($img_return['errors'])){
				$return['errors'] = array_merge($return['errors'], $img_return['errors']);
			}else{
				$wish_list_data['logo_image'] = $return['data']['file'] = $img_return['file'];
				$validate_data = $this->validate_wish_list($wish_list_id, $wish_list_data);
				$validate_data['data']['id'] = $wish_list_id;
				$this->save_wish_list($wish_list_id, $validate_data['data']);
				if($wish_list['logo_image'] && $wish_list['logo_image'] != $img_return['file']){
					$this->CI->Uploads_model->delete_upload($this->upload_config_id, $wish_list_id.'/', $data['logo_image']);
				}
			}
		}else{
			$this->delete_from_wish_list($wish_list_id, $listing_id);
		}
			
		return $return;
	}
	
	/**
	 * Clear wish list logo
	 * @param integer $wish_list_id wish list identifier
	 */
	public function clear_logo($wish_list_id){
		$wish_list = $this->get_wish_list_by_id($wish_list_id);
		$wish_list_data['logo_image'] = '';
		$validate_data = $this->validate_wish_list($wish_list_id, $wish_list_data);
		$validate_data['data']['id'] = $wish_list_id;
		$this->save_wish_list($wish_list_id, $validate_data['data']);
	}
	
	/**
	 * Callback render wish lists for dynamic block
	 * @param array $params block parameters
	 * @param string $view block view
	 */
	public function _dynamic_block_get_wish_lists($params, $view=''){
		$count = $params['count'] ? intval($params['count']) : 4;
		$wish_lists = $this->get_wish_lists_list(array('active'=>1), 1, $count, array('RAND()'=>''));
		$this->CI->template_lite->assign('wish_lists', $wish_lists);
		return $this->CI->template_lite->fetch('helper_wish_lists', 'user', 'listings');
	}
	
	/**
	 * Add wish list name language field
	 * @param integer $lang_id language identifier
	 */
	public function lang_dedicate_module_callback_add($lang_id=false){
		if(!$lang_id) return;
		
		$this->CI->load->dbforge();
		
		$fields['name_'.$lang_id] = array('type'=>'TEXT', 'null'=>TRUE);
		$this->CI->dbforge->add_column(LISTINGS_WISH_LISTS_TABLE, $fields);
		
		$default_lang_id = $this->CI->pg_language->get_default_lang_id();
		if($lang_id != $default_lang_id){
			$this->CI->db->set('name_'.$lang_id, 'name_'.$default_lang_id, false);
			$this->CI->db->update(LISTINGS_WISH_LISTS_TABLE);
		}
	}
	
	/**
	 * Remove wish list name language field
	 * @param integer $lang_id language identifier
	 */
	public function lang_dedicate_module_callback_delete($lang_id=false){
		if(!$lang_id) return;
		
		$this->CI->load->dbforge();
		
		$table_query = $this->CI->db->get(LISTINGS_WISH_LISTS_TABLE);
		$fields_exists = $table_query->list_fields();
		
		$fields = array('name_'.$lang_id);
		foreach($fields as $field_name){
			if(!in_array($field_name, $fields_exists)) continue;
			$this->CI->dbforge->drop_column(LISTINGS_WISH_LISTS_TABLE, $field_name);
		}
	}
}
