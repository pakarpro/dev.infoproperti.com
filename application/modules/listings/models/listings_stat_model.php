<?php
/**
* Listings Statistics Model
* 
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/
define('LISTINGS_STATS_TABLE', DB_PREFIX.'listings_stats');

class Listings_stat_model extends Model{
	/**
	 * link to CodeIgniter object
	 * @var object
	 */
	private $CI;
	
	/**
	 * link to database object
	 * @var object
	 */
	private $DB;
	
	/**
	 * Table fields
	 * @var array
	 */
	private $_fields = array(
		'id',
		'id_user',
		'gid',
		'sale_cnt',
		'buy_cnt',
		'rent_cnt',
		'lease_cnt',
		'sold_cnt',
		'date_modified',
	);
	
	/**
	 * Constructor
	 *
	 * return Listings_stat object
	 */
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
		$this->DB = &$this->CI->db;
	}
	
	/**
	 * Get statistics by ID or GUID
	 * @param string $stat_id statistic identifier/guid
	 * @return array
	 */
	public function get_stat_by_id($stat_id){
		$field = 'id';
		if(!intval($stat_id)){
			$field = 'gid';
			$stat_id= preg_replace('/[^a-z0-9_]/', '', strtolower($stat_id));
		}
		if(!$stat_id) return false;
		
		$this->DB->select(implode(', ', $this->_fields));
		$this->DB->from(LISTINGS_STATS_TABLE);
		$this->DB->where($field, $stat_id);
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			$data = $this->format_stat(array($results[0]));
			return $data[0];
		}
		return array();
	}
	
	/**
	 * Get statistics by GUIDs
	 * @param array $stat_gids statistic guids
	 * @return array
	 */
	public function get_stat_by_gids($stat_gids){
		if(empty($stat_gids)) return array();
		$this->DB->select(implode(', ', $this->_fields));
		$this->DB->from(LISTINGS_STATS_TABLE);
		$this->DB->where_in('gid', $stat_gids);
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			$return = array();
			foreach($results as $result){
				$return[$result['gid']] = $result;
			}
			return $this->format_stat($return);
		}
		return array();
	}
	
	/**
	 * Save statistics
	 * @param integer $stat_id statistics identifier
	 * @param array $data statistics data
	 * @return boolean
	 */
	public function save_stat($stat_id, $data){
		if(!$stat_id){
			if(!isset($data['gid']) || !$data['gid']) return false;
		
			$data['gid'] = preg_replace('/[^a-z0-9_]/', '', strtolower($data['gid']));

			$type = $this->get_stat_by_id($data['gid']);
			if($type) return $type['id'];
			
			$this->DB->insert(LISTINGS_STATS_TABLE, $data);
			$stat_id = $this->DB->insert_id();
		}else{
			$fields = array_flip($this->_fields);
			foreach($data as $key=>$value){
				if(!isset($fields[$key])) unset($data[$key]);
			}
	
			if(empty($data)) return false;
	
			$this->DB->where('id', $stat_id);
			$this->DB->update(LISTINGS_STATS_TABLE, $data);
		}
		return $stat_id;
	}
	
	/**
	 * Remove statistics by ID or GUID
	 * @param mixed $stat_id statistics identifier integer ID / string GUID
	 */
	public function delete_stat($stat_id){
		$stat = $this->get_type_by_id($stat_id);		
		$this->DB->where('id', $stat['id']);
		$this->DB->delete(LISTINGS_STATS_TABLE); 
	}
	
	/**
	 * Validate statistics
	 * @param integer statistics identifier
	 * @param array $data statistics data
	 * @return array
	 */
	public function validate_stat($stat_id, $data){
		$return = array('errors'=>array(), 'data'=>array());
		
		if(isset($data['id'])){
			$return['data']['id'] = intval($data['id']);
			if(empty($return['data']['id'])) unset($return['data']['id']);
		}
		
		if(isset($data['id_user'])){
			$return['data']['id_user'] = intval($data['id_user']);
		}
		
		if(isset($data['gid'])){
			$return['data']['gid'] = trim(strip_tags($data['gid']));
			if(empty($return['data']['gid'])){
				$return['errors'][] = l('error_empty_stat_gid', 'listings');
			}
		}elseif(!$stat_id){
			$return['errors'][] = l('error_empty_stat_gid', 'listings');
		}
		
		if(isset($data['sale_cnt'])){
			$return['data']['sale_cnt'] = intval($data['sale_cnt']);
		}
		
		if(isset($data['buy_cnt'])){
			$return['data']['buy_cnt'] = intval($data['buy_cnt']);
		}
		
		if(isset($data['rent_cnt'])){
			$return['data']['rent_cnt'] = intval($data['rent_cnt']);
		}
		
		if(isset($data['lease_cnt'])){
			$return['data']['lease_cnt'] = intval($data['lease_cnt']);
		}
		
		if(isset($data['sold_cnt'])){
			$return['data']['sold_cnt'] = intval($data['sold_cnt']);
		}
		
		if(isset($data['date_modified'])){
			$value = strtotime($data['date_modified']);
			if($value > 0){
				$return['data']['date_modified'] = date('Y-m-d', $value);
			}else{
				$return['data']['date_modified'] = '0000-00-00 00:00:00';
			}
		}

		return $return;
	}
	
	/**
	 * Format statistics
	 * @param array $data statistics data
	 * @return array
	 */
	public function format_stat($data){
		return $data;
	}
}
