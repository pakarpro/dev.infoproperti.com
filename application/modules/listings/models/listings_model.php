<?php
/**
* Listings Model
* 
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/

define('LISTINGS_TABLE', DB_PREFIX.'listings');
define('LISTINGS_RESIDENTIAL_SALE_TABLE', DB_PREFIX.'listings_residential');
define('LISTINGS_COMMERCIAL_SALE_TABLE', DB_PREFIX.'listings_commercial');
define('LISTINGS_LOT_AND_LAND_SALE_TABLE', DB_PREFIX.'listings_lot_and_land');
//define('LISTINGS_RESIDENTIAL_BUY_TABLE', DB_PREFIX.'listings_residential');
//define('LISTINGS_COMMERCIAL_BUY_TABLE', DB_PREFIX.'listings_commercial');
//define('LISTINGS_LOT_AND_LAND_BUY_TABLE', DB_PREFIX.'listings_lot_and_land');
define('LISTINGS_RESIDENTIAL_RENT_TABLE', DB_PREFIX.'listings_residential');
define('LISTINGS_COMMERCIAL_RENT_TABLE', DB_PREFIX.'listings_commercial');
define('LISTINGS_LOT_AND_LAND_RENT_TABLE', DB_PREFIX.'listings_lot_and_land');
//define('LISTINGS_RESIDENTIAL_LEASE_TABLE', DB_PREFIX.'listings_residential');
//define('LISTINGS_COMMERCIAL_LEASE_TABLE', DB_PREFIX.'listings_commercial');
//define('LISTINGS_LOT_AND_LAND_LEASE_TABLE', DB_PREFIX.'listings_lot_and_land');
define('RESIDENTIAL', DB_PREFIX.'listings_residential');
define('COMMERCIAL', DB_PREFIX.'listings_commercial');

# MOD for displaying slider image from pg_sliders_images #
define('SLIDERS_IMAGES', DB_PREFIX.'sliders_images');
define('DYNAMIC_AREA_BLOCK', DB_PREFIX.'dynblocks_area_block');
# MOD for displaying slider image from pg_sliders_images #

class Listings_model extends Model{
	
	/**
	 * link to CodeIgniter object
	 * @var object
	 */
	private $CI;

	/**
	 * link to DataBase object
	 * @var object
	 */
	private $DB;

	/**
	 * Table fields
	 * @var array
	 */
	private $_fields = array(
		'id',
		'gid',
		'id_user',
		'id_type',
		'id_category',
		'property_type',
		'status',
		'date_open',
		'date_open_begin',
		'date_open_end',
		'date_available',
		'sold',
		'date_created',
		'date_modified',
		'date_activity',
		'date_expire',
		'price',
		'price_old',
		'price_max',
		'price_reduced',
		'price_period',
		'price_type',
		'price_sorting',
		'gid_currency',
		'price_negotiated',
		'price_auction',
		'square',
		'square_max',
		'square_unit',
		'id_city',
		'id_region',
		'id_country',
		'address',
		'zip',
		'lat',
		'lon',
		'headline',
		'listing_file',
		'listing_file_date',
		'listing_file_name',
		'listing_file_comment',
		'listing_video',
		'listing_video_image',
		'listing_video_data',
		'initial_moderation',
		'initial_activity',
		'logo_image',
		'slider_image',
		'id_wish_lists',
		'photo_count',
		'is_vtour',
		'use_calendar',
		'calendar_period_min',
		'calendar_period_max',
		'featured_date_end',
		'lift_up_date_end',
		'lift_up_country_date_end',
		'lift_up_region_date_end',
		'lift_up_city_date_end',
		'highlight_date_end',
		'slide_show_date_end',
		'slider_date_end',		# MOD for slider image #
		'views',
		'set_to_subscribe',
	);
	
	/**
	 * Review fields
	 * @var array
	 */
	private $_review_fields = array(
		'review_data', 
		'review_count', 
		'review_sorter', 
		'review_type'
	);

	private $residential = array(
		'id',
		'id_listing',
		'fe_bd_rooms_1',
		'fe_bth_rooms_1',
		'fe_garages_1',
		'fe_foundation_1',
		'fe_furniture_1',		# MOD for comparison #
		'fe_total_floors_1',
		'fe_field71',			# MOD for comparison #
		'fe_field73',
		'fe_field74'			# MOD for comparison #
	);
	
	private $commercial = array(
		'id',
		'id_listing',
		'fe_year_2',
		'fe_foundation_2',
	);
	
	# MOD for new slider_images #
	private $sliders_images = array(
		'id_slider',
		'id_user',
		'id_listing',
		'stts',
		'approved',
		'declined',
		'slider_name',
		'descs',
		'sorter',
		'slider_url'
	);
	# MOD for new slider_images #
	
	/**
	 * Format settings
	 * @var array
	 */
	private $format_settings = array(
		'use_format' => true,
		'get_user' => true,
		'get_location' => true,
		'get_description' => false,
		'get_photos' => false,
		'get_photos_all' => false,
		'get_virtual_tours' => false,
		'get_virtual_tours_all' => false,
		'get_video' => false,
		'get_file' => false,
		'get_moderation' => false,
		'get_saved_listings' => false,
		'get_qr_code' => false,
		'get_booking' => false,
	);

	/**
	 * Moderation type
	 * @var string
	 */
	private $moderation_type = 'listings';
	
	/**
	 * Moderation share type
	 * @var string
	 */
	private $moderation_share_type = 'share_listing';
	
	/**
	 * Operations types
	 * @var array
	 */
	public $operations_arr = array(1=>'sale', 2=>'buy', 3=>'rent', 4=>'lease');
	
	/**
	 * Operations types disabled
	 * @var array
	 */
	public $operations_arr_disabled = array();
	
	/**
	 * Operations names
	 * @var array
	 */
	private $operations_names_arr = array();
	
	/**
	 * Upload video GUID
	 * @var string
	 */
	public $video_config_id = 'listing-video';
	
	/**
	 * Upload file GUID
	 * @var string
	 */
	public $file_config_id = 'listing-file';

	/**
	 * GUID schema
	 * @var string
	 */
	public $listing_gid_scheme = '[user_name]-[location]';
	
	/**
	 * Name of type for editor form
	 */
	public $form_editor_type = 'listings';
	
	/**
	 * Listing gallery type
	 * @var string
	 */
	public $gallery_type = 'listings-photo';
	
	/**
	 * Listing virtual tour type
	 * @var string
	 */
	public $virtual_tour_type = 'listings-vtour';
	
	/**
	 * Upload image for search guid
	 * @var string
	 */
	public $upload_config_id = 'listing-search-photo';
	
	/**
	 * Upload image for search guid
	 * @var string
	 */
	public $slider_config_id = 'listing-slider-photo';
	
	/**
	 * Field editor config
	 * @var array
	 */
	public $field_editor_types = array(
		'sale' => array(1=>'residential_sale', 2=>'commercial_sale', 3=>'lot_and_land_sale'),
		'buy' => array(1=>'residential_sale', 2=>'commercial_sale', 3=>'lot_and_land_sale'),
		'rent' => array(1=>'residential_rent', 2=>'commercial_rent', 3=>'lot_and_land_rent'),
		'lease' => array(1=>'residential_rent', 2=>'commercial_rent', 3=>'lot_and_land_rent'),
	);

	/**
	 * Field editor fields by types
	 * @var array
	 */
	private $field_editor_fields = array();
	
	/**
	 * Field editor sections by types
	 * @var array
	 */
	private $field_editor_sections = array();
	
	/**
	 * Field editor main search form
	 * @var string
	 */
	public $main_search_form_gid = 'main_search_form';
	
	/**
	 * Field editor main search form for Residential
	 * @var string
	 */
	public $main_search_form_residential_sale_gid = 'main_search_form_1';
	
	/**
	 * Field editor main search form for Commercial
	 * @var string
	 */
	public $main_search_form_commercial_sale_gid = 'main_search_form_2';
	
	/**
	 * Field editor main search form for Lot and land
	 * @var string
	 */
	public $main_search_form_lot_and_land_sale_gid = 'main_search_form_3';
	
	/**
	 * Field editor main search form for Residential (rental)
	 * @var string
	 */
	public $main_search_form_residential_rent_gid = 'main_search_form_4';
	
	/**
	 * Field editor main search form for Commercial (rental)
	 * @var string
	 */
	public $main_search_form_commercial_rent_gid = 'main_search_form_5';
	
	/**
	 * Field editor main search form for Lot and land (rental)
	 * @var string
	 */
	public $main_search_form_lot_and_land_rent_gid = 'main_search_form_6';
	
	/**
	 * Field editor quick search form
	 * @var string
	 */
	public $quick_search_form_gid = 'quick_search_form';
	
	/**
	 * Field editor quick search form for Residential
	 * @var string
	 */
	public $quick_search_form_residential_sale_gid = 'quick_search_form_1';
	
	/**
	 * Field editor quick search form for Commercial
	 * @var string
	 */
	public $quick_search_form_commercial_sale_gid = 'quick_search_form_2';
	
	/**
	 * Field editor quick search form for Lot and land
	 * @var string
	 */
	public $quick_search_form_lot_and_land_sale_gid = 'quick_search_form_3';
	
	/**
	 * Field editor quick search form for Residential (rental)
	 * @var string
	 */
	public $quick_search_form_residential_rent_gid = 'quick_search_form_4';
	
	/**
	 * Field editor quick search form for Commercial (rental)
	 * @var string
	 */
	public $quick_search_form_commercial_rent_gid = 'quick_search_form_5';
	
	/**
	 * Field editor quick search form for Lot and land (rental)
	 * @var string
	 */
	public $quick_search_form_lot_and_land_rent_gid = 'quick_search_form_6';
	
	/**
	 * Field editor advanced search form
	 * @var string
	 */
	public $advanced_search_form_gid = 'advanced_search_form';
	
	/**
	 * Field editor advanced search form for Residential
	 * @var string
	 */
	public $advanced_search_form_residential_sale_gid = 'advanced_search_1';
	
	/**
	 * Field editor advanced search form for Commercial
	 * @var string
	 */
	public $advanced_search_form_commercial_sale_gid = 'advanced_search_2';
	
	/**
	 * Field editor advanced search form for Lot and land
	 * @var string
	 */
	public $advanced_search_form_lot_and_land_sale_gid = 'advanced_search_3';
	
	/**
	 * Field editor advanced search form for Residential (rental)
	 * @var string
	 */
	public $advanced_search_form_residential_rent_gid = 'advanced_search_4';
	
	/**
	 * Field editor advanced search form for Commercial (rental)
	 * @var string
	 */
	public $advanced_search_form_commercial_rent_gid = 'advanced_search_5';
	
	/**
	 * Field editor advanced search form for Lot and land (rental)
	 * @var string
	 */
	public $advanced_search_form_lot_and_land_rent_gid = 'advanced_search_6';
	
	/**
	 * Square units GUID of data source
	 * @var
	 */
	public $square_units_gid = 'square_units';
	
	/**
	 * Radius data GUID of data source
	 * @var
	 */
	public $radius_data_gid = 'radius_data';
	
	/**
	 * Display sections
	 * @var array
	 */
	public $display_sections = array(
		'top' => array('gallery' => 1, 'map' => 1, 'panorama' => 1, 'virtual_tour' => 1, 'video'=>1),
		'bottom' => array('overview' => 1, 'calendar' => 1, 'reviews' => 1, 'file'=>1, 'provider' => 0, 'print'=>0, 'kpr'=>1),
	);

	/**
	 * Period of listings services in days by default
	 * @var integer
	 */
	public $service_period_default = 14;
	
	/**
	 * Steps amount of price range
	 * @var integer
	 */
	public $price_range_steps_default = 50;
	
	/**
	 * Rss logo gid
	 * @var string
	 */
	public $rss_config_id = 'rss-listings-logo';
	
	/**
	 * Max price default
	 * @var integer
	 */
	public $price_max_default = 100000;
	
	/**
	 * QR code update time in hours
	 * @var integer
	 */
	private $qr_update_time = 3;
	
	/**
	 * Constructor
	 *
	 * return Listings_model object
	 */
	#mod for KPR#
	 function list_bank()
	 {
		 $this->db->select('*');
		 $this->db->from('kpr_bank');
		
		 $query = $this -> db -> get();
		 if($query -> num_rows>= 1)
		 {
			$properti = $query->result();
			return $properti;
		 }
		 else
		 {
			return false;
		 }
	 }
	 function list_properti()
	 {
		 $this->db->select('*');
		 $this->db->from('kpr_properti');
		
		 $query = $this -> db -> get();
		 if($query -> num_rows>= 1)
		 {
			$properti = $query->result();
			return $properti;
		 }
		 else
		 {
			return false;
		 }
	 }

	 function list_promosi()
	 {
		 $this->DB->select('*');
		 $this->DB->from('kpr_promosi');
		
		 $query = $this -> DB -> get();
		 if($query -> num_rows>= 1)
		 {
			$promosi = $query->result();
			return $promosi;
		 }
		 else
		 {
			return false;
		 }
	 }
	 function list_pinjaman()
	 {
		 $this->db->select('*');
		 $this->db->from('kpr_pinjaman');
		
		 $query = $this -> db -> get();
		 if($query -> num_rows>= 1)
		 {
			$pinjaman = $query->result();
			return $pinjaman;
		 }
		 else
		 {
			return false;
		 }
	 }

	#end mod for kpr#
	 
	 
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
		$this->DB = &$this->CI->db;

		$this->_get_module_config();
		
		// add reviews fields
		if($this->CI->pg_module->is_module_installed('reviews')){
			$this->_fields = array_merge($this->_fields, $this->_review_fields);
		}
	}
	
	/**
	 * Load module configuration
	 */
	private function _get_module_config(){
		foreach($this->operations_arr as $operation_type_gid){
			$enabled = $this->CI->pg_module->get_module_config('listings', 'for_'.$operation_type_gid.'_enabled');
			if(!$enabled){
			    $this->operations_arr_disabled[] = $operation_type_gid;
			}
		}
	}
	
	/**
	 * Return operation type name
	 * @param integer $operation_type_id type identifier
	 * @return string
	 */
	public function get_operation_type_by_id($operation_type_id){
		if(!isset($this->operations_arr[$operation_type_id])) return '';
		$operation_type_gid = $this->operations_arr[$operation_type_id];
		return $operation_type_gid;
	}
	
	/*
	 * Return operation type identifier
	 * @param string $operation_type operation_type guid
	 * $return integer
	 */
	public function get_operation_type_by_gid($operation_type_gid){
		if(!in_array($operation_type_gid, $this->operations_arr)) return 0;
		return array_search($operation_type_gid, $this->operations_arr);
	}
	
	/**
	 * Return operation type identifier name by default
	 * @param boolean $is_search use all
	 */
	public function get_operation_type_id_default($is_search=false){
		$operation_types = $this->get_operation_types($is_search);
		if(!empty($operation_types)){
			return array_shift(array_keys($operation_types));
		}else{
			return '';
		}
	}
	
	/**
	 * Return operation type guid name by default
	 * @param boolean $is_search use all
	 */
	public function get_operation_type_gid_default($is_search=false){
		$operation_types = $this->get_operation_types($is_search);
		if(!empty($operation_types)){
			return array_shift($operation_types);
		}else{
			return 'all';
		}
	}
	
	/**
	 * Return enabled operation types
	 * @param boolean $is_search use all
	 * @return array
	 */
	public function get_operation_types($is_search=false){
		$operation_types = array_diff($this->operations_arr, $this->operations_arr_disabled);
		if($is_search && in_array('sale', $operation_types)) $operation_types[] = 'sold';
		if($is_search && empty($operation_types)) $operation_types[] = 'all';
		return $operation_types;
	}
	
	/**
	 * Check all types are disabled
	 */
	public function is_operation_types_disabled(){
	    return count($this->get_operation_types()) == 0;
	}
	
	/**
	 * Return field editor type
	 * @param array $data input data
	 * @param boolean $all use disabled
	 * @return string
	 */
	public function get_field_editor_type($data, $all=true){
		if(isset($data['field_editor_type'])) return $data['field_editor_type'];
		
		if($all){
			$operation_types = $this->operations_arr;
		}else{
			$operation_types = $this->get_operation_types();
		}
		
		if(empty($operation_types)) return '';
		
		if(isset($data['operation_type']) && in_array($data['operation_type'], $operation_types)){
			$operation_type = $data['operation_type'];
		}elseif(isset($data['type']) && in_array($data['type'], $operation_types)){
			$operation_type = $data['type'];
		}elseif(isset($data['id_type']) && isset($operation_types[$data['id_type']])){
			$operation_type = $operation_types[$data['id_type']];
		}else{
			$operation_type = current($operation_types);
		}

		if(isset($data['id_category']) && isset($this->field_editor_types[$operation_type][$data['id_category']])){
			$field_editor_type = $this->field_editor_types[$operation_type][$data['id_category']];
		}else{
			$field_editor_type = current($this->field_editor_types[$operation_type]);
		}
		
		return $field_editor_type;
	}
	
	/**
	 * Return field editor type by default
	 */
	public function get_field_editor_type_default(){
		if($this->get_operation_types()) return '';
		return current($this->field_editor_types[$this->get_operation_type_gid_default()]);
	}
	
	/**
	 * Return field editor types
	 * @paramstring $operation_type operation type
	 * @param integer $category_id category identifier
	 */
	public function get_field_editor_types(){
		$operation_types = $this->get_operation_types();
		
		$field_editor_types = array();
		foreach($operation_types as $operation_type){
			$field_editor_types = array_merge($field_editor_types, $this->field_editor_types[$operation_type]);
		}
		
		return array_unique($field_editor_types);
	}
	
	/**
	 * Return category
	 * @param string $category_code category code
	 * @return string
	 */
	public function get_category($category_code){
		$operation_types = $this->operations_arr;
		
		$category_ids = array();
		foreach($operation_types as $operation_type){
			$category_ids = array_merge($category_ids, array_flip($this->field_editor_types[$operation_type]));
			foreach($this->field_editor_types[$operation_type] as $key=>$value){
				$category_ids[preg_replace('/\_'.preg_quote($operation_type, '/').'$/i', '', $value)] = $key;
			}
		}

		return $category_ids[$category_code];
	}
	
	/**
	 * Return category type
	 * @return integer
	 */
	public function get_category_default(){
		$this->CI->load->model('Properties_model');
		$default_lang = $this->CI->pg_language->current_lang_id;
		$property_types = $this->CI->Properties_model->get_categories('property_types', $default_lang);
		return key($property_types);
	}
	
	/**
	 * Return categories as array
	 * @return array
	 */
	public function get_category_ids(){
		$operation_types = $this->get_operation_types();
		
		$category_types = array();
		foreach($operation_types as $operation_type){
			$category_types = array_merge($category_types, array_keys($this->field_editor_types[$operation_type]));
		}
		
		return array_unique($category_types);
	}

	/**
	 * Get listing by identifier
	 * @param string $listing_id listing identifier
	 * @return array
	 */
	public function get_listing_by_id($listing_id){
		$this->DB->select(implode(', ', $this->_fields));
		$this->DB->from(LISTINGS_TABLE);
		$this->DB->where('id', $listing_id);
		$results = $this->DB->get()->result_array();

		if(!empty($results) && is_array($results)){
//			echo 'hello, from the other side'; exit;
			return array_shift($this->format_listings(array($results[0])));
		}
		return array();
	}
	
	/**
	 * Generate GUID for listing
	 * @return strin
	 */
	private function _create_unique_gid($data){
		$scheme = $this->listing_gid_scheme;

		$this->CI->load->library('Translit');
		$current_lang = $this->CI->pg_language->get_lang_by_id($this->CI->pg_language->current_lang_id);
		$current_lang_code = $current_lang['code'];

		///[user_name] [user_id]
		if($data['id_user']){
			$this->CI->load->model('Users_model');
			$user = $this->CI->Users_model->get_user_by_id($data['id_user'], true);
			$user_name_replace = $this->CI->translit->convert($current_lang_code, $user['output_name']);
			$user_name_replace = str_replace(' ', '-', $user_name_replace);
			$user_name_replace = preg_replace('/[-]+/i', '-', $user_name_replace);
			$user_name_replace = preg_replace('/[^a-z0-9\-_]+/i', '', $user_name_replace);
			$user_id_replace = $data['id_user'];
		}else{
			$user_login_replace = $user_id_replace = '';
		}
		$scheme = str_replace('[user_name]', $user_name_replace, $scheme);
		$scheme = str_replace('[user_id]', $user_id_replace, $scheme);

		///[location]
		if($data['id_country'] && $data['id_region'] && $data['id_city']){
			$this->CI->load->helper('countries');
			$listing_locations = cities_output_format(array(array($data['id_country'], $data['id_region'], $data['id_city'])));
			if($listing_locations[0]){
				$location_replace = $this->CI->translit->convert($current_lang_code, $listing_locations[0]);
				$location_replace = str_replace(array(' ', ','), '-', $location_replace);
				$location_replace = preg_replace('/[-]+/i', '-', $location_replace);
				$location_replace = preg_replace('/[^a-z0-9\-_]+/i', '', $location_replace);
			}else{
				$location_replace = '';
			}
		}else{
			$location_replace = '';
		}
		$scheme = str_replace('[location]', $location_replace, $scheme);

		$gid_counts = $this->get_listings_count(array('gid' => $scheme));
		if($gid_counts > 0){
			if(strlen($scheme) > 96){
				$scheme = substr($scheme, 0, 96);
			}
			$scheme = $scheme . '-'. rand(100, 999);
		}
		return $scheme;
	}
	
	/**
	 * Save listing
	 * @param integer $listing_id listing identifier
	 * @param array $data listing data
	 * @param boolean @moderation use moderation
	 * @return integer 
	 */
	public function save_listing($listing_id, $data, $moderation=false){
		// get field editor fields
		$operation_types = $this->get_operation_types();
		if(!$listing_id){
			$listing = array($data['id_category']);
		}else{
			$this->set_format_settings('use_format', false);
			$listing = $this->get_listing_by_id($listing_id);
			$this->set_format_settings('use_format', true);
		}
		
		$property_type_gid = $this->get_field_editor_type(array_merge($listing, $data));
		
		$this->CI->load->model('Field_editor_model');
		$this->CI->Field_editor_model->initialize($property_type_gid);
		
		$fields_for_select = $this->get_fields_for_select($property_type_gid);
		$fields_data = array();
		foreach($fields_for_select as $field){
			if(!isset($data[$field])) continue;
			$fields_data[$field] = $data[$field];
			unset($data[$field]);
		}
		
		// check user type
		if(isset($data['id_user'])){
			$this->CI->load->model('Users_model');
			$user = $this->CI->Users_model->get_user_by_id($data['id_user']);
			$data['user_type'] = $user['user_type'];
		}
		
		$use_moderation = $this->CI->pg_module->get_module_config('listings', 'use_moderation') && $moderation;
		if(!$listing_id){
			$data['gid'] = $this->_create_unique_gid($data);
			$data['initial_moderation'] = $use_moderation ? 0 : 1;		
			$data['date_created'] = $data['date_modified'] = date('Y-m-d H:i:s');
			
			if(in_array($this->operations_arr[$data['id_type']], array('rent', 'lease')) && !isset($data['use_calendar'])) 
				$data['use_calendar'] = 1;
	
			$this->DB->insert(LISTINGS_TABLE, $data);
			$listing_id	= $this->DB->insert_id();
		
			$fields_data['id_listing'] = $listing_id;
			$this->DB->insert(constant('LISTINGS_'.strtoupper($property_type_gid).'_TABLE'), $fields_data);
			$this->DB->insert_id();
	
			if($use_moderation){
				$this->CI->load->model('listings/models/Listings_moderation_model');
				$this->CI->Listings_moderation_model->save_listing($listing_id, array($data, $fields_data));
			}
		}else{
			if($use_moderation){
				$this->CI->load->model('listings/models/Listings_moderation_model');
				//// check listing
				$already_moderated = $this->CI->Listings_moderation_model->is_listing_moderated($listing_id);

				//// set dop fields
				$moderation_fields = $this->CI->Listings_moderation_model->get_fields_for_moderate();
				$moderation_field_editor = $this->CI->Listings_moderation_model->get_field_editor_for_moderate($property_type_gid);
				$moderation_fields = array_merge($moderation_fields, $moderation_field_editor);
		
				$saved_fields = $this->_get_field_editor_data($property_type_gid, $fields_for_select, $listing_id);
				$saved_data = array_merge($listing, array_shift($saved_fields));
	
				$content_changed = $this->CI->Listings_moderation_model->is_data_changed($listing_id, $saved_data, array_merge($data, $fields_data));
				if($already_moderated || $content_changed){
					//// выбираем из данных поля для модерации
					$moderation_data = array('id_listing'=>$listing_id, 'id_user'=>$saved_data['id_user']);
					foreach($moderation_fields as $field){
						if(isset($data[$field])){
							$moderation_data[$field] = $data[$field];
							unset($data[$field]);
						}
						if(isset($fields_data[$field])){
							$moderation_data[$field] = $fields_data[$field];
							unset($fields_data[$field]);
						}
					}
					if(!empty($moderation_data))
						$this->CI->Listings_moderation_model->save_listing($listing_id, $moderation_data);
				}
			}
		
			//remove from wish list
			if(array_key_exists('status', $data) && !$data['status'] && !empty($listing['id_wish_lists'])){
				$this->CI->load->model('listings/models/Wish_list_model');
				
				foreach($listing['id_wish_lists'] as $wish_list){
					$this->CI->Wish_list_model->delete_from_wish_list($wish_list, $listing_id, false);
				}
			
				$save_data = array('id_wish_lists'=>array());
				$validate_data = $this->validate_listing($listing_id, $save_data);
				$data['id_wish_lists'] = $validate_data['data']['id_wish_lists'];
			}
				
			$data['date_modified'] = date('Y-m-d H:i:s');
			$this->DB->where('id', $listing_id);
			$this->DB->update(LISTINGS_TABLE, $data);
		
			if(!empty($fields_data)){
				$this->DB->where('id_listing', $listing_id);
				$this->DB->update(constant('LISTINGS_'.strtoupper($property_type_gid).'_TABLE'), $fields_data);
			}
		}
		
		// update fulltext
		$this->CI->Field_editor_model->update_fulltext_field($listing_id);
		
		// update statistics
		$this->update_module_statistics($listing, $data);
		
		return $listing_id;
	}
	
	/**
	 * Save listing photo
	 * @param integer $listing_id listing identifier
	 * @param integer $photo_id photo identifier
	 * @param string $file_name file name
	 * @param boolean $moderation use moderation
	 * @return array
	 */
	public function save_photo($listing_id, $photo_id, $file_name, $moderation=true){
		$this->CI->load->model('Upload_gallery_model');
		$photo_type = $this->CI->Upload_gallery_model->get_type_by_gid($this->gallery_type);
		$data = array('type_id'=>$photo_type['id'], 'object_id'=>$listing_id);
		$return = $this->CI->Upload_gallery_model->save_file($photo_id, $data, $file_name, $moderation);
		if(!empty($return['errors'])) return $return;
	
		if(!$moderation || $photo_type['use_moderation']){
			$this->_update_logo_image($listing_id);
		}else{
			$file = $this->CI->Upload_gallery_model->get_file_by_id($return['id']);
			if($file['status']) $this->_update_logo_image($listing_id);
		}
		return $return;
	}
	
	/**
	 * Save listing photo by local file
	 * @param integer $listing_id listing identifier
	 * @param integer $photo_id photo identifier
	 * @param string $file_name file name
	 * @param boolean $moderation use moderation
	 * @return array
	 */
	public function save_local_photo($listing_id, $photo_id, $file_name, $moderation=true){
		$this->CI->load->model('Upload_gallery_model');
		$photo_type = $this->CI->Upload_gallery_model->get_type_by_gid($this->gallery_type);
		$data = array('type_id'=>$photo_type['id'], 'object_id'=>$listing_id);
		$return = $this->CI->Upload_gallery_model->save_local_file($photo_id, $data, $file_name, $moderation);
		if(!empty($return['errors'])) return $return;
	
		if(!$moderation || $photo_type['use_moderation']){
			$this->_update_logo_image($listing_id);
		}else{
			$file = $this->CI->Upload_gallery_model->get_file_by_id($return['id']);
			if($file['status'] == 1) $this->_update_logo_image($listing_id);
		}
		return $return;
	}
	
	/**
	 * Save listing panorama
	 * @param integer $listing_id listing identifier
	 * @param integer $panorama_id panorama identifier
	 * @param string $file_name file name
	 * @param boolean $moderation use moderation
	 * @return array
	 */
	public function save_panorama($listing_id, $panorama_id, $file_name, $moderation=true){
		$this->CI->load->model('Upload_gallery_model');
		$virtual_tour_type = $this->CI->Upload_gallery_model->get_type_by_gid($this->virtual_tour_type);
		$data = array('type_id'=>$virtual_tour_type['id'], 'object_id'=>$listing_id);
		$return = $this->CI->Upload_gallery_model->save_file($panorama_id, $data, $file_name, $moderation);
		if(!empty($return['errors'])) return $return;
		
		if(!$moderation || $virtual_tour_type['use_moderation']){
			$this->_update_vtour($listing_id);
		}else{
			$file = $this->CI->Upload_gallery_model->get_file_by_id($return['id']);
			if($file['status'] == 1) $this->_update_vtour($listing_id);
		}
		return $return;
	}
	
	/**
	 * Save listing panorama by local file
	 * @param integer $listing_id listing identifier
	 * @param integer $panorama_id panorama identifier
	 * @param string $file_name file name
	 * @param boolean $moderation use moderation
	 * @return array
	 */
	public function save_local_panorama($listing_id, $panorama_id, $file_name, $moderation=true){
		$this->CI->load->model('Upload_gallery_model');
		$vtour_type = $this->CI->Upload_gallery_model->get_vtour_type();
		$data = array('type_id'=>$vtour_type['id'], 'object_id'=>$listing_id);
		$return = $this->CI->Upload_gallery_model->save_local_file($panorama_id, $data, $file_name, $moderation);
		if(!empty($return['errors'])) return $return;
	
		if(!$moderation || $vtour_type['use_moderation']){
			$this->_update_vtour($listing_id);
		}else{
			$file = $this->CI->Upload_gallery_model->get_file_by_id($return['id']);
			if($file['status'] == 1) $this->_update_vtour($listing_id);
		}
		return $return;
	}
	
	/**
	 * Save listing file
	 * @param integer $listing_id listing identifier
	 * @param string $file_name file name
	 * @param boolean $moderation use moderation
	 * @return array
	 */
	public function save_file($listing_id, $file_name, $data, $moderation=false){
		if(!$this->CI->pg_module->is_module_installed('file_uploads')) return $return;
		
		$return = array('errors'=>array(), 'data'=>array());

		$this->CI->load->model('File_uploads_model');

		$validate_upload = $this->CI->File_uploads_model->validate_upload($this->file_config_id, $file_name);
	
		$this->set_format_settings(array('get_user'=>false, 'get_location'=>false));
		$listing = $this->get_listing_by_id($listing_id);
		$this->set_format_settings(array('get_user'=>true, 'get_location'=>true));

		if($moderation){
			$this->load->model('listings/models/Listings_moderation_model');

			if($this->CI->Listings_moderation_model->is_listing_moderated($listing_id)){
				$fields_for_select = $this->CI->Listings_moderation_model->get_fields_for_moderate();
				$this->CI->Listings_moderation_model->set_dop_fields($fields_for_select);
				$moderated_data = $this->CI->Listings_moderation_model->get_listing_by_id($listing_id);
				$listing = array_merge($listing, $moderated_data);
			}	
		}
	
		if(!isset($validate_upload['errors'])) $validate_upload['errors'] = array();
	
		if(isset($validate_upload['error']) && $validate_upload['error'] != ''){
			if(is_array($validate_upload['error'])){
				$validate_upload['errors'] = $validate_upload['error'];
			}else{
				$validate_upload['errors'][] = $validate_upload['error'];
			}
		}

		if(!empty($validate_upload['errors'])){
			
		}else{
			if(!empty($validate_upload['data']) && !empty($listing['listing_file'])){
				$validate_upload['errors'][] = l('error_max_files_reached', 'listings');
			}
		
			if(empty($validate_upload['data']) && empty($listing['listing_file'])){
				$validate_upload['errors'][] = l('error_empty_file', 'listings');
			}
		}

		$validate_data = $this->validate_listing($listing_id, $data);
		$validate_data['errors'] = array_merge($validate_data['errors'], $validate_upload['errors']);
		if(!empty($validate_data['errors'])){
			$return['errors'] = $validate_data['errors'];
		}else{
			if(!empty($validate_upload['data'])){
				$file_return = $this->CI->File_uploads_model->upload($this->file_config_id, $listing['prefix'], $file_name);
				$validate_data['data']['listing_file'] = $file_return['file'];
				$validate_data['data']['listing_file_date'] = date('Y-m-d H:i:s');
			}
			$this->save_listing($listing_id, $validate_data['data'], $moderation);
			$return['data']['file'] = $file_return['file'];
		}
		return $return;
	}
	
	/**
	 * Save local listing file
	 * @param integer $listing_id listing identifier
	 * @param string $file_name file name
	 * @param boolean $moderation use moderation
	 * @return array
	 */
	public function save_local_file($listing_id, $file_name, $data, $moderation=false){
		if(!$this->CI->pg_module->is_module_installed('file_uploads')) return $return;
		
		$return = array('errors'=>array(), 'data'=>array());

		$this->CI->load->model('File_uploads_model');

		$this->set_format_settings('use_format', false);
		$listing = $this->get_listing_by_id($listing_id);
		$this->set_format_settings('use_format', true);

		if($moderation){
			$this->load->model('listings/models/Listings_moderation_model');

			if($this->CI->Listings_moderation_model->is_listing_moderated($listing_id)){
				$fields_for_select = $this->CI->Listings_moderation_model->get_fields_for_moderate();
				$this->CI->Listings_moderation_model->set_dop_fields($fields_for_select);
				$moderated_data = $this->CI->Listings_moderation_model->get_listing_by_id($listing_id);
				$listing = array_merge($listing, $moderated_data);
			}	
		}
	
		$validate_upload['errors'] = array();
	
		if(!empty($listing['listing_file'])){
			$validate_upload['errors'][] = l('error_max_files_reached', 'listings');
		}
		
		$validate_data = $this->validate_listing($listing_id, $data);
		$validate_data['errors'] = array_merge($validate_data['errors'], $validate_upload['errors']);
		if(!empty($validate_data['errors'])){
			$return['errors'] = $validate_data['errors'];
		}else{
			if(!empty($validate_upload['data'])){
				$file_return = $this->CI->File_uploads_model->upload_exist($this->file_config_id, $listing['prefix'], $file_name);
				$validate_data['data']['listing_file'] = $file_return['file'];
				$validate_data['data']['listing_file_date'] = date('Y-m-d H:i:s');
			}
			$this->save_listing($listing_id, $validate_data['data'], $moderation);
			$return['data']['file'] = $file_return['file'];
		}
		return $return;
	}
	
	/**
	 * Save video name
	 * @param integer $listing_id listing identifier
	 * @param string $video_name video name
	 * @param boolean $moderation use moderation
	 * @return array
	 */
	public function save_video($listing_id, $video_name, $moderation=false){
		if(!$this->CI->pg_module->is_module_installed('video_uploads')) return $return;
		
		$return = array('errors'=>array(), 'data'=>array());

		if(empty($listing_id) || empty($video_name)){
			$return['errors'][] = l('error_empty_video', 'listings'); 
			return $return;
		}

		if(!isset($_FILES[$video_name]) || !is_array($_FILES[$video_name]) || $_FILES[$video_name]['error'] == 4){
			return $return;
		}

		if(!is_uploaded_file($_FILES[$video_name]['tmp_name'])){
			$return['errors'][] = l('error_upload_video', 'listings'); 
			return $return;
		}
		
		$listing = $this->get_listing_by_id($listing_id);
		
		if($moderation){
			$this->CI->load->model('listings/models/Listings_moderation_model');
			if($this->CI->Listings_moderation_model->is_listing_moderated($listing_id)){
				$fields_for_select = $this->CI->Listings_moderation_model->get_fields_for_moderate();
				$this->CI->Listings_moderation_model->set_dop_fields($fields_for_select);
				$moderated_data = $this->CI->Listings_moderation_model->get_listing_by_id($listing_id);
				$listing = array_merge($listing, $moderated_data);
			}	
		}
		
		if(!empty($listing['listing_video'])){
			$return['errors'][] = l('error_max_video_reached', 'listings');
		}else{
			$this->CI->load->model('Video_uploads_model');
			$video_data = array(
				'name' => $listing['location'],
				'description' => $listing['category_str'].'_'.$listing['property_type_str'],
			);
	
			$video_return = $this->CI->Video_uploads_model->upload($this->video_config_id, $listing['prefix'], $video_name, $listing_id, $video_data);
			if(!empty($video_return['errors'])){
				$return['errors'] = $video_return['errors']; 
			}else{
				$return['data']['file'] = $video_return['file'];
			}
		}

		return $return;
	}
	
	/**
	 * Save local video name
	 * @param integer $listing_id listing identifier
	 * @param string $video_name video name
	 * @param boolean $moderation use moderation
	 * @return array
	 */
	public function save_local_video($listing_id, $video_name, $moderation=false){
		if(!$this->CI->pg_module->is_module_installed('video_uploads')) return $return;
		
		$return = array('errors'=>array(), 'data'=>array());

		if(empty($listing_id) || empty($video_name)){
			$return['errors'][] = l('error_empty_video', 'listings'); 
			return $return;
		}
		
		$listing = $this->get_listing_by_id($listing_id);
		
		if($moderation){
			$this->CI->load->model('listings/models/Listings_moderation_model');
			if($this->CI->Listings_moderation_model->is_listing_moderated($listing_id)){
				$fields_for_select = $this->CI->Listings_moderation_model->get_fields_for_moderate();
				$this->CI->Listings_moderation_model->set_dop_fields($fields_for_select);
				$moderated_data = $this->CI->Listings_moderation_model->get_listing_by_id($listing_id);
				$listing = array_merge($listing, $moderated_data);
			}	
		}
		
		if(!empty($listing['listing_video'])){
			$return['errors'][] = l('error_max_video_reached', 'listings');
		}else{
			$this->CI->load->model('Video_uploads_model');
			$video_data = array(
				'name' => $listing['location'],
				'description' => $listing['category_str'].'_'.$listing['property_type_str'],
			);
	
			$video_return = $this->CI->Video_uploads_model->upload_exists($this->video_config_id, $listing['prefix'], $video_name, $listing_id, $video_data);
			if(!empty($video_return['errors'])){
				$return['errors'] = $video_return['errors']; 
			}else{
				$return['data']['file'] = $video_return['file'];
			}
		}

		return $return;
	}
	
	/**
	 * Remove listing
	 * @param integer $listing_id
	 */
	public function delete_listing($listing_id){
		$listing = $this->get_listing_by_id($listing_id);
		
		$property_type_gid = $this->get_field_editor_type($listing);
		
		// remove listing
		$this->DB->where('id', $listing_id);
		$this->DB->delete(LISTINGS_TABLE);
		
		// remove details
		$this->DB->where('id_listing', $listing_id);
		$this->DB->delete(constant('LISTINGS_'.strtoupper($property_type_gid).'_TABLE'));
		
		/// remove uploads
		$this->CI->load->model('Uploads_model');
		if(!empty($listing['logo_image'])){
			$this->CI->Uploads_model->delete_upload($this->upload_config_id, $listing_id.'/', $listing['logo_image']);
		}
		$this->CI->Uploads_model->delete_path($this->upload_config_id, $listing_id.'/');
		
		if(!empty($listing['slider_image'])){
			$this->CI->Uploads_model->delete_upload($this->slider_config_id, $listing_id.'/', $listing['slider_image']);
		}
		$this->CI->Uploads_model->delete_path($this->slider_config_id, $listing_id.'/');
		
		// remove upload gallery
		$this->CI->load->model('Upload_gallery_model');
		$gallery_type = $this->CI->Upload_gallery_model->get_type_by_gid($this->gallery_type);
		$photos_params = array('where'=>array('type_id' => $gallery_type['id'], 'object_id' => $listing_id));
		$photos = $this->CI->Upload_gallery_model->get_files_by_param($photos_params);
		foreach($photos as $photo) $this->CI->Upload_gallery_model->delete_file($photo['id']);
		$this->CI->Upload_gallery_model->delete_path($gallery_type, $listing_id);
		unset($photos);
		
		//remove virtual tour
		$virtual_tour_type = $this->CI->Upload_gallery_model->get_type_by_gid($this->virtual_tour_type);
		$panorama_params = array('where'=>array('type_id'=>$virtual_tour_type['id'], 'object_id'=>$listing_id));
		$panorama_arr = $this->CI->Upload_gallery_model->get_files_by_param($panorama_params);
		foreach($panorama_arr as $panorama)	$this->CI->Upload_gallery_model->delete_file($panorama['id']);
		$this->CI->Upload_gallery_model->delete_path($virtual_tour_type, $listing_id);
		unset($panorama_arr);
		
		// remove file
		if($this->CI->pg_module->is_module_installed('file_uploads') && !empty($listing['listing_file'])){
			$this->CI->load->model('File_uploads_model');
			$this->CI->File_uploads_model->delete_upload($this->file_config_id, $listing['prefix'], $listing['listing_file']);
		}

		if($this->CI->pg_module->is_module_installed('video_uploads') && !empty($listing['listing_video'])){
			$this->CI->load->model('Video_uploads_model');
			$this->CI->Video_uploads_model->delete_upload($this->video_config_id, $listing['prefix'], $listing['listing_video'], $listing['listing_video_data']['data']['upload_type']);
		}
		
		// remove from moderation
		$this->CI->load->model('listings/models/Listings_moderation_model');
		$this->CI->Listings_moderation_model->delete_listing($listing_id);
		$this->CI->Listings_moderation_model->delete_alert($listing_id);
		
		// remove from moderation
		//$this->CI->load->model('Moderation_model');
		//$this->CI->Moderation_model->delete_moderation_item_by_obj($this->moderation_type, $listing_id);
		
		// remove from wish lists
		if(!empty($listing['id_wish_lists'])){
			$this->CI->load->model('listings/models/Wish_list_model');
			foreach($data['id_wish_lists'] as $wish_list){
				$this->CI->Wish_list_model->delete_from_wish_list($wish_list, $listing_id);
			}
		}
		
		// update module statistics
		$this->update_module_statistics($listing, null);
	}
	
	/**
	 * Remove listing photo 
	 * @param integer $listing_id listing identifier
	 * @param integer $photo_id photo identifier
	 */
	public function delete_photo($listing_id, $photo_id){
		$this->CI->load->model('Upload_gallery_model');
		$photo_delete = $this->CI->Upload_gallery_model->get_file_by_id($photo_id);
		if($listing_id != $photo_delete['object_id']) return false;
		$this->CI->Upload_gallery_model->delete_file($photo_id);
		$this->_update_logo_image($listing_id);
		return true;
	}
	
	/**
	 * Remove listing photos 
	 * @param integer $listing_id listing identifier
	 */
	public function delete_photos($listing_id){
		$gallery_type = $this->get_gallery_type();
		$params = array('where'=>array('type_id'=>$gallery_type['id'], 'object_id'=>$listing_id));
		$this->CI->load->model('Upload_gallery_model');
		$this->CI->Upload_gallery_model->delete_files_by_param($params);
		$this->_update_logo_image($listing_id);
		return true;
	}
	
	/**
	 * Remove listing panorama
	 * @param integer $listing_id listing identifier
	 * @param integer $panorama_id panorama identifier
	 */
	public function delete_panorama($listing_id, $panorama_id){
		$this->CI->load->model('Upload_gallery_model');
		$panorama_delete = $this->CI->Upload_gallery_model->get_file_by_id($panorama_id);
		if($listing_id != $panorama_delete['object_id']) return false;
		$this->CI->Upload_gallery_model->delete_file($panorama_id);
		$this->_update_vtour($listing_id, false);
		return true;
	}
	
	/**
	 * Remove listing photos 
	 * @param integer $listing_id listing identifier
	 */
	public function delete_panoramas($listing_id){
		$vtour_type = $this->get_vtour_type();
		$params = array('where'=>array('type_id'=>$vtour_type['id'], 'object_id'=>$listing_id));
		$this->CI->load->model('Upload_gallery_model');
		$this->CI->Upload_gallery_model->delete_files_by_param($params);
		$this->_update_vtour($listing_id, false);
		return true;
	}
	
	/**
	 * Remove listing file 
	 * @param integer $listing_id listing identifier
	 */
	public function delete_file($listing_id){
		$this->set_format_settings('get_file', true);
		$listing = $this->get_listing_by_id($listing_id);
		$this->set_format_settings('get_file', false);
		
		$this->CI->load->model('File_uploads_model');
		$this->CI->File_uploads_model->delete_upload($this->file_config_id, $listing['prefix'], $listing['listing_file']);
		
		$listing_data = array(
			'listing_file' => '',
			'listing_file_name' => '',
			'listing_file_comment' => '',
		);
		$this->save_listing($listing_id, $listing_data);
	}
	
	/**
	 * Remove listing files
	 * @param integer $listing_id listing identifier
	 */
	public function delete_files($listing_id){
		$this->delete_file($listing_id);
	}
	
	/**
	 * Remove listing video
	 * @param integer $listing_id listing identifier
	 */
	public function delete_video($listing_id){
		$this->set_format_settings('get_video', true);
		$listing = $this->get_listing_by_id($listing_id);
		$this->set_format_settings('get_video', false);
		
		$this->load->model('Video_uploads_model');
		$this->Video_uploads_model->delete_upload($this->video_config_id, $listing['prefix'], $listing['listing_video'], $listing['listing_video_image'], $listing['listing_video_data']['data']['upload_type']);
		
		$listing_data = array(
			'listing_video' => '',
			'listing_video_image' => '',
			'listing_video_data' => '',
		);
		$this->Listings_model->save_listing($listing_id, $listing_data);
	}
	
	/**
	 * Remove listing videos
	 * @param integer $listing_id listing identifier
	 */
	public function delete_videos($listing_id){
		$this->delete_video($listing_id);
	}
	
	/**
	 * Activate listing
	 * @param integer $listing_id listing identifier
	 * @param integer $status listing status
	 * @param boolean $date_expire save expired date
	 */
	public function activate_listing($listing_id, $status=1, $date_expired=false){
		$data = array('status'=>intval($status));
		if($status) $data['date_activity'] = date('Y-m-d H:i:s', time());
		if($date_expire !== false) $data['date_expire'] = $date_expired;
		$this->save_listing($listing_id, $data);
	}
	
	/**
	 * Return search criteria as array
	 * @param array $filters filters data
	 * @return array
	 */
	private function _get_search_criteria($filters){
		$params = array();
		
		// by price range
		if(!empty($filters['price_min'])){
			$filters['price_range'][2] = $filters['price_min']; // original $filters['price_range'][0]
			unset($filters['price_min']);
		}elseif($filters['price_min'] == 0 || $filters['price_min'] == NULL){ // #MOD#
			$filters['price_range'][2] = 0000000; 
		}
	
		if(!empty($filters['price_max'])){
			$filters['price_range'][1] = $filters['price_max'];
			//$params = array_merge_recursive($params, array('where'=>array(LISTINGS_TABLE.'.id_user !='=>27)));
			unset($filters['price_max']);
		}

		// by open house
		if(!empty($filters['by_open_house'])){
			$filters['open_house'] = array(date('Y-m-d'), null);
			unset($filters['by_open_house']);
		}
		
		// By private
		if(!empty($filters['by_private'])){
			$filters['user_type'] = 'private';
			unset($filters['by_private']);
		}
		
		// By agents
		if(!empty($filters['by_agents'])){
			$this->CI->load->model('Users_model');
			$filters['user_type'] = $this->CI->Users_model->agents_arr;
			unset($filters['by_agents']);
		}
		
		// By keyword
		if(!empty($filters['keyword'])){
			if(isset($filters['keyword_mode'])){
				$filters['keyword'] .= '*';
				$keyword_mode = 'BOOLEAN MODE';
				unset($filters['keyword_mode']);
			}
			
			$fe_criteria = array();
			
			$this->CI->load->model('Field_editor_model');
			$property_type_gids = $this->get_field_editor_types();
			foreach($property_type_gids as $property_type_gid){
				$this->CI->Field_editor_model->initialize($property_type_gid);
				$temp_criteria = $this->CI->Field_editor_model->return_fulltext_criteria($filters['keyword'], $keyword_mode);
				if(!empty($temp_criteria['listings_'.$property_type_gid]['where_sql'])){
					$field_editor_table = constant('LISTINGS_'.strtoupper($property_type_gid).'_TABLE');
					$fe_criteria[] = str_replace('search_field', $field_editor_table.'.search_field', $temp_criteria['listings_'.$property_type_gid]['where_sql']);
				}
			}
			if(!empty($fe_criteria)){
				$fe_criteria = array('where_sql'=>array('('.implode(' OR ', array_unique($fe_criteria)).')'));
				if(isset($filters['field_editor'])){
					$filters['field_editor'][0] = array_merge($filters['field_editor'][0], $property_type_gids);
					$filters['field_editor'][1] = array_merge($filters['field_editor'][1], $fe_criteria);
				}else{
					$filters['field_editor'] = array($property_type_gids, $fe_criteria);
				}
			}
		}

		// Field editor forms
		if(!empty($filters['field_editor_data'])){
			$field_editor_data = $filters['field_editor_data'];
			unset($filters['field_editor_data']);
			
			/*if(empty($filters['operation_type'])) $filters['operation_type'] = current($this->operation_arr);
			if(empty($filters['id_category'])) $filters['id_category'] = key($this->field_editor_types);
			#MOD disable for by all category listing#
			*/
			
			
			$fe_criteria = array();
			
			$property_type_gid = $this->get_field_editor_type($filters);
			$this->CI->load->model('field_editor/models/Field_editor_forms_model');
			foreach($field_editor_data as $form_name=>$form_data){
				foreach($form_data as $field_gid=>$value){
					if(isset($value['range']) && is_array($value['range'])){
						if(isset($value['range']['min'])) $form_data[$field_gid.'_min'] = $value['range']['min'];
						if(isset($value['range']['max'])) $form_data[$field_gid.'_max'] = $value['range']['max'];
						unset($form_data[$field_gid]);
					}
				}
				$form_gid = $this->{$form_name.'_'.$property_type_gid.'_gid'};
				$temp = $this->CI->Field_editor_forms_model->get_search_criteria($form_gid, $form_data, $property_type_gid);
				if(!empty($temp)) $fe_criteria = array_merge($fe_criteria, $temp);
			}
			if(!empty($fe_criteria)){
				if(isset($filters['field_editor'])){
					if(!in_array($property_type_gid, $filters['field_editor'][0]))
						$filters['field_editor'][0][] = $property_type_gid;
					$filters['field_editor'][1] = array_merge($filters['field_editor'][1], $fe_criteria);
				}else{
					$filters['field_editor'] = array(array($property_type_gid), $fe_criteria);
				}
			}
		}
		
		//booking
		if(isset($filters['booking_date_start'])){
			$filters['booking']['date_start'] = $filters['booking_date_start'];
			unset($filters['booking_date_start']);
		}
		
		if(isset($filters['booking_date_end'])){
			$filters['booking']['date_end'] = $filters['booking_date_end'];
			unset($filters['booking_date_end']);
		}
		
		if(isset($filters['booking_guests'])){
			$filters['booking']['guests'] = $filters['booking_guests'];
			unset($filters['booking_guests']);
		}
	
		if(empty($filters)) return $params;
		
		//$params //= array_merge_recursive($params, array('where'=>array(LISTINGS_TABLE.'.id_user !='=>27)));
		
		$fields = array_flip($this->_fields);
		foreach($filters as $filter_name=>$filter_data){
			if(!is_array($filter_data)) $filter_data = trim($filter_data);
			switch($filter_name){
				// By active
				case 'active':
					if(!$filter_data) break;
					$params = array_merge_recursive($params, array('where'=>array(LISTINGS_TABLE.'.status'=>1)));
				break;
				// By not active
				case 'not_active':
					if(!$filter_data) break;
					$params = array_merge_recursive($params, array('where'=>array(LISTINGS_TABLE.'.status'=>0)));
				break;
				// By not sold
				case 'not_sold':
					if(!$filter_data) break;
					$params = array_merge_recursive($params, array('where'=>array(LISTINGS_TABLE.'.sold'=>0)));
				break;
				// By country
				case 'country':
					if(!$filter_data) break;
					$params = array_merge_recursive($params, array('where'=>array(LISTINGS_TABLE.'.id_country'=>$filter_data)));
				break;
				// By region
				case 'region':
					if(!$filter_data) break;
					$params = array_merge_recursive($params, array('where'=>array(LISTINGS_TABLE.'.id_region'=>$filter_data)));
				break;
				// By city
				case 'city':
					if(!$filter_data) break;
					$params = array_merge_recursive($params, array('where'=>array(LISTINGS_TABLE.'.id_city'=>$filter_data)));
				break;
				// By type
				case 'type':
					if(empty($filter_data)) break;
					if($filter_data == 'sold'){
						$filter_data = 'sale';
						$sold = 1;
					}elseif($filter_data == 'sale'){
						$sold = 0;
					}
					$type_id = $this->get_operation_type_by_gid($filter_data);
					if(!$type_id) break;
					$params = array_merge_recursive($params, array('where'=>array(LISTINGS_TABLE.'.id_type'=>$type_id)));
					if(isset($sold)){
						$params = array_merge_recursive($params, array('where'=>array(LISTINGS_TABLE.'.sold'=>$sold)));
					}
				break;
				// By category
				case 'category':
					if(!$filter_data) break;
					$params = array_merge_recursive($params, array('where'=>array(LISTINGS_TABLE.'.id_category'=>$filter_data)));
				break;
				// By user
				case 'user':
					if(!$filter_data) break;
					$params = array_merge_recursive($params, array('where'=>array(LISTINGS_TABLE.'.id_user'=>$filter_data)));
				break;
				// Exlude user
				case 'not_user':
					if(!$filter_data) break;
					$params = array_merge_recursive($params, array('where'=>array(LISTINGS_TABLE.'.id_user !='=>$filter_data)));
				break;
				// By ids
				case 'ids':
					if(empty($filter_data)) break;
					if(is_array($filter_data)){
						$params = array_merge_recursive($params, array('where_in'=>array(LISTINGS_TABLE.'.id'=>$filter_data)));
					}else{
						$params = array_merge_recursive($params, array('where'=>array(LISTINGS_TABLE.'.id'=>$filter_data)));
					}
				break;
				// Exclude listing
				case 'exclude_id':
					if(empty($filter_data)) break;
					if(is_array($filter_data)){
						$params = array_merge_recursive($params, array('where_not_in'=>array(LISTINGS_TABLE.'.id'=>$filter_data)));
					}else{
						$params = array_merge_recursive($params, array('where'=>array(LISTINGS_TABLE.'.id !='=>$filter_data)));
					}
				break;
				case 'price_range':
					if(empty($filter_data)) break;
					$where = array();
					if($this->CI->pg_module->is_module_installed('payments')){
						$this->CI->load->model('payments/models/Payment_currency_model');
						if(is_array($filter_data)){
							if(!empty($filter_data[1])){
								$filter_data[1] = $this->CI->Payment_currency_model->get_value_into_base_currency(
									$filter_data[1], $this->CI->Payment_currency_model->default_currency['gid']);
										$filter_data[2] = $this->CI->Payment_currency_model->get_value_into_base_currency(
											$filter_data[2], $this->CI->Payment_currency_model->default_currency['gid']);
								$where[] = LISTINGS_TABLE.'.price_sorting>='.$this->DB->escape($filter_data[2]).' AND '.LISTINGS_TABLE.'.price_sorting<='.$this->DB->escape($filter_data[1]); // #MOD search min max price
							}
							if(!empty($filter_data[0])){
								$filter_data[0] = $this->CI->Payment_currency_model->get_value_into_base_currency(
									$filter_data[0], $this->CI->Payment_currency_model->default_currency['gid']);
								$where[] = '(CASE WHEN '.LISTINGS_TABLE.'.id_type='.$this->DB->escape($this->get_operations_type_id('buy')).' OR '.LISTINGS_TABLE.'.id_type='.$this->DB->escape($this->get_operations_type_id('lease')).' THEN ('.LISTINGS_TABLE.'.price_max_sorting=0 OR price_max_sorting>='.$this->DB->escape($filter_data[0]).') ELSE '.LISTINGS_TABLE.'.price_sorting>='.$this->DB->escape($filter_data[0]).' END)';
							}
						}else{
							$filter_data = $this->CI->Payment_currency_model->get_value_into_base_currency(
								$filter_data, $this->CI->Payment_currency_model->default_currency['gid']);
							$where[] = LISTINGS_TABLE.'.price_sorting>='.$this->DB->escape($filter_data);
						}
					}else{
						if(is_array($filter_data)){
							if(!empty($filter_data[1])){
								$where[] = '(CASE WHEN '.LISTINGS_TABLE.'.price_reduced THEN '.LISTINGS_TABLE.'.price_reduced ELSE '.LISTINGS_TABLE.'.price END)<='.$this->DB->escape($filter_data[1]);	
							}
							if(!empty($filter_data[0])){
								$where[] = '(CASE WHEN '.LISTINGS_TABLE.'.id_type='.$this->DB->escape($this->get_operations_type_id('buy')).' OR '.LISTINGS_TABLE.'.id_type='.$this->DB->escape($this->get_operations_type_id('lease')).' THEN ('.LISTINGS_TABLE.'.price_max=0 OR price_max>='.$this->DB->escape($filter_data[0]).') ELSE (CASE WHEN '.LISTINGS_TABLE.'.price_reduced THEN '.LISTINGS_TABLE.'.price_reduced ELSE '.LISTINGS_TABLE.'.price END)>='.$this->DB->escape($filter_data[0]).' END)';
							}
						}else{
							$filter_data = $this->CI->Payment_currency_model->get_value_into_base_currency(
								$filter_data, $this->CI->Payment_currency_model->default_currency['gid']);
							$where[] = LISTINGS_TABLE.'.price_sorting>='.$this->DB->escape($filter_data);
						}
					}
					if(!empty($where)) $params = array_merge_recursive($params, array('where_sql'=>array('('.LISTINGS_TABLE.'.price_negotiated='.$this->DB->escape(1).' OR '.implode(' AND ', $where).')')));
				break;
				case 'with_photo':
					if(empty($filter_data)) break;
					$params = array_merge_recursive($params, array('where'=>array(LISTINGS_TABLE.'.photo_count >'=>0)));
				break;
				case 'open_house':
					if(empty($filter_data)) break;
					if(is_array($filter_data)){
						$where = array();
						if(!empty($filter_data[0])){
							$where[LISTINGS_TABLE.'.date_open >='] = $filter_data[0];
							if(!empty($filter_data[1])){
								$where[LISTINGS_TABLE.'.date_open_begin >='] = $filter_data[1];
							}
							if(!empty($filter_data[2])){
								$where[LISTINGS_TABLE.'.date_open_end <='] = $filter_data[2];
							}
						}						
					}else{
						$where = array(LISTINGS_TABLE.'.date_open >='=>$filter_data);
					}
					$params = array_merge_recursive($params, array('where'=>$where));
				break;
				case 'user_type':
					if(empty($filter_data)) break;
					if(is_array($filter_data)){
						$params = array_merge_recursive($params, array('where_in'=>array(LISTINGS_TABLE.'.user_type'=>$filter_data)));
					}else{
						$params = array_merge_recursive($params, array('where'=>array(LISTINGS_TABLE.'.user_type'=>$filter_data)));
					}
				break;
				// By featured
				case 'featured':
					if(empty($filter_data)) break;
					$params = array_merge_recursive($params, array('where'=>array(LISTINGS_TABLE.'.featured_date_end >'=>date('Y-m-d H:i:s'))));
				break;
				// By slider image
				case 'slider_image':
					if(empty($filter_data)) break;
					$params = array_merge_recursive($params, array('where_sql'=>array(LISTINGS_TABLE.'.slider_image !='.$this->DB->escape(''))));
					//$params = array_merge_recursive(array('where_sql'=>array(LISTINGS_TABLE.'.id <= 113')));
				break;
				// By booking
				case 'booking':
					$this->CI->load->model('listings/models/Listings_booking_model');
					$ids = $this->CI->Listings_booking_model->get_booking_listings($filter_data);
					if(empty($ids)) break;
					$params = array_merge_recursive($params, array('where_not_in'=>array(LISTINGS_TABLE.'.id'=>$ids)));
				break;
				// By field editor
				case 'field_editor':
					if(empty($filter_data)) break;
					foreach($filter_data[0] as $key=>$property_type_gid){
						$filter_data[0][$key] = constant('LISTINGS_'.strtoupper($property_type_gid).'_TABLE');
					}
					$filter_data[0] = array_unique($filter_data[0]);
					foreach($filter_data[0] as $key=>$field_editor_table){
						$filter_data[0][$key] = array('table'=>$field_editor_table, 'condition'=>LISTINGS_TABLE.'.id='.$field_editor_table.'.id_listing', 'type'=>'LEFT');
					}
					$params = array_merge_recursive($params, array('join'=>$filter_data[0]), $filter_data[1]);
				break;
				// By discount
				case 'by_discount':
					if(empty($filter_data)) break;
					$params = array_merge_recursive($params, array('where'=>array(LISTINGS_TABLE.'.price_reduced >'=>0)));
				break;
				// By field
				default:
					if(isset($fields[$filter_name])) 
						$params = array_merge_recursive($params, array('where'=>array(LISTINGS_TABLE.'.'.$filter_name=>$filter_data)));	
				break;
			}
		}
			return $params;	
	}
		
	/**
	 * Return listings as array
	 * @param integer $page
	 * @param string $limits 
	 * @param array $order_by
	 * @param array $params
	 * @param boolean $formatted
	 * @return array
	 */
	private function _get_listings_list($page=null, $limits=null, $order_by=null, $params=array(), $formatted=true){
		$this->DB->select(LISTINGS_TABLE.'.'.implode(', '.LISTINGS_TABLE.'.', $this->_fields));
		$this->DB->from(LISTINGS_TABLE);
		//$this->DB->select(LISTINGS_TABLE.'.'.implode(', '.LISTINGS_TABLE.'.', $this->_fields).", ".LISTINGS_RESIDENTIAL_SALE_TABLE.'.'.implode(', '.LISTINGS_RESIDENTIAL_SALE_TABLE.'.', $this->residential));
		//$this->DB->from(LISTINGS_TABLE);
		//$this->db->join(LISTINGS_RESIDENTIAL_SALE_TABLE, 'pg_listings_residential.id_listing = pg_listings.id');

		if(isset($params['join']) && is_array($params['join']) && count($params['join'])){
			foreach($params['join'] as $join){
				$this->DB->join($join['table'], $join['condition'], $join['type']);
			}
		}

		if(isset($params['where']) && is_array($params['where']) && count($params['where'])){
			foreach($params['where'] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params['where_in']) && is_array($params['where_in']) && count($params['where_in'])){
			foreach($params['where_in'] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}
		
		if(isset($params['where_not_in']) && is_array($params['where_not_in']) && count($params['where_not_in'])){
			foreach($params['where_not_in'] as $field=>$value){
				$this->DB->where_not_in($field, $value);
			}
		}

		if(isset($params['where_sql']) && is_array($params['where_sql']) && count($params['where_sql'])){
			foreach($params['where_sql'] as $value){
				$this->DB->where($value, null, false);
			}
		}
		
		if(is_array($order_by) && count($order_by) > 0){
			foreach($order_by as $field => $dir){
				if(in_array($field, $this->_fields)){
					$this->DB->order_by($field.' '.$dir);
				}
			}
		}elseif($order_by){
			$this->DB->order_by($order_by);
		}
		
		if(!is_null($page)){
			$page = intval($page)?intval($page):1;
			$this->DB->limit($limits, $limits*($page-1));
		}
	
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			foreach($results as $r){
				$data[] = $r;
			}
			if($formatted){
				$data = $this->format_listings($data);
			}
			return $data;
		}
		return array();
	}
	
	
	// additional function get listing for slider
	private function _get_listings_list_slider($page=null, $limits=null, $order_by=null, $params=array(), $formatted=true){
		$this->DB->select(LISTINGS_TABLE.'.'.implode(', '.LISTINGS_TABLE.'.', $this->_fields));
		$this->DB->from(LISTINGS_TABLE);
		//$this->DB->select(LISTINGS_TABLE.'.'.implode(', '.LISTINGS_TABLE.'.', $this->_fields).", ".LISTINGS_RESIDENTIAL_SALE_TABLE.'.'.implode(', '.LISTINGS_RESIDENTIAL_SALE_TABLE.'.', $this->residential));
		//$this->DB->from(LISTINGS_TABLE);
		//$this->db->join(LISTINGS_RESIDENTIAL_SALE_TABLE, 'pg_listings_residential.id_listing = pg_listings.id');

		if(isset($params['join']) && is_array($params['join']) && count($params['join'])){
			foreach($params['join'] as $join){
				$this->DB->join($join['table'], $join['condition'], $join['type']);
			}
		}

		if(isset($params['where']) && is_array($params['where']) && count($params['where'])){
			foreach($params['where'] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params['where_in']) && is_array($params['where_in']) && count($params['where_in'])){
			foreach($params['where_in'] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}
		
		if(isset($params['where_not_in']) && is_array($params['where_not_in']) && count($params['where_not_in'])){
			foreach($params['where_not_in'] as $field=>$value){
				$this->DB->where_not_in($field, $value);
			}
		}

		if(isset($params['where_sql']) && is_array($params['where_sql']) && count($params['where_sql'])){
			foreach($params['where_sql'] as $value){
				$this->DB->where($value, null, false);
			}
		}
		
		if(is_array($order_by) && count($order_by) > 0){
			foreach($order_by as $field => $dir){
				if(in_array($field, $this->_fields)){
					$this->DB->order_by($field.' '.$dir);
				}
			}
		}elseif($order_by){
			$this->DB->order_by($order_by);
		}
		
		if(!is_null($page)){
			$page = intval($page)?intval($page):1;
			$this->DB->limit($limits, $limits*($page-1));
		}
	
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			foreach($results as $r){
				$data[] = $r;
			}
			if($formatted){
				$data = $this->format_listings($data);
			}
			return $data;
		}
		return array();
	}
	
	
	/**
	 * Return number of listings
	 * @param array $params
	 * @return integer
	 */
	private function _get_listings_count($params=null){
		$this->DB->select('COUNT(*) AS cnt');
		$this->DB->from(LISTINGS_TABLE);
		
		if(isset($params['join']) && is_array($params['join']) && count($params['join'])){
			foreach($params['join'] as $join){
				$this->DB->join($join['table'], $join['condition'], $join['type']);
			}
		}
		
		if(isset($params['where']) && is_array($params['where']) && count($params['where'])){
			foreach($params['where'] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params['where_in']) && is_array($params['where_in']) && count($params['where_in'])){
			foreach($params['where_in'] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}
		
		if(isset($params['where_not_in']) && is_array($params['where_not_in']) && count($params['where_not_in'])){
			foreach($params['where_not_in'] as $field=>$value){
				$this->DB->where_not_in($field, $value);
			}
		}

		if(isset($params['where_sql']) && is_array($params['where_sql']) && count($params['where_sql'])){
			foreach($params['where_sql'] as $value){
				$this->DB->where($value, null, false);
			}
		}
		
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			return intval($results[0]['cnt']);
		}
		return 0;
	}
	
	/**
	 * Return list of filtered listings
	 * @param array $filters
	 * @param integer $page
	 * @param integer $items_on_page
	 * @param string $order_by
	 * @param boolean $formatted
	 */
	public function get_listings_list($filters=array(), $page=null, $items_on_page=null, $order_by=null, $formatted=true){
		$params = $this->_get_search_criteria($filters);
		return $this->_get_listings_list($page, $items_on_page, $order_by, $params, $formatted);
	}
	
	public function get_listings_list_slider($filters=array(), $page=null, $items_on_page=null, $order_by=null, $formatted=true){
		$params = $this->_get_search_criteria($filters);
		return $this->_get_listings_list_slider($page, $items_on_page, $order_by, $params, $formatted);
	}
	
	/**
	 * Return number of filtered listings
	 * @param array $filters
	 * @return array
	 */
	public function get_listings_count($filters=array()){
		$params = $this->_get_search_criteria($filters);
		return $this->_get_listings_count($params);
	}
	
	/**
	 * Return pagination of filtered listings 
	 * @param array $filters
	 * @param integer $page
	 * @param integer $items_on_page
	 * @param string $order_by
	 * @param boolean $formatted
	 */
	public function get_listings_pagination($filters=array(), $page=null, $limits=null, $order_by=null, $formatted=true){
		$params = $this->_get_search_criteria($filters);
		
		$this->DB->select(LISTINGS_TABLE.'.'.implode(', '.LISTINGS_TABLE.'.', $this->_fields));
		$this->DB->from(LISTINGS_TABLE);
	
		if(isset($params['join']) && is_array($params['join']) && count($params['join'])){
			foreach($params['join'] as $join){
				$this->DB->join($join['table'], $join['condition'], $join['type']);
			}
		}
		
		if(isset($params['where']) && is_array($params['where']) && count($params['where'])){
			foreach($params['where'] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params['where_in']) && is_array($params['where_in']) && count($params['where_in'])){
			foreach($params['where_in'] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params['where_sql']) && is_array($params['where_sql']) && count($params['where_sql'])){
			foreach($params['where_sql'] as $value){
				$this->DB->where($value, null, false);
			}
		}

		if(is_array($order_by) && count($order_by) > 0){
			foreach($order_by as $field => $dir){
				if(in_array($field, $this->_fields)){
					$this->DB->order_by($field.' '.$dir);
				}
			}
		}elseif($order_by){
			$this->DB->order_by($order_by);
		}

		if(!is_null($page)){
			$page = intval($page)?intval($page):1;
			$this->DB->limit($limits+($page > 1 ? 4 : 2), $limits*($page-1)-($page > 1 ? 2 : 0));
		}	

		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			foreach($results as $r){
				$data[] = $r;
			}
			/*if($formatted){print '3:';
				$data = $this->format_listings($data);
			}*/
			return $data;
		}
		return array();
	}
	
	/**
	 * Return filtered listings by key as array
	 * @param array $filters filters data
	 * @param integer $page pege of results
	 * @param integer $items_on_page rows per page
	 * @param array $order_by sorting parameters
	 * @param boolean $formatted format results
	 * @return array
	 */
	public function get_listings_list_by_key($filters=array(), $page=null, $items_on_page=null, $order_by=null, $formatted=true){
		$params = $this->_get_search_criteria($filters);		
		$listings_data = $this->_get_listings_list($page, $items_on_page, $order_by, $params, $formatted);
		$return_data = array();
		foreach($listings_data as $data){
			$return_data[$data['id']] = $data;
		}
		return $return_data;
	}
	
	/**
	 * Return listings list by set of identifiers
	 * @param array $listing_ids listing identifier set
	 * @param boolean $formatted format results
	 * @return array
	 */
	public function get_listings_list_by_id($listing_ids, $formatted=true){
		$params['where_in']['id'] = $listing_ids;
		return $this->_get_listings_list(null, null, null, $params, $formatted);
	}
	
	/**
	 * Return search criteria by form
	 * @param string $form_gid form GUID
	 * @param array $data listing data
	 * @param string $property_type_gid property type GUID
	 * @return array
	 */
	public function get_listings_ids_by_form($form_gid, $data, $property_type_gid){
		$this->CI->load->model('field_editor/models/Field_editor_forms_model');
		$params = $this->CI->Field_editor_forms_model->get_search_criteria($form_gid, $data, $property_type_gid);		
		return $this->_get_listings_ids_from_category($property_type_gid, $params);
	}
	
	/**
	 * Return listings identifiers from field_editor
	 * @param string $property_type_gid property type GUID
	 * @param string $params search criteria
	 * @return array
	 */
	public function get_listings_ids_from_field_editor($property_type_gid, $params){
		$this->DB->select('id_listing');
		$this->DB->from(constant('LISTINGS_'.strtoupper($property_type_gid).'_TABLE'));
		
		if(isset($params['where']) && is_array($params['where']) && count($params['where'])){
			foreach($params['where'] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params['where_in']) && is_array($params['where_in']) && count($params['where_in'])){
			foreach($params['where_in'] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params['where_sql']) && is_array($params['where_sql']) && count($params['where_sql'])){
			foreach($params['where_sql'] as $value){
				$this->DB->where($value, null, false);
			}
		}

		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			foreach($results as $r){
				$data[] = $r['id_listing'];
			}
			return $data;
		}
		return array();
	}
	
	/**
	 * Change format settings
	 * @param string $name parameter name
	 * @param mixed $value parameter value
	 */
	public function set_format_settings($name, $value=false){
		if(!is_array($name)) $name = array($name=>$value);
		foreach($name as $key => $item)	$this->format_settings[$key] = $item;
	}
	
	/**
	 * Validate listing data
	 * @param integer $listing_id listing identifier
	 * @param array $data
	 * @return array
	 */
	public function validate_listing($listing_id, $data){
		$return = array('errors'=>array(), 'data'=>array());

		if($listing_id){
			$this->set_format_settings('use_format', false);
			$listing = $this->get_listing_by_id($listing_id);
			$this->set_format_settings('use_format', true);
		}else{
			$listing = array();
		}

		if(isset($data['id'])){
			$return['data']['id'] = intval($data['id']);
			if(empty($return['data']['id'])) unset($return['data']['id']);
		}
		
		if(isset($data['gid'])){
			$return['data']['gid'] = trim(strip_tags($data['gid']));
			if(empty($return['data']['gid'])) unset($return['data']['gid']);
		}
		
		if(isset($data['id_user'])){
			$return['data']['id_user'] = intval($data['id_user']);
			if(empty($return['data']['id_user'])) $return['errors'][] = l('error_empty_user', 'listings');
		}elseif(!$listing_id){
			$return['errors'][] = l('error_empty_user', 'listings');
		}
		
		if(isset($data['id_type'])){
			$return['data']['id_type'] = intval($data['id_type']);
			if(empty($return['data']['id_type'])){
				$return['errors'][] = l('error_empty_operation_type', 'listings');
			}elseif(!array_key_exists($return['data']['id_type'], $this->get_operation_types())){
				$return['errors'][] = l('error_invalid_operation_type', 'listings');
			}else{
				$listing['id_type'] = $return['data']['id_type'];
			}
		}elseif(!$listing_id){
			$return['errors'][] = l('error_empty_operation_type', 'listings');
		}
		
		if(isset($data['id_category'])){
			$return['data']['id_category'] = intval($data['id_category']);
			if(empty($return['data']['id_category'])){
				$return['errors'][] = l('error_empty_category', 'listings');
			}elseif(!$this->get_field_editor_type(array_merge($listing, $return['data']))){
				$return['errors'][] = l('error_invalid_category', 'listings');
			}else{
				$listing['id_category'] = $data['id_category'];
			}
		}elseif(!$listing_id){
			$return['errors'][] = l('error_empty_category', 'listings');
		}
		
		if(isset($data['property_type'])){
			$return['data']['property_type'] = intval($data['property_type']);
			if(empty($return['data']['property_type'])){
				$return['errors'][] = l('error_empty_property_type', 'listings');
			}else{
				$listing['property_type'] = $return['data']['property_type'];
				
				$this->CI->load->helper('properties');
				$property_type = property_value($listing);
				if(!$property_type){
					$return['errors'][] = l('error_invalid_property_type', 'listings');
				}
			}
		}elseif(!$listing_id){
			$return['errors'][] = l('error_empty_property_type', 'listings');
		}
		
		if(isset($data['status'])){
			$return['data']['status'] = $listing['status'] = $data['status'] ? 1 : 0;
		}
		
		if(isset($data['date_open'])){
			$value = strtotime($data['date_open']);
			if($value > 0){
				$return['data']['date_open'] = date('Y-m-d', $value);
			}else{
				$return['data']['date_open'] = '0000-00-00 00:00:00';
			}
		}
		
		if(isset($data['date_open_begin'])){
			$return['data']['date_open_begin'] = intval($data['date_open_begin']);
			if(empty($return['data']['date_open_begin'])){
				unset($return['data']['date_open_begin']);
			}else{
				$value = ld_option('dayhour-names', 'start', $return['data']['date_open_begin']);
				if(empty($value)) unset($return['data']['date_open_begin']);
			}
		}
		
		if(isset($data['date_open_end'])){
			$return['data']['date_open_end'] = intval($data['date_open_end']);
			if(empty($return['data']['date_open_end'])){
				unset($return['data']['date_open_end']);
			}else{
				$value = ld_option('dayhour-names', 'start', $return['data']['date_open_end']);
				if(empty($value)) unset($return['data']['date_open_end']);
			}
		}
		
		if(isset($data['date_available'])){
			$value = strtotime($data['date_available']);
			if($value > 0){
				$return['data']['date_available'] = date('Y-m-d', $value);
			}else{
				$return['data']['date_available'] = '0000-00-00 00:00:00';
			}
		}
		
		if(isset($data['sold'])){
			$return['data']['sold'] = $data['sold'] ? 1 : 0;
		}
		
		if(isset($data['date_created'])){
			$value = strtotime($data['date_created']);
			if($value > 0){
				$return['data']['date_created'] = date('Y-m-d', $value);
			}else{
				$return['data']['date_created'] = '0000-00-00 00:00:00';
			}
		}
		
		if(isset($data['date_modified'])){
			$value = strtotime($data['date_modified']);
			if($value > 0){
				$return['data']['date_modified'] = date('Y-m-d', $value);
			}else{
				$return['data']['date_modified'] = '0000-00-00 00:00:00';
			}
		}
		
		if(isset($data['date_activity'])){
			$value = strtotime($data['date_activity']);
			if($value > 0){
				$return['data']['date_activity'] = date('Y-m-d', $value);
			}else{
				$return['data']['date_activity'] = '0000-00-00 00:00:00';
			}
		}
		
		if(isset($data['date_expire'])){
			$value = strtotime($data['date_expire']);
			if($value > 0){
				$return['data']['date_expire'] = date('Y-m-d', $value);
			}else{
				$return['data']['date_expire'] = '0000-00-00 00:00:00';
			}
		}
		
		if(isset($data['price_negotiated'])){
			$return['data']['price_negotiated'] = $listing['price_negotiated'] = $data['price_negotiated'] ? 1 : 0;
		}
		
		$is_payments_installed = $this->CI->pg_module->is_module_installed('payments');
		if($is_payments_installed){
			if(isset($data['gid_currency'])){
				$return['data']['gid_currency'] = trim(strip_tags($data['gid_currency']));
				if(!isset($listing['price_negotiated']) || empty($listing['price_negotiated'])){
					if(empty($return['data']['gid_currency'])){
						$return['errors'][] = l('error_empty_currency', 'listings');
					}else{
						$is_currency = false;
						$this->CI->load->model('payments/models/Payment_currency_model');
						$currencies = $this->CI->Payment_currency_model->get_currency_list();
						foreach($currencies as $currency){
							if($currency['gid'] == $return['data']['gid_currency']){
								$is_currency = true;
								break;
							}
						}
						if(!$is_currency){
							$return['errors'][] = l('error_invalid_currency', 'listings');
						}else{
							$listing['gid_currency'] = $return['data']['gid_currency'];
						}
					}
				}else{
					$listing['gid_currency'] = $return['data']['gid_currency'];
				}
			}elseif(!$listing_id){
				if(!isset($listing['price_negotiated']) || empty($listing['price_negotiated'])){
					$return['errors'][] = l('error_empty_currency', 'listings');
				}
			}
		}
		
		$need_min_price = in_array($this->get_operation_type_by_id($listing['id_type']), array('sale', 'rent'));
		if(isset($data['price'])){
			$return['data']['price'] = floatval($data['price']);
			if(empty($return['data']['price']) && $need_min_price){
				$return['errors'][] = l('error_empty_price', 'listings');
			}else{
				$listing['price'] = $return['data']['price'];
				if($is_payments_installed && isset($listing['gid_currency'])){
					$this->CI->load->model('payments/models/Payment_currency_model');
					$return['data']['price_sorting'] = $this->CI->Payment_currency_model->
						get_value_into_base_currency($listing['price'], $listing['gid_currency']);
				}else{
					$return['data']['price_sorting'] = 0;
				}
			}
		}elseif(!$listing_id && $need_min_price){
			if(!isset($listing['price_negotiated']) || empty($listing['price_negotiated'])){
				$return['errors'][] = l('error_empty_price', 'listings');
			}
		}
		
		if(isset($data['price_max'])){
			$use_price_max = in_array($this->get_operation_type_by_id($listing['id_type']), array('buy', 'lease'));
			if($use_price_max){
				$return['data']['price_max'] = floatval($data['price_max']);
				if(!empty($return['data']['price_max'])){
					if(isset($listing['price']) && $listing['price'] > $return['data']['price_max']){
						$return['errors'][] = l('error_invalid_max_price', 'listings');
					}else{
						$listing['price_max'] = $return['data']['price_max'];
						if($is_payments_installed && isset($listing['gid_currency'])){
							$this->CI->load->model('payments/models/Payment_currency_model');
							$return['data']['price_max_sorting'] = $this->CI->Payment_currency_model->
								get_value_into_base_currency($listing['price_max'], $listing['gid_currency']);
						}else{
							$return['data']['price_max_sorting'] = 0;
						}
					}
				}
			} 
		}
		
		if(empty($listing['price']) && empty($listing['price_max'])){
			if(!isset($listing['price_negotiated']) || empty($listing['price_negotiated'])){
				$return['errors'][] = l('error_empty_price', 'listings');
			}
		}
		
		if(isset($data['price_old'])){
			$return['data']['price'] = floatval($data['price_old']);
		}
		
		if(in_array($this->get_operation_type_by_id($listing['id_type']), array('sale', 'rent'))){
			if(isset($data['price_reduced'])){
				$return['data']['price_reduced'] = floatval($data['price_reduced']);
				if(!empty($return['data']['price_reduced'])){
					if($listing['price'] <= $return['data']['price_reduced']){
						$return['errors'][] = l('error_invalid_reduced_price', 'listings');
					}else{
						$listing['price_reduced'] = $return['data']['price_reduced'];
						if($is_payments_installed && isset($listing['gid_currency'])){
							$this->CI->load->model('payments/models/Payment_currency_model');
							$return['data']['price_sorting'] = $this->CI->Payment_currency_model->
								get_value_into_base_currency($listing['price_reduced'], $listing['gid_currency']);
						}else{
							$return['data']['price_sorting'] = 0;
						}
					}
				} 
			}
		}
		
		if(in_array($this->get_operation_type_by_id($listing['id_type']), array('rent', 'lease'))){
			if(isset($data['price_period'])){
				$return['data']['price_period'] = strval($data['price_period']);
				if(empty($return['data']['price_period'])){
					$value = ld_option('price_period', 'listings', 0);
					//$return['errors'][] = l('error_empty_price_period', 'listings');
				}else{
					$value = ld_option('price_period', 'listings', $return['data']['price_period']);
					if(empty($value)){
						$value = ld_option('price_period', 'listings', 0);
						//$return['errors'][] = l('error_invalid_price_period', 'listings');
					}
				}
			}elseif(!$listing_id){
				//$return['errors'][] = l('error_empty_price_period', 'listings');
			}
		
			if(isset($data['price_type'])){
				$return['data']['price_type'] = strval($data['price_type']);
				if(empty($return['data']['price_type'])){
					$value = ld_option('price_type', 'listings',0);
					//$return['errors'][] = l('error_empty_price_type', 'listings');
				}else{
					$value = ld_option('price_type', 'listings', $return['data']['price_type']);
					if(empty($value)){
						$value = ld_option('price_type', 'listings',0);
						//$return['errors'][] = l('error_invalid_price_type', 'listings');
					}
				}
			}elseif(!$listing_id){
				//$return['errors'][] = l('error_empty_price_type', 'listings');
			}
		} //#MOD#
		
		if(isset($data['price_sorting'])){
			$return['data']['price_sorting'] = floatval($data['price_sorting']);
		}
		
		if(isset($data['price_auction'])){
			$return['data']['price_auction'] = $data['price_auction'] ? 1 : 0;
		}
		
		if(isset($data['square_unit'])){
			$return['data']['square_unit'] = trim(strip_tags($data['square_unit']));
			if(empty($return['data']['square_unit'])){
				$return['errors'][] = l('error_empty_square_unit', 'listings');
			}else{
				$square_unit = ld_option('square_unit', 'listings', $return['data']['square_unit']);
			}
		}
		
		if(isset($data['square'])){
			$return['data']['square'] = floatval($data['square']);
			if(empty($return['data']['square'])){
				$return['errors'][] = l('error_empty_square', 'listings');
			}else{
				$listing['square'] = $return['data']['square'];
			}
		}elseif(!$listing_id){
			$return['errors'][] = l('error_empty_square', 'listings');
		}
		
		if(isset($data['square_max'])){
			$return['data']['square_max'] = floatval($data['square_max']);
			if(!empty($return['data']['square_max'])){
				if($listing['square'] > $return['data']['square_max']){
					$return['errors'][] = l('error_invalid_max_square', 'listings');
				}
			} 
		}
		
		if(isset($data['id_city'])){
			$return['data']['id_city'] = intval($data['id_city']);
			if(empty($return['data']['id_city'])){
				$return['errors'][] = l('error_empty_location', 'listings');
			}else{
				$this->CI->load->model('Countries_model');
				$city = $this->CI->Countries_model->get_city($return['data']['id_city']);
				if($city){
					$return['data']['id_country'] = $listing['id_country'] = $city['country_code'];
					$return['data']['id_region'] = $listing['id_region'] = $city['id_region'];
					$return['data']['id_city'] = $listing['id_city'] = $city['id'];
				}else{
					$return['errors'][] = l('error_invalid_location', 'listings');
				}
			}
		}elseif(!$listing_id){
			$return['errors'][] = l('error_empty_location', 'listings');
		}
		
		if(isset($data['id_region'])){
			$return['data']['id_region'] = intval($data['id_region']);
			if($return['data']['id_region'] != $listing['id_region']){
				$return['errors'][] = l('error_invalid_location', 'listings');
			}
		}
		
		if(isset($data['id_country'])){
			$return['data']['id_country'] = trim(strip_tags($data['id_country']));
			if($return['data']['id_country'] != $listing['id_country']){
				$return['errors'][] = l('error_invalid_location', 'listings');
			}
		}
		
		if(isset($data['address'])){
			$return['data']['address'] = trim(strip_tags($data['address']));
			if(!empty($return['data']['address'])){
				$this->CI->load->model('moderation/models/Moderation_badwords_model');
				$bw_count = $this->CI->Moderation_badwords_model->check_badwords($this->moderation_type, $return['data']['address']);
				if($bw_count){
					$message = l('error_badwords_field', 'listings');
					$message = str_replace('[field]', l('field_address', 'listings'), $message);
					$return['errors'][] = $message;
				}
			}
		}
		
		if(isset($data['zip'])){
			$return['data']['zip'] = trim(strip_tags($data['zip']));
			if(!empty($return['data']['zip'])){
				$this->CI->load->model('moderation/models/Moderation_badwords_model');
				$bw_count = $this->CI->Moderation_badwords_model->check_badwords($this->moderation_type, $return['data']['zip']);
				if($bw_count){
					$message = l('error_badwords_field', 'listings');
					$message = str_replace('[field]', l('field_zip', 'listings'), $message);
					$return['errors'][] = $message;
				}
			}
		}
		
		if(isset($data['lat'])){
			$return['data']['lat'] = floatval($data['lat']);
		}
		
		if(isset($data['lon'])){
			$return['data']['lon'] = floatval($data['lon']);
		}
		
		if(isset($data['listing_file'])){
			$return['data']['listing_file'] = $data['listing_file'];
		}
		
		if(isset($data['listing_file_date'])){
			$value = strtotime($data['listing_file_date']);
			if($value > 0){
				$return['data']['listing_file_date'] = date('Y-m-d', $value);
			}else{
				$return['data']['listing_file_date'] = '0000-00-00 00:00:00';
			}
		}
		
		if(isset($data['listing_file_name'])){
			$return['data']['listing_file_name'] = trim(strip_tags($data['listing_file_name']));
			if(empty($return['data']['listing_file_name'])){
				$return['errors'][] = l('error_empty_file_name', 'listings');
			}else{
				$this->CI->load->model('moderation/models/Moderation_badwords_model');
				$bw_count = $this->CI->Moderation_badwords_model->check_badwords($this->moderation_type, $return['data']['listing_file_name']);
				if($bw_count){
					$message = l('error_badwords_field', 'listings');
					$message = str_replace('[field]', l('field_listing_file_name', 'listings'), $message);
					$return['errors'][] = $message;
				}
			}
		}
		
		if(isset($data['listing_file_comment'])){
			$return['data']['listing_file_comment'] = trim(strip_tags($data['listing_file_comment']));
			if(!empty($return['data']['listing_file_comment'])){
				$this->CI->load->model('moderation/models/Moderation_badwords_model');
				$bw_count = $this->CI->Moderation_badwords_model->check_badwords($this->moderation_type, $return['data']['listing_file_comment']);
				
				if($bw_count){
					$message = l('error_badwords_field', 'listings');
					$message = str_replace('[field]', l('field_listing_file_comment', 'listings'), $message);
					$return['errors'][] = $message;
				}
			}
		}
		
		if(isset($data['listing_video'])){
			$return['data']['listing_video'] = $data['listing_video'];
		}
	
		if(isset($data['listing_video_image'])){
			$return['data']['listing_video_image'] = $data['listing_video_image'];
		}
		
		if(isset($data['listing_video_data'])){
			$return['data']['listing_video_data'] = serialize($data['listing_video_data']);
		}
		
		if(isset($data['initial_moderation'])){
			$return['data']['initial_moderation'] = intval($data['initial_moderation']);
		}
		
		if(isset($data['initial_activity'])){
			$return['data']['initial_activity'] = intval($data['initial_activity']);
		}
		
		if(isset($data['logo_image'])){
			$return['data']['logo_image'] = $data['logo_image'];
		}
		
		if(isset($data['slider_image'])){
			$return['data']['slider_image'] = $data['slider_image'];
		}
		
		if(isset($data['id_wish_lists'])){
			if($listing['status'] || empty($data['id_wish_lists'])){
				$return['data']['id_wish_lists'] = serialize($data['id_wish_lists']);
			}else{
				$return['errors'][] = l('error_inactive_to_wish_list', 'listings');
			}
		}
		
		if(isset($data['photo_count'])){
			$return['data']['photo_count'] = intval($data['photo_count']);
		}
		
		if(isset($data['is_vtour'])){
			$return['data']['is_vtour'] = $data['is_vtour'] ? 1 : 0;
		}
		
		if(isset($data['use_calendar'])){
			$return['data']['use_calendar'] = $data['use_calendar'] ? 1 : 0;
		}

		if(isset($data['calendar_period_min'])){
			$listing['calendar_period_min'] = $return['data']['calendar_period_min'] = intval($data['calendar_period_min']);
		}
		
		if(isset($data['calendar_period_max'])){
			$listing['calendar_period_max'] = $return['data']['calendar_period_max'] = intval($data['calendar_period_max']);
		}
		
		if($listing['calendar_period_max'] > 0 && $listing['calendar_period_min'] > $listing['calendar_period_max']){
			$return['errors'][] = l('error_invalid_calendar_limits', 'listings');
		}
		
		if(isset($data['featured_date_end'])){
			$value = strtotime($data['featured_date_end']);
			if($value > 0){
				$return['data']['featured_date_end'] = date('Y-m-d', $value);
			}else{
				$return['data']['featured_date_end'] = '0000-00-00 00:00:00';
			}
		}
		
		if(isset($data['lift_up_date_end'])){
			$value = strtotime($data['lift_up_date_end']);
			if($value > 0){
				$return['data']['lift_up_date_end'] = date('Y-m-d', $value);
			}else{
				$return['data']['lift_up_date_end'] = '0000-00-00 00:00:00';
			}
		}
		
		if(isset($data['lift_up_country_date_end'])){
			$value = strtotime($data['lift_up_country_date_end']);
			if($value > 0){
				$return['data']['lift_up_country_date_end'] = date('Y-m-d', $value);
			}else{
				$return['data']['lift_up_country_date_end'] = '0000-00-00 00:00:00';
			}
		}
		
		if(isset($data['lift_up_region_date_end'])){
			$value = strtotime($data['lift_up_region_date_end']);
			if($value > 0){
				$return['data']['lift_up_region_date_end'] = date('Y-m-d', $value);
			}else{
				$return['data']['lift_up_region_date_end'] = '0000-00-00 00:00:00';
			}
		}
		
		if(isset($data['lift_up_city_date_end'])){
			$value = strtotime($data['lift_up_city_date_end']);
			if($value > 0){
				$return['data']['lift_up_city_date_end'] = date('Y-m-d', $value);
			}else{
				$return['data']['lift_up_city_date_end'] = '0000-00-00 00:00:00';
			}
		}
		
		if(isset($data['highlight_date_end'])){
			$value = strtotime($data['highlight_date_end']);
			if($value > 0){
				$return['data']['highlight_date_end'] = date('Y-m-d', $value);
			}else{
				$return['data']['highlight_date_end'] = '0000-00-00 00:00:00';
			}
		}
		
		if(isset($data['slide_show_date_end'])){
			$value = strtotime($data['slide_show_date_end']);
			if($value > 0){
				$return['data']['slide_show_date_end'] = date('Y-m-d', $value);
			}else{
				$return['data']['slide_show_date_end'] = '0000-00-00 00:00:00';
			}
		}
		
		# MOD for showing slider images #
		if(isset($data['slider_date_end'])){
			$value = strtotime($data['slider_date_end']);
			if($value > 0){
				$return['data']['slider_date_end'] = date('Y-m-d', $value);
			}else{
				$return['data']['slider_date_end'] = '0000-00-00 00:00:00';
			}
		}
		# MOD for showing slider images #

		if(isset($data['views'])){
			$return['data']['views'] = intval($data['views']);
		}
		
		if(isset($data['set_to_subscribe'])){
			$return['data']['set_to_subscribe'] = $data['set_to_subscribe'] ? 1 : 0;
		}
		
		// field editor validate
		if(isset($listing['id_type']) && isset($listing['id_category'])){
			$property_type_gid = $this->get_field_editor_type($listing);
			$this->CI->load->model('Field_editor_model');
			$this->CI->Field_editor_model->initialize($property_type_gid);
			$params = array('where'=>array('editor_type_gid'=>$property_type_gid));
			$validate_data = $this->CI->Field_editor_model->validate_fields_for_save($params, $data);
			$fields = $this->get_fields_for_select($property_type_gid);
			$fields = $this->CI->Field_editor_model->get_fields_list($params);
			foreach($fields as $field_gid=>$field_data){
				if($field_data['field_type'] != 'text' && $field_data['field_type'] != 'textarea') continue;
				if(isset($data[$field_data['field_name']]) && !empty($data[$field_data['field_name']])){
					$data[$field_data['field_name']] = trim(strip_tags($data[$field_data['field_name']]));
					$this->CI->load->model('moderation/models/Moderation_badwords_model');
					$bw_count = $this->CI->Moderation_badwords_model->check_badwords($this->moderation_type, $data[$field_data['field_name']]);
					if($bw_count){
						$message = str_replace('[field]', $field_data['name'], l('error_badwords_field', 'listings'));
						$validate_data['errors'][] = $message;
					}
				}
			}
			if(!empty($validate_data['errors'])){
				$return['errors'] = array_merge($return['errors'], $validate_data['errors']);
			}else{
				foreach($validate_data['data'] as $field=>$value){
					$return['data'][$field] = $value;
				}
			}
		}
		
		if(isset($data['headline'])){
			$return['data']['headline'] = trim(strip_tags($data['headline']));
			if(!empty($return['data']['headline'])){	
				$this->CI->load->model('moderation/models/Moderation_badwords_model');
				$bw_count = $this->CI->Moderation_badwords_model->check_badwords($this->moderation_type, $return['data']['headline']);
				if($bw_count){
					$message = str_replace('[field]', l('field_headline', 'listings'), l('error_badwords_field', 'listings'));
					$validate_data['errors'][] = $message;
				}
			}
		}
		
		return $return;
	}
	
	/**
	 * Validate listings settings
	 * @param array $data
	 * @return array
	 */
	public function validate_listings_settings($data){
		$return = array('errors'=> array(), 'data' => array());
		return $return;
	}
	
	/**
	 * Format listing
	 * @param array $data listing data
	 * @return array
	 */
	public function format_listing($data){
		return array_shift($this->format_listings(array($data)));
	}
	
	/**
	 * Format listings data
	 * @param array $data listing data
	 * @return array
	 */
	public function format_listings($data){
		//print_r($data); exit;
		if(!$this->format_settings['use_format']) return $data;
		
		$users_ids = array();
		$locations_ids = array();
		$listings_ids = array();
		$property_types_ids = array();
		
		$this->CI->load->model('Properties_model');
		$property_types = $this->CI->Properties_model->get_categories('property_types', $this->CI->pg_language->current_lang_id);
		$property_items = $this->CI->Properties_model->get_all_properties('property_types', $this->CI->pg_language->current_lang_id);

		$square_units = $this->CI->pg_language->ds->get_reference('listings', $this->square_units_gid, $this->CI->pg_language->current_lang_id);
		
		$reference = $this->CI->pg_language->ds->get_reference('start', 'dayhour-names');
		$dayhours = $reference['option'];
		
		$this->CI->load->model('Uploads_model');
		
		if($this->CI->pg_module->is_module_installed('file_uploads') && $this->format_settings['get_file']){
			$file_type_config = $this->get_file_type();
		}
		
		if($this->format_settings['get_video'] && $this->CI->pg_module->is_module_installed('video_uploads')){
			$video_type_config = $this->get_video_type();
		}
		
		if($this->format_settings['get_saved_listings']){
			$saved_listings = $this->get_saved_listings_ids();
		}
		
		foreach($data as $key=>$listing){
			// prefix
			if(!empty($listing['id'])) $listing['prefix'] = $listing['id_user'].'/'.$listing['id'].'/';
			
			$listing['operation_type'] = $this->get_operation_type_by_id($listing['id_type']);
			$listing['operation_type_str'] = l('operation_type_'.$listing['operation_type'], 'listings');

			if($listing['operation_type'] == 'sale' && $listing['sold']){
				$listing['sold_output'] = l('text_sold', 'listings');
			}elseif($listing['status']){
				$listing['status_output'] = l('active_listing', 'listings');
			}else{
				$listing['status_output'] = l('inactive_listing', 'listings');
			}
			
			$listing['category_str'] = $listing['category'] = $property_types[$listing['id_category']];
			$listing['property_type_str'] = $listing['property'] = $property_items[$listing['id_category']][$listing['property_type']];
			
			$listing['field_editor_type'] = $this->get_field_editor_type($listing);
	
			// get_user
			if($this->format_settings['get_user']){
				$users_ids[] = $listing['id_user'];
			}
	
			// get location
			if($this->format_settings['get_location']){
				$locations_ids[$listing['id']] = array($listing['id_country'], $listing['id_region'], $listing['id_city'], $listing['address'], $listing['zip']);
			}
			
			// get description
			if($this->format_settings['get_description']){
				$property_type_gid = $listing['field_editor_type'];
				if(!isset($property_types_ids[$property_type_gid]))	$property_types_ids[$property_type_gid] = array();
				$property_types_ids[$property_type_gid][] = $listing['id'];
			}
			
			// get photos, virtual tours, moderation or booking
			if($this->format_settings['get_photos'] || $this->format_settings['get_photos_all'] ||
			   $this->format_settings['get_virtual_tours'] || $this->format_settings['get_virtual_tours_all'] ||
			   $this->format_settings['get_moderation'] || $this->format_settings['get_booking']){
				$listings_ids[] = $listing['id'];
			}
			
			// get_file
			if($this->CI->pg_module->is_module_installed('file_uploads')){
				if(!empty($listing['listing_file'])){
					if($this->format_settings['get_file']){
						$listing['listing_file_content'] = $this->CI->File_uploads_model->format_upload(
							$this->file_config_id, $listing['prefix'], $listing['listing_file']);
					}
					$listing['is_file'] = true;
				}else{
					$listing['is_file'] = false;
				}
			}else{
				$listing['is_file'] = false;
			}
		
			// get_video
			if($this->CI->pg_module->is_module_installed('video_uploads')){
				if(!empty($listing['listing_video_data'])){
					$listing['listing_video_data'] = $listing['listing_video_data'] ? unserialize($listing['listing_video_data']) : array();
				}
				if(!empty($listing['listing_video']) && $listing['listing_video_data']['status'] == 'end'){
					if($this->format_settings['get_video']){
						$listing['listing_video_content'] = $this->CI->Video_uploads_model->format_upload($this->video_config_id, 
							$listing['prefix'], $listing['listing_video'], $listing['listing_video_image'], 
							$listing['listing_video_data']['data']['upload_type']);
					}
					$listing['is_video'] = true;
				}else{
					$listing['is_video'] = false;
				}
			}else{
				$listing['is_video'] = false;
			}
			
			// saved listings
			if($this->format_settings['get_saved_listings']){
				if(in_array($listing['id'], (array)$saved_listings)){
					$listing['is_saved'] = true;
				}
			}
		
			$listing['price'] *= 1;	
			$listing['price_max'] *= 1;	
			$listing['price_reduced'] *= 1;
			$listing['price_old'] *= 1;
			
			if($listing['price_period']){
				$listing['price_period_str'] = ld_option('price_period', 'listings', $listing['price_period']);
			}else{
				$listing['price_period_str'] = '';
			}
			
			if($listing['price_type']){
				$listing['price_type_str'] = ld_option('price_type', 'listings', $listing['price_type']);
			}else{
				$listing['price_type_str'] = '';
			}
			
			$listing['square_output'] = '';
			
			switch($listing['operation_type']){
				case 'sale':
					$listing['square_output'] .= $listing['square'];	
				break;
				case 'buy':
					if($listing['square']) $listing['square_output'] .= l('text_price_from', 'listings').' '.$listing['square'].' ';
					if($listing['square_max']) $listing['square_output'] .= l('text_price_to', 'listings').' '.$listing['square_max'].' ';
				break;
				case 'rent':
					$listing['square_output'] .= $listing['square'];	
				break;
				case 'lease':
					if($listing['square']) $listing['square_output'] .= l('text_price_from', 'listings').' '.$listing['square'].' ';
					if($listing['square_max']) $listing['square_output'] .= l('text_price_to', 'listings').' '.$listing['square_max'].' ';
				break;
			}
			
			if($listing['square_unit']){
				if(isset($square_units['option'][$listing['square_unit']])){
					$listing['square_output'] .= ' '.$square_units['option'][$listing['square_unit']];
				}else{
					$listing['square_output'] .= ' '.$listing['square_unit'];
				}
			}
			
			$listing['square_output'] = trim($listing['square_output']);
			
			if(!empty($listing['date_open_begin'])){
				$listing['date_open_begin_text'] = $dayhours[$listing['date_open_begin']];
			}else{
				$listing['date_open_begin_text'] = '';
			}
			
			if(!empty($listing['date_open_end'])){
				$listing['date_open_end_text'] = $dayhours[$listing['date_open_end']];
			}else{
				$listing['date_open_end_text'] = '';
			}

			if(!empty($listing['id_wish_lists'])){
				$listing['id_wish_lists'] =  (array)unserialize($listing['id_wish_lists']);
			}else{
				$listing['id_wish_lists'] = array();
			}
			
			if($listing['featured_date_end'] != '0000-00-00 00:00:00' && strtotime($listing['featured_date_end']) > 86400){
				$listing['is_featured'] = true;
			}else{
				$listing['is_featured'] = false;
			}
			
			if($listing['lift_up_date_end'] != '0000-00-00 00:00:00' && strtotime($listing['lift_up_date_end']) > 86400){
				$listing['is_lift_up'] = true;
			}else{
				$listing['is_lift_up'] = false;
			}
			
			if($listing['lift_up_country_date_end'] != '0000-00-00 00:00:00' && strtotime($listing['lift_up_country_date_end']) > 86400){
				$listing['is_lift_up_country'] = true;
			}else{
				$listing['is_lift_up_country'] = false;
			}
			
			if($listing['lift_up_region_date_end'] != '0000-00-00 00:00:00' && strtotime($listing['lift_up_region_date_end']) > 86400){
				$listing['is_lift_up_region'] = true;
			}else{
				$listing['is_lift_up_region'] = false;
			}
			
			if($listing['lift_up_city_date_end'] != '0000-00-00 00:00:00' && strtotime($listing['lift_up_city_date_end']) > 86400){
				$listing['is_lift_up_city'] = true;
			}else{
				$listing['is_lift_up_city'] = false;
			}
			
			if($listing['highlight_date_end'] != '0000-00-00 00:00:00' && strtotime($listing['highlight_date_end']) > 86400){
				$listing['is_highlight'] = true;
			}else{
				$listing['is_highlight'] = false;
			}

			if($listing['photo_count'] && $listing['slide_show_date_end'] != '0000-00-00 00:00:00' && strtotime($listing['slide_show_date_end']) > 86400){
				$listing['is_slide_show'] = true;
			}else{
				$listing['is_slide_show'] = false;
			}
			
			// thumbs
			if(!empty($listing['logo_image'])){
				$listing['media']['photo'] = $this->CI->Uploads_model->format_upload($this->upload_config_id, $listing['id'], $listing['logo_image']);
			}else{
				$listing['media']['photo'] = $this->CI->Uploads_model->format_default_upload($this->upload_config_id);
			}
			
			// slider
			if(!empty($listing['slider_image'])){
				$listing['media']['slider'] = $this->CI->Uploads_model->format_upload($this->slider_config_id, $listing['id'], $listing['slider_image']);
			}else{
				$listing['media']['slider'] = $this->CI->Uploads_model->format_default_upload($this->slider_config_id);
			}
			
			if(isset($listing['review_sorter'])) $listing['review_sorter'] = round($listing['review_sorter'], 1);
			
			// nav settings
			$listing['section'] = 'overview';
			$listing['pdf'] = 'no';
			
			$data[$key] = $listing;
		}
//print_r($data); exit;
		// get user
		if($this->format_settings['get_user'] && !empty($users_ids)){
			$this->CI->load->model('Users_model');
			$users_data = $this->CI->Users_model->get_users_list_by_key(null, null, null, array(), array_unique($users_ids));
			foreach($data as $key=>$listing){
				$listing['user'] = (isset($users_data[$listing['id_user']])) ? 
					$users_data[$listing['id_user']] : $this->CI->Users_model->format_default_user($listing['id_user']);
				$data[$key] = $listing;
			}
		}

		// get location
		if(!empty($locations_ids)){
			$this->CI->load->helper('countries');
			$listing_locations = address_output_format($locations_ids);
			$listing_locations_data = get_location_data($locations_ids);
			foreach($data as $key=>$listing){
				$listing['country'] = (isset($listing_locations_data['country'][$listing['id_country']])) ? $listing_locations_data['country'][$listing['id_country']]['name'] : '';
				$listing['region'] = (isset($listing_locations_data['region'][$listing['id_region']])) ? $listing_locations_data['region'][$listing['id_region']]['name'] : '';
				$listing['region_code'] = (isset($listing_locations_data['region'][$listing['id_region']])) ? $listing_locations_data['region'][$listing['id_region']]['code'] : '';
				$listing['city'] = (isset($listing_locations_data['city'][$listing['id_city']])) ? $listing_locations_data['city'][$listing['id_city']]['name'] : '';
				$listing['output_name'] = $listing['location'] = $listing_locations[$listing['id']];
				$data[$key] = $listing;
			}
		}
		
		// get description
		if($this->format_settings['get_description'] && !empty($property_types_ids)){
			$field_editor_data = array();
			foreach($property_types_ids as $property_type_gid=>$property_type_data){
				$fields_for_select = $this->get_fields_for_select($property_type_gid);	
				if(empty($fields_for_select)) continue;
				$field_editor_data = $this->_get_field_editor_data($property_type_gid, $fields_for_select, $property_type_data);
				foreach($data as $key=>$listing){
					if(!isset($field_editor_data[$listing['id']])) continue;
					foreach($field_editor_data[$listing['id']] as $name=>$value){
						if(isset($listing[$name])) continue;
						$listing[$name] = $value;
						$data[$key] = $listing;
					}
				}
			}
		
			$this->CI->load->model('Field_editor_model');
			$params = array('where_in' => array('editor_type_gid' => array_keys($property_types_ids)));
			$fields_data = $this->CI->Field_editor_model->format_list_fields_for_view($params, $data);
			foreach($data as $key=>$listing){
				if(!isset($fields_data[$key])) continue;
				$data[$key]['field_editor'] = array();
				foreach($this->field_editor_sections[$listing['field_editor_type']] as $field_editor_section){
					foreach($fields_data[$key] as $field_gid=>$field_data){
						if($field_data['section_gid'] != $field_editor_section) continue;
						$data[$key]['field_editor'][] = $field_data;
						switch($field_data['field_type']){			
							case 'select':			
								$data[$key]['field_editor_'.$field_gid.'_output'] = $field_data['value'];
							break;
							case 'checkbox':
								$data[$key]['field_editor_'.$field_gid] = $field_data['value'];
								$data[$key]['field_editor_'.$field_gid.'_text'] = l($field_data['value'] ? 'yes_str' : 'no_str', 'start');
							break;
							case 'multiselect':
								foreach($field_data['value'] as $value=>$name){
									$data[$key]['field_editor_'.$field_gid.'_option_'.$value] = 1;
									$data[$key]['field_editor_'.$field_gid.'_option_'.$value.'_text'] = l('yes_str', 'start');
								}
							break;
						}
					}
				}
			}
		}

		// get photos
		if(($this->format_settings['get_photos'] || $this->format_settings['get_photos_all']) && !empty($listings_ids)){
			$gallery_type_config = $this->get_gallery_type();
			$default_gallery_upload = $this->CI->Uploads_model->format_default_upload($gallery_type_config['gid_upload_config']);
			
			$photos_by_object = array();
			
			$param = array();
			$param['where']['type_id'] = $gallery_type_config['id'];
			$param['where_in']['object_id'] = $listings_ids;
			if(!$this->format_settings['get_photos_all']) $param['where']['status'] = 1;
			$photos = $this->CI->Upload_gallery_model->get_files_by_param($param);
			foreach($photos as $photo){
				if(!isset($photos_by_object[$photo['object_id']])) $photos_by_object[$photo['object_id']] = array();
				$photos_by_object[$photo['object_id']][] = $photo; 
			}
			foreach($data as $key=>$listing){
				$listing['photos'] = isset($photos_by_object[$listing['id']]) ? $photos_by_object[$listing['id']] : array();
				$listing['photo_count'] = count($listing['photos']);
				$listing['photo_default'] = array('media' => $default_gallery_upload);
				$data[$key] = $listing;
			}
		}

		// get virtual tours
		if($this->format_settings['get_virtual_tours'] || $this->format_settings['get_virtual_tours_all']){
			$vtour_type_config = $this->get_vtour_type();
			
			$vtours_by_object = array();
			
			$param = array();
			$param['where']['type_id'] = $vtour_type_config['id'];
			$param['where_in']['object_id'] = $listings_ids;
			if(!$this->format_settings['get_virtual_tours_all']) $param['where']['status'] = 1;
			$vtours = $this->CI->Upload_gallery_model->get_files_by_param($param);
			foreach($vtours as $vtour){
				if(!isset($vtours_by_object[$vtour['object_id']])) $vtours_by_object[$vtour['object_id']] = array();
				$vtours_by_object[$vtour['object_id']][] = $vtour; 
			}
			foreach($data as $key=>$listing){
				$listing['virtual_tour'] = isset($vtours_by_object[$listing['id']]) ? $vtours_by_object[$listing['id']] : array();
				$listing['virtual_tour_count'] = count($listing['virtual_tour']);
				$data[$key] = $listing;
			}
		}

		// get moderation
		if($this->format_settings['get_moderation'] && !empty($listings_ids)){
			$this->CI->load->model('listings/models/Listings_moderation_model');
			$alert_data = $this->CI->Listings_moderation_model->get_listings_status($listings_ids);
			foreach($data as $key=>$listing){
				$listing['moderation_status'] = $alert_data[$listing['id']];
				$data[$key] = $listing;
			}
		}

		// qr code
		if($this->format_settings['get_qr_code']){
			foreach($data as $key=>$listing){
				$listing['qr_code'] = $this->get_qr_code($listing);
				$data[$key] = $listing;
			}
		}
//print_r($data); exit;
		// booking
		if($this->format_settings['get_booking']){
			$this->CI->load->model('listings/models/Listings_booking_model');
			$periods_data = $this->CI->Listings_booking_model->get_periods_list_by_key(array('listings'=>$listings_ids), null, null, array('status'=>'DESC'));
			foreach($data as $key=>$listing){
				$listing['booking']['periods'] = isset($periods_data[$listing['id']]) ? $periods_data[$listing['id']] : array();
				$data[$key] = $listing;
			}
		}
		
		return $data;
//print_r($data); exit;
	}
	
	/**
	 * Format default listing
	 * @param integer $listing_id listing identifier
	 * @return array
	 */
	public function format_default_listing($listing_id){
		$this->CI->load->model('Uploads_model');
		$data['postfix'] = $listing_id;
		$data['output_name'] = 'Listing is deleted';
		$data['media']['photo'] = $this->CI->Uploads_model->format_default_upload($this->upload_config_id);
		return $data;
	}
	
	/**
	 * Return field editor data
	 * @param string $property_type_gid field editor type GUID
	 * @return array
	 */
	public function get_fields_for_select($property_type_gid){
		if(!isset($this->field_editor_fields[$property_type_gid])){
			$this->CI->load->model('Field_editor_model');
			$this->CI->Field_editor_model->initialize($property_type_gid);
			$property_type_sections = array_keys($this->CI->Field_editor_model->get_section_list());
			$property_type_fields = $this->CI->Field_editor_model->get_fields_for_select($property_type_sections);
			$this->field_editor_fields[$property_type_gid] = $property_type_fields;
			$this->field_editor_sections[$property_type_gid] = $property_type_sections;
		}
		return $this->field_editor_fields[$property_type_gid];
	}
	
	/**
	 * Return field editor data
	 * @param string $property_type_gid property type GUID
	 * @param array $fields_for_select fields from field editor
	 * @param array $listing_ids listings identificators
	 */
	private function _get_field_editor_data($property_type_gid, $fields_for_select, $listing_ids){
		array_unshift($fields_for_select, 'id_listing');
		$this->DB->select(implode(',', $fields_for_select));
		$this->DB->from(constant('LISTINGS_'.strtoupper($property_type_gid).'_TABLE'));
		$this->DB->where_in('id_listing', (array)$listing_ids);
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			foreach($results as $r){
				$data[$r['id_listing']] = $r;
				unset($data[$r['id_listing']]['id_listing']);
			}
			return $data;
		}
		return array();
	}
	
	/**
	 * Return display sections
	 * @param array $data listing data
	 * @return string
	 */
	public function get_display_sections($data){
		$display_sections = $this->display_sections;
		
		if($data['operation_type'] != 'rent' || !$data['use_calendar']) 
			$display_sections['bottom']['calendar'] = 0;
	
		if(((float)$data['lat']  != 0 || (float)$data['lon'] != 0) && $this->CI->pg_module->is_module_installed('geomap')){
			$this->CI->load->helpers('geomap');
			if(!geomap_panorama_available('listing_view')) $display_sections['top']['panorama'] = 0;
		}else{
			$display_sections['top']['map'] = 0;
			$display_sections['top']['panorama'] = 0;
		}
		
		if(!$data['is_file']) $display_sections['bottom']['file'] = 0;
		if(!$data['is_video']) $display_sections['top']['video'] = 0;

		if(!$data['review_count']) $display_sections['bottom']['reviews'] = 0;
		if(!$data['is_vtour']) $display_sections['top']['virtual_tour'] = 0;

		return $display_sections;
	}

	/**
	 * Check view listing by new user
	 * @param integer $user_id user identifier
	 * @param integer $listing_id listing identifier
	 */
	public function check_views_count($user_id, $listing_id) {
		$views_array = $this->CI->session->userdata('views_listings_array');
		if(isset($views_array) && !empty($views_array)){
			if(!in_array($listing_id, $views_array)){
				$views_array[] = $listing_id;
				$this->CI->session->set_userdata('views_listings_array', $views_array);
				$this->update_views_field($user_id, $listing_id);
			}
		}else{
			$this->CI->session->set_userdata('views_listings_array', array($listing_id));
			$this->update_views_field($user_id, $listing_id);
		}
	}

	/**
	 * Update views count
	 * @param integer $user_id listing identifier
	 * @param integer $listing_id listing identifier
	 */
	public function update_views_field($user_id, $listing_id){
		if($this->CI->session->userdata('auth_type') == 'user'){
			$visitor_id = intval($this->CI->session->userdata('user_id'));
			if($user_id != $visitor_id){
				$this->CI->load->model('listings/models/Listings_visitor_model');
				$visitor = $this->CI->Listings_visitor_model->get_visitor_by_visitor($user_id, $visitor_id);
				$save_data = array('id_listing'=>$listing_id, 'id_user'=>$user_id, 'id_visitor'=>$visitor_id);
				$this->CI->Listings_visitor_model->save_visitor($visitor['id'], $save_data);
				$save_data = array('id_listing'=>0, 'id_user'=>$user_id, 'id_visitor'=>$visitor_id);
				$this->CI->Listings_visitor_model->save_visitor($visitor['id'], $save_data);
			}
		}
		
		$this->DB->set('views', 'views + 1', FALSE);
		$this->DB->where('id', $listing_id);
		$this->DB->update(LISTINGS_TABLE, $data);
	}

	/**
	 * Return pdf version
	 * @param string $content listing content
	 * @param array $data listing data
	 */
	public function get_pdf_file($content, $data){
		$content = preg_replace('/<script(.*?)<\/script>/is', '', $content);
		$content = str_replace('&nbsp;', ' ', $content);

		$this->CI->load->library('dompdf');

		$this->CI->dompdf->set_paper('a4', 'portrait');
		$this->CI->dompdf->load_html($content);
		$this->CI->dompdf->render();
		$file_name = $data['gid'].'.pdf';
		$this->CI->dompdf->stream($file_name);
	}
	
	/**
	 * Return settings for seo
	 * @param string $method controller method
	 * @param integer $lang_id language identifier
	 * @return array
	 */
	public function get_seo_settings($method='', $lang_id=''){
		if(!empty($method)){
			return $this->_get_seo_settings($method, $lang_id);
		}else{
			$actions = array('index', 'category', 'location', 'user', 'search', 'open_house', 'privates', 'agents', 'discount', 'view', 'wish_lists', 'wish_list');
			$return = array();
			foreach($actions as $action){
				$return[$action] = $this->_get_seo_settings($action, $lang_id);
			}
			return $return;
		}
	}

	/**
	 * Return setting for seo (internal)
	 * @param string $method controller method
	 * @param integer $lang_id language identifier
	 * @return array
	 */
	private function _get_seo_settings($method, $lang_id=''){
		switch($method){
			case 'index':
				return array(
					'title' => l('seo_tags_index_title', 'listings', $lang_id, 'seo'),
					'keyword' => l('seo_tags_index_keyword', 'listings', $lang_id, 'seo'),
					'description' => l('seo_tags_index_description', 'listings', $lang_id, 'seo'),
					'templates' => array('operation_type', 'operation_type_str', 'category', 'property', 'country', 'region', 'region_code', 'city'),
					'header' => l('seo_tags_index_header', 'listings', $lang_id, 'seo'),
					'url_vars' => array(
						'operation_type' => array('operation_type' => 'literal'),
						/*'order' => array('order' => 'literal'),
						'order_direction' => array('order_direction' => 'literal'),
						'page' => array('page' => 'literal'),*/
					),
				);
			break;
			case 'category':
				return array(
					'title' => l('seo_tags_category_title', 'listings', $lang_id, 'seo'),
					'keyword' => l('seo_tags_category_keyword', 'listings', $lang_id, 'seo'),
					'description' => l('seo_tags_category_description', 'listings', $lang_id, 'seo'),
					'templates' => array('operation_type', 'operation_type_str', 'category', 'property'),
					'header' => l('seo_tags_category_header', 'listings', $lang_id, 'seo'),
					'url_vars' => array(
						'id_category' => array('category' => 'literal'),
						'property_type' => array('property' => 'literal'),
						'operation_type' => array('operation_type' => 'literal'),
						/*'order' => array('order' => 'literal'),
						'order_direction' => array('order_direction' => 'literal'),
						'page' => array('page' => 'literal'),*/
					),
				);
			break;
			case 'location':
				return array(
					'title' => l('seo_tags_location_title', 'listings', $lang_id, 'seo'),
					'keyword' => l('seo_tags_location_keyword', 'listings', $lang_id, 'seo'),
					'description' => l('seo_tags_location_description', 'listings', $lang_id, 'seo'),
					'templates' => array('operation_type', 'operation_type_str', 'country', 'region', 'region_code', 'city'),
					'header' => l('seo_tags_location_header', 'listings', $lang_id, 'seo'),
					'url_vars' => array(
						'id_country' => array('country' => 'literal'),
						'id_region' => array('region' => 'literal'),
						'id_city' => array('city' => 'literal'),
						'operation_type' => array('operation_type' => 'literal'),
						/*'order' => array('order' => 'literal'),
						'order_direction' => array('order_direction' => 'literal'),
						'page' => array('page' => 'literal'),*/
					),
				);
			break;
			case 'user':
				return array(
					'title' => l('seo_tags_user_title', 'listings', $lang_id, 'seo'),
					'keyword' => l('seo_tags_user_keyword', 'listings', $lang_id, 'seo'),
					'description' => l('seo_tags_user_description', 'listings', $lang_id, 'seo'),
					'templates' => array('name', 'user_type', 'user_type_str', 'operation_type', 'operation_type_str'),
					'header' => l('seo_tags_user_header', 'listings', $lang_id, 'seo'),
					'url_vars' => array(
						'id_user' => array('id_user' => 'numeric'),
						'operation_type' => array('operation_type' => 'literal'),
						/*'order' => array('order' => 'literal'),
						'order_direction' => array('order_direction' => 'literal'),
						'page' => array('page' => 'literal'),*/
					),
					'optional' => array(
						'user' => array('user' => 'literal'),
					),
				);
			break;
			case 'search':
				return array(
					'title' => l('seo_tags_search_title', 'listings', $lang_id, 'seo'),
					'keyword' => l('seo_tags_search_keyword', 'listings', $lang_id, 'seo'),
					'description' => l('seo_tags_search_description', 'listings', $lang_id, 'seo'),
					'templates' => array('operation_type', 'operation_type_str', 'category', 'property', 'country', 'region', 'region_code', 'city'),
					'header' => l('seo_tags_search_header', 'listings', $lang_id, 'seo'),
					'url_vars' => array(
						'keyword' => array('keyword' => 'literal'),
						'operation_type' => array('operation_type' => 'literal'),
						/*'order' => array('order' => 'literal'),
						'order_direction' => array('order_direction' => 'literal'),
						'page' => array('page' => 'literal'),*/
					),
				);
			break;
			case 'open_house':
				return array(
					'title' => l('seo_tags_open_house_title', 'listings', $lang_id, 'seo'),
					'keyword' => l('seo_tags_open_house_keyword', 'listings', $lang_id, 'seo'),
					'description' => l('seo_tags_open_house_description', 'listings', $lang_id, 'seo'),
					'templates' => array('operation_type', 'operation_type_str'),
					'header' => l('seo_tags_open_house_header', 'listings', $lang_id, 'seo'),
					'url_vars' => array(
						'operation_type' => array('operation_type' => 'literal'),
						/*'order' => array('order' => 'literal'),
						'order_direction' => array('order_direction' => 'literal'),
						'page' => array('page' => 'literal'),*/
					),
				);
			break;
			case 'privates':
				return array(
					'title' => l('seo_tags_privates_title', 'listings', $lang_id, 'seo'),
					'keyword' => l('seo_tags_privates_keyword', 'listings', $lang_id, 'seo'),
					'description' => l('seo_tags_privates_description', 'listings', $lang_id, 'seo'),
					'templates' => array('operation_type', 'operation_type_str'),
					'header' => l('seo_tags_privates_header', 'listings', $lang_id, 'seo'),
					'url_vars' => array(
						'operation_type' => array('operation_type' => 'literal'),
						/*'order' => array('order' => 'literal'),
						'order_direction' => array('order_direction' => 'literal'),
						'page' => array('page' => 'literal'),*/
					),
				);
			break;
			case 'agents':
				return array(
					'title' => l('seo_tags_agents_title', 'listings', $lang_id, 'seo'),
					'keyword' => l('seo_tags_agents_keyword', 'listings', $lang_id, 'seo'),
					'description' => l('seo_tags_agents_description', 'listings', $lang_id, 'seo'),
					'templates' => array('operation_type', 'operation_type_str'),
					'header' => l('seo_tags_agents_header', 'listings', $lang_id, 'seo'),
					'url_vars' => array(
						'operation_type' => array('operation_type' => 'literal'),
						/*'order' => array('order' => 'literal'),
						'order_direction' => array('order_direction' => 'literal'),
						'page' => array('page' => 'literal'),*/
					),
				);
			break;
			case 'discount':
				return array(
					'title' => l('seo_tags_discount_title', 'listings', $lang_id, 'seo'),
					'keyword' => l('seo_tags_discount_keyword', 'listings', $lang_id, 'seo'),
					'description' => l('seo_tags_discount_description', 'listings', $lang_id, 'seo'),
					'templates' => array('operation_type', 'operation_type_str'),
					'header' => l('seo_tags_discount_header', 'listings', $lang_id, 'seo'),
					'url_vars' => array(
						'operation_type' => array('operation_type' => 'literal'),
						/*'order' => array('order' => 'literal'),
						'order_direction' => array('order_direction' => 'literal'),
						'page' => array('page' => 'literal'),*/
					),
				);
			break;
			case 'view':
				return array(
					'title' => l('seo_tags_view_title', 'listings', $lang_id, 'seo'),
					'keyword' => l('seo_tags_view_keyword', 'listings', $lang_id, 'seo'),
					'description' => l('seo_tags_view_description', 'listings', $lang_id, 'seo'),
					'templates' => array('id', 'operation_type', 'operation_type_str', 'property', 'location', 'country', 'region', 'city', 'address', 'section', 'pdf', 'headline'),
					'header' => l('seo_tags_view_header', 'listings', $lang_id, 'seo'),
					'url_vars' => array(
						'id' => array('id' => 'numeric'),
						'section' => array('section' => 'literal'),
						'pdf' => array('pdf' => 'literal'),
					),
					'optional' => array(
						'operation_type' => array('operation_type' => 'literal'),
						'property' => array('property' => 'literal'),
						'country' => array('country' => 'literal'),
						'region' => array('region' => 'literal'),
						'city' => array('city' => 'literal'),
						'address' => array('address' => 'literal'),
					),
				);
			break;
			case 'wish_lists':
				return array(
					'title' => l('seo_tags_wish_lists_title', 'listings', $lang_id, 'seo'),
					'keyword' => l('seo_tags_wish_lists_keyword', 'listings', $lang_id, 'seo'),
					'description' => l('seo_tags_wish_lists_description', 'listings', $lang_id, 'seo'),
					'templates' => array(),
					'header' => l('seo_tags_wish_lists_header', 'listings', $lang_id, 'seo'),
					'url_vars' => array(),
				);
			break;
			case 'wish_list':
				return array(
					'title' => l('seo_tags_wish_list_title', 'listings', $lang_id, 'seo'),
					'keyword' => l('seo_tags_wish_list_keyword', 'listings', $lang_id, 'seo'),
					'description' => l('seo_tags_wish_list_description', 'listings', $lang_id, 'seo'),
					'templates' => array('id', 'name'),
					'header' => l('seo_tags_wish_list_header', 'listings', $lang_id, 'seo'),
					'url_vars' => array(
						'id' => array('id' => 'numeric'),
					),
				);
			break;
		}
	}

	/**
	 * Get alias field for seo
	 * @param string $var_name_from
	 * @param string $var_name_to
	 * @param mixed $value
	 * @return mixed
	 */
	public function request_seo_rewrite($var_name_from, $var_name_to, $value){
		$listing_data = array();

		if($var_name_from == 'pdf' && $var_name_to == 'pdf'){
			return ($value == 'no')?false:true;
		}
	
		if($var_name_from == 'category' && $var_name_to == 'id_category'){
			$this->CI->load->model('Properties_model');
			
			$property_types = $this->CI->Properties_model->get_categories('property_types', $default_lang);
			foreach($property_types as $option_value=>$option_name){
				if($option_name == $value){
					return $option_value;
				}
			}
			foreach($this->CI->pg_language->languages as $lid => $lang_data){
				if($lid == $default_lang) continue;
				$property_types = $this->CI->Properties_model->get_categories('property_types', $lid);
				foreach($property_types as $option_value=>$option_name){
					if($option_name == $value){
						return $option_value;
					}
				}
			}
			return 0;
		}
		
		if($var_name_from == 'property' && $var_name_to == 'property_type'){
			$default_lang = $this->CI->pg_language->current_lang_id;
			$category_ids = $this->CI->Listings_model->get_category_ids();
			foreach($category_ids as $category_id){
				$this->CI->load->model('Properties_model');
				$property_items = $this->CI->Properties_model->get_properties($category_id, $default_lang);
				if(!empty($property_items)){
					foreach($property_items as $option_value=>$option_name){
						if($option_name == $value){
							return $option_value;
						}
					}
					foreach($this->CI->pg_language->languages as $lid => $lang_data){
						if($lid == $default_lang) continue;
						$property_items = $this->CI->Properties_model->get_properties($category_id, $lid);
						if(empty($property_items)) continue;
						foreach($property_items as $option_value=>$option_name){
							if($option_name == $value){
								return $option_value;
							}
						}
					}
				}
			}
			return 0;
		}
	
		if($var_name_from == 'country' && $var_name_to == 'id_country'){
			$this->CI->load->model('Countries_model');
			$country = $this->CI->Countries_model->get_country_by_name($value);
			if($country){
				return $country['code'];
			}
			return 0;
		}
		
		if($var_name_from == 'region' && $var_name_to == 'id_region'){
			$this->CI->load->model('Countries_model');
			$region = $this->CI->Countries_model->get_region_by_name($value);
			if($region){
				return $region['id'];
			}
			return 0;
		}
		
		if($var_name_from == 'city' && $var_name_to == 'id_city'){
			$this->CI->load->model('Countries_model');
			$city = $this->CI->Countries_model->get_city_by_name($value);
			if($city){
				return $city['id'];
			}
			return 0;
		}
		
		if($var_name_from == 'user' && $var_name_to == 'id_user'){
			$this->CI->load->model('Users_model');
			$users = $this->CI->Users_model->get_users_list_by_filters(array('name'=>$value), 1, 1);
			if(!empty($users)){
				return $users[0]['id'];
			}
			return 0;
		}
		
		return $value;
	}
	
	/**
	 * Return urls for site map
	 */
	function get_sitemap_xml_urls(){
		$this->CI->load->helper('seo');
		$this->CI->load->library('Translit');
	
		$current_lang = $this->CI->pg_language->get_lang_by_id($this->CI->pg_language->current_lang_id);
		$current_lang_code = $current_lang['code'];
				
		$return = array(
			array(
				'url' => rewrite_link('listings', 'index'),
				'priority' => 0.1,
			),
			array(
				'url' => rewrite_link('listings', 'edit'),
				'priority' => 0.3,
			),
			array(
				'url' => rewrite_link('listings', 'apply_service'),
				'priority' => 0.5,
			),
			array(
				'url' => rewrite_link('listings', 'search'),
				'priority' => 0.1,
			),
		);
		$listings = $this->get_listings_list(array('active'=>1));
		foreach($listings as $seo_listing){
			$return[] = array(
				'url' => rewrite_link('listings', 'view', $seo_listing),
				'priority' => 0.5,
			);
		}
		
		return $return;
	}

	/**
     * Return urls for site map
	 */
	function get_sitemap_urls(){
		$this->CI->load->helper('seo');
		$auth = $this->CI->session->userdata('auth_type');
		$user_type = $this->CI->session->userdata('user_type');
		$block = array();
		
		$operation_types = $this->get_operation_types(true);
		if(empty($operation_types)) return $block;
		
		$items = array();
		foreach($operation_types as $operation_type){
			if($operation_type == 'all') continue;
			$items[] = array(
				'name' => l('operation_search_'.$operation_type, 'listings'),
				'link' =>rewrite_link('listings', 'index', array('operation_type'=>$operation_type, 'id_category'=>0, 'property_type'=>0, 'order'=>'default', 'order_direction'=>'DESC', 'page'=>1)),
				'clickable' => true,
			);
		}
		
		$items[] = array(
			'name' => l('header_my_saved_listings', 'listings'),
			'link' => site_url().'listings/saved',
			'clickable' => $auth ? true : false,
		);
		
		$items[] = array(
			'name' => l('header_saved_searches', 'listings'),
			'link' => site_url().'listings/preferences',
			'clickable' => $auth ? true : false,
		);
	
		reset($operation_types);
		$block[] = array(
			'name' => l('header_search_listings', 'listings'),
			'link' => rewrite_link('listings', 'index', array('operation_type'=>current($operation_types), 'id_category'=>0, 'property_type'=>0, 'order'=>'default', 'order_direction'=>'DESC', 'page'=>1)),
			'clickable' => true,
			'items' => $items,
		);		
		
		return $block;
	}
	
	/**
	 * Post listing service activate
	 * @param integer $user_id user identifier
	 * @param float $price service price
	 * @param array $service_admin_data service admin data
	 * @param array $service_user_data service user data
	 */
	public function service_post_activate($user_id, $price, $service_admin_data, $service_user_data){
		$listing_id = $service_user_data['id_listing'];
		$period = $service_admin_data['period'];

		$this->set_format_settings(array('get_location'=>false));
		$listing = $this->get_listing_by_id($listing_id);
		$this->set_format_settings(array('get_location'=>true));
		if(strtotime($listing['post_date_end']) > 86400){
			$uts = strtotime($listing['post_date_end']);
		}else{
			$uts = time();
		}

		$data = array('post_date_end' => $date_end);
		$this->save_listing($listing_id, date('Y-m-d H:i:s', $uts+$period*60*60*24));	
		
		// send notification
		$this->config->load('date_formats', TRUE);
		$date_format = $this->config->item('date_format_date_time_literal', 'date_formats');
		
		$mail_data = array(
			'user'=>$listing['user']['output_name'],
			'name'=>l('service_string_name_post', 'listings', $listing['user']['lang_id']), 
			'date'=>date($date_format, $uts+$period*60*60*24),
		);
		
		$this->CI->load->model('Notifications_model');	
		$this->CI->Notifications_model->send_notification($listing['user']['email'], 'listing_service_enabled', $mail_data);
	}

	/**
	 * Post listing service validate
	 * @param integer $user_id user identifier
	 * @param array $data user data
	 * @param array $service_data service data
	 * @param float $price service price
	 */
	public function service_post_validate($user_id, $data, $service_data=array(), $price=''){
		$return = array('errors'=>array(), 'data'=>$data);
		return $return;
	}
	
	/**
	 * Post listing service clean up cron
	 */
	public function service_post_cron(){
		$this->DB->select(implode(', ', $this->_fields))
				 ->from(LISTINGS_TABLE)
				 ->where('status', 1)
				 ->where('date_expire <', date('Y-m-d H:i:s'))
				 ->where('UNIX_TIMESTAMP(date_expire) >', '86400');
		$results = $this->DB->get()->result_array();
		$clean = 0;
		if(!empty($results) && is_array($results)){
			$data['status'] = '0';
			$this->DB->where('date_expire <', date('Y-m-d H:i:s'));
			$this->DB->where('UNIX_TIMESTAMP(date_expire) >', '86400');
			$this->DB->update(LISTINGS_TABLE, $data);
				
			$this->load->model('Notifications_model');
			$results = $this->format_listings($results);
			foreach($results as $listing_data){
				// send notifications
				$listing_data['listing'] = l('field_id', 'listings', $listing_data['user']['lang_id']).$listing_data['id'].', '.$listing_data['location'];
				$listing_data['status'] = l('text_date_expire', 'listings', $listing_data['user']['lang_id']);
				$this->Notifications_model->send_notification($listing_data['user']['email'], 'listing_status_updated', $listing_data);
				
				$listing_data['status'] = 1;
				
				$listing_new = $listing_data;
				$listing_new['status'] = 0;
				$this->update_module_statistics($listing_data, $listing_new);
			}
		}
		echo 'Make clean(Listings active status): '.count($results).' Listings was remove active status';
	}
	
	/**
	 * Featured listing service activate
	 * @param integer $user_id user identifier
	 * @param float $price service price 
	 * @param array $service_admin_data service admin data
	 * @param array $service_user_data service user data
	 */
	public function service_featured_activate($user_id, $price, $service_admin_data, $service_user_data){
		$listing_id = $service_user_data['id_listing'];
		$period = $service_admin_data['period'];

		$this->set_format_settings(array('get_location'=>false));
		$listing = $this->get_listing_by_id($listing_id);
		$this->set_format_settings(array('get_location'=>true));
		if(strtotime($listing['featured_date_end']) > 86400){
			$uts = strtotime($listing['featured_date_end']);
		}else{
			$uts = time();
		}
		
		$data = array('featured_date_end' => date('Y-m-d H:i:s', $uts+$period*60*60*24));
		$this->save_listing($listing_id, $data);	
		
		// send notification
		$this->config->load('date_formats', TRUE);
		$date_format = $this->config->item('date_format_date_time_literal', 'date_formats');
		
		$mail_data = array(
			'user'=>$listing['user']['output_name'],
			'name'=>l('service_string_name_featured', 'listings', $listing['user']['lang_id']), 
			'date'=>date($date_format, $uts+$period*60*60*24),
		);
		
		$this->CI->load->model('Notifications_model');
		$this->CI->Notifications_model->send_notification($listing['user']['email'], 'listing_service_enabled', $mail_data);
	}
	
	/**
	 * Featured listing service validate
	 * @param integer $user_id user identifier
	 * @param array $data user data
	 * @param array $service_data service data
	 * @param float $price service price
	 */
	public function service_featured_validate($user_id, $data, $service_data=array(), $price=''){
		$return = array('errors'=>array(), 'data'=>$data);
		return $return;
	}
	
	/**
	 * Featured listing service clean up cron
	 */
	public function service_featured_cron(){
		$this->DB->select(implode(', ', $this->_fields))
				 ->from(LISTINGS_TABLE)
				 ->where('featured_date_end <', date('Y-m-d H:i:s'))
				 ->where('UNIX_TIMESTAMP(featured_date_end) >', '86400');
		$results = $this->DB->get()->result_array();
		$clean = 0;
		if(!empty($results) && is_array($results)){
			$clean = count($results);
			if($clean > 0){
				$data['featured_date_end'] = '0000-00-00 00:00:00';
				$this->DB->where('featured_date_end <', date('Y-m-d H:i:s'));
				$this->DB->where('UNIX_TIMESTAMP(featured_date_end) >', '86400');
				$this->DB->update(LISTINGS_TABLE, $data);
				
				// send notification
				$this->load->model('Notifications_model');
				$results = $this->format_listings($results);
				foreach($results as $listing_data){
					$mail_data = array(
						'user'=>$listing_data['user']['output_name'],
						'name'=>l('service_string_name_featured', 'listings', $listing_data['user']['lang_id']),
					);
					$this->Notifications_model->send_notification($listing_data['user']['email'], 'listing_service_expired', $mail_data);
				}
			}
		}
		echo 'Make clean(Listings featured): '.$clean.' Listings was removed from featured';
	}
	
	/**
	 * Lift up listing service activate
	 * @param integer $user_id user identifier
	 * @param float $price service price
	 * @param array $service_admin_data service admin data
	 * @param array $service_user_data service user data
	 */
	public function service_lift_up_activate($user_id, $price, $service_admin_data, $service_user_data){
		$listing_id = $service_user_data['id_listing'];
		$period = $service_admin_data['period'];

		$this->set_format_settings('get_location', false);
		$listing = $this->get_listing_by_id($listing_id);
		$this->set_format_settings('get_location', true);
		if(strtotime($listing['lift_up_date_end']) > 86400){
			$uts = strtotime($listing['lift_up_date_end']);
		}else{
			$uts = time();
		}

		$data = array('lift_up_date_end'=>date('Y-m-d H:i:s', $uts+$period*60*60*24));
		$this->save_listing($listing_id, $data);	
		
		// send notification
		$this->config->load('date_formats', TRUE);
		$date_format = $this->config->item('date_format_date_time_literal', 'date_formats');
		
		$mail_data = array(
			'user'=>$listing['user']['output_name'],
			'name'=>l('service_string_name_lift_up', 'listings', $listing['user']['lang_id']), 
			'date'=>date($date_format, $uts+$period*60*60*24),
		);
		
		$this->CI->load->model('Notifications_model');	
		$this->CI->Notifications_model->send_notification($listing['user']['email'], 'listing_service_enabled', $mail_data);
	}
	
	/**
	 * Lift up listing service validate
	 * @param integer $user_id user identifier
	 * @param array $data user data
	 * @param array $service_data service data
	 * @param float $price service price
	 */
	public function service_lift_up_validate($user_id, $data, $service_data=array(), $price=''){
		$return = array('errors' => array(), 'data' => $data);
		return $return;
	}
	
	/**
	 * Lift up listing service clean up cron
	 */
	public function service_lift_up_cron(){
		$this->DB->select(implode(', ', $this->_fields))
				 ->from(LISTINGS_TABLE)
				 ->where('lift_up_date_end <', date('Y-m-d H:i:s'))
				 ->where('UNIX_TIMESTAMP(lift_up_date_end) >', '86400');
		$results = $this->DB->get()->result_array();
		$clean = 0;
		if(!empty($results) && is_array($results)){
			$clean = count($results);
			if($clean > 0){
				$data['lift_up_date_end'] = '0000-00-00 00:00:00';
				$this->DB->where('lift_up_date_end <', date('Y-m-d H:i:s'));
				$this->DB->where('UNIX_TIMESTAMP(lift_up_date_end) >', '86400');
				$this->DB->update(LISTINGS_TABLE, $data);
				
				// send notification
				$this->load->model('Notifications_model');
				$results = $this->format_listings($results);
				foreach($results as $listing_data){
					$mail_data = array(
						'user'=>$listing_data['user']['output_name'],
						'name'=>l('service_string_name_lift_up', 'listings', $listing_data['user']['lang_id']),
					);
					$this->Notifications_model->send_notification($listing_data['user']['email'], 'listing_service_expired', $mail_data);
				}
			}
		}
		echo 'Make clean(Listings lift up): '.$clean.' Listings was removed from lift up';
	}
	
	/**
	 * Lift up listing in country service activate
	 * @param integer $user_id user identifier
	 * @param float $price service price
	 * @param array $service_admin_data service admin data
	 * @param array $service_user_data service user data
	 */
	public function service_lift_up_country_activate($user_id, $price, $service_admin_data, $service_user_data){
		$listing_id = $service_user_data['id_listing'];
		$period = $service_admin_data['period'];

		$this->set_format_settings('get_location', false);
		$listing = $this->get_listing_by_id($listing_id);
		$this->set_format_settings('get_location', true);
		if(strtotime($listing['lift_up_country_date_end']) > 86400){
			$uts = strtotime($listing['lift_up_country_date_end']);
		}else{
			$uts = time();
		}

		$data = array('lift_up_region_date_end'=>date('Y-m-d H:i:s', $uts+$period*60*60*24));
		$this->save_listing($listing_id, $data);	
		
		// send notification
		$this->config->load('date_formats', TRUE);
		$date_format = $this->config->item('date_format_date_time_literal', 'date_formats');
		
		$mail_data = array(
			'user'=>$listing['user']['output_name'],
			'name'=>l('service_string_name_lift_up', 'listings', $listing['user']['lang_id']), 
			'date'=>date($date_format, $uts+$period*60*60*24),
		);
		
		$this->CI->load->model('Notifications_model');	
		$this->CI->Notifications_model->send_notification($listing['user']['email'], 'listing_service_enabled', $mail_data);
	}
	
	/**
	 * Lift up listing in country service validate
	 * @param integer $user_id user identifier
	 * @param array $data user data
	 * @param array $service_data service data
	 * @param float $price service price
	 */
	public function service_lift_up_country_validate($user_id, $data, $service_data=array(), $price=''){
		$return = array('errors'=>array(), 'data'=>$data);
		return $return;
	}
	
	/**
	 * Lift up listing service clean up cron
	 */
	public function service_lift_up_country_cron(){
		$this->DB->select(implode(', ', $this->_fields))
				 ->from(LISTINGS_TABLE)
				 ->where('lift_up_country_date_end <', date('Y-m-d H:i:s'))
				 ->where('UNIX_TIMESTAMP(lift_up_country_date_end) >', '86400');
		$results = $this->DB->get()->result_array();
		$clean = 0;
		if(!empty($results) && is_array($results)){
			$clean = count($results);
			if($clean > 0){
				$data['lift_up_country_date_end'] = '0000-00-00 00:00:00';
				$this->DB->where('lift_up_country_date_end <', date('Y-m-d H:i:s'));
				$this->DB->where('UNIX_TIMESTAMP(lift_up_country_date_end) >', '86400');
				$this->DB->update(LISTINGS_TABLE, $data);
				
				// send notification
				$this->load->model('Notifications_model');
				$results = $this->format_listings($results);
				foreach($results as $listing_data){
					$mail_data = array(
						'user'=>$listing_data['user']['output_name'],
						'name'=>l('service_string_name_lift_up', 'listings', $listing_data['user']['lang_id']),
					);
					$this->Notifications_model->send_notification($listing_data['user']['email'], 'listing_service_expired', $mail_data);
				}
			}
		}
		echo 'Make clean(Listings lift up in country): '.$clean.' Listings was removed from lift up in country';
	}
	
	/**
	 * Lift up listing in region service activate
	 * @param integer $user_id user identifier
	 * @param float $price service price
	 * @param array $service_admin_data service admin data
	 * @param array $service_user_data service user data
	 */
	public function service_lift_up_region_activate($user_id, $price, $service_admin_data, $service_user_data){
		$listing_id = $service_user_data['id_listing'];
		$period = $service_admin_data['period'];

		$this->set_format_settings('get_location', false);
		$listing = $this->get_listing_by_id($listing_id);
		$this->set_format_settings('get_location', true);
		if(strtotime($listing['lift_up_region_date_end']) > 86400){
			$uts = strtotime($listing['lift_up_region_date_end']);
		}else{
			$uts = time();
		}

		$data = array('lift_up_region_date_end'=>date('Y-m-d H:i:s', $uts+$period*60*60*24));
		$this->save_listing($listing_id, $data);
		
		// send notification
		$this->config->load('date_formats', TRUE);
		$date_format = $this->config->item('date_format_date_time_literal', 'date_formats');
		
		$mail_data = array(
			'user'=>$listing['user']['output_name'],
			'name'=>l('service_string_name_lift_up', 'listings', $listing['user']['lang_id']), 
			'date'=>date($date_format, $uts+$period*60*60*24),
		);
		
		$this->CI->load->model('Notifications_model');	
		$this->CI->Notifications_model->send_notification($listing['user']['email'], 'listing_service_enabled', $mail_data);
	}
	
	/**
	 * Lift up listing in region service validate
	 * @param integer $user_id user identifier
	 * @param array $data user data
	 * @param array $service_data service data
	 * @param float $price service price
	 */
	public function service_lift_up_region_validate($user_id, $data, $service_data=array(), $price=''){
		$return = array('errors'=>array(), 'data'=>$data);
		return $return;
	}
	
	/**
	 * Lift up listing in region service clean up cron
	 */
	public function service_lift_up_region_cron(){
		$this->DB->select(implode(', ', $this->_fields))
				 ->from(LISTINGS_TABLE)
				 ->where('lift_up_region_date_end <', date('Y-m-d H:i:s'))
				 ->where('UNIX_TIMESTAMP(lift_up_region_date_end) >', '86400');
		$results = $this->DB->get()->result_array();
		$clean = 0;
		if(!empty($results) && is_array($results)){
			$clean = count($results);
			if($clean > 0){
				$data['lift_up_region_date_end'] = '0000-00-00 00:00:00';
				$this->DB->where('lift_up_region_date_end <', date('Y-m-d H:i:s'));
				$this->DB->where('UNIX_TIMESTAMP(lift_up_region_date_end) >', '86400');
				$this->DB->update(LISTINGS_TABLE, $data);
				
				// send notification
				$this->load->model('Notifications_model');
				$results = $this->format_listings($results);
				foreach($results as $listing_data){
					$mail_data = array(
						'user'=>$listing_data['user']['output_name'],
						'name'=>l('service_string_name_lift_up', 'listings', $listing_data['user']['lang_id']),
					);
					$this->Notifications_model->send_notification($listing_data['user']['email'], 'listing_service_expired', $mail_data);
				}
			}
		}
		echo 'Make clean(Listings lift up in region): '.$clean.' Listings was removed from lift up in region';
	}
	
	/**
	 * Lift up listing in city service activate
	 * @param integer $user_id user identifier
	 * @param float $price service price
	 * @param array $service_admin_data service admin data
	 * @param array $service_user_data service user data
	 */
	public function service_lift_up_city_activate($user_id, $price, $service_admin_data, $service_user_data){
		$listing_id = $service_user_data['id_listing'];
		$period = $service_admin_data['period'];

		$this->set_format_settings('get_location', false);
		$listing = $this->get_listing_by_id($listing_id);
		$this->set_format_settings('get_location', true);
		if(strtotime($listing['lift_up_city_date_end']) > 86400){
			$uts = strtotime($listing['lift_up_city_date_end']);
		}else{
			$uts = time();
		}

		$data = array('lift_up_city_date_end'=>date('Y-m-d H:i:s', $uts+$period*60*60*24));
		$this->save_listing($listing_id, $data);
		
		// send notification
		$this->config->load('date_formats', TRUE);
		$date_format = $this->config->item('date_format_date_time_literal', 'date_formats');
		
		$mail_data = array(
			'user'=>$listing['user']['output_name'],
			'name'=>l('service_string_name_lift_up', 'listings', $listing['user']['lang_id']), 
			'date'=>date($date_format, $uts+$period*60*60*24),
		);
		
		$this->CI->load->model('Notifications_model');	
		$this->CI->Notifications_model->send_notification($listing['user']['email'], 'listing_service_enabled', $mail_data);
	}
	
	/**
	 * Lift up listing in city service validate
	 * @param integer $user_id user identifier
	 * @param array $data user data
	 * @param array $service_data service data
	 * @param float $price service price
	 */
	public function service_lift_up_city_validate($user_id, $data, $service_data=array(), $price=''){
		$return = array('errors'=>array(), 'data'=>$data);
		return $return;
	}
	
	/**
	 * Lift up listing in city service clean up cron
	 */
	public function service_lift_up_city_cron(){
		$this->DB->select(implode(', ', $this->_fields))
				 ->from(LISTINGS_TABLE)
				 ->where('lift_up_city_date_end <', date('Y-m-d H:i:s'))
				 ->where('UNIX_TIMESTAMP(lift_up_city_date_end) >', '86400');
		$results = $this->DB->get()->result_array();
		$clean = 0;
		if(!empty($results) && is_array($results)){
			$clean = count($results);
			if($clean > 0){
				$data['lift_up_city_date_end'] = '0000-00-00 00:00:00';
				$this->DB->where('lift_up_city_date_end <', date('Y-m-d H:i:s'));
				$this->DB->where('UNIX_TIMESTAMP(lift_up_city_date_end) >', '86400');
				$this->DB->update(LISTINGS_TABLE, $data);
				
				// send notification
				$this->load->model('Notifications_model');
				$results = $this->format_listings($results);
				foreach($results as $listing_data){
					$mail_data = array(
						'user'=>$listing_data['user']['output_name'],
						'name'=>l('service_string_name_lift_up', 'listings', $listing_data['user']['lang_id']),
					);
					$this->Notifications_model->send_notification($listing_data['user']['email'], 'listing_service_expired', $mail_data);
				}
			}
		}
		echo 'Make clean(Listings lift upin city): '.$clean.' Listings was removed from lift up in city';
	}
	
	/**
	 * Highlight listing service activate
	 * @param integer $user_id user identifier
	 * @param float $price service 
	 * @param array $service_admin_data service admin data
	 * @param array $service_user_data service user data
	 */
	public function service_highlight_activate($user_id, $price, $service_admin_data, $service_user_data){
		$listing_id = $service_user_data['id_listing'];
		$period = $service_admin_data['period'];

		$this->set_format_settings('get_location', false);
		$listing = $this->get_listing_by_id($listing_id);
		$this->set_format_settings('get_location', true);
		if(strtotime($listing['highlight_date_end']) > 86400){
			$uts = strtotime($listing['highlight_date_end']);
		}else{
			$uts = time();
		}

		$data = array('highlight_date_end'=>date('Y-m-d H:i:s', $uts+$period*60*60*24));
		$this->save_listing($listing_id, $data);
		
		// send notification
		$this->config->load('date_formats', TRUE);
		$date_format = $this->config->item('date_format_date_time_literal', 'date_formats');
		
		$mail_data = array(
			'user'=>$listing['user']['output_name'],
			'name'=>l('service_string_name_highlight', 'listings', $listing['user']['lang_id']), 
			'date'=>date($date_format, $uts+$period*60*60*24),
		);
		
		$this->CI->load->model('Notifications_model');	
		$this->CI->Notifications_model->send_notification($listing['user']['email'], 'listing_service_enabled', $mail_data);
	}
	
	/**
	 * Highlight listing service validate
	 * @param integer $user_id user identifier
	 * @param array $data user data
	 * @param array $service_data service data
	 * @param float $price service price
	 */
	public function service_highlight_validate($user_id, $data, $service_data=array(), $price=''){
		$return = array('errors'=>array(), 'data'=>$data);
		return $return;
	}
	
	/**
	 * Highlight listing service clean up cron
	 */
	public function service_highlight_cron(){
		$this->DB->select(implode(', ', $this->_fields))
				 ->from(LISTINGS_TABLE)
				 ->where('highlight_date_end <', date('Y-m-d H:i:s'))
				 ->where('UNIX_TIMESTAMP(highlight_date_end) >', '86400');
		$results = $this->DB->get()->result_array();
		$clean = 0;
		if(!empty($results) && is_array($results)){
			$clean = count($results);
			if($clean > 0){
				$data['highlight_date_end'] = '0000-00-00 00:00:00';
				$this->DB->where('highlight_date_end <', date('Y-m-d H:i:s'));
				$this->DB->where('UNIX_TIMESTAMP(highlight_date_end) >', '86400');
				$this->DB->update(LISTINGS_TABLE, $data);
				
				// send notification
				$this->load->model('Notifications_model');
				$results = $this->format_listings($results);
				foreach($results as $listing_data){
					$mail_data = array(
						'user'=>$listing_data['user']['output_name'],
						'name'=>l('service_string_name_highlight', 'listings', $listing_data['user']['lang_id']),
					);
					$this->Notifications_model->send_notification($listing_data['user']['email'], 'listing_service_expired', $mail_data);
				}
			}
		}
		echo 'Make clean(Listings highlight): '.$clean.' Listings was removed from highlight';
	}
	
	/**
	 * Slide show listing service activate
	 * @param integer $user_id user identifier
	 * @param float $price service price
	 * @param array $service_admin_data service admin data
	 * @param array $service_user_data service user data
	 */
	public function service_slide_show_activate($user_id, $price, $service_admin_data, $service_user_data){
		$listing_id = $service_user_data['id_listing'];
		$period = $service_admin_data['period'];

		$this->set_format_settings('get_location', false);
		$listing = $this->get_listing_by_id($listing_id);
		$this->set_format_settings('get_location', true);
		if(strtotime($listing['slide_show_date_end']) > 86400){
			$uts = strtotime($listing['slide_show_date_end']);
		}else{
			$uts = time();
		}

		$data = array('slide_show_date_end'=>date('Y-m-d H:i:s', $uts+$period*60*60*24));
		$this->save_listing($listing_id, $data);
		
		// send notification
		$this->config->load('date_formats', TRUE);
		$date_format = $this->config->item('date_format_date_time_literal', 'date_formats');
		
		$mail_data = array(
			'user'=>$listing['user']['output_name'],
			'name'=>l('service_string_name_slide_show', 'listings', $listing['user']['lang_id']), 
			'date'=>date($date_format, $uts+$period*60*60*24),
		);
		
		$this->CI->load->model('Notifications_model');	
		$this->CI->Notifications_model->send_notification($listing['user']['email'], 'listing_service_enabled', $mail_data);	
	}
	
	/**
	 * Slide show listing service validate
	 * @param integer $user_id user identifier
	 * @param array $data user data
	 * @param array $service_data service data
	 * @param float $price service price
	 */
	public function service_slide_show_validate($user_id, $data, $service_data=array(), $price=''){
		$return = array('errors'=>array(), 'data'=>$data);
		return $return;
	}
	
	/**
	 * Slide show listing service clean up cron
	 */
	public function service_slide_show_cron(){
		$this->DB->select(implode(', ', $this->_fields))
				 ->from(LISTINGS_TABLE)
				 ->where('slide_show_date_end <', date('Y-m-d H:i:s'))
				 ->where('UNIX_TIMESTAMP(slide_show_date_end) >', '86400');
		$results = $this->DB->get()->result_array();
		$clean = 0;
		if(!empty($results) && is_array($results)){
			$clean = count($results);
			if($clean > 0){
				$data['slide_show_date_end'] = '0000-00-00 00:00:00';
				$this->DB->where('slide_show_date_end <', date('Y-m-d H:i:s'));
				$this->DB->where('UNIX_TIMESTAMP(slide_show_date_end) >', '86400');
				$this->DB->update(LISTINGS_TABLE, $data);
				
				// send notification
				$this->load->model('Notifications_model');
				$results = $this->format_listings($results);
				foreach($results as $listing_data){
					$mail_data = array(
						'user'=>$listing_data['user']['output_name'],
						'name'=>l('service_string_name_slide_show', 'listings', $listing_data['user']['lang_id']),
					);
					$this->Notifications_model->send_notification($listing_data['user']['email'], 'listing_service_expired', $mail_data);
				}
			}
		}
		echo 'Make clean(Listings slide_show): '.$clean.' Listings was removed from slide_show';
	}
	
	# MOD for Slider image service #
	/**
	 * Slider listing service activate
	 * @param integer $user_id user identifier
	 * @param float $price service price
	 * @param array $service_admin_data service admin data
	 * @param array $service_user_data service user data
	 */
	public function service_slider_activate($user_id, $price, $service_admin_data, $service_user_data){
		$listing_id = $service_user_data['id_listing']; // this will return 14181
		$period = $service_admin_data['period']; // this will return 1

		$this->set_format_settings('get_location', false);
		$listing = $this->get_listing_by_id($listing_id);

		$this->set_format_settings('get_location', true);
		# strtotime($listing['slider_date_end']) --> will result " -62170006286 "
		if(strtotime($listing['slider_date_end']) > 86400){
			$uts = strtotime($listing['slider_date_end']);
		}else{
			$uts = time(); # will result like this, 1444319277
		}

		$data = array('slider_date_end'=>date('Y-m-d H:i:s', $uts+$period*60*60*24));
		//print_r($data); exit;
			# above will return the date : Array ( [slider_date_end] => 2015-10-09 22:51:58 )
		$this->save_listing($listing_id, $data); // update pg_listings (slider_date_end)
		
		// send notification
		$this->config->load('date_formats', TRUE);
		$date_format = $this->config->item('date_format_date_time_literal', 'date_formats'); // this will return 'd F Y, H:i'
		$mail_data = array(
			'user'=>$listing['user']['output_name'],
			'name'=>l('service_string_name_slider', 'listings', $listing['user']['lang_id']), 
			'date'=>date($date_format, $uts+$period*60*60*24),
		);
		# $mail_data returns > Array ( [user] => Willy Susanto [name] => Slider listing [date] => 14 October 2015, 06:46 )
		
		$this->CI->load->model('Notifications_model');	
		$this->CI->Notifications_model->send_notification($listing['user']['email'], 'listing_service_enabled', $mail_data);	
	}
	
	/**
	 * Slider listing service validate
	 * @param integer $user_id user identifier
	 * @param array $data user data
	 * @param array $service_data service data
	 * @param float $price service price
	 */
	public function service_slider_validate($user_id, $data, $service_data=array(), $price=''){
		
		$return = array('errors'=>array(), 'data'=>$data);
		return $return;
	}
	
	/**
	 * Slider listing service clean up cron
	 */
	public function service_slider_cron(){
		$this->DB->select(implode(', ', $this->_fields))
				 ->from(LISTINGS_TABLE)
				 ->where('slider_date_end <', date('Y-m-d H:i:s'))
				 ->where('UNIX_TIMESTAMP(slider_date_end) >', '86400');
		$results = $this->DB->get()->result_array();
		$clean = 0;
		if(!empty($results) && is_array($results)){
			$clean = count($results);
			if($clean > 0){
				$data['slider_date_end'] = '0000-00-00 00:00:00';
				$this->DB->where('slider_date_end <', date('Y-m-d H:i:s'));
				$this->DB->where('UNIX_TIMESTAMP(slider_date_end) >', '86400');
				$this->DB->update(LISTINGS_TABLE, $data);
				
				// send notification
				$this->load->model('Notifications_model');
				$results = $this->format_listings($results);
				foreach($results as $listing_data){
					$mail_data = array(
						'user'=>$listing_data['user']['output_name'],
						'name'=>l('service_string_name_slider', 'listings', $listing_data['user']['lang_id']),
					);
					$this->Notifications_model->send_notification($listing_data['user']['email'], 'listing_service_expired', $mail_data);
				}
			}
		}
		echo 'Make clean(Listings slider): '.$clean.' Listings was removed from slider';
	}
	# END MOD for Slider image service #

	/**
	 * Return form fields from field editor
	 * @param string $firm_gid form GUID
	 * @param boolean $admin_mode admin mode
	 * @return array
	 */
	public function get_search_extend_form($property_type_gid, $form_gid, $admin_mode=true){
		$this->CI->load->model('Field_editor_model');
		$this->CI->Field_editor_model->initialize($property_type_gid);
		$form_gid = $this->{$this->{$form_gid}.'_'.$property_type_gid.'_gid'}; 
		$this->CI->load->model('field_editor/models/Field_editor_forms_model');
		$form = $this->CI->Field_editor_forms_model->get_form_by_gid($form_gid, $property_type_gid);
		$form = $this->CI->Field_editor_forms_model->format_output_form($form);
		return $form['field_data'];
	}
	
	/**
	 * Dynamic block callback method for returning form slider listings
	 * @param array $params block p[arameters
	 * @param string $view block view
	 * @return string
	 */
	public function _dynamic_block_get_form_slider_listings($params, $view='latest_added_1600_440'){
		$count = $params['count'] ? intval($params['count']) : 8;
		
		$sep_pos = strrpos(substr($view, 0, strrpos($view, '_')), '_');
	
		$type = substr($view, 0, $sep_pos);
		$this->CI->template_lite->assign('type', $type);
		
		$view = substr($view, $sep_pos+1);
		$this->CI->template_lite->assign('view', $view);
	
		$filters = array('active'=>1, 'slider_image'=>1);
		$order_by = array();
		
		switch($type){
			case 'featured':
				$filters['featured'] = 1;
				$order_by = array('RAND()'=>'');
			break;
			case 'latest_added':
				$order_by = array('date_created'=>'DESC');
			break;
			case 'sale':
				$filters['type'] = 'sale';
				$order_by = array('date_created'=>'DESC');
			break;
			case 'rent':
				$filters['type'] = 'rent';
				$order_by = array('date_created'=>'DESC');
			break;
		}
	
		$listings = $this->get_listings_list($filters, 1, $count, $order_by);
		$this->CI->template_lite->assign('listings', $listings);
	
		$slider_form_block = $this->get_slider_form('sale'); 
		$this->CI->template_lite->assign('slider_form_block', $slider_form_block);
		
		$slider_auto =  $this->CI->pg_module->get_module_config('listings', 'slider_auto');
		$slider_rotation =  $this->CI->pg_module->get_module_config('listings', 'slider_rotation');
		
		$page_data = array('count'=>count($listings), 'slider_auto'=>$slider_auto, 'rotation'=>$slider_rotation*1000);
		$this->CI->template_lite->assign('slider_form_page_data', $page_data);

		$this->CI->load->helper('start');
		$slider_search_form = main_search_form('sale', 'slider', true);
		$this->template_lite->assign('slider_search_form', $slider_search_form);
		
		# MOD for new slider_images #
		$datas = $this->get_active_sliders();
		$active_slider = $this->_format_slider_data($datas);
		$this->template_lite->assign('sliders', $active_slider);
		# MOD for new slider_images #
		
		return $this->CI->template_lite->fetch('helper_listings_block_form_slider', 'user', 'listings');
	}
	
	#MOD for new slider_image#
	public function get_active_sliders() {
		date_default_timezone_set("Asia/Jakarta");
		$date = date('Y-m-d H:i:s');
		
		# get max slider to be display
		$count = $this->_num_of_slider_to_show(9);

		$qry = "SELECT * FROM ".SLIDERS_IMAGES." WHERE '".$date."' <= date_end AND '".$date."' >= date_start AND stts = 1 ORDER BY sorter ASC LIMIT ".$count;
		$result = $this->DB->query($qry)->result_array();
		$sliders = $this->_format_slider_data($result);

		return $sliders;
	}

	private function _format_slider_data($data) {
		foreach($data as $key=>$val) {
			switch( $val['id_listing'] ) {
				case 0:
					$path_url = base_url().'ext/slider-image/uploads/slider-photo/'.$val['id_slider'].'/1600_440-'.$val['slider_name'];
					break;
				default:
					$path_url = base_url().'uploads/listing-slider-photo/'.$val['id_listing'].'/1600_440-'.$val['slider_name'];
					break;
			}
			
			$format = array(
				'media' => $path_url,
				'key' => $key
			);
			$result[] = array_merge($val, $format);
		}
		
		return $result;
	}
	
	public function get_slider_name_by_id($listing_id)
	{
		$this->DB->select('slider_image')
			->from(LISTINGS_TABLE)
			->where('id', $listing_id);
		
		$result = $this->DB->get()->result_array();
		$result = $result[0]['slider_image'];
		
		return $result;
	}
	
	public function save_request_slider($data) {
		$this->db->insert(SLIDERS_IMAGES, $data);
	}
	
	private function _num_of_slider_to_show($id_block) {
		$result = $this->DB->get_where(DYNAMIC_AREA_BLOCK, array('id_block' => $id_block))->result_array();
		$params = $result[0]['params'];
		$count = unserialize($params);
		$return = $count['count'];

		return $return;
	}

	public function _update_slider_image($listing_id, $filename){
//		echo $listing_id;
//		echo "<br>";
//		print_r($filename);
//		exit;
		$this->DB->where('id', $listing_id);
		$this->DB->update(LISTINGS_TABLE, $filename);
		
		//echo $filename;
	}
	#END MOD for new slider_image#

	/**
	 * Return slider form
	 * @param string $operation_type operation type
	 * @param integer $rand random number
	 */
	public function get_slider_form($operation_type, $rand=null){
		$this->CI->load->model('Properties_model');
		$property_types = $this->CI->Properties_model->get_categories('property_types', $this->CI->pg_language->current_lang_id);
		$this->CI->template_lite->assign('property_types', $property_types);
		
		$property_items = $this->CI->Properties_model->get_all_properties('property_types', $this->CI->pg_language->current_lang_id);
		$this->CI->template_lite->assign('property_items', $property_items);
	
		$operation_types = $this->get_operation_types(true);
		if(!$operation_type || !in_array($operation_type, $operation_types)) $operation_type = current($operation_types);

		//get search criteria
		$filters = $_SESSION['listings_search']['data'];
		$this->CI->template_lite->assign('data', $filters);
		
		if(!$rand) $rand = rand(100000, 999999);
		
		$search_action_data = $this->get_search_link_data();
		$search_action_data['keyword'] = '';
		$search_action_data['page'] = 1;
		
		$form_settings = array(
			'action_data' => $search_action_data,
			'object' => $operation_type,
			'type' => 'short',
			'rand' => $rand,
		);
		$this->CI->template_lite->assign('slider_form_settings', $form_settings);
		
		$show_data = true;
		if($show_data && !empty($_SESSION['listings_search']['data'])){
			$this->CI->template_lite->assign('data', $_SESSION['listings_search']['data']);
		}
		
		return $this->CI->template_lite->fetch('helper_slider_search_form', 'user', 'listings');
	}
	
	/**
	 * Dynamic block callback method for returning slider listings
	 * @param array $params block parameters
	 * @param string $view block view
	 * @return string
	 */
	public function _dynamic_block_get_slider_listings($params, $view='latest_added_980_440'){
		$count = $params['count'] ? intval($params['count']) : 8;
		
		$sep_pos = strrpos(substr($view, 0, strrpos($view, '_')), '_');
	
		$type = substr($view, 0, $sep_pos);
		$this->CI->template_lite->assign('type', $type);
		
		$view = substr($view, $sep_pos+1);
		$this->CI->template_lite->assign('view', $view);
	
		$filters = array('active'=>1, 'slider_image'=>1);
		$order_by = array();
	
		switch($type){
			case 'featured':
				$filters['featured'] = 1;
				$order_by = array('RAND()'=>'');
			break;
			case 'latest_added':
				$order_by = array('date_created'=>'DESC');
			break;
			case 'sale':
				$filters['type'] = 'sale';
				$order_by = array('date_created'=>'DESC');
			break;
			case 'rent':
				$filters['type'] = 'rent';
				$order_by = array('date_created'=>'DESC');
			break;
		}
	
		$listings = $this->_get_listings_list($filters, 1, $count, $order_by);
		$this->CI->template_lite->assign('listings', $listings);
	
		$this->CI->load->model('Properties_model');
		$property_types = $this->CI->Properties_model->get_categories('property_types', $this->CI->pg_language->current_lang_id);
		$this->CI->template_lite->assign('property_types', $property_types);
		
		$slider_auto =  $this->CI->pg_module->get_module_config('listings', 'slider_auto');
		$slider_rotation =  $this->CI->pg_module->get_module_config('listings', 'slider_rotation');
		
		$page_data = array(
			'rand' => rand(100000, 999999),
			'count' => count($listings),
			'visible' => 5,
			'slider_auto' => $slider_auto,
			'rotation' => $slider_rotation*1000,
		);
		$this->CI->template_lite->assign('listings_page_data', $page_data);
		
		return $this->CI->template_lite->fetch('helper_listings_block_slider', 'user', 'listings');
	}
	
	/**
	 * Dynamic block callback method for returning featured listings
	 * @param array $params block parameters
	 * @param string $view block view
	 * @param integer $width block size
	 * @return string
	 */
	public function _dynamic_block_get_featured_listings($params, $view='gallery_middle', $width=100){
		$count = $params['count'] ? intval($params['count']) : 8;
		
		$sep_pos = strrpos($view, '_');
		
		$size = substr($view, $sep_pos+1);
		$this->CI->template_lite->assign('photo_size', $size);
		
		$view = substr($view, 0, $sep_pos);
		
		$filters = array('active'=>1, 'featured'=>1);
		
		$listings = $this->get_listings_list($filters, 1, $count, array('RAND()'=>''));
		$this->CI->template_lite->assign('listings', $listings);
		
		$this->CI->template_lite->assign('type', 'featured');
		
		$page_data = array(
			'rand' => rand(100000, 999999),
			'count' => count($listings),
			'visible' => round(($view == 'scroller' ? 2.5 : 4)*$width/100),
		);
		$this->CI->template_lite->assign('listings_page_data', $page_data);
		
		return $this->CI->template_lite->fetch('helper_listings_block_'.$view, 'user', 'listings');
	}
	
	/**
	 * Dynamic block callback method for returning latest added listings
	 * @param array $params block parameters
	 * @param string $view block view
	 * @param integer $width block size
	 * @return string
	 */
	public function _dynamic_block_get_latest_added_listings($params, $view='gallery_middle', $width=100){
		$count = $params['count'] ? intval($params['count']) : 8;
		
		$sep_pos = strrpos($view, '_');
		
		$size = substr($view, $sep_pos+1);
		$this->CI->template_lite->assign('photo_size', $size);

		$view = substr($view, 0, $sep_pos);
	
		$filters = array('active'=>1);
		
		$listings = $this->get_listings_list($filters, 1, $count, array('date_created'=>'DESC'));
		$this->CI->template_lite->assign('listings', $listings);
	
		$this->CI->template_lite->assign('type', 'latest_added');

		$page_data = array(
			'rand' => rand(100000, 999999),
			'count' => count($listings),
			'visible' => round(($view == 'scroller' ? 2.5 : 4)*$width/100),
		);
		$this->CI->template_lite->assign('listings_page_data', $page_data);

		return $this->CI->template_lite->fetch('helper_listings_block_'.$view, 'user', 'listings');
	}
	
	/**
	 * Dynamic block callback method for returning for sale listings
	 * @param array $params block parameters
	 * @param string $view block view
	 * @param integer $width block size
	 * @return string
	 */
	public function _dynamic_block_get_sale_listings($params, $view='gallery_middle', $width=100){
		$count = $params['count'] ? intval($params['count']) : 8;
		$sep_pos = strrpos($view, '_');
		
		$size = substr($view, $sep_pos+1);
		$this->CI->template_lite->assign('photo_size', $size);
		
		$view = substr($view, 0, $sep_pos);
		
		$filters = array('type'=>'sale', 'active'=>1);
		
		$listings = $this->get_listings_list($filters, 1, $count, array('date_created'=>'DESC'));
		$this->CI->template_lite->assign('listings', $listings);
		
		$this->CI->template_lite->assign('type', 'sale');
		
		$page_data = array(
			'rand' => rand(100000, 999999),
			'count' => count($listings),
			'visible' => round(($view == 'scroller' ? 2.5 : 4)*$width/100),
		);
		$this->CI->template_lite->assign('listings_page_data', $page_data);
		
		return $this->CI->template_lite->fetch('helper_listings_block_'.$view, 'user', 'listings');
	}
	
	/**
	 * Dynamic block callback method for returning for buy listings
	 * @param array $params block parameters
	 * @param string $view block view
	 * @param integer $width block size
	 * @return string
	 */
	public function _dynamic_block_get_buy_listings($params, $view='gallery_middle', $width=100){
		$count = $params['count'] ? intval($params['count']) : 8;

		$sep_pos = strrpos($view, '_');
		
		$size = substr($view, $sep_pos+1);
		$this->CI->template_lite->assign('photo_size', $size);
		
		$view = substr($view, 0, $sep_pos);
		
		$filters = array('type'=>'buy', 'active'=>1);
		
		$listings = $this->get_listings_list($filters, 1, $count, array('date_created'=>'DESC'));
		$this->CI->template_lite->assign('listings', $listings);
		
		$this->CI->template_lite->assign('type', 'buy');
		
		$page_data = array(
			'rand' => rand(100000, 999999),
			'count' => count($listings),
			'visible' => round(($view == 'scroller' ? 2.5 : 4)*$width/100),
		);
		$this->CI->template_lite->assign('listings_page_data', $page_data);
		
		return $this->CI->template_lite->fetch('helper_listings_block_'.$view, 'user', 'listings');
	}
	
	/**
	 * Dynamic block callback method for returning for rent listings
	 * @param array $params block parameters
	 * @param string $view block view
	 * @param integer $width block size
	 * @return string
	 */
	public function _dynamic_block_get_rent_listings($params, $view='gallery_middle', $width=100){
		$count = $params['count'] ? intval($params['count']) : 8;
		$sep_pos = strrpos($view, '_');
		
		$size = substr($view, $sep_pos+1);
		$this->CI->template_lite->assign('photo_size', $size);
		
		$view = substr($view, 0, $sep_pos);
		
		$filters = array('type'=>'rent', 'active'=>1);
		
		$listings = $this->get_listings_list($filters, 1, $count, array('date_created'=>'DESC'));
		$this->CI->template_lite->assign('listings', $listings);
		
		$this->CI->template_lite->assign('type', 'rent');
		
		$page_data = array(
			'rand' => rand(100000, 999999),
			'count' => count($listings),
			'visible' => round(($view == 'scroller' ? 2.5 : 4)*$width/100),
		);
		$this->CI->template_lite->assign('listings_page_data', $page_data);
		
		return $this->CI->template_lite->fetch('helper_listings_block_'.$view, 'user', 'listings');
	}
	
	/**
	 * Dynamic block callback method for returning for lease listings
	 * @param array $params block parameters
	 * @param string $view block view
	 * @param integer $width block size
	 * @return string
	 */
	public function _dynamic_block_get_lease_listings($params, $view='gallery_middle', $width=100){
		$count = $params['count'] ? intval($params['count']) : 8;
		$sep_pos = strrpos($view, '_');
		
		$size = substr($view, $sep_pos+1);
		$this->CI->template_lite->assign('photo_size', $size);
		
		$view = substr($view, 0, $sep_pos);
		
		$filters = array('type'=>'lease', 'active'=>1);
		
		$listings = $this->get_listings_list($filters, 1, $count, array('date_created'=>'DESC'));
		$this->CI->template_lite->assign('listings', $listings);
		
		$this->CI->template_lite->assign('type', 'lease');
		
		$page_data = array(
			'rand' => rand(100000, 999999),
			'count' => count($listings),
			'visible' => round(($view == 'scroller' ? 2.5 : 4)*$width/100),
		);
		$this->CI->template_lite->assign('listings_page_data', $page_data);
		
		return $this->CI->template_lite->fetch('helper_listings_block_'.$view, 'user', 'listings');
	}
	
	/**
	 * Dynamic block callback method for returning listings for sale in categories
	 * @param array $params block parameters
	 * @param string $view block view
	 * @return string
	 */
	public function _dynamic_block_get_sale_categories_search($params, $view='default'){
		return $this->_dynamic_block_get_categories_search('sale', $params, $view);
	}
	
	/**
	 * Dynamic block callback method for returning listings for buy in categories
	 * @param array $params block parameters
	 * @param string $view block view
	 * @return string
	 */
	public function _dynamic_block_get_buy_categories_search($params, $view='default'){
		return $this->_dynamic_block_get_categories_search('buy', $params, $view);
	}
	
	/**
	 * Dynamic block callback method for returning listings for rent in categories
	 * @param array $params block parameters
	 * @param string $view block view
	 * @return string
	 */
	public function _dynamic_block_get_rent_categories_search($params, $view='default'){
		return $this->_dynamic_block_get_categories_search('rent', $params, $view);
	}
	
	/**
	 * Dynamic block callback method for returning listings for lease in categories
	 * @param array $params block parameters
	 * @param string $view block view
	 * @return string
	 */
	public function _dynamic_block_get_lease_categories_search($params, $view='default'){
		return $this->_dynamic_block_get_categories_search('lease', $params, $view);
	}
	
	/**
	 * Dynamic block callback method for returning listings in categories
	 * @param string $type operatrion type
	 * @param array $params block parameters
	 * @param string $view block view
	 * @return string
	 */
	private function _dynamic_block_get_categories_search($type, $params, $view='default'){
		
		$this->CI->load->model('Properties_model');
		$property_types = $this->CI->Properties_model->get_categories('property_types', $this->CI->pg_language->current_lang_id);
		$property_items = $this->CI->Properties_model->get_all_properties('property_types', $this->CI->pg_language->current_lang_id);
		$this->CI->template_lite->assign('property_types', $property_types);
		
		$this->CI->load->model('listings/models/Listings_stat_model');

		$i = 0;
		$max = 0;
		$items = array();
		$stat_gids = array();
		foreach($property_types as $property_type_gid=>$property_type_name){
			$items[$i] = array();
			$items[$i][] = array('name'=>$property_type_name, 'gid'=>'property_type_'.$property_type_gid, 'group'=>true, 'empty'=>false);
			$max = max($max, count($property_items[$property_type_gid]));
			foreach($property_items[$property_type_gid] as $subtype_gid=>$subtype_name){
				$items[$i][] = array(
					'name'=>$subtype_name, 
					'gid'=>'property_type_'.$property_type_gid.'_'.$subtype_gid, 
					'data'=>array(
						'operation_type'=>$type, 
						'id_category'=>$property_type_gid, 
						'category'=>$property_type_name,
						'property_type'=>$subtype_gid, 
						'property'=>$subtype_name,
						'order'=>'default',
						'order_direction'=>'DESC',
						'page'=>1,
					),
					'group'=>false, 
					'empty'=>false
				);
				$stat_gids[] = 'property_type_'.$property_type_gid.'_'.$subtype_gid;
			}
			$stat_gids[] = 'property_type_'.$property_type_gid;
			$i++;
		}
		
		$stats = $this->CI->Listings_stat_model->get_stat_by_gids($stat_gids);		
		foreach($items as $i=>$item){
			foreach($item as $index => $value){
				$item[$index]['statistics'] = array('listings_active'=>isset($stats[$item[$index]['gid']][$type.'_cnt']) ? $stats[$item[$index]['gid']][$type.'_cnt'] : 0);				
			}
			$items[$i] = array_pad($item, $max, array('name'=>'&nbsp;', 'group'=>false, 'empty'=>true));
		}
		
		$block_view = 'property_types';

		$this->CI->template_lite->assign('listings_categories', $items);
		
		$this->CI->template_lite->assign('block_view', $block_view);

		$categories_count = count($listings_categories);
		$this->CI->template_lite->assign('listings_categories_count', $categories_count);

		$columns = !empty($params['columns'])?intval($params['columns']):3;
		$this->CI->template_lite->assign('columns', $columns);

		$listings_per_column = ceil($categories_count/$columns);
		$this->CI->template_lite->assign('listings_per_column', $listings_per_column);
		
		$this->CI->template_lite->assign('type', $type);

		return $this->CI->template_lite->fetch('helper_categories_search', 'user', 'listings');
	}
	
	/**
	 * Dynamic block callback method for returning listings for sale in regions
	 * @param array $params block parameters
	 * @param string $view block view
	 * @return string
	 */
	public function _dynamic_block_get_sale_regions_search($params, $view='default'){
		return $this->_dynamic_block_get_regions_search('sale', $params, $view);
	}
	
	/**
	 * Dynamic block callback method for returning listings for buy in regions
	 * @param array $params block parameters
	 * @param string $view block view
	 * @return string
	 */
	public function _dynamic_block_get_buy_regions_search($params, $view='default'){
		return $this->_dynamic_block_get_regions_search('buy', $params, $view);
	}
	
	/**
	 * Dynamic block callback method for returning listings for rent in regions
	 * @param array $params block parameters
	 * @param string $view block view
	 * @return string
	 */
	public function _dynamic_block_get_rent_regions_search($params, $view='default'){
		return $this->_dynamic_block_get_regions_search('rent', $params, $view);
	}
	
	/**
	 * Dynamic block callback method for returning listings for lease in regions
	 * @param array $params block parameters
	 * @param string $view block view
	 * @return string
	 */
	public function _dynamic_block_get_lease_regions_search($params, $view='default'){
		return $this->_dynamic_block_get_regions_search('lease', $params, $view);
	}
	
	/**
	 * Return listings in regions
	 * @param string $type operation type
	 * @param array $params block parameters
	 * @param string $view block view
	 * @return string
	 */
	private function _dynamic_block_get_regions_search($type, $params, $view='default'){
		$listings_regions = array();
		
		$this->CI->load->helper('seo');
		
		$this->CI->load->model('Countries_model');
		$this->CI->load->model('listings/models/Listings_stat_model');
		
		$stat_gids = array();
		
		$locations = $this->CI->Countries_model->get_countries(null, array(), array(), $this->CI->pg_language->current_lang_id);
		if(count($locations) == 1){
			$country = current($locations);
			$country_name = $country['name'];
			$locations = $this->CI->Countries_model->get_regions($country['code'], null, array(), array(), $this->CI->pg_language->current_lang_id);
			if(count($locations) == 1){
				$region = current($locations);
				$region_name = $region['name'];
				$locations = $this->CI->Countries_model->get_cities(1, 30, array(), array(), array(), $this->CI->pg_language->current_lang_id);
				foreach($locations as $location){
					$stat_gids[] = 'city_'.$location['id'];
				}
				$gid_prefix = 'city_';
				$field_name = 'id';
				$this->template_lite->assign('field_name', 'id_city');
			}else{
				foreach($locations as $location){
					$stat_gids[] = 'region_'.$location['id'];
				}
				$gid_prefix = 'region_';
				$field_name = 'id';
				$this->template_lite->assign('field_name', 'id_region');
			}
		}else{
			foreach($locations as $location){
				$stat_gids[] = 'country_'.strtolower($location['code']);
			}
			$gid_prefix = 'country_';
			$field_name = 'code';
			$this->template_lite->assign('field_name', 'id_country');
		}
		
		if(empty($locations)) return '';
	
		$stats = $this->CI->Listings_stat_model->get_stat_by_gids($stat_gids);	
	
		if(!function_exists('_cmp_locations')){
			function _cmp_locations($a, $b){
				return strcmp($a['name'], $b['name']);
			}
		}
		
		usort($locations, _cmp_locations);
		$max = ceil(count($locations)/3)*3;
		for($i = 0; $i < $max; $i++){
			if(isset($locations[$i])){
					if(isset($locations[$i]['country_code'])){
						$country = $locations[$i]['country_code'];
						if(isset($locations[$i]['id_region'])){
							$region = $locations[$i]['id_region'];
							$city_name = $locations[$i]['name'];
						}else{
							$region = $locations[$i]['id'];
							$city_name = 'all';
						}
					}else{
						$country = $locations[$i]['code'];
						$country_name = $locations[$i]['name'];
						$region = 0;
						$region_name = 'all';
						$city = 0;
						$city_name = 'all';
					}
				
					$seolink = rewrite_link('listings', 'search', $seo_data);
					$listings_regions[] =array(
						'id'=>$locations[$i][$field_name],
						'name'=>$locations[$i]['name'],
						'data'=>array(
							'operation_type'=>$type, 
							'id_country'=>$country,
							'country'=>$country_name,
							'id_region'=>$region,
							'region'=>$region_name,
							'id_city'=>$city,
							'city'=>$city_name,
							'order'=>'default',
							'order_direction'=>'DESC',
							'page'=>1,
						),
						'statistics' => array(
							'listing_active' => isset($stats[$gid_prefix.strtolower($locations[$i][$field_name])][$type.'_cnt']) ? $stats[$gid_prefix.strtolower($locations[$i][$field_name])][$type.'_cnt'] : 0,
						), 
						'empty' => false, 
						'group'=> false,
					);
				}else{
					$listings_regions[] = array('name'=>'&nbsp;', 'empty' => true, 'group'=> false);
				}
		}
	
		$this->CI->template_lite->assign('listings_regions', $listings_regions);
		
		$regions_count = count($listings_regions);
		$this->CI->template_lite->assign('listings_regions_count', $regions_count);

		$columns = !empty($params['columns'])?intval($params['columns']):3;
		$this->CI->template_lite->assign('columns', $columns);

		$listings_per_column = ceil($regions_count/$columns);
		$this->CI->template_lite->assign('listings_per_column', $listings_per_column);

		$this->CI->template_lite->assign('type', $type);

		return $this->CI->template_lite->fetch('helper_regions_search', 'user', 'listings');
	}
	
	/**
	 * Install reviews fields
	 * @param array $fields fields for create
	 */
	public function install_reviews_fields($fields=array()){		
		if(empty($fields)) return;
		$this->CI->load->dbforge();
		$table_fields = $this->CI->db->get(LISTINGS_TABLE)->list_fields();
		foreach((array)$fields as $field_name=>$field_data){
			if(!in_array($field_name, $table_fields)){
				$this->CI->dbforge->add_column(LISTINGS_TABLE, array($field_name=>$field_data));
			}
		}
	}
	
	/**
	 * Uninstall reviews fields
	 * @param array $fields fields for drop
	 */
	public function deinstall_reviews_fields($fields=array()){
		if(empty($fields)) return;
		$this->CI->load->dbforge();
		$table_fields = $this->db->get(LISTINGS_TABLE)->list_fields();
		foreach($fields as $field_name){
			if(in_array($field_name, $table_fields)){
				$this->dbforge->drop_column(LISTINGS_TABLE, $field_name);
			}
		}
	}
	
	/**
	 * Generate qr code
	 * @param array $listing listing data
	 */
	public function get_qr_code($listing){
		//print_r($listing); exit;
		$this->CI->load->helper('seo');
		$this->CI->load->library('Translit');
	
		$current_lang = $this->CI->pg_language->get_lang_by_id($this->CI->pg_language->current_lang_id);
		$current_lang_code = $current_lang['code'];
				
		$link = rewrite_link('listings', 'view', $listing);
		$theme = $this->CI->pg_theme->get_active_settings();
		
		$site_color_hex = $theme['user']['scheme_data']['color_settings']['main_bg'];
		$site_color_bg_hex = $theme['user']['scheme_data']['color_settings']['html_bg'];
		
		$site_color_val = hexdec($site_color_hex);
		$site_color_bg_val = hexdec($site_color_bg_hex);
		
		$site_color_rgb = (0xFF & ($site_color_val >> 0x10)).','.(0xFF & ($site_color_val >> 0x8)).','.(0xFF & $site_color_val);
		$site_color_bg_rgb = (0xFF & ($site_color_bg_val >> 0x10)).','.(0xFF & ($site_color_bg_val >> 0x8)).','.(0xFF & $site_color_bg_val);
	
		$filename = $listing['id'].'_'.$site_color_hex.'.jpg';
		
		$this->CI->load->library('QRcode');
	
		$filepath = FRONTEND_PATH.'qr/'.$site_color_hex.'/'.$filename;

		if(!file_exists($filepath)){
			if(!file_exists(FRONTEND_PATH.'qr/'.$site_color_hex.'/')){
				mkdir(FRONTEND_PATH.'qr/'.$site_color_hex.'/');
			}
			QRcode::jpg($link, $filepath, 'H', 2, 2, 85, $site_color_rgb, $site_color_bg_rgb);
		}elseif(@filemtime($filepath) < time() - $this->qr_update_time*60*60){
			QRcode::jpg($link, $filepath, 'H', 2, 2, 85, $site_color_rgb, $site_color_bg_rgb);
		}
		
		$url = FRONTEND_URL.'qr/'.$site_color_hex.'/'.$filename;
		return $url;
	}
	
	/**
	 * Callback for spam module
	 * @param string $action action name
	 * @param mix $data listing identifiers
	 * @return string
	 */
	public function spam_callback($action, $data){
		switch($action){
			case 'ban':
				$this->save_listing((int)$data, array('banned'=>1));
				return 'banned';
				
				//$this->delete_listing((int)$data);
				//return 'removed';
			break;
			case 'unban':
			$this->save_listing((int)$data, array('banned'=>0));
				return 'unbanned';
			break;
			case 'delete':
				$this->delete_listing((int)$data);
				return 'removed';
			break;
			case 'get_content':
				if(empty($data)) return array();
				$listings = $this->get_listings_list(array('ids'=>(array)$data));
				
				$this->CI->load->helper('listings');
					
				$return = array();
				foreach($listings as $listing){
					$return[$listing['id']] = $listing['output_name'].'<br>'.
						$listing['property_type_str'].' '.
						$listing['operation_type_str'].'<br>'.
						listing_price_block(array('data'=>$listing, 'template'=>'small'));
				}
				return $return;
			break;
			case 'get_link':
				if(empty($data)) return array();
				$listings = $this->get_listings_list(array('ids'=>(array)$data));
				$return = array();
				foreach($listings as $listing){
					$return[$listing['id']] = site_url().'admin/listings/edit/'.$listing['id'];
				}
				return $return;
			break;
			case 'get_object':
				if(empty($data)) return array();				
				$listings = $this->get_listings_list(array('ids'=>(array)$data));
				$return = array();
				foreach($listings as $listing){
					$return[$listing['id']] = $listing;
				}
				return $return;
			break;
		}
	}
	
	/**
	 * Callback for reviews module
	 * @param string $action action name
	 * @param array $data review data
	 * @return string
	 */
	public function callback_reviews($action, $data){
		switch($action){
			case 'update':
				$listing_data['review_type'] = $data['type_gid'];
				$listing_data['review_sorter'] = $data['review_sorter'];
				$listing_data['review_count'] = $data['review_count'];
				$listing_data['review_data'] = serialize($data['review_data']);
				$this->save_listing($data['id_object'], $listing_data);
			break;
			case 'get_object':
				if(empty($data)) return array();				
				$listings = $this->get_listings_list(array('ids'=>(array)$data));
				$return = array();
				foreach($listings as $listing){
					$return[$listing['id']] = $listing;
				}
				return $return;
			break;
		}
	}
	
	/**
	  * Upload video callback
	  * @param integer $object_id object identifier
	  * @param string $status upload status
	  * @param array $data upload data
	  * @param array $errors upload errors
	  */
	public function video_callback($object_id, $status, $data, $errors){
		$this->set_format_settings('get_video', true);
		$listing_data = $this->get_listing_by_id($object_id);
		$this->set_format_settings('get_video', false);

		$this->CI->load->model('listings/models/Listings_moderation_model');
		$already_moderated = $this->CI->Listings_moderation_model->is_listing_moderated($object_id);
		if($already_moderated){
			$fields_for_select = $this->CI->Listings_moderation_model->get_fields_for_moderate();
			$this->CI->Listings_moderation_model->set_dop_fields($fields_for_select);
			$moderated_data = $this->CI->Listings_moderation_model->get_listing_by_id($object_id);
			$listing_data = array_merge($listing_data, $moderated_data);
		}

		if(isset($data['video'])) $update['listing_video'] = $data['video'];
		if(isset($data['image'])) $update['listing_video_image'] = $data['image'];
		$update['listing_video_data'] = $listing_data['listing_video_data'];
		if($status == 'start' || !isset($update['listing_video_data']['data'])) 
			$update['listing_video_data']['data'] = array();
		if(!empty($data)){
			$update['listing_video_data']['data'] = array_merge($update['listing_video_data']['data'], $data);
		}

		$update['listing_video_data']['status'] = $status;
		$update['listing_video_data']['errors'] = $errors;
		
		$validate_data = $this->validate_listing($object_id, $update);
		$this->save_listing($object_id, $validate_data['data'], $already_moderated);
		return;
	}
	
	/**
	  * Upload photo callback
	  * @param string $action action name
	  * @param integer $object_id object identifier
	  * @param array $params photo parameters
	  */
	public function photo_callback($action, $object_id, $params=false){
		switch($action){
			case 'moder_set_status':
				if(!$params) break;
				$this->CI->load->model('Upload_gallery_model');
				$file = $this->Upload_gallery_model->get_file_by_id($object_id);
				$this->_update_logo_image($file['object_id']);
			break;
			case 'moder_delete':
				if(!$params) break;
				$this->_update_logo_image($object_id);
			break;
			case 'recrop':
				if($params['settings']['use_for_search']){
					$this->CI->load->model('Upload_gallery_model');
					$file = $this->Upload_gallery_model->get_file_by_id($object_id);
					$this->_update_logo_image($file['object_id']);
				}
			break;
			case 'rotate':
				if($params['settings']['use_for_search']){
					$this->CI->load->model('Upload_gallery_model');
					$file = $this->Upload_gallery_model->get_file_by_id($object_id);
					$this->_update_logo_image($file['object_id']);
				}
			break;
		}
	}
	
	/**
	  * Upload panorama callback
	  * @param string $action action name
	  * @param integer $object_id object identifier
	  * @param integer $status modreration status
	  */
	public function vtour_callback($action, $object_id, $status=false){
		$this->CI->load->model('Upload_gallery_model');
		switch($action){
			case 'moder_set_status':
				if(!$params) break;
				$this->CI->load->model('Upload_gallery_model');
				$file = $this->Upload_gallery_model->get_file_by_id($object_id);
				$this->_update_vtour($file['object_id']);
			break;
			case 'moder_delete':
				if(!$params) break;
				$vtour_type = $this->CI->Upload_gallery_model->get_type_by_gid($this->virtual_tour_type);
				$params = array();
				$params['where']['type_id'] = $vtour_type['id'];
				$params['where']['object_id'] = $object_id;
				$params['where']['status'] = 1;
				$count = $this->CI->Upload_gallery_model->get_files_count_by_param($params);
				$this->_update_vtour($object_id, $count ? true : false);
			break;
		}
	}
	
	/**
	 * Update virtual tour availability
	 * @param integer $listing_id listing identifier
	 * @param boolean $is_vtour virtual tour availability
	 */
	public function _update_vtour($listing_id, $is_vtour=true){
		$post_data = array('is_vtour'=>$is_vtour);
		$validate_data = $this->validate_listing($listing_id, $post_data);
		if(empty($validate_data['errors'])) $this->save_listing($listing_id, $validate_data['data']);
	}
	
	/**
	 * Update listing image
	 * @param integer $listing_id listing identifier
	 */
	public function _update_logo_image($listing_id){
		$this->CI->load->model('Uploads_model');
		$upload_config = $this->CI->Uploads_model->get_config($this->upload_config_id);
	
		$this->set_format_settings(array('get_user'=>false, 'get_location'=>false));
		$listing = $this->get_listing_by_id($listing_id);
		$this->set_format_settings(array('get_user'=>true, 'get_location'=>true));
		
		$this->CI->load->model('Upload_gallery_model');
		$gallery_type = $this->CI->Upload_gallery_model->get_type_by_gid($this->gallery_type);
		$gallery_config = $this->CI->Uploads_model->get_config($gallery_type['gid_upload_config']);
		
		$params = array();
		$params['where']['type_id'] = $gallery_type['id'];
		$params['where']['object_id'] = $listing['id'];
		$params['where']['status'] = 1;
		$photos = $this->CI->Upload_gallery_model->get_files_by_param($params);
		
		$logo_width = 0;
		$logo_height = 0;
		foreach($upload_config['thumbs'] as $thumb_config){
			$logo_width = max($logo_width, $thumb_config['width']);
			$logo_height = max($logo_height, $thumb_config['height']);
			
		}
		
		$logo_thumb_prefix = '';
		$logo_thumb_width = 0;
		$logo_thumb_height = 0;
		foreach($gallery_config['thumbs'] as $thumb_config){
			if($thumb_config['width'] >= $logo_width && $thumb_config['height'] >= $logo_height &&
				$thumb_config['width']/$logo_width == $thumb_config['height']/$logo_height &&
				(!$logo_thumb_width || $thumb_config['width'] < $logo_thumb_width) && 
				(!$logo_thumb_height || $thumb_config['width'] < $logo_thumb_height)){
				$logo_thumb_prefix = $thumb_config['prefix'];
			}
		}	

		$post_data = array('photo_count'=>count($photos));
		$validate_data = array();

		// check used as logo
		foreach($photos as $i=>$photo){
			if($photo['status'] != 1) continue;
			$file_path = $logo_thumb_prefix ? $photo['media']['thumbs'][$logo_thumb_prefix] : $photo['media']['file_path'];
		
			$img_return = $this->CI->Uploads_model->upload_exist($this->upload_config_id, $listing_id.'/', $file_path);
			if(!empty($img_return['errors'])){
				$validate_data['errors'] = array_merge($validate_data['errors'], $img_return['errors']);
			}else{
				$post_data['logo_image'] = $img_return['file'];
				$photos[$i]['settings']['use_for_search'] = 1;
				$validate_data_photo = $this->CI->Upload_gallery_model->validate_file_data($photo['id'], $photo['type_id'], $photos[$i]);
				$this->CI->Upload_gallery_model->save_local_file($photo['id'], $validate_data_photo['data'], '', false);
				
				$slider_min_width = $this->CI->pg_module->get_module_config('listings', 'slider_min_width');
				$slider_min_height = $this->CI->pg_module->get_module_config('listings', 'slider_min_height');
				
				$image_size = @getimagesize($photo['media']['file_path']);
				if($image_size[0] >= $slider_min_width && $image_size[1] >= $slider_min_height){
					$img_return = $this->CI->Uploads_model->upload_exist($this->slider_config_id, $listing_id.'/', $photo['media']['file_path']);
				}else{
					$img_return['errors'] = true;
				}
				if(!empty($img_return['errors'])){
					$post_data['slider_image'] = '';
				}else{
					$post_data['slider_image'] = $img_return['file'];
				}
				
			}							
			break;
		}	
			
		if(!isset($post_data['logo_image'])){
			$post_data['logo_image'] = '';
			$post_data['slider_image'] = '';
		}
		if($listing['logo_image'] && $listing['logo_image'] != $post_data['logo_image']){
			$this->CI->Uploads_model->delete_upload($this->upload_config_id, $listing_id.'/', $listing['logo_image']);
			if($listing['slider_image'] && $listing['slider_image'] != $post_data['slider_image']){
				$this->CI->Uploads_model->delete_upload($this->slider_config_id, $listing_id.'/', $listing['slider_image']);
			}
		}

		// check used for slide show
		$sources = array();
		if(isset($post_data['logo_image'])) $listing['logo_image'] = $post_data['logo_image'];
		foreach($photos as $photo){
			if($photo['status'] != 1) continue;
			$file_path = $logo_thumb_prefix ? $photo['media']['thumbs'][$logo_thumb_prefix] : $photo['media']['file_path'];
			$sources[] = $file_path;
		}
		if(!empty($sources)){
			$slide_show_images = $this->CI->pg_module->get_module_config('listings', 'slide_show_images');
			$sources = array_slice($sources, 0, $slide_show_images);
			$img_return = $this->CI->Uploads_model->upload_anim($this->upload_config_id, $listing_id.'/', $listing['logo_image'], $sources);
			if(!isset($validate_data)) $validate_data = array();
			if(!empty($img_return['errors'])){
				$validate_data['errors'] = array_merge($validate_data['errors'], $img_return['errors']);
			}
		}

		if(!empty($post_data)){		
			if(!isset($validate_data)) $validate_data = array();
			$data = $this->validate_listing($listing_id, $post_data);
			$validate_data = array_merge_recursive($validate_data, $data);
			if(empty($validate_data['errors'])){
				$this->save_listing($listing_id, $validate_data['data']);
			}
		}		
	
		// update wish lists
		if(!empty($listing['id_wish_lists'])){
			$this->CI->load->model('listings/models/Wish_list_model');
			foreach($listing['id_wish_lists'] as $wish_list_id){
				if($listing['logo_image']){
					$this->CI->Wish_list_model->change_logo($wish_list_id);
				}else{
					$this->CI->Wish_list_model->delete_from_wish_list($wish_list_id, $listing_id);
				}
			}				
		}
	}
	
	/**
	 * Availables banners places (callback method)
	 * @return array
	 */
	public function _banner_available_pages(){
		$return[] = array('link' => 'listings/index', 'name' => l('header_listings', 'listings'));
		$return[] = array('link' => 'listings/search', 'name' => l('header_search_listings', 'listings'));
		$return[] = array('link' => 'listings/user', 'name' => l('header_user_listings', 'listings'));
		$return[] = array('link' => 'listings/view', 'name' => l('header_view_listing', 'listings'));
		return $return;
	}
	
	/**
	 * Return rss settings
	 */
	public function get_rss_settings(){
		$data = array(
			'rss_feed_channel_title' => $this->CI->pg_module->get_module_config('listings', 'rss_feed_channel_title'),
			'rss_feed_channel_description' => $this->CI->pg_module->get_module_config('listings', 'rss_feed_channel_description'),
			'rss_feed_image_url' => $this->CI->pg_module->get_module_config('listings', 'rss_feed_image_url'),
			'rss_feed_image_title' => $this->CI->pg_module->get_module_config('listings', 'rss_feed_image_title'),
			'rss_listings_max_count' => $this->CI->pg_module->get_module_config('listings', 'rss_news_max_count'),
		);

		if($data['rss_feed_image_url']){
			$this->CI->load->model('Uploads_model');
			$data['rss_feed_image_media'] = $this->CI->Uploads_model->format_upload($this->rss_config_id, '', $data['rss_feed_image_url']);
		}

		return $data;
	}
	
	/**
	 * Set photo sorter
	 * @param integer $listing_id listing identifier
	 * @parma array $data photo sorter
	 */
	public function set_photo_sorter($listing_id, $data){
		$this->CI->load->model('Upload_gallery_model');
		foreach($data as $photo_id=>$sort_index){
			$this->CI->Upload_gallery_model->save_sorter($photo_id, $sort_index);
		}
		$this->_update_logo_image($listing_id);
	}
	
	/**
	 * Set photo sorter
	 * @param integer $listing_id listing identifier
	 * @parma array $data panorama sorter
	 */
	public function set_panorama_sorter($listing_id, $data){
		$this->CI->load->model('Upload_gallery_model');
		foreach($data as $photo_id=>$sort_index){
			$this->CI->Upload_gallery_model->save_sorter($photo_id, $sort_index);
		}
	}
	
	/**
	 * Display listing as Sold
	 * @param integer $listing_id listing identifier
	 * @param integer $status sold status
	 */
	public function make_sold($listing_id, $status){
		$data['sold'] = $status ? 1 : 0;
		$this->save_listing($listing_id, $data);
	}
	
	/**
	 * Validate share form 
	 * @param integer $listing_id listing identifier
	 * @param array $data data for validate
	 * @return array
	 */
	public function validate_share($listing_id, $data){
		$return = array('errors'=>array(), 'data'=>array());
		
		// email
		if(isset($data['email'])){
			$return['data']['email'] = trim(strip_tags($data['email']));
			if(empty($return['data']['email'])){
				$return['errors'][] = l('error_empty_email', 'listings');
			}else{
				$this->CI->config->load('reg_exps', TRUE);
				$email_expr = $this->CI->config->item('email', 'reg_exps');
				if(!preg_match($email_expr, $return['data']['email'])){
					$return['errors'][] = l('error_email_incorrect', 'listings');
				}
			}
		}elseif(!$listing_id){
			$return['errors'][] = l('error_empty_email', 'listings');
		}

		// user
		if(isset($data['user'])){
			if(!empty($data['user'])) $data['user'] = trim(strip_tags($data['user']));
			if(!empty($data['user'])){
				$return['data']['user'] = $data['user'];
				$this->CI->load->model('moderation/models/Moderation_badwords_model');
				$bw_count = $this->CI->Moderation_badwords_model->check_badwords($this->moderation_share_type, $return['data']['user']);
				if($bw_count) $return['errors'][] = l('error_badwords_user', 'listings');
			}else{
				$return['errors'][] = l('error_empty_user', 'listings');
			}
		}elseif(!$listing_id){
			$return['errors'][] = l('error_empty_user', 'listings');
		}
		
		// message
		if(isset($data['message'])){
			if(!empty($data['message'])) $data['message'] = trim(strip_tags($data['message']));
			if(!empty($data['message'])){
				$return['data']['message'] = $data['message'];
				$this->CI->load->model('moderation/models/Moderation_badwords_model');
				$bw_count = $this->CI->Moderation_badwords_model->check_badwords($this->moderation_share_type, $return['data']['message']);
				if($bw_count) $return['errors'][] = l('error_badwords_message', 'listings');
			}else{
				$return['errors'][] = l('error_empty_message', 'listings');
			}
		}elseif(!$listing_id){
			$return['errors'][] = l('error_empty_message', 'listings');
		}
		
		return $return;
	}
	
	
	/**
	 * Recalculate min/max price
	 * @param integer $listing_id listing identifier
	 * @param string $operation_type operation type
	 */
	public function get_min_max_price($listing_id, $operation_type){
		$return = array('min'=>0, 'max'=>0);
		$this->DB->select('MIN(CASE WHEN price_reduced>0 THEN price_reduced ELSE price END) AS min, MAX(CASE WHEN price_max>0 THEN price_max ELSE price END) AS max')
			 ->from(LISTINGS_TABLE)
			 ->where('status', 1)
			 ->where('price_negotiated', 0)
			 ->where('id !=', $listing_id);
		
		if($operation_type == 'sold'){
			$operation_type = 'sale';
			$this->DB->where('sold', 1);
		}else{
			$this->DB->where('sold', 0);
		}
		
		$this->DB->where('id_type', $this->get_operation_type_by_gid($operation_type));
		
		$results = $this->DB->get()->result_array();
		$return = $results[0];
		return $return;
	}
	
	/**
	 * Return listings price range by type
	 * @param string $type operation type
	 */
	public function get_price_range($type=null){		
		$price_range = array();
		$operation_types = $this->get_operation_types(true);
		if($type && in_array($type, $operation_types)) $operation_types = array($type);
		foreach($operation_types as $type){
			if($type == 'all'){
				$min_price = 0;
				$max_price = 0;
				foreach($this->operations_arr as $v){
					$min_p = (float)$this->CI->pg_module->get_module_config('listings', 'price_min_'.$v);
					$max_p = (float)$this->CI->pg_module->get_module_config('listings', 'price_max_'.$v);
					
					if($min_price){
						$min_price = min($min_price, $min_p);
					}else{
						$min_price = $min_p;
					}
					
					if($max_price){
						$max_price = max($max_price, $max_p);
					}else{
						$max_price = $max_p;
					}
				}
			}else{
				$min_price = (float)$this->CI->pg_module->get_module_config('listings', 'price_min_'.$type);
				$max_price = (float)$this->CI->pg_module->get_module_config('listings', 'price_max_'.$type);
			}
			$step_val = max(ceil(($max_price-$min_price)/$this->price_range_steps_default), 1);
			$range = array();
			if($min_price) $range[] = 0;
			for($i=$min_price; $i<$max_price; $i+=$step_val){
				$range[] = $i;
			}
			$range[] = $max_price;
			$price_range[$type] = array(
				'min_price' => 0,
				'max_price' => $max_price,
				'data' => $range,
			);
		}
		return $price_range;
	}
	
	/**
	 * Update statistics
	 * @param array $listing current listing data
	 * @param array $data new listing data
	 */
	public function update_module_statistics($listing, $data=null){
		if(empty($data)){
			$data['status'] = 0;
		}elseif(!isset($data['status'])){
			$data['status'] = $listing['status'];
		}
		if(empty($listing['status']) && empty($data['status'])) return;
			
		$no_price_update = empty($listing['status']) && !empty($data['price_negotiated']) ||
						 ! empty($listing['price_negotiated']) && empty($data['status']) || 
						 ! empty($listing['price_negotiated']) && !empty($data['price_negotiated']);
		if(!($no_price_update)){
			$price_min = 0;
			$price_max = 0;
			
			$operation_type_new = null;
			$operation_type_old = null;
		
			if($listing){
				$operation_type_old = $this->get_operation_type_by_id($listing['id_type']);
				if(!$operation_type_new) $operation_type_new = $operation_type_old;
				$price_min = (($operation_type_old == 'sale' || $operation_type_old == 'rent') && $listing['price_reduced']) ? $listing['price_reduced'] : $listing['price'];
				$price_max = (($operation_type_old == 'buy' || $operation_type_old == 'lease') && $listing['price_max']) ? $listing['price_max'] : $price_min;
				if($listing['sold']) $operation_type_old = 'sold';
			}
			
			if(isset($data['id_type'])){
				$type_new_id = $data['id_type'];
			}elseif($listing['id_type']){
				$type_new_id = $listing['id_type'];
			}
			
			if(isset($data['sold'])){
				$sold_new = $data['sold'];
			}elseif($listing['sold']){
				$sold_new = $listing['sold'];
			}
			
			if($sold_new){
				$operation_type_new = 'sold';
			}elseif($type_new_id){
				$operation_types = $this->get_operation_types();
				$operation_type_new = $operation_types[$type_new_id];
			}else{
				$operation_type_new = $operation_type_old;
			}
			
			if(!$listing['status'] || $listing['price_negotiated']){
				 $operation_type_old = null;
			}
			
			if($operation_type_old){		
				$min_price_old = $this->CI->pg_module->get_module_config('listings', 'price_min_'.$operation_type_old);	
				$max_price_old = $this->CI->pg_module->get_module_config('listings', 'price_max_'.$operation_type_old);	
				if($price_min > 0 && $price_min == $min_price_old || $price_max > $this->max_price_default && $price_max == $max_price_old){
					$price_range = $this->get_min_max_price($listing['id'], $operation_type_old);
					$price_min_old = $price_range['min'];
					$price_max_old = max($price_range['max'], $this->price_max_default);	
					$this->CI->pg_module->set_module_config('listings', 'price_min_'.$operation_type_old, (float)$price_min_old);	
					$this->CI->pg_module->set_module_config('listings', 'price_max_'.$operation_type_old, (float)$price_max_old);	
					if($operation_type_new == $operation_type_old){
						$price_min = $price_min_old;
						$price_max = $price_max_old;
					}
				}
			}
		
			if(isset($data['status']) && !$data['status'] ||
			   isset($data['price_negotiated']) && $data['price_negotiated'] ||
			   !isset($data['price_negotiated']) && $listing['price_negotiated']) $operation_type_new = null;

			if($operation_type_new){
				$min_price_new = $this->CI->pg_module->get_module_config('listings', 'price_min_'.$operation_type_new);
				$max_price_new = $this->CI->pg_module->get_module_config('listings', 'price_max_'.$operation_type_new);
		
				if ($operation_type_new == 'buy' || $operation_type_new == 'lease'){
					if(isset($data['price_max'])){
						$price_max = $data['price_max'];
					}
					if(isset($data['price'])){
						$price_min = $data['price'];
					}
				}else{
					if(isset($data['price'])){
						$price_max = $data['price'];
						$price_min = $data['price'];
					}
					
					if(isset($data['price_reduced']) && !empty($data['price_reduced'])){
						$price_max = $data['price_reduced'];
						$price_min = $data['price_reduced'];
					}
				}
		
				if($price_min > 0 && (!$min_price_new || $price_min < $min_price_new)){
					$this->CI->pg_module->set_module_config('listings', 'price_min_'.$operation_type_new, (float)$price_min);	
				}
				if($price_max > 0 && ($price_max > $max_price_new)){
					$price_max = max($price_max, $this->price_max_default);
					$this->CI->pg_module->set_module_config('listings', 'price_max_'.$operation_type_new, (float)$price_max);	
				}
			}
		}
		
		if($listing['sold']) $listing['status'] = 0;
		
		if(isset($data['sold']) && $data['sold'] || !isset($data['sold']) && $listing['sold'])
			$data['status'] = 0;

		//listings by categories & locations
	 	if(isset($data['status']) && $listing['status'] && !$data['status']){
			$this->CI->load->model('listings/models/Listings_stat_model');
			
			$stat_gid = 'property_type_'.$listing['id_category'];
			$stat = $this->CI->Listings_stat_model->get_stat_by_id($stat_gid);
			if($stat){
				$type_name = $this->get_operation_type_by_id($listing['id_type']).'_cnt';
				$save_data = array($type_name=>max($stat[$type_name]-1, 0));
				$validate_data = $this->CI->Listings_stat_model->validate_stat($stat['id'], $save_data);
				$this->CI->Listings_stat_model->save_stat($stat['id'], $validate_data['data']);
			}
			
			$stat_gid = 'property_type_'.$listing['id_category'].'_'.$listing['property_type'];
			$stat = $this->CI->Listings_stat_model->get_stat_by_id($stat_gid);
			if($stat){
				$type_name = $this->get_operation_type_by_id($listing['id_type']).'_cnt';
				$save_data = array($type_name=>max($stat[$type_name]-1, 0));
				$validate_data = $this->CI->Listings_stat_model->validate_stat($stat['id'], $save_data);
				$this->CI->Listings_stat_model->save_stat($stat['id'], $validate_data['data']);
			}
				
			$stat_gid = 'country_'.$listing['id_country'];
			$stat = $this->CI->Listings_stat_model->get_stat_by_id($stat_gid);
			if($stat){
				$type_name = $this->get_operation_type_by_id($listing['id_type']).'_cnt';
				$save_data = array($type_name=>max($stat[$type_name]-1, 0)); 
				$validate_data = $this->CI->Listings_stat_model->validate_stat($stat['id'], $save_data);
				$this->CI->Listings_stat_model->save_stat($stat['id'], $validate_data['data']);
			}
				
			$stat_gid = 'region_'.$listing['id_region'];
			$stat = $this->CI->Listings_stat_model->get_stat_by_id($stat_gid);
			if($stat){
				$type_name = $this->get_operation_type_by_id($listing['id_type']).'_cnt';
				$save_data = array($type_name=>max($stat[$type_name]-1, 0));
				$validate_data = $this->CI->Listings_stat_model->validate_stat($stat['id'], $save_data);
				$this->CI->Listings_stat_model->save_stat($stat['id'], $validate_data['data']);
			}
			
			$stat_gid = 'city_'.$listing['id_city'];
			$stat = $this->CI->Listings_stat_model->get_stat_by_id($stat_gid);
			if($stat){
				$type_name = $this->get_operation_type_by_id($listing['id_type']).'_cnt';
				$save_data = array($type_name=>max($stat[$type_name]-1), 0); 
				$validate_data = $this->CI->Listings_stat_model->validate_stat($stat['id'], $save_data);
				$this->CI->Listings_stat_model->save_stat($stat['id'], $validate_data['data']);
			}
			
			$this->CI->load->model('Users_model');
			
			$user = $this->CI->Users_model->get_user_by_id($listing['id_user']);
			if($user){
				$this->CI->Users_model->update_listings_stat($user['id'], $this->get_operation_type_by_id($listing['id_type']), false);
			}
		}elseif(isset($data['status']) && !$listing['status'] && $data['status']){
			$this->CI->load->model('listings/models/Listings_stat_model');
			
			$type_id = isset($data['id_type']) ? $data['id_type'] : $listing['id_type'];
			
			if(isset($data['id_category']) || isset($listing['id_category'])){
				$type_name = $this->get_operation_type_by_id($type_id).'_cnt';
				$stat_gid = 'property_type_'.(isset($data['id_category']) ? $data['id_category'] : $listing['id_category']);
				$stat = $this->CI->Listings_stat_model->get_stat_by_id($stat_gid);
				if($stat){
					$save_data = array($type_name=>$stat[$type_name]+1); 
					$validate_data = $this->CI->Listings_stat_model->validate_stat($stat['id'], $save_data);
					$this->CI->Listings_stat_model->save_stat($stat['id'], $validate_data['data']);
				}else{
					$save_data = array('gid'=>$stat_gid, $type_name=>1);
					$validate_data = $this->CI->Listings_stat_model->validate_stat(null, $save_data);
					$this->CI->Listings_stat_model->save_stat(null, $validate_data['data']);
				}
			}
			
			if((isset($data['id_category']) || isset($listing['id_category'])) && 
			   (isset($data['property_type']) || isset($listing['property_type']))){
				$type_name = $this->get_operation_type_by_id($type_id).'_cnt';
				$stat_gid = 'property_type_'.(isset($data['id_category']) ? $data['id_category'] : $listing['id_category']).'_'.(isset($data['property_type']) ? $data['property_type'] : $listing['property_type']);
				$stat = $this->CI->Listings_stat_model->get_stat_by_id($stat_gid);
				if($stat){
					$save_data = array($type_name=>$stat[$type_name]+1); 
					$validate_data = $this->CI->Listings_stat_model->validate_stat($stat['id'], $save_data);
					$this->CI->Listings_stat_model->save_stat($stat['id'], $validate_data['data']);
				}else{
					$save_data = array('gid'=>$stat_gid, $type_name=>1);
					$validate_data = $this->CI->Listings_stat_model->validate_stat(null, $save_data);
					$this->CI->Listings_stat_model->save_stat(null, $validate_data['data']);
				}
			}
			
			if(isset($data['id_country']) || isset($listing['id_country'])){
				$type_name = $this->get_operation_type_by_id($type_id).'_cnt';
				$stat_gid = 'country_'.(isset($data['id_country']) ? $data['id_country'] : $listing['id_country']);
				$stat = $this->CI->Listings_stat_model->get_stat_by_id($stat_gid);
				if($stat){
					$save_data = array($type_name=>$stat[$type_name]+1); 
					$validate_data = $this->CI->Listings_stat_model->validate_stat($stat['id'], $save_data);
					$this->CI->Listings_stat_model->save_stat($stat['id'], $validate_data['data']);
				}else{
					$save_data = array('gid'=>$stat_gid, $type_name=>1);
					$validate_data = $this->CI->Listings_stat_model->validate_stat(null, $save_data);
					$this->CI->Listings_stat_model->save_stat(null, $validate_data['data']);
				}
			}
				
			if(isset($data['id_region']) || isset($listing['id_region'])){
				$type_name = $this->get_operation_type_by_id($type_id).'_cnt';
				$stat_gid = 'region_'.(isset($data['id_region']) ? $data['id_region'] : $listing['id_region']);
				$stat = $this->CI->Listings_stat_model->get_stat_by_id($stat_gid);
				if($stat){
					$save_data = array($type_name=>$stat[$type_name]+1); 
					$validate_data = $this->CI->Listings_stat_model->validate_stat($stat['id'], $save_data);
					$this->CI->Listings_stat_model->save_stat($stat['id'], $validate_data['data']);
				}else{
					$save_data = array('gid'=>$stat_gid, $type_name=>1);
					$validate_data = $this->CI->Listings_stat_model->validate_stat(null, $save_data);
					$this->CI->Listings_stat_model->save_stat(null, $validate_data['data']);
				}
			}
				
			if(isset($data['id_city']) && isset($listing['id_city'])){
				$type_name = $this->get_operation_type_by_id($type_id).'_cnt';
				$stat_gid = 'city_'.(isset($data['id_city']) ? $data['id_city'] : $listing['id_city']);
				$stat = $this->CI->Listings_stat_model->get_stat_by_id($stat_gid);
				if($stat){
					$save_data = array($type_name=>$stat[$type_name]+1); 
					$validate_data = $this->CI->Listings_stat_model->validate_stat($stat['id'], $save_data);
					$this->CI->Listings_stat_model->save_stat($stat['id'], $validate_data['data']);
				}else{
					$save_data = array('gid'=>$stat_gid, $type_name=>1);
					$validate_data = $this->CI->Listings_stat_model->validate_stat(null, $save_data);
					$this->CI->Listings_stat_model->save_stat(null, $validate_data['data']);
				}
			}
		
			if(isset($data['id_user']) || isset($listing['id_user'])){
				$this->CI->load->model('Users_model');			
				$user = $this->CI->Users_model->get_user_by_id((isset($data['id_user']) ? $data['id_user'] : $listing['id_user']));
				if($user){
					$this->CI->Users_model->update_listings_stat($user['id'], $this->get_operation_type_by_id($type_id));
				}
			}				
		}elseif((!isset($data['status']) && $listing['status']) || (isset($data['status']) && $data['status'])){
			$this->CI->load->model('listings/models/Listings_stat_model');
			
			$type_id = isset($data['id_type']) ? $data['id_type'] : $listing['id_type'];
					
			if(($listing['id_type'] != $type_id) || (isset($data['id_category']) && $listing['id_category'] != $data['id_category'])){
				$stat_gid = 'property_type_'.$listing['id_category'];
				$stat = $this->CI->Listings_stat_model->get_stat_by_id($stat_gid);
				if($stat){
					$type_name = $this->get_operation_type_by_id($listing['id_type']).'_cnt';
					$save_data = array($type_name=>$stat[$type_name] ? $stat[$type_name]-1 : 0); 
					$validate_data = $this->CI->Listings_stat_model->validate_stat($stat['id'], $save_data);
					$this->CI->Listings_stat_model->save_stat($stat['id'], $validate_data['data']);
				}
					
				$type_name = $this->get_operation_type_by_id($type_id).'_cnt';
				$stat_gid = 'property_type_'.(isset($data['id_category']) ? $data['id_category'] : $listing['id_category']);
				$stat = $this->CI->Listings_stat_model->get_stat_by_id($stat_gid);
				if($stat){						
					$save_data = array($type_name=>$stat[$type_name]+1); 
					$validate_data = $this->CI->Listings_stat_model->validate_stat($stat['id'], $save_data);
					$this->CI->Listings_stat_model->save_stat($stat['id'], $validate_data['data']);
				}else{
					$save_data = array('gid'=>$stat_gid, $type_name=>1);
					$validate_data = $this->CI->Listings_stat_model->validate_stat(null, $save_data);
					$this->CI->Listings_stat_model->save_stat(null, $validate_data['data']);
				}
			}
					
			if(($listing['id_type'] != $type_id) || (isset($data['property_type']) && $listing['property_type'] != $data['property_type'])){
				$stat_gid = 'property_type_'.$listing['id_category'].'_'.$listing['property_type'];
				$stat = $this->CI->Listings_stat_model->get_stat_by_id($stat_gid);
				if($stat){
					$type_name = $this->get_operation_type_by_id($listing['id_type']).'_cnt';
					$save_data = array($type_name=>$stat[$type_name] ? $stat[$type_name]-1 : 0); 
					$validate_data = $this->CI->Listings_stat_model->validate_stat($stat['id'], $save_data);
					$this->CI->Listings_stat_model->save_stat($stat['id'], $validate_data['data']);
				}
					
				$type_name = $this->get_operation_type_by_id($type_id).'_cnt';
				$stat_gid = 'property_type_'.(isset($data['id_category']) ? $data['id_category'] : $listing['id_category']).'_'.(isset($data['property_type']) ? $data['property_type'] : $listing['property_type']);
				$stat = $this->CI->Listings_stat_model->get_stat_by_id($stat_gid);
				if($stat){						
					$save_data = array($type_name=>$stat[$type_name]+1); 
					$validate_data = $this->CI->Listings_stat_model->validate_stat($stat['id'], $save_data);
					$this->CI->Listings_stat_model->save_stat($stat['id'], $validate_data['data']);
				}else{
					$save_data = array('gid'=>$stat_gid, $type_name=>1);
					$validate_data = $this->CI->Listings_stat_model->validate_stat(null, $save_data);
					$this->CI->Listings_stat_model->save_stat(null, $validate_data['data']);
				}
			}
				
			if(($listing['id_type'] != $type_id) || (isset($data['id_country']) && $listing['id_country'] != $data['id_country'])){
				$stat_gid = 'country_'.$listing['id_country'];
				$stat = $this->CI->Listings_stat_model->get_stat_by_id($stat_gid);
				if($stat){
					$type_name = $this->get_operation_type_by_id($listing['id_type']).'_cnt';
					$save_data = array($type_name=>$stat[$type_name] ? $stat[$type_name]-1 : 0); 
					$validate_data = $this->CI->Listings_stat_model->validate_stat($stat['id'], $save_data);
					$this->CI->Listings_stat_model->save_stat($stat['id'], $validate_data['data']);
				}
						
				$type_name = $this->get_operation_type_by_id($type_id).'_cnt';
				$stat_gid = 'country_'.(isset($data['id_country']) ? $data['id_country'] : $listing['id_country']);
				$stat = $this->CI->Listings_stat_model->get_stat_by_id($stat_gid);
				if($stat){
					$save_data = array($type_name=>$stat[$type_name]+1); 
					$validate_data = $this->CI->Listings_stat_model->validate_stat($stat['id'], $save_data);
					$this->CI->Listings_stat_model->save_stat($stat['id'], $validate_data['data']);
				}else{
					$save_data = array('gid'=>$stat_gid, $type_name=>1);
					$validate_data = $this->CI->Listings_stat_model->validate_stat(null, $save_data);
					$this->CI->Listings_stat_model->save_stat(null, $validate_data['data']);
				}
			}
				
			if(($listing['id_type'] != $type_id) || (isset($data['id_region']) && $listing['id_region'] != $data['id_region'])){
				$stat_gid = 'region_'.$listing['id_region'];
				$stat = $this->CI->Listings_stat_model->get_stat_by_id($stat_gid);
				if($stat){
					$type_name = $this->get_operation_type_by_id($listing['id_type']).'_cnt';
					$save_data = array($type_name=>max($stat[$type_name]-1, 0)); 
					$validate_data = $this->CI->Listings_stat_model->validate_stat($stat['id'], $save_data);
					$this->CI->Listings_stat_model->save_stat($stat['id'], $validate_data['data']);
				}
				
				$type_name = $this->get_operation_type_by_id($listing['id_type']).'_cnt';
				$stat_gid = 'region_'.(isset($data['id_region']) ? $data['id_region'] : $listing['id_region']);
				$stat = $this->CI->Listings_stat_model->get_stat_by_id($stat_gid);
				if($stat){
					$save_data = array($type_name=>$stat[$type_name]+1); 
					$validate_data = $this->CI->Listings_stat_model->validate_stat($stat['id'], $save_data);
					$this->CI->Listings_stat_model->save_stat($stat['id'], $validate_data['data']);
				}else{
					$save_data = array('gid'=>$stat_gid, $type_name=>1);
					$validate_data = $this->CI->Listings_stat_model->validate_stat(null, $save_data);
					$this->CI->Listings_stat_model->save_stat(null, $validate_data['data']);
				}
			}
				
			if(($listing['id_type'] != $type_id) || (isset($data['id_city']) && $listing['id_city'] != $data['id_city'])){
				$stat_gid = 'city_'.$listing['id_city'];
				$stat = $this->CI->Listings_stat_model->get_stat_by_id($stat_gid);
				if($stat){
					$type_name = $this->get_operation_type_by_id($listing['id_type']).'_cnt';
					$save_data = array($type_name=>max($stat[$type_name]-1, 0)); 
					$validate_data = $this->CI->Listings_stat_model->validate_stat($stat['id'], $save_data);
					$this->CI->Listings_stat_model->save_stat($stat['id'], $validate_data['data']);
				}
					
				$type_name = $this->get_operation_type_id($type_id).'_cnt';	
				$stat_gid = 'city_'.(isset($data['id_city']) ? $data['id_city'] : $listing['id_city']);
				$stat = $this->CI->Listings_stat_model->get_stat_by_id($stat_gid);
				if($stat){
					$save_data = array($type_name=>$stat[$type_name]+1); 
					$validate_data = $this->CI->Listings_stat_model->validate_stat($stat['id'], $save_data);
					$this->CI->Listings_stat_model->save_stat($stat['id'], $validate_data['data']);
				}else{
					$save_data = array('gid'=>$stat_gid, $type_name=>1);
					$validate_data = $this->CI->Listings_stat_model->validate_stat(null, $save_data);
					$this->CI->Listings_stat_model->save_stat(null, $validate_data['data']);
				}
			}
						
				
			if(($listing['id_type'] != $type_id) || (isset($data['id_user']) && $listing['id_user'] != $data['id_user'])){		
				$this->CI->load->model('Users_model');
		
				$user = $this->CI->Users_model->get_user_by_id($listing['id_user']);
				if($user){
					$this->CI->Users_model->update_listings_stat($user['id'], $this->get_operation_type_by_id($listing['id_type']), false);
				}
		
				if(isset($data['id_user'])) $user = $this->CI->Users_model->get_user_by_id($data['id_user']);
				if($user){
					$this->CI->Users_model->update_listings_stat($user['id'], $this->get_operation_type_by_id($type_id));
				}
			}
		}
	}
	
	/**
	 * Return criteria for fulltext search
	 * @param ineteger $listing_id listing identifier
	 * @param array $fields fulltext fields
	 */
	public function get_fulltext_data($listing_id, $fields){
		$all_langs = $this->CI->pg_language->return_langs();
		$default_lang = $this->CI->pg_language->get_default_lang_id();
		
		$return = array(
			'main_fields'=>array(), 
			'fe_fields'=>array(), 
			'default_lang_id'=>$default_lang,
			'object_lang_id'=>$default_lang, 
		);
		$this->set_format_settings('get_description', true);
		$data = $this->get_listing_by_id($listing_id);
		$this->set_format_settings('get_description', false);

		if(in_array($data['user']['lang_id'], $this->CI->pg_language->languages)) 
			$return['object_lang_id'] = $data['user']['lang_id'];
			
		$current_lang = $return['object_lang_id'];
		$langs[$current_lang] =	$all_langs[$current_lang];
		if($current_lang != $default_lang){
			$langs[$default_lang] = $all_langs[$default_lang];
		};
		
		$return['main_fields'] = array('user' => $data['user']['output_name']);
		if(!empty($data['headline'])) $return['main_fields']['headline'] = $data['headline'];
		
		$this->CI->load->model('Properties_model');
		$this->CI->load->model('Countries_model');
		
		$this->load->helper('start');
		switch($data['operation_type']){
			case 'sale':
				if(!$data['price_negotiated']){
					if($data['price_reduced'] > 0){
						$return['main_fields']['price'] = strip_tags(currency_format_output(array('value'=>$data['price_reduced'], 'cur_gid'=>$data['gid_currency'])));
					}else{
						$return['main_fields']['price'] = strip_tags(currency_format_output(array('value'=>$data['price'], 'cur_gid'=>$data['gid_currency'])));
					}
				}
			break;
			case 'buy':
				if(!$data['price_negotiated']){
					$price = array();
					if($data['price'] > 0){
						$price[] = l('text_price_from', 'listings').' '.strip_tags(currency_format_output(array('value'=>$data['price'], 'cur_gid'=>$data['gid_currency'])));
					}
					if($data['price_max'] > 0){
						$price[] = l('text_price_to', 'listings').' '.strip_tags(currency_format_output(array('value'=>$data['price_max'], 'cur_gid'=>$data['gid_currency'])));
					}
					$return['main_fields']['price'] = implode(' ', $price);
				}
			break;
			case 'rent':
				if(!$data['price_negotiated']){
					if($data['price_reduced'] > 0){
						$return['main_fields']['price'] = strip_tags(currency_format_output(array('value'=>$data['price_reduced'], 'cur_gid'=>$data['gid_currency'])));
					}else{
						$return['main_fields']['price'] = strip_tags(currency_format_output(array('value'=>$data['price'], 'cur_gid'=>$data['gid_currency'])));
					}
					$return['main_fields']['price'] .= ' '.ld_option('price_period', 'listings', $data['price_period']);
					if($return['main_fields']['price_type']) $return['main_fields']['price'] .= '/'.ld_option('price_period', 'listings', $data['price_type']);
				}
			break;
			case 'lease':
				if(!$data['price_negotiated']){
					$price = array();
					if($data['price'] > 0){
						$price[] = l('text_price_from', 'listings').' '.strip_tags(currency_format_output(array('value'=>$data['price'], 'cur_gid'=>$data['gid_currency'])));
					}
					if($data['price_max'] > 0){
						$price[] = l('text_price_to', 'listings').' '.strip_tags(currency_format_output(array('value'=>$data['price_max'], 'cur_gid'=>$data['gid_currency'])));
					}
					$return['main_fields']['price'] = implode(' ', $price);
					$return['main_fields']['price'] .= ' '.ld_option('price_period', 'listings', $data['price_period']);
					if($return['main_fields']['price_type']) $return['main_fields']['price'] .= '/'.ld_option('price_period', 'listings', $data['price_type']);
				}
			break;
		}
		
		$square_units = $this->CI->pg_language->ds->get_reference('listings', $this->square_units_gid, $lang['id']);
		
		foreach($langs as $lang){
			$operation_types = $this->CI->pg_language->ds->get_reference('listings', 'operation_type', $lang['id']);
			$operation_types = $operation_types['option'];
			$return['main_fields']['operation_type_'.$lang['code']] = $operation_types[$data['operation_type']];
			
			$property_types = $this->CI->Properties_model->get_categories('property_types', $lang['id']);
			$property_items = $this->CI->Properties_model->get_all_properties('property_types', $lang['id']);
			$return['main_fields']['category_'.$lang['code']] = $property_types[$data['id_category']];
			$return['main_fields']['property_'.$lang['code']] = $property_items[$data['id_category']][$data['property_type']];
		
			if($data['sold']) $return['main_fields']['sold_'.$lang['code']] = l('field_sold', 'listings', $lang['id']);
			
			$country = $this->CI->Countries_model->get_country($data['id_country'], $lang['id']);
			$region = $this->CI->Countries_model->get_region($data['id_region'], $lang['id']);
			$city = $this->CI->Countries_model->get_city($data['id_city'], $lang['id']);
			
			$return['main_fields']['location_'.$lang['code']] = $country['name'];
			$return['main_fields']['location_'.$lang['code']] .= ' '.$region['name'];
			$return['main_fields']['location_'.$lang['code']] .= ' '.$city['name'];
			if(!empty($data['address'])) $return['main_fields']['location_'.$lang['code']] .= ' '.$data['address'];
			
			if(isset($data['zip'])) $return['main_fields']['zip_'.$lang['code']] = l('field_zip', 'listings', $lang['id']).' '.$data['zip'];
			
			if($data['price_negotiated']){
				$return['main_fields']['price_'.$lang['code']] = l('field_price_negotiated_'.$data['operation_type'], 'listings', $lang['id']);
			}else{
				$return['main_fields']['price_'.$lang['code']] = l('field_price', 'listings', $lang['id']).' '.$return['main_fields']['price'];
			}
			
			$return['main_fields']['square_'.$lang['code']] = l('field_square', 'listings', $lang['id']).' '.$data['square'];
			if($data['square_unit']){
				$return['main_fields']['square_'.$lang['code']] .= ' ';
				if(isset($square_units['option'][$data['square_unit']])){
					$return['main_fields']['square_'.$lang['code']] .= $square_units['option'][$data['square_unit']];
				}else{
					$return['main_fields']['square_'.$lang['code']] .= $data['square_unit'];
				}
			}
		}
		
		if(isset($return['main_fields']['price'])) unset($return['main_fields']['price']);
		
		foreach($fields as $field){
			$return['fe_fields'][$field] = $data[$field];
		}

		return $return;
	}
	
	/**
	 * Return seo link for search
	 */
	public function get_search_link_data(){
		//load properties
		$this->CI->load->model('Properties_model');
		$property_types = $this->CI->Properties_model->get_categories('property_types', $this->CI->pg_language->current_lang_id);
		$property_items = $this->CI->Properties_model->get_all_properties('property_types', $this->CI->pg_language->current_lang_id);
		
		$current_settings = isset($_SESSION['listings_search']) ? $_SESSION['listings_search'] : array();
	
		$operation_types = $this->get_operation_types();
		if(in_array('sale', $operation_types)) $operation_types[] = 'sold';
		if(isset($current_settings['data']['type']) && 
		   in_array($current_settings['data']['type'], $operation_types)){
			$operation_type = $current_settings['data']['type'];
		}else{
			$operation_type = $this->get_operation_type_gid_default();
		}
	
		$category_id = $property_type = 0;
		if(isset($current_settings['data']['id_category'])){
			$category_id = intval($current_settings['data']['id_category']);
			if($category_id && isset($current_settings['data']['property_type'])){
				$property_type = intval($current_settings['data']['property_type']);
			}
		}
		
		if(isset($current_settings['data']['id_country']) || isset($current_settings['data']['id_region']) || isset($current_settings['data']['id_city'])){
			$this->CI->load->helper('countries');
			$locations_data = get_location_data(array(array($current_settings['data']['id_country'], $current_settings['data']['id_region'], $current_settings['data']['id_city'])));
			$country = (isset($locations_data['country'][$current_settings['data']['id_country']])) ? $locations_data['country'][$current_settings['data']['id_country']]['name'] : '';
			$region = (isset($locations_data['region'][$current_settings['data']['id_region']])) ? $locations_data['region'][$current_settings['data']['id_region']]['name'] : '';
			$region_code = (isset($locations_data['region'][$current_settings['data']['id_region']])) ? $locations_data['region'][$current_settings['data']['id_region']]['code'] : '';
			$city = (isset($locations_data['city'][$current_settings['data']['id_city']])) ? $locations_data['city'][$current_settings['data']['id_city']]['name'] : '';
		}else{
			$country = '';
			$region = '';
			$city = '';
		}
		
		$keyword = '';
		if(isset($current_settings['data']['keyword'])){
			$keyword = $current_settings['data']['keyword'];
		}
		
		$order = 'default';
		$order_direction = 'DESC';
		$page = 1;
		
		if(!empty($current_settings)){
			$order = $current_settings['order'];
			$order_direction = $current_settings['order_direction'];
			$page = $current_settings['page'];
		}
		
		$action_data = array(
			'operation_type' => $operation_type,
			'operation_type_str' => l('operation_search_'.$operation_type, 'listings'),
			'id_category' => $category_id,
			'category' => $category_id ? $property_types[$category_id] : 'all',
			'property_type' => $property_type,
			'property' => $property_type ? $property_items[$category_id][$property_type] : 'all',
			'country' => $country,
			'region' => $region,
			'region_code' => $region_code,
			'city' => $city,
			'keyword' => $keyword,
			'order' => $order,
			'order_direction' => $order_direction,
			'page' => $page,
		);

		return $action_data;
	}
	
	/**
	 * Return current search criteria
	 * @return array
	 */
	public function get_search_data(){
		$filters = isset($_SESSION['listings_search']) ? $_SESSION['listings_search']['data'] : array();

		$operation_types = $this->get_operation_types(true);
		
		$current_search_data = array();
		if(isset($filters['type']) && in_array($filters['type'], $operation_types)){
			$operation_type = $filters['type'];
			unset($filters['type']);
		}else{
			$operation_type = current($operation_types);
		}
	
		if($operation_type == 'sold'){
			$current_search_data['type'] = array(
				'name' => l('field_listing_type', 'listings'),
				'value' => 'sold',
				'label' => l('text_sold', 'listings'),
			);
		}else{
			$current_search_data['type'] = array(
				'name' => l('field_listing_type', 'listings'),
				'value' => $operation_type,
				'label' => l('operation_search_'.$operation_type, 'listings'),
			);
		}

		if(isset($filters['id_country'])){
			$location_data = array();
			$location_data[] = $filters['id_country'];
			unset($filters['id_country']);
			
			if(isset($filters['id_region'])){
				$location_data[] = $filters['id_region'];				
				unset($filters['id_region']);
				
				if(isset($filters['id_city'])){
					$location_data[] = $filters['id_city'];
					unset($filters['id_city']);
				}
			}
		
			$this->CI->load->helper('countries');
		
			$location = cities_output_format(array($location_data));
			if(!empty($location)){
				$current_search_data['location'] = array(
					'name' => l('field_location', 'listings'),
					'value' => array($location_data[0], (int)$location_data[1], (int)$location_data[2]),
					'label' => $location[0],
				);
			}
		}

		if(isset($filters['id_category'])){
			$this->CI->load->helper('properties');
			if(isset($filters['property_type'])){
				$value = property_value($filters);
				$current_search_data['property_type'] = array(
					'name' => l('field_category', 'listings'),
					'value' => $filters['id_category'].'_'.$filters['property_type'],
					'label' => $value ? $value : $filters['property_type'],
				);
				unset($filters['property_type']);
			}else{
				$value = category_value($filters);
				$current_search_data['id_category'] = array(
					'name' => l('field_category', 'listings'),
					'value' => $filters['id_category'],
					'label' => $value ? $value : $filters['id_category'],
				);
			}	
			unset($filters['id_category']);
		}
		
		$this->CI->load->helper('start');
		
		if(isset($filters['price_max'])){
			if(!isset($filters['price_min'])){
				$filters['price_min'] = 0;
			}
			$filters['price'] = ' '.l('text_price_to', 'listings').' '.strip_tags(currency_format_output(array('value'=>$filters['price_max'])));
			unset($filters['price_max']);
		}
		
		if(isset($filters['price_min'])){
			$filters['price'] = l('text_price_from', 'listings').' '.strip_tags(currency_format_output(array('value'=>$filters['price_min']))).$filters['price'];
			unset($filters['price_min']);
		}
		
		$this->config->load('date_formats', TRUE);
		$date_format = $this->config->item('date_format_date_literal', 'date_formats');
	
		if(isset($filters['booking_date_start'])){
			$filters['booking_date_start'] = date($date_format, strtotime($filters['booking_date_start']));
		}
		
		if(isset($filters['booking_date_end'])){
			$filters['booking_date_end'] = date($date_format, strtotime($filters['booking_date_end']));
		}
	
		foreach((array)$filters as $field_name => $field_value){	
			if($field_name == 'field_editor_data') continue;
			$current_search_data[$field_name] = array(
				'name' => l('field_'.$field_name, 'listings'),
				'value' => is_array($field_value) ? implode(',', $field_value) : $field_value,
				'label' => is_array($field_value) ? implode(',', $field_value) : $field_value,
			);
		}
		
		if(isset($filters['field_editor_data'])){
			$property_type_gid = $this->get_field_editor_type($filters);
	
			$this->CI->load->model('Field_editor_model');
			$this->CI->Field_editor_model->initialize($property_type_gid);
	
			$fields_gids = array();
			foreach($filters['field_editor_data'] as $form_name => $form_data){
				$fields_gids = array_merge($fields_gids, array_keys($form_data));
			}
	
			$params = array('where_in'=>array('gid'=>$fields_gids));
			$fields_data = $this->CI->Field_editor_model->get_fields_list($params);
	
			$field_editor_data = array('gids'=>array(), 'data'=>array());
			$field_editor_range = array('gids'=>array(), 'data'=>array());
			foreach($filters['field_editor_data'] as $form_name => $form_data){
				foreach($form_data as $field_gid => $field_data){
					if(isset($field_data['range']) && is_array($field_data['range'])){
						if(isset($field_data['range']['min'])){
							$field_editor_data['gids'][$field_gid] = $field_data['range']['min'];
							$field_editor_data['data'][$fields_data[$field_gid]['field_name']] = $field_data['range']['min'];
						}else{
							$field_editor_data['gids'][$field_gid] = 0;
							$field_editor_data['data'][$fields_data[$field_gid]['field_name']] = 0;
						}
						if(isset($field_data['range']['max'])){
							$field_editor_range['gids'][$field_gid] = $field_data['range']['max'];
							$field_editor_range['data'][$fields_data[$field_gid]['field_name']] = $field_data['range']['max'];
						}else{
							$field_editor_range['gids'][$field_gid] = 0;
							$field_editor_range['data'][$fields_data[$field_gid]['field_name']] = 0;
						}
					}else{
						$field_editor_data['gids'][$field_gid] = $field_data;
						$field_editor_data['data'][$fields_data[$field_gid]['field_name']] = $field_data;
					}
				}
			}

			$params = array('where_in'=>array('gid'=>array_keys($field_editor_data['gids'])));
			$results = $this->CI->Field_editor_model->format_list_fields_for_view($params, array($field_editor_data['data']));
			foreach($results as $result){
				foreach($result as $field_gid=>$field_data){
					if($field_data['value_str']){
						$value = $field_data['value_str'];
					}elseif($field_data['value_dec']){
						$value = $field_data['value_dec'];
					}elseif($field_data['value_original']){
						$value = $field_data['value_original'];
					}else{
						$value = $field_data['value'];
					}
					$current_search_data['field_editor/'.$field_gid] = array(
						'name' => $field_data['name'],
						'value' => $filters['field_editor_data'][$field_gid],
						'label' => $value != '' || $field_editor_data['gids'][$field_gid] !== 0 ? $value : 0,
					);
				}
			}
		
			if(!empty($field_editor_range['gids'])){
				$params = array('where_in'=>array('gid'=>array_keys($field_editor_range['gids'])));
				$results = $this->CI->Field_editor_model->format_list_fields_for_view($params, array($field_editor_range['data']));
				foreach($results as $result){
					foreach($result as $field_gid=>$field_data){
						if($field_data['value_str']){
							$value = $field_data['value_str'];
						}elseif($field_data['value_dec']){
							$value = $field_data['value_dec'];
						}elseif($field_data['value_original']){
							$value = $field_data['value_original'];
						}else{
							$value = $field_data['value'];
						}
						if($value){
							if(isset($current_search_data['field_editor/'.$field_gid]) && $current_search_data['field_editor/'.$field_gid]['label']){
								$current_search_data['field_editor/'.$field_gid]['label'] .= '-'.$value;
							}else{
								$current_search_data['field_editor/'.$field_gid]['label'] = '<'.$value;
							}
						}elseif(isset($current_search_data['field_editor/'.$field_gid])){
							$current_search_data['field_editor/'.$field_gid]['label'] = '>'.$current_search_data['field_editor/'.$field_gid]['label'];
						}
					}
				}
			}
		}

		return $current_search_data;
	}
	
	/**
	 * Return gallery type
	 * @return array
	 */
	public function get_gallery_type(){
		$this->CI->load->model('Upload_gallery_model');
		return $this->CI->Upload_gallery_model->get_type_by_gid($this->gallery_type);
	}
	
	/**
	 * Return virtual tour type
	 * @return array
	 */
	public function get_vtour_type(){
		$this->CI->load->model('Upload_gallery_model');
		return $this->CI->Upload_gallery_model->get_type_by_gid($this->virtual_tour_type);
	}
	
	/**
	 * Return file type
	 * @return array
	 */
	public function get_file_type(){
		$this->CI->load->model('File_uploads_model');
		return $this->CI->File_uploads_model->get_config($this->file_config_id);
	}
	
	/**
	 * Return video type
	 * @return array
	 */
	public function get_video_type(){
		$this->CI->load->model('Video_uploads_model');
		return $this->CI->Video_uploads_model->get_config($this->video_config_id);
	}
	
	/**
	 * Return saved listings identifiers
	 */
	public function get_saved_listings_ids(){
		if($this->CI->session->userdata('auth_type') == 'user'){
			$user_id = $this->CI->session->userdata('user_id');
			$saved_listings = $this->CI->session->userdata('saved_listings');
			if(is_array($saved_listings)) return $saved_listings;
			$this->CI->load->model('listings/models/Saved_listings_model');
			$saved_listings = $this->CI->Saved_listings_model->get_saved_list_by_user((int)$user_id);
			foreach($saved_listings as $key=>$row){
				$saved_listings[$key] = $row['id_listing'];
			}
			$this->CI->session->set_userdata('saved_listings', $saved_listings);
			return $saved_listings;
		}
		return isset($_SESSION['saved_listings']) && is_array($_SESSION['saved_listings']) ? 
			$_SESSION['saved_listings'] : array();
	}
	
	#MOD#
	public function get_residensial(){
		$this->DB->select(implode(", ", $this->residential));
		$this->DB->from(RESIDENTIAL);
		$results = $this->DB->get()->result_array();
		return $results;
	}
	
	public function get_commercial(){
		$this->DB->select(implode(", ", $this->commercial));
		$this->DB->from(COMMERCIAL);
		$results = $this->DB->get()->result_array();
		return $results;
	}
	#MOD#
	
	# MOD for Comparison #
	// get data from parameter
	public function get_listing_by_comparison($L1,$L2,$L3,$L4){
		
		if($L1 == NULL){$pramet1 == "";}else{$pramet1 = "id = ".$L1;}
		if($L2 == NULL){$pramet2 == "";}else{$pramet2 = "OR id = ".$L2;}
		if($L3 == NULL){$pramet3 == "";}else{$pramet3 = "OR id = ".$L3;}
		if($L4 == NULL){$pramet4 == "";}else{$pramet4 = "OR id = ".$L4;}
		
		$this->DB->select(implode(', ', $this->_fields));
		$this->DB->from(LISTINGS_TABLE);
		$where = $pramet1." ".$pramet2." ".$pramet3." ".$pramet4." ORDER BY id DESC";
		$this->DB->where($where);
		$results = $this->DB->get()->result_array();
		/*if(!empty($results) && is_array($results)){
			return array_shift($this->format_listings(array($results[0])));
		}
		return array();*/

		return $results;
	}
	
	public function get_developer(){
		
		$this->DB->select(implode(', ', $this->developer));
		$this->DB->from('ipdb.pg_users');
		$results = $this->DB->get()->result_array();
		return $results;
	}
	
	public function get_similar_listings($similar_data){
		$limit = count($similar_data) * 5;
		foreach($similar_data as $k=>$val) {
			$exp_val = explode('-', $val);
			$condition = 'id_city = '.$exp_val[1].' AND property_type = '.$exp_val[2].' AND id != '.$exp_val[0];
			//$condition = 'id_city = 247572 AND property_type = 1';

			//$conditional = implode(' OR ', $condition);
			$this->DB->select(implode(', ', $this->_fields));
			$this->DB->from(LISTINGS_TABLE);
			$where = $condition." ORDER BY date_modified DESC LIMIT 5";
			$this->DB->where($where);
			$results[] = $this->DB->get()->result_array();
		}
		foreach($results as $datas) {
			foreach($datas as $key) {
				$similar[] = $key;
			}
		}
//		print_r($results); exit;
		return $similar;
	}
	
	# MOD for Comparison #
	
	#MOD FOR KPR BUTTON#
	public function get_active_kpr_button(){
		$this->DB->select('*');
		$this->DB->from('kpr_button');
		$this->DB->join('kpr_bank','kpr_bank.bankID=kpr_button.bank_id');
		$this->DB->where('status','1');
		$results = $this->DB->get()->result_array();
		return $results;
	}
	public function get_kpr_button(){
		$this->DB->select('*');
		$this->DB->from('kpr_button');
		$this->DB->join('kpr_bank','kpr_bank.bankID=kpr_button.bank_id');
		$this->DB->order_by('id', 'DESC');
		$results = $this->DB->get()->result_array();
		return $results;
	}
	
	public function activate_kpr_button($button_id, $status=1){
		$data = array(
					   'status' => $status
					);
		
		$this->db->where('id', $button_id);
		$this->db->update('kpr_button', $data); 
	}
	public function get_button_by_id($button_id){
		$this->DB->select('*');
		$this->DB->from('kpr_button');
		$this->DB->where('id', $button_id);
		$results = $this->DB->get()->result_array();
		return $results;
	}	
	
	public function get_button_by_bank_id($bank_id){
		$this->DB->select('*');
		$this->DB->from('kpr_button');
		$this->DB->where('bank_id', $bank_id);
		$results = $this->DB->get()->result_array();
		return $results;
	}	
	function add_kpr_button($data){
		$this->db->insert('kpr_button', $data);
	}
	function update_kpr_button($button_id, $data){
		$this->db->where('id', $button_id);
	 	$this->db->update('kpr_button', $data);
	}
	function delete_kpr_button($button_id){
		// remove button
		$this->DB->where('id', $button_id);
		$this->DB->delete('kpr_button');
	}
	
	#END OF KPR BUTTON MOD#
	
	#MOD FOR LAND AREA GRAB BY LISTING ID#
	public function get_land_area_by_id($listing_id){
		$this->DB->select('fe_field74');
		$this->DB->from('pg_listings_residential');
		$this->DB->where('id_listing',$listing_id);	
		$results = $this->DB->get()->result_array();
		return $results;				
	}
	#END OF LAND AREA GRAB#
}
