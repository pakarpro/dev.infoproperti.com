<?php

/**
* Listings api side controller
*
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Mikhail Chernov <mchernov@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: mchernov $
**/

Class Api_Listings extends Controller{
	
	/**
	 * Constructor
	 *
	 * @return Api_Listings
	 */
	public function __construct(){
		parent::Controller();
		$this->load->model('Listings_model');
	}
	
	/**
	 * Return listing data action
	 * @param integer $listing_id listing identifier
	 */
	public function get($listing_id){
		if(!$listing_id){
			log_message('error', 'listings API: Empty listing id');
			$this->set_api_content('errors', l('api_error_empty_listing_id', 'listings'));
			return false;
		}
		
		$this->Listings_model->set_format_settings(array('get_user'=>false, 'get_description'=>true, 'get_qr_code'=>true, 'get_file'=>true, 'get_video'=>true, 'get_booking'=>true));
		$listing = $this->Listings_model->get_listing_by_id($listing_id);		
		$this->Listings_model->set_format_settings(array('get_user'=>true, 'get_description'=> false, 'get_qr_code'=>false, 'get_file'=>false, 'get_video'=>false, 'get_booking'=>false));
		if(empty($listing) || !$listing['status']){
			$this->set_api_content('messages', l('api_error_listing_not_found', 'listings'));
			return false;
		}
		
		$user_id = $this->session->userdata('user_id');
		
		$site_url = site_url();
		$methods = array(
			'photos' => $site_url.'api/listings/get_photos/'.$listing_id,
			'virtual_tours' => $site_url.'api/listings/get_virtual_tours/'.$listing_id,
			'file' => $site_url.'api/listings/get_file/'.$listing_id,
			'video' => $site_url.'api/listings/get_video/'.$listing_id,
			'pdf' => $site_url.'listings/view/'.$listing_id.'/overview/1',
		);
		
		$this->config->load('date_formats', true);
		$date_formats = $this->config->item('st_format_date_literal', 'date_formats');
		
		$data = array(
			'listing_id' => $listing_id,
			'logo_image' => $listing['media']['photo']['thumbs'],
			'listing_file' => ($listing['is_file'] ? $listing['listing_file_content'] : ''),
			'listing_video' => ($listing['is_video'] ? $listing['listing_video_content'] : ''),
			'methods' => $methods,
			'date_format' => $date_formats,
		);
		
		unset($listing['id']);
		unset($listing['logo_image']);
		unset($listing['media']);
		unset($listing['listing_file']); 
		unset($listing['listing_file_date']);
		unset($listing['listing_video']);
		unset($listing['listing_video_image']);
		unset($listing['listing_video_data']);
		
		foreach($listing as $name=>$value){
			$data['listing_'.$name] = $value;
		}
		
		$this->Listings_model->check_views_count($user_id, $listing_id);
print '<pre>'.print_r($data, true).'</pre>'; exit;
		$this->set_api_content('data', $data);
	}
		
	/**
	 * Return listing photos action
	 * @param integer $listing_id listing identifier
	 */
	public function get_photos($listing_id){
		if(!$listing_id){
			log_message('error', 'listings API: Empty listing id');
			$this->set_api_content('errors', l('api_error_empty_listing_id', 'listings'));
			return false;
		}
		
		$format_settings = array(
			'get_user' => false,
			'get_location' => false, 
			'get_photos' => true, 
		);
		$this->Listings_model->set_format_settings($format_settings);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);	
		$format_settings = array(
			'get_user' => true,
			'get_location' => true, 
			'get_photos' => false,
		);
		$this->Listings_model->set_format_settings($format_settings);
			
		if(empty($listing)){
			$this->set_api_content('messages', l('api_error_listing_not_found', 'listings'));
			return false;
		}
		
		$data = array('photos'=>array());
		
		foreach($listing['photos'] as $photo){
			if(!$photo['status']) continue;
			$data['photos'][] = array(
				'source' => $photo['media']['url'],
				'big' => $photo['media']['thumbs']['big'],
				'middle' => $photo['media']['thumbs']['middle'],
				'small' => $photo['media']['thumbs']['small'],
				'comment' => $photo['comment'],
			);
		}
		
		$this->set_api_content('data', $data);
	}
	
	/**
	 * Return listing virtual tours action
	 * @param integer $listing_id listing identifier
	 */
	public function get_virtual_tours($listing_id){
		if(!$listing_id){
			log_message('error', 'listings API: Empty listing id');
			$this->set_api_content('errors', l('api_error_empty_listing_id', 'listings'));
			return false;
		}
		
		$format_settings = array(
			'get_user' => false,
			'get_location' => false, 
			'get_virtual_tours' => true,
		);
		$this->Listings_model->set_format_settings($format_settings);
		
		$listing = $this->Listings_model->get_listing_by_id($listing_id);		
		if(empty($listing)){
			$this->set_api_content('messages', l('api_error_listing_not_found', 'listings'));
			return false;
		}
		
		$format_settings = array(
			'get_user' => true,
			'get_location' => true, 
			'get_virtual_tours' => false,
		);
		$this->Listings_model->set_format_settings($format_settings);
		
		$data = array('virtual_tours'=>array(), 'max_panorama'=>$listing['max_panorama']);
		foreach($listing['virtual_tour'] as $panorama){
			if(!$panorama['status']) continue;
			$data['virtual_tours'][] = array(
				'source' => $panorama['media']['url'],
				'big' => $panorama['media']['thumbs']['big'],
				'middle' => $panorama['media']['thumbs']['middle'],
				'small' => $panorama['media']['thumbs']['small'],
				'comment' => $panorama['comment'],
			);
		}
		$this->set_api_content('data', $data);
	}
	
	/**
	 * Set listing data action
	 * @param integer $listing_id listing identifier
	 */
	public function set($listing_id){
		$auth_type = $this->session->userdata('auth_type');
		if('user' != $auth_type){
			log_message('error', 'listings API: Wrong auth type ("' . $auth_type . '")');
			$this->set_api_content('errors', l('api_error_wrong_auth_type', 'listings'));
			return false;
		}
		
		$user_id = $this->session->userdata('user_id');
		
		if($listing_id){
			$this->Listings_model->set_format_settings('use_format', false);
			$data = $this->Listings_model->get_listing_by_id($listing_id);
			$this->Listings_model->set_format_settings('use_format', true);
	
			if(!$data || $user_id != $data['id_user']){
				log_message('error', 'listings API: Invalid listing id');
				$this->set_api_content('errors', l('api_error_invalid_listing_id', 'listings'));
				return false;
			}
		}else{
			$data = array();
		}
		
		$post_data = $this->input->post('data', true);
		$post_data['id_country'] = $this->input->post('id_country', true);
		$post_data['id_region'] = $this->input->post('id_region', true);
		$post_data['id_city'] = $this->input->post('id_city', true);	
		$post_data['id_user'] = $user_id;
			
		$property_type_gid = $this->Listings_model->get_field_editor_type($data);
		if($property_type_gid){
			$fields_for_select = $this->Listings_model->get_fields_for_select($property_type_gid);
			foreach($fields_for_select as $field){
				$post_data[$field] = $this->input->post($field, true);
			}
		}
			
		$validate_data = $this->Listings_model->validate_listing($listing_id, $post_data);
		if(!empty($validate_data['errors'])){
			$this->set_api_content('errors', $validate_data['errors']);
			return false;
		}else if(!empty($validate_data['data'])){
			$this->set_api_content('errors', l('error_empty_listing_data', 'listings'));
		}else{
			if($listing_id){
				$this->Listings_model->save_listing($listing_id, $validate_data['data'], true);
				$this->set_api_content('messages', l('success_update_listing', 'listings'));
			}else{
				$listing_id = $this->Listings_model->save_listing(null, $validate_data['data'], true);
				$this->set_api_content('messages', l('success_add_listing', 'listings'));
			}
		}
	}
	
	/**
	 * Create listing data action
	 * @param integer $listing_id listing identifier
	 */
	public function create(){
		$this->set(0);
	}
	
	/**
	 * Remove listing data action
	 * @param integer $listing_id listing identifier
	 */
	public function delete($listing_id){
		$auth_type = $this->session->userdata('auth_type');
		if('user' != $auth_type){
			log_message('error', 'listings API: Wrong auth type ("' . $auth_type . '")');
			$this->set_api_content('errors', l('api_error_wrong_auth_type', 'listings'));
			return false;
		}
		
		$user_id = $this->session->userdata('user_id');
		
		$this->Listings_model->set_format_settings('use_format', false);
		$data = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);
	
		if(!$data){
			log_message('error', 'listings API: Empty listing id');
			$this->set_api_content('errors', l('api_error_empty_listing_id', 'listings'));
			return false;
		}
		
		if($user_id != $data['id_user']){
			log_message('error', 'listings API: Invalid listing id');
			$this->set_api_content('errors', l('api_error_invalid_listing_id', 'listings'));
			return false;
		}

		$this->Listings_model->delete_listing($listing_id);
		$this->set_api_content('messages', l('success_listings_delete', 'listings'));
	}
	
	/**
	 * Upload photo
	 * @param integer $listing_id listing identifier
	 * @param integer $photo_id photo identifier
	 */
	public function photo_upload($listing_id, $photo_id){
		$auth_type = $this->session->userdata('auth_type');
		if('user' != $auth_type){
			log_message('error', 'listings API: Wrong auth type ("' . $auth_type . '")');
			$this->set_api_content('errors', l('api_error_wrong_auth_type', 'listings'));
			return false;
		}
		
		$user_id = $this->session->userdata('user_id');
		
		$listing_id = intval($listing_id);
		if(!$listing_id){
			log_message('error', 'listings API: Empty listing id');
			$this->set_api_content('errors', l('api_error_empty_listing_id', 'listings'));
			return false;
		}
		
		$this->Listings_model->set_format_settings('use_format', false);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);
		if(!$listing || $user_id != $listing['id_user']){
			log_message('error', 'listings API: Invalid listing id');
			$this->set_api_content('errors', l('api_error_invalid_listing_id', 'listings'));
			return false;
		}

		$photo_id = intval($photo_id);
		if($photo_id){
			$this->load->model('Upload_gallery_model');
			$photo = $this->Upload_gallery_model->get_file_by_id($photo_id);
			if($listing_id != $photo['object_id']){
				log_message('error', 'listings API: Invalid listing id');
				$this->set_api_content('errors', l('api_error_invalid_listing_id', 'listings'));
				return false;
			}
		}

		$return = $this->Listings_model->save_photo($listing_id, $photo_id, 'photo_file');
		if(!empty($return['errors'])){
			$this->set_api_content('errors', implode('<br>', $return['errors']));
		}else{
			$this->set_api_content('messages', l('success_photo_uploaded', 'listings'));
		}
	}
	
	/**
	 * Remove photo
	 * @param integer $listing_id listing identifier
	 * @param integer $photo_id photo identifier
	 */
	public function photo_delete($listing_id, $photo_id){
		$auth_type = $this->session->userdata('auth_type');
		if('user' != $auth_type){
			log_message('error', 'listings API: Wrong auth type ("' . $auth_type . '")');
			$this->set_api_content('errors', l('api_error_wrong_auth_type', 'listings'));
			return false;
		}
		
		$user_id = $this->session->userdata('user_id');
		
		$listing_id = intval($listing_id);
		if(!$listing_id){
			log_message('error', 'listings API: Empty listing id');
			$this->set_api_content('errors', l('api_error_empty_listing_id', 'listings'));
			return false;
		}
		
		$photo_id = intval($photo_id);
		if(!$photo_id){
			log_message('error', 'listings API: Empty photo id');
			$this->set_api_content('errors', l('api_error_empty_photo_id', 'listings'));
			return false;
		}
		
		$this->Listings_model->set_format_settings('use_format', false);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);
		if(!$listing || $user_id != $listing['id_user']){
			log_message('error', 'listings API: Invalid listing id');
			$this->set_api_content('errors', l('api_error_invalid_listing_id', 'listings'));
			return false;
		}

		// remove photo
		$result = $this->Listings_model->delete_photo($listing_id, $photo_id);
		if(!$result){
			$this->set_api_content('errors', l('error_photo_deleted', 'listings'));
		}else{
			$this->set_api_content('messages', l('success_photo_deleted', 'listings'));
		}
	}
	
	/**
	 * Upload panorama
	 * @param integer $listing_id listing identifier
	 * @param integer $panorama_id panorama identifier
	 */
	public function panorama_upload($listing_id, $panorama_id){
		$auth_type = $this->session->userdata('auth_type');
		if('user' != $auth_type){
			log_message('error', 'listings API: Wrong auth type ("' . $auth_type . '")');
			$this->set_api_content('errors', l('api_error_wrong_auth_type', 'listings'));
			return false;
		}
		
		$user_id = $this->session->userdata('user_id');
		
		$listing_id = intval($listing_id);
		if(!$listing_id){
			log_message('error', 'listings API: Empty listing id');
			$this->set_api_content('errors', l('api_error_empty_listing_id', 'listings'));
			return false;
		}
		
		$this->Listings_model->set_format_settings('use_format', false);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);
		if(!$listing || $user_id != $listing['id_user']){
			log_message('error', 'listings API: Invalid listing id');
			$this->set_api_content('errors', l('api_error_invalid_listing_id', 'listings'));
			return false;
		}

		$panorama_id = intval($panorama_id);
		
		$this->load->model('Upload_gallery_model');
		if($panorama_id){
			$panorama = $this->Upload_gallery_model->get_file_by_id($panorama_id);
			if($listing_id != $photo['object_id']){
				log_message('error', 'listings API: Invalid listing id');
				$this->set_api_content('errors', l('api_error_invalid_listing_id', 'listings'));
				return false;
			}
		}

		$return = $this->Listings_model->save_panorama($listing_id, $panorama_id, 'panorama_file');
		if(!empty($return['errors'])){
			$this->set_api_content('errors', implode('<br>', $return['errors']));
		}else{
			$this->set_api_content('messages', l('success_panorama_uploaded', 'listings'));
		}
	}
	
	/**
	 * Remove panorama
	 * @param integer $listing_id listing identifier
	 * @param integer $panorama_id panorama identifier
	 */
	public function panorama_delete($listing_id, $panorama_id){
		$auth_type = $this->session->userdata('auth_type');
		if('user' != $auth_type){
			log_message('error', 'listings API: Wrong auth type ("' . $auth_type . '")');
			$this->set_api_content('errors', l('api_error_wrong_auth_type', 'listings'));
			return false;
		}
		
		$user_id = $this->session->userdata('user_id');
		
		$listing_id = intval($listing_id);
		if(!$listing_id){
			log_message('error', 'listings API: Empty listing id');
			$this->set_api_content('errors', l('api_error_empty_listing_id', 'listings'));
			return false;
		}
		
		$panorama_id = intval($panorama_id);
		if(!$panorama_id){
			log_message('error', 'listings API: Empty panorama id');
			$this->set_api_content('errors', l('api_error_empty_panorama_id', 'listings'));
			return false;
		}
		
		$this->Listings_model->set_format_settings('use_format', false);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);
		if(!$listing || $user_id != $listing['id_user']){
			log_message('error', 'listings API: Invalid listing id');
			$this->set_api_content('errors', l('api_error_invalid_listing_id', 'listings'));
			return false;
		}

		// remove panorama
		$result = $this->Listings_model->delete_photo($listing_id, $panorama_id);
		if(!$result){
			$this->set_api_content('errors', l('error_panorama_deleted', 'listings'));
		}else{
			$this->set_api_content('messages', l('success_panorama_deleted', 'listings'));
		}
	}
	
	/**
	 * Upload file
	 * @param integer $listing_id listing identifier
	 */
	public function file_upload($listing_id){
		$auth_type = $this->session->userdata('auth_type');
		if('user' != $auth_type){
			log_message('error', 'listings API: Wrong auth type ("' . $auth_type . '")');
			$this->set_api_content('errors', l('api_error_wrong_auth_type', 'listings'));
			return false;
		}
		
		$user_id = $this->session->userdata('user_id');
		
		$listing_id = intval($listing_id);
		if(!$listing_id){
			log_message('error', 'listings API: Empty listing id');
			$this->set_api_content('errors', l('api_error_empty_listing_id', 'listings'));
			return false;
		}
		
		$this->Listings_model->set_format_settings('use_format', false);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);
		if(!$listing || $user_id != $listing['id_user']){
			log_message('error', 'listings API: Invalid listing id');
			$this->set_api_content('errors', l('api_error_invalid_listing_id', 'listings'));
			return false;
		}
		
		$post_data = $this->Listings_model->save_file($listing_id, 'listing_file', true);
		
		///// delete listing file
		if($listing['listing_file']){
			$this->load->model('File_uploads_model');
			$this->File_uploads_model->delete_upload($this->Listings_model->file_config_id, $listing['prefix'], $listing['listing_file']);
			if(!isset($post_data['data']['file'])) $post_data['data']['listing_file'] = '';
		}

		$data['listing_file'] = $post_data['data']['file'];
		$this->Listings_model->save_listing($listing_id, $data);
		
		$this->set_api_content('messages', l('success_file_uploaded', 'listings'));
	}
	
	/**
	 * Remove file
	 * @param integer $listing_id listing identifier
	 */
	public function file_delete($listing_id){
		$auth_type = $this->session->userdata('auth_type');
		if('user' != $auth_type){
			log_message('error', 'listings API: Wrong auth type ("' . $auth_type . '")');
			$this->set_api_content('errors', l('api_error_wrong_auth_type', 'listings'));
			return false;
		}
		
		$user_id = $this->session->userdata('user_id');
		
		$listing_id = intval($listing_id);
		if(!$listing_id){
			log_message('error', 'listings API: Empty listing id');
			$this->set_api_content('errors', l('api_error_empty_listing_id', 'listings'));
			return false;
		}
		
		$this->Listings_model->set_format_settings('use_format', false);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);
		if(!$listing || $user_id != $listing['id_user']){
			log_message('error', 'listings API: Invalid listing id');
			$this->set_api_content('errors', l('api_error_invalid_listing_id', 'listings'));
			return false;
		}
		
		if(!$listing['listing_file']){
			log_message('error', 'listings API: Empty listing file');
			$this->set_api_content('errors', l('api_error_empty_file', 'listings'));
			return false;
		}
		
		$this->load->model('File_uploads_model');
		$this->File_uploads_model->delete_upload($this->Listings_model->file_config_id, $listing['prefix'], $listing['listing_file']);
		
		$data['listing_file'] = '';
		$this->Listings_model->save_listing($listing_id, $data);
		
		$this->set_api_content('messages', l('success_file_deleted', 'listings'));
	}
	
	/**
	 * Upload video
	 * @param integer $listing_id listing identifier
	 */
	public function video_upload($listing_id){
		$auth_type = $this->session->userdata('auth_type');
		if('user' != $auth_type){
			log_message('error', 'listings API: Wrong auth type ("' . $auth_type . '")');
			$this->set_api_content('errors', l('api_error_wrong_auth_type', 'listings'));
			return false;
		}
		
		$user_id = $this->session->userdata('user_id');
		
		$listing_id = intval($listing_id);
		if(!$listing_id){
			log_message('error', 'listings API: Empty listing id');
			$this->set_api_content('errors', l('api_error_empty_listing_id', 'listings'));
			return false;
		}
		
		$this->Listings_model->set_format_settings('use_format', false);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);
		if(!$listing || $user_id != $listing['id_user']){
			log_message('error', 'listings API: Invalid listing id');
			$this->set_api_content('errors', l('api_error_invalid_listing_id', 'listings'));
			return false;
		}
	
		$post_data = $this->Listings_model->save_video($listing_id, 'listing_video', true);
		///// delete video
		if($data['listing_video']){
			$this->load->model('Video_uploads_model');
			$this->Video_uploads_model->delete_upload($this->Listings_model->video_config_id, $listing['prefix'], $listing['listing_video'], $listing['listing_video_image'], $listing['listing_video_data']['data']['upload_type']);
			if(!isset($post_data['data']['video'])){
				$post_data['data']['listing_video'] = '';
				$post_data['data']['listing_video_image'] = '';
				$post_data['data']['listing_video_data'] = '';
			}
		}			
		
		$data['listing_video'] = $post_data['data']['listing_video'];
		$data['listing_video_image'] = $post_data['data']['listing_video_image'];
		$data['listing_video_data'] = $post_data['data']['listing_video_data'];
		$this->Listings_model->save_listing($listing_id, $data);
		
		$this->set_api_content('messages', l('success_video_uploaded', 'listings'));
	}
	
	/**
	 * Remove video
	 * @param integer $listing_id listing identifier
	 */
	public function video_delete($listing_id){
		$auth_type = $this->session->userdata('auth_type');
		if('user' != $auth_type){
			log_message('error', 'listings API: Wrong auth type ("' . $auth_type . '")');
			$this->set_api_content('errors', l('api_error_wrong_auth_type', 'listings'));
			return false;
		}
		
		$user_id = $this->session->userdata('user_id');
		
		$listing_id = intval($listing_id);
		if(!$listing_id){
			log_message('error', 'listings API: Empty listing id');
			$this->set_api_content('errors', l('api_error_empty_listing_id', 'listings'));
			return false;
		}
		
		$this->Listings_model->set_format_settings('use_format', false);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);
		if(!$listing || $user_id != $listing['id_user']){
			log_message('error', 'listings API: Invalid listing id');
			$this->set_api_content('errors', l('api_error_invalid_listing_id', 'listings'));
			return false;
		}
		
		if(!$listing['listing_video']){
			log_message('error', 'listings API: Empty listing video');
			$this->set_api_content('errors', l('api_error_empty_video', 'listings'));
			return false;
		}
		
		$this->load->model('Video_uploads_model');
		$this->Video_uploads_model->delete_upload($this->Listings_model->video_config_id, $listing['prefix'], $listing['listing_video'], $listing['listing_video_image'], $listing['listing_video_data']['data']['upload_type']);
		
		$data['listing_video'] = '';
		$data['listing_video_image'] = '';
		$data['listing_video_data'] = '';
		$this->Listings_model->save_listing($listing_id, $data);
			
		$this->set_api_content('messages', l('success_video_deleted', 'listings'));
	}
	
	/**
	 * Return my listings 
	 * @param string $type listing type
	 * @param integer $page page of results
	 * @param string $order sorting order 
	 * @param string $order_direction order direction
	 */
	public function my($operation_type='sale', $page=1, $order='date_modified', $order_direction='DESC'){
		$auth_type = $this->session->userdata('auth_type');
		if('user' != $auth_type){
			log_message('error', 'listings API: Wrong auth type ("' . $auth_type . '")');
			$this->set_api_content('errors', l('api_error_wrong_auth_type', 'listings'));
			return false;
		}
		
		$user_id = $this->session->userdata('user_id');
		
		$data['operation_type'] = $operation_type;
		if(empty($data['type'])) $data['operation_type'] = $this->Listings_model->get_operation_type_gid_default();
		
		$data['order'] = $order;
		if(!$data['order'])	$data['order'] = 'date_modified';
		
		$data['order_direction'] = $order_direction;
		if(!$data['order_direction']) $data['order_direction'] = 'DESC';
		
		$data['page'] = $page;
		if(empty($data['page'])) $data['page'] = 1;
			
		$filters = array('user'=> $user_id, 'operation_type'=>$data['operation_type']);		
			
		$data['listings'] = array();
			
		$data['listings_count'] = $this->Listings_model->get_listings_count($filters);
		if($data['listings_count'] > 0){
			$order_array = array();
			if($data['order'] == 'price'){
				$order_array['price_sorting'] = $data['order_direction'];
			}else{
				$order_array[$data['order']] = $data['order_direction'];
			}
			$this->Listings_model->set_format_settings(array('get_user'=>false, 'get_moderation'=>true));
			$listings = $this->Listings_model->get_listings_list($filters, $page, $items_on_page, $order_array);
			$this->Listings_model->set_format_settings(array('get_user'=>true, 'get_moderation'=>false));
			
			foreach($listings as $i=>$listing){
				$data['listings'][$i] = array(
					'listing_id' => $listing['id'],
					'logo_image' => $listing['media']['photo']['thumbs'],
				);
		
				unset($listing['id']);
				unset($listing['logo_image']);
				unset($listing['media']);
				unset($listing['listing_file']); 
				unset($listing['listing_file_date']);
				unset($listing['listing_video']);
				unset($listing['listing_video_image']);
				unset($listing['listing_video_data']);
		
				foreach($listing as $name=>$value){
					$data['listings'][$i]['listing_'.$name] = $value;
				}
			}
		}else{
			$this->set_api_content('messages', l('api_error_listing_not_found', 'listings'));
		}
	
		$this->config->load('date_formats', true);
		$data['date_format'] = $this->config->item('st_format_date_literal', 'date_formats');
		$data['date_time_format'] = $this->config->item('st_format_date_time_literal', 'date_formats');

		if($this->pg_module->is_module_installed('services')) {
			$this->load->model('Services_model');
			$data['featured_service_active'] = $this->Services_model->is_service_active('listings_featured_service');
			$data['highlight_service_active'] = $this->Services_model->is_service_active('listings_highlight_service');
			$data['lift_up_service_active'] = $this->Services_model->is_service_active('listings_lift_up_service');
			$data['lift_up_country_service_active'] = $this->Services_model->is_service_active('listings_lift_up_country_service');
			$data['lift_up_region_service_active'] = $this->Services_model->is_service_active('listings_lift_up_region_service');
			$data['lift_up_city_service_active'] = $this->Services_model->is_service_active('listings_lift_up_city_service');
			$data['slide_show_service_active'] = $this->Services_model->is_service_active('listings_slide_show_service');
		}
	
		$this->set_api_content('data', $data);
	}
	
	/**
	 * Search
	 *
	 * @param integer $page page of results
	 * @param string $order sorting order
	 * @param string $order_direction order direction asc/desc
	 */
	public function search($page=1, $order='date_created', $order_direction='DESC'){
		$filters = $this->input->post('criteria', true);
	
		$filters['active'] = '1';
		
		$data['order'] = trim(strip_tags($order));
		if(!$data['order']) $data['order'] = 'date_created';
		
		$data['order_direction'] = strtoupper(trim(strip_tags($data['order_direction'])));
		if($data['order_direction']!='DESC') $data['order_direction'] = 'ASC';

		$data['page'] = $page;
		if(!$data['page']) $data['page'] = 1;
		
		$items_on_page = $this->pg_module->get_module_config('start', 'index_items_per_page');
		
		$data['listings_count'] = $this->Listings_model->get_listings_count($filters);
		
		$use_leader = false;
		if($data['listings_count'] > 0){
			$order_array = array();

			if($order == 'default'){
				if(isset($data['id_city']) && !empty($data['id_city'])) $order_array['lift_up_city_date_end'] = 'DESC';
				if(isset($data['id_region']) && !empty($data['id_region'])) $order_array['lift_up_region_date_end'] = 'DESC';
				if(isset($data['id_country']) && !empty($data['id_country'])) $order_array['lift_up_country_date_end'] = 'DESC';
				$order_array['lift_up_date_end'] = 'DESC';
				$order_array['id'] = 'DESC';
				$use_leader = true;
			}else{
				$order_array[$data['order']] = $data['order_direction'];
			}
	
			$this->Listings_model->set_format_settings('get_user', false);
			$listings = $this->Listings_model->get_listings_list($filters, $data['page'], $items_on_page, $order_array);
			$this->Listings_model->set_format_settings('get_user', true);
			
			foreach($listings as $i=>$listing){
				$data['listings'][$i] = array(
					'listing_id' => $listing['id'],
					'logo_image' => $listing['media']['photo']['thumbs'],
				);
		
				unset($listing['id']);
				unset($listing['logo_image']);
				unset($listing['media']);
				unset($listing['listing_file']); 
				unset($listing['listing_file_date']);
				unset($listing['listing_video']);
				unset($listing['listing_video_image']);
				unset($listing['listing_video_data']);
		
				foreach($listing as $name=>$value){
					$data['listings'][$i]['listing_'.$name] = $value;
				}
			}
		}else{
			$this->set_api_content('messages', l('api_error_listing_not_found', 'listings'));
		}

		$this->config->load('date_formats', TRUE);
		$data['date_format'] = $this->config->item('st_format_date_literal', 'date_formats');
		$data['date_time_format'] = $this->config->item('st_format_date_time_literal', 'date_formats');
		
		$use_save_search = $this->session->userdata('auth_type') == 'user';
		$data['use_save_search'] = $use_save_search;
		
		$data['use_leader'] = $use_leader;
	
		$this->set_api_content('data', $data);
	}
	
	/**
	 * Saved listings
	 *
	 * @param integer $page page of results
	 * @param string $order sorting order
	 * @param string $order_direction order direction asc/desc
	 */
	public function saved($page=1, $order='date_created', $order_direction='DESC'){
		
		$auth_type = $this->session->userdata('auth_type');
		if('user' != $auth_type){
			log_message('error', 'listings API: Wrong auth type ("' . $auth_type . '")');
			$this->set_api_content('errors', l('api_error_wrong_auth_type', 'listings'));
			return false;
		}
		
		$user_id = $this->session->userdata('user_id');
		
		$filters = array('ids'=>array(), 'active'=>1);
		
		$this->load->model('listings/models/Saved_listings_model');
		$saved_listings = $this->Saved_listings_model->get_saved_list_by_user($user_id);
		foreach($saved_listings as $saved){
			$filters['ids'][] = $saved['id_listing'];
		}
		
		if(empty($filters['ids'])){
			$this->set_api_content('messages', l('api_error_listing_not_found', 'listings'));
			return false;
		}
		
		$data['order'] = trim(strip_tags($order));
		if(!$data['order']) $data['order'] = 'date_created';
		
		$data['order_direction'] = strtoupper(trim(strip_tags($data['order_direction'])));
		if($data['order_direction']!='DESC') $data['order_direction'] = 'ASC';

		$data['page'] = $page;
		if(!$data['page']) $data['page'] = 1;
		
		$items_on_page = $this->pg_module->get_module_config('start', 'index_items_per_page');
		
		$data['listings_count'] = $this->Listings_model->get_listings_count($filters);
		
		$use_leader = false;
		if($data['listings_count'] > 0){
			$order_array = array();

			if($order == 'default'){
				if(isset($data['id_city']) && !empty($data['id_city'])) $order_array['lift_up_city_date_end'] = 'DESC';
				if(isset($data['id_region']) && !empty($data['id_region'])) $order_array['lift_up_region_date_end'] = 'DESC';
				if(isset($data['id_country']) && !empty($data['id_country'])) $order_array['lift_up_country_date_end'] = 'DESC';
				$order_array['lift_up_date_end'] = 'DESC';
				$order_array['id'] = 'DESC';
				$use_leader = true;
			}else{
				$order_array[$data['order']] = $data['order_direction'];
			}
	
			$this->Listings_model->set_format_settings('get_user', false);
			$listings = $this->Listings_model->get_listings_list($filters, $data['page'], $items_on_page, $order_array);
			$this->Listings_model->set_format_settings('get_user', true);
			
			foreach($listings as $i=>$listing){
				$data['listings'][$i] = array(
					'listing_id' => $listing['id'],
					'logo_image' => $listing['media']['photo']['thumbs'],
				);
		
				unset($listing['id']);
				unset($listing['logo_image']);
				unset($listing['media']);
				unset($listing['listing_file']); 
				unset($listing['listing_file_date']);
				unset($listing['listing_video']);
				unset($listing['listing_video_image']);
				unset($listing['listing_video_data']);
		
				foreach($listing as $name=>$value){
					$data['listings'][$i]['listing_'.$name] = $value;
				}
			}
		}else{
			$this->set_api_content('messages', l('api_error_listing_not_found', 'listings'));
			return false;
		}

		$this->config->load('date_formats', TRUE);
		$data['date_format'] = $this->config->item('st_format_date_literal', 'date_formats');
		$data['date_time_format'] = $this->config->item('st_format_date_time_literal', 'date_formats');
		
		$use_save_search = $this->session->userdata('auth_type') == 'user';
		$data['use_save_search'] = $use_save_search;
		
		$data['use_leader'] = $use_leader;
	
		$this->set_api_content('data', $data);
	}

	/**
	 * Add listing to saved
	 * @param integer $listing_id listing identifier
	 */
	public function save($listing_id){
		$auth_type = $this->session->userdata('auth_type');
		if('user' != $auth_type){
			log_message('error', 'listings API: Wrong auth type ("' . $auth_type . '")');
			$this->set_api_content('errors', l('api_error_wrong_auth_type', 'listings'));
			return false;
		}
		
		$user_id = $this->session->userdata('user_id');

		$listing_id = intval($listing_id);
		if(!$listing_id){
			log_message('error', 'listings API: Empty listing id');
			$this->set_api_content('errors', l('api_error_empty_listing_id', 'listings'));
			return false;
		}
		
		$this->Listings_model->set_format_settings('use_format', false);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);
		if(!$listing || $user_id != $listing['id_user']){
			log_message('error', 'listings API: Invalid listing id');
			$this->set_api_content('errors', l('api_error_invalid_listing_id', 'listings'));
			return false;
		}
		
		$this->load->model('listings/models/Saved_listings_model');
		
		$this->Saved_listings_model->get_saved_list_by_user($user_id);
		
		$listing_ids = array();
		foreach($saved_listings as $saved){
			$listing_ids = $saved['id_listing'];
		}
		if(in_array($listing_id, $listing_ids)){
			log_message('error', 'listings API: Listing already saved');
			$this->set_api_content('errors', l('api_error_already_saved', 'listings'));
			return false;
		}		
		
		$data['id_user'] = $user_id;
		$data['id_listing'] = $listing_id;
		$this->Saved_listings_model->save_saved(null, $data);
		
		$this->set_api_content('messages', l('success_listings_saved', 'listings'));
	}
	
	/**
	 * Remove listing from saved
	 * @param integer $listing_id listing identifier
	 */
	public function delete_saved($listing_id){
		$auth_type = $this->session->userdata('auth_type');
		if('user' != $auth_type){
			log_message('error', 'listings API: Wrong auth type ("' . $auth_type . '")');
			$this->set_api_content('errors', l('api_error_wrong_auth_type', 'listings'));
			return false;
		}
		
		$user_id = $this->session->userdata('user_id');

		$listing_id = intval($listing_id);
		if(!$listing_id){
			log_message('error', 'listings API: Empty listing id');
			$this->set_api_content('errors', l('api_error_empty_listing_id', 'listings'));
			return false;
		}
		
		$this->load->model('listings/models/Saved_listings_model');
		$this->Saved_listings_model->delete_saved($user_id, $listing_id);
		
		$this->set_api_content('messages', l('success_delete_saved', 'listings'));
	}
	
	/**
	 * Return price for booking period
	 * @param integer $listing_id listing identifier
	 */
	public function get_booking_price($listing_id){
		$this->Listings_model->set_format_settings('use_format', false);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);

		$date_start = $this->input->post('date_start', true);
		$date_end = $this->input->post('date_end', true);
		
		$guests = intval($this->input->post('guests', true));
		if(!$guests) $guests = 1;
		
		if(!$date_start || !$date_end){
			if($listing['price_negotiated']){
				$format_price_value = l('text_negotiated_price_rent', 'listings');
			}else{
				if($listing['price_reduced'] > 0){
					$price_value = $listing['price_reduced'];
				}else{
					$price_value = $listing['price'];	
				}
					
				$this->load->helper('start');
				$price_arr = array('value'=>$price_value, 'cur_gid'=>$listing['gid_currency']);	
				$format_price_value = l('text_price_from', 'listings').' '.currency_format_output($price_arr);
			}
		}else{
			$this->load->model('listings/models/Listings_booking_model');
			$price_value = $this->Listings_booking_model->get_period_price($listing, $date_start, $date_end, $guests);
			if($price_value){
				$this->load->helper('start');
				$price_arr = array('value'=>$price_value, 'cur_gid'=>$listing['gid_currency']);	
				$format_price_value = currency_format_output($price_arr);
			}else{
				$format_price_value = l('text_negotiated_price_rent', 'listings');
			}
		}
		
		$this->set_api_content('price', $format_price_value);
	}
	
	/**
	 * Bookings of listings
	 * @param string $status order status
	 * @param string $order sorting order
	 * @param string $order_direction order direction
	 * @param integer $page page of results
	 */
	public function orders($status='approve', $order='created', $order_direction='DESC', $page=1){
		$current_settings = isset($_SESSION['listings_booking']) ? $_SESSION['listings_booking'] : array();
		if(!isset($current_settings['data'])) $current_settings['data'] = array();
		if(!isset($current_settings['order'])) $current_settings['order'] = 'created';
		if(!isset($current_settings['order_direction'])) $current_settings['order_direction'] = 'DESC';
		if(!isset($current_settings['page'])) $current_settings['page'] = '1';
		if(!isset($current_settings['filters'])) $current_settings['filters'] = array('status'=>'approve');
		
		$user_id = $this->session->userdata('user_id');
			
		$this->load->model('listings/models/Listings_booking_model');
		
		$data = $this->input->post('data', true);
		if(!empty($data)) $current_settings['filters'] = $data;
		if($status){
			if($status != $current_settings['filters']['status']){
				$page = 1;
			}
			$current_settings['filters']['status'] = $status;
		}
		$filters = $current_settings['filters'];
		if(isset($filters['keyword'])) $filters['keyword_mode'] = 1;
		$filters['not_user'] = $user_id;
		$filters['owners'] = $user_id;
		
		$order = trim(strip_tags($order));
		if(!empty($order)) $current_settings['order'] = $order;
		$order = $current_settings['order'];
	
		$order_direction = strtoupper(trim(strip_tags($order_direction)));
		if(!empty($order_direction)) $current_settings['order_direction'] = $order_direction;
		$order_direction = $current_settings['order_direction'];
		
		$page = intval($page);
		if(!empty($page)) $current_settings['page'] = $page;
		$page = $current_settings['page'];
		
		if($order_direction != 'ASC') $order_direction = 'DESC';
		
		$items_on_page = $this->pg_module->get_module_config('start', 'index_items_per_page');
	
		$orders_count = $this->Listings_booking_model->get_periods_count($filters);
	
		$this->load->helper('sort_order');

		$current_settings['page'] = get_exists_page_number($page, $orders_count, $items_on_page);
	
		$_SESSION['listings_booking'] = $current_settings;
	
		$orders = array();
		if($orders_count > 0){
			$order_array = array();
			switch($order){
				case 'created':
					$order_array['date_created'] = $order_direction;
				break;
				case 'modified':
					$order_array['date_modified'] = $order_direction;
				break;
				default:
					$order_array[$order] = $order_direction;
				break;
			}
			$this->Listings_booking_model->set_format_settings('get_listing', true);
			$orders = $this->Listings_booking_model->get_periods_list($filters, $current_settings['page'], $items_on_page, $order_array);
			$this->Listings_booking_model->set_format_settings('get_listing', false);
		}
		$this->set_api_content('settings', $current_settings);
		$this->set_api_content('orders', $orders);
		$this->set_api_content('orders_count', $orders_count);
	}
	
	/**
	 * Return requests list
	 * @param string $status request status
	 * @param string $order sorting order
	 * @param string $order_direction order direction
	 * @param integer $page page of results
	 */
	public function requests($status='approve', $order='created', $order_direction='DESC', $page=1){
		$current_settings = isset($_SESSION['listings_requests']) ? $_SESSION['listings_requests'] : array();
		if(!isset($current_settings['data'])) $current_settings['data'] = array();
		if(!isset($current_settings['order'])) $current_settings['order'] = 'created';
		if(!isset($current_settings['order_direction'])) $current_settings['order_direction'] = 'DESC';
		if(!isset($current_settings['page'])) $current_settings['page'] = '1';
		if(!isset($current_settings['filters'])) $current_settings['filters'] = array('status'=>'approve');
			
		$this->load->model('listings/models/Listings_booking_model');
		
		$data = $this->input->post('data', true);
		if(!empty($data)) $current_settings['filters'] = $data;
		if($status){
			if($status != $current_settings['filters']['status']){
				$page = 1;
			}
			$current_settings['filters']['status'] = $status;
		}
		$filters = $current_settings['filters'];
		if(isset($filters['keyword'])) $filters['keyword_mode'] = 1;
		$filters['users'] = $this->session->userdata('user_id');
		
		$order = trim(strip_tags($order));
		if(!empty($order)) $current_settings['order'] = $order;
		$order = $current_settings['order'];
	
		$order_direction = strtoupper(trim(strip_tags($order_direction)));
		if(!empty($order_direction)) $current_settings['order_direction'] = $order_direction;
		$order_direction = $current_settings['order_direction'];
		
		$page = intval($page);
		if(!empty($page)) $current_settings['page'] = $page;
		$page = $current_settings['page'];
		
		if($order_direction != 'ASC') $order_direction = 'DESC';
		
		$items_on_page = $this->pg_module->get_module_config('start', 'index_items_per_page');
		
		$requests_count = $this->Listings_booking_model->get_periods_count($filters);
		
		$this->load->helper('sort_order');

		$current_settings['page'] = get_exists_page_number($page, $requests_count, $items_on_page);
	
		$_SESSION['listings_requests'] = $current_settings;
	
		$requests = array();
		if($requests_count > 0){
			$order_array = array();
			switch($order){
				case 'modified':
					$order_array['date_modified'] = $order_direction;
				break;
				default:
					$order_array[$order] = $order_direction;
				break;
			}
			$this->Listings_booking_model->set_format_settings('get_listing', true);
			$requests = $this->Listings_booking_model->get_periods_list($filters, $current_settings['page'], $items_on_page, $order_array);
			$this->Listings_booking_model->set_format_settings('get_listing', false);
		}
		
		$this->set_api_content('settings', $current_settings);
		$this->set_api_content('requests', $requests);
		$this->set_api_content('requests_count', $requests_count);
	}
	
	/**
	 * Remove booking order
	 * @param integer $order_id order identifier
	 */
	public function order_delete($order_id){
		$user_id = $this->session->userdata('user_id');

		$this->load->model('listings/models/Listings_booking_model');

		$this->Listings_booking_model->set_format_settings('get_listing', true);
		$order = $this->Listings_booking_model->get_period_by_id($order_id);
		$this->Listings_booking_model->set_format_settings('get_listing', false);

		if($user_id != $order['listing']['id_user']){
			$this->set_api_content('error', l('api_error_not_access', 'listings'));
			return;
		}

		if(!empty($order_id)){
			$this->Listings_booking_model->delete_period($order_id);
			$this->set_api_content('success', l('success_order_deleted', 'listings'));
		}
	}
	
	/**
	 * Remove booking request
	 * @param integer $request_id request identifier
	 */
	public function request_delete($request_id){
		$user_id = $this->session->userdata('user_id');

		$this->load->model('listings/models/Listings_booking_model');

		$this->Listings_booking_model->set_format_settings('get_listing', true);
		$request = $this->Listings_booking_model->get_period_by_id($request_id);
		$this->Listings_booking_model->set_format_settings('get_listing', false);

		if($user_id != $request['id_user']){
			$this->set_api_content('error', l('api_error_not_access', 'listings'));
			return;
		}

		if(!empty($request_id)){
			$this->Listings_booking_model->delete_period($request_id);
			$this->set_api_content('success', l('success_request_deleted', 'listings'));
		}
	}
	
	/**
	 * Approve booking order
	 * @param integer $order_id order identifier
	 */
	public function order_approve($order_id){
		$user_id = $this->session->userdata('user_id');

		$this->load->model('listings/models/Listings_booking_model');

		$order = $this->Listings_booking_model->get_period_by_id($order_id);

		if($user_id != $order['listing']['id_user']){
			$this->set_api_content('error', l('api_error_not_access', 'listings'));
			return;
		}
		if(!empty($order_id)){
			$is_intersect = $this->Listings_booking_model->is_intersect_exists_orders($listing_id, 0, $date_start, $date_end, $order_id);
			if($is_intersect){
				$this->set_api_content('error', l('error_intersect_calendar_periods', 'listings'));
			}else{
				$this->Listings_booking_model->approve_period($order_id);
				$this->set_api_content('success', l('success_order_approved', 'listings'));
			}
		}
	}
	
	/**
	 * Decline booking order
	 * @param integer $order_id order identifier
	 */
	public function order_decline($order_id){
		$user_id = $this->session->userdata('user_id');

		$this->load->model('listings/models/Listings_booking_model');

		$this->Listings_booking_model->set_format_settings('get_listing', true);
		$order = $this->Listings_booking_model->get_period_by_id($order_id);
		$this->Listings_booking_model->set_format_settings('get_listing', false);

		if($user_id != $order['listing']['id_user']){
			$this->set_api_content('error', l('api_error_not_access', 'listings'));
			return;
		}

		if(!empty($order_id)){
			$this->Listings_booking_model->decline_period($order_id);
			$this->set_api_content('success', l('success_order_declined', 'listings'));
		}
	}
	
	/**
	 * Save order
	 * @param integer $listing_id listing identifier
	 * @param integer $order_id order identifier
	 */
	public function save_order($listing_id, $order_id=null){
		$user_id = $this->session->userdata('user_id');

		$this->Listings_model->set_format_settings('use_format', false);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);

		$this->load->model('listings/models/Listings_booking_model');
		
		$post_period = $this->input->post('period', true);
		$post_period['id_listing'] = $listing_id;
		$post_period['id_user'] = $user_id;
		
		switch($listing['price_type']){
			case 1:
				$date_start_alt = $this->input->post('date_start_alt', true);
				if($date_start_alt) $post_period['date_start'] = $date_start_alt;
						
				$date_end_alt = $this->input->post('date_end_alt', true);
				if($date_end_alt) $post_period['date_end'] = $date_end_alt;
			break;
			case 2: 
				$date_start_month = $this->input->post('date_start_month', true);
				$date_start_year = $this->input->post('date_start_year', true);
				$post_period['date_start'] = $date_start_year.'-'.$date_start_month.'-01';
								
				$date_end_month = $this->input->post('date_end_month', true);
				$date_end_year = $this->input->post('date_end_year', true);
				$post_period['date_end'] = $date_end_year.'-'.$date_end_month.'-01';
			break;
		}
					
		$validate_data = $this->Listings_booking_model->validate_period(null, $post_period, true);
		if(!empty($validate_data['errors'])){
			$this->set_api_content('error', $validate_data['errors']);
		}else{
			$guests = $validate_data['data']['guests'] ? $validate_data['data']['guests'] : 1;
			$validate_data['data']['price'] = $this->Listings_booking_model->get_period_price($listing, $validate_data['data']['date_start'], $validate_data['data']['date_end'], $guests);
			$this->Listings_booking_model->save_period(null, $validate_data['data']);
			$this->set_api_content('success', l('success_order_added', 'listings'));
		}
	}
	
	/**
	 * Save period
	 * @param integer $listing_id listing identifier
	 * @param integer $period_id period identifier
	 */
	public function save_period($listing_id, $period_id=null){
		$user_id = $this->session->userdata('user_id');

		$this->Listings_model->set_format_settings('use_format', false);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);

		if(!$listing || $listing['id_user'] != $user_id){
			$this->set_api_content('error', l('api_error_not_access', 'listings'));
			return;
		}
		
		$this->load->model('listings/models/Listings_booking_model');
		
		$post_period = $this->input->post('period', true);
		$post_period['id_listing'] = $listing_id;
		$post_period['id_user'] = $user_id;
		
		switch($listing['price_type']){
			case 1:
				$date_start_alt = $this->input->post('date_start_alt', true);
				if($date_start_alt) $post_period['date_start'] = $date_start_alt;
						
				$date_end_alt = $this->input->post('date_end_alt', true);
				if($date_end_alt) $post_period['date_end'] = $date_end_alt;
			break;
			case 2: 
				$date_start_month = $this->input->post('date_start_month', true);
				$date_start_year = $this->input->post('date_start_year', true);
				$post_period['date_start'] = $date_start_year.'-'.$date_start_month.'-01';
								
				$date_end_month = $this->input->post('date_end_month', true);
				$date_end_year = $this->input->post('date_end_year', true);
				$post_period['date_end'] = $date_end_year.'-'.$date_end_month.'-01';
			break;
		}
					
		$validate_data = $this->Listings_booking_model->validate_period($period_id, $post_period);
		if(!empty($validate_data['errors'])){
			$this->set_api_content('error', $validate_data['errors']);
		}else{
			$period_id = $this->Listings_booking_model->save_period($period_id, $validate_data['data']);
			$this->set_api_content('success', l('success_period_added', 'listings'));
			$this->set_api_content('id', $period_id);
			$data = $this->Listings_booking_model->format_period($validate_data['data']);
			$this->set_api_content('data', $data);
		}
	}
	
	/**
	 * Remove booking period
	 * @param integer $period_id period identifier
	 */
	public function period_delete($period_id){
		$user_id = $this->session->userdata('user_id');

		if(!$period_id){
			$this->set_api_content('error', l('api_error_not_access', 'listings'));
			return;
		}

		$this->load->model('listings/models/Listings_booking_model');

		$this->Listings_booking_model->set_format_settings('use_format', false);
		$period = $this->Listings_booking_model->get_period_by_id($period_id);
		$this->Listings_booking_model->set_format_settings('use_format', true);

		if($user_id != $period['id_user']){
			$this->set_api_content('error', l('api_error_not_access', 'listings'));
			return;
		}

		$this->Listings_booking_model->delete_period($period_id);
		$this->set_api_content('success', l('success_period_deleted', 'listings'));
	}
}
