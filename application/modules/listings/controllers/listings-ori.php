<?php 

/**
* Listings user side controller
* 
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/

class Listings extends Controller{
	
	/**
	 * Constructor
	 * @return Listings
	 */
	public function __construct(){
		parent::Controller();
		$this->load->model('Listings_model');
		$this->load->model('Menu_model');
		
		/*$this->output->enable_profiler(TRUE);
		$sections = array(
        	'config'  => TRUE,
        	'queries' => TRUE
		);
		$this->output->set_profiler_sections($sections);*/
	}
	
	#mod for KPR#
	function object_to_array($object) {
    	return (array) $object;
	}
	public function fix_property_name($properti)
	{
		$properti = strtolower($properti);
		switch($properti)
		{
			case 'apartment':
				$properti = 'apartemen';
				break;
			default:
				$properti = $properti;
				break;
		}
		return $properti;
	}
	#public function kpr($price, $properti)
	public function kpr($data)	
	{
		#set to send reduced price data if the user already set one#
		if($data['price_reduced']!=0){
			$this->template_lite->assign('price', $data['price_reduced']);
		}
		else{
			$this->template_lite->assign('price', $data['price']);	
		}
		#$this->template_lite->assign('price', $price);
		#$propertiID = $this->fix_property_name($properti);
		$propertiID = $this->fix_property_name($data['property']);
		$propertiID = $this->kpr_get_properti_id($propertiID);
		$this->template_lite->assign('propertiID', $propertiID);
		$promosi = $operation_type = $this->Listings_model->list_promosi();
		$this->template_lite->assign('promosi', $promosi);
		$pinjaman = $operation_type = $this->Listings_model->list_pinjaman();
		$this->template_lite->assign('pinjaman', $pinjaman);
		$properti = $operation_type = $this->Listings_model->list_properti();
		$this->template_lite->assign('properti', $properti);
		
		return $this->template_lite->fetch('kpr');
	}
	public function kpr_get_properti_id($properti)
	{
		mysql_connect("localhost","root","");
		mysql_select_db("ipdb");
		$sql=mysql_query('
		SELECT * FROM kpr_properti
		WHERE kpr_properti.namaproperti LIKE "%'.$properti.'%"
		');
		while($row=mysql_fetch_array($sql)){	
			$propertiID = $row['propertiID'];
		}
		return $propertiID;
	}
	public function kpr_bank($bank, $properti, $id)
	{
		$listing = $this->Listings_model->get_listing_by_id($id);
		$land_area = $this->Listings_model->get_land_area_by_id($id);
		$land_area = $land_area[0]['fe_field74'];
		$propertiID = $this->fix_property_name($properti);
		$propertiID = $this->kpr_get_properti_id($propertiID);
		$bank_detail = $this->get_bank_detail($bank);
		$this->template_lite->assign('listing', $listing);
		$this->template_lite->assign('land_area', $land_area);							
		//$this->template_lite->assign('price', $price);
		$this->template_lite->assign('bank', $bank);
		$this->template_lite->assign('propertiID', $propertiID);
		$this->template_lite->assign('bank_detail', $bank_detail);
		$promosi = $operation_type = $this->Listings_model->list_promosi();
		$this->template_lite->assign('promosi', $promosi);
		$pinjaman = $operation_type = $this->Listings_model->list_pinjaman();
		$this->template_lite->assign('pinjaman', $pinjaman);
		$properti = $operation_type = $this->Listings_model->list_properti();
		$this->template_lite->assign('properti', $properti);
		return $this->template_lite->view('kpr_bank');
	}
	public function get_bank_detail($bank)
	{
		mysql_connect("localhost","root","");
		mysql_select_db("ipdb");
		$sql=mysql_query('
			SELECT * FROM kpr_bank
			WHERE kpr_bank.name LIKE "'.$bank.'" 
		');
		return $row=mysql_fetch_array($sql);
	}
	public function get_bank()
	{
		mysql_connect("localhost","root","");
		mysql_select_db("ipdb");
		$promosi = $_POST['promosiID'];
		$properti = $_POST['propertiID'];
		$bank = $_POST['bank'];
		if ($bank!='')
		{
			$sql=mysql_query('
			SELECT * FROM kpr_bungabank
			JOIN kpr_bank on kpr_bank.bankID=kpr_bungabank.bankID
			JOIN kpr_properti on kpr_properti.propertiID=kpr_bungabank.propertiID
			JOIN kpr_promosi on kpr_promosi.promosiID=kpr_bungabank.promosiID
			WHERE kpr_bank.name LIKE "'.$bank.'" AND kpr_bungabank.propertiID="'.$properti.'"
			');
		}
		else
		{
			$sql=mysql_query('
			SELECT * FROM kpr_bungabank
			JOIN kpr_bank on kpr_bank.bankID=kpr_bungabank.bankID
			JOIN kpr_properti on kpr_properti.propertiID=kpr_bungabank.propertiID
			JOIN kpr_promosi on kpr_promosi.promosiID=kpr_bungabank.promosiID
			WHERE kpr_bungabank.promosiID="'.$promosi.'" AND kpr_bungabank.propertiID="'.$properti.'"
			');
		}
		$i=0;
		while($row=mysql_fetch_array($sql)){	
			 $rows[$i]['name'] = $row['name'];
			 $rows[$i]['properti'] = $row['namaproperti'];
			 $rows[$i]['bunga'] = $row['bunga'];
			 $rows[$i]['periode'] = $row['periode'];
			 $rows[$i]['logo'] = $row['logo'];
			 $i++;
		}
		
		echo json_encode($rows);
	}
	#end of kpr mod#

	/**
	 * Render listings results action
	 * @param string $operation_type operation type
	 * @param string $order order field
	 * @param string $order_direction order direction
	 * @param integer $page page of results
	 */
	 
	private function _list($operation_type=null, $order='default', $order_direction='DESC', $page=1){
		#mod for kpr button#
		$kpr_button = $this->Listings_model->get_active_kpr_button();
		$this->template_lite->assign('kpr_button', $kpr_button);
		#end of kpr button mod#

		$operation_types = $this->Listings_model->get_operation_types(true);
		$this->template_lite->assign('operation_types', $operation_types);
		
		//operation type
		if(!$operation_type || !in_array($operation_type, $operation_types)) 
			$operation_type = $this->Listings_model->get_operation_type_gid_default(true);
		$this->template_lite->assign('current_operation_type', $operation_type);
		
		$block = $this->_list_block($operation_type, $order, $order_direction, $page);
		$this->template_lite->assign('block', $block);
	
		if($this->session->userdata('auth_type') != 'user'){
			$this->template_lite->assign('is_guest', true);			
		}else{
			$user_type = $this->session->userdata('user_type');
			$this->Menu_model->breadcrumbs_set_parent($user_type.'-main-find-item');
			$this->Menu_model->set_menu_active_item($user_type.'_main_menu', $user_type.'-main-find-item');
		}
		
		$use_rss = $this->pg_module->get_module_config('listings', 'use_rss');
		$this->template_lite->assign('use_rss', $use_rss);
		
		$use_poll_in_search = $this->pg_module->get_module_config('listings', 'use_poll_in_search');
		$this->template_lite->assign('use_poll_in_search', $use_poll_in_search);
		
		$this->load->helper('seo');
		
		$search_action_data = $menu_action_data = $this->Listings_model->get_search_link_data();
		$method = isset($search_action_data['keyword']) && !empty($search_action_data['keyword']) ? 'search' : 'index';
		$search_link = rewrite_link('listings', $method, $search_action_data);
		$this->pg_seo->set_seo_data($search_action_data);
		
		$menu_action_data['operation_type'] = '[operation_type]';
		$menu_action_link = rewrite_link('listings', $method, $menu_action_data);
		$this->template_lite->assign('menu_action_link', $menu_action_link);
		
		$this->Menu_model->breadcrumbs_set_active(l('link_search_in_listings', 'listings'), $search_link);	
		$this->Menu_model->breadcrumbs_set_active(l('link_search_results', 'listings'));
	
		$view_mode = isset($_SESSION['listings_view_mode']) ? $_SESSION['listings_view_mode'] : 'list';
	
		$this->template_lite->view('listings_'.$view_mode, 'user', 'listings');
	}
	
	/**
	 * Render listings results action
	 * @param string $operation_type operation type
	 * @param string $order order field
	 * @param string $order_direction order direction
	 * @param integer $page page of results
	 */
	private function _list_block($operation_type, $order='default', $order_direction='DESC', $page=1){
		#mod for kpr button#
		$kpr_button = $this->Listings_model->get_active_kpr_button();
		$this->template_lite->assign('kpr_button', $kpr_button);
		#end of kpr button mod#
		$filters = isset($_SESSION['listings_search']['data']) ? $_SESSION['listings_search']['data'] : array();
		$filters['active'] = '1';
		
		// #MOD#	
		$result['count'] = $this->Listings_model->get_listings_count($filters);
		
		if($result['count'] != 0){
			
			$current_settings = isset($_SESSION['listings_search']) ? $_SESSION['listings_search'] : array();	
			
		}else{
		
			$word1 = "Hasil tidak ditemukan, rekomendasi properti:";
			$this->template_lite->assign('msg_word_1', $word1);
			
		}
		// #MOD#

		if(!isset($current_settings['data'])) $current_settings['data'] = array();
		if(!isset($current_settings['order'])) $current_settings['order'] = 'default';
		if(!isset($current_settings['order_direction'])) $current_settings['order_direction'] = 'DESC';
		if(!isset($current_settings['page'])) $current_settings['page'] = '1';

		if($current_settings['data']['type'] != $operation_type){
			$current_settings['data']['type'] = $operation_type;
			$current_settings['page'] = 1;
			
			//booking
			if(!in_array($operation_type, array('rent', 'lease'))){
				if($current_settings['data']['booking_date_start']) unset($current_settings['data']['booking_date_start']);
				if($current_settings['data']['booking_date_end']) unset($current_settings['data']['booking_date_end']);
				if($current_settings['data']['booking_date_guests']) unset($current_settings['data']['booking_date_guests']);
			}
		}
		
		$filters = $current_settings['data'];
		$filters['active'] = '1';
		$filters['not_user'] = 27;
	
		// *************************************************************
		// Disable the lines IF below to allow 
		// Logged in user to see his/her listing on the search result 
		// *************************************************************
		//if($this->session->userdata('auth_type') == 'user'){
		//	$filters['not_user'] = $this->session->userdata('user_id');
		//}
		
		$order = trim(strip_tags($order));
		if(!empty($order)) $current_settings['order'] = $order;
		$order = $current_settings['order'];
		$this->template_lite->assign('order', $order);
	
		$order_direction = strtoupper(trim(strip_tags($order_direction)));
		if(!empty($order_direction)) $current_settings['order_direction'] = $order_direction;
		$order_direction = $current_settings['order_direction'];
		$this->template_lite->assign('order_direction', $order_direction);
		
		$page = intval($page);
		if(!empty($page)) $current_settings['page'] = $page;
		$page = $current_settings['page'];
		$this->template_lite->assign('page', $page);
		
		if($order_direction != 'ASC') $order_direction = 'DESC';
		
		$view_mode = isset($_SESSION['listings_view_mode']) ? $_SESSION['listings_view_mode'] : 'list';
		
		$items_on_page = $this->pg_module->get_module_config('start', 'index_items_per_page');
		//$items_on_page = $this->pg_module->get_module_config('0', '100');
		
		$items_count = $this->Listings_model->get_listings_count($filters);
		
		$this->load->helper('sort_order');

		$current_settings['page'] = get_exists_page_number($page, $items_count, $items_on_page);
	
		$_SESSION['listings_pagination'] = array(
			'order' => $order,
			'order_direction' => $order_direction,
			'page' => $page,
			'data' => $filters,
		);
		
		$_SESSION['listings_search'] = $current_settings;
	
		if(!empty($filters['country'])){
			if(!empty($filters['region'])){
				if(!empty($filters['city'])){
					$this->template_lite->assign('location_filter', 'city');
				}else{
					$this->template_lite->assign('location_filter', 'region');
				}
			}else{
				$this->template_lite->assign('location_filter', 'country');
			}
		}
		
		$this->load->helper('seo');
		
		$search_action_data = $menu_action_data = $this->Listings_model->get_search_link_data();
		$method = isset($search_action_data['keyword']) && !empty($search_action_data['keyword']) ? 'search' : 'index';
		$url = rewrite_link('listings', $method, $search_action_data);

		// sorting
		$sort_data = array(
			'url' => $url,
			'order' => $current_settings['order'],
			'direction' => $current_settings['order_direction'],
			'links' => array('default'=>l('field_default_sorter', 'listings')),
		);
		if($this->pg_module->is_module_installed('reviews')){
			$this->load->helper('reviews');
			$review_link = get_reviews_sorter();
			if(is_array($review_link)){
				$sort_data['links'] = array_merge($sort_data['links'], $review_link);
			}
		}
		$sort_data['links']['views'] = l('field_views', 'listings');
		$sort_data['links']['price'] = l('field_price', 'listings');
		$sort_data['links']['open'] = l('field_date_open', 'listings');
		$sort_data['links']['modified'] = l('field_date_modified', 'listings');
		$this->template_lite->assign('sort_data', $sort_data);

		$use_map_in_search = true;
		if($view_mode == 'list'){
			$use_map_in_search = $this->pg_module->get_module_config('listings', 'use_map_in_search');
		}
		$this->template_lite->assign('use_map_in_search', $use_map_in_search);

		$use_leader = false;
		if($items_count > 0){
			$order_array = array();
			switch($order){
				case 'default':
					if(isset($filters['city']) && !empty($filters['city']))
						$order_array['lift_up_city_date_end'] = $order_direction;
					if(isset($filters['region']) && !empty($filters['region']))
						$order_array['lift_up_region_date_end'] = $order_direction;
					if(isset($filters['country']) && !empty($filters['country'])) 
						$order_array['lift_up_country_date_end'] = $order_direction;
					$order_array['lift_up_date_end'] = $order_direction;
					$order_array['date_modified'] = $order_direction;
					$use_leader = true;
				break;
				case 'price':
					if(isset($filters['price_range'])) 
						$order_array['price_negotiated'] = $order_direction == 'ASC' ? 'DESC' : 'ASC';
					$order_array['price_sorting'] = $order_direction;
				break;
				case 'open':
					$order_array['date_open'] = $order_direction;
				break;
				case 'modified':
					$order_array['date_modified'] = $order_direction;
				break;
				default:
					$order_array[$order] = $order_direction;
				break;
			}
			
			$this->Listings_model->set_format_settings('get_saved_listings', true);
			$listings = $this->Listings_model->get_listings_list($filters, $current_settings['page'], $items_on_page, $order_array);
			#MOD#
			$residen = $this->Listings_model->get_residensial();
			$commer = $this->Listings_model->get_commercial();
			#MOD#
			
			$this->Listings_model->set_format_settings('get_saved_listings', false);
			$this->template_lite->assign('listings', $listings);
			
			#MOD#
			$this->template_lite->assign('residential', $residen);
			$this->template_lite->assign('commercial', $commer);
			#MOD#
			
			if($this->pg_module->is_module_installed('users_services')){
				$this->load->model('Users_services_model');
				$show_logo = !$this->Users_services_model->is_active_show_logo_services($listing['user_type']);
				$this->template_lite->assign('show_logo', $show_logo);
			}else{
				$this->template_lite->assign('show_logo', true);
			}
			
			if($use_map_in_search){
				$markers = array();
				foreach($listings as $listing){
					if((float)$listing['lat']==0 && (float)$listing['lon']==0) continue;
					$this->template_lite->assign('view_mode', $view_mode);
					$this->template_lite->assign('listing', $listing);
					$info = $this->template_lite->fetch('listing_map_block', 'user', 'listings');
					$markers[] = array( 
						'gid' => $listing['id'], 
						'country' => $listing['country'],
						'region' => $listing['region'],
						'city' => $listing['city'],
						'address' => $listing['address'],
						'postal_code' => $listing['zip'],
						'lat' => (float)$listing['lat'], 
						'lon' => (float)$listing['lon'], 
						'info' => $info,
					);
				}
				$this->template_lite->assign('markers', $markers);
			}
		}
		
		if($use_map_in_search){
			$current_language = $this->pg_language->get_lang_by_id($this->pg_language->current_lang_id);
			$view_settings = array('lang' => $current_language['code']);			
			$this->template_lite->assign('map_settings', $view_settings);
		}
	
		$current_search_data = $this->Listings_model->get_search_data();	
		$this->template_lite->assign('current_search_data', $current_search_data);
		
		$search_filters_block = $this->template_lite->fetch('search_filters_block', 'user', 'listings');
		$this->template_lite->assign('search_filters_block', addslashes($search_filters_block));
		
		$this->load->helper('navigation');
		$this->config->load('date_formats', TRUE);
		$page_data = get_user_pages_data($url.'/'.$order.'/'.$order_direction.'/', $items_count, $items_on_page, $current_settings['page'], 'briefPage');
		$page_data['date_format'] = $this->config->item('st_format_date_literal', 'date_formats');
		$page_data['date_time_format'] = $this->config->item('st_format_date_time_literal', 'date_formats');
		$page_data['use_leader'] = $use_leader;
		$this->template_lite->assign('page_data', $page_data);

		$this->template_lite->assign('tstamp', time());
		
		$this->template_lite->assign('show_user_info', 1);
		$this->template_lite->assign('animate_available', 1);
		return $this->template_lite->fetch('listings_block_'.$view_mode);
	
	}
	
	/**
	 * Render listings list action
	 * @param string $operation_type operation type guid
	 * @param integer $category_id category identifier
	 * @param integer $property_type property type
	 * @param string $order
	 * @param string $order_direction
	 * @param integer $page
	 */
	public function index($operation_type=null, $order='default', $order_direction='DESC', $page=1){
		$this->_list($operation_type, $order, $order_direction, $page);
		$this->config->load('date_formats', TRUE);
	}
	
	/**
	 * Return listings list by ajax
	 * @param string $operaton_type operation type
	 * @param string $order sorting order
	 * @param string $order_direction order direction
	 * @param integer $page page of results
	 */
	public function ajax_index($operation_type, $order=null, $order_direction=null, $page=null){
		$view_mode = isset($_SESSION['listings_view_mode']) ? $_SESSION['listings_view_mode'] : 'list';
		$use_map_in_search = true;
		if($view_mode == 'list'){
			$use_map_in_search = $this->pg_module->get_module_config('listings', 'use_map_in_search');
		}
		$this->template_lite->assign('update_map', $use_map_in_search);	
		$this->template_lite->assign('update_operation_type', $operation_type);		
		$this->template_lite->assign('update_search_block', 1);
		$this->template_lite->assign('update_rss_link', 1);
		
		echo $this->_list_block($operation_type, $order, $order_direction, $page);
	}
	
	/**
	 * Render listings list by cagoryte action
	 * @param string $category_id category identifier
	 * @param integer $property_type property type
	 * @param string $operation_type operation type
	 * @param string $order sorting order 
	 * @param string $order_direction order direction
	 * @param integer $page page of results
	 */
	public function category($category_id, $property_type=null, $operation_type, $order='default', $order_direction='DESC', $page=1){
		$_SESSION['listings_search']['data'] = array('id_category'=>$category_id);
		if($property_type) $_SESSION['listings_search']['data']['property_type'] = $property_type;
		$this->_list($operation_type, $order, $order_direction, $page);
	}
	
	/**
	 * Render listings list by open houses action
	 * @param string $operation_type operation type
	 * @param string $order sorting order
	 * @param string $order_direction order direction
	 * @param integer $page page of results
	 */
	public function open_house($operation_type=null, $order='default', $order_direction='DESC', $page=1){
		$_SESSION['listings_search']['data'] = array('by_open_house'=>'1');
		$this->_list($operation_type, $order, $order_direction, $page);
	}
	
	/**
	 * Render listings by open private persons list action
	 * @param string $operation_type operation type
	 * @param string $order sorting order
	 * @param string $order_direction order direction
	 * @param integer $page page of results
	 */
	public function privates($operation_type=null, $order='default', $order_direction='DESC', $page=1){
		$_SESSION['listings_search']['data'] = array('by_private'=>'1');
		$this->_list($operation_type, $order, $order_direction, $page);
	}
	
	/**
	 * Render listings list by agents (agency + agent) action
	 * @param string $operation_type operation type
	 * @param string $order sorting order
	 * @param string $order_direction order direction
	 * @param integer $page page of results
	 */
	public function agents($operation_type=null, $order='default', $order_direction='DESC', $page=1){
		$_SESSION['listings_search']['data'] = array('by_agents'=>'1');
		$this->_list($operation_type, $order, $order_direction, $page);
	}
	
	/**
	 * Render listings list by location action
	 * @param string $operation_type operation type
	 * @param string $country country code
	 * @param integer $region region identifier
	 * @param integer $city city identifier
	 * @param string $order sorting order 
	 * @param string $order_direction order direction
	 * @param integer $page page of results
	 */
	public function location($country, $region=null, $city=null, $operation_type=null, $order='default', $order_direction='DESC', $page=1){
		$_SESSION['listings_search']['data'] = array('id_country'=>$country);
		if($region) $_SESSION['listings_search']['data']['id_region'] = $region;
		if($city) $_SESSION['listings_search']['data']['id_city'] = $city;
		$this->_list($operation_type, $order, $order_direction, $page);
	}
	
	/**
	 * Render listings list by discounts action
	 * @param string $operation_type operation type
	 * @param string $order sorting order
	 * @param string $order_direction order direction
	 * @param integer $page page of results
	 */
	public function discount($operation_type=null, $order='default', $order_direction='DESC', $page=1){
		$_SESSION['listings_search']['data'] = array('by_discount'=>'1');
		$this->_list($operation_type, $order, $order_direction, $page);
	}
	
	/**
	 * Render view listing action
	 * @param integer $listing_id listing identifier
	 * @param string $section_gid section GUID
	 * @param boolean $pdf pdf-version
	 */
	public function view($listing_id, $section_gid='overview', $pdf=false){
		if(!$listing_id){show_404();return;}

		$this->Listings_model->set_format_settings(array(
			'get_user' => false,
			'get_saved_listings' => true,
		));
		
		if($pdf){
			$ajax_sections = array('print'=>1);
			$this->Listings_model->set_format_settings(array(
				'get_description' => true,
				'get_qr_code' => true, 
				'get_photos' => true, 
				'get_booking' => true,
			));	
		}else{
			$ajax_sections = array('gallery'=>0, 'file'=>0, 'video'=>0, 'overview'=>0, 'calendar'=>0);

			$map_only_load_scripts = 1;
			$panorama_only_load_scripts = 1;
			
			switch($section_gid){
				case 'gallery':
					$this->Listings_model->set_format_settings('get_photos', true);
					$ajax_sections['gallery'] = 1;
				break;
				case 'virtual_tour':
					$this->Listings_model->set_format_settings('get_virtual_tours', true);
					$ajax_sections['virtual_tour'] = 1;
				break;
				case 'file':
					$this->Listings_model->set_format_settings('get_file', true);
					$ajax_sections['file'] = 1;
				break;
				case 'video':
					$this->Listings_model->set_format_settings('get_video', true);
					$ajax_sections['video'] = 1;
				break;
				case 'map':
					$map_only_load_scripts = 0;
				break;
				case 'panorama':
					$panorama_only_load_scripts = 0;
				break;
				case 'overview':
					$this->Listings_model->set_format_settings(array('get_description'=>true, 'get_qr_code'=>true));
					$ajax_sections['overview'] = 1;
				break;
				case 'calendar':
					$this->Listings_model->set_format_settings('get_booking', true);
					$ajax_sections['calendar'] = 1;
				break;
			}
		
			if(!array_key_exists($section_gid, $this->Listings_model->display_sections['top'])){
				$this->Listings_model->set_format_settings('get_photos', true);
				$ajax_sections['gallery'] = 1;
			}
		
			if(!array_key_exists($section_gid, $this->Listings_model->display_sections['bottom'])){
				$this->Listings_model->set_format_settings(array('get_description'=>true, 'get_qr_code'=>true));
				$ajax_sections['overview'] = 1;
			}
		
			$this->template_lite->assign('map_only_load_scripts', $map_only_load_scripts);
			$this->template_lite->assign('panorama_only_load_scripts', $panorama_only_load_scripts);
		}
		
		$listing = $this->Listings_model->get_listing_by_id($listing_id);	
		$this->Listings_model->set_format_settings(array(
			'get_user' => true,
			'get_description' => false,
			'get_qr_code' => false, 
			'get_photos' => false, 
			'get_virtual_tours' => false,
			'get_saved_listings' => false,
			'get_file' => false,
			'get_video' => false,
			'get_booking' => false,
		));	
		if(!$listing){show_404();return;}
		
		$this->load->model('Users_model');
		$this->Users_model->set_format_settings(array('get_contact'=>true, 'get_company'=>true));
		$listing['user'] = $this->Users_model->get_user_by_id($listing['id_user']);
		$this->Users_model->set_format_settings(array('get_contact'=>false, 'get_company'=>false));
		
		$this->template_lite->assign('listing', $listing);
	
		/*if($this->session->userdata('auth_type') != 'user' ||
		   $this->session->userdata('user_id') != $listing['id_user']){
			if(!$listing['status']){show_404(); return;}
		}*/
		
		$is_owner = 0;
		if($this->session->userdata('auth_type') == 'user'){
			$user_id = $this->session->userdata('user_id');
			$this->template_lite->assign('user_id', $user_id);
			if($user_id == $listing['id_user']) $is_owner = 1;
		}
		$this->template_lite->assign('is_listing_owner', $is_owner);
	
		$display_sections = $this->Listings_model->get_display_sections($listing);
		#MOD#
		#check if there is a bank or not#
		$bank_check = $this->Listings_model->list_bank();
		#flag kpr to turn off if no bank is listed in the database
		if(empty($bank_check))$display_sections['bottom']['kpr']=0;
		#END OF MOD#
		foreach($display_sections as $sections){
			foreach($sections as $display_section_gid=>$used){
				if(isset($ajax_sections[$display_section_gid])) $used = $ajax_sections[$display_section_gid];
				if(!$used) continue;
				ob_start();
				$this->_get_section_html($listing, $display_section_gid);
				$listing_content[$display_section_gid] = ob_get_contents();
				ob_end_clean();
			}
		}
	
	
		/*#MOD#
		$residen = $this->Listings_model->get_residensial();
		$commer = $this->Listings_model->get_commercial();
		#MOD#*/
		
		$this->Listings_model->set_format_settings('get_saved_listings', false);
		$this->template_lite->assign('listings', $listings);
		
		/*#MOD#
		$this->template_lite->assign('residential', $residen);
		$this->template_lite->assign('commercial', $commer);
		#MOD#*/
	
	
		$this->template_lite->assign('display_sections', $display_sections);
		$this->template_lite->assign('listing_content', $listing_content);	
		$this->template_lite->assign('section_gid', $section_gid);

		$slider_auto =  $this->pg_module->get_module_config('listings', 'slider_auto');
		$slider_rotation =  $this->pg_module->get_module_config('listings', 'slider_rotation');	
		
		$this->config->load('date_formats', true);	
		$page_data = array(
			'slider_auto' => $slider_auto,
			'rotation' => $rotation,
			'date_format' => $this->config->item('st_format_date_literal', 'date_formats'),
		);
		$this->template_lite->assign('page_data', $page_data);
			
		$is_guest = $this->session->userdata('auth_type') != 'user';
		$this->template_lite->assign('is_guest', $is_guest);

		//search seo
		$this->load->helper('seo');
		$search_action_data = $this->Listings_model->get_search_link_data();
		$search_link = rewrite_link('listings', $search_action_data['keyword'] ? 'search' : 'index', $search_action_data);
		
		$this->Menu_model->breadcrumbs_set_active(l('link_search_in_listings', 'listings'), $search_link);	
		$this->Menu_model->breadcrumbs_set_active($listing['output_name'], site_url().'listings/view/'.$listing_id);
	
		$this->Listings_model->check_views_count($listing['id_user'], $listing_id);

		if($pdf){
			$this->pg_theme->print_type = 'pdf';
			$tpl_output = $this->template_lite->fetch('view_listing');
			$this->Listings_model->get_pdf_file($tpl_output, $listing);
		}else{
			$this->pg_seo->set_seo_data($listing);

			$this->template_lite->view('view_listing', 'user', 'listings');
		}
	}
	
	/**
	 * Render view section
	 * @param array $data listing data
	 * @param string $section_gid section GUID
	 */
	private function _get_section_html($data, $section_gid='overview'){
		$this->template_lite->assign('section_gid', $section_gid);
		$this->template_lite->assign('data', $data);

		switch($section_gid){
			#mod for kpr#
			case 'kpr':
				#mod to add kpr page;
				#$kpr = $this->kpr($data['price'], $data['property']);
				$kpr = $this->kpr($data);
				$this->template_lite->assign('kpr', $kpr);
				#end of kpr page mod			
			#end of kpr mod
//			break;
			case 'overview':
				/*$this->load->model('Field_editor_model');
				$property_type_gid = $data['field_editor_type'];
				$this->Field_editor_model->initialize($property_type_gid);
				$property_type_sections = $this->Field_editor_model->get_section_list();
				$this->template_lite->assign('sections_data', array_values($property_type_sections));*/
			break;	
			case 'gallery':
			case 'virtual_tour':
				$page_data = array(
					'rand' => rand(100000, 999999),
					'visible' => 7,
					'date_format' => $this->config->item('st_format_date_literal', 'date_formats')
				);
				$this->template_lite->assign('page_data', $page_data);
			break;
			case 'map':
			case 'panorama':
				$markers = array(
					array( 
						'gid'			=> $data['id'],
						'country' 		=> $data['country'], 
						'region' 		=> $data['region'], 
						'city' 			=> $data['city'], 
						'address' 		=> $data['address'], 
						'postal_code' 	=> $data['zip'], 
						'lat' 			=> (float)$data['lat'], 
						'lon' 			=> (float)$data['lon'], 
						'info' 			=> ($data['location'] ? $data['location'] : ''),
						'dragging' 		=> false,
					),
				);
				$this->template_lite->assign('markers', $markers);
		
				$current_language = $this->pg_language->get_lang_by_id($this->pg_language->current_lang_id);
				$view_settings = array(
					'width' => '630',
					'height' => '250', 
					'class' => ($section_gid == 'panorama' ? 'hide' : ''), 
					'lang' => $current_language['code'],
					'panorama_disable' => ($section_gid == 'panorama' ? false : true),
				);
				$this->template_lite->assign('map_settings', $view_settings);				
			break;
		}

		$this->config->load('date_formats', TRUE);
		$date_format = $this->config->item('st_format_date_literal', 'date_formats');
		$this->template_lite->assign('date_format', $date_format);
		
		$this->template_lite->view('view_listing_content');
	}
	
	/**
	 * Return section content
	 * @param integer $listing_id listing identifier
	 * @param string $section_gid section GUID
	 */
	public function ajax_get_section($listing_id, $section_gid){

		$this->load->model('Field_editor_model');
		
		switch($section_gid){
			case 'overview':
				$this->Listings_model->set_format_settings(array('get_description'=>true, 'get_qr_code'=>true));
			break;
			case 'gallery':
				$this->Listings_model->set_format_settings('get_photos', true);
			break;
			case 'virtual_tour':
				$this->Listings_model->set_format_settings('get_virtual_tours', true);
			break;
			case 'map':
				$this->template_lite->assign('map_only_load_content', 1);
			break;
			case 'panorama':
				$this->template_lite->assign('panorama_only_load_content', 1);
			break;
			case 'file':
				$this->Listings_model->set_format_settings('get_file', true);
			break;
			case 'video':
				$this->Listings_model->set_format_settings('get_video', true);
			break;
			case 'calendar':
				$this->Listings_model->set_format_settings('get_booking', true);
			break;
			case 'print':
				$this->Listings_model->set_format_settings('get_photos', true);
			break;
		}
		
		$this->template_lite->assign('ajax', 1);
		
		$data = $this->Listings_model->get_listing_by_id($listing_id);
		$this->_get_section_html($data, $section_gid);
	}
	
	/**
	 * Return pdf version
	 * @param integer $listing_id listing identifier
	 * @return string
	 */
	public function pdf($listing_id){
		return $this->view($listing_id, 'overview', true);
	}
	
	/**
	 * Return similar listings
	 * @param integer $listing_id listing identifier
	 */
	public function similar_listing($listing_id){
		if($listing_id){
			$this->Listings_model->set_format_settings('use_format', false);
			$data = $this->Listings_model->get_listing_by_id($listing_id);
			$this->Listings_model->set_format_settings('use_format', true);
			$operation_type = $this->Listings_model->get_operation_type_by_id($data['id_type']);
			if($operation_type == 'sale' && $data['sold']) $operation_type = 'sold';
			$_SESSION['listings_search']['data'] = array();
			$_SESSION['listings_search']['data']['type'] = $operation_type;
			$_SESSION['listings_search']['data']['id_country'] = $data['id_country'];
			$_SESSION['listings_search']['data']['id_region'] = $data['id_region'];
			$_SESSION['listings_search']['data']['id_city'] = $data['id_city'];
			$_SESSION['listings_search']['data']['id_category'] = $data['id_category'];
			$_SESSION['listings_search']['data']['property_type'] = $data['property_type'];
			$_SESSION['listings_search']['data']['exclude_id'] = $listing_id;
		}
		$this->search();
	}
	
	/**
	 * Return listings block
	 * @param string $operation_type opearation type
	 * @param array $data filters data
	 * @param string $order sorting order 
	 * @param string $order_direction order direction
	 * @param integer $page page of results
	 * @param string $prefix template prefix
	 */
	private function _my_listings_block($operation_type, $data=array(), $order='modified', $order_direction='DESC', $page=1){
		$current_settings = isset($_SESSION['my_listings'])?$_SESSION['my_listings']:array();
		if(!isset($current_settings['order']))
			$current_settings['order'] = 'modified';
		if(!isset($current_settings['order_direction']))
			$current_settings['order_direction'] = 'DESC';
		if(!isset($current_settings['page']))
			$current_settings['page'] = 1;
		if(!isset($current_settings['filters']))
			$current_settings['filters'] = array();

		if(!empty($data)){
			$current_settings['filters'] = $data;
		}elseif($current_settings['filters']['type'] != $operation_type){
			$current_settings['filters']['type'] = $operation_type;
			$page = 1;
		}
		$filters = $current_settings['filters'];
		
		$filters['user'] = $this->session->userdata('user_id');
		
		if(isset($filters['keyword'])){
			$filters['keyword_mode'] = 1;
		}
		
		if(!empty($order)) $current_settings['order'] = $order;
		$order = $current_settings['order'];
		$this->template_lite->assign('order', $current_settings['order']);
		
		if(!empty($order_direction)) $current_settings['order_direction'] = $order_direction;
		$order_direction = $current_settings['order_direction'];
		$this->template_lite->assign('order_direction', $order_direction);
		
		$page = intval($page);
		if(!empty($page)) $current_settings['page'] = $page;
		$page = $current_settings['page'];
		
		$items_on_page = $this->pg_module->get_module_config('start', 'index_items_per_page');
	
	
		$sort_data = array(
			'url' => site_url().'listings/my/'.$filters['operation_type'],
			'order' => $order,
			'direction' => $order_direction,
			'links' => array(
				'views' => l('field_views', 'listings'),
				'price' => l('field_price', 'listings'),
				'open' => l('field_date_open', 'listings'),
				'modified' => l('field_date_modified', 'listings'),
			)
		);
		if($this->pg_module->is_module_installed('reviews')){
			$this->load->helper('reviews');
			$review_link = get_reviews_sorter();
			if(is_array($review_link)){
				$first = array_splice($sort_data['links'], 0, 1);
				$sort_data['links'] = array_merge($first, $review_link, $sort_data['links']);
			}
		}
		$this->template_lite->assign('sort_data', $sort_data);
	
		$listings_count = $this->Listings_model->get_listings_count($filters);
		if($listings_count > 0){
			$order_array = array();
			if($order == 'price'){
				$order_array['price_sorting'] = $order_direction;
			}elseif($order == 'open'){
				$order_array['date_open'] = $order_direction;
			}elseif($order == 'modified'){
				$order_array['date_modified'] = $order_direction;
			}else{
				$order_array[$order] = $order_direction;
			}
			if($order == 'price' || isset($filters['price_range'])){
				$price_order_array = array('price_negotiated'=> $order_direction == 'ASC' ? 'DESC' : 'ASC');
				$order_array = array_merge($price_order_array, $order_array);
			}
			$this->Listings_model->set_format_settings('get_moderation', true);
			$listings = $this->Listings_model->get_listings_list($filters, $page, $items_on_page, $order_array);
			$this->Listings_model->set_format_settings('get_moderation', false);
			
			$this->template_lite->assign('listings', $listings);		
		}
	
		$this->load->helper('sort_order');
		$page = get_exists_page_number($page, $listings_count, $items_on_page);
		$current_settings['page'] = $page;
		
		$this->load->helper('users');
		$this->template_lite->assign('is_login_enabled', is_login_enabled());

		$this->load->helper('navigation');
		$this->config->load('date_formats', TRUE);
		$url = site_url().'listings/my/'.$operation_type.'/'.$order.'/'.$order_direction;
		$page_data = get_user_pages_data($url, $listings_count, $items_on_page, $page, 'briefPage');
		$page_data['date_format'] = $this->config->item('st_format_date_time_literal', 'date_formats');
		foreach($filters as $filter_name=>$filter_data){
			$page_data[$filter_name] = $filter_data;
		}
		$this->template_lite->assign('page_data', $page_data);
		
		$_SESSION['my_listings'] = $current_settings;
		
		$this->template_lite->assign('tstamp', time());
		
		return $this->template_lite->fetch('my_block_list', 'user', 'listings');
	}
	
	/**
	 * Render list of my listings
	 * @param array $operation_type operation type
	 * @param string $order sorting order
	 * @param string $order_direction sorting direction
	 * @param integer $page page of results
	 */
	public function my($operation_type=null, $order='date_modified', $order_direction='DESC', $page=1){
		$data = $this->input->post('data', true);
		
		$operation_types = $this->Listings_model->get_operation_types(true);
		$this->template_lite->assign('operation_types', $operation_types);
	
		//operation type
		if(isset($data['type']) && in_array($data['type'], $operation_types)){
			$operation_type = $data['type'];
		}elseif($operation_type && in_array($operation_type, $operation_types)){
			$data['type'] = $operation_type;
		}else{
			$operation_type = $data['type'] = current($operation_types);
		}
		$this->template_lite->assign('current_operation_type', $operation_type);
		
		$block_content = $this->_my_listings_block($operation_type, $data, $order, $order_direction, $page, $prefix);
		$this->template_lite->assign('block', $block_content);
		
		if($this->session->userdata('auth_type') != 'user'){
			$this->template_lite->assign('is_guest', 1);
		}
		
		$user_id = $this->session->userdata('user_id');
		$user_type = $this->session->userdata('user_type');
		
		$listings_count = array();
		foreach($operation_types as $gid){
			$listings_count[$gid] = $this->Listings_model->get_listings_count(array('type'=>$gid, 'user'=>$user_id));
		}
		$this->template_lite->assign('listings_count', $listings_count);
		$this->template_lite->assign('listings_count_sum', array_sum($listings_count));
		
		$this->template_lite->assign('noshow_add_button' , $this->Listings_model->is_operation_types_disabled());
		
		$this->Menu_model->breadcrumbs_set_parent($user_type.'_my_listings_item');
		
		$current_settings = isset($_SESSION['my_listings'])?$_SESSION['my_listings']:array();
		if(!isset($current_settings['order'])) $current_settings['order'] = 'date_created';
		$this->template_lite->assign('order', $current_settings['order']);
		
		if(!isset($current_settings['order_direction'])) $current_settings['order_direction'] = 'DESC';
		$this->template_lite->assign('order_direction', $current_settings['order_direction']);
		
		if(!isset($current_settings['page'])) $current_settings['page'] = 1;
		$this->template_lite->assign('page', $current_settings['page']);
		
				$page_data['date_format'] = $this->config->item('st_format_date_literal', 'date_formats');
		$page_data['date_time_format'] = $this->config->item('st_format_date_time_literal', 'date_formats');
		$this->template_lite->assign('page_data', $page_data);

		
		$this->template_lite->view('my_list', 'user', 'listings');
	}
	
	/**
	 * Return list of my listings by ajax
	 * @param string $operation_type operation type
	 * @param string $order sorting order
	 * @param string $order_direction sorting direction
	 * @param integer $page page of results
	 */
	public function ajax_my($operation_type, $order='date_modified', $order_direction='DESC', $page=1){
		$data = $this->input->post('data', true);
		echo $this->_my_listings_block($operation_type, $data, $order, $order_direction, $page);
	}
	
	/**
	 * Render edit listing action
	 * @param integer $listing_id listing identifier
	 * @param string $section_gid editor section guid
	 * @param integer $step processing step
	 */
	public function edit($listing_id=0, $section_gid='overview', $step=1){
		if($this->Listings_model->is_operation_types_disabled() && $listing_id==0) show_404();
		
		$user_id = $this->session->userdata('user_id');
		$user_type = $this->session->userdata('user_type');	

		if(!$step) $step = 1;

		$this->Menu_model->breadcrumbs_set_parent($user_type.'_my_listings_item');

		$format_settings = array('get_moderation'=>true);
		
		switch($section_gid){
			case 'overview':
				$format_settings['get_location'] = true;
			break;
			case 'description':
				$format_settings['get_description'] = true;
			break;
			case 'gallery':
				$format_settings['get_photos_all'] = true;
				$format_settings['get_virtual_tours_all'] = true;
				$format_settings['get_file'] = true;
				$format_settings['get_video'] = true;
			break;
			case 'map':
				$format_settings['get_location'] = true;
			break;
			case 'calendar':
				$format_settings['get_booking'] = true;
			break;
		}
		
		$this->Listings_model->set_format_settings($format_settings);
		
		if($listing_id){
			$data = $this->Listings_model->get_listing_by_id($listing_id);
			if($user_id != $data['id_user']){show_404(); return;}

			$this->load->model('listings/models/Listings_moderation_model');

			$admin_alert = $this->Listings_moderation_model->get_alert($listing_id);
			if(!empty($admin_alert)){
				$moderation_alerts[$admin_alert['status']] =  $admin_alert['content'];
			}

			if($this->Listings_moderation_model->is_listing_moderated($listing_id)){
				$fields_for_select = $this->Listings_moderation_model->get_fields_for_moderate();
				$this->Listings_moderation_model->set_dop_fields($fields_for_select);
				$moderated_data = $this->Listings_moderation_model->get_listing_by_id($listing_id);
				$data = array_merge($data, $moderated_data);
				$moderation_alerts['info'] = l('info_your_listing_on_moderation', 'listings');
			}			
			$this->template_lite->assign('moderation_alerts', $moderation_alerts);

			$this->Menu_model->breadcrumbs_set_active(l('header_listing_edit', 'listings'), site_url().'listings/edit/'.$listing_id);
			$this->Menu_model->breadcrumbs_set_active(l('filter_section_'.$section_gid, 'listings'));
		}else{
			$data = array();
			$this->Menu_model->breadcrumbs_set_active(l('link_add_listing', 'listings'), site_url().'listings/edit');
		}

		if($this->input->post('save_btn') == 1){
			$post_data = $this->input->post('data', true);			
			switch($section_gid){
				case 'overview':
					$post_data['id_country'] = $this->input->post('id_country', true);
					$post_data['id_region'] = $this->input->post('id_region', true);
					$post_data['id_city'] = $this->input->post('id_city', true);	
					$post_data['id_user'] = $user_id;
				
					if(!$listing_id){
						$operation_type = $this->input->post('type', true);
						
						$post_data['id_type'] = $this->Listings_model->get_operation_type_by_gid($operation_type);
						
						if(isset($post_data['category'])){
							$pos = strpos($post_data['category'], '_');
							$post_data['id_category'] = substr($post_data['category'], 0, $pos);
							$post_data['property_type'] = substr($post_data['category'], $pos + 1);			
							unset($post_data['category']);
						}
					}else{
						if(isset($post_data['category'])){
							$pos = strpos($post_data['category'], '_');
							$post_data['property_type'] = substr($post_data['category'], $pos + 1);			
							unset($post_data['category']);
						}
					}
					
					$post_data['date_open_alt'] = $this->input->post('date_open_alt', true);
					if($post_data['date_open_alt'])	$post_data['date_open'] = $post_data['date_open_alt'];
					$post_data['date_available_alt'] = $this->input->post('date_available_alt', true);
					if($post_data['date_available_alt']) $post_data['date_available'] = $post_data['date_available_alt'];
					
					$step = 1;
				break;
				case 'description':
					if(!$listing_id) break;
					
					$this->load->model('Field_editor_model');
						
					$field_editor_section_gid = $this->input->post('field_editor_section_gid', true);
					
					$property_type_gid = $this->Listings_model->get_field_editor_type($data);	
					$this->Field_editor_model->initialize($property_type_gid);
					$fields_data = $this->Field_editor_model->get_fields_for_select($field_editor_section_gid);
					foreach($fields_data as $field){
						$post_data[$field] = $this->input->post($field, true);
					}
					
					$this->load->model('Field_editor_model');
					$this->Field_editor_model->initialize($data['field_editor_type']);
					$property_type_sections = $this->Field_editor_model->get_section_list();
					$property_type_sections_count = max(count($property_type_sections), 1);
					$step = max($step++, $property_type_sections_count);
				break;
				case 'gallery':
					if($this->input->post('btn_save_file')){
						$validate_data = $this->Listings_model->save_file($listing_id, 'listing_file', $post_data, true);
						///// delete listing file
						if($this->input->post('listing_file_delete') && $listing_id && $data['listing_file']){
							$this->load->model('File_uploads_model');
							$this->File_uploads_model->delete_upload($this->Listings_model->file_config_id, $data['prefix'], $data['listing_file']);
							if(!isset($validate_data['data']['file'])){
								$listing_data = array(
									'listing_file' => '',
									'listing_file_name' => '',
									'listing_file_comment' => '',
								);
								$this->Listings_model->save_listing($listing_id, $listing_data, true);
							}
							$this->system_messages->add_message('success', l('success_file_deleted', 'listings'));
						}elseif($validate_data['data']['file']){
							$this->system_messages->add_message('success', l('success_file_uploaded', 'listings'));
						}
						$step = 3;
					}
					if($this->input->post('btn_save_video')){
						$validate_data = $this->Listings_model->save_video($listing_id, 'listing_video', true);
						///// delete video
						if($this->input->post('listing_video_delete') && $listing_id && $data['listing_video']){
							$this->load->model('Video_uploads_model');
							$video_data = isset($data['listing_video_data']['data']['upload_type']) ? $data['listing_video_data']['data']['upload_type'] : array();
							$this->Video_uploads_model->delete_upload($this->Listings_model->video_config_id, $data['prefix'], $data['listing_video'], $data['listing_video_image'], $video_data);
							if(!isset($validate_data['data']['video'])){
								$listing_data = array(
									'listing_video' => '',
									'listing_video_image' => '',
									'listing_video_data' => '',
								);
								$this->Listings_model->save_listing($listing_id, $listing_data, true);
							}
							$this->system_messages->add_message('success', l('success_video_deleted', 'listings'));
						}elseif($validate_data['data']['file']){
							$this->system_messages->add_message('success', l('success_video_uploaded', 'listings'));
						}					
						$step = 4;
					}
				break;
				case 'map':
					$this->load->model('geomap/models/Geomap_model');
					$this->load->model('geomap/models/Geomap_settings_model');
					$post_settings = $this->input->post('map', true);
					$post_settings['lat'] = $post_data['lat'];
					$post_settings['lon'] = $post_data['lon'];
					$map_gid = $this->Geomap_model->get_default_driver_gid();
					$validate_map = $this->Geomap_settings_model->validate_settings($post_settings);
					if(!empty($validate_map['errors'])) break;
					$this->Geomap_settings_model->save_settings($map_gid, 0, $listing_id, 'listing_view', $validate_map['data']);
				break;
			}
			
			if(isset($validate_data)){
				if(!empty($validate_data['errors'])){
					$this->system_messages->add_message('error', $validate_data['errors']);
				}else{
					$url = site_url().'listings/edit/'.$listing_id.'/'.$section_gid.'/'.$step;
					redirect($url);
				}
			}else{	
				$validate_data = $this->Listings_model->validate_listing($listing_id, $post_data);
				if(!empty($validate_data['errors'])){
					$this->system_messages->add_message('error', $validate_data['errors']);
				}else{
					if(!empty($validate_data['data'])){
						if($listing_id){
							$this->Listings_model->save_listing($listing_id, $validate_data['data'], true);
							$this->system_messages->add_message('success', l('success_update_listing', 'listings'));
						}else{
							$listing_id = $this->Listings_model->save_listing(null, $validate_data['data'], true);
							$this->system_messages->add_message('success', l('success_add_listing', 'listings'));
						}
					}
				
					$url = site_url().'listings/edit/'.$listing_id.'/'.$section_gid.'/'.$step;
					redirect($url);
				}
			}
			
			if(!empty($post_data)){
				$this->Listings_model->set_format_settings('use_format', false);
				$data = $this->Listings_model->get_listing_by_id($listing_id);
				$this->Listings_model->set_format_settings('use_format', true);
			
				$post_data = $this->Listings_model->format_listings(array(array_merge($data, $post_data)));
				$data = $post_data[0];
			}
		}

		switch($section_gid){
			case 'overview':
				$operation_types = $this->Listings_model->get_operation_types(false);
				$this->template_lite->assign('operation_types', $operation_types);
				$this->template_lite->assign('operation_types_count', count($operation_types));
				
				if($this->pg_module->is_module_installed('payments')){
					$this->load->model('payments/models/Payment_currency_model');
					$currencies = $this->Payment_currency_model->get_currency_list();
					$this->template_lite->assign('currencies', $currencies);
					
					if(isset($data['gid_currency']) && !empty($data['gid_currency'])){
						$current_price_currency = $this->Payment_currency_model->get_currency_by_gid($data['gid_currency']);
						$this->template_lite->assign('current_price_currency', $current_price_currency);
					}else{
						$current_price_currency = $this->Payment_currency_model->get_currency_default();
						$data['gid_currency'] = $current_price_currency['gid'];
					}
				}
				
				$square_units = $this->pg_language->ds->get_reference( 'listings', $this->Listings_model->square_units_gid, $this->pg_language->current_lang_id);
				$this->template_lite->assign('square_units', $square_units);
				
				$dayhours = $this->pg_language->ds->get_reference('start', 'dayhour-names');
				$this->template_lite->assign('dayhours', $dayhours);
				
				$this->template_lite->assign('current_lang_id', $this->pg_language->current_lang_id);
				$this->template_lite->assign('langs', $this->pg_language->languages);
				
				$this->config->load('date_formats', TRUE);
				$page_data['date_format'] = $this->config->item('st_format_date_literal', 'date_formats');
				$page_data['date_time_format'] = $this->config->item('st_format_date_time_literal', 'date_formats');
				$page_data['datepicker_date_format'] = $this->config->item('ui_format_date_literal', 'date_formats');
				$page_data['datepicker_alt_format'] = $this->config->item('ui_format_date_numeric', 'date_formats');
				$this->template_lite->assign('page_data', $page_data);
			break;
			case 'description':
				$this->load->model('Field_editor_model');
				$this->Field_editor_model->initialize($data['field_editor_type']);
				$property_type_sections = $this->Field_editor_model->get_section_list();
				$sections = array();
				foreach($property_type_sections as $section){
					$sections[] = $section['gid'];
				}
				$this->template_lite->assign('sections_data', array_values($property_type_sections));
			
				$params['where']['editor_type_gid'] = $data['field_editor_type'];
				$params['where_in']['section_gid'] = $sections;
				$fields_data = $this->Field_editor_model->get_form_fields_list($data, $params);
				$this->template_lite->assign('fields_data', $fields_data);
			break;
			case 'gallery':
				// photos
				$gallery_type_config = $this->Listings_model->get_gallery_type();
				
				$this->load->model('Uploads_model');
				$gallery_type_config['upload_settings'] = $this->Uploads_model->get_config($gallery_type_config['gid_upload_config']);
				
				$max_size = $gallery_type_config['upload_settings']['max_size'];
			
				$i = 1;
				while($max_size > 1000){$max_size /= 1024; $i++;}
				$max_size = round($max_size, 1);
				if($max_size){
					$gallery_type_config['upload_settings']['max_size_str'] = 
						$max_size.' '.l('text_filesize_'.$i, 'listings');
				}
				$this->template_lite->assign('gallery_settings', $gallery_type_config);
				
				//virtual tours
				$virtual_tour_config = $this->Listings_model->get_vtour_type();
				
				$this->load->model('Uploads_model');
				$virtual_tour_config['upload_settings'] = $this->Uploads_model->get_config($virtual_tour_config['gid_upload_config']);
				
				$max_size = $virtual_tour_config['upload_settings']['max_size'];
			
				$i = 1;
				while($max_size > 1000){$max_size /= 1024; $i++;}
				$max_size = round($max_size, 1);
				if($max_size){
					$virtual_tour_config['upload_settings']['max_size_str'] = 
						$max_size.' '.l('text_filesize_'.$i, 'listings');
				}
				$this->template_lite->assign('vtour_settings', $virtual_tour_config);
				
				// files
				$file_type_config = $this->Listings_model->get_file_type();
				
				$max_size = $file_type_config['max_size'];
			
				$i = 1;
				while($max_size > 1000){$max_size /= 1024; $i++;}
				$max_size = round($max_size, 1);
				if($max_size) $file_type_config['max_size_str'] = $max_size.' '.l('text_filesize_'.$i, 'listings');
				$this->template_lite->assign('file_settings', $file_type_config);
				
				// videos
				$video_type_config = $this->Listings_model->get_video_type();
				
				$max_size = $video_type_config['max_size'];
			
				$i = 1;
				while($max_size > 1000){$max_size /= 1024; $i++;}
				$max_size = round($max_size, 1);
				if($max_size) $video_type_config['max_size_str'] = $max_size.' '.l('text_filesize_'.$i, 'listings');
				$this->template_lite->assign('video_settings', $video_type_config);
			break;
			case 'map':
				$markers = array(
					array( 
						'gid'			=> $data['id'],
						'country' 		=> $data['country'], 
						'region' 		=> $data['region'], 
						'city' 			=> $data['city'], 
						'address' 		=> $data['address'], 
						'postal_code'	=> $data['zip'], 
						'lat' 			=> (float)$data['lat'], 
						'lon' 			=> (float)$data['lon'], 
						'dragging'		=> true,
						'info' 			=> ($data['location'] ? $data['location'] : ''),
					),
				);
		
				$current_language = $this->pg_language->get_lang_by_id($this->pg_language->current_lang_id);
				$view_settings = array(
					'width' => '660',
					'height' => '250', 
					'disable_smart_zoom' => 1,
					'zoom_listener' => 'get_listing_zoom_data', 
					'type_listener' => 'get_listing_type_data',
					'drag_listener' => 'get_listing_drag_data',
					'lang' => $current_language['code'],
				);
				$this->template_lite->assign('map_settings', $view_settings);
				$this->template_lite->assign('markers', $markers);
				
				if($listing_id){
					$this->load->model('geomap/models/Geomap_model');
					$this->load->model('geomap/models/Geomap_settings_model');
					$map_gid = $this->Geomap_model->get_default_driver_gid();
					$map_settings = $this->Geomap_settings_model->get_settings($map_gid, 0, $listing_id, 'listing_view');
					$this->template_lite->assign('listing_map_settings', $map_settings);
				}
			break;
			case 'calendar':
				
			break;
		}
		$this->template_lite->assign('step', $step);
		$this->template_lite->assign('section_gid', $section_gid);
		$this->template_lite->assign('data', $data);

		$this->Menu_model->set_menu_active_item($user_type.'_account_menu', 'my_listings_item');
		$this->template_lite->view('listing_form');
	}
	
	/**
	 * Render listing services action
	 * @param integer $listing_id listing identifier
	 */
	public function services($listing_id){
		$user_id = $this->session->userdata('user_id');
		$user_type = $this->session->userdata('user_type');

		$data = $this->Listings_model->get_listing_by_id($listing_id);
//		print_r($data); exit;
		if($user_id != $data['id_user']){show_404(); return;}
		
		$this->config->load('date_formats', TRUE);
		$page_data['date_format'] = $this->config->item('st_format_date_literal', 'date_formats');
		$page_data['date_time_format'] = $this->config->item('st_format_date_time_literal', 'date_formats');

		$this->template_lite->assign('section_gid', 'services');
		$this->template_lite->assign('data', $data);
		$this->template_lite->assign('page_data', $page_data);

		if($this->pg_module->is_module_installed('services')){
			$this->load->model('Services_model');
			///// check pay services exists
			$services_params = array(
				'where_in' => array(
					'template_gid' => array(
						'listings_featured_template',
						'listings_lift_up_template',
						'listings_lift_up_country_template',
						'listings_lift_up_region_template', 
						'listings_lift_up_city_template', 
						'listings_highlight_template',
						'listings_slide_show_template',
						'listings_slider_template'	# MOD for slider image #
					),
				),	
				'where' => array(
					'status' => 1,
				),
			);
			$services_count = $this->Services_model->get_service_count($services_params);
			if($services_count > 0){
				$page_data['listings_services'] = $this->Services_model->get_service_list($services_params);
				$page_data['use_services'] = true;
			}
		}else{
			$page_data['use_services'] = false;
		}
	
		$params = array();
		foreach($page_data['listings_services'] as $service){
			foreach($service['data_admin'] as $param_gid => $param_value){
				$params[$param_gid] = $service['template']['data_admin_array'][$param_gid]['name'];
			}
		}
		if(count($params) == 1){
			$this->template_lite->assign('one_param', 1);
			$this->template_lite->assign('param_gid', key($params));
			$this->template_lite->assign('param_name', current($params));
		}
	
		$this->config->load('date_formats', TRUE);
		$page_data['date_format'] = $this->config->item('st_format_date_literal', 'date_formats');
		$page_data['date_time_format'] = $this->config->item('st_format_date_time_literal', 'date_formats');
		
		$this->template_lite->assign('section_gid', 'services');
		$this->template_lite->assign('data', $data);
		$this->template_lite->assign('page_data', $page_data);

		$this->load->model('payments/models/Payment_currency_model');
		$base_currency = $this->Payment_currency_model->get_currency_default(true);
		$this->template_lite->assign('base_currency', $base_currency);

		$this->Menu_model->breadcrumbs_set_parent($user_type.'_my_listings_item');
		$this->Menu_model->breadcrumbs_set_active(l('link_edit_listing', 'listings'), site_url().'listings/edit/'.$listing_id);
		$this->Menu_model->breadcrumbs_set_active(l('filter_section_services', 'listings'));
		$this->Menu_model->set_menu_active_item($user_type.'_account_menu', 'my_listings_item');
		
		$this->template_lite->view('listing_services_form');
	}

	/**
	 * Apply selected service to listing
	 * @param integer $listing_id listing identifier
	 * @param string $service_gid service GUID
	 */
	public function apply_service($listing_id, $service_gid){
		$user_id = $this->session->userdata('user_id');
		
		$data = $this->Listings_model->get_listing_by_id($listing_id);		
		if(!$data || $user_id != $data['id_user']){show_404();return;}
		$this->session->set_userdata(array('service_redirect'=>'listings/my'));
		
		$this->load->helper('payments');
		$params = array('id_listing'=>$listing_id);
		post_location_request(site_url().'services/form/'.$service_gid, $params);
	}
	
	/**
	 * Render activity status listing action
	 * @param integer $listing_id listing identifier
	 */
	public function activity($listing_id){
		$user_id = $this->session->userdata('user_id');
		$user_type = $this->session->userdata('user_type');

		$this->Listings_model->set_format_settings(array('get_user'=>false, 'get_location'=>false));
		$data = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings(array('get_user'=>true, 'get_location'=>true));
		if($user_id != $data['id_user']){show_404(); return;}

		$this->Menu_model->breadcrumbs_set_parent($user_type.'_my_listings_item');
		$this->Menu_model->breadcrumbs_set_active(l('link_edit_listing', 'listings'), site_url().'listings/edit/'.$listing_id);
		$this->Menu_model->breadcrumbs_set_active(l('filter_section_activity', 'listings'));

		if($this->pg_module->is_module_installed('users_services')){
			///// check pay services exists
			$this->load->model('Services_model');
			$services_params = array(
				'where_in' => array(
					'template_gid' => array($user_type.'_post_template', $user_type.'_combined_template'),
				),
				'where' => array(
					'status' => 1,
				),
			);
			$service_count = $this->Services_model->get_service_count($services_params);
			if($service_count > 0){
				$page_data['use_services'] = true;
			}
		}else{
			$page_data['use_services'] = false;
		}

		if($data['status'] == 0 && strtotime($data['date_activity']) > time()){
			$page_data['can_reactivate'] = true;
		}

		///// actions
		if($this->input->post('btn_activate')){
			if($page_data['use_services']){
				$user_service_id = intval($this->input->post('id_user_service', true));
				$user_service = $this->Users_services_model->get_service($user_service_id);
				if(empty($user_service) || $user_service['id_user'] != $user_id
				   || !$user_service['post_count'] || !$user_service['post_status']){
					$this->system_messages->add_message('error', l('error_service_activating', 'listings'));
					redirect(site_url().'listings/activity/'.$listing_id);
				}else{
					$post_period = $user_service['post_period'];
					$user_service['post_count']--;
					if($user_service['post_count'] <= 0){
						$user_service['post_count'] = $user_service['post_status'] = 0;
					}
					$this->Users_services_model->save_service($user_service_id, $user_service);
				}
			}else{
				$post_period = $this->pg_module->get_module_config('listings', 'default_activation_period');
			}

			if(!$data['initial_moderation']){
				$activate_data['initial_activity'] = $post_period;
				$this->Listings_model->save_listing($listing_id, $activate_data, false);
			}else{
				if(strtotime($data['date_activity']) > time()){
					$date_expire = date('Y-m-d H:i:s', strtotime($data['date_activity'])+$post_period*24*60*60);
				}else{
					$date_expire = date('Y-m-d H:i:s', time()+$post_period*24*60*60);
				}
				$this->Listings_model->activate_listing($listing_id, 1, $date_expire);
			}
			$this->system_messages->add_message('success', l('success_service_activating', 'listings'));
			redirect(site_url().'listings/activity/'.$listing_id);
		}

		if($this->input->post('btn_deactivate') && $data['status'] == 1){
			$this->Listings_model->activate_listing($listing_id, 0);
			$this->system_messages->add_message('success', l('success_service_deactivating', 'listings'));
			redirect(site_url().'listings/activity/'.$listing_id);
		}

		if($this->input->post('btn_reactivate') && $page_data['can_reactivate']){
			$this->Listings_model->activate_listing($listing_id, 1);
			$this->system_messages->add_message('success', l('success_service_activating', 'listings'));
			redirect(site_url().'listings/activity/'.$listing_id);
		}
		
		if($this->input->post('btn_make_sold') && $data['status'] == 1){
			$this->Listings_model->make_sold($listing_id, 1);
			$this->system_messages->add_message('success', l('success_make_sold', 'listings'));
			redirect(site_url().'listings/activity/'.$listing_id);
		}
		
		if($this->input->post('btn_delete_sold') && $data['status'] == 1){
			$this->Listings_model->make_sold($listing_id, 0);
			$this->system_messages->add_message('success', l('success_remove_sold', 'listings'));
			redirect(site_url().'listings/activity/'.$listing_id);
		}

		if($page_data['use_services']){
			/// get user services
			$params['where']['id_user'] = $user_id;
			$params['where']['post_status'] = '1';
			$page_data['user_services'] = $this->Users_services_model->get_services_list(null, $params);
		}else{
			$page_data['default_activation_period'] = $this->pg_module->get_module_config('listings', 'default_activation_period');
			$page_data['end_activation_date'] = date('Y-m-d H:i:s', time()+$page_data['default_activation_period']*24*60*60);
		}

		$this->config->load('date_formats', TRUE);
		$page_data['date_format'] = $this->config->item('st_format_date_literal', 'date_formats');
		$page_data['date_time_format'] = $this->config->item('st_format_date_time_literal', 'date_formats');

		$this->template_lite->assign('section_gid', 'activity');
		$this->template_lite->assign('data', $data);
		$this->template_lite->assign('page_data', $page_data);

		$this->Menu_model->set_menu_active_item($user_type.'_account_menu', 'my_listings_item');
		$this->template_lite->view('listing_activity_form');
	}
	
	/**
	 * Remove listing
	 * @param integer $listing_id listing identifier
	 */
	public function delete($listing_id){
		$user_id = $this->session->userdata('user_id');

		$this->Listings_model->set_format_settings('use_format', false);
		$data = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);

		if($user_id != $data['id_user']){show_404(); return;}

		if(!empty($listing_id)){
			$this->Listings_model->delete_listing($listing_id);
			$this->system_messages->add_message('success', l('success_listings_delete', 'listings'));
		}
		redirect(site_url().'listings/my/');
	}
	
	/**
	 * Close moderation alert
	 * @param integer $listing_id listing identifier
	 */
	public function alert($listing_id){
		if($this->session->userdata('auth_type') != 'user'){
			show_404();
			return;
		}
		
		$user_id = $this->session->userdata('user_id');
		$this->Listings_model->set_format_settings('use_format', false);
		$data = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);

		$this->template_lite->view('alert_form', 'user', 'listings');
	}
	
	/**
	 * Close moderation alert by ajax
	 * @param integer $listing_id listing identifier
	 */
	public function ajax_alert($listing_id){
		$user_id = $this->session->userdata('user_id');
		
		$this->Listings_model->set_format_settings('use_format', false);
		$data = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);

		$this->template_lite->assign('data', $data);
		
		$content = $this->template_lite->fetch('alert_form', 'user', 'listings');
		
		echo json_encode(array('content'=>$content));
		exit;
	}
	
	/**
	 * Return main search form by ajax
	 * @param integer $rand random number
	 */
	public function ajax_get_main_search_form($rand){
		$result = array('short'=>'', 'full'=>'');
		
		$this->template_lite->assign('form_settings', array('rand'=>$rand));
		
		$operation_type = $this->input->post('type', true);
		$category = $this->input->post('category', true);
		if($category){
			$category_chunks = explode('_', $category);
			$category_id = $category_chunks[0];
		}else{
			$category_id = 0;
		}
			
		$data = array('operation_type'=>$operation_type, 'id_category'=>$category_id);
		$property_type_gid = $this->Listings_model->get_field_editor_type($data);
		$main_search_form_data = $this->Listings_model->get_search_extend_form($property_type_gid, 'main_search_form_gid', false);
		if(count($main_search_form_data) > 0){
			$this->template_lite->assign('main_search_form_data', $main_search_form_data);
			$this->template_lite->assign('main_search_form_rand', rand(100000, 999999));
			$main_search_form = $this->template_lite->fetch('helper_main_search_form_block', 'user', 'listings');
			$main_search_form = trim($main_search_form);
			
			if($operation_type != 'rent' && $operation_type != 'lease'){
				if($main_search_form){
					$chunks = array();
				
					while(true){
						$pos = strpos($main_search_form, '<div class="search-field', 1);
						if($pos === false) break;
						$chunks[] = substr($main_search_form, 0, $pos);
						$main_search_form = trim(substr($main_search_form, $pos));
					}
				
					$chunks[] = $main_search_form;
					$chunks_count = count($chunks);
			
					$main_search_extend_form = array_shift($chunks);
					if($chunks_count > 1) $main_search_extend_form .= array_shift($chunks);
					$result['short'] = $main_search_extend_form;
			
					if($chunks_count > 2){
						$result['full'] = implode($chunks);
					}
				}
			}else{
				$result['short'] = '';
				$result['full'] = $main_search_form;
			}
		}
		echo json_encode($result);
		exit;
	}
	
	/**
	 * Return quick search form by ajax
	 */
	public function ajax_get_search_quick_form(){
		$operation_type = $this->input->post('type', true);
		$category_id = $this->input->post('id_category', true);
		$category_id = explode('_', $category_id);
		$data = array('operation_type'=>$operation_type, 'id_category'=>$category_id[0]);
		$property_type_gid = $this->Listings_model->get_field_editor_type($data);
		$quick_search_form_data = $this->Listings_model->get_search_extend_form($property_type_gid, 'quick_search_form_gid', false);
		if(count($quick_search_form_data) > 0){
			$this->template_lite->assign('form_data', $quick_search_form_data);
			$this->template_lite->assign('form_rand', rand(100000, 999999));
			echo $this->template_lite->fetch('helper_search_form_block', 'user', 'listings');
		}
		exit;
	}
	
	/**
	 * Return advanced search form by ajax
	 */
	public function ajax_get_search_advanced_form(){
		$operation_type = $this->input->post('type', true);
		$category_id = $this->input->post('id_category');
		$category_id = explode('_', $category_id);
		$data = array('operation_type'=>$operation_type, 'id_category'=>$category_id[0]);
		$property_type_gid = $this->Listings_model->get_field_editor_type($data);
		$advanced_search_form_data =  $this->Listings_model->get_search_extend_form($property_type_gid, 'advanced_search_form_gid', false);
		if(count($advanced_search_form_data) > 0){
			$this->template_lite->assign('form_data', $advanced_search_form_data);
			$this->template_lite->assign('form_rand', rand(100000, 999999));
			echo  $this->template_lite->fetch('helper_search_form_block', 'user', 'listings');
		}
		exit;
	}
	
	/**
	 * Return export form block
	 */
	public function ajax_get_export_extend_form(){
		$operation_type = $this->input->post('type', true);
		$category_id = $this->input->post('id_category');
		$category_id = explode('_', $category_id);
		$data = array('operation_type'=>$operation_type, 'id_category'=>$category_id[0]);
		$property_type_gid = $this->Listings_model->get_field_editor_type($data);
		
		$this->load->model('listings/models/Listings_export_model');
		echo $this->Listings_export_model->get_export_extend_form($property_type_gid, 'user_export_form_gid', false);
		exit;
	}
	
	/**
	 * Render search results
	 * @param string $keyword keyword string
	 * @param integer $operation_type listing operation type
	 * @param string $order order field
	 * @param string $order_direction order direction
	 * @param integer $page page of results
	 */
	public function search($keyword, $operation_type=null, $order='default', $order_direction='DESC', $page=1){
		$_SESSION['listings_search']['data']['keyword'] = trim(strip_tags($keyword));		
		$this->_list($operation_type, $order, $order_direction, $page);
	}
	
	/**
	 * Return searach result by ajax
	 * @param string $operaton_type operation type
	 */
	public function ajax_search(){
		$current_settings = isset($_SESSION['listings_search']) ? $_SESSION['listings_search'] : array();
		$current_settings['order'] = 'default';
		$current_settings['order_direction'] = 'DESC';
		$current_settings['page'] = 1;
		
		$new = $this->input->post('new');
		if($new){
			$current_settings['data'] = array();
		}
		
		$data = $this->input->post('filters', true);
	
		//operation type
		$operation_types = $this->Listings_model->get_operation_types(true);
		if(isset($data['type'])){
			if(in_array($data['type'], $operation_types)) $current_settings['data']['type'] = $data['type'];
			unset($data['type']);
		}

		// category
		if(isset($data['category'])){
			$category_chunks = explode('_', $data['category']);
			if($category_chunks[0]){
				if(isset($current_settings['data']['field_editor_data'])){
					$this->load->model('field_editor/models/Field_editor_forms_model');
					
					if(isset($current_settings['data']['id_category']) && $current_settings['data']['id_category'] != $category_chunks[0]){
						unset($current_settings['data']['field_editor_data']);
					}else{
						$property_type_gid = $this->Listings_model->field_editor_types[$current_settings['data']['id_category']];
					
						$form_gids = array();
						foreach(array_keys($current_settings['data']['field_editor_data']) as $form_name){
							$form_gids[$form_name] = $this->Listings_model->{$form_name.'_'.$property_type_gid.'_gid'};
						}
					
						$params = array();
						$params['where']['editor_type_gid'] = $property_type_gid;
						$params['where_in']['gid'] = $form_gids;
					
						$forms = $this->Field_editor_forms_model->get_forms_list($params);
						$forms = array_intersect_key(array_flip($form_gids), $forms); 
				
						foreach($forms as $form){
							unset($current_settings['data']['field_editor_data'][$form]);
						}
						if(empty($current_settings['data']['field_editor_data'])) 
							unset($current_settings['data']['field_editor_data']);
					}
				}
				$current_settings['data']['id_category'] = $category_chunks[0];
			}else{
				if(isset($current_settings['data']['id_category'])) unset($current_settings['data']['id_category']);
				if(isset($current_settings['data']['property_type'])) unset($current_settings['data']['property_type']);
				
				if(isset($current_settings['data']['field_editor_data'])){
					$this->load->model('field_editor/models/Field_editor_forms_model');
					
					if(isset($current_settings['data']['id_category'])){
						$category_id = $current_settings['data']['id_category'];
					}else{
						$category_id = 0;
					}
					
					$data = array('operation_type'=>$current_settings['data']['type'], 'id_category'=>$category_id);
					$property_type_gid = $this->Listings_model->get_field_editor_type($data);
					
					$form_gids = array();
					foreach(array_keys($current_settings['data']['field_editor_data']) as $form_name){
						$form_gids[$form_name] = $this->Listings_model->{$form_name.'_'.$property_type_gid.'_gid'};
					}
					
					$params = array();
					$params['where']['editor_type_gid'] = $property_type_gid;
					$params['where_in']['gid'] = $form_gids;
					
					$forms = $this->Field_editor_forms_model->get_forms_list($params);
					$forms = array_intersect_key(array_flip($form_gids), $forms); 
				
					foreach($forms as $form){
						unset($current_settings['data']['field_editor_data'][$form]);
					}
					if(empty($current_settings['data']['field_editor_data'])) 
						unset($current_settings['data']['field_editor_data']);
				}
			}
			if(count($category_chunks) > 1 && $category_chunks[1]){
				$current_settings['data']['property_type'] = $category_chunks[1];
			}else{
				if(isset($current_settings['data']['property_type'])) unset($current_settings['data']['property_type']);
			}
			unset($data['category']);
		}
		
		//price
		$price_ranges = $this->Listings_model->get_price_range();
		$price_range = $price_ranges[$current_settings['data']['type']];
		
		if(isset($data['price_max'])) {
			if(!empty($data['price_max']) && $data['price_max'] < $price_range['max_price']){
				$current_settings['data']['price_max'] = $data['price_max'];
			}else{
				if(isset($current_settings['data']['price_max'])) unset($current_settings['data']['price_max']);
			}
			unset($data['price_max']);
		}

		if(isset($data['price'])){
			if(!empty($data['price'])){
				$chunks = explode('-', $data['price']);
				if($chunks[0]>0){
					$current_settings['data']['price_min'] = $chunks[0];
				}else{
					if(isset($current_settings['data']['price_min'])) unset($current_settings['data']['price_min']);
				}
				if(count($chunks)>1 && $chunks[1]>0 && $chunks[1]<$price_range['max_price']){
					$current_settings['data']['price_max'] = $chunks[1];
				}else{
					if(isset($current_settings['data']['price_max'])) unset($current_settings['data']['price_max']);
				}
			}else{
				if(isset($current_settings['data']['price_min'])) unset($current_settings['data']['price_min']);
				if(isset($current_settings['data']['price_max'])) unset($current_settings['data']['price_max']);
			}
			unset($data['price']);
		}
		
		if(isset($current_settings['data']['type'])){
			$operation_type = $current_settings['data']['type'];
		}else{
			$operation_type = $this->Listings_model->get_operation_type_gid_default();
		}
		
		//booking
		if(in_array($operation_type, array('rent', 'lease'))){
			if(isset($data['booking_date_start'])){
				$value = strtotime($data['booking_date_start']);
				if($value > 0) $current_settings['data']['booking_date_start'] = date('Y-m-d', $value);
				unset($data['booking_date_start']);
			}
		
			if(isset($data['booking_date_end'])){
				$value = strtotime($data['booking_date_end']);
				if($value > 0) $current_settings['data']['booking_date_end'] = date('Y-m-d', $value);
				unset($data['booking_date_end']);
			}
		}else{
			if(isset($data['booking_date_start'])) unset($data['booking_date_start']);
			if(isset($data['booking_date_end'])) unset($data['booking_date_end']);
			if(isset($data['booking_date_guests'])) unset($data['booking_date_guests']);
			
			if($current_settings['data']['booking_date_start']) unset($current_settings['data']['booking_date_start']);
			if($current_settings['data']['booking_date_end']) unset($current_settings['data']['booking_date_end']);
			if($current_settings['data']['booking_date_guests']) unset($current_settings['data']['booking_date_guests']);
		}
		
		//location
		$region_name = $this->input->post('region_name', true);
		if(isset($region_name) && empty($data['id_country']) && empty($data['id_region']) && empty($data['id_city'])){
			if(isset($data['keyword'])){
				$data['keyword'] .= ' '.$region_name;
			}else{
				$data['keyword'] = $region_name;
			}
		}
		
		//keyword
		if(isset($data['keyword'])){
			$data['keyword'] = trim(strip_tags($data['keyword']));
			if(!empty($data['keyword'])){
				 $current_settings['data']['keyword'] = $data['keyword'];
			}else{
				unset($current_settings['data']['keyword']);
			}
			unset($data['keywords']);
		}
	
		// field editor
		$form = $this->input->post('form', true);
		if($form){
			$data['field_editor_data'][$form] = $this->input->post('data', true);
	
			if(!is_array($data['field_editor_data'][$form])){
				$data['field_editor_data'][$form] = array();
			}
			$field_editor_range = $this->input->post('field_editor_range', true);
			if(!is_array($field_editor_range)){
				$field_editor_range = array();
			}
			$short_value = $this->input->post('short_value', true);
		
			foreach($data['field_editor_data'][$form] as $field_gid=>$field_value){
				if(isset($field_value['range']['min'])){
					if(isset($field_editor_range[$field_gid]['min'])){
						$min_value = $field_editor_range[$field_gid]['min'];
					}else{
						$min_value = 0;
					}
					if($field_value['range']['min'] <= $min_value){
						unset($data['field_editor_data'][$form][$field_gid]['range']['min']);
					}
				}
				if(isset($field_value['range']['max'])){
					if(isset($field_editor_range[$field_gid]['max'])){
						$max_value = $field_editor_range[$field_gid]['max'];
						if($field_value['range']['max'] >= $max_value){
							unset($data['field_editor_data'][$form][$field_gid]['range']['max']);
						}
					}elseif(empty($field_value['range']['max'])){
						unset($data['field_editor_data'][$form][$field_gid]['range']['max']);
					}					
				}
				if(isset($data['field_editor_data'][$form][$field_gid]['range']) && 
				   empty($data['field_editor_data'][$form][$field_gid]['range'])){
					unset($data['field_editor_data'][$field_gid]['range']);
				}
			}
			if(isset($current_settings['data']['field_editor_data'])){
				foreach($current_settings['data']['field_editor_data'] as $form_name=>$form_data){
					if($form_name == $form) continue;
					$data['field_editor_data'][$form_name] = array_diff_key($form_data, $data['field_editor_data'][$form]);
					if(empty($data['field_editor_data'][$form_name])){ 
						unset($data['field_editor_data'][$form_name]);
					}
				}
			}
			foreach($data['field_editor_data'][$form] as $field_gid=>$field_data){
				if(empty($field_data)){
					unset($data['field_editor_data'][$form][$field_gid]);
				}elseif(is_array($field_data)){
					$empty = true;
					foreach($field_data as $value){
						if(!empty($value)){
							$empty = false;
							break;
						}
					}
					if($empty){
						unset($data['field_editor_data'][$form][$field_gid]);
					}
				}
			}		
			if(empty($data['field_editor_data'][$form])) unset($data['field_editor_data'][$form]);
		}
		foreach($data as $key=>$value){
			if(!empty($value)){
				$current_settings['data'][$key] = $value;
			}elseif(isset($current_settings['data'][$key])){
				unset($current_settings['data'][$key]);
			}
		}

		$_SESSION['listings_search'] = 	$current_settings;
		
		$this->load->helper('seo');
		
		$search_action_data = $this->Listings_model->get_search_link_data();
		echo rewrite_link('listings', $search_action_data['keyword'] ? 'search' : 'index', $search_action_data);
	}

	/**
	 * Return number of found listings
	 */
	public function ajax_search_counts(){
		$result = array('count'=>0, 'error'=>'', 'string'=>'');
		
		$current_settings = 
		
		$filters = isset($_SESSION['listings_search']['data']) ? $_SESSION['listings_search']['data'] : array();
		$filters['active'] = '1';
		if($this->session->userdata('auth_type') == 'user'){
			$filters['not_user'] = $this->session->userdata('user_id');
		}
		
		$result['count'] = $this->Listings_model->get_listings_count($filters);
		
		echo json_encode($result);
	}
	
	/**
	 * Return link for index or search listings
	 */
	public function ajax_get_search_link(){
		$this->load->helper('seo');
		
		//load properties
		$this->load->model('Properties_model');
		$property_types = $this->Properties_model->get_categories('property_types', $this->pg_language->current_lang_id);
		$this->template_lite->assign('property_types', $property_types);
		$property_items = $this->Properties_model->get_all_properties('property_types', $this->pg_language->current_lang_id);
		$this->template_lite->assign('property_items', $property_items);
		
		$data = array(
			'id_category' => 0,
			'category' => 'all',
			'property_type' => 0,
			'property' => '',
			'order' => 'default', 
			'order_direction' => 'DESC', 
			'page'=>0,
		);
		
		if(isset($_SESSION['listings_search']) && !empty($_SESSION['listings_search'])){
			$data['order'] = $_SESSION['listings_search']['order'];
			$data['order_direction'] = $_SESSION['listings_search']['order_direction'];
			if(isset($_SESSION['listings_search']['data']['type'])){
				$data['type'] = $_SESSION['listings_search']['data']['type'];
			}
			if(isset($_SESSION['listings_search']['data']['id_category'])){
				$data['id_category'] = $_SESSION['listings_search']['data']['id_category'];
				$data['category'] = $property_types[$this->Listings_model->field_editor_types[$data['id_category']]];
				if(isset($_SESSION['listings_search']['data']['property_type'])){
					$data['property_type'] = $_SESSION['listings_search']['data']['property_type'];
					$data['property'] = $property_items[$data['id_category']][$data['property_type']];
				}
			}
			if(isset($_SESSION['listings_search']['data']['keyword'])){
				$data['keyword'] = $_SESSION['listings_search']['data']['keyword'];
			}
		}
		
		$post_data = (array)$this->input->post('filters', true);
		
		$operation_types = $this->Listings_model->get_operation_types(true);
		if(isset($post_data['operation_type']) && in_array($post_data['operation_type'], $operation_types)){
			$data['operation_type'] = $post_data['operation_type'];
		}else{
			$data['operation_type'] = current($operation_types);
		}
		
		if(isset($post_data['id_category'])){
			$data['id_category'] = $post_data['id_category'];
			$data['category'] = $property_types[$post_data['id_category']];
			if(isset($post_data['property_type']) && $post_data['property_type']){
				$data['property_type'] = $post_data['property_type'];
				$data['property'] = $property_items[$post_data['id_category']][$post_data['property_type']];
			}
		}
		
		if(isset($post_data['category']) && !empty($post_data['category'])){
			$category_chunks = explode('_', $post_data['category']);
			$data['id_category'] = $category_chunks[0];
			if(count($category_chunks)>1) $data['property_type'] = $category_chunks[1];
		}
		
		if(isset($post_data['keyword']) && !empty($post_data['keyword'])){
			$data['keyword'] = trim(strip_tags($post_data['keyword']));
		}else{
			$region_name = $this->input->post('region_name', true);
			$region_name = trim($region_name);
			if(!empty($region_name)){
				$data['keyword'] = $region_name;
			}
		}
		
		if(!empty($data['keyword'])){
			echo rewrite_link('listings', 'search', $data);
		}else{
			echo rewrite_link('listings', 'index', $data);
		}
	}
	
	/**
	 * Remove search criteria
	 * @param string $gid filter guid
	 * @param $field_editor_gid filter guid of field editor
	 */
	public function delete_search_criteria($gid, $field_editor_gid=''){
		$delete = array($gid);
		switch($gid){
			case 'id_category':
			case 'property_type':
				$delete[] = 'id_category';
				$delete[] = 'property_type';
			break;
			case 'price':
				$delete[] = 'price_min';
				$delete[] = 'price_max';
			break;
			case 'field_editor':
				if(!isset($_SESSION['listings_search']['data']['field_editor_data'])) break;
				foreach($_SESSION['listings_search']['data']['field_editor_data'] as $form_name=>$form_data){
					if(isset($form_data[$field_editor_gid])){
						unset($form_data[$field_editor_gid]);
						if(empty($form_data)){
							unset($_SESSION['listings_search']['data']['field_editor_data'][$form_name]);
							if(empty($_SESSION['listings_search']['data']['field_editor_data'])){
								unset($_SESSION['listings_search']['data']['field_editor_data']);
							}
						}else{
							$_SESSION['listings_search']['data']['field_editor_data'][$form_name] = $form_data;
						}
						break;	
					}
				}
			break;
			default:
				$delete[] = $gid;
			break;
		}
		foreach($delete as $field_gid){
			if(isset($_SESSION['listings_search']['data'][$field_gid]))
				unset($_SESSION['listings_search']['data'][$field_gid]);
		}
		$this->system_messages->add_message('success', l('success_delete_search_criteria', 'listings'));
		
		//search seo
		$this->load->helper('seo');
		
		$search_action_data = $this->Listings_model->get_search_link_data();
		$search_link = rewrite_link('listings', !empty($search_action_data['keyword']) ? 'search' : 'index', $search_action_data);
		redirect($search_link);
	}
	
	/**
	 * Remove search criteria by ajax
	 * @param string $gid filter guid
	 * @param string $field_editor_gid filter guid of field editor
	 */
	public function ajax_delete_search_criteria($gid, $field_editor_gid=''){
		$return = array('error'=>'', 'success'=>'', 'data'=>'');
		
		$delete = array();
	
		switch($gid){
			case 'id_category':
			case 'property_type':
				$delete[] = 'id_category';
				$delete[] = 'property_type';
			break;
			case 'price':
				$delete[] = 'price_min';
				$delete[] = 'price_max';
			break;
			case 'location':
				$delete[] = 'id_country';
				$delete[] = 'id_region';
				$delete[] = 'id_city';
			case 'field_editor':
				if(!isset($_SESSION['listings_search']['data']['field_editor_data'])) break;
				foreach($_SESSION['listings_search']['data']['field_editor_data'] as $form_name=>$form_data){
					if(isset($form_data[$field_editor_gid])){
						unset($form_data[$field_editor_gid]);
						if(empty($form_data)){
							unset($_SESSION['listings_search']['data']['field_editor_data'][$form_name]);
							if(empty($_SESSION['listings_search']['data']['field_editor_data'])){
								unset($_SESSION['listings_search']['data']['field_editor_data']);
							}
						}else{
							$_SESSION['listings_search']['data']['field_editor_data'][$form_name] = $form_data;
						}
						break;	
					}
				}
			break;
			default:
				$delete[] = $gid;
		}
		foreach($delete as $field_gid){
			if(isset($_SESSION['listings_search']['data'][$field_gid]))
				unset($_SESSION['listings_search']['data'][$field_gid]);
		}
	
		if(!isset($_SESSION['listings_search']['data']['type'])){
			$operation_types = $this->Listings_model->get_operation_types(true);
			$_SESSION['listings_search']['data']['type'] = current($operation_types);
		}
	
		$current_search_data = $this->Listings_model->get_search_data();	
		$this->template_lite->assign('current_search_data', $current_search_data);
		
		$search_filters_block = $this->template_lite->fetch('search_filters_block', 'user', 'listings');
		$this->template_lite->assign('search_filters_block', $search_filters_block);
		
		$return['data'] = $this->template_lite->fetch('search_filters_block', 'user', 'listings');
		$return['type'] = $_SESSION['listings_search']['data']['type'];
		$return['success'] = l('success_delete_search_criteria', 'listings');
		
		echo json_encode($return);
	}
	
	/**
	 * Return main search form by ajax
	 * @param string $operation_type operation type
	 * @param integer $type type of search form
	 */
	public function ajax_search_form($operation_type=null, $type='short'){
		$operation_types = $this->Listings_model->get_operation_types(true);
		if(!$operation_type || !in_array($operation_type, $operation_types))
			$operation_type = $this->Listings_model->get_operation_type_gid_default();	
	
		$this->load->helper('listings');
			
		$rand = $this->input->post('rand', true);
		if(!$rand) $rand = rand(100000, 999999);

		$page_data = array(
			'object' => $operation_type,
			'rand' => $rand,
		);			
		$this->template_lite->assign('form_settings', $page_data);
		
		$form_block = listings_main_search_form($type);
		echo $form_block;
	}
	
	/**
	 * Return slider search form by ajax
	 * @param string $operation_type operation type
	 */
	public function ajax_slider_form($operation_type=null){
		$operation_types = $this->Listings_model->get_operation_types(true);
		if(!$operation_type || !in_array($operation_type, $operation_types))
			$operation_type = current($operation_types);		
		$rand = $this->input->post('rand');
		echo $this->Listings_model->get_slider_form($operation_type);
	}
	
	/**
	 * Save search criteria
	 */
	public function save_search(){
		if(strpos($_SERVER['HTTP_REFERER'], site_url()) !== false){
			$url = $_SERVER['HTTP_REFERER'];
		}else{
			$this->load->helper('seo');
			$search_action_data = $this->get_search_link_data();
			$url = rewrite_link('listings', $search_action_data['keyword'] ? 'search' : 'index', $search_action_data);
		}
		
		$user_id = $this->session->userdata('user_id');
		if(!$user_id){
			$this->system_messages->add_message('error', l('error_empty_user_search_save', 'listings'));
			redirect($url);
		}

		$current_settings = isset($_SESSION['listings_search'])?$_SESSION['listings_search']:array();
		$data = (!empty($current_settings['data']))?$current_settings['data']:array();

		if(empty($data)){
			$this->system_messages->add_message('error', l('error_empty_search_save', 'listings'));
			redirect($url);
		}

		$save_data = array(
			'id_user' => $user_id,
			'search_data' => $data
		);

		$this->load->model('listings/models/Listings_search_model');
		$validate_data = $this->Listings_search_model->validate_search(null, $save_data);

		if(!empty($validate_data['errors'])){
			$this->system_messages->add_message('error', implode('<br>', $validate_data['errors']));
		}else{
			$this->Listings_search_model->save_search(null, $validate_data['data']);
			$this->system_messages->add_message('success', l('success_save_search', 'listings'));
		}
		
		redirect($url);
	}

	/**
	 * Save search criteria by ajax
	 */
	public function ajax_save_search(){
		$return = array('success' => false, 'error' => '', 'data' => '');

		$user_id = $this->session->userdata('user_id');
		if(!$user_id){
			$return['error'] = l('error_empty_user_search_save', 'listings');
			echo json_encode($return); return;
		}

		$current_settings = isset($_SESSION['listings_search'])?$_SESSION['listings_search']:array();
		$data = (!empty($current_settings['data']))?$current_settings['data']:array();

		if(empty($data)){
			$return['error'] = l('error_empty_search_save', 'listings');
			echo json_encode($return); return;
		}

		$save_data = array(
			'id_user' => $user_id,
			'search_data' => $data
		);

		$this->load->model('listings/models/Listings_search_model');
		$validate_data = $this->Listings_search_model->validate_search(null, $save_data);
		
		if(!empty($validate_data['errors'])){
			$return['error'] = implode('<br>', $validate_data['errors']);
		}else{
			$this->Listings_search_model->save_search(null, $validate_data['data']);
	
			$filters = array('user'=>$user_id);
			$save_search_items_on_page = $this->pg_module->get_module_config('listings', 'save_search_items_per_page');
			$save_search_data = $this->Listings_search_model->get_searches_list($filters, 1, $save_search_items_on_page, array('id' => 'DESC'));
			$this->template_lite->assign('save_search_data', $save_search_data);
			$return['data'] = $this->template_lite->fetch('saved_searches_block', 'user', 'listings');
			
			$return['success'] = l('success_save_search', 'listings');
		}
		echo json_encode($return); return;
	}

	/**
	 * Save search criteria
	 * @param integer $page page of results
	 */
	public function preferences($page=1){
		$this->load->model('listings/models/Listings_search_model');
		
		$user_id = $this->session->userdata('user_id');
		$user_type = $this->session->userdata('user_type');
		
		$items_on_page = $this->pg_module->get_module_config('start', 'index_items_per_page');

		$params['user'] = $user_id;
		
		$searches_count = $this->Listings_search_model->get_searches_count($params);
		if($searches_count){
			$searches = $this->Listings_search_model->get_searches_list($params, $page, $items_on_page, array('id'=>'DESC'));
			$this->template_lite->assign('searches', $searches);
		}
		
		$this->load->helper('navigation');
		$url = site_url().'listings/preferences/';
		$page_data = get_user_pages_data($url, $searches_count, $items_on_page, $page, 'briefPage');
		$this->template_lite->assign('page_data', $page_data);

		$this->Menu_model->set_menu_active_item($user_type.'_account_menu', 'my_searches_item');
		$this->Menu_model->breadcrumbs_set_parent($user_type.'_my_searches_item');

		$this->template_lite->view('listings_searches_list');
	}

	/**
	 * Load search criteria
	 * @param integer $search_id saved search identifier
	 */
	public function load_saved_search($search_id){
		$user_id = $this->session->userdata('user_id');

		$this->load->model('listings/models/Listings_search_model');

		$search = $this->Listings_search_model->get_search_by_id($search_id);
		if($search['id_user'] != $user_id){show_404(); return;}

		$current_settings = isset($_SESSION['listings_search'])?$_SESSION['listings_search']:array();
		$current_settings['data'] = $search['search_data'];

		$_SESSION['listings_search'] = $current_settings;

		$this->load->helper('seo');

		$search_action_data = $this->Listings_model->get_search_link_data();
		$search_link = rewrite_link('listings', $search_action_data['keyword'] ? 'search' : 'index', $search_action_data);

		redirect($search_link);
	}

	/**
	 * Remove search criteria
	 * @param integer $search_id saved search identifier
	 */
	public function delete_saved_search($search_id){
		$this->load->model('listings/models/Listings_search_model');
		$user_id = $this->session->userdata('user_id');
		$search = $this->Listings_search_model->get_search_by_id($search_id);
		if($search['id_user'] != $user_id){show_404(); return;}
		$this->Listings_search_model->delete_search($search_id);
		$this->system_messages->add_message('success', l('success_delete_search', 'listings'));
		redirect(site_url().'listings/preferences');
	}
	
	/**
	 * Remove search criteria
	 * @param integer $search_id saved search identifier
	 */
	public function ajax_delete_saved_search($search_id){
		$return = array('error'=>'', 'success'=>'', 'data'=>'');
		if($this->session->userdata('auth_type') != 'user'){
			$return['error'] = l('error_empty_user', 'listings');
		}else{
			$this->load->model('listings/models/Listings_search_model');
			$user_id = $this->session->userdata('user_id');
			$search = $this->Listings_search_model->get_search_by_id($search_id);
			if($search['id_user'] != $user_id){show_404(); return;}
			$this->Listings_search_model->delete_search($search_id);
			$filters = array('user'=>$user_id);
			$save_search_items_on_page = $this->pg_module->get_module_config('listings', 'save_search_items_per_page');
			$save_search_data = $this->Listings_search_model->get_searches_list($filters, 1, $save_search_items_on_page, array('id'=>'DESC'));
			$this->template_lite->assign('save_search_data', $save_search_data);
			$return['data'] = $this->template_lite->fetch('saved_searches_block', 'user', 'listings');
			$return['success'] = l('success_delete_search', 'listings');
		}
		echo json_encode($return);
	}
	
	/**
	 * Render block of listings into comparison list 
	 * @param string $operation_type operation type
	 * @param string $order sorting order
	 * @param string $order_direction order direction
	 * @param integer $page page of results
	 */
	private function _saved_listings_list_block($operation_type, $order='default', $order_direction='DESC', $page=1){
		$current_settings = isset($_SESSION['saved_list'])?$_SESSION['saved_list']:array();

		if(!isset($current_settings['type']))
			$current_settings['type'] = $this->Listings_model->get_operation_type_gid_default();
		if(!isset($current_settings['order']))
			$current_settings['order'] = 'modified';
		if(!isset($current_settings['order_direction']))
			$current_settings['order_direction'] = 'DESC';
		if(!isset($current_settings['page']))
			$current_settings['page'] = 1;
		
		if($current_settings['type'] != $operation_type){
			$current_settings['type'] = $operation_type;
			$page = 1;
		}
		
		if(!empty($order)) $current_settings['order'] = $order;
		$order = $current_settings['order'];
		$this->template_lite->assign('order', $current_settings['order']);
		
		if(!empty($order_direction)) $current_settings['order_direction'] = $order_direction;
		$order_direction = $current_settings['order_direction'];
		$this->template_lite->assign('order_direction', $order_direction);
		
		$page = intval($page);
		if(!empty($page)) $current_settings['page'] = $page;
		$page = $current_settings['page'];
		
		$_SESSION['saved_list'] = $current_settings;
		
		$items_on_page = $this->pg_module->get_module_config('start', 'index_items_per_page');
		
		if($this->session->userdata('auth_type') == 'user'){
			$user_id = $this->session->userdata('user_id');
			$saved_listings = $this->session->userdata('saved_listings');
			if (!isset($saved_listings) || empty($saved_listings)){
				$this->load->model('listings/models/Saved_listings_model');
				$list = $this->Saved_listings_model->get_saved_list_by_user((int)$user_id);
				$saved_listings = array();
				foreach($list as $item){
					$saved_listings[] = $item['id_listing'];
				}
				$this->session->set_userdata('saved_listings', $saved_listings);
			}
		}elseif(isset($_SESSION['saved_listings'])){
			$saved_listings = $_SESSION['saved_listings'];
		}else{
			$saved_listings = array();
		}

		if(!empty($saved_listings)){
			$items_count = array();
			$filters['ids'] = $saved_listings;
			$filters['active'] = 1;
			$filters['type'] = $operation_type;
			$items_count = $this->Listings_model->get_listings_count($filters);
		}else{
			$items_count = 0;
		}
		
		$this->load->helper('sort_order');
		$page = get_exists_page_number($page, $items_count, $items_on_page);

		$sort_data = array(
			'url' => site_url().'listings/saved/'.$operation_type,
			'order' => $order,
			'direction' => $order_direction,
			'links' => array(
				'views' => l('field_views', 'listings'),
				'price' => l('field_price', 'listings'),
				'open' => l('field_date_open', 'listings'),
				'modified' => l('field_date_modified', 'listings'),
			)
		);
		if($this->pg_module->is_module_installed('reviews')){
			$this->load->helper('reviews');
			$review_link = get_reviews_sorter();
			if(is_array($review_link)){
				$first = array_splice($sort_data['links'], 0, 1);
				$sort_data['links'] = array_merge($first, $review_link, $sort_data['links']);
			}
		}
		$this->template_lite->assign('sort_data', $sort_data);

		$use_leader = false;
		if ($items_count > 0){
			$order_array = array();
			if($order == 'price'){
				$order_array['price_sorting'] = $order_direction;
			}elseif($order == 'open'){
				$order_array['date_open'] = $order_direction;
			}elseif($order == 'modified'){
				$order_array['date_modified'] = $order_direction;
			}else{
				$order_array[$order] = $order_direction;
			}
			
			$listings = $this->Listings_model->get_listings_list($filters, $page, $items_on_page, $order_array);
			$this->template_lite->assign('listings', $listings);
		
		}
	
		
		$this->load->helper('navigation');
		$url = site_url().'listings/saved/'.$operation_type.'/'.$order.'/'.$order_direction.'/';
		$page_data = get_user_pages_data($url, $items_count, $items_on_page, $page, 'briefPage');
		$this->config->load('date_formats', TRUE);
		$page_data['date_format'] = $this->config->item('st_format_date_literal', 'date_formats');
		$page_data['date_time_format'] = $this->config->item('st_format_date_time_literal', 'date_formats');
		$page_data['use_leader'] = $use_leader;

		$this->template_lite->assign('page_data', $page_data);

		$use_save_search = false;
		$this->template_lite->assign('use_save_search', $use_save_search);

		return $this->template_lite->fetch('saved_listings_block', 'user', 'listings');
	}

	/**
	 * Render listings into comparison list
	 * @param strung $operation_type operation type
	 * @param string $order sorting order
	 * @param string $order_direction order direction
	 * @param integer $page page of results
	 */
	public function saved($operation_type=null, $order=null, $order_direction=null, $page=1){
		$operation_types = $this->Listings_model->get_operation_types(true);
		$this->template_lite->assign('operation_types', $operation_types);
		
		if(!in_array($operation_type, $operation_types)){
			$operation_type = $this->Listings_model->get_operation_type_gid_default();
		}
		$this->template_lite->assign('current_operation_type', $operation_type);
		
		$block = $this->_saved_listings_list_block($operation_type, $order, $order_direction, $page);
		$this->template_lite->assign('block', $block);
		
		if($this->session->userdata('auth_type') == 'user'){
			$user_id = $this->session->userdata('user_id');
			$user_type = $this->session->userdata('user_type');
			$saved_listings = $this->session->userdata('saved_listings');
			if(!isset($saved_listings) || empty($saved_listings)){
				$this->load->model('listings/models/Saved_listings_model');
				$list = $this->Saved_listings_model->get_saved_list_by_user((int)$user_id);
				$saved_listings = array();
				foreach($list as $item){
					$saved_listings[] = $item['id_listing'];
				}
				$this->session->set_userdata('saved_listings', $saved_listings);
			}
			$this->Menu_model->breadcrumbs_set_parent($user_type.'_my_saved_listings');
		}else{
			if(isset($_SESSION['saved_listings'])){
				$saved_listings = $_SESSION['saved_listings'];
			}else{
				$saved_listings = array();
			}
			$this->Menu_model->breadcrumbs_set_active(l('guest_my_saved_listings', 'listings'));
		}
		
		if(!empty($saved_listings)){
			$items_count = array();
			$filters['ids'] = $saved_listings;
			$filters['active'] = 1;
			foreach($operation_types as $operation_type){
				$filters['type'] = $operation_type;
				$items_count[$operation_type] = $this->Listings_model->get_listings_count($filters);
			}
			$this->template_lite->assign('items_count', $items_count);
			$this->template_lite->assign('saved_count', array_sum($items_count));
		}
		
		$current_settings = isset($_SESSION['saved_list'])?$_SESSION['saved_list']:array();
		if(!isset($current_settings['order'])) $current_settings['order'] = 'date_created';
		$this->template_lite->assign('order', $current_settings['order']);
		
		if(!isset($current_settings['order_direction'])) $current_settings['order_direction'] = 'DESC';
		$this->template_lite->assign('order_direction', $current_settings['order_direction']);
		
		if(!isset($current_settings['page'])) $current_settings['page'] = 1;
		$this->template_lite->assign('page', $current_settings['page']);

		$this->template_lite->view('saved_listings');
	}
	
	/**
	 * Return saved listings by ajax
	 * @param string $operation_type operation type
	 * @param string $order sorting order
	 * @param string $order_direction order direction
	 * @param integer $page page of results
	 */
	public function ajax_saved($operation_type=null, $order=null, $order_direction=null, $page=null){
		$operation_types = $this->Listings_model->get_operation_types(true);
		
		$current_settings = isset($_SESSION['saved_list'])?$_SESSION['saved_list']:array();
		
		if(!$current_settings['type']) $current_settings['type'] = $this->Listings_model->get_operation_type_gid_default();
		if(!$current_settings['order']) $current_settings['order'] = 'date_modified';
		if(!$current_settings['order_direction']) $current_settings['order_direction'] = 'DESC';
		if(!$current_settings['page']) $current_settings['page'] = 1;
		
		if(!in_array($operation_type, $operation_types)) $operation_type = $current_settings['type'];
		if(!$order) $order = $current_settings['order'];
		if(!$order_direction) $order_direction = $current_settings['order_direction'];
		if(!$page) $page = $current_settings['page'];
		
		echo $this->_saved_listings_list_block($operation_type, $order, $order_direction, $page);
		exit;
	}	

	/**
	 * Save listing into comparison list
	 * @param integer $listing_id listing identifier
	 */
	private function _save_listing($listing_id){
		$return = array('error'=>1, 'success'=>0, 'message'=>l('listings_error_saving', 'listings'));
		if(!$listing_id) return $return;
			
		$this->Listings_model->set_format_settings('use_format', false);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);	
		if(!$listing) return $return;
		
		$this->load->model('listings/models/Saved_listings_model');
		if($this->session->userdata('auth_type') == 'user'){
			$user_id = $this->session->userdata('user_id');
			if($listing && $listing['id_user'] != $user_id){
				$saved_listings = $this->session->userdata('saved_listings');
				if(!isset($saved_listings) || empty($saved_listings)){
					$list = $this->Saved_listings_model->get_saved_list_by_user((int)$user_id);
					$saved_listings = array();
					foreach($list as $item){
						$saved_listings[] = $item['id_listing'];
						$this->session->set_userdata('saved_listings', $saved_listings);
					}
				}
				if(!isset($saved_listings) || !in_array($listing_id, $saved_listings)){
					$data['id_user'] = $user_id;
					$data['id_listing'] = $listing_id;
					$this->Saved_listings_model->save_saved(null, $data);
					
					$saved_listings[] = $data['id_listing'];
					$this->session->set_userdata('saved_listings', $saved_listings);
				}
				$return['error'] = 0;
				$return['success'] = 1;
				$return['id'] = $listing_id;
				$return['message'] = l('success_listings_saved', 'listings');
				$_SESSION['saved_listings'][] = $listing_id;
			}else{
				$return['message'] = l('listings_error_saving', 'listings');
			}
		}else{
			$return['error'] = 0;
			$return['success'] = 1;
			$return['id'] = $listing_id;
			$return['message'] = l('success_listings_saved', 'listings');
			$_SESSION['saved_listings'][] = $listing_id;
		}
		return $return;
	}

	/**
	 * Save listing into comparison list
	 * @param integer $listing_id listing identifier
	 */
	public function save($listing_id){
		$res = $this->_save_listing($listing_id);
		if($res['error']){
			$this->system_messages->add_message('error', $res['message']);
		} else {
			$this->system_messages->add_message('success', $res['message']);
		}

		$url = site_url().'listings/view/'.$listing_id;
		redirect($url);

	}

	/**
	 * Save listing into comparison list by ajax
	 * @param integer $listing_id listing identifier
	 */
	public function ajax_save($listing_id){
		echo json_encode($this->_save_listing($listing_id));
	}

	/**
	 * Remove listing from comparison list
	 * @param integer $listing_id listing identifier
	 */
	private function _delete_saved($listing_id){
		$return = array('error'=>1, 'success'=>0, 'message'=>'');
		if($listing_id){
			if($this->session->userdata('auth_type') == 'user'){
				$saved_listings = $this->session->userdata('saved_listings');
				if(!empty($saved_listings)){
					$index = array_search($listing_id, $saved_listings);
					if($index !== false) unset($saved_listings[$index]);
					$this->session->set_userdata('saved_listings', $saved_listings);
				}
				$user_id = $this->session->userdata('user_id');
				$this->load->model('listings/models/Saved_listings_model');
				$this->Saved_listings_model->delete_saved_by_listing($user_id, $listing_id);
			}elseif(isset($_SESSION['saved_listings'])){
				$index = array_search($listing_id, $_SESSION['saved_listings']);
				if($index !== false) unset($_SESSION['saved_listings'][$index]);
			}
			$return['success'] = 1;
			$return['error'] = 0;
			$return['message'] = l('success_delete_listing', 'listings');
		}else{
			$return['message'] = l('error_with_delete', 'listings');
		}
		return $return;
	}

	/**
	 * Remove listing from comparison list by ajax
	 * @param integer $listing_id listing identifier
	 */
	public function ajax_delete_saved($listing_id){
		echo json_encode($this->_delete_saved($listing_id));
	}

	/**
	 * Remove listing from comparison list
	 * @param integer $listing_id listing identifier
	 */
	public function delete_saved($listing_id){
		$res = $this->_delete_saved($listing_id);
		if($res['error']){
			$this->system_messages->add_message('error', $res['message']);
		}else{
			$this->system_messages->add_message('success', $res['message']);
		}
		$url = site_url().'listings/saved';
		redirect($url);
	}

	/**
	 * Render share form
	 * @param integer $listing_id listing identifier
	 */
	public function share_form($listing_id){
		$data = $this->Listings_model->get_listing_by_id($listing_id);
		$this->template_lite->assign('data', $data);
		$this->template_lite->view('share_form', 'user', 'listings');
	}
	
	/**
	 * Render share form
	 * @param integer $listing_id listing identifier
	 */
	public function ajax_share_form($listing_id){
		$data = $this->Listings_model->get_listing_by_id($listing_id);
		$this->template_lite->assign('data', $data);
		$this->template_lite->view('share_form', 'user', 'listings');
	}
	
	/**
	 * Send mail
	 * @param integer $listing_id listing identifier
	 */
	public function ajax_share_mail($listing_id){
		$post_data = array();
		$post_data['email'] = $this->input->post('email', true);
		$post_data['user'] = $this->input->post('user', true);
		$post_data['message'] = $this->input->post('message', true);
		
		$validate_data = $this->Listings_model->validate_share($listing_id, $post_data);

		/*$is_captcha_valid = ($this->input->post('code', true) == $this->session->userdata('captcha_word')) ? 1 : 0;
		if(!$is_captcha_valid){
			$validate_data['errors'][] = l('error_invalid_captcha', 'contact');
		}*/
		
		if(!empty($validate_data['errors'])){
			$return['error'] = implode('<br>', $validate_data['errors']);
		}else{
			$email = $validate_data['data']['email'];
			unset($validate_data['data']['email']);
			
			$this->load->helper('seo');
			$this->load->library('Translit');
	
			$current_lang = $this->pg_language->get_lang_by_id($this->pg_language->current_lang_id);
			$current_lang_code = $current_lang['code'];
				
			//get listing
			$listing = $this->Listings_model->get_listing_by_id($listing_id);
			
			$validate_data['data']['listing_id'] = $listing_id;
			$validate_data['data']['listing_name'] = $listing['output_name'];
			$validate_data['data']['listing_owner'] = $listing['user']['output_name'];
			$validate_data['data']['listing_url'] = rewrite_link('listings', 'view', $listing);
				
			//send mail
			$this->load->model('Notifications_model');
			$this->Notifications_model->send_notification($email, 'share_listing', $validate_data['data']);
			
			$return['success'] = l('success_share_send', 'listings');
		}
		
		echo json_encode($return);
		return;
	}
	
	/**
	 * Return user listings block
	 * @param integer $user user data
	 * @param string $operation_type operation type
	 * @param string $order sorting order
	 * @param string $order_direction order direction
	 * @param integer $page page of results
	 */
	private function _user_listings_block($user, $operation_type, $order=null, $order_direction=null, $page=null){
		$current_settings = isset($_SESSION['user_listings'])?$_SESSION['user_listings']:array();
		if(!isset($current_settings['order']))
			$current_settings['order'] = 'modified';
		if(!isset($current_settings['order_direction']))
			$current_settings['order_direction'] = 'DESC';
		if(!isset($current_settings['page']))
			$current_settings['page'] = 1;
		
		$view_mode = isset($_SESSION['listings_view_mode']) ? $_SESSION['listings_view_mode'] : 'list';

		$filters['type'] = $operation_type;
		$filters['user'] = $user['id'];
		$filters['active'] = 1;
		
		if(!$order) $order = $current_settings['order'];
		$this->template_lite->assign('order', $order);
		$current_settings['order'] = $order;

		if (!$order_direction) $order_direction = $current_settings['order_direction'];
		$this->template_lite->assign('order_direction', $order_direction);
		$current_settings['order_direction'] = $order_direction;

		$page = intval($page);
		if(!$page) $page = $current_settings['page'];
		
		$items_on_page = $this->pg_module->get_module_config('start', 'index_items_per_page');
	
		// save criteria for pagination
		$_SESSION['listings_pagination'] = array(
			'order' => $current_settings['order'],
			'order_direction' => $current_settings['order_direction'],
			'page' => $current_settings['page'],
			'data' => $filters,
		);
		
		$this->load->helper('seo');
		$action_data = array('id_user'=>$user['id'], 'user'=>$user['name'], 'operation_type'=>$operation_type);
		$url = rewrite_link('listings', 'user', $action_data);
		
		// sorting
		$sort_data = array(
			'url' => $url,
			'order' => $order,
			'direction' => $order_direction,
			'links' => array(
				'views' => l('field_views', 'listings'),
				'price' => l('field_price', 'listings'),
				'open' => l('field_date_open', 'listings'),
				'modified' => l('field_date_modified', 'listings'),
			),
		);
		if($this->pg_module->is_module_installed('reviews')){
			$this->load->helper('reviews');
			$review_link = get_reviews_sorter();
			if(is_array($review_link)){
				$first = array_splice($sort_data['links'], 0, 1);
				$sort_data['links'] = array_merge($first, $review_link, $sort_data['links']);
			}
		}
		$this->template_lite->assign('sort_data', $sort_data);
	
	
		$use_map_in_search = $view_mode == 'map';
		$this->template_lite->assign('use_map_in_search', $use_map_in_search);
	
		$listings_count = $this->Listings_model->get_listings_count($filters);
		if($listings_count > 0){
			$order_array = array();
			if($order == 'price'){
				$order_array['price_sorting'] = $order_direction;
			}elseif($order == 'open'){
				$order_array['date_open'] = $order_direction;
			}elseif($order == 'modified'){
				$order_array['date_modified'] = $order_direction;
			}else{
				$order_array[$order] = $order_direction;
			}
			if($order == 'price'){
				$order_array = array_merge(array('price_negotiated'=>$order_direction == 'ASC' ? 'DESC' : 'ASC'), $order_array);
			}
			
			$this->Listings_model->set_format_settings('get_saved_listings', true);
			$listings = $this->Listings_model->get_listings_list($filters, $page, $items_on_page, $order_array);
			
			#MOD#
			$residen = $this->Listings_model->get_residensial();
			$commer = $this->Listings_model->get_commercial();

			$this->Listings_model->set_format_settings('get_saved_listings', false);
			$this->template_lite->assign('listings', $listings);
			
			$this->template_lite->assign('residential', $residen);
			$this->template_lite->assign('commercial', $commer);
			#MOD#
			
			if($use_map_in_search){
				$markers = array();
				foreach($listings as $listing){
					if((float)$listing['lat'] == 0 && (float)$listing['lon'] == 0) continue;
					$this->template_lite->assign('view_mode', $view_mode);
					$this->template_lite->assign('listing', $listing);
					$info = $this->template_lite->fetch('listing_map_block', 'user', 'listings');
					$markers[] = array( 
						'gid' => $listing['id'],
						'country' => $listing['country'], 
						'region' => $listing['region'], 
						'city' => $listing['city'], 
						'address' => $listing['address'], 
						'postal_code' => $listing['zip'], 
						'lat' => (float)$listing['lat'], 
						'lon' => (float)$listing['lon'], 
						'info' => $info,
					);
				}
				$this->template_lite->assign('markers', $markers);
			}
		}
		
		if($use_map_in_search){
			$current_language = $this->pg_language->get_lang_by_id($this->pg_language->current_lang_id);
			$view_settings = array(
				'lang' => $current_language['code'],
			);			
			$this->template_lite->assign('map_settings', $view_settings);
		}
	
		$this->load->helper('sort_order');
		$page = get_exists_page_number($page, $listings_count, $items_on_page);
		$current_settings['page'] = $page;
		
		$_SESSION['user_listings'] = $current_settings;
		
		$this->load->helper('navigation');
		$this->config->load('date_formats', TRUE);
		$page_data = get_user_pages_data($url.'/'.$order.'/'.$order_direction.'/', $listings_count, $items_on_page, $page, 'briefPage');
		$page_data['date_format'] = $this->config->item('st_format_date_literal', 'date_formats');
		$this->template_lite->assign('page_data', $page_data);
		
		$is_owner = 0;
		if($this->session->userdata('auth_type') == 'user'){
			if($this->session->userdata('user_id') == $user['id'])	$is_owner = 1;
		}else{
			$this->template_lite->assign('is_guest', 1);
		}
		$this->template_lite->assign('is_listing_owner', $is_owner);
		
		$this->template_lite->assign('tstamp', time());

		return $this->template_lite->fetch('listings_block_'.$view_mode, 'user', 'listings');
	}
	
	/**
	 * Render user's listings
	 * @param integer $user_id user identifier
	 * @param string $operation_type operation type
	 * @param string $order sorting order
	 * @param string $order_direction order direction
	 * @param integer $page page of results
	 */
	public function user($user_id, $operation_type=null, $order='modified', $order_direction='DESC', $page=1){
		$this->load->model('Users_model');		
		$this->Users_model->set_format_settings(array('get_contact'=>true, 'get_company'=>true));
		$user = $this->Users_model->get_user_by_id($user_id, true);
		$this->Users_model->set_format_settings(array('get_contact'=>false, 'get_company'=>false));
		if(!$user) show_404();
		$this->template_lite->assign('user', $user);
		
		$operation_types = $this->Listings_model->get_operation_types(true);
		$this->template_lite->assign('operation_types', $operation_types);
		
		if(!$operation_type) $operation_type = $this->input->post('type');
		if(!$operation_type || !in_array($operation_type, $operation_types)) 
			$operation_type = $this->Listings_model->get_operation_type_gid_default();
		$this->template_lite->assign('current_operation_type', $operation_type);
		
		$block_content = $this->_user_listings_block($user, $operation_type, $order, $order_direction, $page);
		$this->template_lite->assign('block', $block_content);
		
		$this->load->helper('seo');
		$user_view_link = rewrite_link('users', 'view', $user);
		$this->pg_seo->set_seo_data(array('name'=>$user['name'], 'user_type'=>$user['user_type'], 'user_type_str'=>$user['user_type_str'], 'operation_type'=>$operation_type, 'operation_type_str'=>l('operation_search_'.$operation_type, 'listings')));
		
		$menu_action_data = array('id_user'=>$user['id'], 'user'=>$user['name'], 'operation_type'=>'[operation_type]');
		$menu_action_link = rewrite_link('listings', 'user', $menu_action_data);
		$this->template_lite->assign('menu_action_link', $menu_action_link);
		
		$this->Menu_model->breadcrumbs_set_active($user['output_name'], $user_view_link);
		$this->Menu_model->breadcrumbs_set_active(l('text_user_listing', 'listings'));

		$current_settings = isset($_SESSION['user_listings'])?$_SESSION['user_listings']:array();
		if(!isset($current_settings['order'])) $current_settings['order'] = 'date_created';
		$this->template_lite->assign('order', $current_settings['order']);
		
		if(!isset($current_settings['order_direction'])) $current_settings['order_direction'] = 'DESC';
		$this->template_lite->assign('order_direction', $current_settings['order_direction']);
		
		if(!isset($current_settings['page'])) $current_settings['page'] = 1;
		$this->template_lite->assign('page', $current_settings['page']);

		$view_mode = isset($_SESSION['listings_view_mode']) ? $_SESSION['listings_view_mode'] : 'list';

		$this->template_lite->view('user_listings_'.$view_mode, 'user', 'listings');	
	}
	
	/**
	 * Render user's listings by ajax
	 * @param integer $user_id user identifier
	 * @param string $operation_type operation type
	 * @param string $order sorting order
	 * @param string $order_direction order direction
	 * @param integer $page page of results
	 */
	 
	public function ajax_user($user_id, $operation_type, $order='modified', $order_direction='DESC', $page=1){
		$this->load->model('Users_model');		
		$user = $this->Users_model->get_user_by_id($user_id, true);
		if(!$user) exit;
		if(!$operation_type) $operation_type = $this->input->post('type');
		echo $this->_user_listings_block($user, $operation_type, $order, $order_direction, $page);
		exit;
	}
	
	/**
	 * Return upload photo form by ajax
	 * @param integer $listing_id listing identifier
	 * @param integer $photo_id photo identifier
	 */
	public function ajax_photo_form($listing_id, $photo_id=0){
		if($this->session->userdata('auth_type') != 'user'){show_404();}

		$listing_id = intval($listing_id);
		if(!$listing_id) show_404();

		$user_id = $this->session->userdata('user_id');
		$this->Listings_model->set_format_settings('use_format', false);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);
		if($user_id != $listing['id_user']){show_404();}
	
		$photo_id = intval($photo_id);
		if($photo_id){
		$this->load->model('Upload_gallery_model');
			$photo = $this->Upload_gallery_model->get_file_by_id($photo_id);
			if($photo['object_id'] != $listing_id){show_404();}
			$this->template_lite->assign('data', $photo);
		}
		
		$photo_type = $this->Listings_model->get_gallery_type();

		$this->load->model('uploads/models/Uploads_config_model');
		$upload_config = $this->Uploads_config_model->get_config_by_gid($photo_type['gid_upload_config']);
		$upload_config['file_formats'] = implode(', ', unserialize($upload_config['file_formats']));
		$upload_config['requirements_str'] = l('file_upload_settings_str', 'listings');
		$upload_config['requirements_str'] = str_replace('[width]', $upload_config['max_width'], $upload_config['requirements_str']);
		$upload_config['requirements_str'] = str_replace('[height]', $upload_config['max_height'], $upload_config['requirements_str']);
		$upload_config['requirements_str'] = str_replace('[formats]', $upload_config['file_formats'], $upload_config['requirements_str']);
		$this->template_lite->assign('upload_config', $upload_config);
		
		$selections = array();
		$thumbs_config = $this->Uploads_config_model->get_config_thumbs($upload_config['id']);
		foreach($thumbs_config as $thumb_config){
			$selections[$thumb_config['prefix']] = array(
				'width' => $thumb_config['width'],
				'height' => $thumb_config['height'],
			);
		}
		$this->template_lite->assign('selections', $selections);
		
		$this->template_lite->view('my_photo_edit_form');
	}

	/**
	 * Photo upload
	 * @param integer $listing_id listing identifier
	 * @param integer $photo_id photo identifier
	 */
	public function photo_upload($listing_id, $photo_id=0){
		$listing_id = intval($listing_id);
		if(!$listing_id) show_404();
		
		$photo_id = intval($photo_id);

		$user_id = $this->session->userdata('user_id');
		$this->Listings_model->set_format_settings('use_format', false);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);
		if($user_id != $listing['id_user']){show_404();}

		if($photo_id){
			$this->load->model('Upload_gallery_model');
			$photo = $this->Upload_gallery_model->get_file_by_id($photo_id);
			if($listing_id != $photo['object_id']){show_404();}
		}

		$return = $this->Listings_model->save_photo($listing_id, $photo_id, 'photo_file');
		
		if($this->input->post('no_ajax', true)==1){
			if(!empty($return['errors'])){
				$this->system_messages->add_message('errors', implode('<br>', $return['errors']));
			}
			redirect(site_url().'listings/edit/'.$listing_id.'/gallery');
		}else{
			$return['errors'] = $return['error'] = implode('<br>', $return['errors']);
			echo json_encode($return); return;
		}
	}

	/**
	 * Save photo
	 * @param integer $listing_id listing identifier
	 * @param integer $photo_id photo identifier
	 */
	public function ajax_save_photo_data($listing_id, $photo_id){
		if($this->session->userdata('auth_type') != 'user'){show_404();}

		$photo_id = intval($photo_id);
		if(!$photo_id){show_404();}
		
		$this->load->model('Upload_gallery_model');
		
		$photo = $this->Upload_gallery_model->get_file_by_id($photo_id);
		if(!$photo['object_id']){show_404();}
		
		$user_id = $this->session->userdata('user_id');
		$this->Listings_model->set_format_settings('use_format', false);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);
		if($user_id != $listing['id_user']){show_404();}

		$photo_type = $this->Listings_model->get_gallery_type();

		$post_data['comment'] = $this->input->post('comment', true);
		$validate_data = $this->Upload_gallery_model->validate_file_data($photo_id, $photo_type['id'], $post_data);
		if(!empty($validate_data['errors'])){
			$return['errors'] = $validate_data['errors'];
		}else{
			$data = $validate_data['data'];
			$data['type_id'] = $photo_type['id'];
			$data['object_id'] = $photo['object_id'];
			$return = $this->Upload_gallery_model->save_file($photo_id, $data, '', true);
		}

		$return['error'] = implode('<br>', $return['errors']);
		$return['success'] = (!empty($return['errors']))?'':l('photo_successfully_saved', 'listings');
		echo json_encode($return);
	}
	
	/**
	 * Remove photo by ajax
	 * @param integer $listing_id listing identifier
	 * @param integer $photo_id photo identifier
	 */
	public function ajax_delete_photo($listing_id, $photo_id){
		if($this->session->userdata('auth_type') != 'user'){show_404();}

		$listing_id = intval($listing_id);
		if(!$listing_id) show_404();
		
		$photo_id = intval($photo_id);
		if(!$photo_id) show_404();
		
		$this->Listings_model->set_format_settings('get_photos_all', true);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('get_photos_all', false);
		
		$user_id = $this->session->userdata('user_id');
		if($user_id != $listing['id_user']){show_404();}

		// remove photo
		$result = $this->Listings_model->delete_photo($listing_id, $photo_id);
		if(!$result) show_404();
		
		$return['success'] = l('photo_successfully_deleted', 'listings');
		echo json_encode($return);
	}

	/**
	 * Resort photos
	 * @param integer $listing_id listing identifier
	 */
	public function ajax_save_photo_sorting($listing_id){
		if($this->session->userdata('auth_type') != 'user'){show_404();}

		$listing_id = intval($listing_id);
		if(!$listing_id) show_404();
		
		$user_id = $this->session->userdata('user_id');
		$this->Listings_model->set_format_settings('get_photos_all', true);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('get_photos_all', false);
		if($user_id != $listing['id_user']){show_404();}

		$this->load->model('Upload_gallery_model');
		$sorter = (array)$this->input->post('sorter', true);

		$sorter_data = array();
		foreach($sorter as $item_str => $sort_index){
			$photo_id = intval(str_replace('pItem', '', $item_str));
			$sorter_data[$photo_id] = $sort_index;
		}
			
		if(!empty($sorter_data)){
			$this->Listings_model->set_photo_sorter($listing_id, $sorter_data);
		}	
				
		$return['success'] = l('photo_sorter_successfully_saved', 'listings');
		echo json_encode($return);
	}

	/**
	 * Reload photo block
	 * @param integer $listing_id listing identifier
	 * @param integer $photo_id photo identifier
	 */
	public function ajax_reload_photo_block($listing_id, $photo_id){
		if($this->session->userdata('auth_type') != 'user'){show_404();}

		$listing_id = intval($listing_id);
		if(!$listing_id) show_404();

		$user_id = $this->session->userdata('user_id');
		$this->Listings_model->set_format_settings('use_format', false);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);
		if($user_id != $listing['id_user']){show_404();}
				
		$photo_id = intval($photo_id);
		if(!$photo_id)	show_404();

		$this->load->model('Upload_gallery_model');
		$photo = $this->Upload_gallery_model->get_file_by_id($photo_id);
		if($listing_id != $photo['object_id']){show_404();}
				
		$this->template_lite->assign('photo', $photo);
		$this->template_lite->view('photo_view_block');
	}
	
	/**
	 * Return upload panorama form by ajax
	 * @param integer $listing_id listing identifier
	 * @param integer $panorama_id panorama identifier
	 */
	public function ajax_panorama_form($listing_id, $panorama_id=0){
		if($this->session->userdata('auth_type') != 'user'){show_404();}

		$listing_id = intval($listing_id);
		if(!$listing_id) show_404();

		$user_id = $this->session->userdata('user_id');
		$this->Listings_model->set_format_settings('use_format', false);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);
		if($user_id != $listing['id_user']){show_404();}
	
		$panorama_id = intval($panorama_id);

		$this->load->model('Upload_gallery_model');
		if($panorama_id){
			$panorama = $this->Upload_gallery_model->get_file_by_id($panorama_id);
			if($panorama['object_id'] != $listing_id){show_404();}
			$this->template_lite->assign('data', $panorama);
		}
		
		$virtual_tour_type = $this->Listings_model->get_vtour_type();

		$this->load->model('uploads/models/Uploads_config_model');
		$upload_config = $this->Uploads_config_model->get_config_by_gid($virtual_tour_type['gid_upload_config']);
		$upload_config['file_formats'] = implode(', ', unserialize($upload_config['file_formats']));
		$upload_config['requirements_str'] = l('file_upload_settings_str', 'listings');
		$upload_config['requirements_str'] = str_replace('[width]', $upload_config['max_width'], $upload_config['requirements_str']);
		$upload_config['requirements_str'] = str_replace('[height]', $upload_config['max_height'], $upload_config['requirements_str']);
		$upload_config['requirements_str'] = str_replace('[formats]', $upload_config['file_formats'], $upload_config['requirements_str']);
		$this->template_lite->assign('upload_config', $upload_config);

		$selections = array();
		$thumbs_config = $this->Uploads_config_model->get_config_thumbs($upload_config['id']);
		foreach($thumbs_config as $thumb_config){
			$selections[$thumb_config['prefix']] = array(
				'width' => $thumb_config['width'],
				'height' => $thumb_config['height'],
			);
		}
		$this->template_lite->assign('selections', $selections);

		$this->template_lite->view('my_panorama_edit_form');
	}

	/**
	 * Panorama upload
	 * @param integer $listing_id listing identifier
	 * @param integer $panorama_id panorama identifier
	 */
	public function panorama_upload($listing_id, $panorama_id=0){
		if($this->session->userdata('auth_type') != 'user'){show_404();}

		$listing_id = intval($listing_id);
		if(!$listing_id) show_404();

		$user_id = $this->session->userdata('user_id');
		$this->Listings_model->set_format_settings('use_format', false);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);
		if($user_id != $listing['id_user']){show_404();}

		$panorama_id = intval($panorama_id);

		$this->load->model('Upload_gallery_model');
		if($panorama_id){
			$panorama = $this->Upload_gallery_model->get_file_by_id($panorama_id);
			if($listing_id != $panorama['object_id']){show_404();}
		}

		$return = $this->Listings_model->save_panorama($listing_id, $panorama_id, 'panorama_file');	
		if($this->input->post('no_ajax', true)==1){
			if(!empty($return['errors'])){
				$this->system_messages->add_message('errors', implode('<br>', $return['errors']));
			}
			redirect(site_url().'listings/edit/'.$listing_id.'/gallery');
		}else{	
			$return['errors'] = $return['error'] = implode('<br>', $return['errors']);
			echo json_encode($return);
		}
	}

	/**
	 * Save panorama
	 * @param integer $listing_id listing identifier
	 * @param integer $panorama_id panorama identifier
	 */
	public function ajax_save_panorama_data($listing_id, $panorama_id){
		if($this->session->userdata('auth_type') != 'user'){show_404();}

		$listing_id = intval($listing_id);
		if(!$listing_id) show_404();

		$this->Listings_model->set_format_settings('use_format', false);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);
		
		$user_id = $this->session->userdata('user_id');
		if($user_id != $listing['id_user']){show_404();}

		$panorama = intval($panorama_id);

		$this->load->model('Upload_gallery_model');
		if($panorama_id){
			$panorama = $this->Upload_gallery_model->get_file_by_id($panorama_id);
			if($listing_id != $panorama['object_id']){show_404();}
		}
		
		$virtual_tour_type = $this->Listings_model->get_vtour_type();

		$post_data['comment'] = $this->input->post('comment', true);
		$validate_data = $this->Upload_gallery_model->validate_file_data($panorama_id, $virtual_tour_type['id'], $post_data);
		if(!empty($validate_data['errors'])){
			$return['errors'] = $validate_data['errors'];
		}else{
			$data = $validate_data['data'];
			$data['type_id'] = $virtual_tour_type['id'];
			$data['object_id'] = $listing_id;
			$return = $this->Upload_gallery_model->save_file($panorama_id, $data);
		}

		$return['error'] = implode('<br>', $return['errors']);
		$return['success'] = (!empty($return['errors']))?'':l('panorama_successfully_saved', 'listings');
		echo json_encode($return);
	}
	
	/**
	 * Remove panorama by ajax
	 * @param integer $listing_id listing identifier
	 * @param integer $panorama_id panorama identifier
	 */
	public function ajax_delete_panorama($listing_id, $panorama_id){
		if($this->session->userdata('auth_type') != 'user'){show_404();}

		$listing_id = intval($listing_id);
		if(!$listing_id) show_404();
		
		$panorama_id = intval($panorama_id);
		if(!$panorama_id) show_404();

		$this->Listings_model->set_format_settings('use_format', false);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);
		
		$user_id = $this->session->userdata('user_id');
		if($user_id != $listing['id_user']){show_404();}

		$result = $this->Listings_model->delete_panorama($listing_id, $panorama_id);
		if($result){
			$return['success'] = l('panorama_successfully_deleted', 'listings');
		}else{
			$return['error'] = '';
		}
		echo json_encode($return); return;
	}

	/**
	 * Resort virtual tour
	 * @param integer $listing_id listing identifier
	 */
	public function ajax_save_panorama_sorting($listing_id){
		if($this->session->userdata('auth_type') != 'user'){show_404();}

		$listing_id = intval($listing_id);
		if(!$listing_id) show_404();

		$this->Listings_model->set_format_settings('use_format', false);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);
		
		$user_id = $this->session->userdata('user_id');
		if($user_id != $listing['id_user']){show_404();}

		$this->load->model('Upload_gallery_model');
		$sorter = (array)$this->input->post('sorter', true);

		$sorter_data = array();
		foreach($sorter as $item_str=>$sort_index){
			$panorama_id = intval(str_replace('pItem', '', $item_str));
			$sorter_data[$panorama_id] = $sort_index;
		}
		
		if(!empty($sorter_data)){
			$this->Listings_model->set_panorama_sorter($listing_id, $sorter_data);
		}
		
		$return['success'] = l('panorama_sorter_successfully_saved', 'listings');
		echo json_encode($return); return;
	}

	/**
	 * Reload panorama block
	 * @param integer $listing_id listing identifier
	 * @param integer $panorama_id panorama identifier
	 */
	public function ajax_reload_panorama_block($listing_id, $panorama_id){
		$listing_id = intval($listing_id);
		if(!$listing_id) show_404();

		$this->Listings_model->set_format_settings('use_format', false);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);
		
		$user_id = $this->session->userdata('user_id');
		if($user_id != $listing['id_user']){show_404();}
				
		$panorama_id = intval($panorama_id);
		if(!$panorama_id) show_404();

		$this->load->model('Upload_gallery_model');
		$panorama = $this->Upload_gallery_model->get_file_by_id($panorama_id);
		if($listing_id != $panorama['object_id']){show_404();}	
				
		$this->template_lite->assign('panorama', $panorama);
		$this->template_lite->view('panorama_view_block');
	}
	
	/**
	 * Reload panorama block
	 * @param integer $listing_id listing identifier
	 * @param integer $panorama_id panorama identifier
	 */
	public function ajax_reload_panorama($listing_id, $panorama_id){
		$listing_id = intval($listing_id);
		if(!$listing_id) show_404();

		$this->Listings_model->set_format_settings('use_format', false);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);
		
		$user_id = $this->session->userdata('user_id');
		if($user_id != $listing['id_user']){show_404();}
		
		$panorama_id = intval($panorama_id);
		if(!$panorama_id) show_404();

		$this->load->model('Upload_gallery_model');
		$panorama = $this->Upload_gallery_model->get_file_by_id($panorama_id);
		if($listing_id != $panorama['object_id']){show_404();}	
		
		$this->load->helpers('listings');
		
		echo virtual_tour_block(array('data'=>$panorama));
	}
	
	/**
	 * Wish lists action
	 * @param integer $page page of results
	 */
	public function wish_lists($order='date_created', $order_direction='DESC', $page=1){
		$this->load->model('listings/models/Wish_list_model');
	
		$items_on_page = $this->pg_module->get_module_config('start', 'index_items_per_page');
		
		$filters = array();
		$filters['active'] = 1;
		
		$wish_lists_count = $this->Wish_list_model->get_wish_lists_count($filters);
		if($wish_lists_count > 0){
			$wish_lists = $this->Wish_list_model->get_wish_lists_list($filters, $page, $items_on_page, array($order => $order_direction));
			$this->template_lite->assign('wish_lists', $wish_lists);
		}

		$this->load->helper('navigation');
		$this->config->load('date_formats', TRUE);
		$url = site_url().'listings/wish_lists/';
		$page_data = get_admin_pages_data($url, $wish_lists_count, $items_on_page, $page, 'briefPage');
		$page_data['date_format'] = $this->config->item('st_format_date_time_literal', 'date_formats');
		$this->template_lite->assign('page_data', $page_data);
	
		$this->Menu_model->breadcrumbs_set_active(l('header_wish_list', 'listings'), site_url().'listings/wish_lists');		
		$this->template_lite->view('wish_lists');
	}
	
	/**
	 * Show wish list action
	 * @param integer $wish_list_id wish list identifier
	 * @param integer $page page of results
	 */
	public function wish_list($wish_list_id, $page=1){
		$content = $this->_wish_list_content($wish_list_id, $page);
		$this->template_lite->assign('content', $content);
	
		$wish_list = $this->Wish_list_model->get_wish_list_by_id($wish_list_id, true);	
		
		$this->Menu_model->breadcrumbs_set_active(l('header_wish_list', 'listings'), site_url().'listings/wish_lists');
		$this->Menu_model->breadcrumbs_set_active($wish_list['output_name'], site_url().'listings/wish_list/'.$wish_list_id);
		$this->pg_seo->set_seo_data($wish_list);
		$this->template_lite->view('wish_list_view');
	}
	
	/**
	 * Show wish list action by ajax
	 * @param integer $wish_list_id wish list identifier
	 * @param integer $page page of results
	 */
	public function ajax_wish_list($wish_list_id, $page=1){
		echo $this->_wish_list_content($wish_list_id, $page);
		exit;
	}
	
	/**
	 * Wish list content
	 * @param integer $wish_list_id wish list identifier
	 * @param integer $page page of results
	 */
	private function _wish_list_content($wish_list_id, $page=1){
		$this->load->model('listings/models/Wish_list_model');
		
		$this->Wish_list_model->set_format_content_settings('get_listings', true);
		$wish_list = $this->Wish_list_model->get_wish_list_by_id($wish_list_id, true);
		$this->Wish_list_model->set_format_content_settings('get_listings', false);
		if(!$wish_list || !$wish_list['status']) show_404();		
		$this->template_lite->assign('wish_list', $wish_list);

		$page = max(intval($page), 1);
		
		$items_on_page = $this->pg_module->get_module_config('listings', 'rows_per_page')*4;
	
		$listings_count = $this->Wish_list_model->get_wish_list_content_count($wish_list_id);
		if($listings_count > 0){
			$this->Wish_list_model->set_format_content_settings('get_listings', true);
			$listings = $this->Wish_list_model->get_wish_list_content_list($wish_list_id, $page, $items_on_page);
			$this->Wish_list_model->set_format_content_settings('get_listings', false);	
			$this->template_lite->assign('listings', $listings);
		}
		

		$this->load->helper('navigation');
		$this->config->load('date_formats', TRUE);
		$url = site_url().'listings/wish_list/'.$wish_list_id.'/';
		$page_data = get_user_pages_data($url, $listings_count, $items_on_page, $page, 'briefPage');
		$page_data['date_format'] = $this->config->item('st_format_date_time_literal', 'date_formats');
		$this->template_lite->assign('page_data', $page_data);
		
		return $this->template_lite->fetch('wish_list_content', 'user', 'listings');
	}
	
	/**
	 * Return form for choise wish lists
	 * @param integer $max_select max wish list for selected
	 * @param string $template template name
	 */
	public function ajax_get_wish_lists_form($max_select=1, $template='default'){
		$selected = $this->input->post('selected', true);

		$this->load->model('listings/models/Wish_list_model');
		
		if(!empty($selected)){
			$filters['ids'] = $selected;
			$data['selected'] = $this->Wish_list_model->get_wish_lists_list($filters, null, null, array('name'=>'asc'));
		}else{
			$data['selected'] = array();
		}
		$data['max_select'] = $max_select ? $max_select : 0;

		$this->template_lite->assign('select_data', $data);
		$this->template_lite->view('ajax_wish_list_select_form_'.$template);
	}
	
	/**
	 * Return data of wish lists
	 * @param integer $page page of results
	 */
	public function ajax_get_wish_lists($page=1){
		$selected = $this->input->post('selected', true);
		$search_string = trim(strip_tags($this->input->post('search', true)));
		$filters = $return = array();
		$items_on_page = 100;

		if(!empty($search_string)){
			$filters['keyword'] = $search_string;
		}
		if(!empty($selected)){
			$filters['not_ids'] = $selected;
		}
		
		$this->load->model('listings/models/Wish_list_model');

		$return['all'] = $this->Wish_list_model->get_wish_lists_count($filters);
		$return['pages'] = ceil($return['all']/$items_on_page);
		$return['current_page'] = $page;
		$return['items'] = $this->Wish_list_model->get_wish_lists_list($filters, $page, $items_on_page, array('name'=>'asc'));

		echo json_encode($return);
	}
	
	/**
	 * Return data of selected wish lists
	 */
	public function ajax_get_selected_wish_lists(){
		$selected = $this->input->post('selected', true);

		$this->load->model('listings/models/Wish_list_model');
		
		if(!empty($selected)){
			$filters['ids'] = $selected;
			$return = $this->Wish_list_model->get_wish_lists_list($filters, null, null, array('name'=>'asc'));
		}else{
			$return = array();
		}
		echo json_encode($return);
		exit;
	}
	
	/**
	 * @param integer $max maximum selected property types
	 * @param type $max max properties
	 */
	public function ajax_get_property_type_form($max=1){
		$property_types = array();
		
		$this->load->model('Properties_model');
		$initial_types = $this->Properties_model->get_categories('property_types', $this->pg_language->current_lang_id);
		$initial_items = $this->Properties_model->get_all_properties('property_types', $this->pg_language->current_lang_id);
				
		foreach($initial_types as $type_gid=>$type_data){
			$property_types[$type_gid] = array(
				'gid' => $type_gid,
				'name' => $type_data['value'],
				'parent' => 0,
			);

			foreach($initial_items[$type_gid] as $item_gid=>$item_data){
				$property_types[$type_gid.'_'.$item_gid] = array(
					'gid' => $item_gid,
					'name' => $item_data['value'],
					'parent' => 0,
				);
			}
		}
		
		$data['max'] = $max;
		$data['property_types'] = $property_types;

		$this->template_lite->assign('data', $data);
		$this->template_lite->view('ajax_property_type_form');
	}
	
	/**
	 * Return property types data
	 * @param sring $var selected property type GUID
	 * @return void
	 */
	public function ajax_get_property_type_data($var) {
		$data = array();

		$property_types = array();

		$this->load->model('Properties_model');
		$initial_types = $this->Properties_model->get_categories('property_types', $this->pg_language->current_lang_id);
		$initial_items = $this->Properties_model->get_all_properties('property_types', $this->pg_language->current_lang_id);

		$var = intval($var);
		
		$i = 1;
		
		foreach($initial_types as $type_gid=>$type_data){
			$id = $i++;
			$parent = $id;
			if(!$var){
				$property_types[$type_gid] = array('id'=>$id, 'gid'=>$type_gid, 'name'=>$type_data, 'parent'=>0);
			}elseif($type_gid == $var){
				$data['current_property_type'] = array('id'=>$id, 'gid'=>$type_gid, 'name'=>$type_data, 'parent'=>0);
			}
			foreach($initial_items[$type_gid] as $item_gid=>$item_data){
				$id = $i++;
				if(!$var || $type_gid == $var){
					$property_types[$id] = array(
						'id' => $id,
						'gid' => $item_gid,
						'name' => $item_data,
						'parent' => $parent,
					);
				}
			}
		}
		$data['property_types'] = $property_types;
		
		echo json_encode($data);
		return;
	}
	
	/**
	 * Set listing coordinates after geocode
	 * @param integer $listing_id listing identifier
	 */
	public function ajax_set_map($listing_id, $param){
		$user_id = $this->session->userdata('user_id');
		
		$this->Listings_model->set_format_settings('use_format', false);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);
		
		if(!$listing) return;
	
		if($listing['id_user'] == $user_id) $user_id = 0;
		
		$this->load->model('Geomap_model');
		$this->load->model('geomap/models/Geomap_settings_model');
		
		$map_gid = $this->Geomap_model->get_default_driver_gid();
		$settings = $this->Geomap_settings_model->get_settings($map_gid, $user_id, $listing_id, 'listing_view');
		if(!$settings) $settings = array('gid'=>'listing_view');
		switch($param){
			case 'type': $settings['view_type'] = $this->input->post('type', true); break;
			case 'zoom': $settings['zoom'] = $this->input->post('zoom', true); break;
			case 'coordinates':
				$settings['lat'] = $this->input->post('lat', true);
				$settings['lon'] = $this->input->post('lon', true);
			break;
		}
		
		$validate_data = $this->Geomap_settings_model->validate_settings($settings);
		if(empty($validate_data['errors'])){
			$this->Geomap_settings_model->save_settings($map_gid, $user_id, $listing_id, 'listing_view', $validate_data['data']);
		}
	
		if($param == 'coordinates'){
			$data['lat'] = $this->input->post('lat', true);
			$data['lon'] = $this->input->post('lon', true);
			$this->Listings_model->save_listing($listing_id, $data);
		}
	}
	
	/**
	 * Load laguages for mortgage calculator
	 */
	public function ajax_mortgage_calc(){
		$result = array();
		$result['alert1'] = l('mortgage_calc_alert1', 'listings');
		$result['alert2'] = l('mortgage_calc_alert2', 'listings');
		$result['alert3'] = l('mortgage_calc_alert3', 'listings');
		$result['alert4'] = l('mortgage_calc_alert4', 'listings');
		$result['mes1'] = l('mortgage_calc_mes1', 'listings');
		$result['mes2'] = l('mortgage_calc_mes2', 'listings');
		$result['mes3'] = l('mortgage_calc_mes3', 'listings');
		$result['mes4'] = l('mortgage_calc_mes4', 'listings');
		$result['mes5'] = l('mortgage_calc_mes5', 'listings');
		$result['mes6'] = l('mortgage_calc_mes6', 'listings');
		$result['mes7'] = l('mortgage_calc_mes7', 'listings');
		$result['mes8'] = l('mortgage_calc_mes8', 'listings');
		$result['mes9'] = l('mortgage_calc_mes9', 'listings');
		$result['mes10'] = l('mortgage_calc_mes10', 'listings');
		$result['mes11'] = l('mortgage_calc_mes11', 'listings');
		$result['mes12'] = l('mortgage_calc_mes12', 'listings');
		$result['mes13'] = l('mortgage_calc_mes13', 'listings');
		$result['content1'] = l('mortgage_calc_content1', 'listings');
		$result['content2'] = l('mortgage_calc_content2', 'listings');		
		echo json_encode($result);
		exit;
	}
	
	/**
	 * Close moderation alert
	 * @param integer $listing_id listing identifier
	 */
	public function ajax_close_alert($listing_id){
		$user_id = $this->session->userdata('user_id');
		$return = array('success' => 0);

		$this->Listings_model->set_format_settings('use_format', false);
		$data = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);

		if($user_id == $data['id_user']){
			$this->load->model('listings/models/Listings_moderation_model');
			$this->Listings_moderation_model->delete_alert($listing_id);
			$return['success'] = 1;
		}
		echo json_encode($return);
		return;
	}

	/**
	 * Generate rss
	 * @param string rss_gid criteria identifier
	 */
	public function rss($rss_gid){
		$this->load->model('listings/models/Listings_rss_model');
		
		$rss_settings = $this->Listings_model->get_rss_settings();
		$this->load->library('rssfeed');
		$current_lang = $this->pg_language->languages[$this->pg_language->current_lang_id];

		$this->rssfeed->set_channel(
			site_url(),
			$rss_settings['rss_feed_channel_title'],
			$rss_settings['rss_feed_channel_description'],
			$current_lang['code']
		);

		if($rss_settings['rss_feed_image_url']){
			$this->rssfeed->set_image(
				$rss_settings['rss_feed_image_media']['thumbs']['rss'],
				$rss_settings['rss_feed_image_title'],
				site_url()
			);
		}
		$rss = $this->Listings_rss_model->get_rss_by_gid($rss_gid);
		if(!$rss) show_404();
	
		$filters = $rss['data'];
		$filters['active'] = '1';

		$listings = $this->Listings_model->get_listings_list($filters, 1, $rss_settings['rss_listings_max_count'], array('date_add'=>'DESC'));
		if(!empty($listings)){
			$this->load->helper('seo');
			$this->load->library('Translit');
	
			$current_lang = $this->pg_language->get_lang_by_id($this->pg_language->current_lang_id);
			$current_lang_code = $current_lang['code'];
			
			foreach($listings as $item){
				$url = rewrite_link('listings', 'view', $item);
				$this->template_lite->assign('listing', $item);
				$content = $this->template_lite->fetch('listing_rss_block', 'user', 'listings');
				$this->rssfeed->set_item($url, $item['output_name'], $content, $item['date_modified']);
			}
		}
		$this->rssfeed->send();
		return;
	}
	
	/**
	 * Return visitors block
	 * @param string $field_name visitor/visit
	 * @param string $order sorting order 
	 * @param string $order_direction order direction
	 * @param integer $page page of results
	 */
	private function _visitors_block($field_name, $order='date_created', $order_direction='DESC', $page=1){
		$current_settings = isset($_SESSION['listings_visitors_list'])?$_SESSION['listings_visitors_list']:array();
		if(!isset($current_settings['order']))
			$current_settings['order'] = 'date_created';
		if(!isset($current_settings['order_direction']))
			$current_settings['order_direction'] = 'DESC';
		if(!isset($current_settings['page']))
			$current_settings['page'] = 1;
		if(!isset($current_settings['filters']))
			$current_settings['filters'] = array();

		$visit_type = $field_name == 'id_visitor' ? 'visitors' : 'visits';

		$filters = $current_settings['filters'];
		$filters['only_listings'] = 1;

		if(!$order) $order = $current_settings['order'];
		$this->template_lite->assign('order', $order);
		$current_settings['order'] = $order;

		if(!$order_direction) $order_direction = $current_settings['order_direction'];
		$this->template_lite->assign('order_direction', $order_direction);
		$current_settings['order_direction'] = $order_direction;

		$page = intval($page);
		if(!$page) $page = $current_settings['page'];
		$items_on_page = $this->pg_module->get_module_config('start', 'index_items_per_page');
	
		$sort_data = array(
			'url' => site_url().'listings/'.$visit_type,
			'order' => $order,
			'direction' => $order_direction,
			'links' => array(
				'visits_count' => l('field_visits_count', 'listings'), 
				'last_visit_date' => l('field_last_visit_date', 'listings'),
			)
		);
		$this->template_lite->assign('sort_data', $sort_data);
		
		$this->load->model('listings/models/Listings_visitor_model');
		
		$listings_count = $this->Listings_visitor_model->get_visitors_count($filters);
		if($listings_count > 0){
			$order_array = array($order=>$order_direction, 'id'=>$order_direction);
			$visitors = $this->Listings_visitor_model->get_visitors_list($filters, $page, $items_on_page, $order_array);
			
			$listings_for_search = array();
			foreach($visitors as $value){
				$listings_for_search[] = $value['id_listing'];
			}
			
			$listings_by_ids = array();
			$results = $this->Listings_model->get_listings_list(array('ids'=>$listings_for_search));
			foreach($results as $result){
				$listings_by_ids[$result['id']] = $result;
			}
			$listings = array();
			foreach($listings_for_search as $listing_id){
				$listings[$listing_id] = isset($listings_by_ids[$listing_id]) ? $listings_by_ids[$listing_id] :
					$this->Listings_model->format_default_listing($listing_id);
			}
			$this->template_lite->assign('listings', $listings);		
		}
		$this->template_lite->assign('listings_count', $listings_count);
	
		$this->load->helper('sort_order');
		$page = get_exists_page_number($page, $listings_count, $items_on_page);
		$current_settings['page'] = $page;

		$this->load->helper('navigation');
		$this->config->load('date_formats', TRUE);
		$url = site_url().'listings/'.$visit_type.'/'.$order.'/'.$order_direction.'/';
		$page_data = get_user_pages_data($url, $listings_count, $items_on_page, $page, 'briefPage');
		$page_data['date_format'] = $this->config->item('st_format_date_time_literal', 'date_formats');
		$this->template_lite->assign('page_data', $page_data);
		
		$_SESSION['listings_visitors_list'] = $current_settings;

		return $this->template_lite->fetch('visitors_block', 'user', 'listings');
	}
	
	/**
	 * Render visitors list action
	 * @param string $order sorting order 
	 * @param string $order_description order direction
	 * @param integer $page page of results
	 */
	public function visitors($order='date_created', $order_direction='DESC', $page=1){
		$current_settings = isset($_SESSION['listings_visitors_list'])?$_SESSION['listings_visitors_list']:array();
		
		$user_id = intval($this->session->userdata('user_id'));
		$filters = array('user'=>$user_id);
		
		$current_settings['order'] = $order;
		$current_settings['order_direction'] = $order_direction;
		$current_settings['page'] = $page;
		$current_settings['filters'] = $filters;	
		$_SESSION['listings_visitors_list'] = $current_settings;
	
		$listing_block = $this->_visitors_block('id_visitor', $order, $order_direction, $page);
		$this->template_lite->assign('block', $listing_block);
		
		$current_settings = isset($_SESSION['listings_visitors_list'])?$_SESSION['listings_visitors_list']:array();
		if(!isset($current_settings['order'])) $current_settings['order'] = 'date_created';
		$this->template_lite->assign('order', $current_settings['order']);
		
		if(!isset($current_settings['order_direction'])) $current_settings['order_direction'] = 'DESC';
		$this->template_lite->assign('order_direction', $current_settings['order_direction']);
		
		if(!isset($current_settings['page'])) $current_settings['page'] = 1;
		$this->template_lite->assign('page', $current_settings['page']);
		
		$this->Menu_model->breadcrumbs_set_active(l('text_listings_visitors', 'listings'));
		
		$this->template_lite->view('visitors', 'user', 'listings');
	}
	
	/**
	 * Render visitors list action by ajax
	 * @param string $order sorting order
	 * @param string $order_description order direction
	 * @param integer $page page of results
	 */
	public function ajax_visitors($order='date_created', $order_direction='DESC', $page=1){
		$current_settings = isset($_SESSION['listings_visitors_list'])?$_SESSION['listings_visitors_list']:array();
		
		$user_id = intval($this->session->userdata('user_id'));
		$filters = array('user'=>$user_id);
		
		$current_settings['order'] = $order;
		$current_settings['order_direction'] = $order_direction;
		$current_settings['page'] = $page;
		$current_settings['filters'] = $filters;	
		$_SESSION['listings_visitors_list'] = $current_settings;
	
		echo $this->_visitors_block('id_visitor', $order, $order_direction, $page);
	}
	
	/**
	 * Render visits list action
	 * @param string $order sorting order 
	 * @param string $order_description order direction
	 * @param integer $page page of results
	 */
	public function visits($order='date_created', $order_direction='DESC', $page=1){
		$current_settings = isset($_SESSION['listings_visitors_list'])?$_SESSION['listings_visitors_list']:array();
		
		$user_id = intval($this->session->userdata('user_id'));
		$filters = array('visitor'=>$user_id);
		
		$current_settings['order'] = $order;
		$current_settings['order_direction'] = $order_direction;
		$current_settings['page'] = $page;
		$current_settings['filters'] = $filters;		
		$_SESSION['listings_visitors_list'] = $current_settings;
	
		$listing_block = $this->_visitors_block('id_user', $order, $order_direction, $page);
		$this->template_lite->assign('block', $listing_block);
		
		$current_settings = isset($_SESSION['listings_visitors_list'])?$_SESSION['listings_visitors_list']:array();
		if(!isset($current_settings['order'])) $current_settings['order'] = 'date_created';
		$this->template_lite->assign('order', $current_settings['order']);
		
		if(!isset($current_settings['order_direction'])) $current_settings['order_direction'] = 'DESC';
		$this->template_lite->assign('order_direction', $current_settings['order_direction']);
		
		if(!isset($current_settings['page'])) $current_settings['page'] = 1;
		$this->template_lite->assign('page', $current_settings['page']);
		
		$this->Menu_model->breadcrumbs_set_active(l('text_listings_visits', 'listings'));
		
		$this->template_lite->view('visits', 'user', 'listings');
	}
	
	/**
	 * Render visits list action by ajax
	 * @param string $order soting order
	 * @param string $order_description order direction
	 * @param integer $page page of results
	 */
	public function ajax_visits($order='date_created', $order_direction='DESC', $page=1){
		$current_settings = isset($_SESSION['listings_visits_list'])?$_SESSION['listings_visitors_list']:array();
		
		$user_id = intval($this->session->userdata('user_id'));
		$filters = array('visitor'=>$user_id);
		
		$current_settings['order'] = $order;
		$current_settings['order_direction'] = $order_direction;
		$current_settings['page'] = $page;
		$current_settings['filters'] = $filters;		
		$_SESSION['listings_visitors_list'] = $current_settings;
	
		echo $this->_visitors_block('id_user', $order, $order_direction, $page);
	}
	
	/**
	 * Change view mode
	 * @param string $view_mode view mode
	 */
	public function set_view_mode($view_mode){
		if(in_array($view_mode, array('list', 'map'))){
			$_SESSION['listings_view_mode'] = $view_mode;
		}
		redirect($_SERVER['HTTP_REFERER']);
	}
	
	/**
	 * Generate rss link
	 * @param 
	 * @return string
	 */
	public function ajax_get_rss_link(){
		$use_rss = $this->pg_module->get_module_config('listings', 'use_rss');
		if(!$use_rss) return '';
		
		$this->load->model('listings/models/Listings_rss_model');
		
		$data = isset($_SESSION['listings_search']['data']) ? $_SESSION['listings_search']['data'] : array();
		
		$rss_gid = $this->Listings_rss_model->generate_rss_gid(null, $data);
		$rss = $this->Listings_rss_model->get_rss_by_gid($rss_gid);
		if(!$rss){
			$rss_data = array(
				'gid' => $rss_gid,
				'data' => $data,
			);
			$validate_data = $this->Listings_rss_model->validate_rss(null, $rss_data);
			if(empty($validate_data['errors']))	$this->Listings_rss_model->save_rss(null, $validate_data['data']);
		}	
		
		echo site_url().'listings/rss/'.$rss_gid;
	}
	
	/**
	 * Bookings of listings
	 * @param string $status order status
	 * @param string $order sorting order
	 * @param string $order_direction order direction
	 * @param integer $page page of results
	 */
	public function orders($status='wait', $order='created', $order_direction='DESC', $page=1){
		$data = $this->input->post('data', true);

		$block = $this->_orders_block($status, $data, $order, $order_direction, $page);
		$this->template_lite->assign('block', $block);
		
		$this->template_lite->assign('status', $status);
		
		$user_id = $this->session->userdata('user_id');
		$this->template_lite->assign('user_id', $user_id);
		
		$user_type = $this->session->userdata('user_type');
		$this->Menu_model->breadcrumbs_set_parent($user_type.'_my_booking_item');
		
		$current_settings = isset($_SESSION['listings_booking'])?$_SESSION['listings_booking']:array();
		if(!isset($current_settings['order'])) $current_settings['order'] = 'created';
		$this->template_lite->assign('order', $current_settings['order']);
		
		if(!isset($current_settings['order_direction'])) $current_settings['order_direction'] = 'DESC';
		$this->template_lite->assign('order_direction', $current_settings['order_direction']);
		
		if(!isset($current_settings['page'])) $current_settings['page'] = 1;
		$this->template_lite->assign('page', $current_settings['page']);
		
		if(isset($current_settings['filters']['listings'])){
			$this->template_lite->assign('selected', $current_settings['filters']['listings']);}
		
		$this->load->model('listings/models/Listings_booking_model');
		
		$orders_count = array();
		$orders_count['wait'] = $this->Listings_booking_model->get_periods_count(array('not_user'=>$user_id, 'owners'=>$user_id, 'status'=>'wait'));
		$orders_count['approve'] = $this->Listings_booking_model->get_periods_count(array('not_user'=>$user_id, 'owners'=>$user_id, 'status'=>'approve'));
		$orders_count['decline'] = $this->Listings_booking_model->get_periods_count(array('not_user'=>$user_id, 'owners'=>$user_id, 'status'=>'decline'));
		
		$this->template_lite->assign('orders_count', $orders_count);
		$this->template_lite->assign('orders_count_sum', array_sum($orders_count));

		$this->template_lite->view('my_orders');
	}
	
	/**
	 * Return orders list by ajax
	 * @param string $status order status
	 * @param string $order sorting order
	 * @param string $order_direction order direction
	 * @param integer $page page of results
	 */
	public function ajax_orders($status='wait', $order='created', $order_direction='DESC', $page=1){
		$data = $this->input->post('data', true);
		echo $this->_orders_block($status, $data, $order, $order_direction, $page);
	}
	
	/**
	 * Bookings of listings block
	 * @param string $status order status
	 * @param string $order sorting order
	 * @param string $order_direction order direction
	 * @param integer $page page of results
	 */
	private function _orders_block($status='wait', $data=array(), $order='created', $order_direction='DESC', $page=1){
		$current_settings = isset($_SESSION['listings_booking']) ? $_SESSION['listings_booking'] : array();
		if(!isset($current_settings['data'])) $current_settings['data'] = array();
		if(!isset($current_settings['order'])) $current_settings['order'] = 'created';
		if(!isset($current_settings['order_direction'])) $current_settings['order_direction'] = 'DESC';
		if(!isset($current_settings['page'])) $current_settings['page'] = '1';
		if(!isset($current_settings['filters'])) $current_settings['filters'] = array('status'=>'wait');
		
		$user_id = $this->session->userdata('user_id');
			
		$this->load->model('listings/models/Listings_booking_model');
		
		if(!empty($data)) $current_settings['filters'] = $data;
		if($status){
			if($status != $current_settings['filters']['status']){
				$page = 1;
			}
			$current_settings['filters']['status'] = $status;
		}
		$filters = $current_settings['filters'];
		if(isset($filters['keyword'])) $filters['keyword_mode'] = 1;
		$filters['not_user'] = $user_id;
		$filters['owners'] = $user_id;
		
		$order = trim(strip_tags($order));
		if(!empty($order)) $current_settings['order'] = $order;
		$order = $current_settings['order'];
		$this->template_lite->assign('order', $order);
	
		$order_direction = strtoupper(trim(strip_tags($order_direction)));
		if(!empty($order_direction)) $current_settings['order_direction'] = $order_direction;
		$order_direction = $current_settings['order_direction'];
		$this->template_lite->assign('order_direction', $order_direction);
		
		$page = intval($page);
		if(!empty($page)) $current_settings['page'] = $page;
		$page = $current_settings['page'];
		$this->template_lite->assign('page', $page);
		
		if($order_direction != 'ASC') $order_direction = 'DESC';
		
		$items_on_page = $this->pg_module->get_module_config('start', 'index_items_per_page');
	
		$orders_count = $this->Listings_booking_model->get_periods_count($filters);
	
		$this->load->helper('sort_order');

		$current_settings['page'] = get_exists_page_number($page, $orders_count, $items_on_page);
	
		$_SESSION['listings_booking'] = $current_settings;
	
		// sorting
		$sort_data = array(
			'url' => site_url().'listings/orders/',
			'order' => $current_settings['order'],
			'direction' => $current_settings['order_direction'],
			'links' => array('created'=>l('field_date_created', 'listings')),
		);
		$this->template_lite->assign('sort_data', $sort_data);

		if($orders_count > 0){
			$order_array = array();
			switch($order){
				case 'created':
					$order_array['date_created'] = $order_direction;
				break;
				case 'modified':
					$order_array['date_modified'] = $order_direction;
				break;
				default:
					$order_array[$order] = $order_direction;
				break;
			}
			$this->Listings_booking_model->set_format_settings('get_listing', true);
			$orders = $this->Listings_booking_model->get_periods_list($filters, $current_settings['page'], $items_on_page, $order_array);
			$this->Listings_booking_model->set_format_settings('get_listing', false);
			$this->template_lite->assign('orders', $orders);
		}
		$this->template_lite->assign('orders_count', $orders_count);
		
		$this->load->helper('navigation');
		
		$url = site_url().'listings/orders/'.$order.'/'.$order_direction.'/';

		$page_data = get_user_pages_data($url, $orders_count, $items_on_page, $current_settings['page'], 'briefPage');
		$this->config->load('date_formats', TRUE);
		$page_data['date_format'] = $this->config->item('st_format_date_literal', 'date_formats');
		$page_data['date_time_format'] = $this->config->item('st_format_date_time_literal', 'date_formats');
		$this->template_lite->assign('page_data', $page_data);
	
		return $this->template_lite->fetch('my_orders_block', 'user', 'listings');
	}
	
	/**
	 * Bookings of listings (requests)
	 * @param string $status request status
	 * @param string $order sorting order
	 * @param string $order_direction order direction
	 * @param integer $page page of results
	 */
	public function requests($status='wait', $order='created', $order_direction='DESC', $page=1){
		$data = $this->input->post('data', true);
		
		$block = $this->_requests_block($status, $data, $order, $order_direction, $page);
		$this->template_lite->assign('block', $block);
		
		$this->template_lite->assign('status', $status);
		
		$user_id = $this->session->userdata('user_id');
		$this->template_lite->assign('user_id', $user_id);
		
		$user_type = $this->session->userdata('user_type');
		$this->Menu_model->breadcrumbs_set_parent($user_type.'_my_requests_item');
		
		$current_settings = isset($_SESSION['listings_requests'])?$_SESSION['listings_requests']:array();
		if(!isset($current_settings['order'])) $current_settings['order'] = 'created';
		$this->template_lite->assign('order', $current_settings['order']);
		
		if(!isset($current_settings['order_direction'])) $current_settings['order_direction'] = 'DESC';
		$this->template_lite->assign('order_direction', $current_settings['order_direction']);
		
		if(!isset($current_settings['page'])) $current_settings['page'] = 1;
		$this->template_lite->assign('page', $current_settings['page']);
		
		$requests_count = array();
		$requests_count['wait'] = $this->Listings_booking_model->get_periods_count(array('users'=>$user_id, 'not_owner'=>$user_id, 'status'=>'wait'));
		$requests_count['approve'] = $this->Listings_booking_model->get_periods_count(array('users'=>$user_id, 'not_owner'=>$user_id, 'status'=>'approve'));
		$requests_count['decline'] = $this->Listings_booking_model->get_periods_count(array('users'=>$user_id, 'not_owner'=>$user_id, 'status'=>'decline'));
		
		$this->template_lite->assign('requests_count', $requests_count);
		$this->template_lite->assign('requests_count_sum', array_sum($requests_count));
		
		$this->template_lite->view('my_requests');
	}
	
	/**
	 * Return requests list by ajax
	 * @param string $status request status
	 * @param string $order sorting order
	 * @param string $order_direction order direction
	 * @param integer $page page of results
	 */
	public function ajax_requests($status='wait', $order='created', $order_direction='DESC', $page=1){
		$data = $this->input->post('data', true);
		echo $this->_requests_block($status, $data, $order, $order_direction, $page);
	}
	
	/**
	 * Bookings of listings block (requests)
	 * @param string $status request status
	 * @param string $order sorting order
	 * @param string $order_direction order direction
	 * @param integer $page page of results
	 */
	private function _requests_block($status='wait', $data=array(), $order='created', $order_direction='DESC', $page=1){
		$current_settings = isset($_SESSION['listings_requests']) ? $_SESSION['listings_requests'] : array();
		if(!isset($current_settings['data'])) $current_settings['data'] = array();
		if(!isset($current_settings['order'])) $current_settings['order'] = 'created';
		if(!isset($current_settings['order_direction'])) $current_settings['order_direction'] = 'DESC';
		if(!isset($current_settings['page'])) $current_settings['page'] = '1';
		if(!isset($current_settings['filters'])) $current_settings['filters'] = array('status'=>'approve');
			
		$this->load->model('listings/models/Listings_booking_model');
		
		if(!empty($data)) $current_settings['filters'] = $data;
		if($status){
			if($status != $current_settings['filters']['status']){
				$page = 1;
			}
			$current_settings['filters']['status'] = $status;
		}
		$filters = $current_settings['filters'];
		if(isset($filters['keyword'])) $filters['keyword_mode'] = 1;
		
		$user_id = $this->session->userdata('user_id');
		
		$filters['users'] = $user_id;
		$filters['not_owner'] = $user_id;
		
		$order = trim(strip_tags($order));
		if(!empty($order)) $current_settings['order'] = $order;
		$order = $current_settings['order'];
		$this->template_lite->assign('order', $order);
	
		$order_direction = strtoupper(trim(strip_tags($order_direction)));
		if(!empty($order_direction)) $current_settings['order_direction'] = $order_direction;
		$order_direction = $current_settings['order_direction'];
		$this->template_lite->assign('order_direction', $order_direction);
		
		$page = intval($page);
		if(!empty($page)) $current_settings['page'] = $page;
		$page = $current_settings['page'];
		$this->template_lite->assign('page', $page);
		
		if($order_direction != 'ASC') $order_direction = 'DESC';
		
		$items_on_page = $this->pg_module->get_module_config('start', 'index_items_per_page');
		
		$requests_count = $this->Listings_booking_model->get_periods_count($filters);
		
		$this->load->helper('sort_order');

		$current_settings['page'] = get_exists_page_number($page, $requests_count, $items_on_page);
	
		$_SESSION['listings_requests'] = $current_settings;
	
		// sorting
		$sort_data = array(
			'url' => site_url().'listings/requests/',
			'order' => $current_settings['order'],
			'direction' => $current_settings['order_direction'],
			'links' => array('created'=>l('field_date_created', 'listings')),
		);
		$this->template_lite->assign('sort_data', $sort_data);

		if($requests_count > 0){
			$order_array = array();
			switch($order){
				case 'modified':
					$order_array['date_modified'] = $order_direction;
				break;
				default:
					$order_array[$order] = $order_direction;
				break;
			}
			$this->Listings_booking_model->set_format_settings('get_listing', true);
			$requests = $this->Listings_booking_model->get_periods_list($filters, $current_settings['page'], $items_on_page, $order_array);
			$this->Listings_booking_model->set_format_settings('get_listing', false);
			$this->template_lite->assign('requests', $requests);
		}
		$this->template_lite->assign('requests_count', $requests_count);
		
		$this->load->helper('navigation');
		
		$url = site_url().'listings/requests/'.$order.'/'.$order_direction.'/';

		$page_data = get_user_pages_data($url, $requests_count, $items_on_page, $current_settings['page'], 'briefPage');
		$this->config->load('date_formats', TRUE);
		$page_data['date_format'] = $this->config->item('st_format_date_literal', 'date_formats');
		$page_data['date_time_format'] = $this->config->item('st_format_date_time_literal', 'date_formats');
		$this->template_lite->assign('page_data', $page_data);
	
		return $this->template_lite->fetch('my_requests_block', 'user', 'listings');
	}
	
	/**
	 * Remove booking order
	 * @param integer $order_id order identifier
	 */
	public function order_delete($order_id){
		$user_id = $this->session->userdata('user_id');

		$this->load->model('listings/models/Listings_booking_model');

		$this->Listings_booking_model->set_format_settings('get_listing', true);
		$order = $this->Listings_booking_model->get_period_by_id($order_id);
		$this->Listings_booking_model->set_format_settings('get_listing', false);

		if(!$order_id || $user_id != $order['listing']['id_user']){show_404(); return;}

		$this->Listings_booking_model->delete_period($order_id);
		$this->system_messages->add_message('success', l('success_order_deleted', 'listings'));
		
		redirect(site_url().'listings/orders');
	}
	
	
	/**
	 * Remove booking order
	 * @param integer $order_id order identifier
	 */
	public function ajax_order_delete($order_id){
		$return = array('error'=>'', 'success'=>'');
		
		$user_id = $this->session->userdata('user_id');

		$this->load->model('listings/models/Listings_booking_model');

		$this->Listings_booking_model->set_format_settings('get_listing', true);
		$order = $this->Listings_booking_model->get_period_by_id($order_id);
		$this->Listings_booking_model->set_format_settings('get_listing', false);

		if($user_id != $order['listing']['id_user']){
			$return['error'] = l('error_invalid_order', 'listings');
		}else{
			$this->Listings_booking_model->delete_period($order_id);
			$return['success'] = l('success_order_deleted', 'listings');
		}
		
		echo json_encode($return);
	}
	
	/**
	 * Remove booking request
	 * @param integer $request_id request identifier
	 */
	public function request_delete($request_id){
		$user_id = $this->session->userdata('user_id');

		$this->load->model('listings/models/Listings_booking_model');

		$this->Listings_booking_model->set_format_settings('get_listing', true);
		$request = $this->Listings_booking_model->get_period_by_id($request_id);
		$this->Listings_booking_model->set_format_settings('get_listing', false);

		if(!$request_id || $user_id != $request['id_user']){show_404(); return;}

		$this->Listings_booking_model->delete_period($request_id);
		$this->system_messages->add_message('success', l('success_request_deleted', 'listings'));
		
		redirect(site_url().'listings/requests');
	}
	
	/**
	 * Remove booking request by ajax
	 * @param integer $request_id request identifier
	 */
	public function ajax_request_delete($request_id){
		$return = array('error'=>array(), 'success'=>'');
		
		$user_id = $this->session->userdata('user_id');

		$this->load->model('listings/models/Listings_booking_model');

		$this->Listings_booking_model->set_format_settings('get_listing', true);
		$request = $this->Listings_booking_model->get_period_by_id($request_id);
		$this->Listings_booking_model->set_format_settings('get_listing', false);

		if(!$request_id || $user_id != $request['id_user']){
			$return['error'] = l('error_invalid_order', 'listings');
		}else{
			$this->Listings_booking_model->delete_period($request_id);
			$return['success'] = l('success_request_deleted', 'listings');
		}
		
		echo json_encode($return);
	}
	
	/**
	 * Approve booking order
	 * @param integer $order_id order identifier
	 */
	public function order_approve($order_id){
		$user_id = $this->session->userdata('user_id');

		$this->load->model('listings/models/Listings_booking_model');

		$this->Listings_booking_model->set_format_settings('get_listing', true);
		$order = $this->Listings_booking_model->get_period_by_id($order_id);
		$this->Listings_booking_model->set_format_settings('get_listing', false);

		if(!$order || $user_id != $order['listing']['id_user']){show_404(); return;}

		$is_intersect = $this->Listings_booking_model->is_intersect_exists_orders($listing_id, 0, $order['date_start'], $order['date_end'], $order_id);
		if($is_intersect){
			$this->system_messages->add_message('errors', l('error_intersect_calendar_periods', 'listings'));
		}else{
			$this->Listings_booking_model->approve_period($order_id);
			$this->system_messages->add_message('success', l('success_order_approved', 'listings'));
		}
		redirect(site_url().'listings/orders');
	}
	
	/**
	 * Approve booking order by ajax
	 * @param integer $order_id order identifier
	 */
	public function ajax_order_approve($order_id){
		$return = array('error'=>array(), 'success'=>'');
		
		$user_id = $this->session->userdata('user_id');

		$this->load->model('listings/models/Listings_booking_model');

		$this->Listings_booking_model->set_format_settings('get_listing', true);
		$order = $this->Listings_booking_model->get_period_by_id($order_id);
		$this->Listings_booking_model->set_format_settings('get_listing', false);

		if(!$order || $user_id != $order['listing']['id_user']){
			$return['error'] = l('error_invalid_order', 'listings');
		}else{
			$post_data = $this->input->post('period');
			$validate_data = $this->Listings_booking_model->validate_period($order_id, $post_data);
			$is_intersect = $this->Listings_booking_model->is_intersect_exists_periods($listing_id, 0, $order['date_start'], $order['date_end'], $order_id);
			if($is_intersect){
				$validate_data['errors'][] = l('error_intersect_calendar_periods', 'listings');
			}
			if(!empty($validate_data['errors'])){
				$return['error'] = implode('<br>', $validate_data['errors']);
			}else{
				if(!empty($validate_data['data'])) $this->Listings_booking_model->save_period($order_id, $validate_data['data']);
				$this->Listings_booking_model->approve_period($order_id);
				$return['success'] = l('success_order_approved', 'listings');
			}
		}
		
		echo json_encode($return);
	}
	
	/**
	 * Decline booking order
	 * @param integer $order_id order identifier
	 */
	public function order_decline($order_id){
		$user_id = $this->session->userdata('user_id');

		$this->load->model('listings/models/Listings_booking_model');

		$this->Listings_booking_model->set_format_settings('get_listing', true);
		$order = $this->Listings_booking_model->get_period_by_id($order_id);
		$this->Listings_booking_model->set_format_settings('get_listing', false);

		if(!$order_id || $user_id != $order['listing']['id_user']){show_404(); return;}

		$this->Listings_booking_model->decline_period($order_id);
		$this->system_messages->add_message('success', l('success_order_declined', 'listings'));
		
		redirect(site_url().'listings/orders');
	}
	
	/**
	 * Decline booking order by ajax
	 * @param integer $order_id order identifier
	 */
	public function ajax_order_decline($order_id){
		$return = array('error'=>array(), 'success'=>'');
		
		$user_id = $this->session->userdata('user_id');

		$this->load->model('listings/models/Listings_booking_model');

		$this->Listings_booking_model->set_format_settings('get_listing', true);
		$order = $this->Listings_booking_model->get_period_by_id($order_id);
		$this->Listings_booking_model->set_format_settings('get_listing', false);

		if(!$order_id || $user_id != $order['listing']['id_user']){
			$return['error'] = l('error_invalid_order', 'listings');
		}else{
			$this->Listings_booking_model->decline_period($order_id);
			$return['success'] = l('success_order_declined', 'listings');
		}
		
		echo json_encode($return);
	}
	
	/**
	 * Return booking order answer form by ajax
	 * @param integer $order_id order identifier
	 */
	public function ajax_order_approve_form($order_id){
		$this->load->model('listings/models/Listings_booking_model');
		$this->Listings_booking_model->set_format_settings('get_listing', true);
		$order = $this->Listings_booking_model->get_period_by_id($order_id);
		$this->Listings_booking_model->set_format_settings('get_listing', false);
		$this->template_lite->assign('order', $order);
		echo $this->template_lite->fetch('ajax_order_approve_form', 'user', 'listings');
	}
	
	/**
	 * Return order form by ajax
	 * @param integer $listing_id listing identifier
	 * @param integer $order_id order identifier
	 */
	public function ajax_order_form($listing_id, $order_id=null){
		$user_id = $this->session->userdata('user_id');

		$this->Listings_model->set_format_settings('use_format', false);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);

		if(!$listing) return '';

		$this->template_lite->assign('listing', $listing);

		if($order_id){
			$this->load->model('listings/models/Listings_booking_model');
			$this->Listings_booking_model->set_format_settings('use_format', false);
			$order = $this->Listings_booking_model->get_period_by_id($order_id);
			$this->Listings_booking_model->set_format_settings('use_format', true);
			$this->template_lite->assign('order', $order);
		}

		$this->config->load('date_formats', TRUE);
		$page_data['date_format'] = $this->config->item('st_format_date_literal', 'date_formats');
		$page_data['date_time_format'] = $this->config->item('st_format_date_time_literal', 'date_formats');
		$page_data['datepicker_date_format'] = $this->config->item('ui_format_date_literal', 'date_formats');
		$page_data['datepicker_alt_format'] = $this->config->item('ui_format_date_numeric', 'date_formats');
		$this->template_lite->assign('page_data', $page_data);

		switch($listing['price_period']){
			case 1:
				if($order_id){
					$date_start = date('Y-m-d', strtotime($order['date_start']));
					$date_end = date('Y-m-d', strtotime($order['date_end']));
				}else{
					$date_start = date('Y-m-d');
					$date_end = date('Y-m-d', time()+max($listing['calendar_period_min'], 1)*86400);
				}
			
				$this->template_lite->assign('current_date_start', $date_start);
				$this->template_lite->assign('current_date_end', $date_end);
			break;
			case 2:
				if($order_id){
					$start = strtotime($order['date_start']);
					$month_start = date('m', $start);
					$year_start = date('Y', $start);
					
					$end = strtotime($order['date_end']);
					$month_end = date('m', $end);
					$year_end = date('Y', $end);
				}else{
					$month_start = date('m');
					$year_start = date('Y');
					
					$month_end = $month_start+max($listing['calendar_period_min'], 1)%12;
					$year_end = $year_start + intval(max($listing['calendar_period_min'], 1)/12);
				}
				
				$this->template_lite->assign('current_month_start', $month_start);
				$this->template_lite->assign('current_year_start', $year_start);
				
				$this->template_lite->assign('current_month_end', $month_end);
				$this->template_lite->assign('current_year_end', $year_end);
			
				$date_start = $year_start.'-'.$month_start.'-01';
				$date_end = $year_end.'-'.$month_end.'-01';
			break;
		}

		if($listing['id_user'] != $user_id){
			$this->load->model('listings/models/Listings_booking_model');
			$price = $this->Listings_booking_model->get_period_price($listing, $date_start, $date_end);
			$this->template_lite->assign('current_price', $price);
			
			$this->template_lite->assign('show_price', 1);
		}

		$this->template_lite->assign('rand', rand(100000, 999999));

		$this->template_lite->view('ajax_order_form');
	}
	
	/**
	 * Save order by ajax
	 * @param integer $listing_id listing identifier
	 * @param integer $order_id order identifier
	 */
	public function ajax_save_order($listing_id, $order_id=null){
		$result = array('error'=>array(), 'success'=>array(), 'id'=>0, 'data'=>'');
		
		$user_id = $this->session->userdata('user_id');

		$this->Listings_model->set_format_settings('use_format', false);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);

		$booking = $listing['id_user'] != $user_id;
		
		$this->load->model('listings/models/Listings_booking_model');
		
		$post_period = $this->input->post('period', true);
		$post_period['id_listing'] = $listing_id;
		$post_period['id_user'] = $user_id;
		if(!$booking) $post_period['status'] = 'book';
		
		switch($listing['price_period']){
			case 1:
				$date_start_alt = $this->input->post('date_start_alt', true);
				if($date_start_alt) $post_period['date_start'] = $date_start_alt;
						
				$date_end_alt = $this->input->post('date_end_alt', true);
				if($date_end_alt) $post_period['date_end'] = $date_end_alt;
			break;
			case 2: 
				$date_start_month = $this->input->post('date_start_month', true);
				$date_start_year = $this->input->post('date_start_year', true);
				$post_period['date_start'] = $date_start_year.'-'.$date_start_month.'-01';
								
				$date_end_month = $this->input->post('date_end_month', true);
				$date_end_year = $this->input->post('date_end_year', true);
				$post_period['date_end'] = $date_end_year.'-'.$date_end_month.'-01';
			break;
		}
					
		$validate_data = $this->Listings_booking_model->validate_period($order_id, $post_period, $booking);
		if(!empty($validate_data['errors'])){
			$result['error'] = implode('<br>', $validate_data['errors']);
		}else{
			if($booking){
				$this->load->model('Users_model');
				$this->Users_model->add_contact($user_id, $listing['id_user']);
				$this->Users_model->add_contact($listing['id_user'], $user_id);
			}
			
			$guests = $validate_data['data']['guests'] ? $validate_data['data']['guests'] : 1;
			
			$validate_data['data']['price'] = $this->Listings_booking_model->get_period_price($listing, $validate_data['data']['date_start'], $validate_data['data']['date_end'], $guests);
			
			if($order_id){
				$result['success'] = l('success_order_updated', 'listings');
			}else{
				$result['success'] = l('success_order_added', 'listings');
			}
			
			$order_id = $this->Listings_booking_model->save_period($order_id, $validate_data['data']);
			
			$result['id'] = $validate_data['data']['id'] = $order_id;
			
			if(!$booking){
				$this->template_lite->assign('listing', $listing);
				
				$period = $this->Listings_booking_model->format_period($validate_data['data']);
				$this->template_lite->assign('period', $period);
	
				$this->config->load('date_formats', TRUE);
				$page_data['date_format'] = $this->config->item('st_format_date_literal', 'date_formats');
				$page_data['date_time_format'] = $this->config->item('st_format_date_time_literal', 'date_formats');
				$page_data['datepicker_date_format'] = $this->config->item('ui_format_date_literal', 'date_formats');
				$page_data['datepicker_alt_format'] = $this->config->item('ui_format_date_numeric', 'date_formats');
				$this->template_lite->assign('page_data', $page_data);
			
				$this->template_lite->assign('edit', 1);
		
				$result['data'] = $this->template_lite->fetch('calendar_period', 'user', 'listings');
			}
		}
		
		echo json_encode($result);
		exit;
	}
	
	/**
	 * Return period form by ajax
	 * @param integer $listing_id listing identifier
	 * @param integer $period_id period identifier
	 */
	public function ajax_period_form($listing_id, $period_id=null){
		$user_id = $this->session->userdata('user_id');

		$this->Listings_model->set_format_settings(array('get_user'=>false, 'get_location'=>false));
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings(array('get_user'=>true, 'get_location'=>true));

		if(!$listing || $listing['id_user'] != $user_id) return '';

		$this->template_lite->assign('data', $listing);
		
		if($period_id){
			$this->load->model('listings/models/Listings_booking_model');
			$period = $this->Listings_booking_model->get_period_by_id($period_id);
			$this->template_lite->assign('period', $period);
		}
		
		if($this->pg_module->is_module_installed('payments')){
			$this->load->model('payments/models/Payment_currency_model');
			$currencies = $this->Payment_currency_model->get_currency_list();
			$this->template_lite->assign('currencies', $currencies);
					
			if(isset($listing['gid_currency']) && !empty($listing['gid_currency'])){
				$current_price_currency = $this->Payment_currency_model->get_currency_by_gid($listing['gid_currency']);
				$this->template_lite->assign('current_price_currency', $current_price_currency);
			}
		}
			
		$this->config->load('date_formats', TRUE);
		$page_data['date_format'] = $this->config->item('st_format_date_literal', 'date_formats');
		$page_data['date_time_format'] = $this->config->item('st_format_date_time_literal', 'date_formats');
		$page_data['datepicker_date_format'] = $this->config->item('ui_format_date_literal', 'date_formats');
		$page_data['datepicker_alt_format'] = $this->config->item('ui_format_date_numeric', 'date_formats');
		$this->template_lite->assign('page_data', $page_data);
		
		$this->template_lite->assign('rand', rand(100000, 999999));
		
		$this->template_lite->view('ajax_period_form');
	}
	
	/**
	 * Save period by ajax
	 * @param integer $listing_id listing identifier
	 * @param integer $period_id period identifier
	 */
	public function ajax_save_period($listing_id, $period_id=null){
		$result = array('error'=>array(), 'success'=>array(), 'id'=>0, 'data'=>'');
		
		$user_id = $this->session->userdata('user_id');

		$this->Listings_model->set_format_settings('use_format', false);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);

		if(!$listing || $listing['id_user'] != $user_id) return '';
		
		$this->load->model('listings/models/Listings_booking_model');
		
		$post_period = $this->input->post('period', true);
		$post_period['id_listing'] = $listing_id;
		$post_period['id_user'] = $user_id;
		
		switch($listing['price_period']){
			case 1:
				$date_start_alt = $this->input->post('date_start_alt', true);
				if($date_start_alt) $post_period['date_start'] = $date_start_alt;
						
				$date_end_alt = $this->input->post('date_end_alt', true);
				if($date_end_alt) $post_period['date_end'] = $date_end_alt;
			break;
			case 2: 
				$date_start_month = $this->input->post('date_start_month', true);
				$date_start_year = $this->input->post('date_start_year', true);
				if($date_start_year && $date_start_month) 
					$post_period['date_start'] = $date_start_year.'-'.$date_start_month.'-01';
								
				$date_end_month = $this->input->post('date_end_month', true);
				$date_end_year = $this->input->post('date_end_year', true);
				if($date_end_year && $date_end_month) 
					$post_period['date_end'] = $date_end_year.'-'.$date_end_month.'-01';
			break;
		}
					
		$validate_data = $this->Listings_booking_model->validate_period($period_id, $post_period);
		if(!empty($validate_data['errors'])){
			$result['error'] = implode('<br>', $validate_data['errors']);
		}else{
			if($period_id){
				$result['success'] = l('success_period_updated', 'listings');
			}else{
				$result['success'] = l('success_period_added', 'listings');
			}
			
			$period_id = $this->Listings_booking_model->save_period($period_id, $validate_data['data']);
			
			$result['id'] = $validate_data['data']['id'] = $period_id;
			
			$this->template_lite->assign('listing', $listing);
			
			$period = $this->Listings_booking_model->format_period($validate_data['data']);
			$this->template_lite->assign('period', $period);
			
			$this->config->load('date_formats', TRUE);
			$page_data['date_format'] = $this->config->item('st_format_date_literal', 'date_formats');
			$page_data['date_time_format'] = $this->config->item('st_format_date_time_literal', 'date_formats');
			$page_data['datepicker_date_format'] = $this->config->item('ui_format_date_literal', 'date_formats');
			$page_data['datepicker_alt_format'] = $this->config->item('ui_format_date_numeric', 'date_formats');
			$this->template_lite->assign('page_data', $page_data);
			
			$this->template_lite->assign('edit', 1);
		
			$result['data'] = $this->template_lite->fetch('calendar_period', 'user', 'listings');
		}
		
		echo json_encode($result);
		exit;
	}
	
	/**
	 * Remove booking period
	 * @param integer $period_id period identifier
	 */
	public function period_delete($period_id){
		$user_id = $this->session->userdata('user_id');

		if(!$period_id){show_404(); return;}

		$this->load->model('listings/models/Listings_booking_model');

		$this->Listings_booking_model->set_format_settings('use_format', false);
		$period = $this->Listings_booking_model->get_period_by_id($period_id);
		$this->Listings_booking_model->set_format_settings('use_format', true);

		if($user_id != $period['id_user']){show_404(); return;}

		$this->Listings_booking_model->delete_period($period_id);
		$this->system_messages->add_message('success', l('success_period_deleted', 'listings'));
		
		redirect(site_url().'listings/edit/'.$period['id_listing'].'/calendar/2');
	}
	
	/**
	 * Remove booking period by ajax
	 * @param integer $period_id period identifier
	 */
	public function ajax_period_delete($period_id){
		$return = array('error'=>'', 'success'=>'');
		
		$user_id = $this->session->userdata('user_id');

		if(!$period_id){$return['error'] = 'Not access'; echo json_encode($return); exit;}

		$this->load->model('listings/models/Listings_booking_model');

		$this->Listings_booking_model->set_format_settings('use_format', false);
		$period = $this->Listings_booking_model->get_period_by_id($period_id);
		$this->Listings_booking_model->set_format_settings('use_format', true);

		if($user_id != $period['id_user']){$return['error'] = 'Not access';  echo json_encode($return); exit;}

		$this->Listings_booking_model->delete_period($period_id);
		$return['success'] = l('success_period_deleted', 'listings');
		
		echo json_encode($return);
	}
	
	/**
	 * Get list of listings by ajax action
	 * @param integer $page page of results
	 */
	public function ajax_get_listings($page=1){
		$selected = $this->input->post('selected', true);
		$operation_type = $this->input->post('operation_type', true);
		$id_user = $this->input->post('id_user', true);
		$search_string = trim(strip_tags($this->input->post('search', true)));
		$params = $return = array();
		$items_on_page = 100;

		if(!empty($search_string)){
			$params['keyword'] = $search_string.'*';
			$params['keyword_mode'] = 1;
		}
		if(!empty($selected)){
			$params['exclude_id'] = $selected;
		}

		if($operation_type){
			$params['type'] = $operation_type;
		}
		
		if($id_user){
			$params['id_user'] = $id_user;
		}
	
		$return['all'] = $this->Listings_model->get_listings_count($params);
		$return['pages'] = ceil($return['all']/$items_on_page);
		$return['current_page'] = $page;
		$return['items'] = $this->Listings_model->get_listings_list($params, $page, $items_on_page, array('date_modified'=>'DESC'));

		echo json_encode($return);
		return;
	}

	/**
	 * Return list of selected listings by ajax action
	 */
	public function ajax_get_selected_listings(){
		$selected = $this->input->post('selected', true);

		if(!empty($selected)){
			$params['ids'] = $selected;
			$return = $this->Listings_model->get_listings_list($params, null, null, array('date_modified'=>'DESC'));
		}else{
			$return = array();
		}

		if(!empty($return)){
			$this->load->helper('seo');
			$this->load->helper('listings');

			$current_lang = $this->pg_language->get_lang_by_id($this->pg_language->current_lang_id);
			$current_lang_code = $current_lang['code'];

			foreach($return as $key=>$listing){
				$listing['seo_url'] = rewrite_link('listings', 'view', $listing);
				$listing['price_str'] = listing_price_block(array('operation_type'=>$listing['operation_type'], 'data'=>$listing, 'template'=>'small'));
				$return[$key] = $listing;
			}
		}
		
		echo json_encode($return);
		return;
	}

	/**
	 * Return form for filtering listings by ajax listings
	 * @param integer $max_select
	 * @param string $template
	 */
	public function ajax_get_listings_form($max_select=1, $template='default'){
		$selected = $this->input->post('selected', true);
		if(!empty($selected)){
			$params['ids'] = $selected;
			$data['selected'] = $this->Listings_model->get_listings_list($params, null, null, array('date_modified'=>'DESC'));
		}else{
			$data['selected'] = array();
		}
		$data['max_select'] = $max_select ? $max_select : 0;

		$this->template_lite->assign('select_data', $data);
		$this->template_lite->view('ajax_listing_select_form_'.$template);
	}
	
	/**
	 * Return price for booking period
	 * @param integer $listing_id listing identifier
	 */
	public function ajax_get_booking_price($listing_id){
		$return = array('price' => 0);

		$this->Listings_model->set_format_settings('use_format', false);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);

		$date_start = $this->input->post('date_start', true);
		$date_start_alt = $this->input->post('date_start_alt', true);
		if($date_start_alt) $date_start = $date_start_alt;
		
		$date_end = $this->input->post('date_end', true);
		$date_end_alt = $this->input->post('date_end_alt', true);
		if($date_end_alt) $date_end = $date_end_alt;
		
		$guests = intval($this->input->post('guests', true));	
		if(!$guests) $guests = 1;
		
		if(!$date_start || !$date_end){
			if($listing['price_negotiated']){
				$return['price'] = l('text_negotiated_price_rent', 'listings');
			}else{
				if($listing['price_reduced'] > 0){
					$price_value = $listing['price_reduced'];
				}else{
					$price_value = $listing['price'];	
				}
					
				$this->load->helper('start');
				$price_arr = array('value'=>$price_value, 'cur_gid'=>$listing['gid_currency']);	
				$return['price'] = l('text_price_from', 'listings').' '.currency_format_output($price_arr);
			}
		}else{
			$this->load->model('listings/models/Listings_booking_model');
			$price_value = $this->Listings_booking_model->get_period_price($listing, $date_start, $date_end, $guests);
			if($price_value){
				$this->load->helper('start');
				$price_arr = array('value'=>$price_value, 'cur_gid'=>$listing['gid_currency']);	
				$return['price'] = currency_format_output($price_arr);
			}else{
				$return['price'] = l('text_negotiated_price_rent', 'listings');
			}
		}
		
		echo json_encode($return);
	}
	
	/**
	 * Return calendar
	 * @param integer $listing_id listing identifier
	 * @param integer $count number of calendars
	 * @param integer $year year
	 * @param integer $month month
	 */
	public function ajax_get_calendar($listing_id, $count, $year, $month=1){
		$this->Listings_model->set_format_settings('get_booking', true);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('get_booking', false);
		
		$params = array(
			'listing' => $listing,
			'count' => $count,
			'year' => $year,
			'month' => $month,
			'template' => 'view',
		);
		
		$this->load->helper('listings');
		echo listings_calendar_block($params, true);
	}

	# MOD for Comparison #
	public function comparison($id_1, $id_2, $id_3, $id_4)
	{
		$id1 = str_replace('id:','',$id_1);
		$id2 = str_replace('id:','',$id_2);
		$id3 = str_replace('id:','',$id_3);
		$id4 = str_replace('id:','',$id_4);

		$listing = $this->Listings_model->get_listing_by_comparison($id1, $id2, $id3, $id4);
		$developer = $this->Listings_model->get_developer();
		$residen = $this->Listings_model->get_residensial();
		$commer = $this->Listings_model->get_commercial();
		
		$this->template_lite->assign('develop',$developer);
		$this->template_lite->assign('listings',$listing);
		$this->template_lite->assign('residential',$residen);
		$this->template_lite->assign('commercial',$commer);
		
		# MOD to get similar listings based on its #
		foreach($listing as $key=>$val) {
			$similar_data[] = $val['id'].'-'.$val['id_city'].'-'.$val['property_type'];
		}
		$similar_listing = $this->Listings_model->get_similar_listings($similar_data);
		# MOD to get similar listings based on its #
		$this->template_lite->assign('similar', $similar_listing);
		
		$this->template_lite->view('comparison');
	}
	# End MOD for Comparison #

	#MOD FOR GRABBING CURRENT SESSION ID FROM LOGGED IN USER#
	public function grab_session_user_id()
	{
		return $this->session->userdata('user_id');
	}
	#END OF MOD#

}