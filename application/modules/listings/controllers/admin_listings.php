<?php
/**
* Listings admin side controller
* 
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/
class Admin_Listings extends Controller{
	
	/**
	 * Available filters
	 * @var array
	 */
	private $filters = array('all', 'not_active', 'active');

	/**
	 * Constructor
	 *
	 * @return Admin_Listings
	 */
	public function __construct(){
		parent::Controller();
		$this->load->model('Menu_model');
		#MOD FOR KPR BUTTON MENU BUTTON HIGHLIGHT, IT WAS NOT DISABLED BY DEFAULT#
		#$this->Menu_model->set_menu_active_item('admin_menu', 'listings_menu_item');
		#END OF MOD#
		$this->system_messages->set_data('header', l('admin_header_listings', 'listings'));
	}
	
	/**
	 * Render listings list
	 * @param string $filter filter type
	 * @param string $order sorting order 
	 * @param string $order_direction order direction
	 * @param integer $page page of results
	 */
	private function _get_listings($filter=null, $order=null, $order_direction=null, $page=null){
		$this->load->model('Listings_model');

		$this->Menu_model->set_menu_active_item('admin_listings_menu', 'listings-list-item');
		
		$current_settings = isset($_SESSION['listings_list']) ? $_SESSION['listings_list'] : array();
		if(!isset($current_settings['filter'])) $current_settings['filter'] = $this->filters[0];
		if(!isset($current_settings['order'])) $current_settings['order'] = 'date_created';
		if(!isset($current_settings['order_direction'])) $current_settings['order_direction'] = 'DESC';
		if(!isset($current_settings['page'])) $current_settings['page'] = 1;
		
		if(!$filter) $filter = $current_settings['filter'];
		if(!in_array($filter, $this->filters)) $filter = $this->filters[0];
		$this->template_lite->assign('filter', $filter);
		
		$current_settings['page'] = $page;		
		$current_settings['filter'] = $filter;
	
		$filter_data['all'] = $this->Listings_model->get_listings_count();
		$filter_data['active'] = $this->Listings_model->get_listings_count(array('active'=>1));
		$filter_data['not_active'] = $this->Listings_model->get_listings_count(array('not_active'=>1));
		$this->template_lite->assign('filter_data', $filter_data);
	
		if($this->input->post('filter-submit')){
			$_SESSION['listings_filters'] = array();
			
			$post_data['type'] = strval($this->input->post('type', true));
			if($post_data['type']) $_SESSION['listings_filters']['type'] = $post_data['type'];
			
			$post_data['id_country'] = strval($this->input->post('id_country', true));
			if($post_data['id_country']) $_SESSION['listings_filters']['id_country'] = $post_data['id_country'];
			
			$post_data['id_region'] = intval($this->input->post('id_region', true));
			if($post_data['id_region']) $_SESSION['listings_filters']['id_region'] = $post_data['id_region'];
			
			$post_data['id_city'] = intval($this->input->post('id_city', true));
			if($post_data['id_city']) $_SESSION['listings_filters']['id_city'] = $post_data['id_city'];
			
			$post_data['category'] = strval($this->input->post('category', true));
			if($post_data['category']){
				$t = explode('_', $post_data['category']);
				$_SESSION['listings_filters']['id_category'] = intval($t[0]);
				if(!empty($t[1])) $_SESSION['listings_filters']['property_type'] = intval($t[1]);			
			}
		}

		if($this->input->post('filter-reset')){
			$_SESSION['listings_filters'] = array();
		}
		
		$filters = isset($_SESSION['listings_filters']) ? $_SESSION['listings_filters'] : array();
		if($filter != 'all') $filters[$filter] = '1';
		
		if (!$order) $order = $current_settings['order'];
		$this->template_lite->assign('order', $order);
		$current_settings['order'] = $order;

		if (!$order_direction) $order_direction = $current_settings['order_direction'];
		$this->template_lite->assign('order_direction', $order_direction);
		$current_settings['order_direction'] = $order_direction;

		$listings_count = $this->Listings_model->get_listings_count($filters);

		if(!$page) $page = $current_settings['page'];
		$items_on_page = $this->pg_module->get_module_config('start', 'admin_items_per_page');
			
		$this->load->helper('sort_order');
		$page = get_exists_page_number($page, $listings_count, $items_on_page);
		$current_settings['page'] = $page;

		$_SESSION['listings_list'] = $current_settings;

		$sort_links = array(
			'date_created' => site_url().'admin/listings/'.$filter.'/date_created/'.(($order != 'date_created' xor $order_direction == 'DESC') ? 'ASC' : 'DESC'),
			'review_count' => site_url().'admin/listings/'.$filter.'/review_count/'.(($order != 'review_count' xor $order_direction == 'DESC') ? 'ASC' : 'DESC'),
		);
		$this->template_lite->assign('sort_links', $sort_links);
		
		if($listings_count > 0){
			$listings = $this->Listings_model->get_listings_list($filters, $page, $items_on_page, array($order=>$order_direction));
			$this->template_lite->assign('listings', $listings);
		}
		
		$operation_types = $this->Listings_model->get_operation_types(true);
		$this->template_lite->assign('operation_types', $operation_types);

		$is_operation_types_disabled = $this->Listings_model->is_operation_types_disabled();
		$this->template_lite->assign('is_operation_types_disabled', $is_operation_types_disabled);
		
		$this->load->helper('navigation');
		$this->config->load('date_formats', TRUE);
		$page_data = get_admin_pages_data(site_url().'admin/listings/'.$filter.'/'.$order.'/'.$order_direction.'/', $listings_count, $items_on_page, $page, 'briefPage');
		$page_data['date_format'] = $this->config->item('st_format_date_time_literal', 'date_formats');
		$page_data['filter'] = $filters;
		
		$this->template_lite->assign('page_data', $page_data);
		$this->template_lite->view('list_listings');
	}
	
	/**
	 * Render list of listings for sale
	 * @param string $order sorting order
	 * @param string $order_direction order direction
	 * @param integer $page page of results
	 */
	public function index($order=null, $order_direction=null, $page=null){
		$this->_get_listings(null, $order, $order_direction, $page);
	}
	
	/**
	 * Render list of listings for sale
	 * @param string $order sorting order 
	 * @param string $order_direction order direction
	 * @param integer $page page of results
	 */
	public function all($order=null, $order_direction=null, $page=null){		
		$this->_get_listings('all', $order, $order_direction, $page);
	}
	
	/**
	 * Render list of listings for sale
	 * @param string $order sorting order
	 * @param string $order_direction order direction
	 * @param integer $page page of results
	 */
	public function active($order=null, $order_direction=null, $page=null){
		$this->_get_listings('active', $order, $order_direction, $page);
	}
	
	/**
	 * Render list of listings for sale
	 * @param string $order sorting order
	 * @param string $order_direction order direction
	 * @param integer $page page of results
	 */
	public function not_active($order=null, $order_direction=null, $page=null){
		$this->_get_listings('not_active', $order, $order_direction, $page);
	}
	
	/**
	 * Render list of listings for sale
	 * @param string $order sorting order
	 * @param string $order_direction order direction
	 * @param integer $page page of results
	 */
	public function sale($order=null, $order_direction=null, $page=null){
		$this->_get_listings('sale', $order, $order_direction, $page);
	}
	
	/**
	 * list of listings for by
	 * @param string $order sorting order 
	 * @param string $order_direction order direction
	 * @param integer $page page of results
	 */
	public function buy($order=null, $order_direction=null, $page=null){
		$this->_get_listings('buy', $order, $order_direction, $page);
	}	
	
	/**
	 * Show listing
	 * @param integer $listing_id listing identifier
	 */
	public function show($listing_id){
		if(!$listing_id){show_404();return;}
		
		$this->load->model('Listings_model');	
		
		$current_settings = isset($_SESSION['listings_list']) ? $_SESSION['listings_list'] : array('filter'=>'sale');
		$this->template_lite->assign('filter', $current_settings['filter']);

		$this->load->helper('navigation');
		$this->config->load('date_formats', TRUE);
		$url = site_url().'admin/listings/index/'.$order.'/'.$order_direction.'/';
		$page_data = get_admin_pages_data($url, $listings_count, $items_on_page, $page, 'briefPage');
		$page_data['date_format'] = $this->config->item('st_format_date_time_literal', 'date_formats');
		$this->template_lite->assign('page_data', $page_data);
		
		$data = $this->Listings_model->get_sale_by_id($listing_id);		
		$this->template_lite->assign('data', $data);

		$this->template_lite->view('listings_view');
	}
	
	/**
	 * Render edit listing action
	 * @param integer $listing_id listing identifier
	 * @param string $section_gid section guid
	 */
	public function edit($listing_id=0, $section_gid='overview'){
		$this->load->model('Listings_model');
		
		$format_settings = array('get_moderation'=>true);
		
		switch($section_gid){
			case 'overview':
				$format_settings['get_user'] = true;
				$format_settings['get_location'] = true;
			break;
			case 'description':
				$format_settings['get_description'] = true;
			break;
			case 'gallery':
				$format_settings['get_photos_all'] = true;
				$format_settings['get_virtual_tours'] = true;
				$format_settings['get_file'] = true;
				$format_settings['get_video'] = true;
			break;
			case 'map':
				$format_settings['get_location'] = true;
			break;
			case 'calendar':
				$format_settings['get_booking'] = true;
			break;
		}
		
		$this->Listings_model->set_format_settings($format_settings);
		
		if($listing_id){
			$data = $this->Listings_model->get_listing_by_id($listing_id);
		}else{
			$is_operation_types_disabled = $this->Listings_model->is_operation_types_disabled();
			if($is_operation_types_disabled) show_404();
			$data = array();
		}
	
		if($this->input->post('btn_save')){
			$section  = $this->input->post('property_type_section', true);
			$post_data = $this->input->post('data', true);
			$errors = array();
			switch($section_gid){
				case 'overview':
					$post_data['id_country'] = $this->input->post('id_country', true);
					$post_data['id_region'] = $this->input->post('id_region', true);
					$post_data['id_city'] = $this->input->post('id_city', true);
					$post_data['services'] = $this->input->post('services', true);
					$post_data['id_wish_lists'] = $this->input->post('id_wish_lists', true);
					$post_data['id_user'] = $this->input->post('id_user', true);
					
					if(!$listing_id){
						$operation_types = array_flip($this->Listings_model->get_operation_types());
						$operation_type = $this->input->post('type', true);
						if($operation_type && isset($operation_types[$operation_type])){ 
							$post_data['id_type'] = $operation_types[$operation_type];
						}
						
						if(isset($post_data['category'])){
							$pos = strpos($post_data['category'], '_');
							$post_data['id_category'] = substr($post_data['category'], 0, $pos);
							$post_data['property_type'] = substr($post_data['category'], $pos + 1);	
							unset($post_data['category']);
						}
					}else{
						if(isset($post_data['category'])){
							$pos = strpos($post_data['category'], '_');
							$post_data['property_type'] = substr($post_data['category'], $pos + 1);	
							unset($post_data['category']);
						}
					}
			
					$post_data['date_open_alt'] = $this->input->post('date_open_alt', true);
					if($post_data['date_open_alt'])	$post_data['date_open'] = $post_data['date_open_alt'];
					$post_data['date_available_alt'] = $this->input->post('date_available_alt', true);
					if($post_data['date_available_alt'])	$post_data['date_available'] = $post_data['date_available_alt'];
					
					if($post_data['services']['featured'] != $data['is_featured']){
						if($post_data['services']['featured']){
							$post_data['featured_date_end'] = date('Y-m-d H:i:s', time()+$this->Listings_model->service_period_default*24*60*60);
						}else{
							$post_data['featured_date_end'] = '0000-00-00 00:00:00';
						}
					}
					if($post_data['services']['lift_up'] != $data['is_lift_up']){
						if($post_data['services']['lift_up']){
							$post_data['lift_up_date_end'] = date('Y-m-d H:i:s', time()+$this->Listings_model->service_period_default*24*60*60);	
						}else{
							$post_data['lift_up_date_end'] = '0000-00-00 00:00:00';
						}
					}
					if($post_data['services']['lift_up_country'] != $data['is_lift_up_country']){
						if($post_data['services']['lift_up_country']){
							$post_data['lift_up_country_date_end'] = date('Y-m-d H:i:s', time()+$this->Listings_model->service_period_default*24*60*60);	
						}else{
							$post_data['lift_up_country_date_end'] = '0000-00-00 00:00:00';
						}
					}
					if($post_data['services']['lift_up_region'] != $data['is_lift_up_region']){
						if($post_data['services']['lift_up_region']){
							$post_data['lift_up_region_date_end'] = date('Y-m-d H:i:s', time()+$this->Listings_model->service_period_default*24*60*60);
						}else{
							$post_data['lift_up_region_date_end'] = '0000-00-00 00:00:00';
						}
					}
					if($post_data['services']['lift_up_city'] != $data['is_lift_up_city']){
						if($post_data['services']['lift_up_city']){
							$post_data['lift_up_city_date_end'] = date('Y-m-d H:i:s', time()+$this->Listings_model->service_period_default*24*60*60);	
						}else{
							$post_data['lift_up_city_date_end'] = '0000-00-00 00:00:00';
						}
					}
					if($post_data['services']['highlight'] != $data['services']['is_highlight']){
						if($post_data['services']['highlight']){
							$post_data['highlight_date_end'] = date('Y-m-d H:i:s', time()+$this->Listings_model->service_period_default*24*60*60);	
						}else{
							$post_data['highlight_date_end'] = '0000-00-00 00:00:00';
						}
					}
					if($post_data['services']['slide_show'] != $data['services']['is_slide_show']){
						if($post_data['services']['slide_show']){
							$post_data['slide_show_date_end'] = date('Y-m-d H:i:s', time()+$this->Listings_model->service_period_default*24*60*60);	
						}else{
							$post_data['slide_show_date_end'] = '0000-00-00 00:00:00';
						}
					}
				break;
				case 'description':
					$property_type_gid = $this->Listings_model->get_field_editor_type($data);
					$fields_for_select = $this->Listings_model->get_fields_for_select($property_type_gid);
					foreach($fields_for_select as $field){
						$post_data[$field] = $this->input->post($field, true);
					}
				break;
				case 'gallery':
					if($this->input->post('btn_save_file')){
						$validate_data = $this->Listings_model->save_file($listing_id, 'listing_file', $post_data);
						///// delete listing file
						if($this->input->post('listing_file_delete') && $listing_id && $data['listing_file']){
							$this->load->model('File_uploads_model');
							$this->File_uploads_model->delete_upload($this->Listings_model->file_config_id, $data['prefix'], $data['listing_file']);
							if(!isset($validate_data['data']['file'])){
								$listing_data = array(
									'listing_file' => '',
									'listing_file_name' => '',
									'listing_file_comment' => '',
								);
								$this->Listings_model->save_listing($listing_id, $listing_data);
							}	
							$this->system_messages->add_message('success', l('success_file_deleted', 'listings'));
						}elseif($validate_data['data']['file']){
							$this->system_messages->add_message('success', l('success_file_uploaded', 'listings'));
						}
						$step = 3;
					}else{
						unset($post_data['listing_file_name']);
						unset($post_data['listing_file_comment']);
					}
					if($this->input->post('btn_save_video')){
						$validate_data = $this->Listings_model->save_video($listing_id, 'listing_video');
						///// delete video
						if($this->input->post('listing_video_delete') && $listing_id && $data['listing_video']){
							$this->load->model('Video_uploads_model');
							$this->Video_uploads_model->delete_upload($this->Listings_model->video_config_id, $data['prefix'], $data['listing_video'], $data['listing_video_image'], $data['listing_video_data']['data']['upload_type']);
							if(!isset($validate_data['data']['video'])){
								$listing_data = array(
									'listing_video' => '',
									'listing_video_image' => '',
									'listing_video_data' => '',
								);
								$this->Listings_model->save_listing($listing_id, $listing_data);
							}
							$this->system_messages->add_message('success', l('success_video_deleted', 'listings'));
						}elseif($validate_data['data']['file']){
							$this->system_messages->add_message('success', l('success_video_uploaded', 'listings'));
						}
						$step = 4;
					}
				break;
				case 'map':
					$this->load->model('geomap/models/Geomap_model');
					$this->load->model('geomap/models/Geomap_settings_model');
					$post_settings = $this->input->post('map', true);
					$post_settings['lat'] = $post_data['lat'];
					$post_settings['lon'] = $post_data['lon'];
					$map_gid = $this->Geomap_model->get_default_driver_gid();
					$validate_map = $this->Geomap_settings_model->validate_settings($post_settings);
					if(!empty($validate_map['errors'])) break;
					$this->Geomap_settings_model->save_settings($map_gid, 0, $listing_id, 'listing_view', $validate_map['data']);
				break;
			}
	
			if(isset($validate_data)){
				if(!empty($validate_data['errors'])){
					$this->system_messages->add_message('error', $validate_data['errors']);
				}else{
					$url = site_url().'admin/listings/edit/'.$listing_id.'/'.$section_gid;
					redirect($url);
				}
			}else{
				$validate_data = $this->Listings_model->validate_listing($listing_id, $post_data);
				if(!empty($validate_data['errors'])){
					$this->system_messages->add_message('error', $validate_data['errors']);
				}else{
					if(!empty($validate_data['data'])){
						if($listing_id){
							$this->Listings_model->save_listing($listing_id, $validate_data['data']);
							$this->system_messages->add_message('success', l('success_update_listing', 'listings'));
						}else{
							$listing_id = $this->Listings_model->save_listing(null, $validate_data['data']);
							$this->system_messages->add_message('success', l('success_add_listing', 'listings'));
						}
						if(isset($validate_data['data']['id_wish_lists']) && !empty($post_data['id_wish_lists'])){
							$this->load->model('listings/models/Wish_list_model');
							if(isset($data['id_wish_lists']) && !empty($data['id_wish_lists'])){
								$delete_from_wish_lists = array_diff($data['id_wish_lists'], $post_data['id_wish_lists']);
								foreach($delete_from_wish_lists as $wish_list_id_){
									$this->Wish_list_model->delete_from_wish_list($wish_list_id_, array($listing_id));
								}
								$add_to_wish_lists = array_diff($post_data['id_wish_lists'], $data['id_wish_lists']);
								foreach($add_to_wish_lists as $wish_list_id_){
									$this->Wish_list_model->add_to_wish_list($wish_list_id_, array($listing_id));
								}
							}else{
								foreach($post_data['id_wish_lists'] as $wish_list_id_){
									$this->Wish_list_model->add_to_wish_list($wish_list_id_, array($listing_id));
								}						
							}
						}
						$url = site_url().'admin/listings/edit/'.$listing_id.'/'.$section_gid;
						redirect($url);
					}
				}
			}
			
			if(!empty($post_data)){
				$this->Listings_model->set_format_settings('use_format', false);
				$data = $this->Listings_model->get_listing_by_id($listing_id);
				$this->Listings_model->set_format_settings('use_format', true);
		
				$post_data = $this->Listings_model->format_listings(array(array_merge($data, $post_data)));
				$data = $post_data[0];
			}	
		}

		if($listing_id){
			$this->load->model('listings/models/Listings_moderation_model');
			if($this->Listings_moderation_model->is_listing_moderated($listing_id)){
				$this->template_lite->assign('on_moderation', 1);
				$this->system_messages->add_message('error', l('error_listing_on_moderation', 'listings'));
			}
		}
		
		switch($section_gid){
			case 'overview':
				$operation_types = $this->Listings_model->get_operation_types();
				$this->template_lite->assign('operation_types', $operation_types);
				$this->template_lite->assign('operation_types_count', count($operation_types));
				
				if($this->pg_module->is_module_installed('payments')){
					$this->load->model('payments/models/Payment_currency_model');
					$currencies = $this->Payment_currency_model->get_currency_list();
					$this->template_lite->assign('currencies', $currencies);
					if(isset($data['gid_currency']) && !empty($data['gid_currency'])){
						$current_price_currency = $this->Payment_currency_model->get_currency_by_gid($data['gid_currency']);
						$this->template_lite->assign('current_price_currency', $current_price_currency);
					}else{
						$current_price_currency = $this->Payment_currency_model->get_currency_default();
						$data['gid_currency'] = $current_price_currency['gid'];
					}
				}
				
				$this->template_lite->assign('current_lang_id', $this->pg_language->current_lang_id);
				$this->template_lite->assign('langs', $this->pg_language->languages);
				
				$this->config->load('date_formats', TRUE);
				$page_data['date_format'] = $this->config->item('st_format_date_literal', 'date_formats');
				$page_data['date_time_format'] = $this->config->item('st_format_date_time_literal', 'date_formats');
				$page_data['datepicker_date_format'] = $this->config->item('ui_format_date_literal', 'date_formats');
				$page_data['datepicker_alt_format'] = $this->config->item('ui_format_date_numeric', 'date_formats');
				$this->template_lite->assign('page_data', $page_data);

				///// check pay services exists
				if($this->pg_module->is_module_installed('services')){
					$this->load->model('Services_model');			
					$services_params = array(
						'where_in' => array(
							'template_gid' => array(
								'listings_featured_template',
								'listings_lift_up_template', 
								'listings_lift_up_country_template', 
								'listings_lift_up_region_template', 
								'listings_lift_up_city_template', 
								'listings_highlight_template',
								'listings_slide_show_template',
							),
						),
						'where' => array(
							'status' => 1,
						),
					);
					$services_count = $this->Services_model->get_service_count($services_params);
					if($services_count > 0){
						$listings_services = $this->Services_model->get_service_list($services_params);
						foreach($listings_services as $service){
							$this->template_lite->assign('use_'.$service['gid'], $service['status']);
						}
						$this->template_lite->assign('use_services', true);
					}else{
						$this->template_lite->assign('use_services', false);
					}
				}
			break;
			case 'description':
				$this->load->model('Field_editor_model');
				$property_type_gid = $this->Listings_model->get_field_editor_type($data);
				$this->Field_editor_model->initialize($property_type_gid);
				$property_type_sections = $this->Field_editor_model->get_section_list();
				$sections = array();
				foreach($property_type_sections as $section){
					$sections[] = $section['gid'];
				}
				$this->template_lite->assign('sections_data', array_values($property_type_sections));
			
				$params['where_in']['section_gid'] = $sections;
				$fields_data = $this->Field_editor_model->get_form_fields_list($data, $params);
				$this->template_lite->assign('fields_data', $fields_data);
			break;
			case 'gallery':
				// photos
				$gallery_type_config = $this->Listings_model->get_gallery_type();
				
				$this->load->model('Uploads_model');
				$gallery_type_config['upload_settings'] = $this->Uploads_model->get_config($gallery_type_config['gid_upload_config']);
				
				$max_size = $gallery_type_config['upload_settings']['max_size'];
			
				$i = 1;
				while($max_size > 1000){$max_size /= 1024; $i++;}
				$max_size = round($max_size, 1);
				if($max_size){
					$gallery_type_config['upload_settings']['max_size_str'] = 
						$max_size.' '.l('text_filesize_'.$i, 'listings');
				}
				$this->template_lite->assign('gallery_settings', $gallery_type_config);
				
				//virtual tours
				$virtual_tour_config = $this->Listings_model->get_vtour_type();
				
				$this->load->model('Uploads_model');
				$virtual_tour_config['upload_settings'] = $this->Uploads_model->get_config($virtual_tour_config['gid_upload_config']);
				
				$max_size = $virtual_tour_config['upload_settings']['max_size'];
			
				$i = 1;
				while($max_size > 1000){$max_size /= 1024; $i++;}
				$max_size = round($max_size, 1);
				if($max_size){
					$virtual_tour_config['upload_settings']['max_size_str'] = 
						$max_size.' '.l('text_filesize_'.$i, 'listings');
				}
				$this->template_lite->assign('vtour_settings', $virtual_tour_config);
				
				// files
				$file_type_config = $this->Listings_model->get_file_type();
				
				$max_size = $file_type_config['max_size'];
			
				$i = 1;
				while($max_size > 1000){$max_size /= 1024; $i++;}
				$max_size = round($max_size, 1);
				if($max_size) $file_type_config['max_size_str'] = $max_size.' '.l('text_filesize_'.$i, 'listings');
				$this->template_lite->assign('file_settings', $file_type_config);
				
				// videos
				$video_type_config = $this->Listings_model->get_video_type();
				
				$max_size = $video_type_config['max_size'];
			
				$i = 1;
				while($max_size > 1000){$max_size /= 1024; $i++;}
				$max_size = round($max_size, 1);
				if($max_size) $video_type_config['max_size_str'] = $max_size.' '.l('text_filesize_'.$i, 'listings');
				$this->template_lite->assign('video_settings', $video_type_config);
			break;
			case 'map':
				$markers = array(
					array( 
						'gid'			=> $data['id'], 
						'country'		=> $data['country'],
						'region'		=> $data['region'],
						'city'			=> $data['city'],
						'address'		=> $data['address'],
						'postal_code'	=> $data['zip'],
						'lat'			=> (float)$data['lat'], 
						'lon'			=> (float)$data['lon'], 
						'dragging'		=> true,
						'info'			=> ($data['location'] ? $data['location'] : ''),
					),
				);	
				$this->template_lite->assign('markers', $markers);		
				
				$current_language = $this->pg_language->get_lang_by_id($this->pg_language->current_lang_id);
				$view_settings = array(
					'disable_smart_zoom' => 1,
					'zoom_listener' => 'get_listing_zoom_data', 
					'type_listener' => 'get_listing_type_data',
					'drag_listener' => 'get_listing_drag_data',
					'lang' => $current_language['code'],
				);
				$this->template_lite->assign('map_settings', $view_settings);
				
				if($listing_id){
					$this->load->model('geomap/models/Geomap_model');
					$this->load->model('geomap/models/Geomap_settings_model');
					$map_gid = $this->Geomap_model->get_default_driver_gid();
					$map_settings = $this->Geomap_settings_model->get_settings($map_gid, 0, $listing_id, 'listing_view');
					$this->template_lite->assign('listing_map_settings', $map_settings);
				}
			break;
			case 'calendar':
				
			break;
		}

		$this->template_lite->assign('section_gid', $section_gid);
		$this->template_lite->assign('data', $data);
		
		$this->Menu_model->set_menu_active_item('admin_listings_menu', 'listings-list-item');
		$this->template_lite->view('edit_listings_form');
	}
	
	/**
	 * Render listing services action
	 * @param integer $listing_id listing identifier
	 */
	public function services($listing_id){
		$this->load->model('Listings_model');
		
		$this->Listings_model->set_format_settings('use_format', false);
		$data = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);
		
		$this->config->load('date_formats', TRUE);
		$page_data['date_format'] = $this->config->item('st_format_date_literal', 'date_formats');
		$page_data['date_time_format'] = $this->config->item('st_format_date_time_literal', 'date_formats');

		$this->template_lite->assign('section_gid', 'services');
		$this->template_lite->assign('data', $data);
		$this->template_lite->assign('page_data', $page_data);

		if($this->pg_module->is_module_installed('services')){
			$this->load->model('Services_model');
			///// check pay services exists
			$services_params = array(
				'where_in' => array(
					'template_gid' => array(
						'listings_featured_template',
						'listings_lift_up_template', 
						'listings_highlight_template',
						'listings_slide_show_template',
					),
				),
				'where' => array(
					'status' => 1,
				),
			);
			$services_count = $this->Services_model->get_service_count($services_params);
			if($services_count){
				$page_data['listings_services'] = $this->Services_model->get_service_list($services_params);
				$page_data['use_services'] = true;
			}
		}else{
			$page_data['use_services'] = false;
		}
	
		$this->config->load('date_formats', TRUE);
		$page_data['date_format'] = $this->config->item('st_format_date_literal', 'date_formats');
		$page_data['date_time_format'] = $this->config->item('st_format_date_time_literal', 'date_formats');

		$this->template_lite->assign('section_gid', 'services');
		$this->template_lite->assign('data', $data);
		$this->template_lite->assign('page_data', $page_data);
		$this->template_lite->assign('section_gid', 'services');
		
		$this->template_lite->view('edit_listings_services_form');
	}
	
	/**
	 * Remove listings by identifiers
	 * @param integer $listing_ids listing identifier
	 */
	public function delete($listing_ids=null){
		if(!$listing_ids) $listing_ids = $this->input->post('ids', true);
		if(!empty($listing_ids)){
			$this->load->model('Listings_model');
			foreach((array)$listing_ids as $listing_id) $this->Listings_model->delete_listing($listing_id);
			$this->system_messages->add_message('success', l('success_listings_delete', 'listings'));
		}		
		redirect(site_url().'admin/listings/index');
	}
	
	/**
	 * Activate listing
	 * @param integer $listing_id listing identifier
	 * @param integer $status activity status
	 */
	public function activate($listing_id, $status=0){
		$this->load->model('Listings_model');
		if(!empty($listing_id)){
			if($status){
				$this->Listings_model->set_format_settings('use_format', false);
				$data = $this->Listings_model->get_listing_by_id($listing_id);
				$this->Listings_model->set_format_settings('use_format', true);
				$post_period = $this->pg_module->get_module_config('listings', 'default_activation_period');
				if(strtotime($data['date_activity']) > time()){
					$date_activity = date('Y-m-d H:i:s', strtotime($data['date_activity'])+$post_period*24*60*60);
				}else{
					$date_activity = date('Y-m-d H:i:s', time()+$post_period*24*60*60);
				}
				$this->Listings_model->activate_listing($listing_id, $status, $date_activity);
				$this->system_messages->add_message('success', l('success_activate_listing', 'listings'));
			}else{
				$this->Listings_model->activate_listing($listing_id, $status);
				$this->system_messages->add_message('success', l('success_deactivate_listing', 'listings'));
			}
		}
		$url = site_url().'admin/listings/index/';
		redirect($url);
	}
	
	/**
	 * Render module settings
	 */
	public function settings(){
		$this->Menu_model->set_menu_active_item('admin_listings_menu', 'listings_settings_item');
		
		if($this->input->post('btn_save')){
			$this->load->model('Listings_model');
			
			$validate_data = $this->Listings_model->validate_listings_settings($post_data);
			if(!empty($validate_data['errors'])){
				$this->system_messages->add_message('error', $validate_data['errors']);
				$data = $validate_data['data'];
			}else{
				foreach($validate_data['data'] as $setting=>$value){
					$this->pg_module->set_module_config('listings', $setting, $value);
				}
				$this->system_messages->add_message('success', l('success_settings_saved', 'listings'));
				$data = $validate_data['data'];
			}
		}else{
			
		}
		$this->template_lite->assign('data', $data);		
		$this->template_lite->view('settings');
	}
	
	/**
	 * Render listings pending moderation action
	 * @param string $order sorting order
	 * @param string $order_direction order direction
	 * @param integer $page page of results
	 */
	public function moderation($order='date_created', $order_direction='DESC', $page=1){
		$this->load->model('listings/models/Listings_moderation_model');

		$current_settings = isset($_SESSION['listings_list'])?$_SESSION['listings_list']:array();

		if(!isset($current_settings['order'])) $current_settings['order'] = 'date_created';
		if(!isset($current_settings['order_direction'])) $current_settings['order_direction'] = 'DESC';
		if(!isset($current_settings['page'])) $current_settings['page'] = 1;

		$current_settings['page'] = $page;

		if(!$order) $order = $current_settings['order'];
		$this->template_lite->assign('order', $order);
		$current_settings['order'] = $order;

		if(!$order_direction) $order_direction = $current_settings['order_direction'];
		$this->template_lite->assign('order_direction', $order_direction);
		$current_settings['order_direction'] = $order_direction;

		$listings_count = $this->Listings_moderation_model->get_listings_count();

		if(!$page) $page = $current_settings['page'];
		
		$items_on_page = $this->pg_module->get_module_config('start', 'admin_items_per_page');
		
		$this->load->helper('sort_order');
		$page = get_exists_page_number($page, $listings_count, $items_on_page);
		$current_settings['page'] = $page;

		$_SESSION['listings_list'] = $current_settings;

		$sort_links = array(
			'date_created' => site_url().'admin/listings/moderation/date_created/'.(($order!='date_created' xor $order_direction=='DESC')?'ASC':'DESC'),
		);

		$this->template_lite->assign('sort_links', $sort_links);

		if($listings_count > 0){
			$listings = $this->Listings_moderation_model->get_listings_list($page, $items_on_page, array($order=>$order_direction));
			$this->template_lite->assign('listings', $listings);
		}

		$this->load->helper('navigation');
		$this->config->load('date_formats', TRUE);
		$url = site_url().'admin/listings/moderation/'.$order.'/'.$order_direction.'/';
		$page_data = get_admin_pages_data($url, $listings_count, $items_on_page, $page, 'briefPage');
		$page_data['date_format'] = $this->config->item('st_format_date_time_literal', 'date_formats');
		$this->template_lite->assign('page_data', $page_data);

		$this->Menu_model->set_menu_active_item('admin_menu', 'moderation-items');
		$this->template_lite->view('list_listings_moderation');
	}
	
	/**
	 * Edit moderated listing
	 * @param integer $listing_id listing identifier
	 * @param string $section_gid view section
	 */
	public function moderation_edit($listing_id, $section_gid='overview'){
		$this->load->model('listings/models/Listings_moderation_model');
		$data = $this->Listings_moderation_model->get_listing_by_id($listing_id);
		
		$property_type_gid = $this->Listings_model->get_field_editor_type($data);
		
		$this->load->model('Field_editor_model');
		$this->Field_editor_model->initialize($property_type_gid);
		$fields_for_select = $this->Field_editor_model->get_fields_for_select($section_gid);
		$this->Listings_moderation_model->set_dop_fields($fields_for_select);

		
		if(isset($_POST['btn_save']) || isset($_POST['btn_save_approve']) || isset($_POST['btn_decline'])){
			$post_data = (array)$this->input->post('data', true);
			
			foreach($fields_for_select as $field){
				$post_data[$field] = $this->input->post($field, true);
			}

			$validate_data = $this->Listings_model->validate_listing($listing_id, $post_data);
			$validate_data['data']['admin_alert'] = $this->input->post('admin_alert', true);

			if(!empty($validate_data['errors'])){
				$this->system_messages->add_message('error', $validate_data['errors']);
			}else{
				///// delete listing file
				if($this->input->post('listing_file_delete') && $listing_id && $data['listing_file']){
					$this->load->model('File_uploads_model');
					$this->File_uploads_model->delete_upload($this->Listings_model->file_config_id, $data['prefix'], $data['listing_file']);
					$validate_data['data']['listing_file'] = '';
					$validate_data['data']['listing_file_name'] = '';
					$validate_data['data']['listing_file_comment'] = '';
				}

				///// delete video
				if($this->input->post('listing_video_delete') && $listing_id && $data['listing_video']){
					$this->load->model('Video_uploads_model');
					$this->Video_uploads_model->delete_upload($this->Listings_model->video_config_id, $data['prefix'], $data['listing_video'], $data['listing_video_image'], $data['listing_video_data']['data']['upload_type']);
					$validate_data['data']['listing_video'] = $validate_data['data']['listing_video_image'] = $validate_data['data']['listing_video_data'] = '';
				}
				
				$this->Listings_moderation_model->save_listing($listing_id, $validate_data['data']);

				if(isset($_POST['btn_save'])){
					$this->system_messages->add_message('success', l('success_update_moderation', 'listings'));
					redirect(site_url().'admin/listings/moderation_edit/'.$listing_id.'/'.$section_gid);
				}

				if(isset($_POST['btn_save_approve'])){
					$this->Listings_moderation_model->approve_listing($listing_id);
					
					$this->load->model('Notifications_model');
					
					$this->config->load('date_formats', TRUE);
					$date_format = $this->config->item('date_format_date_literal', 'date_formats');
			
					$listing_data = $this->Listings_model->get_listing_by_id($listing_id);
					$email = $listing_data['user']['email'];
					$listing_data['user'] = $listing_data['user']['output_name'];
					$listing_data['listing'] = l('field_id', 'listings').$listing_data['id'].', '.$listing_data['location'];
					$listing_data['status'] = $listing_data['status'] ? l('text_approve_active', 'listings').' '.date($date_format, strtotime($listing_data['date_expire'])) : l('text_approve_inactive', 'listings');
					$this->Notifications_model->send_notification($email, 'listing_status_updated', $listing_data);
					
					$this->system_messages->add_message('success', l('success_approve_moderation', 'listings'));
					redirect(site_url().'admin/listings/moderation/');
				}

				if(isset($_POST['btn_decline'])){
					$this->Listings_moderation_model->decline_listing($listing_id);
					
					$this->load->model('Notifications_model');
					
					$listing_data = $this->Listings_model->get_listing_by_id($listing_id);
					$listing_data['listing'] = l('field_id', 'listings').$listing_data['id'].', '.$listing_data['location'];
					$listing_data['status'] = l('text_decline', 'listings');
					$this->Notifications_model->send_notification($listing_data['user']['email'], 'listing_status_updated', $listing_data);
					
					$this->system_messages->add_message('success', l('success_decline_moderation', 'listings'));
					redirect(site_url().'admin/listings/moderation/');
				}
			}
			$validate_data['data'] = $this->Listings_moderation_model->format_listing($validate_data['data']);
			$data = array_merge($data, $validate_data['data']);
		}

		$this->template_lite->assign('section_gid', $section_gid);
		$this->template_lite->assign('data', $data);
		
		$params['where']['editor_type_gid'] = $property_type_gid;
		$params['where_in']['field_type'] = array('text', 'textarea');
		$fields_data = $this->Field_editor_model->get_form_fields_list($data, $params);
		$this->template_lite->assign('fields_data', $fields_data);
		
		$compare_data = $this->Listings_moderation_model->compare($listing_id);
		$this->template_lite->assign('compare_data', $compare_data);

		$this->Menu_model->set_menu_active_item('admin_menu', 'moderation-items');
		$this->template_lite->view('edit_listings_moderation_form');
	}
	
	/**
	 * Return upload form by ajax
	 * @param integer $listing_id listing identifier
	 * @param integer $photo_id photo identifier
	 */
	public function ajax_photo_form($listing_id, $photo_id){
		$this->load->model('Upload_gallery_model');
		if($photo_id){
			$photo = $this->Upload_gallery_model->get_file_by_id($photo_id);
			$this->template_lite->assign('data', $photo);
		}
		
		$this->load->model('Listings_model');
		$photo_type = $this->Listings_model->get_gallery_type();

		$this->load->model('uploads/models/Uploads_config_model');
		$upload_config = $this->Uploads_config_model->get_config_by_gid($photo_type['gid_upload_config']);
		$upload_config['file_formats'] = implode(', ', (array)unserialize($upload_config['file_formats']));
		$upload_config['requirements_str'] = l('file_upload_settings_str', 'listings');
		$upload_config['requirements_str'] = str_replace('[width]', $upload_config['max_width'], $upload_config['requirements_str']);
		$upload_config['requirements_str'] = str_replace('[height]', $upload_config['max_height'], $upload_config['requirements_str']);
		$upload_config['requirements_str'] = str_replace('[formats]', $upload_config['file_formats'], $upload_config['requirements_str']);
		$this->template_lite->assign('upload_config', $upload_config);

		$this->template_lite->view('photo_edit_form');
	}
	
	/**
	 * Upload photo to listing gallery
	 * @param integer $listing_id listing identifier
	 * @param integer $photo_id photo identifier
	 */
	public function photo_upload($listing_id, $photo_id=0){
		$this->load->model('Listings_model');
		$return = $this->Listings_model->save_photo($listing_id, $photo_id, 'photo_file', false);
		if($this->input->post('no_ajax', true)==1){
			if(!empty($return['errors'])){
				$this->system_messages->add_message('errors', implode('<br>', $return['errors']));
			}
			redirect(site_url().'admin/listings/edit/'.$listing_id.'/gallery');
		}else{
			$return['errors'] = $return['error'] = implode('<br>', $return['errors']);
			echo json_encode($return); exit;
		}
	}
	
	/**
	 * Save photo data by ajax
	 * @param integer $listing_id listing identifier
	 * @param integer $photo_id photo identifier
	 */
	public function ajax_save_photo_data($listing_id, $photo_id){
		$this->load->model('Upload_gallery_model');
		if($photo_id) $photo = $this->Upload_gallery_model->get_file_by_id($photo_id);
		
		$this->load->model('listings/models/Listings_moderation_model');
		$photo_type = $this->Listings_model->get_gallery_type();

		$post_data['comment'] = $this->input->post('comment', true);
		$validate_data = $this->Upload_gallery_model->validate_file_data($photo_id, $photo_type['id'], $post_data);
		if(!empty($validate_data['errors'])){
			$return['errors'] = $validate_data['errors'];
		}else{
			$data = $validate_data['data'];
			$data['type_id'] = $photo_type['id'];
			$data['object_id'] = $listing_id;
			$return = $this->Upload_gallery_model->save_file($photo_id, $data, '', false);
		}

		$return['errors'] = $return['error'] = implode('<br>', $return['errors']);
		$return['success'] = (!empty($return['errors']))?'':l('photo_successfully_saved', 'listings');
		echo json_encode($return); return;
	}

	/**
	 * Remove photo from listing gallery by ajax
	 * @param integer $listing_id listing identifier
	 * @param integer $photo_id photo identifier
	 */
	public function ajax_delete_photo($listing_id, $photo_id){
		$this->load->model('Listings_model');
	
		// remove photo
		$result = $this->Listings_model->delete_photo($listing_id, $photo_id);
		if(!$result){
			$return['error'] = '';
			echo json_encode($return); exit;
		}

		$return['success'] = l('photo_successfully_deleted', 'listings');
		echo json_encode($return); exit;
	}

	/**
	 * Save photo sorting by ajax
	 * @param integer $listing_id listing identifier
	 */
	public function ajax_save_photo_sorting($listing_id){
		$this->load->model('Listings_model');
		
		$sorter = (array)$this->input->post('sorter', true);

		$sorter_data = array();
		foreach($sorter as $item_str => $sort_index){
			$photo_id = intval(str_replace('pItem', '', $item_str));
			$sorter_data[$photo_id] = $sort_index;
		}
			
		if(!empty($sorter_data)){
			$this->Listings_model->set_photo_sorter($listing_id, $sorter_data);
		}	
		
		$return['success'] = l('photo_sorter_successfully_saved', 'listings');
		echo json_encode($return); return;
	}

	/**
	 * Refresh photo block
	 * @param integer $listing_id listing identifier
	 * @param integer $photo_id photo identifier
	 */
	public function ajax_reload_photo_block($listing_id, $photo_id){
		$this->load->model('Upload_gallery_model');
		$photo = $this->Upload_gallery_model->get_file_by_id($photo_id);
				
		$this->template_lite->assign('photo', $photo);
		$this->template_lite->view('photo_view_block');
	}
	
	/**
	 * Return panorama upload form by ajax
	 * @param integer $listing_id listing identifier
	 * @param integer $panorama_id panorama identifier
	 */
	public function ajax_panorama_form($listing_id, $panorama_id){
		$this->load->model('Upload_gallery_model');
		if($panorama_id){
			$panorama = $this->Upload_gallery_model->get_file_by_id($panorama_id);
			$this->template_lite->assign('data', $panorama);
		}
		
		$this->load->model('Listings_model');
		$virtual_tour_type = $this->Listings_model->get_vtour_type();

		$this->load->model('uploads/models/Uploads_config_model');
		$upload_config = $this->Uploads_config_model->get_config_by_gid($virtual_tour_type['gid_upload_config']);
		$upload_config['file_formats'] = implode(', ', (array)unserialize($upload_config['file_formats']));
		$upload_config['requirements_str'] = l('file_upload_settings_str', 'listings');
		$upload_config['requirements_str'] = str_replace('[width]', $upload_config['max_width'], $upload_config['requirements_str']);
		$upload_config['requirements_str'] = str_replace('[height]', $upload_config['max_height'], $upload_config['requirements_str']);
		$upload_config['requirements_str'] = str_replace('[formats]', $upload_config['file_formats'], $upload_config['requirements_str']);
		$this->template_lite->assign('upload_config', $upload_config);

		$this->template_lite->view('panorama_edit_form');
	}
	
	/**
	 * Upload panorama to listing virtual tour
	 * @param integer $listing_id listing identifier
	 * @param integer $panorama_id panorama identifier
	 */
	public function panorama_upload($listing_id, $panorama_id=0){
		$this->load->model('Listings_model');
		$return = $this->Listings_model->save_panorama($listing_id, $panorama_id, 'panorama_file', false);
		if($this->input->post('no_ajax', true)==1){
			if(!empty($return['errors'])){
				$this->system_messages->add_message('errors', implode('<br>', $return['errors']));
			}
			redirect(site_url().'admin/listings/edit/'.$listing_id.'/gallery');
		}else{
			$return['errors'] = $return['error'] = implode('<br>', $return['errors']);
			echo json_encode($return); exit;
		}
	}
	
	/**
	 * Save panorama data by ajax
	 * @param integer $listing_id listing identifier
	 * @param integer $panorama_id panorama identifier
	 */
	public function ajax_save_panorama_data($listing_id, $panorama_id){
		$this->load->model('Upload_gallery_model');
		if($panorama_id) $panorama = $this->Upload_gallery_model->get_file_by_id($panorama_id);
		
		$this->load->model('listings/models/Listings_moderation_model');
		$virtual_tour_type = $this->Listings_model->get_vtour_type();

		$post_data['comment'] = $this->input->post('comment', true);
		$validate_data = $this->Upload_gallery_model->validate_file_data($panorama_id, $photo_type['id'], $post_data);
		if(!empty($validate_data['errors'])){
			$return['errors'] = $validate_data['errors'];
		}else{
			$data = $validate_data['data'];
			$data['type_id'] = $virtual_tour_type['id'];
			$data['object_id'] = $listing_id;
			$return = $this->Upload_gallery_model->save_file($panorama_id, $data, '', false);
		}

		$return['error'] = implode('<br>', $return['errors']);
		$return['success'] = (!empty($return['errors']))?'':l('panorama_successfully_saved', 'listings');
		echo json_encode($return); return;
	}

	/**
	 * Remove panorama from listing gallery by ajax
	 * @param integer $listing_id listing identifier
	 * @param integer $panorama_id panorama identifier
	 */
	public function ajax_delete_panorama($listing_id, $panorama_id){
		$this->load->model('Listings_model');
		$result = $this->Listings_model->delete_panorama($listing_id, $panorama_id);
		if($result){
			$return['success'] = l('panorama_successfully_deleted', 'listings');
		}else{
			$return['error'] = '';
		}
		echo json_encode($return); exit;
	}

	/**
	 * Save panorama sorting by ajax
	 * @param integer $listing_id listing identifier
	 */
	public function ajax_save_panorama_sorting($listing_id){
		$this->load->model('Listings_model');
		$this->load->model('Upload_gallery_model');
		$sorter = $this->input->post('sorter', true);

		foreach($sorter as $item_str => $sort_index){
			$panorama_id = intval(str_replace('pItem', '', $item_str));
			$this->Upload_gallery_model->save_sorter($panorama_id, $sort_index);
		}
		$return['success'] = l('panorama_sorter_successfully_saved', 'listings');
		echo json_encode($return); return;
	}

	/**
	 * Refresh panorama block
	 * @param integer $listing_id listing identifier
	 * @param integer $panorama_id panorama identifier
	 */
	public function ajax_reload_panorama_block($listing_id, $panorama_id){
		$this->load->model('Upload_gallery_model');
		$panorama = $this->Upload_gallery_model->get_file_by_id($panorama_id);
				
		$this->template_lite->assign('panorama', $panorama);
		$this->template_lite->view('panorama_view_block');
	}
	
	/**
	 * Reload panorama block
	 * @param integer $listing_id listing identifier
	 * @param integer $panorama_id panorama identifier
	 */
	public function ajax_reload_panorama($listing_id, $panorama_id){
		$this->load->model('Upload_gallery_model');
		$panorama = $this->Upload_gallery_model->get_file_by_id($panorama_id);
		if($listing_id != $panorama['object_id']){show_404();}	
		$this->load->helpers('listings');
		echo virtual_tour_block(array('data'=>$panorama));
	}
	
	/**
	 * Return export form block
	 */
	public function ajax_get_export_extend_form(){
		$operation_type = $this->input->post('type', true);
		$category_id = $this->input->post('id_category');
		$category_id = explode('_', $category_id);
		
		$this->load->model('Listings_model');
		
		$data = array('operation_type'=>$operation_type, 'id_category'=>$category_id[0]);
		$property_type_gid = $this->Listings_model->get_field_editor_type($data);
	
		$this->load->model('listings/models/Listings_export_model');
		echo $this->Listings_export_model->get_export_extend_form($property_type_gid, 'admin_export_form_gid');
		exit;
	}
	
	/**
	 * Manage wish lists
	 * @param string $order sorting order
	 * @param string $order_direction order direction
	 * @param integer $page page of results
	 */
	public function wish_lists($order='date_add', $order_direction='DESC', $page=1){
		$this->load->model('listings/models/Wish_list_model');
	
		$this->Menu_model->set_menu_active_item('admin_listings_menu', 'wish-lists-item');

		$filters = array();
		
		$current_settings = isset($_SESSION['wish_list']) ? $_SESSION['wish_list'] : array();
		if(!isset($current_settings['order'])) $current_settings['order'] = 'date_add';
		if(!isset($current_settings['order_direction'])) $current_settings['order_direction'] = 'DESC';
		if(!isset($current_settings['page'])) $current_settings['page'] = 1;

		$current_settings['page'] = $page;
		
		if(!$order) $order = $current_settings['order'];
		$this->template_lite->assign('order', $order);
		$current_settings['order'] = $order;

		if(!$order_direction) $order_direction = $current_settings['order_direction'];
		$this->template_lite->assign('order_direction', $order_direction);
		$current_settings['order_direction'] = $order_direction;

		$wish_lists_count = $this->Wish_list_model->get_wish_lists_count($filters);

		if(!$page) $page = $current_settings['page'];
		$items_on_page = $this->pg_module->get_module_config('start', 'admin_items_per_page');
			
		$this->load->helper('sort_order');
		$page = get_exists_page_number($page, $wish_lists_count, $items_on_page);
		$current_settings['page'] = $page;

		$_SESSION['wish_list'] = $current_settings;

		if($order == 'user') $order = 'id_user';
		
		if($wish_lists_count > 0){
			$wish_lists = $this->Wish_list_model->get_wish_lists_list($filters, $page, $items_on_page, array($order=>$order_direction));
			$this->template_lite->assign('wish_lists', $wish_lists);
		}

		$this->load->helper('navigation');
		$this->config->load('date_formats', TRUE);
		$url = site_url().'admin/listings/wish_lists/'.$order.'/'.$order_direction.'/';
		$page_data = get_admin_pages_data($url, $wish_lists_count, $items_on_page, $page, 'briefPage');
		$page_data['date_format'] = $this->config->item('st_format_date_time_literal', 'date_formats');
		$this->template_lite->assign('page_data', $page_data);
		
		$this->template_lite->view('wish_lists');
	}
	
	/**
	 * Render edit wish list action
	 * @param integer $wish_list_id wish list identifier
	 */
	public function edit_wish_list($wish_list_id=0){
		$this->load->model('listings/models/Wish_list_model');
		
		if($wish_list_id){
			$data = $this->Wish_list_model->get_wish_list_by_id($wish_list_id, true);
		}else{
			$data = array();
		}
		
		if($this->input->post('btn_save')){
			$post_data = $this->input->post('data', true);
			$post_data['id_user'] = 0;
			$validate_data = $this->Wish_list_model->validate_wish_list($wish_list_id, $post_data);
			if(!empty($validate_data['errors'])){
				$this->system_messages->add_message('error', $validate_data['errors']);
			}else{
				if(!empty($validate_data['data'])){
					$this->Wish_list_model->save_wish_list($wish_list_id, $validate_data['data']);
					if($wish_list_id){
						$this->system_messages->add_message('success', l('success_update_wish_list', 'listings'));
					}else{
						$this->system_messages->add_message('success', l('success_add_wish_list', 'listings'));
					}
					$url = site_url().'admin/listings/wish_lists';
					redirect($url);
				}
			}
		}
		$this->template_lite->assign('data', $data);
		
		$this->template_lite->assign('current_lang_id', $this->pg_language->current_lang_id);
		$this->template_lite->assign('langs', $this->pg_language->languages);

		$this->Menu_model->set_menu_active_item('admin_listings_menu', 'wish-lists-item');
		$this->template_lite->view('edit_wish_list_form');
	}
	
	/**
	 * Remove wish lists
	 * @param integer $wish_list_ids wish list identifiers
	 */
	public function delete_wish_list($wish_list_ids=null){
		if(!$wish_list_ids) $wish_list_ids = $this->input->post('ids', true);
		if(!empty($wish_list_ids)){
			$this->load->model('listings/models/Wish_list_model');
			foreach((array)$wish_list_ids as $wish_list_id) $this->Wish_list_model->delete_wish_list($wish_list_id);
			$this->system_messages->add_message('success', l('success_wish_lists_delete', 'listings'));
		}		
		redirect(site_url().'admin/listings/wish_lists/');
	}
	
	/**
	 * Activate wish list
	 * @param integer $wish_list_id wish list identifier
	 * @param integer $status activity status
	 */
	function activate_wish_list($wish_list_id, $status=0){
		if(!empty($wish_list_id)){
			$this->load->model('listings/models/Wish_list_model');
			if($status){
				if($this->Wish_list_model->activate_wish_list($wish_list_id)){
					$this->system_messages->add_message('success', l('success_activate_wish_list', 'listings'));
				}else{
					$this->system_messages->add_message('error', l('error_activate_wish_list', 'listings'));
				}
			}else{
				$this->Wish_list_model->deactivate_wish_list($wish_list_id, $status);
				$this->system_messages->add_message('success', l('success_deactivate_wish_list', 'listings'));
			}
		}
		$url = site_url().'admin/listings/wish_lists/';
		redirect($url);
	}
	
	/**
	 * Activate wish list
	 * @param integer $wish_list_id wish list identifier
	 * @param integer $page page of results
	 */
	function wish_list_content($wish_list_id, $page=1){
		$this->load->model('Listings_model');
		$this->load->model('listings/models/Wish_list_model');
		
		$this->Wish_list_model->set_format_content_settings('get_listings', true);
		$data = $this->Wish_list_model->get_wish_list_by_id($wish_list_id, true);
		$this->Wish_list_model->set_format_content_settings('get_listings', false);
		if(!$data) show_404();
		$this->template_lite->assign('wish_list', $data);
	
		$items_on_page = $this->pg_module->get_module_config('start', 'admin_items_per_page');
		
		$listings_count = $this->Wish_list_model->get_wish_list_content_count($wish_list_id);
		if($listings_count > 0){
			$this->Wish_list_model->set_format_content_settings('get_listings', true);
			$listings = $this->Wish_list_model->get_wish_list_content_list($wish_list_id, $page, $items_on_page);
			$this->Wish_list_model->set_format_content_settings('get_listings', false);
			$this->template_lite->assign('listings', $listings);
		}
		
		$this->load->helper('navigation');
		$this->config->load('date_formats', TRUE);
		$url = site_url().'admin/listings/wish_list_content/'.$wish_list_id.'/';
		$page_data = get_admin_pages_data($url, $listings_count, $items_on_page, $page, 'briefPage');
		$page_data['date_format'] = $this->config->item('st_format_date_time_literal', 'date_formats');
		$this->template_lite->assign('page_data', $page_data);

		$this->Menu_model->set_menu_active_item('admin_listings_menu', 'wish-lists-item');
	
		$this->system_messages->set_data('header', str_replace('[name]', $data['name'], l('admin_header_wish_list_content', 'listings')));
	
		$this->system_messages->set_data('back_link', site_url().'admin/listings/wish_lists');
		$this->template_lite->view('wish_list_content');
	}
	
	/**
	 * Add listings to wish list
	 * @param integer $wish_list_id wish list identifier
	 */
	public function add_to_wish_list($wish_list_id){
		$listing_ids = $this->input->post('ids', true);
		$this->load->model('listings/models/Wish_list_model');
		$this->Wish_list_model->add_to_wish_list($wish_list_id, $listing_ids);
		$this->system_messages->add_message('success', l('success_add_to_wish_list', 'listings'));
		redirect(site_url().'admin/listings/wish_list_content/'.$wish_list_id);
	}
	
	/**
	 * Remove listing from wish list
	 * @param integer $wish_list_id wish list identifier
	 * @param integer $listing_id listing identifier
	 */
	public function delete_from_wish_list($wish_list_id, $listing_id=false){
		$this->load->model('listings/models/Wish_list_model');
		if($listing_id){
			$this->Wish_list_model->delete_from_wish_list($wish_list_id, array($listing_id));
		}else{
			$this->Wish_list_model->clear_wish_list($wish_list_id);
		}
		$this->system_messages->add_message('success', l('success_delete_from_wish_list', 'listings'));
		redirect(site_url().'admin/listings/wish_list_content/'.$wish_list_id);
	}
	
	/**
	 * Save wish list sorting by ajax
	 * @param integer $wish_list_id wish list identifier
	 */
	public function ajax_save_wish_list_sorting($wish_list_id){
		$this->load->model('listings/models/Wish_list_model');
		
		$sorter = $this->input->post('sorter' ,true);
		foreach($sorter as $item_str => $sort_index){
			$listing_id = intval(str_replace('pItem', '', $item_str));
			$this->Wish_list_model->save_content_sorter($wish_list_id, $listing_id, $sort_index);
		}
		
		$this->Wish_list_model->change_logo($wish_list_id);
		
		$return['success'] = l('success_wish_list_sorter_saved', 'listings');
		echo json_encode($return); return;
	}
	
	/**
	 * Remove listing from wish list
	 * @param integer $wish_list_id wish list identifier
	 */
	public function ajax_delete_from_wish_list($wish_list_id, $listing_id){
		$this->load->model('listings/models/Wish_list_model');
		$this->Wish_list_model->delete_from_wish_list($wish_list_id, array($listing_id));
		$return['success'] = l('success_delete_from_wish_list', 'listings');
		echo json_encode($return); 
		exit;
	}
	
	/**
	 * Refresh wish list block
	 * @param integer $listing_id listing identifier
	 */
	public function ajax_reload_wish_list_content($listing_id){
		$this->load->model('Listings_model');
		$this->Listings_model->set_format_settings('use_format', false);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);				
		$this->Listings_model->set_format_settings('use_format', true);
		$this->template_lite->assign('listing', $listing);
		$this->template_lite->view('listing_view_block');
	}
	
	/**
	 * Set map zoom for listing
	 * @param integer $listing_id listing identifier
	 * @param string $option_name map option name
	 */
	public function ajax_set_map($listing_id, $option_name){
		$this->Listings_model->set_format_settings('use_format', false);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings(array('use_format'=>true));
		
		if(!$listing) return;
		
		$this->load->model('Geomap_model');
		$this->load->model('geomap/models/Geomap_settings_model');
		
		$map_gid = $this->Geomap_model->get_default_driver_gid();
		$settings = $this->Geomap_settings_model->get_settings($map_gid, 0, $listing_id, 'listing_view');
		if(!$settings) $settings = array('gid'=>'listing_view');
		switch($option_name){
			case 'type': $settings['view_type'] = $this->input->post('type', true); break;
			case 'zoom': $settings['zoom'] = $this->input->post('zoom', true); break;
			case 'coordinates':
				$settings['lat'] = $this->input->post('lat', true);
				$settings['lon'] = $this->input->post('lon', true);
			break;
		}
	
		$validate_data = $this->Geomap_settings_model->validate_settings($settings);
		if(empty($validate_data['errors'])){
			$this->Geomap_settings_model->save_settings($map_gid, 0, $listing_id, 'listing_view', $validate_data['data']);
		}
	
		if($option_name == 'coordinates'){
			$data['lat'] = $this->input->post('lat', true);
			$data['lon'] = $this->input->post('lon', true);
			$this->Listings_model->save_listing($listing_id, $data);
		}
	}
	
	/**
	 * Bookings of listings
	 * @param string $status order status
	 * @param string $order sorting order
	 * @param string $order_direction order direction
	 * @param integer $page page of results
	 */
	public function orders($status=null, $order='modified', $order_direction='DESC', $page=1){
		$current_settings = isset($_SESSION['listings_booking']) ? $_SESSION['listings_booking'] : array();
		if(!isset($current_settings['filters'])) $current_settings['data'] = array('status'=>'wait');
		if(!isset($current_settings['order'])) $current_settings['order'] = 'modified';
		if(!isset($current_settings['order_direction'])) $current_settings['order_direction'] = 'DESC';
		if(!isset($current_settings['page'])) $current_settings['page'] = '1';
		
		if($this->input->post('filter-submit')){
			$current_settings['filters'] = array('status'=>'wait');
			
			$post_data['id_listing'] = $this->input->post('id_listing', true);
			if(!empty($post_data['id_listing'])){
				$current_settings['filters']['listings'] = $post_data['id_listing'];
			}elseif(isset($current_settings['filters']['listings'])){
				unset($current_settings['filters']['listings']);
			}
			
			$post_data['id_owner'] = $this->input->post('id_owner', true);
			if(!empty($post_data['id_owner'])){
				$current_settings['filters']['owners'] = $post_data['id_owner'];
			}elseif(isset($current_settings['filters']['owners'])){
				unset($current_settings['filters']['owners']);
			}
			
			$post_data['id_user'] = $this->input->post('id_user', true);
			if(!empty($post_data['id_user'])){
				$current_settings['filters']['users'] = $post_data['id_user'];
			}elseif(isset($current_settings['filters']['users'])){
				unset($current_settings['filters']['users']);
			}
		}

		if($this->input->post('filter-reset')){
			$current_settings['filters'] = array('status'=>'wait');
		}
		
		$this->load->model('listings/models/Listings_booking_model');
		
		if($status){
			if($status != $current_settings['filters']['status']){
				$page = 1;
			}
			$current_settings['filters']['status'] = $status;
		}else{
			$status = $current_settings['filters']['status'];
		}
		$filters = $current_settings['filters'];

		$order = trim(strip_tags($order));
		if(!empty($order)) $current_settings['order'] = $order;
		$order = $current_settings['order'];
		$this->template_lite->assign('order', $order);
	
		$order_direction = strtoupper(trim(strip_tags($order_direction)));
		if(!empty($order_direction)) $current_settings['order_direction'] = $order_direction;
		$order_direction = $current_settings['order_direction'];
		$this->template_lite->assign('order_direction', $order_direction);
		
		$page = intval($page);
		if(!empty($page)) $current_settings['page'] = $page;
		$page = $current_settings['page'];
		$this->template_lite->assign('page', $page);
		
		if($order_direction != 'ASC') $order_direction = 'DESC';
		
		$items_on_page = $this->pg_module->get_module_config('start', 'admin_items_per_page');
		
		$items_count = $this->Listings_booking_model->get_periods_count($filters);
		
		$this->load->helper('sort_order');

		$current_settings['page'] = get_exists_page_number($page, $items_count, $items_on_page);
	
		$_SESSION['listings_booking'] = $current_settings;
	
		// sorting
		$sort_data = array(
			'url' => site_url().'admin/listings/orders/',
			'order' => $current_settings['order'],
			'direction' => $current_settings['order_direction'],
			'links' => array('modified'=>l('field_date_modified', 'listings')),
		);
		$this->template_lite->assign('sort_data', $sort_data);

		if($items_count > 0){
			$order_array = array();
			switch($order){
				case 'modified':
					$order_array['date_modified'] = $order_direction;
				break;
				default:
					$order_array[$order] = $order_direction;
				break;
			}
			$this->Listings_booking_model->set_format_settings('get_listing', true);
			$orders = $this->Listings_booking_model->get_periods_list($filters, $current_settings['page'], $items_on_page, $order_array);
			$this->Listings_booking_model->set_format_settings('get_listing', false);
			$this->template_lite->assign('orders', $orders);
		}
		$this->template_lite->assign('orders_count', $items_count);
		
		$this->load->helper('navigation');
		
		$url = site_url().'admin/listings/orders/'.$order.'/'.$order_direction.'/';

		$page_data = get_user_pages_data($url, $items_count, $items_on_page, $current_settings['page'], 'briefPage');
		$this->config->load('date_formats', TRUE);
		$page_data['date_format'] = $this->config->item('st_format_date_literal', 'date_formats');
		$page_data['date_time_format'] = $this->config->item('st_format_date_time_literal', 'date_formats');
		$this->template_lite->assign('page_data', $page_data);
		
		$status_data = array('approve'=>0, 'decline'=>0, 'wait'=>0);
		$status_data['approve'] = $this->Listings_booking_model->get_periods_count(array('status'=>'approve'));
		$status_data['decline'] = $this->Listings_booking_model->get_periods_count(array('status'=>'decline'));
		$status_data['wait'] = $this->Listings_booking_model->get_periods_count(array('status'=>'wait'));
		$this->template_lite->assign('status_data', $status_data);
		
		$this->template_lite->assign('status', $status);
		
		$this->template_lite->assign('filters', $current_settings['filters']);
		
		$this->system_messages->set_data('header', l('admin_header_orders', 'listings'));
		
		$this->Menu_model->breadcrumbs_set_parent('admin_listings_item');
		
		$this->template_lite->view('orders');
	}
	
	/**
	 * Edit order
	 * @param integer $order_id order identifier
	 */
	public function order_edit($order_id=null){
		$this->load->model('listings/models/Listings_booking_model');
		
		$this->Listings_booking_model->set_format_settings('get_listing', true);
		$order = $this->Listings_booking_model->get_period_by_id($order_id);
		$this->Listings_booking_model->set_format_settings('get_listing', false);
		
		if($order_id) show_404();
		
		if($this->input->post('btn_save')){
			$post_data = $this->input->post('period', true);
			$validate_data = $this->Listings_booking_model->validate_period($order_id, $post_data);
			if(!empty($validate_data['errors'])){
				$this->system_messages->add_message('error', $validate_data['errors']);
			}else{
				if($order_id){
					$this->Listings_booking_model->save_period($order_id, $validate_data['data']);
					$this->system_messages->add_message('success', l('success_order_updated', 'listings'));
				}else{
					$listing_id = $this->Listings_booking_model->save_period(null, $validate_data['data']);
					$this->system_messages->add_message('success', l('success_order_added', 'listings'));
				}
				
				$url = site_url().'admin/listings/order_edit/'.$order_id;
				redirect($url);
			}
		}
		
		$this->template_lite->assign('data', $order);
		
		$this->system_messages->set_data('header', l('admin_header_order_edit', 'listings'));
		
		$this->Menu_model->breadcrumbs_set_parent('admin_listings_item');
		
		$this->template_lite->view('order_edit');
	}
	
	/**
	 * Remove order
	 * @param integer $order_id order identifier
	 */
	public function order_delete($order_id){
		$this->load->model('listings/models/Listings_booking_model');
		
		if(!empty($order_id)){
			$this->Listings_booking_model->delete_period($order_id);
			$this->system_messages->add_message('success', l('success_order_deleted', 'listings'));
		}
		
		redirect(site_url().'admin/listings/orders');
	}
	
	/**
	 * Remove order by ajax
	 * @param integer $order_id order identifier
	 */
	public function ajax_order_delete($order_id){
		$return = array('error'=>'', 'success'=>'');
		
		$this->load->model('listings/models/Listings_booking_model');
		
		if(!empty($order_id)){
			$this->Listings_booking_model->delete_period($order_id);
			$return['success'] = l('success_order_deleted', 'listings');
		}else{
			$return['error'] = 'No order';
		}
		
		echo json_encode($return);
	}
	
	/**
	 * Approve order
	 * @param integer $order_id order identifier
	 */
	public function order_approve($order_id){
		$this->load->model('listings/models/Listings_booking_model');
		
		if(!empty($order_id)){
			$this->Listings_booking_model->approve_period($order_id);
			$this->system_messages->add_message('success', l('success_order_approved', 'listings'));
		}
		
		redirect(site_url().'admin/listings/orders');
	}
	
	/**
	 * Decline order
	 * @param integer $order_id order identifier
	 */
	public function order_decline($order_id){
		$this->load->model('listings/models/Listings_booking_model');

		if(!empty($order_id)){
			$this->Listings_booking_model->decline_period($order_id);
			$this->system_messages->add_message('success', l('success_order_declined', 'listings'));
		}
		
		redirect(site_url().'admin/listings/orders');
	}
	
	/**
	 * Return order form by ajax
	 * @param integer $listing_id listing identifier
	 * @param integer $order_id order identifier
	 */
	public function ajax_order_form($listing_id, $order_id=null){
		$this->load->model('Listings_model');
		
		$this->Listings_model->set_format_settings('use_format', false);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);

		if(!$listing) return '';

		$this->template_lite->assign('listing', $listing);

		if($order_id){
			$this->load->model('listings/models/Listings_booking_model');
			$this->Listings_booking_model->set_format_settings('use_format', false);
			$order = $this->Listings_booking_model->get_period_by_id($order_id);
			$this->Listings_booking_model->set_format_settings('use_format', true);
			$this->template_lite->assign('order', $order);
		}
		
		$this->config->load('date_formats', TRUE);
		$page_data['date_format'] = $this->config->item('st_format_date_literal', 'date_formats');
		$page_data['date_time_format'] = $this->config->item('st_format_date_time_literal', 'date_formats');
		$page_data['datepicker_date_format'] = $this->config->item('ui_format_date_literal', 'date_formats');
		$page_data['datepicker_alt_format'] = $this->config->item('ui_format_date_numeric', 'date_formats');
		$this->template_lite->assign('page_data', $page_data);

		$this->template_lite->assign('rand', rand(100000, 999999));

		$this->template_lite->view('ajax_order_form');
	}
	
	/**
	 * Save order by ajax
	 * @param integer $listing_id listing identifier
	 * @param integer $order_id order identifier
	 */
	public function ajax_save_order($listing_id, $order_id=null){
		$result = array('error'=>array(), 'success'=>array(), 'id'=>0, 'data'=>'');
		
		$this->load->model('Listings_model');
		
		$this->Listings_model->set_format_settings('use_format', false);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);

		$this->load->model('listings/models/Listings_booking_model');
		
		$post_period = $this->input->post('period', true);
		$post_period['id_listing'] = $listing_id;
		$post_period['id_user'] = $listing['id_user'];
		$post_period['status'] = 'book';
		
		switch($listing['price_period']){
			case 1:
				$date_start_alt = $this->input->post('date_start_alt', true);
				if($date_start_alt) $post_period['date_start'] = $date_start_alt;
						
				$date_end_alt = $this->input->post('date_end_alt', true);
				if($date_end_alt) $post_period['date_end'] = $date_end_alt;
			break;
			case 2: 
				$date_start_month = $this->input->post('date_start_month', true);
				$date_start_year = $this->input->post('date_start_year', true);
				$post_period['date_start'] = $date_start_year.'-'.$date_start_month.'-01';
								
				$date_end_month = $this->input->post('date_end_month', true);
				$date_end_year = $this->input->post('date_end_year', true);
				$post_period['date_end'] = $date_end_year.'-'.$date_end_month.'-01';
			break;
		}
					
		$validate_data = $this->Listings_booking_model->validate_period($order_id, $post_period, true);
		if(!empty($validate_data['errors'])){
			$result['error'] = implode('<br>', $validate_data['errors']);
		}else{
			$guests = $validate_data['data']['guests'] ? $validate_data['data']['guests'] : 1;
			
			$validate_data['data']['price'] = $this->Listings_booking_model->get_period_price($listing, $validate_data['data']['date_start'], $validate_data['data']['date_end'], $guests);
			
			if($order_id){
				$result['success'] = l('success_order_updated', 'listings');
			}else{
				$result['success'] = l('success_order_added', 'listings');
			}
			
			$order_id = $this->Listings_booking_model->save_period($order_id, $validate_data['data']);
			
			$result['id'] = $validate_data['data']['id'] = $order_id;
			
			$this->template_lite->assign('listing', $listing);
			
			$period = $this->Listings_booking_model->format_period($validate_data['data']);
			$this->template_lite->assign('period', $period);
	
			$this->config->load('date_formats', TRUE);
			$page_data['date_format'] = $this->config->item('st_format_date_literal', 'date_formats');
			$page_data['date_time_format'] = $this->config->item('st_format_date_time_literal', 'date_formats');
			$page_data['datepicker_date_format'] = $this->config->item('ui_format_date_literal', 'date_formats');
			$page_data['datepicker_alt_format'] = $this->config->item('ui_format_date_numeric', 'date_formats');
			$this->template_lite->assign('page_data', $page_data);
			
			$this->template_lite->assign('edit', 1);
		
			$result['data'] = $this->template_lite->fetch('calendar_period', 'admin', 'listings');
		}
		
		echo json_encode($result);
		exit;
	}
	
	/**
	 * Return period form by ajax
	 * @param integer $listing_id listing identifier
	 * @param integer $period_id period identifier
	 */
	public function ajax_period_form($listing_id, $period_id=null){
		$this->load->model('Listings_model');
		
		$this->Listings_model->set_format_settings(array('get_user'=>false, 'get_location'=>false));
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings(array('get_user'=>true, 'get_location'=>true));

		if(!$listing) return '';

		$this->template_lite->assign('data', $listing);
		
		if($period_id){
			$this->load->model('listings/models/Listings_booking_model');
			$period = $this->Listings_booking_model->get_period_by_id($period_id);
			$this->template_lite->assign('period', $period);
		}
		
		if($this->pg_module->is_module_installed('payments')){
			$this->load->model('payments/models/Payment_currency_model');
			$currencies = $this->Payment_currency_model->get_currency_list();
			$this->template_lite->assign('currencies', $currencies);
					
			if(isset($listing['gid_currency']) && !empty($listing['gid_currency'])){
				$current_price_currency = $this->Payment_currency_model->get_currency_by_gid($listing['gid_currency']);
				$this->template_lite->assign('current_price_currency', $current_price_currency);
			}
		}
			
		$this->config->load('date_formats', TRUE);
		$page_data['date_format'] = $this->config->item('st_format_date_literal', 'date_formats');
		$page_data['date_time_format'] = $this->config->item('st_format_date_time_literal', 'date_formats');
		$page_data['datepicker_date_format'] = $this->config->item('ui_format_date_literal', 'date_formats');
		$page_data['datepicker_alt_format'] = $this->config->item('ui_format_date_numeric', 'date_formats');
		$this->template_lite->assign('page_data', $page_data);
		
		$this->template_lite->assign('rand', rand(100000, 999999));
		
		$this->template_lite->view('ajax_period_form');
	}
	
	/**
	 * Save period by ajax
	 * @param integer $listing_id listing identifier
	 * @param integer $period_id order identifier
	 */
	public function ajax_save_period($listing_id, $period_id=null){
		$result = array('error'=>array(), 'success'=>array(), 'id'=>0, 'data'=>'');
		
		$this->load->model('Listings_model');
		
		$this->Listings_model->set_format_settings('use_format', false);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);

		$this->load->model('listings/models/Listings_booking_model');
		
		$post_period = $this->input->post('period', true);
		$post_period['id_listing'] = $listing_id;
		$post_period['id_user'] = $listing['id_user'];

		switch($listing['price_period']){
			case 1:
				$date_start_alt = $this->input->post('date_start_alt', true);
				if($date_start_alt) $post_period['date_start'] = $date_start_alt;
						
				$date_end_alt = $this->input->post('date_end_alt', true);
				if($date_end_alt) $post_period['date_end'] = $date_end_alt;
			break;
			case 2: 
				$date_start_month = $this->input->post('date_start_month', true);
				$date_start_year = $this->input->post('date_start_year', true);
				$post_period['date_start'] = $date_start_year.'-'.$date_start_month.'-01';
								
				$date_end_month = $this->input->post('date_end_month', true);
				$date_end_year = $this->input->post('date_end_year', true);
				$post_period['date_end'] = $date_end_year.'-'.$date_end_month.'-01';
			break;
		}
					
		$validate_data = $this->Listings_booking_model->validate_period($period_id, $post_period);
		if(!empty($validate_data['errors'])){
			$result['error'] = implode('<br>', $validate_data['errors']);
		}else{
			if($period_id){
				$result['success'] = l('success_period_updated', 'listings');
			}else{
				$result['success'] = l('success_period_added', 'listings');
			}
			
			$period_id = $this->Listings_booking_model->save_period($period_id, $validate_data['data']);
			
			$result['id'] = $validate_data['data']['id'] = $period_id;
			
			$this->template_lite->assign('listing', $listing);
			
			$period = $this->Listings_booking_model->format_period($validate_data['data']);
			$this->template_lite->assign('period', $period);
			
			$this->config->load('date_formats', TRUE);
			$page_data['date_format'] = $this->config->item('st_format_date_literal', 'date_formats');
			$page_data['date_time_format'] = $this->config->item('st_format_date_time_literal', 'date_formats');
			$page_data['datepicker_date_format'] = $this->config->item('ui_format_date_literal', 'date_formats');
			$page_data['datepicker_alt_format'] = $this->config->item('ui_format_date_numeric', 'date_formats');
			$this->template_lite->assign('page_data', $page_data);
			
			$this->template_lite->assign('edit', 1);
		
			$result['data'] = $this->template_lite->fetch('calendar_period', 'admin', 'listings');
		}
		
		echo json_encode($result);
		exit;
	}
	
	/**
	 * Remove booking period
	 * @param integer $period_id period identifier
	 */
	public function period_delete($period_id){
		$this->load->model('listings/models/Listings_booking_model');

		$this->Listings_booking_model->set_format_settings('use_format', false);
		$period = $this->Listings_booking_model->get_period_by_id($period_id);
		$this->Listings_booking_model->set_format_settings('use_format', true);

		$this->Listings_booking_model->delete_period($period_id);
		$this->system_messages->add_message('success', l('success_period_deleted', 'listings'));
		
		redirect(site_url().'admin/listings/edit/'.$period['id_listing'].'/calendar/2');
	}
	
	/**
	 * Remove booking period by ajax
	 * @param integer $period_id period identifier
	 */
	public function ajax_period_delete($period_id){
		$return = array('error'=>'', 'success'=>'');
		
		$this->load->model('listings/models/Listings_booking_model');

		$this->Listings_booking_model->set_format_settings('use_format', false);
		$period = $this->Listings_booking_model->get_period_by_id($period_id);
		$this->Listings_booking_model->set_format_settings('use_format', true);

		$this->Listings_booking_model->delete_period($period_id);
		$return['success'] = l('success_period_deleted', 'listings');
		
		echo json_encode($return);
	}
	
	/**
	 * Return calendar
	 * @param integer $listing_id listing identifier
	 * @param integer $l number of calendars
	 * @param integer $year year
	 * @param integer $month month
	 */
	public function ajax_get_calendar($listing_id, $count, $year, $month=1){
		$this->load->model('Listings_model');
		
		$this->Listings_model->set_format_settings('get_booking', true);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('get_booking', false);
		
		$params = array(
			'listing' => $listing,
			'count' => $count,
			'year' => $year,
			'month' => $month,
			'template' => 'view',
		);
		
		$this->load->helper('listings');
		echo listings_calendar_block($params, true);
	}
	
	/**
	 * Return price for booking period
	 * @param integer $listing_id listing identifier
	 */
	public function ajax_get_booking_price($listing_id){
		$return = array('price' => 0);

		$this->Listings_model->set_format_settings('use_format', false);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);

		$date_start = $this->input->post('date_start', true);
		$date_end = $this->input->post('date_end', true);
		
		$guests = intval($this->input->post('guests', true));
		if(!$guests) $guests = 1;
		
		if(!$date_start || !$date_end){
			if($listing['price_negotiated']){
				$return['price'] = l('text_negotiated_price_rent', 'listings');
			}else{
				if($listing['price_reduced'] > 0){
					$price_value = $listing['price_reduced'];
				}else{
					$price_value = $listing['price'];	
				}
					
				$this->load->helper('start');
				$price_arr = array('value'=>$price_value, 'cur_gid'=>$listing['gid_currency']);	
				$return['price'] = l('text_price_from', 'listings').' '.currency_format_output($price_arr);
			}
		}else{
			$this->load->model('listings/models/Listings_booking_model');
			$price_value = $this->Listings_booking_model->get_period_price($listing, $date_start, $date_end, $guests);
			if($price_value){
				$this->load->helper('start');
				$price_arr = array('value'=>$price_value, 'cur_gid'=>$listing['gid_currency']);	
				$return['price'] = currency_format_output($price_arr);
			}else{
				$return['price'] = l('text_negotiated_price_rent', 'listings');
			}
		}
		
		echo json_encode($return);
	}
	
	#MOD FOR KPR BUTTON#
	public function kpr_button(){
		$this->load->model('Listings_model');		
		$kpr_button = $this->Listings_model->get_kpr_button();
		$this->template_lite->assign("kpr_button", $kpr_button);
		$this->Menu_model->set_menu_active_item('admin_listings_menu', 'listings_menu_item');
		$this->template_lite->view('kpr_button_list', 'admin', 'listings');
	}	
	
	public function activate_kpr_button($button_id, $status=0){
		$this->load->model('Listings_model');
		if(!empty($button_id)){
			if($status){
				$this->Listings_model->activate_kpr_button($button_id, $status);
				$this->system_messages->add_message('success', l('success_activate_listing', 'listings'));
			}else{
				$this->Listings_model->activate_kpr_button($button_id, $status);
				$this->system_messages->add_message('success', l('success_deactivate_listing', 'listings'));
			}
		}
		$url = site_url().'admin/listings/kpr_button/';
		redirect($url);
	}
	public function add_kpr_button(){
		$this->load->model('Listings_model');		
		$bank = $this->Listings_model->list_bank();
		$this->template_lite->assign("bank", $bank);
		$this->template_lite->view("add_kpr_button");
	}
	public function edit_kpr_button($button_id){
		$this->load->model('Listings_model');	
		$button = $this->Listings_model->get_button_by_id($button_id);	
		$button = $button[0];
		$bank = $this->Listings_model->list_bank();
		$this->template_lite->assign("bank", $bank);
		$this->template_lite->assign("button", $button);		
		$this->template_lite->view("add_kpr_button");
	}	
	public function update_kpr_button($button_id){
		$this->load->model('Listings_model');
		if(!is_dir('./uploads/kpr_button')){
			mkdir('./uploads/kpr_button');
		}		
		$config['upload_path'] = './uploads/kpr_button/';
		$config['allowed_types'] = 'gif|jpg|jpeg|png';
		$config['max_size']	= '10240';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';
		$this->load->library('upload', $config);
		$bank = $this->Listings_model->list_bank();	
		$button = $this->Listings_model->get_button_by_id($button_id);	
		$button = $button[0];
		$bank_id = $this->input->post('bank_id');
		$status = $this->input->post('status');
		$this->template_lite->assign("bank", $bank);
		$this->template_lite->assign("button", $button);				
		$this->template_lite->assign("bank_id", $bank_id);
		$this->template_lite->assign("status", $status);					
		if($bank_id=="empty"){
			$this->system_messages->add_message('error', l('error_no_bank_chosen', 'listings'));
			$this->template_lite->view("add_kpr_button");
		}
		else
		{
			if (!empty($_FILES['userfile']['name'])){
				if ( ! $this->upload->do_upload())
				{
					$data['error'] = $this->upload->display_errors();
					$this->system_messages->add_message('error', l('error_fail_button_image_upload', 'listings'));
					$this->template_lite->view("add_kpr_button");
				}
				else
				{
					$data = array('upload_data' => $this->upload->data());
					$upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
					$file_name = $upload_data['file_name'];
					$data = array(
						'bank_id'=>$bank_id,
						'status'=>$status,
						'image'=>$file_name
					);
					unlink($config['upload_path'].$button['image']);
					$this->Listings_model->update_kpr_button($button_id, $data);
					$this->system_messages->add_message('success', l('success_update_kpr_button', 'listings'));					
					$this->kpr_button();
				}
			}
			else
			{
				$data = array(
					'bank_id'=>$bank_id,
					'status'=>$status
				);
				$this->Listings_model->update_kpr_button($button_id, $data);
				$this->system_messages->add_message('success', l('success_update_kpr_button', 'listings'));					
				$this->kpr_button();
			}
		}
		
	}
	public function save_kpr_button(){
		$this->load->model('Listings_model');
		if(!is_dir('./uploads/kpr_button')){
			mkdir('./uploads/kpr_button');
		}
		$config['upload_path'] = './uploads/kpr_button/';
		#echo $config['upload_path'];exit;
		$config['allowed_types'] = 'gif|jpg|jpeg|png';
		$config['max_size']	= '10240';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';
		$this->load->library('upload', $config);
		$bank = $this->Listings_model->list_bank();								
		#$exist = $this->Listings_model->get_button_by_bank_id($bank_id);		
		$bank_id = $this->input->post('bank_id');
		$status = $this->input->post('status');
		$this->template_lite->assign("bank", $bank);
		$this->template_lite->assign("bank_id", $bank_id);
		$this->template_lite->assign("status", $status);					
		if($bank_id=="empty"){
			$this->system_messages->add_message('error', l('error_no_bank_chosen', 'listings'));
			$this->template_lite->view("add_kpr_button");
		}
		/*
		else if($exist!=NULL){
				$this->system_messages->add_message('error', l('error_kpr_button_exist', 'listings'));
				$this->template_lite->view("add_kpr_button");
		}
		*/
		else
		{
			if (!empty($_FILES['userfile']['name'])){
				if ( ! $this->upload->do_upload())
				{
					echo $data['error'] = $this->upload->display_errors();
					$this->system_messages->add_message('error', l('error_fail_button_image_upload', 'listings'));
					$this->template_lite->view("add_kpr_button");
				}
				else
				{
					$data = array('upload_data' => $this->upload->data());
					$upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
					$file_name = $upload_data['file_name'];
					$data = array(
						'bank_id'=>$bank_id,
						'status'=>$status,
						'image'=>$file_name
					);
					$this->Listings_model->add_kpr_button($data);
					$this->system_messages->add_message('success', l('success_add_kpr_button', 'listings'));					
					$this->kpr_button();
				}
			}
			else
			{
				$this->system_messages->add_message('error', l('error_no_button_image', 'listings'));
				$this->template_lite->view("add_kpr_button");
			}
		}
	}

	public function delete_kpr_button($button_id=null){
		if(!empty($button_id)){
			$this->load->model('Listings_model');
			$button_details = $this->Listings_model->get_button_by_id($button_id);
			$image_path = './uploads/kpr_button/'.$button_details[0]['image'];
			unlink($image_path);			
			$this->Listings_model->delete_kpr_button($button_id);
			$this->system_messages->add_message('success', l('success_delete_kpr_button', 'listings'));
		}		
		redirect(site_url().'admin/listings/kpr_button');
	}

	
	#END OF MOD FOR KPR BUTTON#
}
