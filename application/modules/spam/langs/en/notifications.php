<?php

$install_lang["notification_spam_object"] = "Spam alert";
$install_lang["tpl_spam_object_content"] = "Hello admin,\n\nThere is a new spam alert:\n[reason]\n[message]\n\nBest regards,\n[name_from]";
$install_lang["tpl_spam_object_subject"] = "[domain] | [type] (ID=[object_id]) is marked as spam";

