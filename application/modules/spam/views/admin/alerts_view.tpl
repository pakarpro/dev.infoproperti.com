{include file="header.tpl"}
<div class="edit-form n150">
	<div class="row header">{l i='admin_header_alerts_show' gid='spam'}</div>
	<div class="row">
		<div class="h">{l i='field_alert_content' gid='spam'}: </div>
		<div class="v">{$data.content}</div>
	</div>
	{if $data.type.form_type eq 'select_text'}
	<div class="row">
		<div class="h">{l i='field_spam_reason' gid='spam'}: </div>
		<div class="v">{$data.reason}</div>
	</div>
	<div class="row">
		<div class="h">{l i='field_spam_message' gid='spam'}: </div>
		<div class="v">{$data.message}</div>
	</div>
	{/if}
	<div class="row">
		<div class="h">{l i='field_alert_date_add' gid='spam'}: </div>
		<div class="v">{$data.date_add|date_format:$date_format}</div>
	</div>
</div>
{if $data.link}<div class="btn"><div class="l"><a href="{$data.link}">{l i='btn_content_edit' gid='spam'}</a></div></div>{/if}
<a class="cancel" href="{$site_url}admin/spam/index">{l i='btn_cancel' gid='start'}</a>
<div class="clr"></div>
<script>{literal}
$(function(){
	$("div.row:odd").addClass("zebra");
});
{/literal}</script>
{include file="footer.tpl"}
