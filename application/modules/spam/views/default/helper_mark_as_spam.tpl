{js module=spam file='spam.js'}
{if $template eq 'link'}
{if !$is_send}<a href="{$site_url}spam/mark_as_spam" data-id="{$object_id}" data-type="{$type.gid}" id="mark-as-span-{$rand}" class="btn-link link-r-margin">{l i='btn_mark_as_spam' gid='spam' type='button'}</a>{/if}
{else}
<a {if !$is_send}href="{$site_url}spam/mark_as_spam"{/if} data-id="{$object_id}" data-type="{$type.gid}" id="mark-as-span-{$rand}" class="btn-link link-r-margin" title="{l i='btn_mark_as_spam' gid='spam' type='button'}"><ins class="with-icon {if $is_send}g{/if} i-spam"></ins></a>
{/if}
{if !$is_send}
<script>{literal}
$(function(){
	{/literal}{if $is_guest}{literal}
	$('#mark-as-span-{/literal}{$rand}{literal}').bind('click', function(){
		$('html, body').animate({
			scrollTop: $("#ajax_login_link").offset().top
		}, 2000);
		$("#ajax_login_link").click();
		return false;
	});
	{/literal}{else:}{literal}
	new Spam({
		siteUrl: '{/literal}{$site_url}{literal}', 
		use_form: {/literal}{if $type.form_type!='checkbox'}true{else}false{/if}{literal},
		{/literal}{if $is_spam_owner}isOwner: true,{/if}{literal}
		mark_as_spam_btn: '{/literal}mark-as-span-{$rand}{literal}',
	});		
	{/literal}{/if}{literal}
});
{/literal}</script>
{/if}
