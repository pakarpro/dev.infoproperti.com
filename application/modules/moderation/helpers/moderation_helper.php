<?php

/**
* Moderation helpers
* 
* @package PG_RealEstate
* @subpackage application
* @category	helpers
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Mikhail Makeev <mmakeev@pilotgroup.net>
* @version $Revision: 68 $ $Date: 2010-01-11 16:02:23 +0300 (Пн, 11 янв 2010) $ $Author: irina $
**/
if(!function_exists("admin_home_moderation_block")){
	/**
	 * Return homepage statistics
	 */
	function admin_home_moderation_block(){
		$CI = & get_instance();

		$auth_type = $CI->session->userdata("auth_type");
		if($auth_type != "admin") return "";

		$user_type = $CI->session->userdata("user_type");

		$show = true;

		$stat_moderation = array(
			"index_method" => true
		);

		if($user_type == "moderator"){
			$show = false;
			$CI->load->model("Ausers_model");
			$methods_moderation = $CI->Ausers_model->get_module_methods("moderation");
			if((is_array($methods_moderation) && !in_array("index", $methods_moderation))){
				$show = true;
			}else{
				$permission_data = $CI->session->userdata("permission_data");
				if((isset($permission_data["moderation"]["index"]) && $permission_data["moderation"]["index"] == 1)){
					$show = true;
					$stat_moderation["index_method"] = (bool)$permission_data["moderation"]["index"];
				}
			}
		}

		if(!$show) return "";
		
		$CI->load->model("Moderation_model");
		$CI->load->model("moderation/models/Moderation_type_model");
		
		$moderation_types = $CI->Moderation_type_model->get_types();
		foreach($moderation_types as $i=>$moderation_type){
			if($moderation_type['name'] != 'user_logo' && $moderation_type['name'] != 'upload_gallery'){
				unset($moderation_types[$i]);
				continue;
			}
			$stat_moderation['type_'.$moderation_type['name']] = $CI->Moderation_model->get_moderation_list_count($moderation_type['name']);
		}
		$stat_moderation['types'] = $moderation_types;

		$CI->template_lite->assign("stat_moderation", $stat_moderation);
		return $CI->template_lite->fetch("helper_admin_home_block", "admin", "moderation");
	}
}
