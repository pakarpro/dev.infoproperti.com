function moderation(optionArr){
    this.properties = {
	siteUrl: '',
	urlFewApproveItem: 'admin/moderation/few_approve'
    }

    var _self = this;


    this.Init = function(options){
	_self.properties = $.extend(_self.properties, options);
	_self.init_checkbox();
    }

    this.init_checkbox = function(){
	$('#check_all_items').bind('click' , function(){
	    var is_checked = $(this).is(':checked');
	    if(is_checked){
		$('.item_ckeckbox').attr('checked' , 'checked');
	    }
	    else{
		$('.item_ckeckbox').removeAttr('checked');
	    }
	});
	
	$('#approve_few_items').bind('click' , function(){
	    var items = new Array();
	    
	    $('.item_ckeckbox').each(function(){
		if($(this).is(':checked')) items.push($(this).val());
	    });
	    
	    if(items.length != 0){
		//alert(_self.properties.siteUrl + _self.properties.urlFewApproveItem + '/' + items.join('-'));
		document.location.href = _self.properties.siteUrl + _self.properties.urlFewApproveItem + '/' + items.join('-');
	    }
	    
	});
    }

    _self.Init(optionArr);
}
