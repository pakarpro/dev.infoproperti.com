<?php
/**
* Moderation install model
*
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/


class Moderation_install_model extends Model
{
	/**
	 * Link to Code Igniter object
	 * @var object
	 */
	var $CI;
	
	/**
	 * Menu configuration
	 */
	private $menu = array(
		'admin_menu' => array(
			'action' => 'none',
			'items' => array(
				'main_items' => array(
					'action' => 'none',
					'items' => array(
						'moderation-items' => array(
							'action'=>'none',
							'items' => array(
								'objects_list_item' => array('action' => 'create', 'link' => 'admin/moderation/index', 'status' => 1, 'sorter' => 2),
								'moder_settings_item_2' => array('action' => 'create', 'link' => 'admin/moderation/settings', 'status'=>1, 'sorter' => 3),
								'badwords_item' => array('action' => 'create', 'link' => 'admin/moderation/badwords', 'status' => 1, 'sorter' => 4)
							)
						)
					)
				)
			)
		),
		'admin_moderation_menu' => array(
			'action' => 'create',
			'name' => 'Admin mode - Moderation',
			'items' => array(
				'object_list_item' => array('action' => 'create', 'link' => 'admin/moderation/index', 'status' => 1),
				'moder_setting_item' => array('action' => 'create', 'link' => 'admin/moderation/settings', 'status' => 1),
				'badwords_item' => array('action' => 'create', 'link' => 'admin/moderation/badwords', 'status' => 1)
			)
		)
	);
	
	/**
	 * Indicators configuration
	 */
	private $menu_indicators = array(
		array(
			'gid'				=> 'new_item_for_moderation',
			'delete_by_cron'	=> false,
			'auth_type'			=> 'admin',
		),
	);
	
	/**
	 * Ausers configuration
	 */
	private $ausers_methods = array(
		array('module' => 'moderation', 'method' => 'index', 'is_default' => 1),
		array('module' => 'moderation', 'method' => 'settings', 'is_default' => 0),
		array('module' => 'moderation', 'method' => 'badwords', 'is_default' => 0),
	);
	
	/**
	 * Notifications configuration
	 * @var array
	 */
	private $notifications = array(
		"templates" => array(
			array("gid"=>"auser_need_moderate", "name"=>"New object awaiting moderation", "vars"=>array("type"), "content_type"=>"text"),
		),
		"notifications" => array(
			array("gid"=>"auser_need_moderate", "template"=>"auser_need_moderate", "send_type"=>"simple"),
		),
	);

	/**
	 * Constructor
	 *
	 * @return Install object
	 */
	function Moderation_install_model()
	{
		parent::Model();
		$this->CI = & get_instance();
		//// load langs
		$this->CI->load->model('Install_model');
	}

	public function install_menu() {
		$this->CI->load->helper('menu');
		foreach($this->menu as $gid => $menu_data){
			$this->menu[$gid]['id'] = linked_install_set_menu($gid, $menu_data["action"], $menu_data["name"]);
			linked_install_process_menu_items($this->menu, 'create', $gid, 0, $this->menu[$gid]["items"]);
		}
		if(!empty($this->menu_indicators)) {
			$this->CI->load->model('menu/models/Indicators_model');
			foreach($this->menu_indicators as $data){
				$this->CI->Indicators_model->save_type(null, $data);
			}
		}
	}

	public function install_menu_lang_update($langs_ids = null) {
		if(empty($langs_ids)) return false;
		$langs_file = $this->CI->Install_model->language_file_read('moderation', 'menu', $langs_ids);

		if(!$langs_file) { log_message('info', 'Empty menu langs data'); return false; }

		$this->CI->load->helper('menu');

		foreach($this->menu as $gid => $menu_data){
			linked_install_process_menu_items($this->menu, 'update', $gid, 0, $this->menu[$gid]["items"], $gid, $langs_file);
		}
		// Indicators
		if(!empty($this->menu_indicators)) {
			$langs_file = $this->CI->Install_model->language_file_read('moderation', 'indicators', $langs_ids);
			if(!$langs_file) {
				log_message('info', '(resumes) Empty indicators langs data');
				return false;
			} else {
				$this->CI->load->model('menu/models/Indicators_model');
				$this->CI->Indicators_model->update_langs($this->menu_indicators, $langs_file, $langs_ids);
			}
		}
		return true;
	}

	public function install_menu_lang_export($langs_ids) {
		if(empty($langs_ids)) return false;
		$this->CI->load->helper('menu');

		$return = array();
		foreach($this->menu as $gid => $menu_data){
			$temp = linked_install_process_menu_items($this->menu, 'export', $gid, 0, $this->menu[$gid]["items"], $gid, $langs_ids);
			$return = array_merge($return, $temp);
		}
		if(!empty($this->menu_indicators)) {
			$this->CI->load->model('menu/models/Indicators_model');
			$indicators_langs = $this->CI->Indicators_model->export_langs($this->menu_indicators, $langs_ids);
		}
		return array("menu"=>$return, "indicators"=>$indicators_langs);
	}

	public function deinstall_menu() {
		$this->CI->load->helper('menu');
		foreach($this->menu as $gid => $menu_data){
			if($menu_data['action'] == 'create'){
				linked_install_set_menu($gid, 'delete');
			}else{
				linked_install_delete_menu_items($gid, $this->menu[$gid]['items']);
			}
		}
		if(!empty($this->menu_indicators)) {
			$this->CI->load->model('menu/models/Indicators_model');
			foreach($this->menu_indicators as $data){
				$this->CI->Indicators_model->delete_type($data['gid']);
			}
		}
	}

	/**
	 * Ausers module methods
	 */
	public function install_ausers() {
		// install ausers permissions
		$this->CI->load->model('Ausers_model');

		foreach($this->ausers_methods as $method){
			$this->CI->Ausers_model->save_method(null, $method);
		}
	}

	public function install_ausers_lang_update($langs_ids = null) {
		$langs_file = $this->CI->Install_model->language_file_read('moderation', 'ausers', $langs_ids);

		// install ausers permissions
		$this->CI->load->model('Ausers_model');
		$params['where']['module'] = 'moderation';
		$methods = $this->CI->Ausers_model->get_methods_lang_export($params);

		foreach($methods as $method){
			if(!empty($langs_file[$method['method']])){
				$this->CI->Ausers_model->save_method($method['id'], array(), $langs_file[$method['method']]);
			}
		}
	}

	public function install_ausers_lang_export($langs_ids) {
		$this->CI->load->model('Ausers_model');
		$params['where']['module'] = 'moderation';
		$methods =  $this->CI->Ausers_model->get_methods_lang_export($params, $langs_ids);
		foreach($methods as $method){
			$return[$method['method']] = $method['langs'];
		}
		return array('ausers' => $return);
	}

	public function deinstall_ausers() {
		// delete moderation methods in ausers
		$this->CI->load->model('Ausers_model');
		$params['where']['module'] = 'moderation';
		$this->CI->Ausers_model->delete_methods($params);
	}
	
	/**
	 * Install notifications
	 */
	public function install_notifications(){
		// add notification
		$this->CI->load->model("Notifications_model");
		$this->CI->load->model("notifications/models/Templates_model");

		$templates_ids = array();

		foreach((array)$this->notifications["templates"] as $template_data){
			if(is_array($template_data["vars"])) $template_data["vars"] = implode(", ", $template_data["vars"]);			
			$validate_data = $this->CI->Templates_model->validate_template(null, $template_data);
			if(!empty($validate_data["errors"])) continue;
			$templates_ids[$template_data['gid']] = $this->CI->Templates_model->save_template(null, $validate_data["data"]);			
		}
		
		foreach((array)$this->notifications["notifications"] as $notification_data){
			if(!isset($templates_ids[$notification_data["template"]])){
				 $template = $this->CI->Templates_model->get_template_by_gid($notification_data["template"]);
				 $templates_ids[$notification_data["template"]] = $template["id"];
			}
			$notification_data["id_template_default"] = $templates_ids[$notification_data["template"]];
			$validate_data = $this->CI->Notifications_model->validate_notification(null, $notification_data);
			if(!empty($validate_data["errors"])) continue;
			$this->CI->Notifications_model->save_notification(null, $validate_data["data"], $lang_data);
		}
	}
	
	/**
	 * Import notifiactions languages
	 * @param array $langs_ids
	 */
	public function install_notifications_lang_update($langs_ids=null){
		if(empty($langs_ids)) return false;
		$this->CI->load->model("Notifications_model");
		
		$langs_file = $this->CI->Install_model->language_file_read("moderation", "notifications", $langs_ids);
		if(!$langs_file){log_message("info", "Empty notifications langs data");return false;}
		
		$this->CI->Notifications_model->update_langs($this->notifications, $langs_file, $langs_ids);
		return true;
	}
	
	/**
	 * Export notifications languages
	 * @param array $langs_ids
	 */
	public function install_notifications_lang_export($langs_ids=null){
		$this->CI->load->model("Notifications_model");
		$langs = $this->CI->Notifications_model->export_langs($this->notifications, $langs_ids);
		return array("notifications" => $langs);
	}
	
	/**
	 * Unistall notifacations
	 */
	public function deinstall_notifications(){
		$this->CI->load->model("Notifications_model");
		$this->CI->load->model("notifications/models/Templates_model");
		
		foreach((array)$this->notifications["notifications"] as $notification_data){
			$this->CI->Notifications_model->delete_notification_by_gid($notification_data["gid"]);
		}
		
		foreach((array)$this->notifications["templates"] as $template_data){
			$this->CI->Templates_model->delete_template_by_gid($template_data["gid"]);
		}
	}

	function _arbitrary_installing(){
		if($this->pg_module->is_module_installed('ausers')){
			$this->CI->load->model("Ausers_model");
			$users = $this->CI->Ausers_model->get_users_list(null, 1, null, array('where'=>array('user_type'=>'admin')));
			if(!empty($users)){ 
				$this->CI->pg_module->set_module_config("moderation", 'admin_moderation_emails', $users[0]["email"]);
			}
		}
	}

	function _arbitrary_deinstalling(){
	}
}
