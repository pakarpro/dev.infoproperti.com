{include file="header.tpl"}
{helper func_name=get_admin_level1_menu helper_name=menu func_param='admin_moderation_menu'}
<div class="actions">&nbsp;</div>

<form method="post" action="" name="moder_sattings_save">
	<div class="edit-form n150">
		{foreach item=item key=key from=$moder_types}
		<div class="row">
			<div class="h">{l i='mtype_'+$item.name gid='moderation'}:</div>
			<div class="v">
				<input type="hidden" name="type_id[]" value="{$item.id}">
				{if $item.mtype >= 0}
				<input type="radio" name="mtype[{$item.id}]" value="2" id="mtype_{$item.id}_2"{if $item.mtype eq '2'}checked{/if}><label for="mtype_{$item.id}_2">{l i='mtype_2' gid='moderation'}</label><br>
				<input type="radio" name="mtype[{$item.id}]" value="1" id="mtype_{$item.id}_1"{if $item.mtype eq '1'}checked{/if}><label for="mtype_{$item.id}_1">{l i='mtype_1' gid='moderation'}</label><br>
				<input type="radio" name="mtype[{$item.id}]" value="0" id="mtype_{$item.id}_0"{if $item.mtype eq '0'}checked{/if}><label for="mtype_{$item.id}_0">{l i='mtype_0' gid='moderation'}</label><br>
				{else}
				<input type="hidden" value="mtype[{$item.id}]" value="{$item.mtype}">
				{/if}
				{if $item.check_badwords ne '2'}<input type="checkbox" name="check_badwords[{$item.id}]" value="1" {if $item.check_badwords eq '1'}checked{/if} id="chbw_{$item.id}"><label for="chbw_{$item.id}">{l i='check_badwords' gid='moderation'}</label>{/if}
			</div>
		</div>
		{foreachelse}
		<div class="row content">{l i='no_types' gid='moderation'}</div>
		{/foreach}
		<div class="row">
			<div class="h"><label for="moderation_send_mail">{l i='field_moderation_send_mail' gid='moderation'}</label>: </div>
			<div class="v">
				<input type="hidden" name="moderation_send_mail" value="0"> 
				<input type="checkbox" name="moderation_send_mail" value="1" id="moderation_send_mail" {if $data.moderation_send_mail}checked="checked"{/if}> 
				&nbsp;&nbsp;
				{l i='field_admin_moderation_emails' gid='moderation'}
				<input type="text" name="admin_moderation_emails" value="{$data.admin_moderation_emails|escape}" id="admin_moderation_emails" {if !$data.moderation_send_mail}disabled{/if}> 
			</div>
		</div>
	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="{l i='btn_save' gid='start' type='button'}"></div></div>
</form>
<script>{literal}
$(function(){
	$("div.row:not(.hide):even").addClass("zebra");
	$('#moderation_send_mail').bind('change', function(){
		if(this.checked){
			$('#admin_moderation_emails').removeAttr('disabled');
		}else{
			$('#admin_moderation_emails').attr('disabled', 'disabled');
		}
	});
});
{/literal}</script>


{include file="footer.tpl"}
