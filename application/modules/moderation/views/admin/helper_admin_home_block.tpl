	<table cellspacing="0" cellpadding="0" class="data" width="100%">
	<tr>
		<th class="first" colspan="2">{l i='stat_header_moderation' gid='moderation'}</th>
	</tr>
	{if $stat_moderation.index_method}
	{foreach item=item from=$stat_moderation.types}
	{assign var='type_gid' value='type_'+$item.name}
	{counter print=false assign=counter}
	<tr {if $counter is div by 2}class="zebra"{/if}>
		<td class="first"><a href="{$site_url}admin/moderation/index/">{l i='mtype_'$item.name gid='moderation'}</a></td>
		<td class="w50 center"><a href="{$site_url}admin/moderation/index/">{$stat_moderation[$type_gid]}</a></td>
	</tr>
	{/foreach}
	{/if}
	</table>

