<?php

$install_lang["add_badword_hint"] = "одно слово или несколько слов, разделенных пробелом";
$install_lang["admin_header_moderation"] = "Модерация";
$install_lang["admin_header_moderation_badwords_managment"] = "Модерация | Запрещенные слова";
$install_lang["admin_header_moderation_managment"] = "Модерация | Объекты";
$install_lang["admin_header_moderation_settings_managment"] = "Модерация | Настройки";
$install_lang["all_objects"] = "Все";
$install_lang["approve_object"] = "Одобрить";
$install_lang["badwords_in_base_exists"] = "Объект уже существует:";
$install_lang["check_badwords"] = "Проверить запрещенные слова";
$install_lang["decline_object"] = "Отклонить";
$install_lang["delete_object"] = "Удалить объект";
$install_lang["delete_word"] = "Удалить";
$install_lang["edit_object"] = "Редактировать объект";
$install_lang["error_empty_email"] = "Укажите email";
$install_lang["error_invalid_email"] = "Неверный email";
$install_lang["field_admin_moderation_emails"] = "Адреса электронной почты";
$install_lang["field_date_add"] = "Дата добавления";
$install_lang["field_moderation_send_mail"] = "Отправить уведомление по почте";
$install_lang["header_add_badword"] = "Добавить запрещенные слова";
$install_lang["header_badword_found"] = "Найдены запрещенные слова";
$install_lang["header_badwords_base"] = "Запрещенные слова";
$install_lang["header_check_text"] = "Проверка текста на наличие запрещенных слов";
$install_lang["moder_object"] = "Подробно";
$install_lang["moder_object_type"] = "Тип содержимого";
$install_lang["mtype_0"] = "<b>Отключить</b> модерацию";
$install_lang["mtype_1"] = "<b>Пост-модерация</b> (объекты будут видны на сайте, пока вы их не отклоните)";
$install_lang["mtype_2"] = "<b>Пре-модерация</b> (объекты станут видны на сайте только после того, как вы их одобрите)";
$install_lang["no_objects"] = "Нет объектов для проверки";
$install_lang["no_types"] = "Отсутствуют типы объектов для проверки";
$install_lang["note_delete_badword"] = "Вы уверены, что хотите удалить это запрещенное слово?";
$install_lang["note_delete_object"] = "Вы уверены, что хотите удалить этот объект?";
$install_lang["stat_header_moderation"] = "Загрузки в ожидании проверки";
$install_lang["success_settings_saved"] = "Настройки успешно сохранены";
$install_lang["view_object"] = "Просмотр объекта";

