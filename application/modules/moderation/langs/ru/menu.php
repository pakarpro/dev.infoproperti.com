<?php

$install_lang["admin_menu_main_items_moderation-items_badwords_item"] = "Запрещенные слова";
$install_lang["admin_menu_main_items_moderation-items_badwords_item_tooltip"] = "База запрещенных слов";
$install_lang["admin_menu_main_items_moderation-items_moder_settings_item_2"] = "Настройки модерации";
$install_lang["admin_menu_main_items_moderation-items_moder_settings_item_2_tooltip"] = "Настройка модерации загрузок, проверка на запрещенные слова";
$install_lang["admin_menu_main_items_moderation-items_objects_list_item"] = "Загрузки";
$install_lang["admin_menu_main_items_moderation-items_objects_list_item_tooltip"] = "Аватары пользователей и фотографии из объявлений в ожидании проверки";
$install_lang["admin_moderation_menu_badwords_item"] = "Запрещенные слова";
$install_lang["admin_moderation_menu_badwords_item_tooltip"] = "";
$install_lang["admin_moderation_menu_moder_settings_item"] = "Настройки модерации";
$install_lang["admin_moderation_menu_moder_settings_item_tooltip"] = "";
$install_lang["admin_moderation_menu_moder_setting_item"] = "Настройки модерации";
$install_lang["admin_moderation_menu_moder_setting_item_tooltip"] = "";
$install_lang["admin_moderation_menu_object_list_item"] = "Загрузки";
$install_lang["admin_moderation_menu_object_list_item_tooltip"] = "";

