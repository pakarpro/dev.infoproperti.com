<?php
$module['module'] = 'moderation';
$module['install_name'] = 'Moderation';
$module['install_descr'] = 'Moderation of different content types';
$module['version'] = '1.02';
$module['files'] = array(
	array('file', 'read', "application/modules/moderation/controllers/admin_moderation.php"),
	array('file', 'read', "application/modules/moderation/helpers/moderation_helper.php"),
	array('file', 'read', "application/modules/moderation/install/module.php"),
	array('file', 'read', "application/modules/moderation/install/permissions.php"),
	array('file', 'read', "application/modules/moderation/install/settings.php"),
	array('file', 'read', "application/modules/moderation/install/structure_deinstall.sql"),
	array('file', 'read', "application/modules/moderation/install/structure_install.sql"),
	array("file", "read", "application/modules/moderation/js/moderation.js"),
	array('file', 'read', "application/modules/moderation/models/moderation_badwords_model.php"),
	array('file', 'read', "application/modules/moderation/models/moderation_install_model.php"),
	array('file', 'read', "application/modules/moderation/models/moderation_model.php"),
	array('file', 'read', "application/modules/moderation/models/moderation_type_model.php"),
	array('file', 'read', "application/modules/moderation/views/admin/css/style-ltr.css"),
	array('file', 'read', "application/modules/moderation/views/admin/css/style-rtl.css"),
	array('file', 'read', "application/modules/moderation/views/admin/admin_moder_badwords.tpl"),
	array('file', 'read', "application/modules/moderation/views/admin/admin_moder_list.tpl"),
	array('file', 'read', "application/modules/moderation/views/admin/admin_moder_settings.tpl"),
	array('file', 'read', "application/modules/moderation/views/admin/helper_admin_home_block.tpl"),
	array('file', 'read', "application/modules/moderation/views/admin/link_settings.tpl"),
	array('dir', 'read', 'application/modules/moderation/langs'),
);
$module['dependencies'] = array(
	'start' 		=> array('version'=>'1.01'),
	'menu' 			=> array('version'=>'1.01'),
	'notifications' => array('version'=>'1.03'),
);
$module['linked_modules'] = array(
	'install' => array(
		'ausers'		=> 'install_ausers',
		'menu'			=> 'install_menu',
		'notifications'	=> 'install_notifications',
	),
	'deinstall' => array(
		'ausers'		=> 'deinstall_ausers',
		'menu'			=> 'deinstall_menu',
		'notification'	=> 'deinstall_notifications',
	)
);
