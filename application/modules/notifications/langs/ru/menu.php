<?php

$install_lang["admin_menu_other_items_newsletters-items_notifications_menu_item"] = "Письменные уведомления";
$install_lang["admin_menu_other_items_newsletters-items_notifications_menu_item_tooltip"] = "SMTP сервер, тексты уведомлений, подписка";
$install_lang["admin_notifications_menu_nf_items"] = "Уведомления";
$install_lang["admin_notifications_menu_nf_items_tooltip"] = "";
$install_lang["admin_notifications_menu_nf_pool_item"] = "Очередь на отправку";
$install_lang["admin_notifications_menu_nf_pool_item_tooltip"] = "";
$install_lang["admin_notifications_menu_nf_settings_item"] = "Настройки";
$install_lang["admin_notifications_menu_nf_settings_item_tooltip"] = "";
$install_lang["admin_notifications_menu_nf_templates_item"] = "Шаблоны";
$install_lang["admin_notifications_menu_nf_templates_item_tooltip"] = "";

