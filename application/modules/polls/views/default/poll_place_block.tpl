{if !$hide_poll}
<div id="poll_block_{$poll_data.id}" class="poll_block">
	<link rel="stylesheet" type="text/css" href="{$site_root}application/modules/polls/views/default/css/style-{$_LANG.rtl}.css" />
	{js module=polls file='polls.js'}
	<h2>{if $language}{$poll_data.question[$language]}{else}{$poll_data.question[$cur_lang]}{/if}</h2>
	<div class="poll">
		{$poll_block}
	</div>
	<script type="text/javascript">
		{literal}
			$(function() {
				new Polls({
					siteUrl: '{/literal}{$site_url}{literal}',
					poll_id: '{/literal}{$poll_data.id}{literal}'
				});
			});
		{/literal}
	</script>
</div>
{/if}
