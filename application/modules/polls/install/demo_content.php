<?php
return array(
	array(
		'question' => array(
			'en' => 'How did you find us?',
			'ru' => 'How did you find us?'),
		'answers' => array(
			1 => array(
				'en' => 'Search engine (Google, Yahoo, Ask, etc.)',
				'ru' => 'Search engine (Google, Yahoo, Ask, etc.)',
				'color' => 'F27000'
			),
			2 => array(
				'en' => 'Advertisement in mass media (TV, radio, newspaper, etc.)',
				'ru' => 'Advertisement in mass media (TV, radio, newspaper, etc.)',
				'color' => 'FDC800'
			),
			3 => array(
				'en' => 'I saw it on a poster/flyer/brochure',
				'ru' => 'I saw it on a poster/flyer/brochure',
				'color' => '29B43D'
			),
			4 => array(
				'en' => 'Word of mouth (heard from a spouse, colleague, friend, etc.)',
				'ru' => 'Word of mouth (heard from a spouse, colleague, friend, etc.)',
				'color' => 'DE5575'
			),
			5 => array(
				'en' => 'Other (please specify)',
				'ru' => 'Other (please specify)',
				'color' => 'E29FC4'
			),
		),
		'status' => 1,
		'poll_type' => 0,
		'answer_type' => 1,
		'sorter' => 0,
		'show_results' => 1,
		'date_start' => date('Y-m-d h:i:s'),
		'use_comments' => 1,
		'language' => 0,
		'use_expiration' => 0,
		'date_end' => '1970-01-01 04:00:00',
	),
	array(
		'question' => array(
			'en' => 'Based on your experience, how would you rate the quality of this website?',
			'ru' => 'Based on your experience, how would you rate the quality of this website?'),
		'answers' => array(
			1 => array(
				'en' => 'Very high quality',
				'ru' => 'Very high quality',
				'color' => 'F27000'
			),
			2 => array(
				'en' => 'High quality',
				'ru' => 'High quality',
				'color' => 'FDC800'
			),
			3 => array(
				'en' => 'Average',
				'ru' => 'Average',
				'color' => '29B43D'
			),
			4 => array(
				'en' => 'Below average',
				'ru' => 'Below average',
				'color' => 'DE5575'
			),
			5 => array(
				'en' => 'Unacceptable',
				'ru' => 'Unacceptable',
				'color' => 'E29FC4'
			),
		),
		'status' => 1,
		'poll_type' => 0,
		'answer_type' => 0,
		'sorter' => 0,
		'show_results' => 1,
		'date_start' => date('Y-m-d h:i:s'),
		'use_comments' => 0,
		'language' => 0,
		'use_expiration' => 0,
		'date_end' => '1970-01-01 04:00:00',
	),
	array(
		'question' => array(
			'en' => 'For your primary residence, do you…',
			'ru' => 'For your primary residence, do you…'),
		'answers' => array(
			1 => array(
				'en' => 'Own',
				'ru' => 'Own',
				'color' => 'F27000'
			),
			2 => array(
				'en' => 'Rent',
				'ru' => 'Rent',
				'color' => 'FDC800'
			),
			3 => array(
				'en' => 'Other (please specify)',
				'ru' => 'Other (please specify)',
				'color' => '29B43D'
			),
		),
		'status' => 1,
		'poll_type' => 0,
		'answer_type' => 0,
		'sorter' => 0,
		'show_results' => 1,
		'date_start' => date('Y-m-d h:i:s'),
		'use_comments' => 1,
		'language' => 0,
		'use_expiration' => 0,
		'date_end' => '1970-01-01 04:00:00',
	),
	array(
		'question' => array(
			'en' => 'What could we do in the future to improve the service?',
			'ru' => 'What could we do in the future to improve the service?'),
		'answers' => array(
			1 => array(
				'en' => 'All is good',
				'ru' => 'All is good',
				'color' => 'F27000'
			),
			2 => array(
				'en' => 'Live chat support',
				'ru' => 'Live chat support',
				'color' => 'FDC800'
			),
			3 => array(
				'en' => 'Help articles',
				'ru' => 'Help articles',
				'color' => '29B43D'
			),
			4 => array(
				'en' => 'Other (please specify)',
				'ru' => 'Other (please specify)',
				'color' => 'DE5575'
			),
		),
		'status' => 1,
		'poll_type' => 0,
		'answer_type' => 0,
		'sorter' => 0,
		'show_results' => 1,
		'date_start' => date('Y-m-d h:i:s'),
		'use_comments' => 1,
		'language' => 0,
		'use_expiration' => 0,
		'date_end' => '1970-01-01 04:00:00',
	),
);
