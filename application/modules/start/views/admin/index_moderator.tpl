{include file="header.tpl"}
<p>{l i='moderator_comment' gid='admin_home_page'}</p>
<h2>{l i='header_quick_start' gid='admin_home_page'}</h2>

{helper func_name=admin_home_users_block module=users}

<div class="right-side">
	{helper func_name=admin_home_moderation_block module=moderation}
	{helper func_name=admin_home_spam_block module=spam}
	{helper func_name=admin_home_banners_block module=banners}
	{helper func_name=admin_home_contact_block module=contact}	
</div>

<div class="left-side">
	{helper func_name=admin_home_listings_block module=listings}
	{helper func_name=admin_home_reviews_block module=reviews}
	{helper func_name=admin_home_payments_block module=payments}
</div>

<div class="clr"></div>

{include file="footer.tpl"}
