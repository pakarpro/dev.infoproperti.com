{include file="header.tpl"}
<table cellspacing="0" cellpadding="0" class="data" width="100%">
<tr>
	<th class="first" colspan=3>{l i='header_get_started' gid='admin_home_page'}</th>
</tr>
<tr>
	<td class="first">
		{module_tpl module=ausers tpl=link_edit theme_type=admin}<br/>
		{module_tpl module=notifications tpl=link_settings theme_type=admin}<br/>
		{module_tpl module=seo tpl=link_default_listing theme_type=admin}<br/>
		{module_tpl module=cronjob tpl=link_index theme_type=admin}<br/>
	</td>
	<td>
		{module_tpl module=countries tpl=link_index theme_type=admin}<br/>
		{module_tpl module=video_uploads tpl=link_system_settings theme_type=admin}<br/>
		{module_tpl module=uploads tpl=link_watermarks theme_type=admin}<br/>
		{module_tpl module=file_uploads tpl=link_index theme_type=admin}<br/>
	</td>
	<td>
		{module_tpl module=moderation tpl=link_settings theme_type=admin}<br/>
		{module_tpl module=dynamic_blocks tpl=link_index theme_type=admin}<br/>
		{module_tpl module=languages tpl=link_pages theme_type=admin}<br/>
		{module_tpl module=content tpl=link_index theme_type=admin}<br/>
	</td>
</tr>
</table>
<h2>{l i='header_quick_start' gid='admin_home_page'}</h2>

{helper func_name=admin_home_users_block module=users}
{helper func_name=admin_home_stat module=users cache='true'}

<div class="right-side">
	{helper func_name=admin_home_moderation_block module=moderation}
	{helper func_name=admin_home_spam_block module=spam}
	{helper func_name=admin_home_banners_block module=banners}
	{helper func_name=admin_home_contact_block module=contact}	
</div>

<div class="left-side">
	{helper func_name=admin_home_listings_block module=listings}
	{helper func_name=admin_home_reviews_block module=reviews}
	{helper func_name=admin_home_payments_block module=payments}
</div>

<div class="clr"></div>

{include file="footer.tpl"}
