{include file="header.tpl"}
<div class="menu-level2">
	<ul>
		<li{if $section eq 'overview'} class="active"{/if}><div class="l"><a href="{$site_url}admin/start/settings/overview">{l i='sett_overview_item' gid='start'}</a></div></li>
		<li{if $section eq 'numerics'} class="active"{/if}><div class="l"><a href="{$site_url}admin/start/settings/numerics">{l i='sett_numerics_item' gid='start'}</a></div></li>
		<li{if $section eq 'formats'} class="active"{/if}><div class="l"><a href="{$site_url}admin/start/settings/formats">{l i='sett_formats_item' gid='start'}</a></div></li>
		<li{if $section eq 'widgets'} class="active"{/if}><div class="l"><a href="{$site_url}admin/start/settings/widgets">{l i='sett_widgets_item' gid='start'}</a></div></li>
		{foreach item=item key=key from=$other_settings}
		<li{if $key eq $section} class="active"{/if}><div class="l"><a href="{$site_url}admin/start/settings/{$key}">{l i='sett_'+$key+'_item' gid='start'}</a></div></li>
		{/foreach}
	</ul>
	&nbsp;
</div>
<div class="actions">&nbsp;</div>
{if $section eq 'overview'}
<div class="right-side">
	<table cellspacing="0" cellpadding="0" class="data" width="100%">
	{foreach item=module_data key=module from=$settings_data.other}
	<tr>
		<th class="first" colspan=2>{$module_data.name}</th>
	</tr>
	{foreach item=item key=key from=$module_data.vars}
	<tr{if $key is div by 2} class="zebra"{/if}>
		<td class="first">{$item.field_name}</td>
		<td>
			{if $item.type eq 'checkbox'}
				{if $item.value}
					{l i='option_checkbox_yes' gid='start'}
				{else}
					{l i='option_checkbox_no' gid='start'}
				{/if}
			{else}
				{$item.value}
			{/if}
		</td>
	</tr>
	{/foreach}
	{/foreach}
	</table>
</div>

<div class="left-side">
	{counter print=false assign=counter start=0}
	<table cellspacing="0" cellpadding="0" class="data" width="100%">
	<tr>
		<th class="first" colspan=2>{l i='sett_numerics_item' gid='start'}</th>
	</tr>
	{foreach item=module_data key=module from=$settings_data.numerics}
	{foreach item=item key=key from=$module_data.vars}
	{counter print=false assign=counter}
	<tr{if $counter is div by 2} class="zebra"{/if}>
		<td class="first">{$item.field_name}</td>
		<td class="w100">
			{if $item.type eq 'checkbox'}
				{if $item.value}
					{l i='option_checkbox_yes' gid='start'}
				{else}
					{l i='option_checkbox_no' gid='start'}
				{/if}
			{else}
				{$item.value}
			{/if}
		</td>
	</tr>
	{/foreach}
	{/foreach}
	<tr>
		<th class="first" colspan=2>{l i='sett_formats_item' gid='start'}</th>
	</tr>
	{foreach item=module_data key=module from=$settings_data.formats}
	{foreach item=item key=key from=$module_data.vars}
	{counter print=false assign=counter}
	<tr{if $counter is div by 2} class="zebra"{/if}>
		<td class="first">{$item.field_name}</td>
		<td class="w100">
			{if $item.type eq 'checkbox'}
				{if $item.value}
					{l i='option_checkbox_yes' gid='start'}
				{else}
					{l i='option_checkbox_no' gid='start'}
				{/if}
			{else}
				{$item.value}
			{/if}
		</td>
	</tr>
	{/foreach}
	{/foreach}
	<tr>
		<th class="first" colspan=2>{l i='sett_widgets_item' gid='start'}</th>
	</tr>
	{foreach item=module_data key=module from=$settings_data.widgets}	
	{foreach item=item key=key from=$module_data.vars}
	{counter print=false assign=counter}
	<tr{if $counter is div by 2} class="zebra"{/if}>
		<td class="first">{$item.field_name}</td>
		<td class="w100">
			{if $item.type eq 'checkbox'}
				{if $item.value}
					{l i='option_checkbox_yes' gid='start'}
				{else}
					{l i='option_checkbox_no' gid='start'}
				{/if}
			{else}
				{$item.value}
			{/if}
		</td>
	</tr>
	{/foreach}
	{/foreach}
	</table>
</div>
<div class="clr"><a class="cancel" href="{$site_url}admin/start/menu/system-items">{l i='btn_cancel' gid='start'}</a></div>


{elseif $section eq 'numerics' || $section eq 'formats' || $section eq 'widgets'}
<form method="post" action="{$data.action}" name="save_form" enctype="multipart/form-data">
	<div class="edit-form n250">
		{counter print=false assign=counter start=0}
		{foreach item=module_data key=module from=$settings_data}
		{*<div class="row header">{$module_data.name}</div>*}
		{foreach item=item key=key from=$module_data.vars}
		{counter print=false assign=counter}
		<div class="row{if $counter is div by 2} zebra{/if}">
			<div class="h">{$item.field_name}:</div>
			<div class="v">
				{if $item.type eq 'checkbox'}
				<input type="hidden" name="settings[{$module}][{$item.field}]" value="0">
				<input type="checkbox" name="settings[{$module}][{$item.field}]" value="1" {if $item.value}checked{/if} class="short">
				{elseif $item.type eq 'int'}
				<input type="text" name="settings[{$module}][{$item.field}]" value="{$item.value|escape}" class="short">
				{elseif $item.type eq 'text'}
				<input type="text" name="settings[{$module}][{$item.field}]" value="{$item.value|escape}" class="middle">
				{elseif $item.type eq 'textarea'}
				<textarea name="settings[{$module}][{$item.field}]" rows="5" cols="80">{$item.value|escape}"</textarea>
				{else}
				<input type="text" name="settings[{$module}][{$item.field}]" value="{$item.value|escape}" class="short">
				{/if}
			</div>
		</div>
		{/foreach}
		{/foreach}
	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="{l i='btn_save' gid='start' type='button'}"></div></div>
	<a class="cancel" href="{$site_url}admin/start/menu/system-items">{l i='btn_cancel' gid='start'}</a>
</form>
{else}
<form method="post" action="{$data.action}" name="save_form" enctype="multipart/form-data">
	<div class="edit-form n250">
		<div class="row header">{$settings_data.name}</div>
		{foreach item=item key=key from=$settings_data.vars}
		{if !($section eq 'listings' and $item.field eq 'admin_moderation_emails')}
		<div class="row{if $key is div by 2} zebra{/if}">
		{if $section eq 'countries'}
			<div class="h">{$item.field_name}:</div>
			<div class="v"><input type="text" name="settings[{$item.field}]" value="{$item.value|escape}"><br><i>{l i=$item.field+'_settings_descr' gid='countries'}</i></div>
		{else}
			<div class="h">{$item.field_name}:</div>
			<div class="v">
				{if $item.type eq 'checkbox'}
				<input type="hidden" name="settings[{$item.field}]" value="0">
				<input type="checkbox" name="settings[{$item.field}]" value="1" {if $item.value}checked{/if} class="short" {if $section eq 'listings' and $item.field eq 'moderation_send_mail'}id="moderation_send_mail"{/if}>
				{if $section eq 'listings' and $item.field eq 'moderation_send_mail'}
				&nbsp;&nbsp;
				{$admin_moderation_emails.field_name}
				<input type="text" name="settings[admin_moderation_emails]" value="{$admin_moderation_emails.value|escape}" id="admin_moderation_emails" {if !$item.value}disabled{/if}> 
				<script>{literal}
					$(function(){
						$("div.row:not(.hide):even").addClass("zebra");
						$('#moderation_send_mail').bind('change', function(){
							if(this.checked){
								$('#admin_moderation_emails').removeAttr('disabled');
							}else{
								$('#admin_moderation_emails').attr('disabled', 'disabled');
							}
						});
					});
				{/literal}</script>
				{/if}
				{elseif $item.type eq 'int'}
				<input type="text" name="settings[{$item.field}]" value="{$item.value|escape}" class="short">
				{elseif $item.type eq 'text'}
				<input type="text" name="settings[{$item.field}]" value="{$item.value|escape}" class="middle">
				{elseif $item.type eq 'textarea'}
				<textarea name="settings[{$item.field}]" rows="5" cols="80">{$item.value|escape}"</textarea>
				{else}
				<input type="text" name="settings[{$item.field}]" value="{$item.value|escape}" class="short">
				{/if}
			</div>
		{/if}
		</div>
		{/if}
		{/foreach}
	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="{l i='btn_save' gid='start' type='button'}"></div></div>
	<a class="cancel" href="{$site_url}admin/start/menu/system-items">{l i='btn_cancel' gid='start'}</a>
</form>
{/if}
<div class="clr"></div>

{include file="footer.tpl"}
