<h2>{l i='header_my_statistics' gid='start'}</h2>
<div class="container statistic">
	{block name=listings_count module=listings}
	{block name=contacts_count module=mailbox}
	{block name=new_messages module=mailbox template=homepage}
	{block name=user_account_block module=users_payments}
	{block name=listings_saved_block module=listings}
	{block name=listings_searches_block module=listings}
</div>

<h2>{l i='header_activity' gid='start'}</h2>
<div class="container statistic">
	<div>
		<table cellspacing="0" cellpadding="0" class="data" width="100%">
		<tr>
			<th class="first">{l i='stat_header_name' gid='start'}</th>
			<th class="w100">{l i='stat_header_month' gid='start'}</th>
			<th class="w100">{l i='stat_header_week' gid='start'}</th>
		</tr>
		{block name=users_visitors_block module=users}
		{block name=listings_visitors_block module=listings}
		{block name=reviews_visitors_block module=reviews type='users_object'}
		{block name=reviews_visitors_block module=reviews type='listings_object'}
		</table>
	</div>
	
	<div>
		<table cellspacing="0" cellpadding="0" class="data" width="100%">
		<tr>
			<th class="first">{l i='stat_header_name' gid='start'}</th>
			<th class="w100">{l i='stat_header_month' gid='start'}</th>
			<th class="w100">{l i='stat_header_week' gid='start'}</th>
		</tr>
		{block name=users_visits_block module=users}
		{block name=listings_visits_block module=listings}
		{block name=reviews_visits_block module=reviews type='users_object'}
		{block name=reviews_visits_block module=reviews type='listings_object'}
		</table>
	</div>
</div>

<h2>{l i='header_actions' gid='start'}</h2>
<div class="container statistic">
	{module_tpl module=users tpl=link_profile}
	{module_tpl module=users module_1=users_payments tpl=link_add_funds}
	{module_tpl module=users tpl=link_change_password}
	{module_tpl module=contact_us tpl=link_contact_admin}
</div>
