{if $page_type eq 'cute'}
<div class="pages">
	<div class="inside">		
		<ins class="prev{if $page_data.prev_page eq $page_data.cur_page} gray{/if}"><a href="{if $page_data.base_url|strpos:'[page]'}{$page_data.base_url|replace:'[page]':$page_data.prev_page}{else}{$page_data.base_url}{$page_data.prev_page}{/if}" data-page="{$page_data.prev_page}">&nbsp;</a></ins>
		<ins class="current">{$page_data.cur_page} {l i='text_of' gid='start'} {$page_data.total_pages}</ins>
		<ins class="next{if $page_data.next_page eq $page_data.cur_page} gray{/if}"><a href="{if $page_data.base_url|strpos:'[page]'}{$page_data.base_url|replace:'[page]':$page_data.next_page}{else}{$page_data.base_url}{$page_data.next_page}{/if}" data-page="{$page_data.next_page}">&nbsp;</a></ins>
	</div>
</div>
{elseif $page_type eq 'full'}
{if $page_data.nav}
<div class="line pages">
	<div class="inside">{$page_data.nav}</div>
</div>
{/if}
{/if}
