{include file="header.tpl"}
{include file="left_panel.tpl" module="start"}

<div class="rc">
	<div class="content-block">
		<h1>{l i='header_homepage' gid='start'}</h1>
		<div class="statistic_block">
			{include file=`$user_type`"_stat_block.tpl" module="start" theme="default"}
		</div>
	</div>
</div>
{include file="footer.tpl"}

