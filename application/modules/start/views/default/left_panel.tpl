<div class="lc">
	<div class="inside account_menu">
	{if $user_session_data.user_type}
		{menu gid=`$user_session_data.user_type`'_account_menu' template='account_menu'}
    <!-- mod for news privilege -->
        {if $news_privilege}    
   		<ul>
        	<li><a href="{$base_url}news-add_news_edit">Add News</a></li>
        </ul>
        {/if}        
    <!-- end of mod -->            
	{else}
		{menu gid='user_footer_menu' template='account_menu'}
	{/if}
	
	{*block name=listings_search_block module=listings*}
	{*block name=users_search_block module=users*}
	{*block name=show_poll_place_block module=polls one_poll_place=0*}

	{helper func_name=show_profile_info module=users}
	{helper func_name=show_banner_place module=banners func_param='big-left-banner'}
	{helper func_name=show_banner_place module=banners func_param='left-banner'}
	</div>
</div>
