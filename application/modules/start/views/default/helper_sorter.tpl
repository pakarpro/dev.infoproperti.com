{l i='sort_by' gid=$sort_module}: 
<select id="sorter-select-{$sort_rand}">
{foreach item=item key=key from=$sort_links}
<option value="{$key}"{if $key eq $sort_order} selected{/if}>{$item}</option>
{/foreach}
</select>
<input type="button" id="sorter-dir-{$sort_rand}" name="sorter_btn" value="" class="sorter-btn i-sorter with-icon-small{if $sort_direction eq 'ASC'} up{else} down{/if}">
