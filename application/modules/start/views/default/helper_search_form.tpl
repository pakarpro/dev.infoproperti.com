{js module=start file='search.js'}
{js module=start file='selectbox.js'}
{js module=start file='checkbox.js'}
{js module=start file='bookmark.js'}
<script>var forms_{$form_settings.rand} = {literal}{}{/literal};</script>
<div class="search-box"><!-- Search home -->
    {if $form_settings.show_tabs}
	<div class="tabs tab-size-15" id="search-bookmark-{$form_settings.rand}">
	    <ul>
		{foreach item=item key=key from=$form_settings.tabs}
		<li{if $form_settings.object eq $key} class="active"{/if} id="{$key}-form-tab-{$form_settings.rand}"><a href="#">{$item}</a></li>
		<script>forms_{$form_settings.rand}['{$key}']='{$modules[$key]}';</script>
		{/foreach}
	    </ul>
	</div>
    {/if}
    <div id="search-form-block-{$form_settings.rand}">{$form_block}</div>
</div>
{if $form_block}
    {literal}
    <script type="text/javascript">
    $(function(){
	new searchBookmark({'bmID': 'search-bookmark-{/literal}{$form_settings.rand}{literal}'});
	new search({
	    siteUrl: '{/literal}{$site_url}{literal}', 
	    currentForm: '{/literal}{$form_settings.object}{literal}', 
	    currentFormType: '{/literal}{$form_settings.type}{literal}',
	    forms: forms_{/literal}{$form_settings.rand}{literal},
	    rand: '{/literal}{$form_settings.rand}{literal}'
	});
    });
    </script>
    {/literal}
{/if}
