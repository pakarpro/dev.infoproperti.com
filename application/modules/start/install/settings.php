<?php

$install_settings["admin_items_per_page"] = "50";
$install_settings["index_items_per_page"] = "30";
$install_settings["default_date_format"] = '%d %m %Y';
$install_settings["product_version"] = '2013.04';
$install_settings["product_version_update"]	= '2013.04';
$install_settings["product_version_last_update"] = '';
$install_settings["product_order_key"] = '';
$install_settings["use_phone_format"] = 1;
$install_settings["phone_format"] = '(999) 999-9999';
