<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('demo_panel')){
	function demo_panel($type="user"){
		if(DEMO_MODE){
			$CI = &get_instance();
			$html = $CI->template_lite->fetch("demo_panel", $type);
			echo $html;
		}
	}
}

if ( ! function_exists('product_version')){
	function product_version(){
		if(INSTALL_MODULE_DONE){
			$CI = &get_instance();

			if($CI->pg_module->is_module_installed('start')){
				$current_version = $CI->pg_module->get_module_config('start', 'product_version');
				$cache_new_version = $CI->pg_module->get_module_config('start', 'product_version_update');

				$old_array = explode('_', $current_version);
				$old_array = array_slice($old_array, 0, 2);
				$formated_current_version = implode('.', $old_array);

				$last_update = $CI->pg_module->get_module_config('start', 'product_version_last_update');
				$uts_last_update = strtotime($last_update);

				if((!$cache_new_version || $cache_new_version==$current_version)	&& (!$last_update || (time() - $uts_last_update > 24*60*60)) ){
					$new_version = get_new_version($current_version);
					if($new_version){
						$cache_new_version = $new_version;
					}
				}

				if($cache_new_version && compare_versions($current_version, $cache_new_version)){
					$new_array = explode('_', $cache_new_version);
					$new_array = array_slice($new_array, 0, 2);
					$formated_new_version = implode('.', $new_array);
				}else{
					$formated_new_version = '';
				}

				$html = l('system_version', 'start').": ".$formated_current_version." ";
				if($formated_new_version){
					$html .= str_replace('[version]', $formated_new_version, l('system_version_available', 'start'));
				}
				echo $html;
			}
		}
	}
}

if (!function_exists('get_new_version')) {
	function get_new_version($current_version) {
		$CI = &get_instance();
		$CI->load->library('Snoopy');
		$CI->snoopy->fetch('http://www.pilotgroup.net/feeder/realestate/version.php');
		if ($CI->snoopy->status == '200' && strlen($CI->snoopy->results) <= 12) {
			$new_version = $CI->snoopy->results;
			$CI->pg_module->set_module_config('start', 'product_version_update', $new_version);
		}else{
			$new_version = "";
		}
		$CI->pg_module->set_module_config('start', 'product_version_last_update', date('Y-m-d H:i:s'));
		return $new_version;
	}
}
if (!function_exists('compare_versions')) {
	function compare_versions($current_version, $new_version) {
		$months = array(
			'JAN' => 1, 'FEB' => 2, 'MAR' => 3,
			'APR' => 4, 'MAY' => 5, 'JUN' => 6,
			'JUL' => 7, 'AUG' => 8, 'SEP' => 9,
			'OCT' => 10, 'NOV' => 11, 'DEC' => 12
		);
		$old_array = explode('_', $current_version);
		$new_array = explode('_', $new_version);
		if (
				count($new_array) >= 2 &&
				count($old_array) >= 2 &&
				(
					$new_array[1] > $old_array[1] ||
					(
						$new_array[1] == $old_array[1] &&
						isset($months[$new_array[0]]) &&
						isset($months[$old_array[0]]) &&
						$months[$new_array[0]] > $months[$old_array[0]]
					)
				)
		) {
			return true;
		}else{
			return false;
		}

	}
}

if(!function_exists("main_search_form")){
	/**
	 * Search form for index page
	 * @param string $object object name
	 * @param string $type render block type
	 * @param boolean $show_data filled selected data
	 */
	function main_search_form($object="", $type="line", $show_data=false){
		$CI = &get_instance();

		$user_id = $CI->session->userdata("user_id");
		$auth_type = $CI->session->userdata("auth_type");
		$user_type = $CI->session->userdata("user_type");
	
		$tabs = array();
		$show_tabs = false;
		$modules = array();
	
		if($type != "line"){
			if($CI->pg_module->is_module_installed("listings")){
				$more_tabs = listings_main_search_tabs();
				if(!empty($more_tabs)){
					$tabs = array_merge($tabs, $more_tabs);
					$modules = array_keys($more_tabs);
					$modules = array_combine($modules, array_fill(0, count($modules), 'listings'));
				}
			}
			
			if($type == 'slider'){
				$count = 1;
			}
			
			$count = count($tabs);
			if($count){
				$show_tabs = true;
				if($type == 'slider' && $count == 1) $show_tabs = false;
				if(!$object || !in_array($object, $tabs)) $object = key($tabs);
			}
		}
		
		$page_data = array(
			"show_tabs" => $show_tabs,
			"object" => $object,
			"type" => $type,
			'tabs' => $tabs,
			'rand' => rand(100000, 999999),
		);			
		$CI->template_lite->assign("form_settings", $page_data);

		if($CI->pg_module->is_module_installed("listings")){
			$CI->load->model('Listings_model');
			$operation_types = $CI->Listings_model->get_operation_types(true);
			if($type == 'line' || in_array($object, $operation_types)){
				$CI->load->helper("listings");
				$form_block = listings_main_search_form($type, $show_data);
				
			}
		}
		if($form_block) $CI->template_lite->assign("form_block", $form_block);
		$CI->template_lite->assign('modules', $modules);
		
		return $CI->template_lite->fetch("helper_search_form", "user", "start");
	}
}


if ( ! function_exists('selectbox')){
	function selectbox($params){
		$CI = &get_instance();
		foreach($params as $key=>$value){
			$CI->template_lite->assign("sb_".$key, $value);
		}
		if(!isset($params['subvalue'])) $CI->template_lite->assign('sb_subvalue', '');
		return $CI->template_lite->fetch("helper_selectbox", 'user', 'start');
	}
}

if ( ! function_exists('checkbox')){
	function checkbox($params){
		
		if(empty($params['value'])) return '';
		
		$CI = &get_instance();

		$values = array();
		$selected = (!empty($params['selected']))?$params['selected']:array();
		if(!is_array($selected)){
			$selected = array($selected);
		}

		if(!is_array($params['value'])) $params['value'] = array(1=>$params['value']);

		foreach($params['value'] as $key => $value){
			$values[$key] = array('name' => $value, 'checked' => (in_array($key, $selected))?1:0);
		}

		$params['value'] = $values;
		
		$cb_count = count($params['value']);
		$CI->template_lite->assign("cb_count", $cb_count);
		
		unset($params['selected']);

		foreach($params as $key=>$value){
			$CI->template_lite->assign("cb_".$key, $value);
		}
		return $CI->template_lite->fetch("helper_checkbox", 'user', 'start');
	}
}

if ( ! function_exists('pagination')){
	function pagination($params){
		$CI = &get_instance();
		foreach($params as $key=>$value){
			$CI->template_lite->assign("page_".$key, $value);
		}
		return $CI->template_lite->fetch("helper_pagination", 'user', 'start');
	}
}

if ( ! function_exists('sorter')){
	function sorter($params){
		$CI = &get_instance();
		if(!$params["module"]) $params["module"] = "start";

		$params["rand"] = rand(0, 9999);
		foreach($params as $key=>$value){
			$CI->template_lite->assign("sort_".$key, $value);
		}
		return $CI->template_lite->fetch("helper_sorter", 'user', 'start');
	}
}

if ( ! function_exists('available_brousers')){
	function available_brousers(){
		$CI = &get_instance();
		$html = $CI->template_lite->fetch("available_brousers", 'user');
		echo $html;
	}
}

if ( ! function_exists('lang_inline_editor')){
	function lang_inline_editor($params){
		$CI = &get_instance();
		if(isset($params['textarea']) && $params['textarea']) $CI->template_lite->assign('textarea', true);
		$CI->template_lite->assign('rand', rand(100000, 999999));
		return $CI->template_lite->fetch("helper_lang_inline_editor_js", null, 'start');
	}
}

if ( ! function_exists('get_phone_format')){
	function get_phone_format($type='js'){
		$CI = &get_instance();
		$use_phone_format = (bool)$CI->pg_module->get_module_config("start", "use_phone_format");
		if($use_phone_format){
			$format = $CI->pg_module->get_module_config("start", "phone_format");
			if($type == 'regexp'){
				$format = prepare_regexp_phone_format($format);
			}
		}else{
			$format = false;
		}
		return $format;
	}
}
if ( ! function_exists('prepare_regexp_phone_format')){
	function prepare_regexp_phone_format($format){
		$format = preg_quote($format);
		for($i=20; $i>0; $i--){
			$str = str_repeat('9', $i);
			$format = str_replace($str, '[0-__NINE__]{'.$i.'}', $format);
		}
		$format = "/".str_replace('__NINE__', '9', $format)."/iU";
		return $format;
	}
}

if (!function_exists('currency_format_output')) {

	/**
	 * Returns formatted currency string
	 * Or unformatted if payments is not installed
	 *
	 * @param int $params['cur_id'] currency id
	 * @param string $params['cur_gid'] currency gid
	 * @param int $params['value'] amount
	 * @param string $params['template']&nbsp;[abbr][value|dec_part:2|dec_sep:.|gr_sep:&nbsp;]
	 * @return string
	 */
	function currency_format_output($params = array()) {
		$CI = & get_instance();
		if($CI->pg_module->is_module_installed('payments')){
			$CI->load->helper('payments');
			return currency_format($params);
		} else {
			return '<span dir="ltr">' . $params['value'] . '&nbsp;USD</span>';
		}
	}
}

if (!function_exists('currency_output')) {

	/**
	 * Returns unformatted currency string
	 * Or unformatted if payments is not installed
	 *
	 * @param int $params['cur_id'] currency id
	 * @param string $params['cur_gid'] currency gid
	 * @param int $params['value'] amount
	 * @param string $params['template']&nbsp;[abbr][value|dec_part:2|dec_sep:.|gr_sep:&nbsp;]
	 * @return string
	 */
	function currency_output($params = array()) {
		$CI = & get_instance();
		if($CI->pg_module->is_module_installed('payments')){
			$CI->load->helper('payments');
			return currency($params);
		} else {
			return '<span dir="ltr">' . $params['value'] . '&nbsp;USD</span>';
		}
	}
}

if (!function_exists('currency_format_regexp_output')) {

	/**
	 * Returns formatted currency string
	 * Or unformatted if payments is not installed
	 *
	 * @param int $params['cur_id'] currency id
	 * @param string $params['cur_gid'] currency gid
	 * @param int $params['value'] amount
	 * @param string $params['template']&nbsp;[abbr][value|dec_part:2|dec_sep:.|gr_sep:&nbsp;]
	 * @return string
	 */
	function currency_format_regexp_output($params = array()) {
		$CI = & get_instance();
		if($CI->pg_module->is_module_installed('payments')){
			$CI->load->helper('payments');
			return currency_format_regexp($params);
		} else {
			return 'function(value){return value+\' USD\'}';
		}
	}
}
