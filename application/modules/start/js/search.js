(function() {
	var orig = $.fn.remove;
	$.fn.remove = function() {
		$(this).each(function(i, item){
			var element = $(item);
			var events = element.data('events');
		    if(events !== undefined){
				if(events){
					$.each(events, function(evName, e) {
						if(evName == 'remove'){
							element.trigger('remove');
						}
					});
				}
			}
		});
		return orig.apply(this, arguments);
	}
})();

function search(optionArr){
	this.properties = {
		siteUrl: '',
		currentForm: null,
		forms: {},
		slideSteps: 2,
		slideCurrentStep: 1,
		currentFormType: 'line',
		formUrl: 'ajax_search_form',
		searchUrl: 'ajax_search',
		countUrl: 'ajax_search_counts',
		rand: null,				
		preloadInterval: 2000,
		preloadUTS: 0,
		preloadTID: false,
		errorObj: new Errors()
	};

	var _self = this;


	this.Init = function(options){
		_self.properties = $.extend(_self.properties, options);
		_self.init_tabs();
		if(_self.properties.currentForm) _self.init_form(_self.properties.currentForm);
	},
	
	this.init_tabs = function(){
		for(var i in _self.properties.forms){
			$('#'+i+'-form-tab-'+_self.properties.rand).bind('click', function(){
				_self.properties.currentForm = this.id.replace('-form-tab-'+_self.properties.rand, '');
				_self.load_form(_self.properties.currentForm);
				return false;
			});
		}
	},
	
	this.load_form = function(form){
		var url = _self.properties.siteUrl + _self.properties.forms[form] + '/' + _self.properties.formUrl +'/' + form + '/' + _self.properties.currentFormType;
		$.ajax({
			url: url, 
			data: {rand: _self.properties.rand},
			type: 'POST',
			cache: false,
			success: function(data){
				$('#search-form-block-'+_self.properties.rand).html(data);
				_self.init_form(form);
			}
		});
		
	}

	this.init_form = function(form){
		for(var i in _self.properties.forms){
			$('#'+i+'-form-tab-'+_self.properties.rand).removeClass('active');
		}
		$('#'+form+'-form-tab-'+_self.properties.rand).addClass('active');
		
		//// form type
		_self.properties.slideSteps = 1;
		_self.properties.slideCurrentStep = 1;
		if($('#line-search-form-'+_self.properties.rand).length > 0){
			_self.properties.currentFormType = 'line';
		}
		
		if($('#slider-search-form-'+_self.properties.rand).length > 0){
			_self.properties.currentFormType = 'slider';
		}
		
		if($('#short-search-form-'+_self.properties.rand).length > 0){
			_self.properties.currentFormType = 'short';
		}
		
		if($('#full-search-form-'+_self.properties.rand).length > 0){
			_self.properties.slideSteps = 2;
			if($('#full-search-form-'+_self.properties.rand).is(':visible')){
				_self.properties.currentFormType = 'full';
				_self.properties.slideCurrentStep = 2;
			}
		}
		
		if(_self.properties.currentFormType != 'line'){
			$('#search-form-block-'+_self.properties.rand+' input[type=text]').bind('change', function(){
				_self.preload_results();
			});
			
			$('#search-form-block-'+_self.properties.rand+' input[type=hidden]').live('change', function(){
				_self.preload_results();
			});
			
			$('#search-form-block-'+_self.properties.rand+' input[type=hidden]').live('remove', function(){
				_self.preload_results();
			});
		}
		
		//// more | less link
		switch(_self.properties.currentFormType){
			case 'line':
				$('#more-options-link-'+_self.properties.rand).bind('click', function(){
					//_self.load_form(_self.properties.currentForm);
					return false;
				});
			break;
			case 'slider':
			
			break;
			default:
				$('#more-options-link-'+_self.properties.rand+', #less-options-link-'+_self.properties.rand).hide();
				$('#more-options-link-'+_self.properties.rand+', #less-options-link-'+_self.properties.rand).bind('click', function(){
					_self.slide_form(); return false;
				});
				if(_self.properties.slideSteps > 1 && _self.properties.slideCurrentStep == _self.properties.slideSteps){
					$('#less-options-link-'+_self.properties.rand).show();
				}else if(_self.properties.slideSteps > 1 ){
					$('#more-options-link-'+_self.properties.rand).show();
				}
			break;
		}
		
		//// submit button
		$('#main_search_button_'+_self.properties.rand).unbind().bind('click', function(){
			if($('#main_' + form + '_results').length > 0){
				//// ajax results loading
				_self.load_results();
				return false;
			}
		});
	}
	
	this.slide_form = function(){
		if(_self.properties.slideSteps > 1 && _self.properties.slideCurrentStep == _self.properties.slideSteps ){
			$('#less-options-link-'+_self.properties.rand).hide();
			$('#more-options-link-'+_self.properties.rand).show();
			$('#advanced-search-form-'+_self.properties.rand+', #full-search-form-'+_self.properties.rand).fadeOut();
			_self.properties.slideCurrentStep = 1;			
		}else if(_self.properties.slideSteps > 1 ){
			if(_self.properties.slideCurrentStep == 1 ){
				$('#full-search-form-'+_self.properties.rand).fadeIn();
			}else if(_self.properties.slideCurrentStep == 2){
				$('#advanced-search-form-'+_self.properties.rand).fadeIn();
			}
			_self.properties.slideCurrentStep++;
			if(_self.properties.slideCurrentStep == _self.properties.slideSteps){
				$('#more-options-link-'+_self.properties.rand).hide();
				$('#less-options-link-'+_self.properties.rand).show();
			}
		}
	}
	
	this.load_results = function(){
		var url = _self.properties.siteUrl + _self.properties.forms[_self.properties.currentForm] + '/' + _self.properties.searchUrl + '/' + _self.properties.currentForm;
		$.ajax({
			url: url, 
			type: 'POST',
			data: $('#main_search_form_'+_self.properties.rand).serialize(),
			cache: false,
			success: function(data){
				$('#main_' + _self.properties.currentForm + '_form').html(data);
			}
		});
		
	}
	
	this.preload_results = function(){
		var date = new Date();

		if(_self.properties.preloadUTS != 0){
			if(date.getTime() - _self.properties.preloadUTS < _self.properties.preloadInterval){
				if(_self.properties.preloadTID == false){
					_self.properties.preloadTID = setTimeout(_self.preload_results, _self.properties.preloadInterval);
				}
				return;
			}
		}
		
		_self.properties.preloadTID = false;
		_self.properties.preloadUTS = date.getTime();
		
		var url = _self.properties.siteUrl + _self.properties.forms[_self.properties.currentForm] + '/' + _self.properties.countUrl + '/' + _self.properties.currentForm;
		
		$.ajax({
			url: url, 
			type: 'POST',
			data: $('#main_search_form_'+_self.properties.rand).serialize(),
			cache: false,
			dataType: 'json',			
			success: function(data){
				$('#search-preresult-'+_self.properties.rand).html(data.string);
			}
		});
	}
	
	_self.Init(optionArr);
}
