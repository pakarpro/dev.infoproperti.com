function selectBox(optionArr){
	this.properties = {
		labelClass: 'label',
		arrowClass: 'arrow',
		dropdownClass: 'dropdown',
		dataClass: 'data',
		elementsIDs: []	
	}
	var _self = this;
	
	this.Init = function(options){
		_self.properties = $.extend(_self.properties, options);
		for(var m in _self.properties.elementsIDs){
			_self.initBox(_self.properties.elementsIDs[m]);
		}
		_self.initBg();
	}
	
	this.initBg = function(){
		$('body').append('<div id="select_box_bg"></div>');
		$('#select_box_bg').css({
			'display': 'none',
			'position': 'fixed',
			'z-index': '98999',
			'width': '1px',
			'height': '1px',
			'left': '1px',
			'top': '1px'
		});
	}
	
	this.clear = function(){
		for(var m in _self.properties.elementsIDs){
			_self.unsetBox(_self.properties.elementsIDs[m]);
		}
	}
	
	this.expandBg = function(box_id){

		$('#select_box_bg').css({
			'width': $(window).width()+'px',
			'height': $(window).height()+'px',
			'display': 'block'
		}).bind('click', function(){
			_self.closeBox(box_id);
		});
		
	}
	
	this.collapseBg = function(){
		$('#select_box_bg').css({
			'width': '1px',
			'height': '1px',
			'display': 'none'
		}).unbind();
	}
	
	this.initBox = function(box_id){
		if($('#'+box_id+'_box').length<1) return;
		_self.createDropDown(box_id);
		$('#'+box_id+'_box, #'+box_id+'_dropdown').bind('click', function(){
			if($('#'+box_id+'_dropdown:visible').length > 0){
				_self.closeBox(box_id);
			}else{
				_self.openBox(box_id);
			}
		});
		_self.setDefault(box_id);
		$('#'+box_id+'_dropdown li').live('click', function(){
			_self.setActiveBox(box_id, $(this));
		});
	}
	
	this.unsetBox = function(box_id){
		$('#'+box_id+'_dropdown li').unbind().remove();
		$('#'+box_id+'_dropdown').unbind().remove();
		$('#'+box_id+'_box').unbind();
	}
	
	this.openBox = function(box_id){
		_self.expandBg(box_id);
		_self.resetDropDown(box_id);
		$('#'+box_id+'_dropdown').slideDown();
	}
	
	this.createDropDown = function(box_id){
		var data = $('#'+box_id+'_box .'+_self.properties.dataClass).html();
		$('body').append('<div class="'+_self.properties.dropdownClass+'" id="'+box_id+'_dropdown">'+data+'</div>');
		_self.resetDropDown(box_id);
	}
	
	this.resetDropDown = function(box_id){
		var top = $('#'+box_id+'_box').offset().top + $('#'+box_id+'_box .label').outerHeight();
		$('#'+box_id+'_dropdown').css({
			width: $('#'+box_id+'_box').width()+'px',
			left: $('#'+box_id+'_box').offset().left+'px',
			top: top +'px'
		});
	}
	
	this.closeBox = function(box_id){
		_self.collapseBg();
		$('#'+box_id+'_dropdown').slideUp();
	}
	
	this.setActiveBox = function(box_id, item){
		$('#'+box_id+'_dropdown li').removeClass('active');
		item.addClass('active');
		$('#'+box_id+'_box .'+_self.properties.labelClass).html(item.text());
		$('#'+box_id).val(item.attr('gid')).change();
	}
	
	this.resetValues = function(box_id, data, selected){
		if(!selected) selected = $('#'+box_id).val();
		var selected_used = false;

		$('#'+box_id+'_dropdown > ul > li[gid!=""]').remove();

		if(data){
			for(var m in data){
				$('#'+box_id+'_dropdown ul').append('<li gid="'+m+'">'+data[m]+'</li>');
				if(data[m].id == selected){
					_self.setActiveBox(box_id, $('#'+box_id+'_dropdown li[gid="'+m+'"]'));
					selected_used = true;
				}
			}	
		}
		if(selected == 0 || !selected_used){
			_self.setDefault(box_id);
		}
	}
	
	this.setDefault = function(box_id){
		selected = $('#'+box_id).val();
		if(!selected) selected = $('#'+box_id+'_dropdown > ul > li:first').attr('gid');
		_self.setActiveBox(box_id, $('#'+box_id+'_dropdown li[gid="'+selected+'"]'));
	}
	_self.Init(optionArr);		
}	
