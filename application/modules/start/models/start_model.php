<?php  
/**
* Start model
* 
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Start_model extends Model
{
	var $CI;

	function Start_model()
	{
		parent::Model();
		$this->CI = & get_instance();
	}


	////// seo
	function get_seo_settings($method='', $lang_id=''){
		if(!empty($method)){
			return $this->_get_seo_settings($method, $lang_id);
		}else{
			$actions = array('index');
			$return = array();
			foreach($actions as $action){
				$return[$action] = $this->_get_seo_settings($action, $lang_id);
			}
			return $return;
		}
	}

	function _get_seo_settings($method, $lang_id=''){
		if($method == "index"){
			return array(
				"title" => l('seo_tags_index_title', 'start', $lang_id, 'seo'),
				"keyword" => l('seo_tags_index_keyword', 'start', $lang_id, 'seo'),
				"description" => l('seo_tags_index_description', 'start', $lang_id, 'seo'),
				"templates" => array()
			);
		}
	}

	function get_sitemap_xml_urls(){
		$this->CI->load->helper('seo');
		$return = array(
			array(
				"url" => rewrite_link('start', 'index'),
				"priority" => 0.1
			)
		);
		return $return;
	}

	function get_sitemap_urls(){
		$this->CI->load->helper('seo');

		$block[] = array(
			"name" => l('header_start_page', 'start'),
			"link" => rewrite_link('start', 'index'),
			"clickable" => true,
		);
		return $block;
	}

	function clear_trash_folder(){
		$result = false;
		if ($handle = opendir(SITE_PHYSICAL_PATH . TRASH_FOLDER)){
			while (false !== ($file = readdir($handle))){
				if ($file != "." && $file != ".."){
					@unlink(SITE_PHYSICAL_PATH . TRASH_FOLDER . $file);
				}
			}
			closedir($handle);
			$result = true;
		}
		return $result;
	}

	////// banners callback method
	public function _banner_available_pages(){
		$return[] = array("link"=>"start/index", "name"=> l('header_start_page', 'start'));
		$return[] = array("link"=>"start/homepage", "name"=> l('header_homepage', 'start'));
		return $return;
	}
	
	public function _dynamic_block_get_stat_block() {
		$stat = array();

		if($this->CI->pg_module->is_module_installed("users")){
			$this->CI->load->model("Users_model");
			$user_types = $this->CI->Users_model->get_user_types();
			foreach($user_types as $user_type){
				$where = array();
				$where["where"]["user_type"] = $user_type;
				$stat[] = array(
					"object_name" => l($user_type, "users"),
					"count" 	  => $this->CI->Users_model->get_active_users_count($where),
				); 
		
			}
		}
		
		if($this->CI->pg_module->is_module_installed("listings")) {
			$this->CI->load->model("Listings_model");
			$where = array();
			$where["where"]["status"] = 1;
			$stat[] = array(
				"object_name" => l("listings", "start"),
				"count" => $this->CI->Listings_model->get_listings_count($where),
				"last" => 1
			);
		}
		
		if(count($stat)) {
			$this->CI->template_lite->assign('stat', $stat);
			return $this->CI->template_lite->fetch('stat_block', 'user', 'start');
		} else {
			return false;
		}
	}
	
	public function _dynamic_block_get_search_form(){
		$this->CI->load->helper("start");
		return main_search_form("sale", "short", true);
	}
}
