<?php

/**
* Start admin side controller
* 
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (��, 02 ��� 2010) $ $Author: kkashkova $
**/


Class Admin_start extends Controller
{
	/**
	 * link to CodeIgniter object
	 * @var object
	 */

	private $numerics_settings = array(
		"start" => array(
			array('name'=>'admin_items_per_page', 'type'=>'int'),
			array('name'=>'index_items_per_page', 'type'=>'int'),
		),
		"listings" => array(
			array('name'=>"rows_per_page", 'type'=>'int'),
		),
		"mailbox" => array(
			array('name'=>'items_per_page', 'type'=>'int'),
		),
		"news" => array(
			array('name'=>'userside_items_per_page', 'type'=>'int'),
		),
		"payments" => array(
			array('name'=>'items_per_page', 'type'=>'int'),
		),
	);
	
	private $formats_settings = array(
		"start" => array(
			array('name'=>'use_phone_format', 'type'=>'checkbox'),
			array('name'=>'phone_format', 'type'=>'text'),
		),
	);
	
	private $widgets_settings = array(
		"listings" => array(
			array('name'=>'use_map_in_search', 'type'=>'checkbox'),
			array('name'=>'use_poll_in_search', 'type'=>'checkbox'),
			array('name'=>'show_mortgage_calc', 'type'=>'checkbox'),
		),
		"users" => array(
			array('name'=>'use_map_in_search', 'type'=>'checkbox'),
			array('name'=>'use_poll_in_search', 'type'=>'checkbox'),
		),
	);
	
	private $other_settings = array(
		"countries" => array(
			array('name'=>'output_country_format', 'type'=>'text'),
			array('name'=>'output_region_format', 'type'=>'text'),
			array('name'=>'output_city_format', 'type'=>'text'),
			array('name'=>'output_address_format', 'type'=>'text'),
		),		
		"listings" => array(
			array('name'=>'use_moderation', 'type'=>'checkbox'),
			array('name'=>'similar_items', 'type'=>'int'),
			array('name'=>'slide_show_images', 'type'=>'int'),
			array('name'=>'default_activation_period', 'type'=>'int'),
			array('name'=>'moderation_send_mail', 'type'=>'checkbox'),
			array('name'=>'admin_moderation_emails', 'type'=>'email'),
			array('name'=>'slider_auto', 'type'=>'checkbox'),
			array('name'=>'slider_rotation', 'type'=>'int'),
			array('name'=>'slider_min_width', 'type'=>'int'),
			array('name'=>'slider_min_height', 'type'=>'int'),
		),
	);
	/**
	 * Constructor
	 */
	function Admin_start()
	{
		parent::Controller();
		
		if($this->pg_module->is_module_installed("listings")){
			$this->load->model('Listings_model');
			$operation_types = array_reverse($this->Listings_model->operations_arr);
			foreach($operation_types as $operation_type){
				array_unshift($this->other_settings['listings'], array('name'=>'for_'.$operation_type.'_enabled', 'type'=>'checkbox'));
			}
		}
	}
	
	function index(){
		if($this->session->userdata("auth_type") != 'admin'){
			redirect(site_url()."admin/ausers/login");
		}

		$this->system_messages->set_data('header', l("header_admin_homepage", 'start'));

		if($this->session->userdata("user_type") == "moderator"){
			$this->template_lite->view('index_moderator');
		}else{
			$this->template_lite->view('index');
		}
	}
	
	function error($error_type='', $request_method=''){
		if($request_method == 'ajax'){
			$this->template_lite->assign('ajax', '1');
		}
		$this->template_lite->view('error');
	}

	function mod_login(){
		$data["action"] = site_url()."admin/install/login";
		$this->template_lite->assign("data", $data);
		
		$this->system_messages->set_data('header', l("header_modinstaller_login", 'start'));
		$this->template_lite->view('modules_login');
		return;
	}
	
	function menu($menu_item_gid){
		$this->load->model('Menu_model');
		$this->Menu_model->set_menu_active_item('admin_menu', $menu_item_gid);
		
		//// add link to menu
		$menu_data = $this->Menu_model->get_menu_by_gid('admin_menu');
		$menu_item = $this->Menu_model->get_menu_item_by_gid($menu_item_gid, $menu_data["id"]);

		$user_type = $this->session->userdata("user_type");
		if($user_type == "admin"){
			$menu_data["check_permissions"] = false;
			$permissions = array();
		}else{
			$menu_data["check_permissions"] = true;
			$permissions = $this->session->userdata("permission_data");
		}
		$menu_items = $this->Menu_model->get_menu_active_items_list($menu_data["id"], $menu_data["check_permissions"], array(), $menu_item["id"], $permissions);
		
		$this->template_lite->assign("menu", $menu_item);
		$this->template_lite->assign("options", $menu_items);
		$this->system_messages->set_data('header', l("header_settings_list", 'start')."".$menu_item["value"]);
		$this->template_lite->view('menu_list');
		return;
	}
	
	function settings($section="overview"){
		$this->load->model('Menu_model');
		$this->Menu_model->set_menu_active_item('admin_menu', 'system-items');
		
		if(!$this->pg_module->is_module_installed("mailbox")){
			unset($this->numerics_settings['mailbox']);
		}
		
		if(!$this->pg_module->is_module_installed("news")){
			unset($this->numerics_settings['news']);
		}
		
		if(!$this->pg_module->is_module_installed("payments")){
			unset($this->numerics_settings['payments']);
		}
		
		if(!$this->pg_module->is_module_installed("countries")){
			unset($this->other_settings['countries']);
		}
		
		if(!$this->pg_module->is_module_installed("listings")){
			unset($this->numerics_settings['listings']);
			unset($this->widgets_settings['listings']);
			unset($this->other_settings['listings']);
		}
		
		if(!$this->pg_module->is_module_installed("users")){
			unset($this->widgets_settings['users']);
		}
		
		if($this->input->post('btn_save')){
			$errors = array();
			$post_data = $this->input->post('settings', true);
			if($section == 'numerics' || $section == 'formats' || $section == 'widgets'){
				foreach($this->{$section.'_settings'} as $module => $settings){
					foreach($settings as $var){
						if(!isset($post_data[$module][$var['name']])) continue;
						switch($var['type']){
							case 'checkbox':
								$value = !empty($post_data[$module][$var['name']]);
								$this->pg_module->set_module_config($module, $var['name'], $value);
							break;
							case 'int':
								$value = !empty($post_data[$module][$var['name']])?intval($post_data[$module][$var['name']]):0;
								if(empty($value)){
									$errors[] = l('error_numerics_empty', 'start', '', 'text', array('field' => l($module."_".$var['name']."_field", "start")));
								}else{
									$this->pg_module->set_module_config($module, $var['name'], $value);
								}
							break;
							case 'email':
								$value = trim(strip_tags($post_data[$section][$var['name']]));
								if(!empty($value)){
									$this->CI->config->load("reg_exps", TRUE);
									$email_expr = $this->CI->config->item("email", "reg_exps");
									$chunks = explode(',', $value);
									foreach($chunks as $chunk){
										if(empty($chunk) || !preg_match($email_expr, trim($chunk))){
											$errors[] = l("error_invalid_email", "start");
											break;
										}
									}
									if(!empty($errors)) break;
									$this->pg_module->set_module_config($section, $var['name'], $value);
								}elseif($section == 'listings' && $var['name'] == 'admin_moderation_emails'){
									if(isset($post_data['listings']["moderation_send_mail"])){
										$moderation_send_mail = $post_data['listings']["moderation_send_mail"] ? 1 : 0;
									}else{
										$moderation_send_mail = $this->pg_module->get_module_config('listings', 'moderation_send_mail');
									}
									if($moderation_send_mail) $errors[] = l("error_empty_email", "start");
								}
							break;
							default:
								$value = $post_data[$module][$var['name']];
								$this->pg_module->set_module_config($module, $var['name'], $value);
							break;
						}						
					}
				}
			}else{
				$settings = $this->other_settings[$section];
				foreach($settings as $var){
					if(!isset($post_data[$var['name']])) continue;
					switch($var['type']){
						case 'checkbox':
							$value = !empty($post_data[$var['name']]);
							$this->pg_module->set_module_config($section, $var['name'], $value);
						break;
						case 'int':
							$value = !empty($post_data[$var['name']])?intval($post_data[$var['name']]):0;
							if(empty($value)){
								$errors[] = l('error_numerics_empty', 'start', '', 'text', array('field' => l($section."_".$var['name']."_field", "start")));
							}else{
								$this->pg_module->set_module_config($section, $var['name'], $value);
							}
						break;
						case 'email':
							$value = trim(strip_tags($post_data[$var['name']]));
							if(!empty($value)){
								$this->config->load("reg_exps", TRUE);
								$email_expr = $this->config->item("email", "reg_exps");
								$chunks = explode(',', $value);
								foreach($chunks as $chunk){
									if(empty($chunk) || !preg_match($email_expr, trim($chunk))){
										$errors[] = l("error_invalid_email", "start");
										break;
									}
								}
								if(!empty($errors)) break;
								$this->pg_module->set_module_config($section, $var['name'], $value);
							}elseif($section == 'listings' && $var['name'] == 'admin_moderation_emails'){
								if(isset($post_data["moderation_send_mail"])){
									$moderation_send_mail = $post_data["moderation_send_mail"] ? 1 : 0;
								}else{
									$moderation_send_mail = $this->pg_module->get_module_config('listings', 'moderation_send_mail');
								}
								if($moderation_send_mail) $errors[] = l("error_empty_email", "start");
							}
						break;
						default:
							$value = $post_data[$var['name']];
							$this->pg_module->set_module_config($section, $var['name'], $value);
						break;
					}
				}
			}

			if(empty($errors)){
				$this->system_messages->add_message('success', l('success_update_numerics_data', 'start'));
			}else{
				$this->system_messages->add_message('error', $errors);
			}
		}

		switch($section){
			case 'overview':
				foreach($this->numerics_settings as $module => $settings){
					$settings_data["numerics"][$module]["name"] = l($module."_settings_module", "start");
					foreach($settings as $var){
						$settings_data["numerics"][$module]['vars'][] = array(
							"field" => $var['name'],
							"field_name" => l($module."_".$var['name']."_field", "start"),
							'type' => $var['type'],
							"value" => $this->pg_module->get_module_config($module, $var['name'])
						);
					}
				}
				foreach($this->formats_settings as $module => $settings){
					$settings_data["formats"][$module]["name"] = l($module."_settings_module", "start");
					foreach($settings as $var){
						$settings_data["formats"][$module]['vars'][] = array(
							"field" => $var['name'],
							"field_name" => l($module."_".$var['name']."_field", "start"),
							'type' => $var['type'],
							"value" => $this->pg_module->get_module_config($module, $var['name'])
						);
					}
				}
				foreach($this->widgets_settings as $module => $settings){
					$settings_data["widgets"][$module]["name"] = l($module."_settings_module", "start");
					foreach($settings as $var){
						$settings_data["widgets"][$module]['vars'][] = array(
							"field" => $var['name'],
							"field_name" => l($module."_".$var['name']."_field", "start"),
							'type' => $var['type'],
							"value" => $this->pg_module->get_module_config($module, $var['name'])
						);
					}
				}
				foreach($this->other_settings as $module => $settings){
					$settings_data["other"][$module]["name"] = l($module."_settings_module", "start");
					foreach($settings as $var){
						$settings_data["other"][$module]['vars'][] = array(
							"field" => $var['name'],
							"field_name" => l($module."_".$var['name']."_field", "start"),
							'type' => $var['type'],
							"value" => $this->pg_module->get_module_config($module, $var['name'])
						);
					}
				}
			break;
			case 'numerics':
			case 'formats':
			case 'widgets':
				foreach($this->{$section.'_settings'} as $module => $settings){
					$settings_data[$module]["name"] = l($module."_settings_module", "start");
					foreach($settings as $var){
						$settings_data[$module]['vars'][] = array(
							"field" => $var['name'],
							"field_name" => l($module."_".$var['name']."_field", "start"),
							'type' => $var['type'],
							"value" => $this->pg_module->get_module_config($module, $var['name'])
						);
					}
				}
			break;
			default:
				$settings_data["name"] = l($section."_settings_module", "start");
				foreach($this->other_settings[$section] as $var){
					if($section == 'listings' && $var['name'] == 'admin_moderation_emails'){
						$admin_moderation_email = array(
							"field" => $var['name'],
							"field_name" => l($section."_".$var['name']."_field", "start"),
							'type' => $var['type'],
							"value" => $this->pg_module->get_module_config('listings', 'admin_moderation_emails'),
						);
						$this->template_lite->assign("admin_moderation_emails", $admin_moderation_email);
					}else{
						$settings_data['vars'][] = array(
							"field" => $var['name'],
							"field_name" => l($section."_".$var['name']."_field", "start"),
							'type' => $var['type'],
							"value" => $this->pg_module->get_module_config($section, $var['name']),
						);
					}
				}
			break;
		}
		
		$this->template_lite->assign("section", $section);
		$this->template_lite->assign("settings_data", $settings_data);
		$this->template_lite->assign("numerics_settings", $this->numerics_settings);
		$this->template_lite->assign("other_settings", $this->other_settings);
		$this->system_messages->set_data('header', l("header_settings_numerics_list", 'start'));
		$this->template_lite->view('numerics_list');
		return;
	}
	
	function lang_inline_editor($is_textarea=0){
		$this->template_lite->assign('langs', $this->pg_language->languages);
		$this->template_lite->assign('is_textarea', $is_textarea);
		$this->template_lite->view('helper_lang_inline_editor');
		return;
	}
}
