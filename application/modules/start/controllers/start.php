<?php
/**
* Start user side controller
*
* @package PG_RealEstate
* @subpackage application
* @category	modules
* @copyright Pilot Group <http://www.pilotgroup.net/>
* @author Katya Kashkova <katya@pilotgroup.net>
* @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/


Class Start extends Controller
{
	/**
	 * link to CodeIgniter object
	 * @var object
	 */

	/**
	 * Constructor
	 */
	function Start()
	{
		parent::Controller();
		$this->load->model('Menu_model');
	}

	function index(){
		//echo site_url(); exit;
		/*if($this->session->userdata("auth_type") == "user" && $this->session->userdata('user_id')){
			redirect(site_url()."start/homepage");
		}*/
		$this->template_lite->view('index');
		//$this->load->view('under-construction.html');
	}

	function homepage(){

		$user_type = $this->session->userdata('user_type');
		#MOD FOR NEWS PRIVILEGE3
		$news_privilege = $this->session->userdata("news_privilege");
		$this->template_lite->assign('news_privilege', $news_privilege);
		#END OF MOD#		
		$stats = array();
		$this->Menu_model->breadcrumbs_set_parent($user_type.'_overview_item');
		$this->template_lite->assign('user_type', $user_type);
		$this->template_lite->view('homepage');
	}

	function error(){
		header("HTTP/1.0 404 Not Found");
		$this->Menu_model->breadcrumbs_set_active(l('header_error', 'start'));
		$this->template_lite->view('error');
	}

	function print_version(){
		echo $this->pg_module->get_module_config('start', 'product_version');
	}

	//// test methods
	function test_file_upload(){

		$this->load->model("file_uploads/models/File_uploads_config_model");

		$configs = $this->File_uploads_config_model->get_config_list();
		$this->template_lite->assign('configs', $configs);

		if($this->input->post('btn_save') && $this->input->post('config') ){
			$config = $this->input->post('config');
			$file_name = 'file';

			if( isset($_FILES[$file_name]) && is_array($_FILES[$file_name]) && is_uploaded_file($_FILES[$file_name]["tmp_name"])){
				$this->load->model("File_uploads_model");
				$return = $this->File_uploads_model->upload($config, '', $file_name);

				if(!empty($return["errors"])){
					$this->system_messages->add_message('error', $return["errors"]);
				}else{
					$this->system_messages->add_message('success', $return["file"]);
				}
			}
		}

		$this->template_lite->view('test_file_upload');
	}

	function demo($type=null){
		$this->session->set_userdata('demo_user_type', $type);
		redirect();
	}
	
	/**
	 * Has to be here, because users_services may not be installed.
	 *
	 * @param int $id_contact
	 */
	public function ajax_available_contact($id_contact) {
		if ($this->session->userdata('auth_type') != 'user') {
			$return = array('available' => 0, 'content' => '', 'display_login' => 1);
			echo json_encode($return);
		} elseif(!$this->pg_module->is_module_installed('users_services')) {
			$return = array('available' => 1, 'content' => '', 'display_login' => 0);
			echo json_encode($return);
		} else {
			redirect(site_url() . 'users_services/ajax_available_contact/' . $id_contact);
		}
	}
	
	/**
	 * Return languages form
	 */
	function lang_inline_editor($is_textarea=0){
		$this->template_lite->assign('langs', $this->pg_language->languages);
		$this->template_lite->assign('is_textarea', $is_textarea);
		$this->template_lite->view('helper_lang_inline_editor');
		return;
	}
}
