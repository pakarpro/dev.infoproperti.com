<?php
$module['module'] = 'mail_list';
$module['install_name'] = 'Mailing lists';
$module['install_descr'] = 'Mailing lists management';
$module['version'] = '1.03';

$module['files'] = array(
	array('file', 'read', "application/modules/mail_list/controllers/admin_mail_list.php"),
	array('file', 'read', "application/modules/mail_list/install/module.php"),
	array('file', 'read', "application/modules/mail_list/install/permissions.php"),
	array('file', 'read', "application/modules/mail_list/install/structure_deinstall.sql"),
	array('file', 'read', "application/modules/mail_list/install/structure_install.sql"),
	array('file', 'read', "application/modules/mail_list/js/admin-mail-list.js"),
	array('file', 'read', "application/modules/mail_list/models/mail_list_install_model.php"),
	array('file', 'read', "application/modules/mail_list/models/mail_list_model.php"),
	array('file', 'read', "application/modules/mail_list/views/admin/css/style-ltr.css"),
	array('file', 'read', "application/modules/mail_list/views/admin/css/style-rtl.css"),
	array('file', 'read', "application/modules/mail_list/views/admin/filters.tpl"),
	array('file', 'read', "application/modules/mail_list/views/admin/users_form.tpl"),
	array('file', 'read', "application/modules/mail_list/views/admin/users.tpl"),
	array('dir', 'read', 'application/modules/mail_list/langs'),
);

$module['dependencies'] = array(
	'start' => array('version'=>'1.01'),
	'menu' => array('version'=>'1.01'),
	'users' => array('version'=>'1.01'),
	'subscriptions' => array('version'=>'1.01'),
);
$module['linked_modules'] = array(
	'install' => array(
		'menu'		=> 'install_menu',
		'ausers'	=> 'install_ausers'
	),
	'deinstall' => array(
		'menu'		=> 'deinstall_menu'
	)
);
