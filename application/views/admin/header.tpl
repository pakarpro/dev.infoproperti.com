<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html DIR="{$_LANG.rtl}">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="expires" content="0">
	<meta http-equiv="pragma" content="no-cache">
	<meta name="revisit-after" content="3 days">
	<meta name="robot" content="All">
	{seotag tag='title|description|keyword'}

	{helper func_name=css helper_name=theme func_param=$load_type}
	<script type="text/javascript">
		var site_url = '{$site_url}';
		var site_rtl_settings = '{$_LANG.rtl}';
		var site_error_position = 'left';
	</script>

	<link rel="shortcut icon" href="{$site_root}favicon.ico">
	{helper func_name=js helper_name=theme func_param=$load_type}
</head>

<body>
{*pg_include_file=demo_help_admin_menu.html*}
{helper func_name=demo_panel helper_name=start func_param='admin'}
	<div id="error_block">{foreach item=item from=$_PREDEFINED.error}{$item}<br>{/foreach}</div>
	<div id="info_block">{foreach item=item from=$_PREDEFINED.info}{$item}<br>{/foreach}</div>
	<div id="success_block">{foreach item=item from=$_PREDEFINED.success}{$item}<br>{/foreach}</div>

	<div class="bg">
		<div class="main">
			<!-- Left column -->
			<div class="lc">
				<!-- Logo -->
				<div class="logo">
					<div class="b">
						<a href="{$site_url}admin/"><img src="{$site_root}{$logo_settings.path}" border="0" alt="logo" width="{$logo_settings.width}" height="{$logo_settings.height}"></a>
					</div>
				</div>

				<!-- Menu -->
				{if $initial_setup}
					{helper func_name=get_initial_setup_menu helper_name=install func_param=$step}
				{elseif $modules_setup}
					{helper func_name=get_modules_setup_menu helper_name=install func_param=$step}
				{elseif $product_setup}
					{helper func_name=get_product_setup_menu helper_name=install func_param=$step}
				{else}
					{helper func_name=get_admin_main_menu helper_name=menu}
				{/if}

			</div>

			<!-- Right column -->
			<div class="rc">
			{if $auth_type eq 'admin'}
				<div class="w-login-str">
					<div class="version">{helper func_name=product_version helper_name=start}</div>
				{if !$initial_setup && !$modules_setup && !$product_setup}
					{block name=lang_select module=languages}
				{/if} |
				<a href="https://pilotgroup.zendesk.com/home" target="_blank">{l i='link_get_help' module='start'}</a>
				{*<select onchange="redirectDocumentation($('#redirect_drop').val());" id="redirect_drop">
				<option value="" selected>Get help</option>
				<option value="http://docs.pilotgroup.net/display/RealEstateSitePro/Home">Documentation</option>
				<option value="http://server.iad.liveperson.net/hc/12270993/?cmd=file&file=visitorWantsToChat&site=12270993&imageUrl=http://server.iad.liveperson.net">Ask a question</option>
				<option value="http://community.pilotgroup.net/pilotgroup/products/pilotgroup_pg_realestate_site_pro">Share an idea</option>
				<option value="http://community.pilotgroup.net/pilotgroup/products/pilotgroup_pg_realestate_site_pro">Report a problem</option>
				<option value="http://community.pilotgroup.net/pilotgroup/products/pilotgroup_pg_realestate_site_pro">Give praise</option>
				</select>*}
				<script>{literal}
					function redirectDocumentation(value) {
						if (value) {
							document.location.href = value;
						}
						return false;
					}
				{/literal}</script>

				{if $modules_setup} | <a href="{$site_url}admin/install/logoff" class="logoff">LogOff</a>
				{elseif $initial_setup || $product_setup}
				{elseif $auth_type eq 'admin'} | <a href="{$site_url}admin/ausers/logoff" class="logoff">LogOff</a>
				{/if}
				</div>
				{/if}
				<div class="w-area">
					<div class="b">
{if $_PREDEFINED.back_link}
							<div class="quest-block">
								 <a href="{$_PREDEFINED.back_link}" class="back">{l i='btn_back' gid='start'}</a>&nbsp;
{*								<a href="#"><img src="{$site_root}{$img_folder}icon-quest.gif" width="30" height="31" border="0" alt="ask me!"></a> *}
							</div>
{/if}
							<h1>{$_PREDEFINED.header}</h1>
