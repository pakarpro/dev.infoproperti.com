<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html DIR="{$_LANG.rtl}">
<head>
	<meta name="alexaVerifyID" content="CbAsMUFljkRZ0p5Dw3R7wCKjB9U"/>
	<meta name="msvalidate.01" content="DB03CDF777924BCFEEA97FE8A36E329A" />
	<meta name="google-site-verification" content="bpvQb7SNgF0hvwHRwIWurxJKSvGRnnWi32KVCBOqtSE" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="expires" content="0">
	<meta http-equiv="pragma" content="no-cache">
	<meta name="revisit-after" content="3 days">
	<meta name="robot" content="All">
	{seotag tag='title|description|keyword'}
	{helper func_name=css helper_name=theme func_param=$load_type}
	<?php /* <link type="text/css" href="{$site_url}application/views/default/sets/ipblue/css/general-ltr.css">
	<link type="text/css" href="{$site_url}application/views/default/sets/ipblue/css/style-ltr.css">
	<link type="text/css" href="{$site_url}application/views/default/sets/ipblue/css/print-ltr.css"> */ ?>
   
	<script type="text/javascript">
		var site_url = '{$site_url}';
		var site_rtl_settings = '{$_LANG.rtl}';
		var site_error_position = 'center';
	</script>
	
	<link rel="shortcut icon" href="{$site_root}favicon.ico">
	{helper func_name=js helper_name=theme func_param=$load_type}

	{helper func_name=banner_initialize module=banners}
	{helper func_name=show_social_networks_head module=social_networking}
	
	{if $_LANG.rtl eq 'rtl'}<!--[if IE]><link rel="stylesheet" type="text/css" href="{$site_root}application/views/default/ie-rtl-fix.css" /><![endif]-->{/if}
	<link rel='stylesheet' type='text/css' media='screen, projection' href='{$site_root}application/views/default/css/jquery.bxslider.css' />
	<script type='text/javascript' src='{$site_root}application/js/jquery.bxslider.js'></script>
	
</head>
<body>

{helper func_name=seo_traker helper_name=seo_module module=seo func_param='top'}
{*pg_include_file=demo_help_menu.html*}
{helper func_name=demo_panel helper_name=start func_param='user'}
{if $display_brouser_error}
	{helper func_name=available_brousers helper_name=start}
{/if}

	<div id="error_block">{foreach item=item from=$_PREDEFINED.error}{$item}<br>{/foreach}</div>
	<div id="info_block">{foreach item=item from=$_PREDEFINED.info}{$item}<br>{/foreach}</div>
	<div id="success_block">{foreach item=item from=$_PREDEFINED.success}{$item}<br>{/foreach}</div>
	
	<div class="header clearfix">
		<div class="content-head clearfix">
			<script>{literal}
				$(function(){
					$('.top_menu ul>li').each(function(i, item){
						var element = $(item);
						var submenu = element.find('.sub_menu_block');
						if(submenu.length == 0) return;
						element.children('a').bind('touchstart', function(){
							if(element.hasClass('hover')){
								element.removeClass('hover');
							}else{
								$('.top_menu ul>li').removeClass('hover');
								element.addClass('hover');
							}
							return false;
						});
						element.children('a').bind('touchenter', function(){
							element.removeClass('hover');
							element.trigger('hover');
							return false;
						}).bind('touchleave', function(){
							element.removeClass('hover').trigger('blur');
							return false;
						});
					});
				});
			{/literal}</script>
			<div class="logo">
				<?php /* DISABLE TEMPORARILY AND REPLACE WITH NEW LOGO AND CODE
				<a href="{$site_url}"><img src="{$base_url}{$logo_settings.path}" border="0" alt="Welcome to Infoproperti.com" width="{$logo_settings.width}" height="{$logo_settings.height}"></a>
				*/?>
				<a href="{$site_url}"><img src="{$base_url}{$logo_settings.path}" border="0" alt="Welcome to Infoproperti.com" width="{*$logo_settings.width*}" height="{*$logo_settings.height*}"></a>
			</div>
	
			<div class="top_menu">
				<div class="container980">
					{if $auth_type eq 'user'}
					{menu gid=$user_session_data.user_type+'_main_menu' template='user_main_menu'}
					{else}
					{menu gid='guest_main_menu' template='user_main_menu'}
					{/if}
				</div>
			</div>
			
			<?php /* disabled and move the content to modules/menu/view/default/user_main_menu.tpl 
			<div id="social-media">
				<ul class="clearfix">
					<li><a id="fb" class="socmed-icons" href="https://www.facebook.com/inproperti" target="blank">Join Infoproperti on Facebook</a></li>
					<li><a id="twitter" class="socmed-icons" href="https://twitter.com/inproperti" target="blank">Join Infoproperti on Twitter</a></li>
					<li><a id="gplus" class="socmed-icons" href="https://plus.google.com/u/0/111307853610134749510/about" target="blank">Join Infoproperti on Google Plus</a></li>
					<li><a id="youtube" class="socmed-icons" href="http://www.youtube.com/channel/UCpY67JNdkJ8xxxeacwXJT4Q" target="blank">Join Infoproperti on Youtube</a></li>
					<!-- <li><a id="pinterest" class="socmed-icons" href="#" target="blank">Join Infoproperti on Pinterest</a></li>-->
					<li><a id="linkedin" class="socmed-icons" href="https://id.linkedin.com/in/infoproperti" target="blank">Join Infoproperti on linkedIn</a></li>
					<li><a id="instagram" class="socmed-icons" href="#">Join Infoproperti on Instagram</a></li>
				</ul>
			</div>
			*/ ?>
			
			<div id="top-login">
				<div id="login-register" class="clearfix">
					<h3 class="dark-blue">Log in / Sign up</h3>
					<form id="login-form" action="{$site_url}users/login" method="post" >
						<!--<div class="f">{l i='field_email' gid='users'}:&nbsp;*</div>-->
						<div class="input-entry">
							<span>Email:*</span>
							<input class="top-login-form-input" type="text" name="email" {if $DEMO_MODE}value="{$demo_user_type_login_settings.login|escape}"{/if}>
						</div>
						<!--<div class="f">{l i='field_password' gid='users'}:&nbsp;*</div>-->
						<div class="input-entry">
							<span>Password:*</span>
							<input class="top-login-form-input" type="password" name="password" {if $DEMO_MODE}value="{$demo_user_type_login_settings.password|escape}"{/if}>
							<span class="v-link"><a href="{$site_url}users/restore">{l i='link_restore' gid='users'}</a></span>
						</div>
						
						{* Don't delete (openid) *}
						{*<h3>{l i='field_open_id' gid='users'}</h3>
						<div class="v"><input type="text" name="user_open_id" class="openid"></div><br>*}
						
						<input class="signin-btn" type="submit" value="{l i='btn_login' gid='start' type='button'}" name="logbtn">
					</form>
					
					<p>
					Pendatang baru di situs ini? Situs kami akan membantu bisnis anda! Buat akun anda untuk mengunakan fitur penuh situs ini.<br /><br />
					Register sebagai:
					<ul id="reg-type">
						<li><a href="{$site_url}registration-as-private-person">Perorangan/Pribadi</a></li>
						<li><a href="{$site_url}registration-as-company">Perusahaan</a></li>
						<li class="no-margin"><a href="{$site_url}registration-as-agent">Agen</a></li>
					</ul>	
					</p>
				</div>
			</div>
			
			<div class="header-menu">
				<ul>
					{block name=users_lang_select module=users}
					{block name=site_currency_select module=users}
					{block name=auth_links module=users}
					{block name=post_listing_button module=listings}
				</ul>
			</div>
			<script>{literal}
				$( "a#ajax_login_link" ).click(function() {
				  $( "div#login-register" ).slideToggle( "fast" );
				});  
			{/literal}</script>
			
		</div>
	</div>
	<div class="clr"></div>
	
	<div class="main">
			
		<div {if $header_type == 'index'}id="index-pg"{/if} {if $header_type ne 'index'}id="general-pg"{/if} class="content clearfix">
			{if $header_type ne 'index'}
			{start_search_form type='line' show_data=1}
			{/if}
			
			{breadcrumbs}