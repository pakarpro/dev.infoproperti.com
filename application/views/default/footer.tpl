		</div>
	</div>	
	{helper func_name=show_banner_place module=banners func_param='bottom-banner'}
	<div class="footer">
		<div class="content">
			{menu gid='user_footer_menu'}
			<div class="copyright">&copy;&nbsp;2014&nbsp;Infoproperti.com</div>
		</div>
	</div>

	{helper func_name=lang_editor module=languages}
	{helper func_name=seo_traker helper_name=seo_module module=seo func_param='footer'}

	
<script>{literal}
$( "li#social-links" ).click(function() {
  $( "ul#social-contents" ).slideToggle( "fast" );
});

$( "h2.finance-tool-btn" ).click(function() {
  $( "div#fin-tools" ).slideToggle( "fast" );
});

$( "button.close-btn" ).click(function() {
  $( "div#fin-tools" ).slideToggle( "fast" );
});

$(".xcloseBtn").click(function() {
	$("#autogen_error_block").slideToggle("fast");
});

$(document).ready(function () {
   $('.feat-comp').bxSlider({
	   auto: false,
	   autoHover:true,
	   touchEnabled: false,
	   autoControls: false,
	   pager: true,
	   minSlides: 4,
	   maxSlides: 4,
	   slideWidth: 220,
	   startSlide: 0
	});
});

{/literal}</script>

<script>{literal}
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-56118169-1', 'auto');
  ga('send', 'pageview');

{/literal}</script>


</body>
</html>
