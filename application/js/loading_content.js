function loadingContent(optionArr){
	var randNumber = Math.round(Math.random(1000)*1000);
	this.properties = {
		loadBlockID: 'user_load_content'+randNumber,
		loadBlockBgID: 'user_load_content_bg'+randNumber,
		loadBlockBgClass: 'load_content_bg',
		linkerObjID: '',
		loadBlockWidth: '50%',
		loadBlockLeftType: 'center',		/// left, right
		loadBlockLeftPoint: 0,
		loadBlockTopType: 'center',		/// top, bottom
		loadBlockTopPoint: 0,
		dir: site_rtl_settings, 			/// rtl
		posPropertyLeft: "left",
		posPropertyRight: "right",
		
		closeBtnID: 'user_load_content_close'+randNumber,
		closeBtnClass: 'load_content_close',
		closeBtnUse: true,
		closeBtnPadding: 5,
		draggable: false,
		showAfterImagesLoad: true,
		contentImages: 0,
		contentLoadedImages: 0,
		
		///// left / right buttons 
		leftBtnID:  'user_load_content_left'+randNumber,
		leftBtnClass: 'load_content_left',
		leftBtnFunction: function(){},
		leftBtnCreated: false,
		
		rightBtnID:  'user_load_content_right'+randNumber,
		rightBtnClass: 'load_content_right',
		rightBtnFunction: function(){},
		rightBtnCreated: false
	}

	var _self = this;

	this.Init = function(options){
		_self.properties = $.extend(_self.properties, options);

		if(_self.properties.dir == 'rtl'){
			_self.properties.posPropertyLeft = "right";
			_self.properties.posPropertyRight = "left";
			
			if(_self.properties.loadBlockLeftType != 'center' && _self.properties.linkerObjID){
				if(_self.properties.loadBlockLeftType == 'left')
					_self.properties.loadBlockLeftType = 'right';
				else
					_self.properties.loadBlockLeftType = 'left';
			}
		}
		
		_self.create_load_block();
	}

	this.extend_errors = function(errors){
		_self.errors = $.extend(_self.errors, errors);
	}

	this.create_load_block = function(){
		$("body").append('<div id="'+_self.properties.loadBlockID+'"></div>');
		if(_self.properties.linkerObjID){
			var position = 'absolute';
		}else{
			var position = 'fixed';
		}
		$("#"+_self.properties.loadBlockID).css({
			'position': position,
			'display': 'none',
			'z-index': '1000',
		});
		if(_self.properties.loadBlockWidth != 'none'){
			$("#"+_self.properties.loadBlockID).css('width', _self.properties.loadBlockWidth);
		}
		
		if(_self.properties.draggable){
			$("#"+_self.properties.loadBlockID).draggable({ handle: "h1" });
		}

		$("body").append('<div id="'+_self.properties.loadBlockBgID+'"></div>');
		$("#"+_self.properties.loadBlockBgID).addClass(_self.properties.loadBlockBgClass);
		$("#"+_self.properties.loadBlockBgID).css({
			'display': 'none',
			'position': 'fixed',
			'z-index': '500',
			'width': '1px',
			'height': '1px',
			'left': '1px',
			'top': '1px'
		});
		
		if(_self.properties.closeBtnUse){
			_self.create_close();
		}
	}

	this.show_load_block = function(content){
		_self.active_bg();
		$("#"+_self.properties.loadBlockID).html(content);
		_self.reposition_load_block();
	
		
		if(_self.properties.showAfterImagesLoad){
			_self.properties.contentImages = $("#"+_self.properties.loadBlockID + ' img').length;
			_self.properties.contentLoadedImages = 0;
			_self.show_after_load();
		}else{
			$("#"+_self.properties.loadBlockID).fadeIn();
			if(_self.properties.closeBtnUse){
				_self.active_close();	
			}
			if(_self.properties.leftBtnCreated){
				_self.active_left();	
			}
			if(_self.properties.rightBtnCreated){
				_self.active_right();	
			}
		}
	}
	
	this.show_after_load = function(){
		if(_self.properties.contentLoadedImages < _self.properties.contentImages){
			$("#"+_self.properties.loadBlockID + ' img').load(function(){
				_self.properties.contentLoadedImages++;
			});
			setTimeout( _self.show_after_load, 300);
		}else{
			_self.reposition_load_block();
			$("#"+_self.properties.loadBlockID).fadeIn();
			if(_self.properties.closeBtnUse){
				_self.active_close();	
			}
			if(_self.properties.leftBtnCreated){
				_self.active_left();	
			}
			if(_self.properties.rightBtnCreated){
				_self.active_right();	
			}
		}
	
	}

	this.reposition_load_block = function(){
		if(_self.properties.linkerObjID){
			_self.properties.loadBlockLeftPoint = $("#"+_self.properties.linkerObjID).offset().left;
			_self.properties.loadBlockTopPoint = $("#"+_self.properties.linkerObjID).offset().top;

			if(_self.properties.loadBlockLeftType == 'right'){
				_self.properties.loadBlockLeftPoint = $(window).width() - _self.properties.loadBlockLeftPoint - $("#"+_self.properties.linkerObjID).width();
			}else if(_self.properties.loadBlockLeftType == 'center'){
				_self.properties.loadBlockLeftPoint = _self.properties.loadBlockLeftPoint - ($("#"+_self.properties.loadBlockID).width() - $("#"+_self.properties.linkerObjID).width())/2;
			}
			if(_self.properties.loadBlockTopType == 'bottom'){
				_self.properties.loadBlockTopPoint = _self.properties.loadBlockTopPoint + $("#"+_self.properties.linkerObjID).height()+15;
			}else if(_self.properties.loadBlockTopType == 'center'){
				_self.properties.loadBlockTopPoint = _self.properties.loadBlockTopPoint - ($("#"+_self.properties.loadBlockID).height() - $("#"+_self.properties.linkerObjID).height())/2;
			}
		}else{
			if(_self.properties.loadBlockLeftType == 'center'){
				_self.properties.loadBlockLeftPoint = ($(window).width() - $("#"+_self.properties.loadBlockID).width())/2;
			}
			if(_self.properties.loadBlockTopType == 'center'){
				_self.properties.loadBlockTopPoint = ($(window).height() - $("#"+_self.properties.loadBlockID).height())/2;
			}
		}

		if(_self.properties.linkerObjID && _self.properties.loadBlockLeftType == 'right'){
			$("#"+_self.properties.loadBlockID).css('right', _self.properties.loadBlockLeftPoint+'px');		
		}else{
			$("#"+_self.properties.loadBlockID).css('left', _self.properties.loadBlockLeftPoint+'px');		
		}
		$("#"+_self.properties.loadBlockID).css('top', _self.properties.loadBlockTopPoint+'px');		
		if(_self.properties.closeBtnUse){
			_self.position_close();	
		}
		if(_self.properties.leftBtnCreated){
			_self.position_left();	
		}
		if(_self.properties.rightBtnCreated){
			_self.position_right();	
		}
	}

	this.hide_load_block = function(){
		if($("#"+_self.properties.loadBlockID).css('display') == 'none'){
			return;
		}
		_self.clear_buttons();
		$("#"+_self.properties.loadBlockID).fadeOut(300, function(){
			$("#"+_self.properties.loadBlockID).html('');
			_self.inactive_bg();
		});
	}
	
	this.clear_buttons = function(){
		if(_self.properties.closeBtnUse){
			_self.inactive_close();		
		}
		if(_self.properties.leftBtnCreated){
			_self.inactive_left();	
		}
		if(_self.properties.rightBtnCreated){
			_self.inactive_right();	
		}	
	}

	this.active_bg = function(){
		if($("#"+_self.properties.loadBlockBgID).css('display') == 'none'){
			$("#"+_self.properties.loadBlockBgID).css('width', $(window).width()+'px');
			$("#"+_self.properties.loadBlockBgID).css('height', $(window).height()+'px');
			$("#"+_self.properties.loadBlockBgID).unbind().bind('click', function(){
				_self.hide_load_block();
			});
			
/*			if(_self.properties.closeBtnUse){
				$("#"+_self.properties.loadBlockBgID).bind('mouseleave', function(){
					_self.active_close();	
				});
				$("#"+_self.properties.loadBlockBgID).bind('mouseenter', function(){
					_self.inactive_close();	
				});
			}*/
			$("#"+_self.properties.loadBlockBgID).fadeIn();
		}
	}

	this.inactive_bg = function(){
		if($("#"+_self.properties.loadBlockBgID).css('display') != 'none'){
			$("#"+_self.properties.loadBlockBgID).fadeOut();		
			$("#"+_self.properties.loadBlockBgID).css('width', '1px');
			$("#"+_self.properties.loadBlockBgID).css('height', '1px');
			$("#"+_self.properties.loadBlockBgID).unbind();
		}
	}
	
	this.active_close = function(){
		if($("#"+_self.properties.closeBtnID).css('display') == 'none'){
			_self.position_close();
			$("#"+_self.properties.closeBtnID).bind('click', function(){
				_self.hide_load_block();
			});
			$("#"+_self.properties.closeBtnID).fadeIn();
		}
	}
	
	this.inactive_close = function(){
		if($("#"+_self.properties.closeBtnID).css('display') != 'none'){
			$("#"+_self.properties.closeBtnID).unbind();
			$("#"+_self.properties.closeBtnID).fadeOut();
		}
	}
	
	this.create_close = function(){
		$("body").append('<div id="'+_self.properties.closeBtnID+'"></div>');
		$("#"+_self.properties.closeBtnID).addClass(_self.properties.closeBtnClass);
		if(_self.properties.linkerObjID)
			var position = 'absolute';
		else
			var position = 'fixed';
		$("#"+_self.properties.closeBtnID).css({
			'cursor': 'pointer',
			'position': position,
			'display': 'none',
			'z-index': '1001'
		});
		_self.position_close();
	}
	
	this.position_close = function(){
		var padding = _self.properties.closeBtnPadding;
		var main_window_left = parseInt($("#"+_self.properties.loadBlockID).offset().left);
		var main_window_top = parseInt($("#"+_self.properties.loadBlockID).css('top'));
		var main_window_width = $("#"+_self.properties.loadBlockID).width();

		if(_self.properties.dir == 'ltr')
			var x = main_window_left + main_window_width - $("#"+_self.properties.closeBtnID).width()-padding;
		else 
			var x = main_window_left + padding;
		var y = main_window_top + padding;

		$("#"+_self.properties.closeBtnID).css('left', x+'px');
		$("#"+_self.properties.closeBtnID).css('top', y+'px');
	}

	this.create_left = function(func){
		_self.inactive_left();
		$("body").append('<div id="'+_self.properties.leftBtnID+'"></div>');
		_self.properties.leftBtnFunction = func;
		_self.properties.leftBtnCreated = true;
		$("#"+_self.properties.leftBtnID).addClass(_self.properties.leftBtnClass);
		if(_self.properties.linkerObjID)
			var position = 'absolute';
		else
			var position = 'fixed';
		$("#"+_self.properties.leftBtnID).css({
			'cursor': 'pointer',
			'position': position,
			'display': 'none',
			'z-index': '1001'
		});
		_self.position_left();		
	}

	this.create_right = function(func){
		_self.inactive_right();
		$("body").append('<div id="'+_self.properties.rightBtnID+'"></div>');
		_self.properties.rightBtnFunction = func;
		_self.properties.rightBtnCreated = true;
		$("#"+_self.properties.rightBtnID).addClass(_self.properties.rightBtnClass);
		if(_self.properties.linkerObjID)
			var position = 'absolute';
		else
			var position = 'fixed';
		$("#"+_self.properties.rightBtnID).css({
			'cursor': 'pointer',
			'position': position,
			'display': 'none',
			'z-index': '1001'
		});
		_self.position_right();		
	}	
	
	this.active_left = function(){
		_self.position_left();
		$("#"+_self.properties.leftBtnID).bind('click', _self.properties.leftBtnFunction);
		$("#"+_self.properties.leftBtnID).fadeIn();
	}
	
	this.inactive_left = function(){
		$("#"+_self.properties.leftBtnID).remove();
		_self.properties.leftBtnCreated = false;
	}
	
	this.active_right = function(){
		_self.position_right();
		$("#"+_self.properties.rightBtnID).bind('click', _self.properties.rightBtnFunction);
		$("#"+_self.properties.rightBtnID).fadeIn();
	}
	
	this.inactive_right = function(){
		$("#"+_self.properties.rightBtnID).remove();
		_self.properties.rightBtnCreated = false;
	}

	this.position_left = function(){
		var padding = 8;
		var main_window_left = parseInt($("#"+_self.properties.loadBlockID).css(_self.properties.posPropertyLeft));
		var main_window_top = parseInt($("#"+_self.properties.loadBlockID).css('top'));
		var main_window_height = $("#"+_self.properties.loadBlockID).height();
		var x = main_window_left + padding;
		var y = main_window_top + (main_window_height - $("#"+_self.properties.leftBtnID).height())/2;
		$("#"+_self.properties.leftBtnID).css(_self.properties.posPropertyLeft, x+'px');
		$("#"+_self.properties.leftBtnID).css('top', y+'px');
	}
	this.position_right = function(){
		var padding = 8;
		var main_window_left = parseInt($("#"+_self.properties.loadBlockID).css(_self.properties.posPropertyLeft));
		var main_window_top = parseInt($("#"+_self.properties.loadBlockID).css('top'));
		var main_window_width = $("#"+_self.properties.loadBlockID).width();
		var main_window_height = $("#"+_self.properties.loadBlockID).height();
		var x = main_window_left + main_window_width - $("#"+_self.properties.rightBtnID).width()-padding;
		var y = main_window_top + (main_window_height - $("#"+_self.properties.rightBtnID).height())/2;
		$("#"+_self.properties.rightBtnID).css(_self.properties.posPropertyLeft, x+'px');
		$("#"+_self.properties.rightBtnID).css('top', y+'px');
	}		
	_self.Init(optionArr);

};