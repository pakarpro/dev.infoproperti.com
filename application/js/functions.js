var loading_object;
var error_object;
var timeout;
$(function(){
	loading_object=new Loading;
	$(document.body).ajaxStart(function() {
		loading_object.setLoading();
	}).ajaxStop(function() {
		loading_object.unsetLoading();
	});

	error_object=new Errors({position: site_error_position});
	timeout = 0;

	if($('#error_block').html()){
		error_object.show_error_block($('#error_block').html(), 'error');
		timeout = 2000;
	}
	if($('#info_block').html()){
		if(timeout){
			setTimeout( function(){
				error_object.show_error_block($('#info_block').html(), 'info');
			}, timeout);
		}else{
			error_object.show_error_block($('#info_block').html(), 'info');
		}
	}
	if($('#success_block').html()){
		if(timeout){
			setTimeout( function(){
				error_object.show_error_block($('#success_block').html(), 'success');
			}, timeout);
		}else{
			error_object.show_error_block($('#success_block').html(), 'success');
		}
	}
});
