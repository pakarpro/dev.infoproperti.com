function galleryUploads(optionArr){
	this.properties = {
		siteUrl: '',
		objectId: 0,
		maxPhotos: 0,
		maxPhotoError: '',
		fileElementId: 'photo_file',
		commentElementId: 'photo_comment',
		uploadPhotoUrl: '',
		savePhotoDataUrl: '',
		saveSortingUrl: '',
		deletePhotoUrl: '',
		formPhotoUrl: '',
		editPhotoUrl: '',
		reloadBlockUrl: '',
		reloadCallback: '',
		deleteCallback: '',
		listItemPrefix: 'pItem',
		listItemID: 'sortList',
		closeAfterSave: true,
		errorObj: new Errors,
		contentObj: new loadingContent({loadBlockWidth: 'no'})
	}

	var _self = this;

	this.Init = function(options){
		_self.properties = $.extend(_self.properties, options);
		_self.set_sorting();
	}

	this.set_sorting = function(){
		$("#"+_self.properties.listItemID).sortable({
			items: 'li',
			scroll: true,
			forcePlaceholderSize: true,
			placeholder: 'limiter',
			revert: true,
			stop: function(event, ui) { 
//				_self.update_closers();
			},
			update: function(event, ui) { 
				_self.update_sorting();
			}
		});
	}
	
	this.update_sorting = function(){
		var data = new Object;
		$("#"+_self.properties.listItemID+" > li").each(function(i){
			data[$(this).attr('id')] = i+1;
		});
		$.ajax({
			url: _self.properties.siteUrl + _self.properties.saveSortingUrl, 
			type: 'POST',
			data: ({sorter: data}), 
			dataType: 'json',
			cache: false,
			success: function(data){
				_self.properties.errorObj.show_error_block(data.success, 'success');
			}
		});
	}

	this.upload_photo = function(id){
		if($('#'+_self.properties.fileElementId).val() != ''){
			var url =  _self.properties.siteUrl+_self.properties.uploadPhotoUrl+id;
			$.ajaxFileUpload({
				url: url,
				fileElementId: _self.properties.fileElementId,
				dataType: 'json',
				success: function(data, status){
					if(typeof(data.error) != 'undefined' && data.error != ''){
						_self.properties.errorObj.show_error_block(data.error, 'error');
					}else{
						if(typeof(data.id) != 'undefined' && data.id != ''){
							_self.save_photo_data(data.id);
						}
					}
				}
			});
		}else{
			if(id){
				_self.save_photo_data(id);
			}
		}
	}

	this.save_photo_data = function(id){
		var url =  _self.properties.siteUrl+_self.properties.savePhotoDataUrl+id;
		var post_data = {comment: $('#'+_self.properties.commentElementId).val()};
		$.ajax({
			url: url, 
			type: 'POST',
			cache: false,
			data: post_data, 
			dataType: 'json',
			success: function(data){
				if(typeof(data.error) != 'undefined' && data.error != ''){
					_self.properties.errorObj.show_error_block(data.error, 'error');
				}else{
					_self.properties.errorObj.show_error_block(data.success, 'success');
					if(_self.properties.closeAfterSave) _self.close_open_form();
				}
				_self.refresh_block_item(id);
			}
		});
	}

	this.refresh_block_item = function(id){
		if(!$("#"+_self.properties.listItemPrefix + id).length){
			$("#"+_self.properties.listItemID).append('<li id="'+_self.properties.listItemPrefix+id+'"></li>');
		}
		var url =  _self.properties.siteUrl+_self.properties.reloadBlockUrl+id;
		$.ajax({
			url: url, 
			cache: false,
			success: function(data){
				$("#"+_self.properties.listItemPrefix + id).html(data);
			}
		});
		if(_self.properties.reloadCallback){
			_self.properties.reloadCallback(id);
		}
	}

	this.delete_block_item = function(id){
		$("#"+_self.properties.listItemPrefix + id).remove();
		_self.properties.contentObj.hide_load_block();
		if(_self.properties.deleteCallback){
			_self.properties.deleteCallback(id);
		}
	}

	this.delete_photo = function(id){
		$.ajax({
			url: _self.properties.siteUrl + _self.properties.deletePhotoUrl+id, 
			type: 'POST',
			dataType: 'json',
			cache: false,
			success: function(data){
				if(typeof(data.error) != 'undefined' && data.error != ''){
					_self.properties.errorObj.show_error_block(data.error, 'error');
				}else{
					_self.properties.errorObj.show_error_block(data.success, 'success');
					_self.delete_block_item(id);
				}
			}
		});
	}

	this.open_edit_form = function(id){
		if(id == 0 && _self.properties.maxPhotos != 0 && $("#"+_self.properties.listItemID+" > li").length >= _self.properties.maxPhotos){
			_self.properties.errorObj.show_error_block(_self.properties.maxPhotoError, 'error');
			return;
		}
		var url =  _self.properties.siteUrl+_self.properties.formPhotoUrl+id;
		$.ajax({
			url: url, 
			cache: false,
			success: function(data){
				_self.properties.contentObj.show_load_block(data);
			}
		});
	}

	this.close_open_form = function(){
		_self.properties.contentObj.hide_load_block();
	}

	this.full_view = function(content){
		content = '<div class="load_content" style="text-align: center"><img src="'+content+'"></div>';
		_self.properties.contentObj.show_load_block(content);
	}
	
	this.open_full_edit = function(id){
		var url =  _self.properties.siteUrl+_self.properties.editPhotoUrl+id;
		$.ajax({
			url: url, 
			cache: false,
			success: function(data){
				_self.properties.contentObj.show_load_block(data);
			}
		});
	}

	_self.Init(optionArr);
}
